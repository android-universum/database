Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 2.x ##

### [2.4.1](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 08.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [2.4.0](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 23.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).
- Small updates and improvements.

### [2.3.3](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 15.05.2018

- Updated instantiation related exceptions thrown by the library to be **more descriptive**.

### [2.3.2](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 21.03.2018

- `EmptyCursor` in **support** version of the library has been **moved** to the proper package.

### [2.3.1](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 06.01.2018

- Annotated `loaderId` method parameters with `@IntRange(...)` annotation.
- Removed deprecated elements from previous versions.
- `Selection` now may be used also to cary array of **arguments** for the selection statement.
- Small updates.

### [2.3.0](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 09.12.2017

- Added `EmptyCursor` implementation.
- `Entity` now returns instance of `EmptyCursor` from `query(...)` method whenever `SQLiteQueryBuilder`
  returns **null** `Cursor`.
- **Updated** to the latest **to date available** dependencies. **Mainly** to the _Android Platform 27 (8.1)_.

### [2.2.0](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 25.07.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_ for both versions of the library.
- Support for new features.
- Fixed some of reported issues.

### [2.1.0](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 21.03.2017

- Minor updates and improvements.

### [2.0.2](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 05.03.2017

- Added `DatabaseExecutors` utility class which provides one **primary** executor used across _Database_
  library for execution of database content related operations on a background thread. This executor
  should be also used by a dependent Android applications so all database operations are executed
  on a **single thread** to prevent inconsistency in data stored in SQLite database which may occur
  if data of database are manipulated from a multiple threads.

### [2.0.1](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 15.02.2017

- `FlagsQuery` now allows to specify also **where** statement.

### [2.0.0](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 05.02.2017

- Removed all deprecated classes and methods from the previous release.

### [2.0.0-beta1](https://bitbucket.org/android-universum/database/wiki/version/2.x) ###
> 02.02.2017

- Fixed `notifyCursorDataSetActionSelected(...)` not being propagated to the adapter's internal
  `onCursorDataSetActionSelected(...)` before it is dispatched to the registered listeners.
- Cursor adapters now call `notifyCursorDataSetInvalidated()` when they receive a **null** `Cursor`
  through `swapCursor(Cursor)` method.

## [Version 1.x](https://bitbucket.org/android-universum/database/wiki/version/1.x) ##
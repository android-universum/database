@Database-Model
===============

This module groups the following modules into one **single group**:

- [Model-Core](https://bitbucket.org/android-universum/database/src/main/library-model-core)
- [Model-Cursor](https://bitbucket.org/android-universum/database/src/main/library-model-cursor)
- [Model-Collection](https://bitbucket.org/android-universum/database/src/main/library-model-collection)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-model:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor)
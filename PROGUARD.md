Proguard
===============

This file describes which proguard rules **should be used** in order to preserve _proper working_ of
the source code provided by this library in case when the **Proguard's obfuscation** process is applied
to a project that depends on this library.

> **Note that the proguard rules listed below are not guarantied to ensure obfuscation that will not
affect proper working of your application. Each application has its specific needs and caveats,
so it is hard to find rules that will satisfy all of them. New general rules may be added in the future.**

### Proguard-Rules ###

> Use below rules to keep **"sensitive"** source code of the library.

    # Keep all database annotations.
    -keep public @interface universum.studios.android.database.annotation.** { *; }
    -keep @interface universum.studios.android.database.**$** { *; }
    # Keep entities implementation details:
    # - public empty constructor [always]
    -keepclassmembers class * implements universum.studios.android.database.DatabaseEntity {
        public <init>();
    }
    # Keep entity models implementation details:
    # - parcelable CREATOR [always],
    # - model FACTORY [avoids instantiation via reflection],
    # - public empty constructor [always],
    # - columns [always],
    -keepclassmembers class * implements universum.studios.android.database.model.EntityModel {
        public static final android.os.Parcelable$Creator CREATOR;
        public static final universum.studios.android.database.model.EntityModel$*Factory FACTORY;
        public <init>();
        @universum.studios.android.database.annotation.Column <fields>;
    }
    # Keep cursor wrappers implementation details:
    # - public constructor taking Cursor parameter [if using wrappers within LoaderAdapter]
    -keepclassmembers class * extends universum.studios.android.database.cursor.BaseCursorWrapper {
        public <init>(android.database.Cursor);
    }
    # Keep annotation handlers implementation details:
    # - constructor taking Class parameter [always]
    -keepclassmembers class * extends universum.studios.android.database.annotation.handler.BaseAnnotationHandler {
        public <init>(java.lang.Class);
    }

> Use below rules to keep **entire** source code of the library.

    # Keep all classes within the library package.
    -keep class universum.studios.android.database.** { *; }
    
Database-LoremIpsum-Entity
===============

This module contains implementation of `ModelEntity` for `LoremIpsum` model.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-loremipsum-entity:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-base](https://bitbucket.org/android-universum/database/src/main/library-base),
[database-query](https://bitbucket.org/android-universum/database/src/main/library-query),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-entity](https://bitbucket.org/android-universum/database/src/main/library-entity_group),
[database-model](https://bitbucket.org/android-universum/database/src/main/library-model_group),
[database-loremipsum-core](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-core),
[database-loremipsum-model](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-model)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [LoremIpsumEntity](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-entity/src/main/java/universum/studios/android/database/loremipsum/LoremIpsumEntity.java)
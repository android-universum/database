/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import universum.studios.android.database.entity.ModelEntity;

/**
 * A {@link ModelEntity} implementation for storing of data represented by {@link LoremIpsum} model
 * within database.
 *
 * <h3>Usage</h3>
 * <pre>
 * public final class MyLoremIpsumEntity extends LoremIpsumEntity {
 *
 *      // Creates an instance of MyLoremIpsumEntity with initial content in size of 1000 entries.
 *      public MyLoremIpsumEntity() {
 *          super(1000);
 *      }
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 */
public class LoremIpsumEntity extends ModelEntity<LoremIpsum> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LoremIpsumEntity";

	/**
	 * Name of the lorem ipsum database table.
	 */
	public static final String NAME = "lorem_ipsum";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Determines size of initial content that should be created for this entity when its database
	 * table is first time created.
	 */
	private final int mInitialContentSize;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #LoremIpsumEntity(int)} with initial content size of {@code 100}.
	 */
	public LoremIpsumEntity() {
		this(100);
	}

	/**
	 * Creates a new instance of LoremIpsumEntity with <b>"lorem_ipsum"</b> as name for database
	 * table and {@link LoremIpsum} as model of this entity.
	 *
	 * @param initialContentSize Size that determines count of {@link LoremIpsum} models created
	 *                           and inserted into database for this entity as its initial content.
	 * @see #onCreateInitialContent(SQLiteDatabase)
	 */
	public LoremIpsumEntity(@IntRange(from = 0) int initialContentSize) {
		super(NAME, LoremIpsum.class);
		this.mInitialContentSize = initialContentSize;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the count of {@link LoremIpsum} models that has been/will be inserted as initial content
	 * of this entity.
	 *
	 * @return Size of this entity's initial content.
	 */
	@IntRange(from = 0)
	public final int getInitialContentSize() {
		return mInitialContentSize;
	}

	/**
	 */
	@Override
	public boolean createsInitialContent() {
		return mInitialContentSize > 0;
	}

	/**
	 */
	@Override
	protected void onCreateInitialContent(@NonNull SQLiteDatabase db) {
		final List<LoremIpsum> initialContent = new ArrayList<>(mInitialContentSize);
		for (int i = 1; i < mInitialContentSize + 1; i++) {
			initialContent.add(new LoremIpsum((long) i));
		}
		insertInitialModels(db, initialContent);
	}

	/**
	 */
	@NonNull
	@Override
	protected LoremIpsum onCreateModel() {
		return LoremIpsum.FACTORY.createModel();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
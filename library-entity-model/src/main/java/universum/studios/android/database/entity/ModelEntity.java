/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.Transaction;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.annotation.handler.EntityAnnotationHandler;
import universum.studios.android.database.annotation.handler.ModelEntityAnnotationHandler;
import universum.studios.android.database.annotation.handler.ModelEntityAnnotationHandlers;
import universum.studios.android.database.content.IdSelection;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.EntityModelArrayList;
import universum.studios.android.database.model.EntityModelList;

/**
 * Extended implementation of {@link Entity} that allows to perform <b>INSERT, QUERY, UPDATE, DELETE</b>
 * operations with using directly entity model instances instead of raw {@link ContentValues} or {@link Cursor}
 * for such purpose.
 * <p>
 * Below are listed all API methods that may be used with entity specific model instance/-s:
 * <ul>
 * <li>{@link #insertModel(EntityModel)}</li>
 * <li>{@link #insertModels(List)}</li>
 * <li>{@link #queryModel(String[], String, String[], String)}</li>
 * <li>{@link #queryModels(String[], String, String[], String)}</li>
 * <li>{@link #updateModel(EntityModel)}</li>
 * <li>{@link #updateModels(List)}</li>
 * <li>{@link #deleteModel(EntityModel)}</li>
 * <li>{@link #deleteModels(List)}</li>
 * </ul>
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link universum.studios.android.database.annotation.Model @Model} <b>[class]</b>
 * <p>
 * If this annotation is presented, a class provided via this annotation will be used to instantiate
 * a new instance of that model for purpose of model related transactions like
 * {@link #queryModel(String[], String, String[], String)} or {@link #queryModels(String[], String, String[], String)}.
 * </li>
 * <li>{@link Entity + super annotations}</li>
 * </ul>
 *
 * @param <M> Type of the model of which data can this entity implementation store and query.
 * @author Martin Albedinsky
 */
public abstract class ModelEntity<M extends EntityModel> extends Entity {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ModelEntity";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Class of this entity's model. This class is used to instantiate a new model instance whenever
	 * it is needed by this entity.
	 */
	private final Class<M> mModelClass;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Like {@link Entity#Entity()} creates a new instance of ModelEntity.
	 * <p>
	 * <b>Note</b>, that this constructor is meant to be called only if required annotation
	 * {@link universum.studios.android.database.annotation.Model @Model} is presented above a
	 * subclass of this ModelEntity, otherwise an exception will be thrown.
	 */
	@SuppressWarnings("unchecked")
	public ModelEntity() {
		super();
		if (!DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) {
			throw DatabaseException.annotationsNotEnabled();
		}
		this.mModelClass = (Class<M>) ((ModelEntityAnnotationHandler) mAnnotationHandler).getModelClass();
		if (mModelClass == null) {
			throw DatabaseException.missingClassAnnotation(Model.class, getClass());
		}
	}

	/**
	 * Like {@link Entity#Entity()} creates a new instance of ModelEntity with the specified <var>name</var>
	 * along with the given <var>classOfModel</var> which is used to instantiate new model instances
	 * when needed.
	 *
	 * @param classOfModel Class of entity model specific for the new entity that is used to create
	 *                     new instances of such model whenever they are needed by the entity. This
	 *                     is in most cases when invoking one of transaction methods that operates
	 *                     with model like {@link #queryModel(String[], String, String[], String)}.
	 */
	protected ModelEntity(@NonNull String name, @NonNull Class<M> classOfModel) {
		super(name);
		this.mModelClass = classOfModel;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	EntityAnnotationHandler onCreateAnnotationHandler() {
		return ModelEntityAnnotationHandlers.obtainModelEntityHandler(getClass());
	}

	/**
	 */
	@NonNull
	@Override
	@SuppressWarnings("unchecked")
	protected ModelEntityAnnotationHandler<M> getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return (ModelEntityAnnotationHandler<M>) mAnnotationHandler;
	}

	/**
	 * Returns class of the model associated with this entity.
	 *
	 * @return Model class.
	 */
	@NonNull
	public final Class<M> getModelClass() {
		return mModelClass;
	}

	/**
	 * @throws DatabaseException If annotations processing is not enabled for the Database library
	 *                           or creation of the database table for this entity has failed.
	 */
	@Override
	protected void onCreate(@NonNull SQLiteDatabase db) {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		final ModelEntityAnnotationHandler annotationsHandler = (ModelEntityAnnotationHandler) mAnnotationHandler;
		if (!annotationsHandler.handleCreate(this, db)) {
			throw DatabaseException.misconfiguration(
					"Failed to create database table(" + mName + ") for ModelEntity(" + getClass().getSimpleName() + ") " +
							"according to table structure defined by Model(" + mModelClass.getSimpleName() + ")."
			);
		}
	}

	/**
	 */
	@Override
	protected void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		super.onUpgrade(db, oldVersion, newVersion);
		if (mAnnotationHandler != null) {
			final ModelEntityAnnotationHandler annotationsHandler = (ModelEntityAnnotationHandler) mAnnotationHandler;
			if (!annotationsHandler.handleUpgrade(this, db, oldVersion, newVersion)) {
				Log.e(getClass().getSimpleName(), "Failed to upgrade database table(" + mName + ") from old version(" + oldVersion + ") to new version(" + newVersion + ").");
			}
		}
	}

	/**
	 * Inserts the specified <var>models</var> into the given <var>db</var> as initial content of
	 * this entity.
	 *
	 * @param db     SQLite database where to insert the specified models (theirs content values)
	 *               into table represented by name of this entity.
	 * @param models The desired list of models that to insert as initial content.
	 * @return Count of the successfully inserted models or {@code 0} if database transaction failed.
	 * @see #dispatchBulkInsert(SQLiteDatabase, ContentValues[])
	 */
	protected int insertInitialModels(@NonNull SQLiteDatabase db, @NonNull List<M> models) {
		if (models.isEmpty()) {
			return 0;
		}
		final ContentValues[] contentValues = new ContentValues[models.size()];
		for (int i = 0; i < models.size(); i++) {
			contentValues[i] = models.get(i).toContentValues();
		}
		return dispatchBulkInsert(db, contentValues);
	}

	/**
	 * Builds a set of <b>INSERT</b> operations for the specified <var>models</var>.
	 *
	 * @param models The desired list of models for which (theirs content values) to build the new
	 *               insert operations.
	 * @return The requested operations type of <b>INSERT</b> for the specified models.
	 * @see #createInsertModelOperation(EntityModel)
	 */
	@NonNull
	public List<ContentProviderOperation> createInsertModelsOperations(@NonNull List<M> models) {
		if (models.isEmpty()) {
			return new ArrayList<>(0);
		}
		final List<ContentProviderOperation> operations = new ArrayList<>(models.size());
		for (final M model : models) {
			operations.add(createInsertModelOperation(model));
		}
		return operations;
	}

	/**
	 * Builds a new <b>INSERT</b> operation using {@link #createInsertOperation(ContentValues)} for
	 * the specified <var>model</var>.
	 *
	 * @param model The desired model for which (its content values) to build the new insert operation.
	 * @return New instance of {@link ContentProviderOperation} type of <b>INSERT</b>.
	 */
	@NonNull
	public ContentProviderOperation createInsertModelOperation(@NonNull M model) {
		// experiment: return values back to the pool
		return createInsertOperation(model.toContentValues());
	}

	/**
	 * Builds a set of <b>UPDATE</b> operations for the specified <var>models</var>.
	 * <p>
	 * Model's selections created via {@link EntityModel#createSelection()} will be used as
	 * <var>selection</var> argument for each of them.
	 *
	 * @param models The desired list of models for which (theirs content values) to build the new
	 *               update operations.
	 * @return The requested operations type of <b>UPDATE</b> for the specified models.
	 * @see #createUpdateModelOperation(EntityModel)
	 */
	@NonNull
	public List<ContentProviderOperation> createUpdateModelsOperations(@NonNull List<M> models) {
		if (models.isEmpty()) {
			return new ArrayList<>(0);
		}
		final List<ContentProviderOperation> operations = new ArrayList<>(models.size());
		for (final M model : models) {
			operations.add(createUpdateModelOperation(model));
		}
		return operations;
	}

	/**
	 * Builds a new <b>UPDATE</b> operation using {@link #createUpdateOperation(ContentValues, String, String[])}
	 * for the specified <var>model</var>.
	 * <p>
	 * Model's selection along with arguments created via {@link EntityModel#createSelection()} will
	 * be used as selection parameters for the requested update operation.
	 *
	 * @param model The desired model for which (its content values) to build the new update operation.
	 * @return New instance of {@link ContentProviderOperation} type of <b>UPDATE</b>.
	 */
	@NonNull
	public ContentProviderOperation createUpdateModelOperation(@NonNull M model) {
		final EntityModel.Selection modelSelection = model.createSelection();
		// experiment: return values back to the pool
		return createUpdateOperation(model.toContentValues(), modelSelection.getValue(), modelSelection.getArguments());
	}

	/**
	 * Same as {@link #createDeleteOperation(long[])} where model's selections created via
	 * {@link EntityModel#createSelection()} will be used as <var>selection</var> argument for each
	 * of them.
	 *
	 * @param models The desired list of models for which to build the new delete operation.
	 */
	@NonNull
	public List<ContentProviderOperation> createDeleteModelsOperation(@NonNull List<M> models) {
		if (models.isEmpty()) {
			return new ArrayList<>(0);
		}
		final List<ContentProviderOperation> operations = new ArrayList<>(models.size());
		for (final M model : models) {
			operations.add(createDeleteModelOperation(model));
		}
		return operations;
	}

	/**
	 * Builds a new <b>DELETE</b> operation using {@link #createDeleteOperation(String, String[])}
	 * for the specified <var>model</var>.
	 * <p>
	 * Model's selection along with arguments created via {@link EntityModel#createSelection()} will
	 * be used as selection parameters for the requested delete operation.
	 *
	 * @param model The desired model for which to build the new delete operation.
	 */
	@NonNull
	public ContentProviderOperation createDeleteModelOperation(@NonNull M model) {
		final EntityModel.Selection modelSelection = model.createSelection();
		return createDeleteOperation(modelSelection.getValue(), modelSelection.getArguments());
	}

	/**
	 * Builds a set of <b>ASSERT</b> operations for the specified <var>models</var>.
	 * <p>
	 * Model's selections created via {@link EntityModel#createSelection()} will be used as
	 * <var>selection</var> argument for each of them.
	 *
	 * @param models The desired list of models for which (theirs content values) to build the new
	 *               assert operations.
	 * @return The requested operations type of <b>ASSERT</b> for the specified models.
	 * @see #createAssertModelOperation(EntityModel)
	 */
	@NonNull
	public List<ContentProviderOperation> createAssertModelsOperations(@NonNull List<M> models) {
		if (models.isEmpty()) {
			return new ArrayList<>(0);
		}
		final List<ContentProviderOperation> operations = new ArrayList<>(models.size());
		for (final M model : models) {
			operations.add(createAssertModelOperation(model));
		}
		return operations;
	}

	/**
	 * Builds a new <b>ASSERT</b> operation using {@link #createAssertOperation(ContentValues, String, String[])}
	 * for the specified <var>model</var>.
	 * <p>
	 * Model's selection along with arguments created via {@link EntityModel#createSelection()} will
	 * be used as selection parameters for the requested assert operation.
	 *
	 * @param model The desired model for which (its content values) to build the new assert operation.
	 * @return New instance of {@link ContentProviderOperation} type of <b>ASSERT</b>.
	 */
	@NonNull
	public ContentProviderOperation createAssertModelOperation(@NonNull M model) {
		final EntityModel.Selection modelSelection = model.createSelection();
		return createAssertOperation(model.toContentValues(), modelSelection.getValue(), modelSelection.getArguments());
	}

	/**
	 * Bulk method for {@link #insertModel(EntityModel)}.
	 * <p>
	 * This implementation iterates through the given list of models and calls {@link #insertModel(EntityModel)}
	 * for each one of them.
	 * <p>
	 * <b>Also note, that this operation is wrapped into {@link Transaction#EXCLUSIVE EXCLUSIVE}
	 * database transaction which is always ended successfully.</b>
	 *
	 * @param models The desired list of models of which rows to update.
	 * @return Count of successfully inserted rows for the specified models arguments.
	 * @see #queryModels(String[], String, String[], String)
	 * @see #updateModels(List)
	 * @see #deleteModels(List)
	 */
	public int insertModels(@NonNull List<M> models) {
		if (models.isEmpty()) {
			return 0;
		}
		int count = 0;
		final Lock notificationsLock = lockContentNotifications();
		beginTransaction(Transaction.EXCLUSIVE);
		for (final M model : models) {
			if (insertModel(model) != null) count++;
		}
		endTransaction(true);
		notificationsLock.release();
		if (count > 0) {
			notifyContentChanged();
		}
		return count;
	}

	/**
	 * Same as {@link #insert(ContentValues)} where the given <var>model</var> will be transformed
	 * into ContentValues via {@link EntityModel#toContentValues()}.
	 *
	 * @param model The desired model for which to create a new row.
	 * @return Uri of the newly inserted row or {@code null} if content values created from the given
	 * model were empty.
	 * @see #queryModel(String[], String, String[], String)
	 * @see #updateModel(EntityModel)
	 * @see #deleteModel(EntityModel)
	 */
	@Nullable
	public Uri insertModel(@NonNull M model) {
		final ContentValues values = model.toContentValues();
		// experiment: return values back to the pool
		return values.size() > 0 ? insert(model.toContentValues()) : null;
	}

	/**
	 * Same as {@link #queryModels(String[], String, String[], String)} with all query parameters
	 * specified as {@code null}.
	 *
	 * @return Queried all models or empty list if there are no rows presented within database.
	 */
	@NonNull
	public EntityModelList<M> queryAllModels() {
		return queryModels(null, null, null, null);
	}

	/**
	 * Same as {@link #queryModels(String[], String, String[], String)} where the <var>selection</var>
	 * and <var>selectionArgs</var> arguments will be created via {@link IdSelection} builder for
	 * the specified <var>ids</var>.
	 *
	 * @param ids Array of primary ids of the desired models to query.
	 * @return Queried models or empty list if there are no rows presented within database with the
	 * specified primary ids.
	 * @see IdSelection#IdSelection(String)
	 * @see IdSelection#withIds(long[])
	 * @see IdSelection#build()
	 * @see IdSelection#buildArgs()
	 */
	@NonNull
	public EntityModelList<M> queryModels(@NonNull @Size(min = 1) long[] ids) {
		final IdSelection selection = new IdSelection(mPrimaryColumnName).withIds(ids);
		return queryModels(null, selection.build(), selection.buildArgs(), null);
	}

	/**
	 * Same as {@link #query(String[], String, String[], String)} where the obtained {@link Cursor}
	 * will be used to bind list of models with theirs corresponding data.
	 * <p>
	 * <b>Note</b>, that only models for which {@link EntityModel#fromCursor(Cursor)} has been successful
	 * will be included into the resulting list.
	 *
	 * @param projection    The desired projection for the query.
	 * @param selection     The desired selection for the query.
	 * @param selectionArgs The desired selection arguments for the query.
	 * @param sortOrder     The desired sort order for the query.
	 * @return Queried models or empty list if there are no rows presented within database for the
	 * specified query parameters.
	 * @see #updateModels(List)
	 * @see #deleteModels(List)
	 */
	@NonNull
	public EntityModelList<M> queryModels(@Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		final Cursor cursor = query(projection, selection, selectionArgs, sortOrder);
		final String entityName = getClass().getSimpleName();
		final EntityModelList<M> models = new EntityModelArrayList<>(cursor.getCount());
		if (cursor.moveToFirst()) {
			do {
				final M model = onCreateModel();
				if (model.fromCursor(cursor)) {
					models.add(model);
				} else {
					Log.e(entityName, "Failed to bind data from cursor to model at position(" + cursor.getPosition() + ").");
				}
			} while (cursor.moveToNext());
		}
		cursor.close();
		return models;
	}

	/**
	 * Same as {@link #queryModel(String[], String, String[], String)} where the <var>selection</var>
	 * and <var>selectionArgs</var> arguments will be created via {@link IdSelection} builder for
	 * the specified <var>id</var>.
	 *
	 * @param id Primary id of the desired model to query.
	 * @return Queried model or {@code null} if there is no row presented within database with such
	 * primary id.
	 * @see IdSelection#IdSelection(String)
	 * @see IdSelection#withIds(long[])
	 * @see IdSelection#build()
	 * @see IdSelection#buildArgs()
	 */
	@Nullable
	public M queryModel(long id) {
		final IdSelection selection = new IdSelection(mPrimaryColumnName).withIds(new long[]{id});
		return queryModel(null, selection.build(), selection.buildArgs(), null);
	}

	/**
	 * Same as {@link #query(String[], String, String[], String)} where the obtained {@link Cursor}
	 * will be used to bind a single model with its corresponding data.
	 * <p>
	 * If the obtained cursor will contain multiple rows, only the first one will be used.
	 *
	 * @param projection    The desired projection for the query.
	 * @param selection     The desired selection for the query.
	 * @param selectionArgs The desired selection arguments for the query.
	 * @param sortOrder     The desired sort order for the query.
	 * @return Queried model or {@code null} if there is no row presented within database for the
	 * specified query parameters.
	 * @see #insertModel(EntityModel)
	 * @see #updateModel(EntityModel)
	 * @see #deleteModel(EntityModel)
	 */
	@Nullable
	public M queryModel(@Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		final Cursor cursor = query(projection, selection, selectionArgs, sortOrder);
		M model = null;
		if (cursor.moveToFirst()) {
			model = onCreateModel();
			if (!model.fromCursor(cursor)) {
				model = null;
				Log.e(getClass().getSimpleName(), "Failed to bind data from cursor to model(selection: " + selection + ", args: " + Arrays.toString(selectionArgs) + ").");
			}
		}
		cursor.close();
		return model;
	}

	/**
	 * Bulk method for {@link #updateModel(EntityModel)}.
	 * <p>
	 * This implementation iterates through the given list of models and calls {@link #updateModel(EntityModel)}
	 * for each one of them.
	 * <p>
	 * <b>Also note, that this operation is wrapped into {@link Transaction#EXCLUSIVE EXCLUSIVE}
	 * database transaction which is always ended successfully.</b>
	 *
	 * @param models The desired list of models of which rows to update.
	 * @return Count of successfully updated rows for the specified models arguments.
	 * @see #insertModels(List)
	 * @see #queryModels(String[], String, String[], String)
	 * @see #deleteModels(List)
	 */
	public int updateModels(@NonNull List<M> models) {
		if (models.isEmpty()) {
			return 0;
		}
		int count = 0;
		final Lock notificationsLock = lockContentNotifications();
		beginTransaction(Transaction.EXCLUSIVE);
		for (final M model : models) {
			final int updated = updateModel(model);
			if (updated >= 0) count += updated;
		}
		endTransaction(true);
		notificationsLock.release();
		if (count > 0) {
			notifyContentChanged();
		}
		return count;
	}

	/**
	 * Same as {@link #update(ContentValues, String, String[])} where the given <var>model</var>
	 * will be transformed into ContentValues via {@link EntityModel#toContentValues()} and its
	 * selection along with arguments created via {@link EntityModel#createSelection()} and will be
	 * used as selection parameters.
	 *
	 * @param model The desired model of which row to update.
	 * @return Count of successfully updated rows for the specified model arguments.
	 * @see #insertModel(EntityModel)
	 * @see #queryModel(String[], String, String[], String)
	 * @see #deleteModel(EntityModel)
	 */
	public int updateModel(@NonNull M model) {
		final EntityModel.Selection modelSelection = model.createSelection();
		// experiment: return values back to the pool
		return update(model.toContentValues(), modelSelection.getValue(), modelSelection.getArguments());
	}

	/**
	 * Bulk method for {@link #deleteModel(EntityModel)}.
	 * <p>
	 * This implementation iterates through the given list of models and calls {@link #deleteModel(EntityModel)}
	 * for each one of them.
	 * <p>
	 * <b>Also note, that this operation is wrapped into {@link Transaction#EXCLUSIVE EXCLUSIVE}
	 * database transaction which is always ended successfully.</b>
	 *
	 * @param models The desired list of models of which rows to delete.
	 * @return Count of successfully deleted rows for the specified models arguments.
	 * @see #insertModels(List)
	 * @see #queryModels(String[], String, String[], String)
	 * @see #updateModels(List)
	 */
	public int deleteModels(@NonNull List<M> models) {
		if (models.isEmpty()) {
			return 0;
		}
		int count = 0;
		final Lock notificationsLock = lockContentNotifications();
		beginTransaction(Transaction.EXCLUSIVE);
		for (final M model : models) {
			final int updated = deleteModel(model);
			if (updated >= 0) count += updated;
		}
		endTransaction(true);
		notificationsLock.release();
		if (count > 0) {
			notifyContentChanged();
		}
		return count;
	}

	/**
	 * Same as {@link #delete(String, String[])} where selection along with arguments of the given
	 * <var>model</var> created via {@link EntityModel#createSelection()} will be used as selection
	 * parameters.
	 *
	 * @param model The desired model of which row to delete.
	 * @return Count of successfully deleted rows for the specified model arguments.
	 * @see #insertModel(EntityModel)
	 * @see #queryModel(String[], String, String[], String)
	 * @see #updateModel(EntityModel)
	 */
	public int deleteModel(@NonNull M model) {
		final EntityModel.Selection modelSelection = model.createSelection();
		return delete(modelSelection.getValue(), modelSelection.getArguments());
	}

	/**
	 * Invoked whenever this entity needs a new instance of its model. This is basically when
	 * {@link #queryModel(String[], String, String[], String)} or {@link #queryModels(String[], String, String[], String)}
	 * is called.
	 *
	 * @return New instance of the requested model.
	 * @throws DatabaseException If annotations processing is not enabled.
	 */
	@NonNull
	@SuppressWarnings("unchecked")
	protected M onCreateModel() {
		final M model = mAnnotationHandler == null ? null : ((ModelEntityAnnotationHandler<M>) mAnnotationHandler).handleCreateModel();
		if (model == null) {
			final String assistantName = getClass().getSimpleName();
			throw DatabaseException.misconfiguration(
					"Cannot create new instance of model for entity(" + assistantName + "). " +
							"Annotations processing is not enabled or @Model annotation was not found."
			);
		}
		return model;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
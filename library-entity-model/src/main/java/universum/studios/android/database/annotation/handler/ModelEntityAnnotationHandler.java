/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.annotation.Upgrade;
import universum.studios.android.database.entity.ModelEntity;
import universum.studios.android.database.model.EntityModel;

/**
 * An {@link EntityAnnotationHandler} extended interface for annotation handlers from the Database
 * library that are used to handle processing of annotations attached to classes derived from the
 * <b>Model Entity</b> class provided by this library.
 *
 * @author Martin Albedinsky
 *
 * @see ModelEntity
 */
public interface ModelEntityAnnotationHandler<M extends EntityModel> extends EntityAnnotationHandler {

	/**
	 * Returns the model class obtained from {@link Model @Model} annotation (if presented).
	 *
	 * @return Via annotation specified model class related to the model entity for which is this
	 * handler created or {@code null} if there is no annotation presented.
	 */
	@Nullable
	Class<M> getModelClass();

	/**
	 * Handles creation of a model instance specific for the model entity for which is this handler
	 * created.
	 *
	 * @return New instance of the requested model.
	 *
	 * @see EntityModelAnnotationHandler#createModel()
	 */
	@NonNull
	M handleCreateModel();

	/**
	 * Handles creation process of a table within the given SQLite <var>db</var> for the specified
	 * <var>entity</var>.
	 *
	 * @param entity The model entity for which to create a new table within the database.
	 * @param db     The database where to create new table.
	 * @return {@code True} if database table has been successfully created with all columns specified
	 * by an entity model associated with the given entity, {@code false} if some error has occurred
	 * or the database table cannot be created due to invalid columns configuration.
	 */
	boolean handleCreate(@NonNull ModelEntity entity, @NonNull SQLiteDatabase db);

	/**
	 * Handles upgrade process of an existing table within the given SQLite <var>db</var> for the
	 * specified <var>entity</var> according to the specified database versions.
	 *
	 * @param entity     The model entity for which to upgrade its existing table within the database.
	 * @param db         The database where to upgrade the table.
	 * @param oldVersion Old version of the database.
	 * @param newVersion New version of the database.
	 * @return {@code True} if database table has been successfully upgraded (if requested via {@link Upgrade @Upgrade}
	 * annotation presented for columns to upgrade of entity model associated with the given entity),
	 * {@code false} if some has occurred.
	 */
	boolean handleUpgrade(@NonNull ModelEntity entity, @NonNull SQLiteDatabase db, int oldVersion, int newVersion);
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.lang.reflect.Field;

import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.Upgrade;
import universum.studios.android.database.entity.ModelEntity;
import universum.studios.android.database.util.TableColumn;

/**
 * ModelEntityUpgrader is a helper class that is used by {@link ModelEntityAnnotationHandler} to
 * handle upgrade process of a database table for its related model entity.
 * <p>
 * This helper uses a model specific for the attached entity as table structure definition as such
 * entity model has its fields marked by {@link Column @Column} annotation so this helper can use that
 * annotations along with {@link Upgrade @Upgrade} annotation to create valid ALTER TABLE query that
 * is executed whenever {@link #handleUpgrade(SQLiteDatabase, int, int)} is called.
 *
 * @author Martin Albedinsky
 */
final class ModelEntityUpgrader extends ModelEntityTableHelper {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ModelEntityUpgrader";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Format for SQL <b>ADD COLUMN</b> statement.
	 */
	private static final String ADD_COLUMN_FORMAT = "ALTER TABLE %s ADD COLUMN %s";

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of ModelEntityUpgrader for the specified <var>entity</var>.
	 *
	 * @param entity The model entity for which should the new upgrader handle upgrade of its
	 *               database table. The table for the specified entity will be upgraded from its
	 *               related model class that will serve as a table structure definition as such
	 *               model has annotated its fields as columns to upgrade.
	 */
	ModelEntityUpgrader(ModelEntity entity) {
		super(entity);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Handles upgrade of a database table within the specified <var>db</var> for the attached
	 * entity.
	 *
	 * @param db The SQLite database where to upgrade the requested table.
	 * @param oldVersion Old version of the database used to resolve which columns to upgrade.
	 * @param newVersion New version of the database used to resolve which columns to upgrade.
	 * @return {@code True} if table has been successfully upgraded, {@code false} if some error has
	 * occurred.
	 */
	@SuppressWarnings("ConstantConditions")
	boolean handleUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		final String modelName = mEntity.getModelClass().getSimpleName();
		final String entityName = mEntity.getClass().getSimpleName();
		final String tableName = mEntity.getName();
		final EntityModelAnnotationHandler modelHandler = EntityModelAnnotationHandlers.obtainModelHandler(mEntity.getModelClass());
		final AnnotatedColumn[] annotatedColumns = modelHandler.getColumns();
		if (annotatedColumns.length == 0) {
			return true;
		}
		for (final AnnotatedColumn annotatedColumn : annotatedColumns) {
			// Do not process joined columns.
			if (annotatedColumn.isJoined()) {
				continue;
			}
			if (annotatedColumn.isDeprecated()) {
				// Deprecated column no longer participates on table's structure.
				// todo: ALTER TABLE to delete column. Unfortunately, the SQLite does not support
				// todo: altering tables in order to delete/modify theirs columns.
				continue;
			}
			if (annotatedColumn.requiresUpgrade(oldVersion, newVersion)) {
				final Field columnField = annotatedColumn.getField();
				if (COLUMN_CREATOR.canCreateFromField(columnField)) {
					final TableColumn column = COLUMN_CREATOR.createFromField(columnField);
					final String alterStatement = String.format(ADD_COLUMN_FORMAT, tableName, column.build());
					if (DatabaseConfig.LOG_ENABLED) {
						Log.v(entityName, "Altering table using SQL statement('" + alterStatement + "').");
					}
					db.execSQL(alterStatement);
				} else {
					Log.e(entityName, "Cannot upgrade table column for model's field(" + modelName + "." + columnField.getName() + ").");
				}
			}
		}
		return true;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}

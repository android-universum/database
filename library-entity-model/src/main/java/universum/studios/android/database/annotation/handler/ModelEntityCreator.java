/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.entity.ModelEntity;
import universum.studios.android.database.query.CreateQuery;
import universum.studios.android.database.util.TableColumn;

/**
 * ModelEntityCreator is a helper class that is used by {@link ModelEntityAnnotationHandler} to
 * handle creation process of a database table for its related model entity.
 * <p>
 * This helper uses a model specific for the attached entity as table structure definition as such
 * entity model has its fields marked by {@link Column @Column} annotation so this helper can use
 * those annotations to create valid CREATE TABLE query that is executed whenever
 * {@link #handleCreate(SQLiteDatabase)} is called.
 *
 * @author Martin Albedinsky
 */
final class ModelEntityCreator extends ModelEntityTableHelper {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ModelEntityCreator";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of ModelEntityCreator for the specified <var>entity</var>.
	 *
	 * @param entity The model entity for which should the new creator handle creation of its database
	 *               table. The table for the specified entity will be created from its related model
	 *               class that will serve as a table structure definition as such model has annotated
	 *               its fields as columns.
	 */
	ModelEntityCreator(ModelEntity entity) {
		super(entity);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Handles creation of a database table within the specified <var>db</var> for the attached
	 * entity.
	 *
	 * @param db The SQLite database where to create the requested table.
	 * @return {@code True} if table has been successfully created, {@code false} if some error has
	 * occurred.
	 */
	boolean handleCreate(SQLiteDatabase db) {
		final CreateQuery createQuery = buildCreateQuery();
		if (createQuery != null && createQuery.isExecutable()) {
			final String createStatement = createQuery.build();
			if (DatabaseConfig.LOG_ENABLED) {
				Log.v(mEntity.getClass().getSimpleName(), "Crating table using SQL statement('" + createStatement + "').");
			}
			db.execSQL(createStatement);
			return true;
		}
		return false;
	}

	/**
	 * Builds a new instance of CreateQuery that can be executed in order to create database table
	 * for the attached entity.
	 */
	@SuppressWarnings("ConstantConditions")
	private CreateQuery buildCreateQuery() {
		final String modelName = mEntity.getModelClass().getSimpleName();
		final String entityName = mEntity.getClass().getSimpleName();
		final EntityModelAnnotationHandler modelHandler = EntityModelAnnotationHandlers.obtainModelHandler(mEntity.getModelClass());
		final AnnotatedColumn[] annotatedColumns = modelHandler.getColumns();
		if (annotatedColumns.length == 0) {
			return null;
		}
		final CreateQuery query = new CreateQuery().table(mEntity.getName());
		final List<TableColumn> columns = new ArrayList<>(annotatedColumns.length);
		final List<TableColumn> primaryColumns = new ArrayList<>();
		final List<TableColumn> uniqueColumns = new ArrayList<>();
		for (final AnnotatedColumn annotatedColumn : annotatedColumns) {
			// Do not process joined nor deprecated columns.
			if (annotatedColumn.isJoined() || annotatedColumn.isDeprecated()) {
				continue;
			}
			final Field columnField = annotatedColumn.getField();
			if (COLUMN_CREATOR.canCreateFromField(columnField)) {
				final TableColumn column = COLUMN_CREATOR.createFromField(columnField);
				columns.add(column);
				if (column.isPrimaryKey()) {
					primaryColumns.add(column);
				}
				if (column.isUniqueKey()) {
					uniqueColumns.add(column);
				}
			} else {
				Log.e(entityName, "Cannot create table column from model's field(" + modelName + "." + columnField.getName() + ").");
			}
		}
		resolveMultiplePrimaryKeys(query, primaryColumns);
		resolveMultipleUniqueKeys(query, uniqueColumns);
		for (final TableColumn column : columns) {
			query.column(column.build());
		}
		return query;
	}

	/**
	 * If an entity table should have multiple primary keys, we will ensure here that none of the
	 * specified primary columns will have any <b>primary key</b> related constraint specified so
	 * when its CREATE COLUMN DEFINITION statement is created it will look just like for ordinary
	 * column.
	 *
	 * @param query          Create query where to specify set of multiple primary keys if needed.
	 * @param primaryColumns List with columns that should represent primary columns within a database
	 *                       table.
	 */
	private void resolveMultiplePrimaryKeys(CreateQuery query, List<TableColumn> primaryColumns) {
		if (primaryColumns.size() > 1) {
			for (final TableColumn column : primaryColumns) {
				column.removeConstraint(TableColumn.CONSTRAINT_PRIMARY_KEY);
				column.removeConstraint(TableColumn.CONSTRAINT_AUTOINCREMENT);
				query.primaryKey(column.name());
			}
		}
	}

	/**
	 * Like for {@link #resolveMultiplePrimaryKeys(CreateQuery, List)}, we will ensure here that
	 * none of the specified unique columns will have any <b>unique key</b> related constraint
	 * specified.
	 *
	 * @param query         Create query where to specify set of multiple unique keys if needed.
	 * @param uniqueColumns List with columns that should represent unique columns within a database
	 *                      table.
	 */
	private void resolveMultipleUniqueKeys(CreateQuery query, List<TableColumn> uniqueColumns) {
		if (uniqueColumns.size() > 1) {
			for (final TableColumn column : uniqueColumns) {
				column.removeConstraint(TableColumn.CONSTRAINT_UNIQUE);
				query.uniqueKey(column.name());
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import universum.studios.android.database.entity.ModelEntity;
import universum.studios.android.database.util.TableColumn;

/**
 * Base helper class of which derivatives are used by annotation handlers for entity classes to
 * handle operations related to database tables (creation, upgrade, ...).
 *
 * @author Martin Albedinsky
 * @see ModelEntityCreator
 * @see ModelEntityUpgrader
 */
abstract class ModelEntityTableHelper {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ModelEntityTableHelper";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Default column creator instance.
	 */
	static final TableColumn.ColumnCreator COLUMN_CREATOR = new TableColumn.ColumnCreator();

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Entity of which table related operation will be handled by this helper.
	 */
	final ModelEntity mEntity;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of ModelEntityTableHelper for the specified <var>entity</var>.
	 *
	 * @param entity The entity of which table related operation to handle by this helper.
	 */
	ModelEntityTableHelper(ModelEntity entity) {
		this.mEntity = entity;
	}

	/*
	 * Methods =====================================================================================
	 */

	/*
	 * Inner classes ===============================================================================
	 */
}
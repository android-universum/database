/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.Database;
import universum.studios.android.database.DatabaseEntity;
import universum.studios.android.database.model.EntityModel;

/**
 * A {@link ModelEntity} implementation that may be used to represent an entity that joins multiple
 * database tables of which structure is represented by a specific entity model.
 *
 * @author Martin Albedinsky
 * @see JoinEntity
 */
public abstract class JoinModelEntity<M extends EntityModel> extends ModelEntity<M> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "JoinModelEntity";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Class of entity of which uri will be used to set notification uri.
	 */
	private Class<? extends DatabaseEntity> mNotificationEntityClass;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of JoinModelEntity.
	 *
	 * @see ModelEntity#ModelEntity()
	 */
	public JoinModelEntity() {
		super();
	}

	/**
	 * Creates a new instance of JoinModelEntity with the specified <var>classOfModel</var> and empty
	 * name.
	 *
	 * @see ModelEntity#ModelEntity(String, Class)
	 */
	protected JoinModelEntity(@NonNull Class<M> classOfModel) {
		super("", classOfModel);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Same a {@link #setNotificationUri(android.net.Uri)} where the specified <var>classOfEntity</var>
	 * will be used to obtain instance of such entity via {@link Database#findEntityByClass(Class)}
	 * using the database to which is (or will be) this entity attached and content uri of that entity
	 * will be used as notification uri for this entity.
	 */
	protected void setNotificationEntity(@NonNull Class<? extends DatabaseEntity> classOfEntity) {
		this.mNotificationEntityClass = classOfEntity;
		if (isAttachedToDatabase()) {
			setNotificationUri(getDatabase().findEntityByClass(classOfEntity).getContentUri());
		}
	}

	/**
	 */
	@Override
	protected void onAttachedToDatabase(@NonNull Database database) {
		super.onAttachedToDatabase(database);
		if (mNotificationEntityClass != null && mNotificationUri == null) {
			this.mNotificationUri = database.findEntityByClass(mNotificationEntityClass).getContentUri();
		}
	}

	/**
	 */
	@Override
	protected void onCreate(@NonNull SQLiteDatabase db) {
		// Join entity does not create any database table by default.
	}

	/**
	 */
	@NonNull
	@Override
	protected Cursor onQuery(@NonNull SQLiteDatabase db, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		// This will call prepareQueryBuilder() where we are expecting valid JOIN statement.
		return super.onQuery(db, projection, selection, selectionArgs, sortOrder);
	}

	/**
	 * Prepares a new instance of SQLiteQueryBuilder for purpose of {@link #onQuery(SQLiteDatabase, String[], String, String[], String)}
	 * method with the <b>JOIN statement</b> created via {@link #onCreateJoinStatement()}.
	 *
	 * @return SQLiteQueryBuilder builder with this entity's join statement set as {@link SQLiteQueryBuilder#setTables(String)}.
	 */
	@NonNull
	@Override
	protected SQLiteQueryBuilder prepareQueryBuilder() {
		final SQLiteQueryBuilder builder = super.prepareQueryBuilder();
		final String joinStatement = onCreateJoinStatement();
		final JoinMatcher matcher = JoinMatcher.obtain();
		final boolean joinStatementValid = matcher.validateStatement(joinStatement);
		matcher.recycle();
		if (joinStatementValid) {
			builder.setTables(joinStatement);
			return builder;
		}
		final String entityName = getClass().getSimpleName();
		throw new IllegalArgumentException("Join statement(" + joinStatement + ") for the entity(" + entityName + ") is not valid.");
	}

	/**
	 * Invoked from {@link #prepareQueryBuilder()} to create JOIN statement for SQLiteQueryBuilder.
	 *
	 * @return SQLite JOIN statement for purpose of query method. Must be valid join statement otherwise
	 * exception will be thrown.
	 * @see JoinMatcher#validateStatement(String)
	 */
	@NonNull
	protected abstract String onCreateJoinStatement();

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.entity.ModelEntity;
import universum.studios.android.database.model.EntityModel;

/**
 * An {@link EntityAnnotationHandlers} implementation providing {@link AnnotationHandler} instances
 * for model entity classes.
 *
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public final class ModelEntityAnnotationHandlers extends EntityAnnotationHandlers {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	ModelEntityAnnotationHandlers() {
		super();
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains a {@link ModelEntityAnnotationHandler} implementation for the given <var>classOfEntity</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	public static ModelEntityAnnotationHandler obtainModelEntityHandler(@NonNull Class<? extends ModelEntity> classOfEntity) {
		return obtainHandler(ModelEntityHandler.class, classOfEntity);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link ModelEntityAnnotationHandler} implementation for {@link ModelEntity} class.
	 */
	@SuppressWarnings("WeakerAccess") static final class ModelEntityHandler<M extends EntityModel> extends EntityHandler implements ModelEntityAnnotationHandler<M> {

		/**
		 * Annotations handler of the related entity's model.
		 */
		private final EntityModelAnnotationHandler<M> mModelHandler;

		/**
		 * Creates a new instance of ModelEntityHandler for the specified <var>annotatedClass</var>
		 * with {@link ModelEntity} as <var>maxSuperClass</var>.
		 *
		 * @see EntityHandler#EntityHandler(Class, Class)
		 */
		@SuppressWarnings("unchecked")
		public ModelEntityHandler(@NonNull Class<?> annotatedClass) {
			super(annotatedClass, ModelEntity.class);
			final Model model = findAnnotation(Model.class);
			this.mModelHandler = model == null ? null : EntityModelAnnotationHandlers.obtainModelHandler((Class<M>) model.value());
		}

		/**
		 */
		@Nullable
		@Override
		@SuppressWarnings("unchecked")
		public Class<M> getModelClass() {
			final Model model = findAnnotationRecursive(Model.class);
			return model == null ? null : (Class<M>) model.value();
		}

		/**
		 */
		@NonNull
		@Override
		public M handleCreateModel() {
			if (mModelHandler == null) throw DatabaseException.missingClassAnnotation(Model.class, mAnnotatedClass);
			return mModelHandler.createModel();
		}

		/**
		 */
		@Override
		public boolean handleCreate(@NonNull ModelEntity entity, @NonNull SQLiteDatabase db) {
			return new ModelEntityCreator(entity).handleCreate(db);
		}

		/**
		 */
		@Override
		public boolean handleUpgrade(@NonNull ModelEntity entity, @NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
			return new ModelEntityUpgrader(entity).handleUpgrade(db, oldVersion, newVersion);
		}
	}
}
Database-Entity-Model
===============

This module contains implementations of basic `Entity` class that use `EntityModel` to declare **structure**
for theirs corresponding **database table** in order to support **ORM** design.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-entity-model:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-base](https://bitbucket.org/android-universum/database/src/main/library-base),
[database-query](https://bitbucket.org/android-universum/database/src/main/library-query),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-entity-core](https://bitbucket.org/android-universum/database/src/main/library-entity-core),
[database-model-core](https://bitbucket.org/android-universum/database/src/main/library-model-core),
[database-model-collection](https://bitbucket.org/android-universum/database/src/main/library-model-collection)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ModelEntity](https://bitbucket.org/android-universum/database/src/main/library-entity-model/src/main/java/universum/studios/android/database/entity/ModelEntity.java)
- [JoinModelEntity](https://bitbucket.org/android-universum/database/src/main/library-entity-model/src/main/java/universum/studios/android/database/entity/JoinModelEntity.java)
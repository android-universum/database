/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.query;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * @author Martin Albedinsky
 */
public final class BaseQueryTest extends TestCase {

	@Test public void testInstantiation() {
		// Arrange:
		final TestQuery query = new TestQuery();
		// Act + Assert:
		assertThat(query.table(), is(""));
		assertThat(query.statement(), is(""));
		assertThat(query.changed(), is(false));
		assertThat(query.isExecutable(), is(false));
	}

	@Test public void testTable() {
		// Arrange:
		final TestQuery query = new TestQuery();
		// Act + Assert:
		// - #1:
		query.table("users");
		assertThat(query.table(), is("users"));
		assertThat(query.changed(), is(true));
		query.build();
		// - #2:
		query.table("users");
		assertThat(query.changed(), is(false));
		query.build();
		// - #3:
		query.table("accounts");
		assertThat(query.changed(), is(true));
	}

	@Test public void testStatement() {
		// Arrange:
		final TestQuery query = new TestQuery();
		// Act + Assert:
		// - #1:
		query.statement("CREATE TABLE %s IF NOT EXISTS (%s)");
		assertThat(query.statement(), is("CREATE TABLE %s IF NOT EXISTS (%s)"));
		assertThat(query.changed(), is(true));
		query.build();
		// - #2:
		query.statement("CREATE TABLE %s IF NOT EXISTS (%s)");
		assertThat(query.changed(), is(false));
		query.build();
		// - #3:
		query.statement("QUERY %s FROM %s %s");
		assertThat(query.changed(), is(true));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testAreStringValuesDifferent() {
		// Act + Assert:
		assertThat(BaseQuery.areStringValuesDifferent("users", "users"), is(false));
		assertThat(BaseQuery.areStringValuesDifferent(null, null), is(false));
		assertThat(BaseQuery.areStringValuesDifferent("users", "accounts"), is(true));
		assertThat(BaseQuery.areStringValuesDifferent(null, "accounts"), is(true));
		assertThat(BaseQuery.areStringValuesDifferent("users", null), is(true));
	}

	@Test public void testClear() {
		// Arrange:
		final TestQuery query = new TestQuery();
		query.table("users").statement("CREATE TABLE %s IF NOT EXISTS (%s)");
		query.clear();
		assertThat(query.table(), is(""));
		assertThat(query.statement(), is(""));
		assertThat(query.changed(), is(true));
	}

	@Test public void testBuild() {
		// Arrange:
		final TestQuery query = new TestQuery();
		// Act + Assert:
		assertThat(query.build(), is(""));
		final String queryString = query.table("users").build();
		assertThat(query.build(), is(queryString));
		assertThat(query.table("users").build(), is(queryString));
		assertThat(query.table("accounts").build(), is(not(queryString)));
	}

	@Test public void testAppendBuilder() {
		// Arrange:
		final StringBuilder builder = new StringBuilder("QUERY STATEMENT");
		// Act:
		BaseQuery.appendBuilder(builder, " WITH ADDITIONAL PART");
		// Assert:
		assertThat(builder.toString(), is("QUERY STATEMENT WITH ADDITIONAL PART"));
	}

	@Test public void testAppendBuilderWithEmptyValue() {
		// Arrange:
		final StringBuilder builder = new StringBuilder("builder_value");
		// Act:
		BaseQuery.appendBuilder(builder, "");
		assertThat(builder.toString(), is("builder_value"));
	}

	@Test public void testAppendBuilderSafely() {
		// Arrange:
		final StringBuilder builder = new StringBuilder();
		// Act:
		BaseQuery.appendBuilderSafely(builder, "QUERY STATEMENT");
		// Assert:
		assertThat(builder.toString(), is("QUERY STATEMENT"));
	}

	@Test public void testAppendBuilderSafelyWithInitialValue() {
		// Arrange:
		final StringBuilder builder = new StringBuilder("QUERY STATEMENT");
		// Act:
		BaseQuery.appendBuilderSafely(builder, "WITH ADDITIONAL PART");
		// Assert:
		assertThat(builder.toString(), is("QUERY STATEMENT WITH ADDITIONAL PART"));
	}

	@Test public void testAppendBuilderSafelyWithEmptyValue() {
		// Arrange:
		final StringBuilder builder = new StringBuilder("builder_value");
		// Act:
		BaseQuery.appendBuilderSafely(builder, "");
		assertThat(builder.toString(), is("builder_value"));
	}

	@Test public void testAppendBuilderWithDelimiter() {
		// Arrange:
		final StringBuilder builder = new StringBuilder();
		// Act:
		BaseQuery.appendBuilderWithDelimiter(builder, "column_1", false);
		BaseQuery.appendBuilderWithDelimiter(builder, "column_2", true);
		// Assert:
		assertThat(builder.toString(), is("column_1, column_2"));
	}

	@Test public void testAppendBuilderWithDelimiterWithEmptyValue() {
		// Arrange:
		final StringBuilder builder = new StringBuilder();
		// Act:
		BaseQuery.appendBuilderWithDelimiter(builder, "", true);
		// Assert:
		assertThat(builder.toString(), is(""));
	}

	static void assertListIsEmptyCollection(final List<String> list) {
		assertThat(list, is(Collections.EMPTY_LIST));
	}

	static void assertListWithValues(final List<String> list, final String... values) {
		assertThat(list, is(notNullValue()));
		assertThat(list.size(), is(values.length));
		final String[] listAsArray = new String[values.length];
		list.toArray(listAsArray);
		assertThat(listAsArray, is(values));
	}

	static void assertOnBuildThrowsExceptionWithMessage(final BaseQuery query, final String message) {
		try {
			query.onBuild();
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage(), is(message));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	static void assertQueryBuildsRawQuery(final BaseQuery query, final String rawQuery) {
		final String rawQueryToCheck = query.onBuild();
		assertThat(rawQueryToCheck, is(notNullValue()));
		assertThat(rawQueryToCheck, is(rawQuery));
	}

	private static final class TestQuery extends BaseQuery {

		static final String STATEMENT = "TEST QUERY FOR %s";

		@Override @NonNull protected String onBuild() {
			// Valid implementation of BaseQuery should throw an exception when there are not all
			// required parameters specified.
			if (tableName == null || tableName.length() == 0) return "";
			return String.format(STATEMENT, tableName);
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.query;

import org.junit.Test;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.query.BaseQueryTest.assertListIsEmptyCollection;
import static universum.studios.android.database.query.BaseQueryTest.assertListWithValues;
import static universum.studios.android.database.query.BaseQueryTest.assertOnBuildThrowsExceptionWithMessage;
import static universum.studios.android.database.query.BaseQueryTest.assertQueryBuildsRawQuery;

/**
 * @author Martin Albedinsky
 */
public final class CreateQueryTest extends TestCase {

	@Test public void testInstantiation() {
		// Act:
		final CreateQuery query = new CreateQuery();
		// Assert:
		assertListIsEmptyCollection(query.columns());
		assertListIsEmptyCollection(query.primaryKeys());
		assertListIsEmptyCollection(query.uniqueKeys());
		assertThat(query.isExecutable(), is(false));
	}

	@Test public void testColumns() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.columns("_id", "tag", "author");
		// Assert:
		assertListWithValues(query.columns(), "_id", "tag", "author");
	}

	@Test public void testColumnsWithEmptyArray() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.columns(new String[]{});
		// Assert:
		assertListIsEmptyCollection(query.columns());
	}

	@Test public void testColumn() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.column("_id");
		query.column("author");
		// Assert:
		assertListWithValues(query.columns(), "_id", "author");
	}

	@Test public void testPrimaryKeys() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.primaryKeys("_id", "tag");
		// Assert:
		assertListWithValues(query.primaryKeys(), "_id", "tag");
	}

	@Test public void testPrimaryKeysWithEmptyArray() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.primaryKeys(new String[]{});
		// Assert:
		assertListIsEmptyCollection(query.primaryKeys());
	}

	@Test public void testPrimaryKey() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.primaryKey("_id");
		query.primaryKey("tag");
		// Assert:
		assertListWithValues(query.primaryKeys(), "_id", "tag");
	}

	@Test public void testUniqueKeys() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.uniqueKeys("tag", "author");
		// Assert:
		assertListWithValues(query.uniqueKeys(), "tag", "author");
	}

	@Test public void testUniqueKeysWithEmptyArray() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.uniqueKeys(new String[]{});
		// Assert:
		assertListIsEmptyCollection(query.uniqueKeys());
	}

	@Test public void testUniqueKey() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.uniqueKey("tag");
		query.uniqueKey("author");
		// Assert:
		assertListWithValues(query.uniqueKeys(), "tag", "author");
	}

	@Test public void testClear() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.columns("_id", "tag", "author");
		query.primaryKey("_id");
		query.uniqueKey("tag");
		query.clear();
		// Assert:
		assertListIsEmptyCollection(query.columns());
		assertListIsEmptyCollection(query.primaryKeys());
		assertListIsEmptyCollection(query.uniqueKeys());
	}

	@Test public void testIsExecutable() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.table("articles");
		assertThat(query.isExecutable(), is(false));
		query.column("author");
		assertThat(query.isExecutable(), is(true));
	}

	@Test public void testOnBuild() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.table("articles");
		query.columns(
				"_id INTEGER NOT NULL",
				"author TEXT DEFAULT '' UNIQUE"
		);
		// Assert:
		assertQueryBuildsRawQuery(query, "CREATE TABLE IF NOT EXISTS articles (" +
						"_id INTEGER NOT NULL, " +
						"author TEXT DEFAULT '' UNIQUE" +
						")"
		);
	}

	@Test public void testOnBuildWithSinglePrimaryKey() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.table("articles");
		query.columns(
				"_id INTEGER NOT NULL",
				"author TEXT DEFAULT '' UNIQUE"
		);
		query.primaryKey("_id");
		// Assert:
		assertQueryBuildsRawQuery(query, "CREATE TABLE IF NOT EXISTS articles (" +
						"_id INTEGER NOT NULL, " +
						"author TEXT DEFAULT '' UNIQUE, " +
						"PRIMARY KEY (_id)" +
						")"
		);
	}

	@Test public void testOnBuildWithMultiplePrimaryKeys() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.table("articles");
		query.columns(
				"_id INTEGER NOT NULL",
				"tag TEXT NOT NULL",
				"author TEXT DEFAULT '' UNIQUE"
		);
		query.primaryKeys("_id", "tag");
		// Assert:
		assertQueryBuildsRawQuery(query, "CREATE TABLE IF NOT EXISTS articles (" +
						"_id INTEGER NOT NULL, " +
						"tag TEXT NOT NULL, " +
						"author TEXT DEFAULT '' UNIQUE, " +
						"PRIMARY KEY (_id, tag)" +
						")"
		);
	}

	@Test public void testOnBuildWithSingleUniqueKey() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.table("articles");
		query.columns(
				"_id INTEGER NOT NULL",
				"tag TEXT NOT NULL"
		);
		query.uniqueKey("tag");
		// Assert:
		assertQueryBuildsRawQuery(query, "CREATE TABLE IF NOT EXISTS articles (" +
						"_id INTEGER NOT NULL, " +
						"tag TEXT NOT NULL, " +
						"UNIQUE (tag) ON CONFLICT REPLACE" +
						")"
		);
	}

	@Test public void testOnBuildWithMultipleUniqueKeys() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.table("articles");
		query.columns(
				"_id INTEGER NOT NULL",
				"author TEXT DEFAULT ''",
				"tag TEXT NOT NULL"
		);
		query.uniqueKeys("author", "tag");
		// Assert:
		assertQueryBuildsRawQuery(query, "CREATE TABLE IF NOT EXISTS articles (" +
						"_id INTEGER NOT NULL, " +
						"author TEXT DEFAULT '', " +
						"tag TEXT NOT NULL, " +
						"UNIQUE (author, tag) ON CONFLICT REPLACE" +
						")"
		);
	}

	@Test public void testOnBuildWithPrimaryAndUniqueKeys() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act:
		query.table("articles");
		query.columns(
				"_id INTEGER NOT NULL",
				"author TEXT DEFAULT ''",
				"tag TEXT NOT NULL"
		);
		query.primaryKey("_id");
		query.uniqueKeys("author", "tag");
		// Assert:
		assertQueryBuildsRawQuery(query, "CREATE TABLE IF NOT EXISTS articles (" +
						"_id INTEGER NOT NULL, " +
						"author TEXT DEFAULT '', " +
						"tag TEXT NOT NULL, " +
						"PRIMARY KEY (_id), " +
						"UNIQUE (author, tag) ON CONFLICT REPLACE" +
						")"
		);
	}

	@Test public void testOnBuildWithoutTableNameSpecified() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		// Act + Assert:
		assertOnBuildThrowsExceptionWithMessage(query, "No 'table' specified.");
	}

	@Test public void testOnBuildWithoutColumnsSpecified() {
		// Arrange:
		final CreateQuery query = new CreateQuery();
		query.table("table_name");
		// Act + Assert:
		assertOnBuildThrowsExceptionWithMessage(query, "No 'columns' specified.");
	}
}
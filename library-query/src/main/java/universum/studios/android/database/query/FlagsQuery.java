/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.query;

import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link BaseQuery} implementation that may be used to update (set, add, remove) a set of flags
 * for a desired column in {@link SQLiteDatabase}. The table where to update flags may be specified
 * via {@link #table()}, the desired column to be updated via {@link #column(String)} and the flags
 * via one of {@link #set(int)}, {@link #add(int)} or {@link #remove(int)} intent methods.
 *
 * @author Martin Albedinsky
 */
public class FlagsQuery extends BaseQuery<FlagsQuery> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "FlagsQuery";

	/**
	 * Constant indicating that flags specified for the query should replace those stored in the
	 * desired database column.
	 */
	private static final int SET = 0x00;

	/**
	 * Constant indicating that flags specified for the query should be added into those stored in
	 * the desired database column.
	 */
	private static final int ADD = 0x01;

	/**
	 * Constant indicating that flags specified for the query should be removed from those stored in
	 * the desired database column.
	 */
	private static final int REMOVE = 0x02;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Name of column where to update flags.
	 */
	private String mColumn;

	/**
	 * Intent type indicating what to do with the specified {@link #mFlags}.
	 */
	private int mIntent = SET;

	/**
	 * Flags specified for this query via one of {@link #set(int)}, {@link #add(int)}, {@link #remove(int)}
	 * intent methods.
	 */
	private int mFlags;

	/**
	 * Where statement specified for this query.
	 */
	private String mWhere;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Specifies a column where to update flags.
	 *
	 * @param column The desired column to be updated.
	 * @return This query to allow methods chaining.
	 * @see #set(int)
	 * @see #add(int)
	 * @see #remove(int)
	 */
	public FlagsQuery column(@NonNull String column) {
		this.mColumn = column;
		markChanged();
		return this;
	}

	/**
	 * Specifies a group of flags that should be set as value of the desired column.
	 *
	 * @param flags Group of flags to set as new value of the column.
	 * @return This query to allow methods chaining.
	 * @see #column(String)
	 * @see #add(int)
	 * @see #remove(int)
	 */
	public FlagsQuery set(int flags) {
		this.mIntent = SET;
		this.mFlags = flags;
		markChanged();
		return this;
	}

	/**
	 * Specifies a group of flags that should be added into value of the desired column.
	 *
	 * @param flags Group of flags to add into the current value of the column.
	 * @return This query to allow methods chaining.
	 * @see #column(String)
	 * @see #set(int)
	 * @see #remove(int)
	 */
	public FlagsQuery add(int flags) {
		this.mIntent = ADD;
		this.mFlags = flags;
		markChanged();
		return this;
	}

	/**
	 * Specifies a group of flags that should be removed from value of the desired column.
	 *
	 * @param flags Group of flags to remove from the current value of the column.
	 * @return This query to allow methods chaining.
	 * @see #column(String)
	 * @see #set(int)
	 * @see #add(int)
	 */
	public FlagsQuery remove(int flags) {
		this.mIntent = REMOVE;
		this.mFlags = flags;
		markChanged();
		return this;
	}

	/**
	 * Specifies a where statement that should be used to target update operation only for a specific
	 * rows.
	 *
	 * @param where The desired where statement. May be {@code null} to clear the current one.
	 * @return This query to allow methods chaining.
	 */
	public FlagsQuery where(@Nullable String where) {
		this.mWhere = where;
		markChanged();
		return this;
	}

	/**
	 */
	@Override
	public boolean isExecutable() {
		return !TextUtils.isEmpty(tableName) && !TextUtils.isEmpty(mColumn);
	}

	/**
	 */
	@NonNull
	@Override
	protected String onBuild() {
		final String updateStatement;
		switch (mIntent) {
			case ADD:
				updateStatement = mColumn + "|(" + Integer.toString(mFlags) + ")";
				break;
			case REMOVE:
				updateStatement = mColumn + "&(~" + Integer.toString(mFlags) + ")";
				break;
			default:
				updateStatement = Integer.toString(mFlags);
				break;
		}
		final String update = "UPDATE " + tableName + " SET " + mColumn + "=" + updateStatement;
		final String where = TextUtils.isEmpty(mWhere) ? "" : " WHERE " + mWhere;
		return update + where;
	}

	/**
	 */
	@Override
	public FlagsQuery clear() {
		this.mColumn = null;
		this.mIntent = SET;
		this.mFlags = 0;
		this.mWhere = null;
		return super.clear();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
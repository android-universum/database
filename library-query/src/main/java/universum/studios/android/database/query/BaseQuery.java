/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.query;

import androidx.annotation.NonNull;

/**
 * A {@link Query} implementation that represents a base structure for all query builders.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public abstract class BaseQuery<Q extends BaseQuery> implements Query {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseQuery";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Table name for which should query created.
	 */
	String tableName;

	/**
	 * Statement format used to build query.
	 */
	String statementFormat;

	/**
	 * Query that has been build when {@link #onBuild()} was last time called.
	 */
	private String query;

	/**
	 * Flag indicating whether the data for this query has changed or not. If {@code true} the cached
	 * query need to be re-build.
	 */
	private boolean changed;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Specifies a name of the database table for this query.
	 *
	 * @param tableName The desired table name.
	 * @return This query to allow methods chaining.
	 *
	 * @see #table(String)
	 * @see #statement(String)
	 */
	@SuppressWarnings("unchecked")
	public Q table(@NonNull final String tableName) {
		if (areStringValuesDifferent(this.tableName, tableName)) {
			this.tableName = tableName;
			markChanged();
		}
		return (Q) this;
	}

	/**
	 * Returns name of the table specified for this query.
	 *
	 * @return Database table name or empty string if no table has been specified yet.
	 *
	 * @see #table(String)
	 */
	@NonNull public String table() {
		return tableName == null ? "" : tableName;
	}

	/**
	 * Specifies a statement format for this query.
	 *
	 * @param statementFormat The desired format used when building this query.
	 * @return This query to allow methods chaining.
	 *
	 * @see #statement()
	 */
	@SuppressWarnings("unchecked")
	public Q statement(@NonNull final String statementFormat) {
		if (areStringValuesDifferent(this.statementFormat, statementFormat)) {
			this.statementFormat = statementFormat;
			markChanged();
		}
		return (Q) this;
	}

	/**
	 * Returns the SQLite statement specified for this query.
	 *
	 * @return The statement format used when building this query or empty string if no statement
	 * has been specified yet.
	 *
	 * @see #statement(String)
	 */
	@NonNull public String statement() {
		return statementFormat == null ? "" : statementFormat;
	}

	/**
	 * Checks whether the specified string values are different or not.
	 *
	 * @param first  The first string value to be compared.
	 * @param second The second string value to be compared.
	 * @return {@code True} if the specified string values are different, {@code false} otherwise.
	 */
	static boolean areStringValuesDifferent(final String first, final String second) {
		return !(first == null && second == null) && (first == null || !first.equalsIgnoreCase(second));
	}

	/**
	 * Returns a boolean flag indicating whether data of this query has changed or not.
	 *
	 * @return {@code True} if changed, {@code false} otherwise.
	 *
	 * @see #markChanged()
	 */
	protected final boolean changed() {
		return changed;
	}

	/**
	 * Marks this query as changed so its {@link #onBuild()} method will be invoked when {@link #build()}
	 * is next time called.
	 *
	 * @see #changed()
	 */
	protected final void markChanged() {
		this.changed = true;
	}

	/**
	 * Clears the current data of this query.
	 *
	 * @return This query to allow methods chaining.
	 */
	@SuppressWarnings("unchecked")
	public Q clear() {
		markChanged();
		this.tableName = null;
		this.statementFormat = null;
		return (Q) this;
	}

	/**
	 */
	@Override public boolean isExecutable() {
		return false;
	}

	/**
	 */
	@Override @NonNull public String build() {
		if (changed || query == null || query.length() == 0) {
			this.query = onBuild();
		}
		this.changed = false;
		return query;
	}

	/**
	 * Invoked whenever {@link #build()} is called an the data of this query builder has changed.
	 *
	 * @return SQLite query that is specific for this type of query builder.
	 */
	@NonNull protected abstract String onBuild();

	/**
	 * Appends the specified <var>builder</var> with the specified <var>value</var> with delimiter
	 * (', ') before it, if requested.
	 * <p>
	 * <b>Note</b>, that if the specified value is empty it will not be appended.
	 *
	 * @param builder   Builder where the given value append.
	 * @param value     Value to be appended.
	 * @param delimiter {@code True} to append also delimiter, {@code false otherwise}.
	 * @return {@code True} if value has been appended, {@code false otherwise}.
	 */
	protected static boolean appendBuilderWithDelimiter(@NonNull final StringBuilder builder, @NonNull final String value, final boolean delimiter) {
		if (value.length() == 0) {
			return false;
		}
		if (delimiter) builder.append(", ");
		builder.append(value);
		return true;
	}

	/**
	 * Appends the specified <var>builder</var> with the specified <var>value</var>.
	 * <p>
	 * <b>Note</b>, that if the specified value is empty it will not be appended.
	 *
	 * @param builder Builder where the given value append.
	 * @param value   Value to be appended.
	 * @return {@code True} if value has been appended, {@code false otherwise}.
	 */
	protected static boolean appendBuilder(@NonNull final StringBuilder builder, @NonNull final String value) {
		if (value.length() == 0) {
			return false;
		}
		builder.append(value);
		return true;
	}

	/**
	 * Like {@link #appendBuilder(StringBuilder, String)}, but this will also append a space before
	 * that value if the given builder has already some content presented.
	 */
	protected static boolean appendBuilderSafely(@NonNull final StringBuilder builder, @NonNull final String value) {
		if (value.length() == 0) {
			return false;
		}
		if (builder.length() > 0) builder.append(" ");
		builder.append(value);
		return true;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
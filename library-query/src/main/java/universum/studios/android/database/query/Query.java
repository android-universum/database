/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.query;

import androidx.annotation.NonNull;

/**
 * Interface for SQLite queries.
 *
 * @author Martin Albedinsky
 */
public interface Query {

	/**
	 * Checks whether this query (its SQLite statement) is executable or not.
	 *
	 * @return {@code True} if {@link #build()} will produce valid SQLite statement that can be executed,
	 * {@code false} if there are some missing parameters that has not been specified for this query
	 * yet and are required for this query.
	 */
	boolean isExecutable();

	/**
	 * Builds the SQLite statement that is specific for this type of query.
	 *
	 * @return The valid SQLite statement build from parameters specified for this query.
	 * @see #isExecutable()
	 */
	@NonNull
	String build();
}
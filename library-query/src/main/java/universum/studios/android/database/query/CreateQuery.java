/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * A {@link BaseQuery} implementation that can be used to build a valid SQLite <b>CREATE TABLE</b>
 * statement.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class CreateQuery extends BaseQuery<CreateQuery> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CreateQuery";

	/**
	 * Format for <b>CREATE TABLE ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>table name</b></li>
	 * <li>2) <b>columns</b></li>
	 * </ul>
	 */
	public static final String CREATE_TABLE_FORMAT = "CREATE TABLE %s (%s)";

	/**
	 * Format for <b>CREATE TABLE IF NOT EXISTS ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>table name</b></li>
	 * <li>2) <b>columns to create</b></li>
	 * </ul>
	 */
	public static final String CREATE_TABLE_IF_NOT_EXISTS_FORMAT = "CREATE TABLE IF NOT EXISTS %s (%s)";

	/**
	 * SQL key keyword used to specify set of primary keys for the CREATE TABLE statement.
	 */
	public static final String PRIMARY_KEY = "PRIMARY KEY";

	/**
	 * SQL key word used to specify set of unique keys for the CREATE TABLE statement.
	 */
	public static final String UNIQUE = "UNIQUE";

	/**
	 * SQL key word used to specify (for section of unique keys) that when there is a conflicting
	 * row to be inserted into database table (according to the unique keys) it should overwrite the
	 * previous one.
	 */
	public static final String ON_CONFLICT_REPLACE = "ON CONFLICT REPLACE";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Set of columns to be created.
	 * <p>
	 * Note, that these are full create column statements like: "name TEXT DEFAULT 'none'".
	 */
	private List<String> columns;

	/**
	 * Set of column names that should represent a primary keys. These will be added at the end of
	 * the CREATE TABLE statement before unique keys.
	 */
	private List<String> primaryKeys;

	/**
	 * Set of column names that should represent a unique keys. These will be added at the end of
	 * the CREATE TABLE statement after primary keys and before foreign keys.
	 */
	private List<String> uniqueKeys;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of CreateQuery with {@link #CREATE_TABLE_IF_NOT_EXISTS_FORMAT} as
	 * default create table statement format.
	 */
	public CreateQuery() {
		super();
		this.statementFormat = CREATE_TABLE_IF_NOT_EXISTS_FORMAT;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Same as {@link #column(String)} for set of columns.
	 *
	 * @param columns The desired set of columns to append.
	 * @return This query to allow methods chaining.
	 *
	 * @see #columns()
	 */
	public CreateQuery columns(@NonNull final String... columns) {
		if (columns.length > 0) {
			markChanged();
			ensureColumns(columns.length);
			this.columns.addAll(Arrays.asList(columns));
		}
		return this;
	}

	/**
	 * Appends the specified <var>column</var> into the current columns specified for this query.
	 *
	 * @param column The desired column to append.
	 * @return This query to allow methods chaining.
	 *
	 * @see #columns(String...)
	 * @see #columns()
	 */
	public CreateQuery column(@NonNull final String column) {
		markChanged();
		ensureColumns(1);
		columns.add(column);
		return this;
	}

	/**
	 * Ensures that the columns list is initialized.
	 *
	 * @param initialSize Initial size for the list.
	 */
	private void ensureColumns(final int initialSize) {
		if (columns == null) columns = new ArrayList<>(initialSize);
	}

	/**
	 * Returns the set of columns specified for this query.
	 * @return Columns used when building this query or {@link Collections#EMPTY_LIST} if no columns
	 * have been specified yet.
	 *
	 * @see #column(String)
	 * @see #columns(String...)
	 */
	@SuppressWarnings("unchecked")
	@NonNull public List<String> columns() {
		return columns == null ? Collections.EMPTY_LIST : columns;
	}

	/**
	 * Same as {@link #primaryKey(String)} for set of keys.
	 *
	 * @param primaryKeys The desired set of primary keys to append.
	 * @return This query to allow methods chaining.
	 *
	 * @see #columns(String[])
	 * @see #uniqueKeys(String[])
	 * @see #primaryKeys()
	 */
	public CreateQuery primaryKeys(@NonNull final String... primaryKeys) {
		if (primaryKeys.length > 0) {
			markChanged();
			ensurePrimaryKeys(primaryKeys.length);
			this.primaryKeys.addAll(Arrays.asList(primaryKeys));
		}
		return this;
	}

	/**
	 * Appends the specified <var>primaryKey</var> into the current primary keys specified for this
	 * query.
	 * <p>
	 * When building this query via {@link #build()} all primary keys will be added at the end of the
	 * CREATE TABLE statement before unique keys definition into PRIMARY KEY(...) section.
	 *
	 * @param primaryKey The desired primary key to append.
	 * @return This query to allow methods chaining.
	 *
	 * @see #uniqueKey(String)
	 * @see #column(String)
	 * @see #primaryKeys()
	 */
	public CreateQuery primaryKey(@NonNull final String primaryKey) {
		markChanged();
		ensurePrimaryKeys(1);
		primaryKeys.add(primaryKey);
		return this;
	}

	/**
	 * Ensures that the primary keys list is initialized.
	 *
	 * @param initialSize Initial size for the list.
	 */
	private void ensurePrimaryKeys(final int initialSize) {
		if (primaryKeys == null) primaryKeys = new ArrayList<>(initialSize);
	}

	/**
	 * Returns the set of primary keys specified for this query.
	 *
	 * @return Primary keys used when building this query or {@link Collections#EMPTY_LIST} if no
	 * primary keys have been specified yet.
	 *
	 * @see #primaryKey(String)
	 * @see #primaryKeys(String...)
	 */
	@SuppressWarnings("unchecked")
	@NonNull public List<String> primaryKeys() {
		return primaryKeys == null ? Collections.EMPTY_LIST : primaryKeys;
	}

	/**
	 * Same as {@link #uniqueKey(String)} for set of keys.
	 *
	 * @param uniqueKeys The desired set of unique keys to append.
	 * @return This query to allow methods chaining.
	 *
	 * @see #primaryKeys(String[])
	 * @see #columns(String[])
	 */
	public CreateQuery uniqueKeys(@NonNull final String... uniqueKeys) {
		if (uniqueKeys.length > 0) {
			markChanged();
			ensureUniqueKeys(uniqueKeys.length);
			this.uniqueKeys.addAll(Arrays.asList(uniqueKeys));
		}
		return this;
	}

	/**
	 * Appends the specified <var>uniqueKey</var> into the current unique keys specified for this
	 * query.
	 * <p>
	 * When building this query via {@link #build()} all unique keys will be added at the end of the
	 * CREATE TABLE statement after primary keys definition into UNIQUE(...) section.
	 *
	 * @param uniqueKey The desired unique key to append.
	 * @return This query to allow methods chaining.
	 *
	 * @see #primaryKey(String)
	 * @see #column(String)
	 */
	public CreateQuery uniqueKey(@NonNull final String uniqueKey) {
		markChanged();
		ensureUniqueKeys(1);
		uniqueKeys.add(uniqueKey);
		return this;
	}

	/**
	 * Ensures that the unique keys list is initialized.
	 *
	 * @param initialSize Initial size for the list.
	 */
	private void ensureUniqueKeys(final int initialSize) {
		if (uniqueKeys == null) uniqueKeys = new ArrayList<>(initialSize);
	}

	/**
	 * Returns the set of unique keys specified for this query.
	 *
	 * @return Unique keys used when building this query or {@link Collections#EMPTY_LIST} if no
	 * unique keys have been specified yet.
	 */
	@SuppressWarnings("unchecked")
	@NonNull public List<String> uniqueKeys() {
		return uniqueKeys == null ? Collections.EMPTY_LIST : uniqueKeys;
	}

	/**
	 * <b>Note</b>, that this will also clear the current statement to the default {@link #CREATE_TABLE_IF_NOT_EXISTS_FORMAT}
	 * format.
	 */
	@Override public CreateQuery clear() {
		super.clear();
		this.columns = null;
		this.uniqueKeys = null;
		this.primaryKeys = null;
		this.statementFormat = CREATE_TABLE_IF_NOT_EXISTS_FORMAT;
		return this;
	}

	/**
	 */
	@Override public boolean isExecutable() {
		return ensureCanBuildOrThrow(false);
	}

	/**
	 */
	@Override @NonNull protected String onBuild() {
		this.ensureCanBuildOrThrow(true);
		final StringBuilder statementBuilder = new StringBuilder(columns.size() * 15);
		this.appendColumns(statementBuilder);
		this.appendPrimaryKeys(statementBuilder);
		this.appendUniqueKeys(statementBuilder);
		return String.format(statementFormat, tableName, statementBuilder.toString());
	}

	/**
	 * Checks whether a valid SQL create query can be build from the current data. If not a
	 * DatabaseException is thrown if <var>throwException</var> was requested with {@code true}.
	 *
	 * @param throwException {@code True} to throw exception, {@code false} otherwise.
	 * @return {@code True} if we can build valid SQL query, {@code false} otherwise or exception
	 * is thrown.
	 */
	private boolean ensureCanBuildOrThrow(final boolean throwException) {
		if (tableName == null || tableName.length() == 0) {
			if (throwException) throw new IllegalArgumentException("No 'table' specified.");
			return false;
		}
		if (columns == null) {
			if (throwException) throw new IllegalArgumentException("No 'columns' specified.");
			return false;
		}
		return true;
	}

	/**
	 * Appends the specified <var>statementBuilder</var> with set of columns specified for this query.
	 *
	 * @param statementBuilder The query statement builder to be appended.
	 */
	private void appendColumns(final StringBuilder statementBuilder) {
		final int columnsCount = columns.size();
		for (int i = 0; i < columnsCount; i++) {
			appendBuilderWithDelimiter(statementBuilder, columns.get(i), i > 0);
		}
	}

	/**
	 * Appends the specified <var>statementBuilder</var> with set of primary keys specified for this
	 * query (if any).
	 *
	 * @param statementBuilder The query statement builder to be appended.
	 */
	private void appendPrimaryKeys(final StringBuilder statementBuilder) {
		appendKeys(statementBuilder, primaryKeys, PRIMARY_KEY);
	}

	/**
	 * Appends the specified <var>statementBuilder</var> with set of unique keys specified for this
	 * query (if any).
	 *
	 * @param statementBuilder The query statement builder to be appended.
	 */
	private void appendUniqueKeys(final StringBuilder statementBuilder) {
		if (appendKeys(statementBuilder, uniqueKeys, UNIQUE)) {
			appendBuilderSafely(statementBuilder, ON_CONFLICT_REPLACE);
		}
	}

	/**
	 * Appends the specified <var>statementBuilder</var> with the given list of <var>keys</var>.
	 *
	 * @param statementBuilder The query statement builder to be appended.
	 * @param keys             List of keys to be appended to the given builder.
	 * @param keysIdentifier   Identifier that will be added before keys definition section.
	 * @return {@code True} if there was appended at least one key, {@code false} otherwise.
	 */
	private static boolean appendKeys(final StringBuilder statementBuilder, final List<String> keys, final String keysIdentifier) {
		if (keys == null || keys.isEmpty()) {
			return false;
		}
		statementBuilder.append(", ");
		statementBuilder.append(keysIdentifier);
		statementBuilder.append(" (");
		for (int i = 0; i < keys.size(); i++) {
			appendBuilderWithDelimiter(statementBuilder, keys.get(i), i > 0);
		}
		statementBuilder.append(")");
		return true;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
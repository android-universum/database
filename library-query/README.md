Database-Query
===============

This module contains **query** builders that may be used directly with `SQLite` database.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-query:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [CreateQuery](https://bitbucket.org/android-universum/database/src/main/library-query/src/main/java/universum/studios/android/database/query/CreateQuery.java)
- [FlagsQuery](https://bitbucket.org/android-universum/database/src/main/library-query/src/main/java/universum/studios/android/database/query/FlagsQuery.java)
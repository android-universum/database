Database-Cursor
===============

This module contains elements and utils that may be used to simplify usage of `Cursors` in an
**Android** application.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-cursor:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseCursorWrapper](https://bitbucket.org/android-universum/database/src/main/library-cursor/src/main/java/universum/studios/android/database/cursor/BaseCursorWrapper.java)
- [CursorUtils](https://bitbucket.org/android-universum/database/src/main/library-cursor/src/main/java/universum/studios/android/database/cursor/CursorUtils.java)
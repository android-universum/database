/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.cursor;

import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import org.junit.Test;
import org.robolectric.annotation.Config;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author Martin Albedinsky
 */
public final class EmptyCursorTest extends AndroidTestCase {

	@Test public void testCreate() {
		// Act + Assert:
		assertThat(EmptyCursor.create(), is(notNullValue()));
		assertThat(EmptyCursor.create(), is(not(EmptyCursor.create())));
	}

	@Test public void testCreateIfNull() {
		// Arrange:
		final Cursor cursor = new MatrixCursor(new String[0], 0);
		// Act + Assert:
		assertThat(EmptyCursor.createIfNull(null), is(notNullValue()));
		assertThat(EmptyCursor.createIfNull(cursor), is(cursor));
	}

	@Test public void testInstance() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.getCount(), is(0));
		assertThat(cursor.getColumnCount(), is(0));
		assertThat(cursor.isClosed(), is(false));
	}

	@Config(sdk = Build.VERSION_CODES.KITKAT)
	@Test public void testNotificationUri() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.getNotificationUri(), is(nullValue()));
		final Uri notificationUri = Uri.parse("empty");
		cursor.setNotificationUri(null, notificationUri);
		assertThat(cursor.getNotificationUri(), is(notificationUri));
	}

	@Test public void testObservers() {
		// Only ensure that registering/un-registering observers does not cause any troubles.
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		final ContentObserver mockContentObserver = mock(ContentObserver.class);
		// Act + Assert:
		cursor.registerContentObserver(mockContentObserver);
		cursor.unregisterContentObserver(mockContentObserver);
		verifyZeroInteractions(mockContentObserver);
		final DataSetObserver mockDataSetObserver = mock(DataSetObserver.class);
		cursor.registerDataSetObserver(mockDataSetObserver);
		cursor.unregisterDataSetObserver(mockDataSetObserver);
		verifyZeroInteractions(mockDataSetObserver);
	}

	@Test public void testMove() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(cursor.move(i), is(false));
			assertThat(cursor.getPosition(), is(EmptyCursor.NO_POSITION));
		}
		for (int i = 0; i < 10; i++) {
			assertThat(cursor.moveToPosition(i), is(false));
			assertThat(cursor.getPosition(), is(EmptyCursor.NO_POSITION));
		}
		assertThat(cursor.moveToFirst(), is(false));
		assertThat(cursor.getPosition(), is(EmptyCursor.NO_POSITION));
		assertThat(cursor.moveToLast(), is(false));
		assertThat(cursor.getPosition(), is(EmptyCursor.NO_POSITION));
		assertThat(cursor.moveToNext(), is(false));
		assertThat(cursor.getPosition(), is(EmptyCursor.NO_POSITION));
		assertThat(cursor.moveToPrevious(), is(false));
		assertThat(cursor.getPosition(), is(EmptyCursor.NO_POSITION));
	}

	@Test public void testIsAtPosition() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.isBeforeFirst(), is(true));
		assertThat(cursor.isFirst(), is(false));
		assertThat(cursor.isLast(), is(false));
		assertThat(cursor.isAfterLast(), is(false));
	}

	@Test public void testGetPosition() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.getPosition(), is(EmptyCursor.NO_POSITION));
	}

	@Test public void testGetColumnInfo() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.getColumnCount(), is(0));
		assertThat(cursor.getColumnIndex("column"), is(EmptyCursor.NO_COLUMN_INDEX));
		assertThat(cursor.getColumnIndexOrThrow("Column"), is(EmptyCursor.NO_COLUMN_INDEX));
		assertThat(cursor.getColumnNames(), is(new String[0]));
		for (int i = 0; i < 10; i++) {
			assertThat(cursor.getColumnName(i), is(EmptyCursor.NO_COLUMN_NAME));
		}
	}

	@Test public void testGetType() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(cursor.getType(i), is(EmptyCursor.FIELD_TYPE_NULL));
			assertThat(cursor.isNull(i), is(true));
		}
	}

	@Test public void testGetColumnValue() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		for (int i = 0; i < 10; i++) {
			assertThat(cursor.getBlob(i), is(new byte[0]));
			assertThat(cursor.getString(i), is(""));
			assertThat(cursor.getShort(i), is((short) 0));
			assertThat(cursor.getInt(i), is(0));
			assertThat(cursor.getLong(i), is(0L));
			assertThat(cursor.getFloat(i), is(0f));
			assertThat(cursor.getDouble(i), is(0D));
		}
	}

	@Test public void testCopyStringToBuffer() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		final CharArrayBuffer buffer = new CharArrayBuffer(0);
		for (int i = 0; i < 10; i++) {
			cursor.copyStringToBuffer(i, buffer);
		}
		assertThat(buffer.sizeCopied, is(0));
	}

	@Test public void testRequery() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.requery(), is(false));
	}

	@Test public void testGetWantsAllOnMoveCalls() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.getWantsAllOnMoveCalls(), is(false));
	}

	@Test public void testGetDefaultExtras() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.getExtras(), is(nullValue()));
	}

	@Config(sdk = Build.VERSION_CODES.M)
	@Test public void testSetExtras() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.getExtras(), is(nullValue()));
		cursor.setExtras(new Bundle());
		assertThat(cursor.getExtras(), is(nullValue()));
	}

	@Test public void testRespond() {
		// Arrange:
		final Bundle bundle = new Bundle();
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		assertThat(cursor.respond(bundle), is(bundle));
	}

	@Test public void testDeactivate() {
		// Only ensure that deactivation does not cause any troubles.
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act:
		cursor.deactivate();
	}

	@Test public void testClose() {
		// Arrange:
		final Cursor cursor = EmptyCursor.create();
		// Act + Assert:
		cursor.close();
		assertThat(cursor.isClosed(), is(true));
		cursor.close();
		assertThat(cursor.isClosed(), is(true));
	}
}
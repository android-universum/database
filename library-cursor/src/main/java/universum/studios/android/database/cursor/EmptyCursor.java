/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.cursor;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;

import java.util.concurrent.atomic.AtomicBoolean;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link MatrixCursor} implementation which may be used to create instance of {@link Cursor}
 * without any data.
 *
 * @author Martin Albedinsky
 * @since 2.3.0
 */
public final class EmptyCursor implements Cursor {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "EmptyCursor";

	/**
	 * Constant that identifies invalid/unspecified position.
	 */
	public static final int NO_POSITION = -1;

	/**
	 * Constant that identifies invalid/unspecified column index.
	 */
	public static final int NO_COLUMN_INDEX = -1;

	/**
	 * Constant that identifies invalid/unspecified column name.
	 */
	public static final String NO_COLUMN_NAME = "";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Notification uri set for this cursor.
	 */
	private Uri notificationUri;

	/**
	 * Flag indicating whether this mock cursor is closed or not.
	 */
	private final AtomicBoolean closed = new AtomicBoolean(false);

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of EmptyCursor.
	 */
	private EmptyCursor() {
		// Not allowed to be instantiated publicly via constructor.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of EmptyCursor if the given <var>cursor</var> is {@code null}.
	 *
	 * @param cursor The cursor to be replaced by the empty cursor if it is {@code null}.
	 * @return The given cursor if it is not {@code null}, new instance of EmptyCursor otherwise.
	 */
	@NonNull public static Cursor createIfNull(@Nullable final Cursor cursor) {
		return cursor == null ? create() : cursor;
	}

	/**
	 * Creates a new instance of EmptyCursor without any column names nor row data.
	 *
	 * @return Instance of EmptyCursor ready to be used.
	 */
	@NonNull public static Cursor create() {
		return new EmptyCursor();
	}

	/**
	 */
	@Override public int getCount() {
		return 0;
	}

	/**
	 */
	@Override public void setNotificationUri(@NonNull final ContentResolver contentResolver, @Nullable final Uri uri) {
		this.notificationUri = uri;
	}

	/**
	 */
	@Override @Nullable public Uri getNotificationUri() {
		return notificationUri;
	}

	/**
	 */
	@Override public void registerContentObserver(@NonNull ContentObserver contentObserver) {}

	/**
	 */
	@Override public void unregisterContentObserver(@NonNull ContentObserver contentObserver) {}

	/**
	 */
	@Override public void registerDataSetObserver(@NonNull DataSetObserver dataSetObserver) {}

	/**
	 */
	@Override public void unregisterDataSetObserver(@NonNull DataSetObserver dataSetObserver) {}

	/**
	 */
	@Override public boolean move(final int offset) {
		return false;
	}

	/**
	 */
	@Override public boolean moveToPosition(final int position) {
		return false;
	}

	/**
	 */
	@Override public boolean moveToFirst() {
		return false;
	}

	/**
	 */
	@Override public boolean moveToLast() {
		return false;
	}

	/**
	 */
	@Override public boolean moveToNext() {
		return false;
	}

	/**
	 */
	@Override public boolean moveToPrevious() {
		return false;
	}

	/**
	 */
	@Override public boolean isFirst() {
		return false;
	}

	/**
	 */
	@Override public boolean isLast() {
		return false;
	}

	/**
	 */
	@Override public boolean isBeforeFirst() {
		return true;
	}

	/**
	 */
	@Override public boolean isAfterLast() {
		return false;
	}

	/**
	 */
	@Override public int getPosition() {
		return NO_POSITION;
	}

	/**
	 */
	@Override public int getColumnCount() {
		return 0;
	}

	/**
	 */
	@Override public int getColumnIndex(@NonNull final String columnName) {
		return NO_COLUMN_INDEX;
	}

	/**
	 */
	@Override public int getColumnIndexOrThrow(@NonNull final String columnName) throws IllegalArgumentException {
		return NO_COLUMN_INDEX;
	}

	/**
	 */
	@Override @NonNull public String[] getColumnNames() {
		return new String[0];
	}

	/**
	 */
	@Override @NonNull public String getColumnName(final int columnIndex) {
		return NO_COLUMN_NAME;
	}

	/**
	 */
	@Override public boolean isNull(final int columnIndex) {
		return getType(columnIndex) == FIELD_TYPE_NULL;
	}

	/**
	 */
	@Override public int getType(final int columnIndex) {
		return FIELD_TYPE_NULL;
	}

	/**
	 */
	@Override public byte[] getBlob(final int columnIndex) {
		return new byte[0];
	}

	/**
	 */
	@Override public String getString(final int columnIndex) {
		return "";
	}

	/**
	 */
	@Override public void copyStringToBuffer(int columnIndex, @NonNull CharArrayBuffer buffer) {}

	/**
	 */
	@Override public short getShort(final int columnIndex) {
		return 0;
	}

	/**
	 */
	@Override public int getInt(final int columnIndex) {
		return 0;
	}

	/**
	 */
	@Override public long getLong(final int columnIndex) {
		return 0;
	}

	/**
	 */
	@Override public float getFloat(final int columnIndex) {
		return 0;
	}

	/**
	 */
	@Override public double getDouble(final int columnIndex) {
		return 0;
	}

	/**
	 */
	@Override public boolean requery() {
		return false;
	}

	/**
	 */
	@Override public boolean getWantsAllOnMoveCalls() {
		return false;
	}

	/**
	 */
	@Override public void setExtras(@Nullable Bundle bundle) {}

	/**
	 */
	@Override @Nullable public Bundle getExtras() {
		return null;
	}

	/**
	 */
	@Override public Bundle respond(@NonNull final Bundle bundle) {
		return bundle;
	}

	/**
	 */
	@Override public void deactivate() {}

	/**
	 */
	@Override public void close() {
		this.closed.set(true);
	}

	/**
	 */
	@Override public boolean isClosed() {
		return closed.get();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
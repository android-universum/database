/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.cursor;

import android.database.Cursor;

import androidx.annotation.NonNull;

/**
 * Interface for cursors that can provide theirs data via an item model.
 *
 * @param <I> Type of the item model through which can this item cursor provide its data.
 * @author Martin Albedinsky
 */
public interface ItemCursor<I> extends Cursor {

	/**
	 * Returns a single item containing data for the current position of this cursor.
	 *
	 * @return Item with data corresponding to the cursor's current position.
	 * @throws IllegalStateException If this cursor is not moved to any position so the item with
	 *                               corresponding data cannot be obtained.
	 */
	@NonNull
	I getItem();
}
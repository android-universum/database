/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.cursor;

import android.database.Cursor;
import android.database.CursorWrapper;

import androidx.annotation.NonNull;

/**
 * A {@link CursorWrapper} implementation that extends API provided by such wrapper so it is even
 * more easy to implement cursor wrapper for a desired {@link Cursor} without implementing a bunch
 * of {@code move...(...)} and {@code get...(getColumnIndex(COLUMN_NAME))} methods.
 *
 * <h3>Usage</h3>
 * Below sample demonstrates usage of BaseCursorWrapper:
 * <pre>
 * public class UsersCursor extends BaseCursorWrapper {
 *
 *      private static final String COLUMN_NICKNAME         = "nickname";
 *      private static final String COLUMN_EMAIL            = "email";
 *      private static final String COLUMN_BIRTH_DATE       = "birth_date";
 *
 *      // ... other column names, should be better defined within a specific entity class
 *
 *      private String mNickname;
 *      private String mEmail;
 *      private Date mBirthDate;
 *
 *      // ... other members
 *
 *      public UsersCursor(&#64;NonNull Cursor cursor) {
 *          super(cursor);
 *      }
 *
 *      public String getNickname() {
 *          return mNickname;
 *      }
 *
 *      public String getEmail() {
 *          return mEmail;
 *      }
 *
 *      public Date getBirthDate() {
 *          return mBirthDate;
 *      }
 *
 *      &#64;Override
 *      protected boolean onBindData() {
 *          // Obtaining of data for the current position. Note that this is save whenever the wrapped
 *          // cursor contains the requested columns or not due to valid column index check for all
 *          // get[TYPE](...) methods.
 *
 *          this.mNickname = getString(COLUMN_NICKNAME);
 *          this.mEmail = getString(COLUMN_EMAIL);
 *          this.mBirthDate = new Date(getLong(COLUMN_BIRTH_DATE));
 *
 *          // ... obtain other values from the wrapped cursor
 *
 *          return true; // Return true if all data has been successfully obtained, false otherwise.
 *      }
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 */
public abstract class BaseCursorWrapper extends CursorWrapper {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseCursorWrapper";

	/**
	 * Constant used to identify no position.
	 */
	protected static final int NO_POSITION = -1;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Wrapped cursor instance.
	 */
	protected final Cursor mCursor;

	/**
	 * Id obtained for the current cursor position.
	 */
	private long mId = -1;

	/**
	 * Temporary saved current position before move operation.
	 */
	private int mSavedPosition = NO_POSITION;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseCursorWrapper to wrap the given <var>cursor</var>.
	 *
	 * @param cursor Cursor to wrap.
	 * @throws NullPointerException If the given cursor is not valid.
	 */
	public BaseCursorWrapper(@NonNull Cursor cursor) {
		super(cursor);
		this.mCursor = cursor;
		// If the wrapped cursor has been already moved to some position we need to bind data for
		// that position.
		final int position = mCursor.getPosition();
		if (position >= 0 && position < mCursor.getCount()) bindData();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	public boolean moveToFirst() {
		this.savePosition();
		boolean result = super.moveToFirst();
		if (result && hasPositionChanged()) {
			result = bindData();
		} else if (!result) {
			this.clearData();
		}
		this.clearSavedPosition();
		return result;
	}

	/**
	 */
	@Override
	public boolean moveToLast() {
		this.savePosition();
		boolean result = super.moveToLast();
		if (result && hasPositionChanged()) {
			result = bindData();
		} else if (!result) {
			this.clearData();
		}
		this.clearSavedPosition();
		return result;
	}

	/**
	 */
	@Override
	public boolean moveToNext() {
		this.savePosition();
		boolean result = super.moveToNext();
		if (result && hasPositionChanged()) {
			result = bindData();
		} else if (!result) {
			this.clearData();
		}
		this.clearSavedPosition();
		return result;
	}

	/**
	 */
	@Override
	public boolean moveToPrevious() {
		this.savePosition();
		boolean result = super.moveToPrevious();
		if (result && hasPositionChanged()) {
			result = bindData();
		} else if (!result) {
			this.clearData();
		}
		this.clearSavedPosition();
		return result;
	}

	/**
	 */
	@Override
	public boolean moveToPosition(int position) {
		this.savePosition();
		boolean result = super.moveToPosition(position);
		if (result && hasPositionChanged()) {
			result = bindData();
		} else if (!result) {
			this.clearData();
		}
		this.clearSavedPosition();
		return result;
	}

	/**
	 */
	@Override
	public boolean move(int offset) {
		this.savePosition();
		boolean result = super.move(offset);
		if (result && hasPositionChanged()) {
			result = bindData();
		} else if (!result) {
			this.clearData();
		}
		this.clearSavedPosition();
		return result;
	}

	/**
	 * Saves the current position.
	 */
	private void savePosition() {
		this.mSavedPosition = getPosition();
	}

	/**
	 * Checks whether the current position has changed.
	 *
	 * @return {@code True} if {@link #getPosition()} differs from the current saved position.
	 * @see #savePosition()
	 * @see #clearSavedPosition()
	 */
	private boolean hasPositionChanged() {
		return mSavedPosition != getPosition();
	}

	/**
	 * Clears the current saved position.
	 */
	private void clearSavedPosition() {
		this.mSavedPosition = NO_POSITION;
	}

	/**
	 * Returns id of the row for the current cursor position.
	 *
	 * @return The id obtained from {@link #getLong(String)} for {@code _id} column name or {@code -1}
	 * by default.
	 */
	public long getId() {
		return mId;
	}

	/**
	 * Checks whether a column with the specified <var>columnName</var> is included in the projection
	 * of the wrapped cursor.
	 *
	 * @param columnName Name of the desired column to check if it is projected.
	 * @return {@code True} if column is in projection and the column's type related {@code get[TYPE](columnName)}
	 * method will return value of that column for the current position, {@code false} otherwise.
	 */
	public boolean isColumnProjected(@NonNull String columnName) {
		return getColumnIndex(columnName) >= 0;
	}

	/**
	 * Delegates to {@link CursorUtils#obtainShort(Cursor, String)}.
	 */
	@NonNull
	public Short getShort(@NonNull String columnName) {
		return CursorUtils.obtainShort(mCursor, columnName);
	}

	/**
	 * Delegates to {@link CursorUtils#obtainInt(Cursor, String)}.
	 */
	public int getInt(@NonNull String columnName) {
		return CursorUtils.obtainInt(mCursor, columnName);
	}

	/**
	 * Delegates to {@link CursorUtils#obtainLong(Cursor, String)}.
	 */
	public long getLong(@NonNull String columnName) {
		return CursorUtils.obtainLong(mCursor, columnName);
	}

	/**
	 * Delegates to {@link CursorUtils#obtainFloat(Cursor, String)}.
	 */
	public float getFloat(@NonNull String columnName) {
		return CursorUtils.obtainFloat(mCursor, columnName);
	}

	/**
	 * Delegates to {@link CursorUtils#obtainDouble(Cursor, String)}.
	 */
	public double getDouble(@NonNull String columnName) {
		return CursorUtils.obtainDouble(mCursor, columnName);
	}

	/**
	 * Delegates to {@link CursorUtils#obtainString(Cursor, String)}.
	 */
	@NonNull
	public String getString(@NonNull String columnName) {
		return CursorUtils.obtainString(mCursor, columnName);
	}

	/**
	 * Delegates to {@link CursorUtils#obtainBoolean(Cursor, String)}.
	 */
	public boolean getBoolean(@NonNull String columnName) {
		return CursorUtils.obtainBoolean(mCursor, columnName);
	}

	/**
	 * Delegates to {@link CursorUtils#obtainIntBoolean(Cursor, String)}.
	 */
	public boolean getIntBoolean(@NonNull String columnName) {
		return CursorUtils.obtainIntBoolean(mCursor, columnName);
	}

	/**
	 * Performs binding of this wrapper's data.
	 *
	 * @return Result from {@link #onBindData()}.
	 * @see #clearData()
	 */
	private boolean bindData() {
		this.mId = getLong("_id");
		return onBindData();
	}

	/**
	 * Invoked to obtain data from the wrapped cursor for the current position.
	 * <p>
	 * This will be invoked whenever one of {@code move...()} methods is called upon this wrapper
	 * and the wrapped cursor can be moved to the resulting position.
	 *
	 * @return {@code True} if binding was successful and data for the current position are prepared
	 * to be "served", {@code false} otherwise.
	 * @see #onClearData()
	 */
	protected abstract boolean onBindData();

	/**
	 * Performs reset of this wrapper's current data.
	 *
	 * @see #bindData()
	 */
	private void clearData() {
		this.mId = -1;
		this.onClearData();
	}

	/**
	 * Invoked to clear the current data bound for the current position.
	 * <p>
	 * This will be invoked whenever one of {@code move...()} methods is called upon this wrapper
	 * but the wrapped cursor cannot be moved to the resulting position.
	 *
	 * @see #onBindData()
	 */
	protected abstract void onClearData();

	/*
	 * Inner classes ===============================================================================
	 */
}
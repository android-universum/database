/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.cursor;

import android.database.Cursor;

import androidx.annotation.NonNull;

/**
 * Utility class that may be used for safe obtaining of values provided by a specific cursor via
 * column names associated with the desired values.
 *
 * @author Martin Albedinsky
 */
public final class CursorUtils {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant used to determine invalid column index obtained via {@link Cursor#getColumnIndex(String)}.
	 */
	public static final int INVALID_COLUMN_INDEX = -1;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private CursorUtils() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Delegates to {@link Cursor#getShort(int)} with a column index obtained from
	 * {@link Cursor#getColumnIndex(String)} for the specified <var>columnName</var>.
	 *
	 * @param cursor     The cursor from which to obtain the desired value.
	 * @param columnName Name of the column for which to obtain the desired value.
	 * @return The desired value or {@code 0} if the column index for the specified <var>columnName</var>
	 * is {@link #INVALID_COLUMN_INDEX}.
	 */
	@NonNull
	public static Short obtainShort(@NonNull final Cursor cursor, @NonNull final String columnName) {
		final int index = cursor.getColumnIndex(columnName);
		return index == INVALID_COLUMN_INDEX ? 0 : cursor.getShort(index);
	}

	/**
	 * Delegates to {@link Cursor#getInt(int)} with a column index obtained from
	 * {@link Cursor#getColumnIndex(String)} for the specified <var>columnName</var>.
	 *
	 * @param cursor     The cursor from which to obtain the desired value.
	 * @param columnName Name of the column for which to obtain the desired value.
	 * @return The desired value or {@code 0} if the column index for the specified <var>columnName</var>
	 * is {@link #INVALID_COLUMN_INDEX}.
	 */
	public static int obtainInt(@NonNull final Cursor cursor, @NonNull final String columnName) {
		final int index = cursor.getColumnIndex(columnName);
		return index == INVALID_COLUMN_INDEX ? 0 : cursor.getInt(index);
	}

	/**
	 * Delegates to {@link Cursor#getLong(int)} with a column index obtained from
	 * {@link Cursor#getColumnIndex(String)} for the specified <var>columnName</var>.
	 *
	 * @param cursor     The cursor from which to obtain the desired value.
	 * @param columnName Name of the column for which to obtain the desired value.
	 * @return The desired value or {@code 0} if the column index for the specified <var>columnName</var>
	 * is {@link #INVALID_COLUMN_INDEX}.
	 */
	public static long obtainLong(@NonNull final Cursor cursor, @NonNull final String columnName) {
		final int index = cursor.getColumnIndex(columnName);
		return index == INVALID_COLUMN_INDEX ? 0 : cursor.getLong(index);
	}

	/**
	 * Delegates to {@link Cursor#getFloat(int)} with a column index obtained from
	 * {@link Cursor#getColumnIndex(String)} for the specified <var>columnName</var>.
	 *
	 * @param cursor     The cursor from which to obtain the desired value.
	 * @param columnName Name of the column for which to obtain the desired value.
	 * @return The desired value or {@code 0} if the column index for the specified <var>columnName</var>
	 * is {@link #INVALID_COLUMN_INDEX}.
	 */
	public static float obtainFloat(@NonNull final Cursor cursor, @NonNull final String columnName) {
		final int index = cursor.getColumnIndex(columnName);
		return index == INVALID_COLUMN_INDEX ? 0 : cursor.getFloat(index);
	}

	/**
	 * Delegates to {@link Cursor#getDouble(int)} with a column index obtained from
	 * {@link Cursor#getColumnIndex(String)} for the specified <var>columnName</var>.
	 *
	 * @param cursor     The cursor from which to obtain the desired value.
	 * @param columnName Name of the column for which to obtain the desired value.
	 * @return The desired value or {@code 0} if the column index for the specified <var>columnName</var>
	 * is {@link #INVALID_COLUMN_INDEX}.
	 */
	public static double obtainDouble(@NonNull final Cursor cursor, @NonNull final String columnName) {
		final int index = cursor.getColumnIndex(columnName);
		return index == INVALID_COLUMN_INDEX ? 0 : cursor.getDouble(index);
	}

	/**
	 * Delegates to {@link Cursor#getString(int)} with a column index obtained from
	 * {@link Cursor#getColumnIndex(String)} for the specified <var>columnName</var>.
	 *
	 * @param cursor     The cursor from which to obtain the desired value.
	 * @param columnName Name of the column for which to obtain the desired value.
	 * @return The desired value or an {@code empty string} if the column index for the specified
	 * <var>columnName</var> is {@link #INVALID_COLUMN_INDEX}.
	 */
	@NonNull
	public static String obtainString(@NonNull final Cursor cursor, @NonNull final String columnName) {
		final int index = cursor.getColumnIndex(columnName);
		return index == INVALID_COLUMN_INDEX ? "" : cursor.getString(index);
	}

	/**
	 * Returns value obtained from {@link #obtainString(Cursor, String)} parsed as <b>boolean</b>.
	 *
	 * @return {@code True} if the obtained string has {@code value == 'true'}, {@code false} otherwise.
	 * @see #obtainIntBoolean(Cursor, String)
	 */
	public static boolean obtainBoolean(@NonNull final Cursor cursor, @NonNull final String columnName) {
		return Boolean.parseBoolean(obtainString(cursor, columnName));
	}

	/**
	 * Returns value obtained from {@link #obtainInt(Cursor, String)} resolved as <b>boolean</b>.
	 *
	 * @return {@code True} if the obtained integer has {@code value == 1}, {@code false} otherwise.
	 * @see #obtainBoolean(Cursor, String)
	 */
	public static boolean obtainIntBoolean(@NonNull final Cursor cursor, @NonNull final String columnName) {
		return obtainInt(cursor, columnName) == 1;
	}
}
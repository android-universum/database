/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.database.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import androidx.annotation.NonNull;
import universum.studios.android.database.adapter.BaseCursorAdapter;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public final class SampleCursorAdapter extends BaseCursorAdapter<Cursor, String, View> {

	public SampleCursorAdapter(@NonNull final Context context) {
		super(context);
	}

	@Override protected void onBindViewHolder(@NonNull View viewHolder, @NonNull Cursor cursor, int position) {}
}
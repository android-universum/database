/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.database.data.model;

import android.database.Cursor;
import android.os.Parcel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.model.EntityModelCursorWrapper;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.samples.database.data.entity.ContactsEntity;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("WeakerAccess")
public final class Contact extends SimpleEntityModel<Contact> {

	public static final Creator<Contact> CREATOR = new Creator<Contact>() {

		@Override public Contact createFromParcel(@NonNull final Parcel source) { return new Contact(source); }

		@Override public Contact[] newArray(final int size) { return new Contact[size]; }
	};

	public static final PoolFactory<Contact> FACTORY = new PoolFactory<Contact>(10) {

		@Override @NonNull public Contact createModel() { return new Contact(); }
	};

	@Column.Primary
	@Column(ContactsEntity.Columns.ID)
	public Long id;

	Contact() {}

	Contact(@NonNull final Parcel source) {
		super(source);
	}

	@Override
	@SuppressWarnings("StringBufferReplaceableByString")
	public String toString() {
		final StringBuilder builder = new StringBuilder(64);
		builder.append(getClass().getSimpleName());
		builder.append("{id: ");
		builder.append(id);
		return builder.append("}").toString();
	}

	@Override public void recycle() {
		super.recycle();
		FACTORY.recycleModel(this);
	}

	public static final class CursorWrapper extends EntityModelCursorWrapper<Contact> {

		private CursorWrapper(@NonNull final Cursor cursor) {
			super(cursor, FACTORY.obtainModel());
			setRecycleModelOnClose(true);
		}

		@NonNull public static CursorWrapper wrap(@NonNull final Cursor cursor) {
			return cursor instanceof CursorWrapper ? (CursorWrapper) cursor : new CursorWrapper(cursor);
		}

		@Override @NonNull protected Contact onGetModel() {
			return accessModel();
		}

		@Override @Nullable protected Contact onBindModel(@NonNull final Contact model) {
			return super.onBindModel(FACTORY.obtainModel());
		}
	}
}
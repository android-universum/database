@Database-Entity
===============

This module groups the following modules into one **single group**:

- [Entity-Core](https://bitbucket.org/android-universum/database/src/main/library-entity-core)
- [Entity-Model](https://bitbucket.org/android-universum/database/src/main/library-entity-model)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-entity:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-base](https://bitbucket.org/android-universum/database/src/main/library-base),
[database-query](https://bitbucket.org/android-universum/database/src/main/library-query),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-model-core](https://bitbucket.org/android-universum/database/src/main/library-model-core),
[database-model-collection](https://bitbucket.org/android-universum/database/src/main/library-model-collection)
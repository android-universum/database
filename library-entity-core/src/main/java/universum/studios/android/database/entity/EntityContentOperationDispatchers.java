/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseEntity;
import universum.studios.android.database.cursor.EmptyCursor;

/**
 * Factory that provides implementations of {@link DatabaseEntity.ContentOperationDispatcher ContentOperationDispatchers}.
 *
 * @author Martin Albedinsky
 */
public final class EntityContentOperationDispatchers {

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * A {@link DatabaseEntity.ContentOperationDispatcher} implementation that dispatches all content
	 * related operations directly to the passed entity via one of its corresponding dispatch methods.
	 */
	private static final DatabaseEntity.ContentOperationDispatcher ENTITY = new BaseDispatcher() {

		/**
		 */
		@Override
		public int dispatchBulkInsert(@NonNull DatabaseEntity entity, @NonNull ContentValues[] values) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.dispatchBulkInsert(entity.getDatabase().asWritable(), values);
		}

		/**
		 */
		@Override
		public Uri dispatchInsert(@NonNull DatabaseEntity entity, @Nullable ContentValues values) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.dispatchInsert(entity.getDatabase().asWritable(), values);
		}

		/**
		 */
		@NonNull
		@Override
		public Cursor dispatchQuery(@NonNull DatabaseEntity entity, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.dispatchQuery(entity.getDatabase().asReadable(), projection, selection, selectionArgs, sortOrder);
		}

		/**
		 */
		@Override
		public int dispatchUpdate(@NonNull DatabaseEntity entity, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.dispatchUpdate(entity.getDatabase().asWritable(), values, selection, selectionArgs);
		}

		/**
		 */
		@Override
		public int dispatchDelete(@NonNull DatabaseEntity entity, @Nullable String selection, @Nullable String[] selectionArgs) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.dispatchDelete(entity.getDatabase().asWritable(), selection, selectionArgs);
		}
	};

	/**
	 * A {@link DatabaseEntity.ContentOperationDispatcher} implementation that dispatches all content
	 * related operations to the passed entity using {@link ContentResolver} via one of its corresponding
	 * methods.
	 */
	private static final DatabaseEntity.ContentOperationDispatcher CONTENT_RESOLVER = new BaseDispatcher() {

		/**
		 */
		@Override
		public int dispatchBulkInsert(@NonNull DatabaseEntity entity, @NonNull ContentValues[] values) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.getDatabase().getContentResolver().bulkInsert(entity.getContentUri(), values);
		}

		/**
		 */
		@Override
		public Uri dispatchInsert(@NonNull DatabaseEntity entity, @Nullable ContentValues values) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.getDatabase().getContentResolver().insert(entity.getContentUri(), values);
		}

		/**
		 */
		@NonNull
		@Override
		public Cursor dispatchQuery(@NonNull DatabaseEntity entity, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return EmptyCursor.createIfNull(entity.getDatabase().getContentResolver().query(entity.getContentUri(), projection, selection, selectionArgs, sortOrder));
		}

		/**
		 */
		@Override
		public int dispatchUpdate(@NonNull DatabaseEntity entity, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.getDatabase().getContentResolver().update(entity.getContentUri(), values, selection, selectionArgs);
		}

		/**
		 */
		@Override
		public int dispatchDelete(@NonNull DatabaseEntity entity, @Nullable String selection, @Nullable String[] selectionArgs) {
			assertEntityIsAttachedToDatabaseOrThrow(entity);
			return entity.getDatabase().getContentResolver().delete(entity.getContentUri(), selection, selectionArgs);
		}
	};

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private EntityContentOperationDispatchers() {
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns instance of {@link DatabaseEntity.ContentOperationDispatcher} implementation that
	 * dispatches all content related operations directly to the passed entity via one of its
	 * corresponding dispatch methods.
	 *
	 * @return Dispatcher implementation for dispatching directly to the desired entity.
	 */
	@NonNull
	public static DatabaseEntity.ContentOperationDispatcher entity() {
		return ENTITY;
	}

	/**
	 * Returns instance of {@link DatabaseEntity.ContentOperationDispatcher} implementation that
	 * dispatches all content related operations to the passed entity using {@link ContentResolver}
	 * via one of its corresponding methods.
	 *
	 * @return Dispatcher implementation for dispatching via {@link ContentResolver}.
	 */
	@NonNull
	public static DatabaseEntity.ContentOperationDispatcher contentResolver() {
		return CONTENT_RESOLVER;
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Base implementation of {@link DatabaseEntity.ContentOperationDispatcher} for dispatchers
	 * implemented in this factory class.
	 */
	private static abstract class BaseDispatcher implements DatabaseEntity.ContentOperationDispatcher {

		/**
		 * Asserts that the given <var>entity</var> is attached to some database, if not an exception
		 * is thrown.
		 *
		 * @param entity The entity to check.
		 * @throws IllegalStateException If entity is not attached to any database.
		 */
		void assertEntityIsAttachedToDatabaseOrThrow(DatabaseEntity entity) {
			if (!entity.isAttachedToDatabase()) {
				throw new IllegalStateException("DatabaseEntity(" + entity.getClass() + ") is not attached to any database.");
			}
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;
import androidx.loader.content.AsyncTaskLoader;
import universum.studios.android.database.Database;
import universum.studios.android.database.DatabaseEntity;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.Transaction;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.handler.EntityAnnotationHandler;
import universum.studios.android.database.annotation.handler.EntityAnnotationHandlers;
import universum.studios.android.database.content.IdSelection;
import universum.studios.android.database.cursor.EmptyCursor;
import universum.studios.android.database.query.BaseQuery;

/**
 * An Entity that is implementation of {@link DatabaseEntity} is intended to represent one table within
 * a database. By representation here is meant that a particular Entity implementation can operate
 * upon content stored within its associated database table in the way that is supported by the
 * Android SQL framework itself, so like <b>inserting</b>, <b>updating</b>, <b>deleting</b> of such
 * content and more. Each entity instance need to be created with its corresponding name via
 * {@link #Entity(String)} constructor. The entity name is used as name for its related database
 * table. Optionally also name of the primary column may be supplied via {@link #Entity(String, String)}
 * constructor.
 * <p>
 * To support all this logic, the Entity class implements all dispatch methods required by {@link Database}
 * via {@link DatabaseEntity} interface. Below section describes some of the implementation details
 * of such logic:
 * <ul>
 * <li>
 * {@link #dispatchInsert(SQLiteDatabase, ContentValues)}
 * <p>
 * This method wraps {@link #onInsert(SQLiteDatabase, ContentValues)} into database transaction block
 * and returns result from that method if such transaction has been successful, {@link #NO_ROWS} result
 * otherwise.
 * <p>
 * Basically this method will be invoked from the {@link Database} object to which is that particular
 * entity attached whenever its {@link Database#insert(Uri, ContentValues)} is invoked, thus
 * {@link ContentResolver#insert(Uri, ContentValues)} has been called to start <b>INSERT</b> operation
 * chain from the appropriate context.
 * </li>
 * <li>
 * {@link #dispatchBulkInsert(SQLiteDatabase, ContentValues[])}
 * <p>
 * This method wraps {@link #onBulkInsert(SQLiteDatabase, ContentValues[])} into database transaction
 * block and returns result from that method if such transaction has been successful, {@link #NO_ROWS}
 * result otherwise.
 * <p>
 * Basically this method will be invoked from the {@link Database} object to which is that particular
 * entity attached whenever its {@link Database#bulkInsert(Uri, ContentValues[])} is invoked, thus
 * {@link ContentResolver#bulkInsert(Uri, ContentValues[])} has been called to start <b>BULK INSERT</b>
 * operation chain from the appropriate context.
 * </li>
 * <li>
 * {@link #dispatchQuery(SQLiteDatabase, String[], String, String[], String)}
 * <p>
 * This method calls directly {@link #onQuery(SQLiteDatabase, String[], String, String[], String)}
 * and returns an instance of {@link Cursor} returned by that method.
 * <p>
 * Basically this method will be invoked from the {@link Database} object to which is that particular
 * entity attached whenever its {@link Database#query(Uri, String[], String, String[], String)} is
 * invoked, thus {@link ContentResolver#query(Uri, String[], String, String[], String)} has been called
 * to start <b>QUERY</b> operation chain from the appropriate context.
 * </li>
 * <li>
 * {@link #dispatchUpdate(SQLiteDatabase, ContentValues, String, String[])}
 * <p>
 * This method wraps {@link #onUpdate(SQLiteDatabase, ContentValues, String, String[])} into database
 * transaction block and returns result from that method if such transaction has been successful,
 * {@link #NO_ROWS} result otherwise.
 * <p>
 * Basically this method will be invoked from the {@link Database} object to which is that particular
 * entity attached whenever its {@link Database#update(Uri, ContentValues, String, String[])} is invoked,
 * thus {@link ContentResolver#update(Uri, ContentValues, String, String[])} has been called to start
 * <b>UPDATE</b> operation chain from the appropriate context.
 * </li>
 * <li>
 * {@link #dispatchDelete(SQLiteDatabase, String, String[])}
 * <p>
 * This method wraps {@link #onDelete(SQLiteDatabase, String, String[])} into database transaction
 * block and returns result from that method if such transaction has been successful, {@link #NO_ROWS}
 * result otherwise.
 * <p>
 * Basically this method will be invoked from the {@link Database} object to which is that particular
 * entity attached whenever its {@link Database#delete(Uri, String, String[])} is invoked, thus
 * {@link ContentResolver#delete(Uri, String, String[])} has been called to start <b>DELETE</b>
 * operation chain from the appropriate context.
 * </li>
 * </ul>
 *
 * <h3>Life cycle</h3>
 * <b>1) Attaching:</b>
 * <p>
 * As described in {@link Database}, all attached entities (theirs instances) are managed within
 * context of such Database object. What is important for context of Entity class and its lifecycle
 * is that to successfully execute one of its provided API methods to modify, create or delete a
 * content within its associated database table it is required that a particular entity is attached
 * to its associated Database via {@link #attachToDatabase(Database)} so it has access to
 * {@link ContentResolver} that is used to support all database content related operations. Basically
 * this does not need to bother you in any way if you will always access your desired entities as
 * described in <b>4) Accessing</b> section below.
 * <p>
 * <b>2) Creating:</b>
 * <p>
 * When an Android application database is being first time created the associated Database will
 * invoke {@link #dispatchCreate(SQLiteDatabase)} upon each of its assigned entities.
 * <p>
 * <b>3) Upgrading:</b>
 * <p>
 * When an Android application database is being upgraded from its old version to a new one, the
 * associated Database will invoke {@link #dispatchUpgrade(SQLiteDatabase, int, int)} upon each of
 * its assigned entities.
 * <p>
 * <b>4) Accessing:</b>
 * <p>
 * Basically whenever you request an instance of your desired entity via {@link Database#findEntityByClass(Class)}
 * or via {@link Database#findEntityByUri(Uri)} that returned entity instance will be always attached
 * to its associated Database, thus can be used to perform immediately the desired content related
 * operations.
 * <p>
 * <b>Avoid creating of new instances of your entities by yourself because it may lead to errors
 * related to not attached entities to Database object or not proper dispatching of database requests
 * to such entities.</b>
 *
 * <h3>Content</h3>
 * <ul>
 * <li>
 * <b>Creating initial content</b>:
 * <p>
 * If a particular entity wants to insert some initial content into its database table while it is
 * being first time created, it can do so via requesting such intent via {@link #createsInitialContent()}.
 * If requested, the associated Database will invoke {@link #dispatchCreateInitialContent(SQLiteDatabase)}
 * upon each of its assigned entities that requested creation of theirs initial content.
 * </li>
 * <li>
 * <b>Upgrading of existing content</b>:
 * <p>
 * If a particular entity wants to upgrade an existing content within its database table while it is
 * being upgraded to a new version, it can do so via requesting such intent via {@link #upgradesContent(int, int)}.
 * If requested (for the upgrading version), the associated Database will invoke
 * {@link #dispatchUpgradeContent(SQLiteDatabase, int, int)} upon each of its assigned entities that
 * requested upgrade of theirs existing content.
 * </li>
 * </ul>
 *
 * <h3>Configuration</h3>
 * Below are listed methods that may be used to configure a specific entity instance:
 * <ul>
 * <li>{@link #setAutoInsertOnUpdateEnabled(boolean)}</li>
 * <li>{@link #setContentChangeNotificationEnabled(boolean)}</li>
 * <li>{@link #setMimeType(String)}</li>
 * </ul>
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link universum.studios.android.database.annotation.EntityName @Name} <b>[class]</b>
 * <p>
 * If this annotation is presented, a text provided via this annotation will be used as name for a
 * subclass of the Entity class.
 * </li>
 * </ul>
 *
 * @author Martin Albedinsky
 */
public abstract class Entity implements DatabaseEntity {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "Entity";

	/**
	 * Private flag indicating that insert action should be automatically performed on update if there
	 * is no row for the specified selection within database table yet.
	 */
	private static final int PFLAG_AUTO_INSERT_ON_UPDATE = 0x00000001;

	/**
	 * Private flag indicating that content change notifications are enabled.
	 */
	private static final int PFLAG_CONTENT_CHANGE_NOTIFICATION_ENABLED = 0x00000001 << 1;

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Empty results returned by {@link #applyBatchOperations(ArrayList)} in case when some error occurs.
	 */
	private static final ContentProviderResult[] EMPTY_RESULTS = new ContentProviderResult[0];

	/**
	 * Default content operation dispatcher used by all entities.
	 */
	private static final ContentOperationDispatcher OPERATION_DISPATCHER = EntityContentOperationDispatchers.entity();

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Handler responsible for processing of all annotations of this class and also for handling all
	 * annotations related operations for this class.
	 */
	final EntityAnnotationHandler mAnnotationHandler;

	/**
	 * Name of this entity. Should be unique, because it will be used when creating the database table
	 * represented by this entity.
	 */
	final String mName;

	/**
	 * Name of the primary column used when performing transactions for single id or set of multiple ids.
	 */
	final String mPrimaryColumnName;

	/**
	 * Content uri of this entity. Used to access content within the database table represented by
	 * this entity.
	 */
	private Uri mContentUri;

	/**
	 * Content uri used to notify registered observers that a content of this entity has been changed.
	 * This is in most cases the same uri as {@link #mContentUri} but can be changed to support notification
	 * on different uri if needed.
	 */
	Uri mNotificationUri;

	/**
	 * List of locks created to lock content notifications.
	 */
	private final List<Lock> mContentNotificationLocks = new ArrayList<>(2);

	/**
	 * Mime type of content stored within the database table represented by this entity.
	 */
	private String mMimeType;

	/**
	 * Dispatcher that is used to dispatch content related operations requested for this entity instance.
	 */
	private ContentOperationDispatcher mOperationDispatcher = OPERATION_DISPATCHER;

	/**
	 * Database to which is this entity attached. May be {@code null} if this entity is not attached
	 * to any database.
	 */
	private Database mDatabase;

	/**
	 * Stores all private flags of this entity instance.
	 */
	private int mPrivateFlags = PFLAG_CONTENT_CHANGE_NOTIFICATION_ENABLED;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #Entity(String)} where entity name will be obtained from the attached annotation
	 * or created automatically from this entity's class name.
	 * <p>
	 * This constructor will use the following annotations to properly configure the new entity instance:
	 * <ul>
	 * <li>{@link universum.studios.android.database.annotation.EntityName @EntityName}</li>
	 * </ul>
	 */
	public Entity() {
		this.mAnnotationHandler = onCreateAnnotationHandler();
		this.mName = mAnnotationHandler == null ?
				createNameFromClass(getClass()) :
				mAnnotationHandler.getEntityName(createNameFromClass(getClass()));
		this.mPrimaryColumnName = Column.Primary.COLUMN_NAME;
	}

	/**
	 * Creates a new instance of database Entity with the specified <var>name</var> that is used as
	 * name for database table represented by this entity.
	 *
	 * @param name The desired name for the new entity. Will be used when creating database table
	 *             represented by this entity.
	 */
	protected Entity(@NonNull String name) {
		this(name, Column.Primary.COLUMN_NAME);
	}


	/**
	 * Creates a new instance of database Entity with the specified <var>name</var> and <var>primaryColumnName</var>.
	 *
	 * @param name              The desired name for the new entity. Will be used when creating database
	 *                          table represented by this entity.
	 * @param primaryColumnName Name of the primary column used when performing operations for single
	 *                          id or set of multiple ids.
	 */
	protected Entity(@NonNull String name, @NonNull String primaryColumnName) {
		this.mAnnotationHandler = onCreateAnnotationHandler();
		this.mName = name;
		this.mPrimaryColumnName = primaryColumnName;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Parses an id from the specified <var>rowUri</var>.
	 *
	 * @param rowUri The uri of the row of which id to parse.
	 * @return Parsed id from the uri or {@code -1} if the given uri is invalid.
	 * @throws NumberFormatException If last path segment of the given uri is not a number, thus
	 *                               cannot be parsed into id.
	 */
	public static long parseRowId(@Nullable Uri rowUri) {
		final String lastPathSegment = rowUri == null ? null : rowUri.getLastPathSegment();
		return TextUtils.isEmpty(lastPathSegment) ? -1 : Long.parseLong(lastPathSegment);
	}

	/**
	 * Invoked to create annotations handler for this instance.
	 *
	 * @return Annotations handler specific for this class.
	 */
	EntityAnnotationHandler onCreateAnnotationHandler() {
		return EntityAnnotationHandlers.obtainEntityHandler(getClass());
	}

	/**
	 * Returns handler that is responsible for annotations processing of this class and also for
	 * handling all annotations related operations for this class.
	 *
	 * @return Annotations handler specific for this class.
	 * @throws DatabaseException If annotations processing is not enabled for the Database library.
	 */
	@NonNull
	protected EntityAnnotationHandler getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return mAnnotationHandler;
	}

	/**
	 * Creates a entity name from the specified <var>classOfEntity</var>.
	 * <p>
	 * <b>Example: ReceivedInvoicesEntity -> received_invoices</b>
	 *
	 * @param classOfEntity The entity class that should be used to create a new name.
	 * @return New entity name that is extracted from the simple name of the specified entity class
	 * and transformed from CamelCase text into lowercase text with underscores.
	 */
	private static String createNameFromClass(Class<?> classOfEntity) {
		final String entityName = classOfEntity.getSimpleName().replace(TAG, "");
		final StringBuilder nameBuilder = new StringBuilder(entityName.length());
		for (int i = 0; i < entityName.length(); i++) {
			final char ch = entityName.charAt(i);
			if (Character.isUpperCase(ch)) {
				if (i > 0) {
					nameBuilder.append("_");
				}
				nameBuilder.append(Character.toLowerCase(ch));
			} else {
				nameBuilder.append(ch);
			}
		}
		return nameBuilder.toString();
	}

	/**
	 */
	@NonNull
	@Override
	public final String getName() {
		return mName;
	}

	/**
	 * Returns the primary column name specified for this entity.
	 *
	 * @return This entity's primary column name.
	 */
	@NonNull
	public final String getPrimaryColumnName() {
		return mPrimaryColumnName;
	}

	/**
	 * Sets a flag indicating whether the data change notification for this entity's uri is enabled
	 * or not.
	 * <p>
	 * If enabled, whenever some database transaction is successfully executed upon table represented
	 * by this entity, {@link ContentResolver#notifyChange(Uri, android.database.ContentObserver)}
	 * will be fired with this entity's content uri.
	 *
	 * @param enabled {@code True} to enable, {@code false} to disable.
	 * @see #isContentChangeNotificationEnabled()
	 */
	public void setContentChangeNotificationEnabled(boolean enabled) {
		if (enabled) this.mPrivateFlags |= PFLAG_CONTENT_CHANGE_NOTIFICATION_ENABLED;
		else this.mPrivateFlags &= ~PFLAG_CONTENT_CHANGE_NOTIFICATION_ENABLED;
	}

	/**
	 * Returns the flag indicating whether the data change notification is enabled or not.
	 * <p>
	 * By default this option <b>is enabled</b>.
	 *
	 * @return {@code True} when enabled and there are no notification locks acquired at this time,
	 * {@code false} otherwise.
	 * @see #setContentChangeNotificationEnabled(boolean)
	 * @see #lockContentNotifications()
	 */
	public boolean isContentChangeNotificationEnabled() {
		return (mContentNotificationLocks == null || mContentNotificationLocks.isEmpty()) &&
				(mPrivateFlags & PFLAG_CONTENT_CHANGE_NOTIFICATION_ENABLED) != 0;
	}

	/**
	 * Sets a flag indicating whether the <b>INSERT</b> operation should be performed instead of
	 * requested <b>update</b> operation where such a request failed, mostly due to not yet existing
	 * row with the specified arguments.
	 *
	 * @param enabled {@code True} to enable, {@code false} to disable.
	 * @see #isAutoInsertOnUpdateEnabled()
	 */
	public void setAutoInsertOnUpdateEnabled(boolean enabled) {
		if (enabled) this.mPrivateFlags |= PFLAG_AUTO_INSERT_ON_UPDATE;
		else this.mPrivateFlags &= ~PFLAG_AUTO_INSERT_ON_UPDATE;
	}

	/**
	 * Returns the flag indicating whether the <b>INSERT</b> operation should be performed instead of
	 * requested <b>update</b> operation if such operation fails.
	 * <p>
	 * By default this option <b>is not enabled</b>.
	 *
	 * @return {@code True} when enabled, {@code false} otherwise.
	 * @see #setAutoInsertOnUpdateEnabled(boolean)
	 */
	public boolean isAutoInsertOnUpdateEnabled() {
		return (mPrivateFlags & PFLAG_AUTO_INSERT_ON_UPDATE) != 0;
	}

	/**
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #isAttachedToDatabase()
	 */
	@NonNull
	@Override
	public final Uri getContentUri() {
		this.assertAttachedToDatabaseOrThrow("ACCESS CONTENT URI");
		return mContentUri;
	}

	/**
	 * Specifies an uri used to notify registered observers that a content of this entity has been
	 * changed.
	 *
	 * @param notificationUri The desired uri. Can be {@code null} to use content uri of this entity
	 *                        instead.
	 * @see #getContentUri()
	 * @see #getNotificationUri()
	 */
	protected final void setNotificationUri(@Nullable Uri notificationUri) {
		this.mNotificationUri = notificationUri;
	}

	/**
	 * Returns the uri used to notify content changes of this entity.
	 * <p>
	 * By default this is the same uri as {@link #getContentUri()} but can be changed to support cases
	 * when this entity is created only to JOIN multiple entities so it should use uri of one of that
	 * entities otherwise content notification will not work.
	 *
	 * @return Notification uri of this entity.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #setNotificationUri(Uri)
	 * @see #isAttachedToDatabase()
	 */
	@NonNull
	public final Uri getNotificationUri() {
		if (mNotificationUri == null && mContentUri == null) {
			this.assertAttachedToDatabaseOrThrow("ACCESS NOTIFICATION URI");
		}
		return mNotificationUri == null ? mContentUri : mNotificationUri;
	}

	/**
	 * Sets a Mime type of a content stored within a table represented by this entity.
	 *
	 * @param mimeType The desired content Mime type.
	 * @see #getMimeType()
	 */
	protected final void setMimeType(@NonNull String mimeType) {
		this.mMimeType = mimeType;
	}

	/**
	 * @throws IllegalStateException If this entity does not have mime-type specified and it is not
	 *                               attached to any database.
	 * @see #isAttachedToDatabase()
	 */
	@NonNull
	@Override
	public final String getMimeType() {
		if (TextUtils.isEmpty(mMimeType)) {
			this.assertAttachedToDatabaseOrThrow("ACCESS MIME TYPE");
		}
		return mMimeType;
	}

	/**
	 */
	@Override
	public void setContentOperationDispatcher(@Nullable ContentOperationDispatcher dispatcher) {
		this.mOperationDispatcher = dispatcher == null ? OPERATION_DISPATCHER : dispatcher;
	}

	/**
	 */
	@Override
	public final void attachToDatabase(@NonNull Database database) {
		if (mDatabase == null) {
			this.mDatabase = database;
			final String authority = database.getAuthority();
			this.mContentUri = Database.createContentUri(authority, mName);
			this.mMimeType = TextUtils.isEmpty(mMimeType) ?
					Database.createMimeType(
							ContentResolver.CURSOR_DIR_BASE_TYPE,
							authority,
							mName
					) :
					mMimeType;
			onAttachedToDatabase(database);
		} else {
			throw new IllegalStateException("Entity(" + getClass().getSimpleName() + ") is already attached to database.");
		}
	}

	/**
	 * Invoked whenever {@link #attachToDatabase(Database)} is called upon this entity instance and
	 * this entity is not attached to any database yet.
	 * <p>
	 * When entity is attached to database it may perform all database content related operations.
	 */
	protected void onAttachedToDatabase(@NonNull Database database) {
		// Inheritance hierarchies may perform here logic related to event when they are attached
		// to theirs corresponding database.
	}

	/**
	 * Asserts that this entity is attached to database. If not, an exception is thrown.
	 *
	 * @param forAction Name of action for which is required that entity is attached to database.
	 * @throws IllegalStateException If this entity instance is not attached to database yet.
	 */
	private void assertAttachedToDatabaseOrThrow(String forAction) {
		if (!isAttachedToDatabase()) {
			throw new IllegalStateException(
					"Cannot perform entity action(" + forAction + "). " +
							"Entity(" + getClass().getSimpleName() + ") is not attached to any database."
			);
		}
	}

	/**
	 */
	@Override
	public final boolean isAttachedToDatabase() {
		return mDatabase != null;
	}

	/**
	 */
	@NonNull
	@Override
	public final Database getDatabase() {
		assertAttachedToDatabaseOrThrow("ACCESS DATABASE");
		return mDatabase;
	}

	/**
	 * Beings a new transaction for the {@link SQLiteDatabase} managed by the database instance to
	 * which is this entity attached in the specified <var>mode</var>.
	 *
	 * @param mode The desired transaction mode. One of modes defined by {@link Transaction.Mode @Transaction.Mode}
	 *             annotation.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #isAttachedToDatabase()
	 * @see #endTransaction(boolean)
	 * @see Database#beginTransaction(int)
	 */
	protected final void beginTransaction(@Transaction.Mode int mode) {
		assertAttachedToDatabaseOrThrow("BEGIN TRANSACTION");
		mDatabase.beginTransaction(mode);
	}

	/**
	 * Ends the transaction started via {@link #beginTransaction(int)} either <b>successfully</b>
	 * or not.
	 *
	 * @param successful {@code True} if transaction has been successful, {@code false} otherwise.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #isAttachedToDatabase()
	 * @see Database#setTransactionSuccessful()
	 * @see Database#endTransaction()
	 */
	protected final void endTransaction(boolean successful) {
		assertAttachedToDatabaseOrThrow("END TRANSACTION");
		if (successful) {
			mDatabase.setTransactionSuccessful();
		}
		mDatabase.endTransaction();
	}

	/**
	 */
	@Override
	public final void dispatchCreate(@NonNull SQLiteDatabase db) {
		onCreate(db);
	}

	/**
	 * Invoked from {@link #dispatchCreate(SQLiteDatabase)} to create a database table represented by
	 * this entity.
	 */
	protected abstract void onCreate(@NonNull SQLiteDatabase db);

	/**
	 */
	@Override
	public boolean createsInitialContent() {
		return false;
	}

	/**
	 */
	@Override
	public final void dispatchCreateInitialContent(@NonNull SQLiteDatabase db) {
		onCreateInitialContent(db);
	}

	/**
	 * Invoked from {@link #dispatchCreateInitialContent(SQLiteDatabase)} to create initial content
	 * for database table represented by this entity.
	 */
	protected void onCreateInitialContent(@NonNull SQLiteDatabase db) {
		// Inheritance hierarchies may perform here insertion of theirs initial content into the DB.
	}

	/**
	 * This implementation by default invokes {@link #createsInitialContent()} method.
	 */
	@Override
	public boolean createsInitialContent(int oldVersion, int newVersion) {
		return createsInitialContent();
	}

	/**
	 */
	@Override
	public final void dispatchCreateInitialContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreateInitialContent(db, oldVersion, newVersion);
	}

	/**
	 * Invoked from {@link #dispatchCreateInitialContent(SQLiteDatabase, int, int)} to create initial
	 * content for database table represented by this entity according to the specified  database versions.
	 * <p>
	 * This implementation by default invokes {@link #onCreateInitialContent(SQLiteDatabase)} method.
	 */
	protected void onCreateInitialContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreateInitialContent(db);
	}

	/**
	 * Inserts the specified <var>content</var> values into the given <var>db</var> as initial content
	 * of this entity.
	 * <p>
	 * This is same as {@link #dispatchBulkInsert(SQLiteDatabase, ContentValues[])} and is here only
	 * for convenience.
	 *
	 * @param db      SQLite database where to insert the specified content values into table represented
	 *                by name of this entity.
	 * @param content The desired set of content values to be inserted as initial content.
	 * @return Count of the successfully inserted values or {@code -1} if database transaction failed.
	 */
	protected int insertInitialContent(@NonNull SQLiteDatabase db, @NonNull ContentValues[] content) {
		return dispatchBulkInsert(db, content);
	}

	/**
	 */
	@Override
	public void dispatchUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	/**
	 * Invoked from {@link #dispatchUpgrade(SQLiteDatabase, int, int)} to
	 * upgrade database table represented by this entity to a new version.
	 */
	protected void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		// Inheritance hierarchies may perform here theirs specific upgrade logic.
	}

	/**
	 */
	@Override
	public boolean upgradesContent(int oldVersion, int newVersion) {
		return false;
	}

	/**
	 */
	@Override
	public void dispatchUpgradeContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgradeContent(db, oldVersion, newVersion);
	}

	/**
	 * Invoked from {@link #dispatchUpgradeContent(SQLiteDatabase, int, int)} to upgrade existing
	 * content within database table represented by this entity to a new version.
	 */
	protected void onUpgradeContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		// Inheritance hierarchies may perform here theirs specific content related upgrade logic.
	}

	/**
	 */
	@Override
	public void dispatchDowngrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		onDowngrade(db, oldVersion, newVersion);
	}

	/**
	 * Invoked from {@link #dispatchDowngrade(SQLiteDatabase, int, int)} to downgrade database table
	 * represented by this entity to a new version.
	 */
	protected void onDowngrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		// Inheritance hierarchies may perform here theirs specific downgrade logic.
	}

	/**
	 * Builds a set of <b>INSERT</b> operations for the specified <var>mContentValues</var>.
	 * <p>
	 * This is basically a bulk operation for {@link #createInsertOperation(ContentValues)} method.
	 *
	 * @param contentValues Set of content values for which to create insert operations.
	 * @return The requested operations type of <b>INSERT</b> for the specified values.
	 */
	@NonNull
	public List<ContentProviderOperation> createInsertOperations(@NonNull @Size(min = 1) ContentValues[] contentValues) {
		final List<ContentProviderOperation> operations = new ArrayList<>(contentValues.length);
		for (final ContentValues values : contentValues) {
			operations.add(createInsertOperation(values));
		}
		return operations;
	}

	/**
	 * Builds a new <b>INSERT</b> operation using {@link #prepareInsertOperation()} for the specified
	 * <var>mContentValues</var>.
	 *
	 * @param contentValues The desired content values for which to build the new insert operation.
	 * @return New instance of {@link ContentProviderOperation} type of <b>INSERT</b>.
	 */
	@NonNull
	public ContentProviderOperation createInsertOperation(@NonNull ContentValues contentValues) {
		final ContentProviderOperation.Builder operationBuilder = prepareInsertOperation();
		operationBuilder.withValues(contentValues);
		return operationBuilder.build();
	}

	/**
	 * Prepares a new instance of ContentProviderOperation.Builder type of <b>INSERT</b> that can be
	 * used to build an operation for a desired {@link ContentValues} to insert a new content (row)
	 * into database table represented by this entity.
	 * <p>
	 * Such operation can be then executed as part of batch processing using {@link ContentResolver#applyBatch(String, ArrayList)}
	 *
	 * @return New instance of {@link ContentProviderOperation.Builder} for <b>INSERT</b> operation.
	 * @throws IllegalStateException If this entity is not attached to any database, thus it has no
	 *                               content uri for which to create the requested operation.
	 * @see #createInsertOperation(ContentValues)
	 */
	@NonNull
	public ContentProviderOperation.Builder prepareInsertOperation() {
		this.assertAttachedToDatabaseOrThrow("PREPARE INSERT OPERATION");
		return ContentProviderOperation.newInsert(mContentUri);
	}

	/**
	 * Builds a new <b>UPDATE</b> operation using {@link #prepareUpdateOperation()} for the specified
	 * <var>mContentValues</var>.
	 *
	 * @param contentValues The desired content values for which to build the new update operation.
	 * @param selection     Selection (where) statement.
	 * @param selectionArgs Arguments for selection statement.
	 * @return New instance of {@link ContentProviderOperation} type of <b>UPDATE</b>.
	 */
	@NonNull
	public ContentProviderOperation createUpdateOperation(@NonNull ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
		final ContentProviderOperation.Builder operationBuilder = prepareUpdateOperation();
		operationBuilder.withSelection(selection, selectionArgs);
		operationBuilder.withValues(contentValues);
		return operationBuilder.build();
	}

	/**
	 * Prepares a new instance of ContentProviderOperation.Builder type of <b>UPDATE</b> that can be
	 * used to build an operation for a desired {@link ContentValues} to update an existing content
	 * (row/s) targeted by a desired selection within database table represented by this entity.
	 * <p>
	 * Such operation can be then executed as part of batch processing using {@link ContentResolver#applyBatch(String, ArrayList)}
	 *
	 * @return New instance of {@link ContentProviderOperation.Builder} for <b>UPDATE</b> operation.
	 * @throws IllegalStateException If this entity is not attached to any database, thus it has no
	 *                               content uri for which to create the requested operation.
	 * @see #createUpdateOperation(ContentValues, String, String[])
	 */
	@NonNull
	public ContentProviderOperation.Builder prepareUpdateOperation() {
		this.assertAttachedToDatabaseOrThrow("PREPARE UPDATE OPERATION");
		return ContentProviderOperation.newUpdate(mContentUri);
	}

	/**
	 * Builds a new <b>DELETE</b> operation intended to delete a whole content within database table
	 * represented by this entity.
	 * <p>
	 * Same as {@link #createDeleteOperation(String, String[])} with {@code null} selection and
	 * selection arguments.
	 *
	 * @return New instance of {@link ContentProviderOperation} type of <b>DELETE</b>.
	 */
	@NonNull
	public ContentProviderOperation createDropContentOperation() {
		return createDeleteOperation(null, null);
	}

	/**
	 * Builds a new <b>DELETE</b> operation using {@link #createDeleteOperation(String, String[])}
	 * for the specified <var>ids</var> that are used to build a proper selection statement
	 *
	 * @param ids The desired ids for which to build the new delete operation.
	 * @return New instance of {@link ContentProviderOperation} type of <b>DELETE</b>.
	 */
	@NonNull
	public ContentProviderOperation createDeleteOperation(@NonNull @Size(min = 1) long[] ids) {
		final IdSelection selection = new IdSelection(mPrimaryColumnName).withIds(ids);
		return createDeleteOperation(selection.build(), selection.buildArgs());
	}

	/**
	 * Builds a new <b>DELETE</b> operation using {@link #createDeleteOperation(String, String[])}
	 * for the specified <var>id</var> that is used to build a proper selection statement.
	 *
	 * @param id The desired id for which to build the new delete operation.
	 * @return New instance of {@link ContentProviderOperation} type of <b>DELETE</b>.
	 */
	@NonNull
	public ContentProviderOperation createDeleteOperation(long id) {
		final IdSelection selection = new IdSelection(mPrimaryColumnName).withIds(new long[]{id});
		return createDeleteOperation(selection.build(), selection.buildArgs());
	}

	/**
	 * Builds a new <b>DELETE</b> operation using {@link #prepareDeleteOperation()} for the specified
	 * <var>selection</var> and <var>selectionArgs</var>.
	 *
	 * @param selection     Selection (where) statement.
	 * @param selectionArgs Arguments for selection statement.
	 * @return New instance of {@link ContentProviderOperation} type of <b>DELETE</b>.
	 */
	@NonNull
	public ContentProviderOperation createDeleteOperation(@Nullable String selection, @Nullable String[] selectionArgs) {
		final ContentProviderOperation.Builder operationBuilder = prepareDeleteOperation();
		operationBuilder.withSelection(selection, selectionArgs);
		return operationBuilder.build();
	}

	/**
	 * Prepares a new instance of ContentProviderOperation.Builder type of <b>DELETE</b> that can be
	 * used to build an operation to delete an existing content (row/s) targeted by a desired selection
	 * within database table represented by this entity.
	 * <p>
	 * Such operation can be then executed as part of batch processing using {@link ContentResolver#applyBatch(String, ArrayList)}
	 *
	 * @return New instance of {@link ContentProviderOperation.Builder} for <b>DELETE</b> operation.
	 * @throws IllegalStateException If this entity is not attached to any database, thus it has no
	 *                               content uri for which to create the requested operation.
	 * @see #createDeleteOperation(String, String[])
	 */
	@NonNull
	public ContentProviderOperation.Builder prepareDeleteOperation() {
		this.assertAttachedToDatabaseOrThrow("PREPARE DELETE OPERATION");
		return ContentProviderOperation.newDelete(mContentUri);
	}

	/**
	 * Builds a new <b>ASSERT</b> operation using {@link #prepareAssertOperation()} for the specified
	 * <var>mContentValues</var>, <var>selection</var> and <var>selectionArgs</var>.
	 *
	 * @param contentValues The desired content values for which to build the new assert operation.
	 * @param selection     Selection (where) statement.
	 * @param selectionArgs Arguments for selection statement.
	 * @return New instance of {@link ContentProviderOperation} type of <b>ASSERT</b>.
	 */
	@NonNull
	public ContentProviderOperation createAssertOperation(@NonNull ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
		final ContentProviderOperation.Builder operationBuilder = prepareAssertOperation();
		operationBuilder.withValues(contentValues);
		operationBuilder.withSelection(selection, selectionArgs);
		return operationBuilder.build();
	}

	/**
	 * Prepares a new instance of ContentProviderOperation.Builder type of <b>ASSERT</b> that can be
	 * used to build an operation to assert that a specific content (row/s) is presented within
	 * within database table represented by this entity targeted by a desired selection.
	 * <p>
	 * Such operation can be then executed as part of batch processing using {@link ContentResolver#applyBatch(String, ArrayList)}
	 *
	 * @return New instance of {@link ContentProviderOperation.Builder} for <b>ASSERT</b> operation.
	 * @throws IllegalStateException If this entity is not attached to any database, thus it has no
	 *                               content uri for which to create the requested operation.
	 * @see #createAssertOperation(ContentValues, String, String[])
	 */
	@NonNull
	public ContentProviderOperation.Builder prepareAssertOperation() {
		this.assertAttachedToDatabaseOrThrow("PREPARE ASSERT OPERATION");
		return ContentProviderOperation.newAssertQuery(mContentUri);
	}

	/**
	 * Same as {@link #applyBatchOperations(ArrayList)} where the applying of the specified operations
	 * will be wrapped into one single database transaction as shown below:
	 * <pre>
	 *     final Database database = ... the database to which is this entity attached
	 *     database.beginTransaction(transactionMode);
	 *     if (applyBatchOperations(operations).length == operations.size()) {
	 *         database.setTransactionSuccessful();
	 *     }
	 *     database.endTransaction();
	 * </pre>
	 * <p>
	 * <b>Note, that the transaction is marked as successful only if all operations has been applied
	 * successfully.</b>
	 */
	@NonNull
	public ContentProviderResult[] applyBatchOperations(@NonNull ArrayList<ContentProviderOperation> operations, @Transaction.Mode int transactionMode) {
		assertAttachedToDatabaseOrThrow("APPLY BATCH OPERATIONS IN TRANSACTION MODE");
		mDatabase.beginTransaction(transactionMode);
		final ContentProviderResult[] results;
		if ((results = applyBatchOperations(operations)).length == operations.size()) {
			mDatabase.setTransactionSuccessful();
		}
		mDatabase.endTransaction();
		return results;
	}

	/**
	 * Applies all the specified <var>operations</var> using {@link ContentResolver#applyBatch(String, ArrayList)}
	 * for the authority of this entity.
	 * <p>
	 * <b>Note,</b> that basically this method can be used to apply batch operations to any entity
	 * (its database table) as the specified operations are already carrying that entity's content uri
	 * determining to which database table should be these operations applied. But it is a good practice
	 * to use a particular instance of entity to create its operations via one of {@code create...Operation(...)}
	 * methods and use that same entity to apply those operations via this method.
	 * <p>
	 * <b>Note</b>, that during execution of this method the content notifications are locked via
	 * {@link #lockContentNotifications()} to ensure that any of the applying content operations do
	 * not fire content notification change which could cause performance issues due to many times
	 * updated UI when using {@link AsyncTaskLoader}. A caller should after this method returns invoke
	 * either {@link #notifyContentChanged()} or {@link #forceNotifyContentChanged()} to notify
	 * registered content observers (if desired).
	 *
	 * @param operations The desired operations to be applied.
	 * @return Array with applications results of same size as the specified operations if all the
	 * applications succeeded, smaller size if not or size of 0 if some operations related exception
	 * occurs.
	 * @throws IllegalStateException If this entity is not attached to any database or content resolver
	 *                               obtained from the current attached context is invalid.
	 */
	@NonNull
	public ContentProviderResult[] applyBatchOperations(@NonNull ArrayList<ContentProviderOperation> operations) {
		if (operations.isEmpty()) {
			return EMPTY_RESULTS;
		}
		assertAttachedToDatabaseOrThrow("APPLY BATCH OPERATIONS");
		final Lock lock = lockContentNotifications();
		ContentProviderResult[] results = null;
		try {
			results = mDatabase.getContentResolver().applyBatch(mDatabase.getAuthority(), operations);
		} catch (RemoteException | OperationApplicationException e) {
			e.printStackTrace();
		}
		lock.release();
		return results == null ? EMPTY_RESULTS : results;
	}

	/**
	 * Inserts the given <var>values</var> into database table represented by this entity in a bulk
	 * operation.
	 *
	 * @param values The desired array of content values to insert as new rows.
	 * @return Count of successfully inserted rows or {@link #NO_ROWS} if database transaction failed.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 */
	public int bulkInsert(@NonNull @Size(min = 1) ContentValues[] values) {
		assertAttachedToDatabaseOrThrow("BULK INSERT");
		return mOperationDispatcher.dispatchBulkInsert(this, values);
	}

	/**
	 */
	@Override
	public int dispatchBulkInsert(@NonNull SQLiteDatabase db, @NonNull ContentValues[] values) {
		int rows = 0;
		db.beginTransaction();
		try {
			rows = onBulkInsert(db, values);
			db.setTransactionSuccessful();
		} catch (IllegalStateException e) {
			Log.e(TAG, "BULK INSERT transaction has failed.", e);
			rows = NO_ROWS;
		} finally {
			db.endTransaction();
			if (rows > 0) {
				notifyContentChanged();
			}
		}
		return rows;
	}

	/**
	 * Bulk method for {@link #onInsert(SQLiteDatabase, ContentValues)}.
	 * <p>
	 * This implementation iterates through the given array of values and calls {@link #onInsert(SQLiteDatabase, ContentValues)}
	 * for each one of them.
	 * <p>
	 * <b>Note, that this call is already wrapped into database transaction.</b>
	 *
	 * @return Count of rows successfully inserted into database.
	 */
	protected int onBulkInsert(@NonNull SQLiteDatabase db, @NonNull ContentValues[] values) {
		int rows = 0;
		if (values.length > 0) {
			for (final ContentValues value : values) {
				if (onInsert(db, value) != null) rows++;
			}
		}
		return rows;
	}

	/**
	 * Inserts the given <var>values</var> into database table represented by this entity.
	 *
	 * @param values The desired content values to insert as a new row.
	 * @return Uri of the newly inserted row or {@code null} if database transaction failed.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #query(String[], String, String[], String)
	 * @see #update(ContentValues, String, String[])
	 * @see #delete(String, String[])
	 */
	@Nullable
	public Uri insert(@Nullable ContentValues values) {
		assertAttachedToDatabaseOrThrow("INSERT");
		return mOperationDispatcher.dispatchInsert(this, values);
	}

	/**
	 */
	@Nullable
	@Override
	public Uri dispatchInsert(@NonNull SQLiteDatabase db, @Nullable ContentValues values) {
		Uri rowUri = null;
		db.beginTransaction();
		try {
			rowUri = onInsert(db, values);
			db.setTransactionSuccessful();
		} catch (IllegalStateException e) {
			Log.e(TAG, "INSERT transaction has failed.", e);
		} finally {
			db.endTransaction();
			if (rowUri != null) {
				notifyContentChanged();
			}
		}
		return rowUri;
	}

	/**
	 * Invoked from {@link #dispatchInsert(SQLiteDatabase, ContentValues)} to insert the specified
	 * <var>values</var> into database table represented by this entity.
	 * <p>
	 * This implementation invokes {@link SQLiteDatabase#insert(String, String, ContentValues)}
	 * and creates the requested Uri from this entity's content uri and received row <var>id</var>,
	 * so the result is: <b>this.entity.content.uri/id</b>.
	 * <p>
	 * <b>Note, that this call is already wrapped into database transaction.</b>
	 */
	@Nullable
	protected Uri onInsert(@NonNull SQLiteDatabase db, @Nullable ContentValues values) {
		final long id = db.insert(mName, null, values);
		return id == -1 ? null : Uri.parse(mContentUri + "/" + Long.toString(id));
	}

	/**
	 * Returns the count of all rows stored within database table represented by this entity.
	 *
	 * @return Count of rows.
	 */
	public int count() {
		final Cursor cursor = query(new String[]{mPrimaryColumnName}, null, null, null);
		final int count = cursor.getCount();
		cursor.close();
		return count;
	}

	/**
	 * Returns an array with ids of all rows stored within database table represented by this entity.
	 *
	 * @return Ids array or empty {@code long[0]} array if there are no rows presented.
	 */
	@NonNull
	public long[] ids() {
		final Cursor cursor = query(new String[]{mPrimaryColumnName}, null, null, null);
		final long[] ids = new long[cursor.getCount()];
		if (cursor.moveToFirst()) {
			do {
				ids[cursor.getPosition()] = cursor.getLong(cursor.getColumnIndex(mPrimaryColumnName));
			} while (cursor.moveToNext());
		}
		cursor.close();
		return ids;
	}

	/**
	 * Checks whether a row with the specified <var>id</var> exists within database table represented
	 * by this entity.
	 *
	 * @param id Primary id of the desired row check.
	 * @return {@code True} if this entity's table contains row where value of the primary column
	 * ({@link #getPrimaryColumnName()}) matches the specified id, {@code false} otherwise.
	 * @see #exists(String, String[])
	 */
	public boolean exists(long id) {
		return exists(mPrimaryColumnName + "=?", new String[]{Long.toString(id)});
	}

	/**
	 * Checks whether a row targeted by the specified <var>selection</var> + <var>selectionArgs</var>
	 * exists within database table represented by this entity.
	 *
	 * @param selection     Selection (where) statement.
	 * @param selectionArgs Arguments for selection statement.
	 * @return {@code True} if this entity's table contains row matched by the specified selection,
	 * {@code false} otherwise.
	 */
	public boolean exists(@Nullable String selection, @Nullable String[] selectionArgs) {
		final Cursor cursor = query(new String[]{mPrimaryColumnName}, selection, selectionArgs, null);
		final boolean exists = cursor.getCount() > 0;
		cursor.close();
		return exists;
	}

	/**
	 * Same as {@link #query(String[], String, String[], String)} with all query parameters specified
	 * as {@code null}.
	 */
	@NonNull
	public Cursor queryAll() {
		return query(null, null, null, null);
	}

	/**
	 * Returns a new instance of Cursor for the specified <var>projection</var> for database table
	 * represented by this entity.
	 *
	 * @param projection    The desired projection for the query.
	 * @param selection     The desired selection for the query.
	 * @param selectionArgs The desired selection arguments for the query.
	 * @param sortOrder     The desired sort order for the query.
	 * @return Requested cursor or {@code null} if database transaction failed.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #insert(ContentValues)
	 * @see #update(ContentValues, String, String[])
	 * @see #delete(String, String[])
	 */
	@NonNull
	public Cursor query(@Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		assertAttachedToDatabaseOrThrow("QUERY");
		return mOperationDispatcher.dispatchQuery(this, projection, selection, selectionArgs, sortOrder);
	}

	/**
	 */
	@NonNull
	@Override
	public Cursor dispatchQuery(@NonNull SQLiteDatabase db, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		try {
			return onQuery(db, projection, selection, selectionArgs, sortOrder);
		} catch (IllegalStateException e) {
			Log.e(TAG, "Failed to query Cursor.", e);
		}
		return EmptyCursor.create();
	}

	/**
	 * Invoked from {@link #dispatchQuery(SQLiteDatabase, String[], String, String[], String)} to
	 * query a new instance of Cursor for the specified parameters.
	 * <p>
	 * This implementation also set ups notification uri for the queried cursor.
	 *
	 * @see #prepareQueryBuilder()
	 * @see #onSetUpNotificationUri(Cursor)
	 */
	@NonNull
	protected Cursor onQuery(@NonNull SQLiteDatabase db, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		final Cursor cursor = this.prepareQueryBuilder().query(db, projection, selection, selectionArgs, null, null, sortOrder);
		return onSetUpNotificationUri(EmptyCursor.createIfNull(cursor));
	}

	/**
	 * Prepares a new instance of SQLiteQueryBuilder for purpose of {@link #onQuery(SQLiteDatabase, String[], String, String[], String)}
	 * method.
	 *
	 * @return SQLiteQueryBuilder builder with this entity's name set as {@link SQLiteQueryBuilder#setTables(String)}.
	 */
	@NonNull
	protected SQLiteQueryBuilder prepareQueryBuilder() {
		final SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		builder.setTables(mName);
		return builder;
	}

	/**
	 * Invoked to set up notification Uri for the specified <var>cursor</var>.
	 *
	 * @param cursor Cursor for which to set up notification uri.
	 * @return The given cursor with notification Uri of this entity or {@code null} if the given
	 * cursor is invalid.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #setContentChangeNotificationEnabled(boolean)
	 */
	@NonNull
	protected Cursor onSetUpNotificationUri(@NonNull Cursor cursor) {
		assertAttachedToDatabaseOrThrow("SET UP NOTIFICATION URI");
		cursor.setNotificationUri(
				mDatabase.getContentResolver(),
				mNotificationUri == null ? mContentUri : mNotificationUri
		);
		return cursor;
	}

	/**
	 * Updates columns of a row targeted by the specified <var>selection</var> + <var>selectionArgs</var>
	 * with the given <var>mContentValues</var> stored within database table represented by this entity.
	 *
	 * @param values        Content values to update the desired row with.
	 * @param selection     Selection (where) statement.
	 * @param selectionArgs Arguments for selection statement.
	 * @return Count of rows affected by the operation {@link #NO_ROWS} if database transaction failed.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #insert(ContentValues)
	 * @see #query(String[], String, String[], String)
	 * @see #delete(String, String[])
	 */
	public int update(@Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
		assertAttachedToDatabaseOrThrow("UPDATE");
		return mOperationDispatcher.dispatchUpdate(this, values, selection, selectionArgs);
	}

	/**
	 */
	@Override
	public int dispatchUpdate(@NonNull SQLiteDatabase db, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
		int rows = 0;
		db.beginTransaction();
		try {
			rows = onUpdate(db, values, selection, selectionArgs);
			db.setTransactionSuccessful();
		} catch (IllegalStateException e) {
			Log.e(TAG, "UPDATE transaction has failed.", e);
			rows = NO_ROWS;
		} finally {
			db.endTransaction();
			if (rows > 0) {
				notifyContentChanged();
			}
		}
		return rows;
	}

	/**
	 * Invoked from {@link #dispatchUpdate(SQLiteDatabase, ContentValues, String, String[])} to update
	 * a row with the specified <var>values</var> within database table represented by this entity.
	 * <p>
	 * <b>Note, that this call is already wrapped into database transaction.</b>
	 *
	 * @return Count of rows affected by the operation.
	 */
	protected int onUpdate(@NonNull SQLiteDatabase db, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
		int updated = db.update(mName, values, selection, selectionArgs);
		final boolean autoInsertOnUpdate = (mPrivateFlags & PFLAG_AUTO_INSERT_ON_UPDATE) != 0;
		if (updated <= 0 && autoInsertOnUpdate && onInsert(db, values) != null) {
			updated = 1;
		}
		return updated;
	}

	/**
	 * Same as {@link #delete(String, String[])} with both parameters {@code null}.
	 * <p>
	 * This transaction is intended to be used when a whole content within database table represented
	 * by this entity need to be deleted. Executing this transaction will delete only content, table
	 * structure within database will be preserved.
	 *
	 * @return Count of rows affected by the operation.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 */
	public int dropContent() {
		return delete(null, null);
	}

	/**
	 * Deletes all rows with the specified <var>ids</var> stored within database table represented
	 * by this entity.
	 *
	 * @param ids Ids of the desired rows to delete.
	 * @return Count of rows affected by the operation {@link #NO_ROWS} if database transaction failed.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 */
	public int delete(@NonNull @Size(min = 1) long[] ids) {
		final IdSelection selection = new IdSelection(mPrimaryColumnName).withIds(ids);
		return delete(selection.build(), selection.buildArgs());
	}

	/**
	 * Deletes a row with the specified <var>id</var> stored within database table represented by
	 * this entity.
	 *
	 * @param id Primary id of the desired row to delete.
	 * @return Count of rows affected by the operation.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 */
	public int delete(long id) {
		return delete(mPrimaryColumnName + "=?", new String[]{Long.toString(id)});
	}

	/**
	 * Deletes a row targeted by the specified <var>selection</var> + <var>selectionArgs</var> stored
	 * within database table represented by this entity.
	 *
	 * @param selection     Selection (where) statement.
	 * @param selectionArgs Arguments for selection statement.
	 * @return Count of rows affected by the operation.
	 * @throws IllegalStateException If this entity is not attached to any database.
	 * @see #insert(ContentValues)
	 * @see #query(String[], String, String[], String)
	 * @see #update(ContentValues, String, String[])
	 */
	public int delete(@Nullable String selection, @Nullable String[] selectionArgs) {
		assertAttachedToDatabaseOrThrow("DELETE");
		return mOperationDispatcher.dispatchDelete(this, selection, selectionArgs);
	}

	/**
	 */
	@Override
	public int dispatchDelete(@NonNull SQLiteDatabase db, @Nullable String selection, @Nullable String[] selectionArgs) {
		int rows = 0;
		db.beginTransaction();
		try {
			rows = onDelete(db, selection, selectionArgs);
			db.setTransactionSuccessful();
		} catch (IllegalStateException e) {
			Log.e(TAG, "DELETE transaction has failed.", e);
			rows = NO_ROWS;
		} finally {
			db.endTransaction();
			if (rows > 0) {
				notifyContentChanged();
			}
		}
		return rows;
	}

	/**
	 * Invoked from {@link #dispatchDelete(SQLiteDatabase, String, String[])} to delete a row within
	 * database table represented by this entity.
	 * <p>
	 * <b>Note, that this call is already wrapped into database transaction.</b>
	 *
	 * @return Count of rows affected by the operation.
	 */
	protected int onDelete(@NonNull SQLiteDatabase db, @Nullable String selection, @Nullable String[] selectionArgs) {
		return db.delete(mName, selection, selectionArgs);
	}

	/**
	 * Resets value of the <b>seq</b> column in <b>sqlite_sequence</b> table for database table
	 * represented by this entity to {@code 0}.
	 * <p>
	 * Simply, this method resets counter for primary key of this entity's table (if auto-incremented).
	 * <p>
	 * <b>Note, that do not invoke this method unless you know exactly what consequences may follow
	 * as result of this action. To ensure data consistency for a particular entity table, this
	 * method should be invoked synchronously after {@link #dropContent()} method and only from ONE
	 * place in an Android application.</b>
	 */
	protected final boolean resetSequence() {
		assertAttachedToDatabaseOrThrow("RESET SEQUENCE");
		return mDatabase.executeQuery(new ResetSequenceQuery().table(mName));
	}

	/**
	 * Creates and returns a new instance of Lock that causes the content notifications to be disabled
	 * (locked) until all by this method returned locks are released via {@link Lock#release()}.
	 * <p>
	 * If there are any un-released content notification locks, {@link #isContentChangeNotificationEnabled()}
	 * method will return {@code false} regardless if {@link #setContentChangeNotificationEnabled(boolean)}
	 * has been the last time called with {@code true} parameter and so {@link #notifyContentChanged()},
	 * when invoked, will not fire content change notification. This may be useful if there need to
	 * be applied a set of content operations upon this entity and any registered content observers
	 * should be notified after this 'bulk' change, like in case of {@link #applyBatchOperations(ArrayList)}.
	 * <p>
	 * However, if content change notification is for some cases required to be fired regardless of
	 * disabled/locked content notifications, {@link #forceNotifyContentChanged()} should serve such
	 * purpose perfectly.
	 * <p>
	 * <b>Note that this is a synchronized operation.</b>
	 *
	 * @return New content notifications lock.
	 */
	@NonNull
	public final Lock lockContentNotifications() {
		final Lock lock = new ContentNotificationLock(this);
		synchronized (mContentNotificationLocks) {
			this.mContentNotificationLocks.add(lock);
		}
		return lock;
	}

	/**
	 * Releases/removes the specified <var>lock</var> from created content notification locks.
	 * <p>
	 * <b>Note that this is a synchronized operation.</b>
	 *
	 * @param lock The lock to be released/removed.
	 */
	final void releaseContentNotificationsLock(final Lock lock) {
		synchronized (mContentNotificationLocks) {
			this.mContentNotificationLocks.remove(lock);
		}
	}

	/**
	 * Notifies all registered ContentObservers that are listening for changes in content of database
	 * table represented by this entity that the content has been changed.
	 * <p>
	 * Change will be notified only if content change notifications are enabled for this entity and
	 * there are no active (not released) notification locks acquired via {@link #lockContentNotifications()}.
	 *
	 * @throws IllegalStateException If this entity is not attached to any database or content resolver
	 *                               obtained from the current attached context is invalid.
	 * @see #forceNotifyContentChanged()
	 * @see #setContentChangeNotificationEnabled(boolean)
	 * @see #setNotificationUri(Uri)
	 */
	public void notifyContentChanged() {
		if (isContentChangeNotificationEnabled()) forceNotifyContentChanged();
	}

	/**
	 * Like {@link #notifyContentChanged()} this method will also notify all registered ContentObservers
	 * about content change, but this method does not take into count enabled/disabled state of
	 * content notifications.
	 * <p>
	 * This method can be used when a caller wants to content change be always notified to the
	 * content observers.
	 *
	 * @throws IllegalStateException If this entity is not attached to any database or content resolver
	 *                               obtained from the current attached context is invalid.
	 */
	public void forceNotifyContentChanged() {
		assertAttachedToDatabaseOrThrow("NOTIFY CONTENT CHANGE");
		mDatabase.getContentResolver().notifyChange(
				mNotificationUri == null ? mContentUri : mNotificationUri,
				null
		);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A simple object used to lock a specific functionality of database {@link Entity}. The locked
	 * functionality will not be available (executable) until all created locks are released via
	 * {@link #release()}.
	 *
	 * @author Martin Albedinsky
	 */
	public static abstract class Lock {

		/**
		 * Entity for which is this lock created.
		 */
		private Entity entity;

		/**
		 * Boolean flag indicating whether this lock has been released or not.
		 */
		final AtomicBoolean released = new AtomicBoolean(false);

		/**
		 * Creates a new instance of Lock for the given <var>entity</var>.
		 *
		 * @param entity The entity for which is the new lock being created.
		 */
		Lock(final Entity entity) {
			this.entity = entity;
		}

		/**
		 * Releases this instance of lock for the associated entity.
		 *
		 * @throws IllegalStateException If this lock has been already released.
		 * @see #isReleased()
		 */
		public final void release() {
			if (released.get()) {
				throw new IllegalStateException("Lock has been already released!");
			}
			released.set(true);
			onRelease(entity);
			this.entity = null;
		}

		/**
		 * Invoked to release this lock.
		 *
		 * @param entity The entity for which has been this lock created.
		 */
		abstract void onRelease(final Entity entity);

		/**
		 * Checks whether this lock is already released or not.
		 *
		 * @return {@code True} of this lock has been already released, {@code false} otherwise.
		 */
		public final boolean isReleased() {
			return released.get();
		}
	}

	/**
	 * A {@link Lock} implementation used for locking of content notifications.
	 */
	private static final class ContentNotificationLock extends Lock {

		/**
		 * Creates a new instance of ContentNotificationLock for the given <var>entity</var>.
		 *
		 * @param entity The entity for which is the new lock being created.
		 */
		ContentNotificationLock(final Entity entity) {
			super(entity);
		}

		/**
		 */
		@Override
		void onRelease(final Entity entity) {
			entity.releaseContentNotificationsLock(this);
		}
	}

	/**
	 * A {@link BaseQuery} implementation that may be used to reset sequence column for a particular
	 * database table.
	 */
	private static final class ResetSequenceQuery extends BaseQuery<ResetSequenceQuery> {

		/**
		 */
		@Override
		public boolean isExecutable() {
			// We assume that the query is internally always properly configured.
			return true;
		}

		/**
		 */
		@NonNull
		@Override
		protected String onBuild() {
			return "UPDATE sqlite_sequence SET seq=0 WHERE name='" + table() + "'";
		}
	}
}
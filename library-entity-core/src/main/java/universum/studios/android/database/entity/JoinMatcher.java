/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;

/**
 * Simple helper used by {@link JoinEntity JoinEntities} to validate theirs JOIN statements.
 *
 * @author Martin Albedinsky
 *
 * @see JoinMatcher#PATTERN
 */
public final class JoinMatcher {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "JoinMatcher";

	/**
	 * Pattern that is used for validation of JOIN statements.
	 */
	public static final String PATTERN = "^([a-zA-Z0-9_]+)( (JOIN|NATURAL JOIN|CROSS JOIN|LEFT JOIN|LEFT OUTER JOIN) ([a-zA-Z0-9_]+) ON \\(([a-zA-Z0-9_\\.]+)=([a-zA-Z0-9_\\.]+)\\))+$";

	/**
	 * Size for the pool array containing matcher instances.
	 */
	private static final int POOL_SIZE = 2;

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Array containing already recycled ({@link #recycle()}) matcher instances that can be obtained
	 * via {@link #obtain()} and thus reused.
	 */
	private static final JoinMatcher[] POOL = new JoinMatcher[POOL_SIZE];

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Matcher that is used to validate JOIN statement passed via {@link #validateStatement(String)}.
	 */
	private final Matcher mMatcher;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of JoinMatcher with {@link Matcher} for compiled {@link #PATTERN}.
	 */
	private JoinMatcher() {
		this.mMatcher = Pattern.compile(PATTERN).matcher("");
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains a new JoinMatcher object (if available from the pool).
	 * <p>
	 * When you are done with the obtained matcher do not forget to recycle it via {@link #recycle()}.
	 *
	 * @return New or recycled JoinMatcher.
	 */
	@NonNull
	static JoinMatcher obtain() {
		synchronized (POOL) {
			JoinMatcher matcher;
			for (int i = 0; i < POOL_SIZE; i++) {
				matcher = POOL[i];
				if (matcher != null) {
					POOL[i] = null;
					return matcher;
				}
			}
		}
		return new JoinMatcher();
	}

	/**
	 * Validates the specified <var>joinStatement</var> against the {@link #PATTERN}.
	 *
	 * @param joinStatement The desired JOIN statement to validate.
	 * @return {@code True} if the specified join statement matches the requested pattern, {@code false}
	 * otherwise.
	 */
	boolean validateStatement(@NonNull String joinStatement) {
		return mMatcher.reset(joinStatement).matches();
	}

	/**
	 * Puts this matcher object back into the pool.
	 * <p>
	 * Do not touch this matcher after this call.
	 */
	void recycle() {
		this.mMatcher.reset("");
		synchronized (POOL) {
			for (int i = 0; i < POOL_SIZE; i++) {
				if (POOL[i] == null) {
					POOL[i] = this;
					return;
				}
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
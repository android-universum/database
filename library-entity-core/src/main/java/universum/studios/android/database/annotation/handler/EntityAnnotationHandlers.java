/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.annotation.EntityName;
import universum.studios.android.database.entity.Entity;

/**
 * An {@link AnnotationHandlers} implementation providing {@link AnnotationHandler} instances for
 * entity classes.
 *
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public class EntityAnnotationHandlers extends AnnotationHandlers {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	EntityAnnotationHandlers() {
		super();
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains an {@link EntityAnnotationHandler} implementation for the given <var>classOfEntity</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	public static EntityAnnotationHandler obtainEntityHandler(@NonNull Class<? extends Entity> classOfEntity) {
		return obtainHandler(EntityHandler.class, classOfEntity);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link EntityAnnotationHandler} implementation for {@link Entity} class.
	 */
	static class EntityHandler extends BaseAnnotationHandler implements EntityAnnotationHandler {

		/**
		 * Same as {@link #EntityHandler(Class, Class)} with {@link Entity} as <var>maxSuperClass</var>.
		 */
		public EntityHandler(@NonNull Class<?> annotatedClass) {
			this(annotatedClass, Entity.class);
		}

		/**
		 * Creates a new instance of EntityHandler for the specified <var>annotatedClass</var>.
		 *
		 * @see BaseAnnotationHandler#BaseAnnotationHandler(Class, Class)
		 */
		EntityHandler(Class<?> annotatedClass, Class<?> maxSuperClass) {
			super(annotatedClass, maxSuperClass);
		}

		/**
		 */
		@NonNull
		@Override
		public String getEntityName(@NonNull String defaultName) {
			final EntityName name = findAnnotation(EntityName.class);
			return name == null || TextUtils.isEmpty(name.value()) ? defaultName : name.value();
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.BaseTransaction;
import universum.studios.android.database.Database;
import universum.studios.android.database.DatabaseEntity;
import universum.studios.android.database.DatabaseProvider;
import universum.studios.android.database.DatabaseTransaction;

/**
 * A {@link BaseTransaction} implementation that can be used to centralize a specific entity transaction
 * logic (query, update, insert, delete, ..., or theirs combination) into one place to allow re-use
 * of such logic and also to lighten execution of such queries from within Android application.
 * <p>
 * A specific entity transaction can be executed via {@link #execute()}. Implementations of this
 * transaction class need to implement {@link #onExecute(Database, DatabaseEntity)} where will be
 * passed DatabaseEntity instance for the entity class specified during initialization along with
 * Database instance ({@link #EntityTransaction(Class, Class)}). If some exception
 * occurs during execution of entity transaction, then {@link #onError(Database, DatabaseEntity, Throwable)}
 * will be invoked with that exception.
 * <p>
 * Entity transaction can be also executed asynchronously via {@link #executeAsync()} in such case
 * {@link #onExecuteAsync(Database, DatabaseEntity)} will be invoked. By default, EntityTransaction
 * uses {@link AsyncTask} for asynchronous execution and default {@link #EXECUTOR} as executor. The
 * executor may be changed via {@link #setExecutor(java.util.concurrent.Executor)}.
 *
 * @param <E> Type of the entity upon which will be this transaction executing queries.
 * @param <D> Type of the database upon which can this transaction execute additional queries if
 *            needed.
 * @author Martin Albedinsky
 * @see DatabaseTransaction
 */
public abstract class EntityTransaction<R, E extends DatabaseEntity, D extends Database> extends BaseTransaction<R> {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "EntityTransaction";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Class of the entity upon which will be this transaction executing its specific queries.
	 */
	private final Class<E> mEntityClass;

	/**
	 * Class of the database upon which can this transaction execute additional queries.
	 */
	private final Class<D> mDatabaseClass;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DatabaseEntityTransaction for the specified <var>classOfEntity</var>.
	 *
	 * @param classOfEntity   Class of the entity upon which will be the new transaction executing queries.
	 * @param classOfDatabase Class of the database to which is the entity assigned. When executing
	 *                        the new query, instance of that database can be used to execute some
	 *                        additional queries if needed.
	 */
	public EntityTransaction(@NonNull Class<E> classOfEntity, @NonNull Class<D> classOfDatabase) {
		super();
		this.mEntityClass = classOfEntity;
		this.mDatabaseClass = classOfDatabase;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	public void executeAsync() {
		final D database = DatabaseProvider.provideDatabase(mDatabaseClass);
		onExecuteAsync(database, database.findEntityByClass(mEntityClass));
	}

	/**
	 * Like {@link #onExecute(Database, DatabaseEntity)} but this is invoked due to call to {@link #executeAsync()}.
	 * <p>
	 * By default this will execute asynchronous task to perform execution of {@link #onExecute(Database, DatabaseEntity)}
	 * asynchronously. Inheritance instances may override this method to perform custom asynchronous
	 * execution.
	 * <p>
	 * If {@link #onExecute(Database, DatabaseEntity)} finishes successfully {@link #onExecutedAsync(Database, DatabaseEntity, Object)}
	 * is invoked, otherwise {@link #onError(Database, DatabaseEntity, Throwable)}.
	 */
	protected void onExecuteAsync(@NonNull D database, @NonNull E entity) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			new AsyncTransactionTask<>(this, database, entity).executeOnExecutor(getExecutor());
		} else {
			new AsyncTransactionTask<>(this, database, entity).execute();
		}
	}

	/**
	 * Invoked after {@link #onExecute(Database, DatabaseEntity)} has finished asynchronously after
	 * call to {@link #onExecuteAsync(Database, DatabaseEntity)}.
	 * <p>
	 * <b>Note</b>, that this is always invoked on the UI thread.
	 *
	 * @param database The database instance specific for this transaction.
	 * @param entity   The database entity instance specific for this transaction.
	 * @param result   Result returned via {@link #onExecute(Database, DatabaseEntity)} or via
	 *                 {@link #onError(Database, DatabaseEntity, Throwable)}.
	 */
	protected void onExecutedAsync(@NonNull D database, @NonNull E entity, @Nullable R result) {
		// Inheritance hierarchies may perform here logic related to finished asynchronous execution.
	}

	/**
	 * @see #onSuccess(Database, DatabaseEntity, Object)
	 * @see #onError(Database, DatabaseEntity, Throwable)
	 */
	@Nullable
	@Override
	protected R onExecute() {
		final D database = DatabaseProvider.provideDatabase(mDatabaseClass);
		final E entity = database.findEntityByClass(mEntityClass);
		final int mode = getMode();
		if (mode != NONE) {
			database.beginTransaction(mode);
		}
		R result;
		boolean success = true;
		try {
			result = onExecute(database, entity);
			if (mode != NONE) {
				database.setTransactionSuccessful();
			}
		} catch (Exception error) {
			success = false;
			result = onError(database, entity, error);
		} finally {
			if (mode != NONE) {
				database.endTransaction();
			}
		}
		if (success) {
			onSuccess(database, entity, result);
		}
		return result;
	}

	/**
	 * Invoked from {@link #execute()} to perform execution of database queries specific for this
	 * transaction.
	 * <p>
	 * <b>Note</b>, that this will be invoked on the same thread as {@link #execute()} or on a background
	 * thread if {@link #executeAsync()} has been invoked.
	 *
	 * @param database The database instance to which is the entity assigned. This database can be
	 *                 used to execute some additional queries or to obtain other entities that are
	 *                 assigned to it if needed for purpose of this transaction.
	 * @param entity   The entity instance upon which to execute queries.
	 * @return Result specific for this transaction when execution is successful. Can be {@code null}.
	 */
	@Nullable
	protected abstract R onExecute(@NonNull D database, @NonNull E entity);

	/**
	 * Invoked if {@link #onExecute()} has finished successfully.
	 *
	 * @param database The database instance specific for this transaction.
	 * @param entity   The database entity instance specific for this transaction.
	 * @param result   Result returned by {@link #onExecute(Database, DatabaseEntity)}.
	 * @see #onError(Database, DatabaseEntity, Throwable)
	 */
	protected void onSuccess(@NonNull D database, @NonNull E entity, @Nullable R result) {
		// Inheritance hierarchies may perform here logic related to successful execution.
	}

	/**
	 * Invoked if {@link #onExecute(Database, DatabaseEntity)} throws some exception during execution.
	 * <p>
	 * <b>Note</b>, that in case of asynchronous execution via {@link #onExecuteAsync(Database, DatabaseEntity)}
	 * is this method always invoked on the UI thread and the result is passed to {@link #onExecutedAsync(Database, DatabaseEntity, Object)}.
	 *
	 * @param database The database instance specific for this transaction.
	 * @param entity   The database entity instance specific for this transaction.
	 * @param error    The error thrown when performing execution of this transaction.
	 * @return Result specific for this transaction when some error occurs. Can be {@code null}.
	 * @see #onSuccess(Database, DatabaseEntity, Object)
	 */
	@Nullable
	protected R onError(@NonNull D database, @NonNull E entity, @NonNull Throwable error) {
		final String databaseName = database.getClass().getSimpleName();
		final String entityName = entity.getClass().getSimpleName();
		final String transactionName = getClass().getSimpleName();
		Log.e(databaseName + "#" + entityName + "#" + transactionName, error.getMessage(), error);
		return null;
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * An {@link AsyncTask} implementation used to asynchronously execute DatabaseEntityTransaction.
	 *
	 * @see #onExecuteAsync(Database, DatabaseEntity)
	 */
	private static final class AsyncTransactionTask<R, E extends DatabaseEntity, D extends Database> extends AsyncTask<Void, Void, R> {

		/**
		 * Transaction to be executed.
		 */
		final EntityTransaction<R, E, D> transaction;

		/**
		 * Database instance to which is {@link #entity} assigned.
		 */
		private final D database;

		/**
		 * Database entity instance to be used for desired queries execution.
		 */
		private final E entity;

		/**
		 * Creates a new instance of AsyncTransactionTask for the specified <var>database</var> and
		 * <var>transaction</var>.
		 *
		 * @param transaction Transaction to be executed.
		 * @param database    The database to which is the entity assigned. Can be used for additional
		 *                    queries execution.
		 * @param entity      The database entity to be used for queries execution.
		 */
		AsyncTransactionTask(EntityTransaction<R, E, D> transaction, D database, E entity) {
			super();
			this.entity = entity;
			this.database = database;
			this.transaction = transaction;
		}

		/**
		 */
		@Override
		protected R doInBackground(Void... params) {
			return transaction.execute();
		}

		/**
		 */
		@Override
		protected void onPostExecute(@Nullable R result) {
			transaction.onExecutedAsync(database, entity, result);
		}
	}
}
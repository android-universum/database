/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.EntityName;
import universum.studios.android.database.entity.Entity;

/**
 * An {@link AnnotationHandler} extended interface for annotation handlers from the Database library
 * that are used to handle processing of annotations attached to classes derived from the <b>Entity</b>
 * class provided by this library.
 *
 * @author Martin Albedinsky
 * @see Entity
 */
public interface EntityAnnotationHandler extends AnnotationHandler {

	/**
	 * Returns the name obtained from {@link EntityName @EntityName} annotation (if presented).
	 *
	 * @param defaultName Default entity name to be returned if there is no annotation presented.
	 * @return Via annotation specified entity name or <var>defaultName</var>.
	 */
	@NonNull
	String getEntityName(@NonNull String defaultName);
}
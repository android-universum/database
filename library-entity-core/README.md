Database-Entity-Core
===============

This module contains basic `DatabaseEntity` implementations that may be used to represent **database tables**
in `SQLite` database.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-entity-core:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-base](https://bitbucket.org/android-universum/database/src/main/library-base),
[database-query](https://bitbucket.org/android-universum/database/src/main/library-query),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Entity](https://bitbucket.org/android-universum/database/src/main/library-entity-core/src/main/java/universum/studios/android/database/entity/Entity.java)
- [JoinEntity](https://bitbucket.org/android-universum/database/src/main/library-entity-core/src/main/java/universum/studios/android/database/entity/JoinEntity.java)
- [EntityTransaction](https://bitbucket.org/android-universum/database/src/main/library-entity-core/src/main/java/universum/studios/android/database/entity/EntityTransaction.java)
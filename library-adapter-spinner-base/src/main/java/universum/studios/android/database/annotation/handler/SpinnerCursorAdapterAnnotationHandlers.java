/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.adapter.BaseSpinnerCursorAdapter;
import universum.studios.android.database.annotation.CursorDropDownView;

/**
 * A {@link CursorAdapterAnnotationHandlers} implementation providing {@link AnnotationHandler} instances
 * for <b>SpinnerCursorAdapter</b> like classes.
 *
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public final class SpinnerCursorAdapterAnnotationHandlers extends CursorAdapterAnnotationHandlers {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private SpinnerCursorAdapterAnnotationHandlers() {
		super();
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains a {@link SpinnerCursorAdapterAnnotationHandler} implementation for the given <var>classOfAdapter</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	public static SpinnerCursorAdapterAnnotationHandler obtainSpinnerCursorAdapterHandler(@NonNull Class<?> classOfAdapter) {
		return obtainHandler(SpinnerAdapterHandler.class, classOfAdapter);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link SpinnerCursorAdapterAnnotationHandler} implementation for {@link BaseSpinnerCursorAdapter}
	 * like adapters.
	 */
	@SuppressWarnings("WeakerAccess") static class SpinnerAdapterHandler extends AdapterHandler implements SpinnerCursorAdapterAnnotationHandler {

		/**
		 * Layout resource of the related adapter's drop down view obtained from the annotated class.
		 */
		private final int dropDownViewRes;

		/**
		 * Same as {@link #SpinnerAdapterHandler(Class, Class)} with {@link BaseSpinnerCursorAdapter}
		 * as <var>maxSuperClass</var>.
		 */
		public SpinnerAdapterHandler(@NonNull Class<?> annotatedClass) {
			this(annotatedClass, BaseSpinnerCursorAdapter.class);
		}

		/**
		 * Creates a new instance of SpinnerAdapterHandler for the specified <var>annotatedClass</var>.
		 *
		 * @see AdapterHandler#AdapterHandler(Class, Class)
		 */
		SpinnerAdapterHandler(Class<?> annotatedClass, Class<?> maxSuperClass) {
			super(annotatedClass, maxSuperClass);
			final CursorDropDownView dropDownView = findAnnotationRecursive(CursorDropDownView.class);
			this.dropDownViewRes = dropDownView == null ? NO_VIEW_RES : dropDownView.value();
		}

		/**
		 */
		@Override
		public int getDropDownViewRes(int defaultViewRes) {
			return dropDownViewRes == NO_VIEW_RES ? defaultViewRes : dropDownViewRes;
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.CursorDropDownView;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.handler.CursorAdapterAnnotationHandler;
import universum.studios.android.database.annotation.handler.SpinnerCursorAdapterAnnotationHandler;
import universum.studios.android.database.annotation.handler.SpinnerCursorAdapterAnnotationHandlers;

/**
 * A {@link BaseCursorAdapter} implementation that may be used to provide optimized adapter also for
 * {@link android.widget.Spinner Spinner} widget. This "spinner based" adapter also supports API to
 * store current selected item and its corresponding position. The position of the selected item is
 * stored whenever {@link #getView(int, View, ViewGroup)} is called. The selected item position may
 * be obtained via {@link #getSelectedItemPosition()}.
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link universum.studios.android.database.annotation.CursorDropDownView @CursorDropDownView} <b>[class - inherited]</b>
 * <p>
 * If this annotation is presented, a resource id provided via this annotation will be used to inflate
 * the desired drop down view in {@link #onCreateDropDownView(ViewGroup, int)}.
 * </li>
 * <li>{@link BaseCursorAdapter + super annotations}</li>
 * </ul>
 *
 * @param <VH>  Type of the view holder used within a subclass of this BaseCursorSpinnerAdapter.
 * @param <DVH> Type of the drop down view holder used within a subclass of this BaseCursorSpinnerAdapter.
 * @author Martin Albedinsky
 */
public abstract class BaseSpinnerCursorAdapter<C extends Cursor, I, VH, DVH> extends BaseCursorAdapter<C, I, VH> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseSpinnerCursorAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Currently selected position.
	 */
	private int mSelectedPosition = NO_POSITION;

	/**
	 * Id of the currently selected position.
	 */
	private long mSelectedId = NO_ID;

	/**
	 * Resource id of the view which should be inflated as drop down view.
	 */
	private int mDropDownViewRes = CursorAdapterAnnotationHandler.NO_VIEW_RES;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #BaseSpinnerCursorAdapter(Context, Cursor)} with {@code null} initial cursor.
	 */
	public BaseSpinnerCursorAdapter(@NonNull Context context) {
		this(context, null);
	}

	/**
	 * Creates a new instance of BaseCursorSpinnerAdapter with the given initial {@code cursor} data
	 * set.
	 * <p>
	 * If {@link CursorDropDownView @CursorDropDownView} annotation is presented above a subclass of
	 * this BaseCursorSpinnerAdapter, it will be processed here.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param cursor  Initial cursor data set for this adapter. Can be {@code null}.
	 */
	public BaseSpinnerCursorAdapter(@NonNull Context context, @Nullable C cursor) {
		super(context, cursor);
		if (mAnnotationHandler != null) {
			this.mDropDownViewRes = ((SpinnerCursorAdapterAnnotationHandler) mAnnotationHandler).getDropDownViewRes(mDropDownViewRes);
		}
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	CursorAdapterAnnotationHandler onCreateAnnotationHandler() {
		return SpinnerCursorAdapterAnnotationHandlers.obtainSpinnerCursorAdapterHandler(getClass());
	}

	/**
	 */
	@NonNull
	@Override
	protected SpinnerCursorAdapterAnnotationHandler getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return (SpinnerCursorAdapterAnnotationHandler) mAnnotationHandler;
	}

	/**
	 */
	@Override
	protected boolean onCursorChange(@Nullable C newCursor, @Nullable C oldCursor) {
		if (newCursor == null || newCursor.getCount() == 0 || !newCursor.moveToFirst()) {
			this.mSelectedPosition = NO_POSITION;
			this.mSelectedId = NO_ID;
		} else if (mSelectedId == NO_ID) {
			this.mSelectedPosition = 0;
			this.mSelectedId = newCursor.getLong(mDataSet.getIdColumnIndex());
		} else if (mSelectedPosition == NO_POSITION) {
			this.mSelectedPosition = findItemPosition(newCursor, mSelectedId);
		} else {
			// Ensure that the new cursor has item with the current selected id, if not selected
			// the first one as default.
			final int selectedPosition = findItemPosition(newCursor, mSelectedId);
			if (selectedPosition == NO_POSITION) {
				this.mSelectedPosition = 0;
				newCursor.moveToFirst();
				this.mSelectedId = newCursor.getLong(mDataSet.getIdColumnIndex());
			}
		}
		// Let super to handle data set change and notify the registered observers.
		return false;
	}

	/**
	 * Returns the item model of this adapter for the current selected position.
	 *
	 * @return Item obtained via {@link #getItem(int)} for the current selected position.
	 * @see #getSelectedItemPosition()
	 * @see #getSelectedItemId()
	 */
	@NonNull
	public I getSelectedItem() {
		return getItem(mSelectedPosition);
	}

	/**
	 * Returns the position of the currently selected item.
	 *
	 * @return Position of item that is currently selected within a related Spinner widget or
	 * {@link #NO_POSITION} if this adapter does not have its data set specified.
	 * @see #getSelectedItem()
	 * @see #getSelectedItemId()
	 */
	public int getSelectedItemPosition() {
		return mSelectedPosition;
	}

	/**
	 * Sets the current selected position to the position of an item from the current data set with
	 * the specified <var>id</var>.
	 *
	 * @param id Id of the desired item to determine which position to select.
	 * @return {@code True} if the current selected id has been changed, {@code false} otherwise.
	 * @see #getSelectedItemId()
	 */
	public boolean setSelectedItemId(long id) {
		if (mSelectedId == id) {
			return false;
		}
		this.mSelectedId = id;
		this.mSelectedPosition = findItemPosition(getCursor(), id);
		notifyCursorDataSetChanged();
		return true;
	}

	/**
	 * Finds a position of an item with the specified <var>itemId</var> within the given cursor
	 * data set.
	 *
	 * @param cursor Cursor in which to find the item's position with the requested id.
	 * @param itemId Id of the item of which position to find.
	 * @return Item's position within the current data set or {@link #NO_POSITION} if such item was
	 * not found.
	 */
	private int findItemPosition(C cursor, long itemId) {
		if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
			final int idColumnIndex = mDataSet.getIdColumnIndex();
			do {
				if (itemId == cursor.getLong(idColumnIndex)) {
					return cursor.getPosition();
				}
			} while (cursor.moveToNext());
		}
		return NO_POSITION;
	}

	/**
	 * Returns the id of the currently selected item.
	 *
	 * @return Id of item that is currently selected within a related Spinner widget or {@link #NO_ID}
	 * if this adapter does not have its data set specified yet.
	 * @see #getSelectedItemPosition()
	 * @see #getSelectedItem()
	 */
	public long getSelectedItemId() {
		return mSelectedId;
	}

	/**
	 */
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		this.mSelectedPosition = position;
		this.mSelectedId = getItemId(position);
		return super.getView(position, convertView, parent);
	}

	/**
	 */
	@Override
	@SuppressWarnings("unchecked")
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		Object viewHolder;
		this.mCurrentViewType = getItemViewType(position);
		if (view == null) {
			view = onCreateDropDownView(parent, position);
			final Object holder = onCreateDropDownViewHolder(view, position);
			if (holder == null) {
				viewHolder = view;
			} else {
				view.setTag(viewHolder = holder);
			}
		} else {
			final Object holder = view.getTag();
			viewHolder = holder == null ? view : holder;
		}
		ensureViewHolderPosition(viewHolder, position);
		onBindDropDownViewHolder((DVH) viewHolder, position);
		return view;
	}

	/**
	 * Invoked to create a drop down view for an item from the current data set at the specified
	 * <var>position</var>.
	 * <p>
	 * This is invoked only if <var>convertView</var> for the specified <var>position</var> in
	 * {@link #getDropDownView(int, View, ViewGroup)} was {@code null}.
	 * <p>
	 * <b>Note</b>, that if {@link universum.studios.android.database.annotation.CursorDropDownView @CursorDropDownView}
	 * annotation is presented, a resource id provided via this annotation will be used to inflate
	 * the requested view, otherwise {@link #onCreateView(ViewGroup, int)} will be called to provide
	 * the drop down view.
	 *
	 * @param parent   A parent view, to resolve correct layout params for the newly creating view.
	 * @param position Position of the item from the current data set for which should be a new drop
	 *                 down view created.
	 * @return New instance of the requested drop down view.
	 * @see #inflate(int, ViewGroup)
	 */
	@NonNull
	protected View onCreateDropDownView(@NonNull ViewGroup parent, int position) {
		return mDropDownViewRes == CursorAdapterAnnotationHandler.NO_VIEW_RES ?
				onCreateView(parent, position) :
				inflate(mDropDownViewRes, parent);
	}

	/**
	 * Inflates a drop down view for item of this adapter via {@link #inflate(int, ViewGroup)} using
	 * layout resource specified via {@link CursorDropDownView @CursorDropDownView} annotation.
	 *
	 * @param parent A parent view, to resolve correct layout params for the newly creating view.
	 * @return The root view of the inflated drop down view hierarchy.
	 * @throws DatabaseException If there was no @CursorDropDownView annotation specified.
	 */
	@NonNull
	protected View inflateDropDownView(@NonNull ViewGroup parent) {
		if (mDropDownViewRes == CursorAdapterAnnotationHandler.NO_VIEW_RES) {
			throw DatabaseException.missingClassAnnotation(
					CursorDropDownView.class,
					getClass(),
					"Cannot inflate drop down view."
			);
		}
		return inflate(mDropDownViewRes, parent);
	}

	/**
	 * Invoked to create a view holder for a drop down view of an item from the current data set at
	 * the specified <var>position</var>.
	 * <p>
	 * This is invoked only if <var>convertView</var> for the specified <var>position</var> in
	 * {@link #getDropDownView(int, View, ViewGroup)} was {@code null}, so drop down view along with
	 * its corresponding holder need to be created.
	 *
	 * @param itemView The same view as obtained from {@link #onCreateDropDownView(ViewGroup, int)}
	 *                 for the specified position.
	 * @param position Position of the item from the current data set for which should be a new drop
	 *                 down view holder created.
	 * @return New instance of the requested view holder or {@code null} if holder for the drop
	 * down view is not required.
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	protected DVH onCreateDropDownViewHolder(@NonNull View itemView, int position) {
		return (DVH) onCreateViewHolder(itemView, position);
	}

	/**
	 * This implementation by default invokes {@link #onUpdateViewHolder(Object, Cursor, int)} with
	 * attached cursor moved to the current selected position.
	 *
	 * @see #getSelectedItemPosition()
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void onBindViewHolder(@NonNull VH viewHolder, @NonNull C cursor, int position) {
		final C selectedCursor = getCursorAt(mSelectedPosition);
		if (selectedCursor != null) onUpdateViewHolder((DVH) viewHolder, selectedCursor, position);
	}

	/**
	 * Invoked to configure and bind a drop down view of an item from the current data set at the
	 * specified <var>position</var>. This is invoked whenever {@link #getDropDownView(int, View, ViewGroup)}
	 * is called.
	 * <p>
	 * <b>Note</b>, that if {@link #onCreateDropDownViewHolder(View, int)} returns {@code null} for
	 * the specified <var>position</var> here passed <var>viewHolder</var> will be the view created
	 * by {@link #onCreateDropDownView(ViewGroup, int)} for the specified position or just recycled
	 * view for that position. This approach may be used when a view hierarchy of a specific item is
	 * represented by single custom view, where such view represents a holder for all its child views.
	 *
	 * @param viewHolder The same holder as provided by {@link #onCreateDropDownViewHolder(View, int)}
	 *                   for the specified position or converted view as holder as described above.
	 * @param position   Position of the item from the current data set of which view to bind with data.
	 * @see #onBindDropDownViewHolder(Object, Cursor, int)
	 */
	protected void onBindDropDownViewHolder(@NonNull DVH viewHolder, int position) {
		final C cursor = mDataSet.getCursor();
		if (cursor == null || cursor.isClosed()) {
			throw new IllegalStateException("Cannot present data from the invalid cursor.");
		}
		final int correctedPosition = correctCursorPosition(position);
		if (cursor.moveToPosition(correctedPosition)) {
			onBindDropDownViewHolder(viewHolder, cursor, position);
		} else {
			throw new IllegalStateException(
					"Cannot move the attached cursor to the position(" + position + ") corrected as(" + correctedPosition + ")."
			);
		}
	}

	/**
	 * Invoked to configure and bind a drop down view of the specified <var>viewHolder</var> with data
	 * of the current <var>cursor</var>. This is invoked whenever {@link #onBindDropDownViewHolder(Object, int)}
	 * is called for the current iterated position.
	 *
	 * @param viewHolder The same holder as provided by {@link #onCreateDropDownViewHolder(View, int)}
	 *                   for the specified position or converted view as holder as described above.
	 * @param cursor     Attached cursor already moved to the current iterated position.
	 * @param position   Position of the item from the current data set of which drop down view to
	 *                   bind with data.
	 */
	protected void onBindDropDownViewHolder(@NonNull DVH viewHolder, @NonNull C cursor, int position) {
		onUpdateViewHolder(viewHolder, cursor, position);
	}

	/**
	 * Invoked to update views of the given <var>viewHolder</var> with data of the given <var>cursor</var>.
	 *
	 * @param viewHolder The view holder passed either to {@link #onBindViewHolder(Object, Cursor, int)}
	 *                   or to {@link #onBindDropDownViewHolder(Object, Cursor, int)}.
	 * @param cursor     Attached cursor already moved to the specified position.
	 * @param position   Position of the item from the current data set of which view holder to update.
	 */
	protected void onUpdateViewHolder(@NonNull DVH viewHolder, @NonNull C cursor, int position) {
		// Inheritance hierarchies may perform here update logic of the view holder.
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
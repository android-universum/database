Database-Adapter-Spinner-Base
===============

This module contains base implementation of **cursor adapter** for `Spinner` widget.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-spinner-base:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-adapter-core](https://bitbucket.org/android-universum/database/src/main/library-adapter-core),
[database-adapter-list-base](https://bitbucket.org/android-universum/database/src/main/library-adapter-list-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseSpinnerCursorAdapter](https://bitbucket.org/android-universum/database/src/main/library-adapter-spinner-base/src/main/java/universum/studios/android/database/adapter/BaseSpinnerCursorAdapter.java)
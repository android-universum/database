Database-Adapter-List-Base
===============

This module contains base implementation of **cursor adapter** for `AdapterView` widget.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-list-base:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-adapter-core](https://bitbucket.org/android-universum/database/src/main/library-adapter-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseCursorAdapter](https://bitbucket.org/android-universum/database/src/main/library-adapter-list-base/src/main/java/universum/studios/android/database/adapter/BaseCursorAdapter.java)
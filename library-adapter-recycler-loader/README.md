Database-Adapter-Recycler-Loader
===============

This module contains extended implementation of `BaseRecyclerCursorAdapter` that uses `Loader` to 
load its `Cursor` data set.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-recycler-loader:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-content-loader](https://bitbucket.org/android-universum/database/src/main/library-content-loader),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-adapter-core](https://bitbucket.org/android-universum/database/src/main/library-adapter-core),
[database-adapter-loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-loader),
[database-adapter-recycler-base](https://bitbucket.org/android-universum/database/src/main/library-adapter-recycler-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [RecyclerCursorLoaderAdapter](https://bitbucket.org/android-universum/database/src/main/library-adapter-recycler-loader/src/main/java/universum/studios/android/database/adapter/RecyclerCursorLoaderAdapter.java)
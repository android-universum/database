/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Parcelable;
import android.util.AndroidRuntimeException;
import android.view.AbsSavedState;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.CursorItemView;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.handler.CursorAdapterAnnotationHandler;
import universum.studios.android.database.annotation.handler.CursorAdapterAnnotationHandlers;

/**
 * An {@link RecyclerView.Adapter RecyclerView.Adapter} implementation that can present its data set
 * from an attached {@link Cursor}.
 * <p>
 * This adapter implementation provides same functionality (related to management of the Cursor data
 * set) as {@code BaseCursorAdapter} like {@link #changeCursor(Cursor)} or {@link #swapCursor(Cursor)},
 * and also expects that its cursor is automatically managed by an instance of {@link android.app.LoaderManager LoaderManager}
 * by which has been such cursor loaded.
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link CursorItemView @CursorItemView} <b>[class - inherited]</b>
 * <p>
 * If this annotation is presented, a resource id provided via this annotation will be used to inflate
 * the desired view in {@link #onCreateViewHolder(ViewGroup, int)} and passed to the default {@link RecyclerView.ViewHolder}
 * implementation.
 * </li>
 * </ul>
 *
 * @param <C>  Type of the cursor of which data will be presented by a subclass of this BaseRecyclerCursorAdapter.
 * @param <I>  Type of the item model that can represent data for a single row of the specified
 *             cursor type.
 * @param <VH> Type of the view holder used within a subclass of this BaseRecyclerCursorAdapter.
 * @author Martin Albedinsky
 */
public abstract class BaseRecyclerCursorAdapter<C extends Cursor, I, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> implements CursorDataSetAdapter<C, I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseRecyclerCursorAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Context in which will be this adapter used.
	 */
	@NonNull
	protected final Context mContext;

	/**
	 * Layout inflater used to inflate new views for this adapter.
	 */
	@NonNull
	protected final LayoutInflater mLayoutInflater;

	/**
	 * Application resources that may be used to obtain strings, texts, drawables, ... and other resources.
	 */
	@NonNull
	protected final Resources mResources;

	/**
	 * Handler responsible for processing of all annotations of this class and also for handling all
	 * annotations related operations for this class.
	 */
	final CursorAdapterAnnotationHandler mAnnotationHandler;

	/**
	 * Data set handling cursor specified for this adapter.
	 */
	private final CursorAdapterDataSet<BaseRecyclerCursorAdapter<C, I, VH>, C, I> mDataSet;

	/**
	 * Resource id of the view which should be inflated as item view.
	 */
	private int mItemViewRes = CursorAdapterAnnotationHandler.NO_VIEW_RES;

	/**
	 * Cursor passed to {@link #swapCursor(Cursor)} of this adapter. This cursor is used to check
	 * whether a new cursor passed to {@link #swapCursor(Cursor)} is really a new instance or the
	 * same instance as previously specified.
	 */
	private Cursor mOriginalCursor;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #BaseRecyclerCursorAdapter(Context, Cursor)} with {@code null} initial cursor.
	 */
	public BaseRecyclerCursorAdapter(@NonNull Context context) {
		this(context, null);
	}

	/**
	 * Creates a new instance of BaseRecyclerCursorAdapter with the given initial {@code cursor} data set.
	 * <p>
	 * If {@link CursorItemView @CursorItemView} annotation is presented above a subclass of this
	 * BaseRecyclerCursorAdapter, it will be processed here.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param cursor  Initial cursor data set for this adapter. May be {@code null}.
	 */
	public BaseRecyclerCursorAdapter(@NonNull Context context, @Nullable C cursor) {
		super();
		this.mContext = context;
		this.mLayoutInflater = LayoutInflater.from(context);
		this.mResources = context.getResources();
		this.mAnnotationHandler = onCreateAnnotationHandler();
		if (mAnnotationHandler != null) {
			this.mItemViewRes = mAnnotationHandler.getItemViewRes(mItemViewRes);
		}
		this.mDataSet = new CursorAdapterDataSet<>(this);
		this.mDataSet.attachCursor(cursor);
		this.mOriginalCursor = cursor;
		setHasStableIds(true);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Invoked to create annotations handler for this instance.
	 *
	 * @return Annotations handler specific for this class.
	 */
	CursorAdapterAnnotationHandler onCreateAnnotationHandler() {
		return CursorAdapterAnnotationHandlers.obtainCursorAdapterHandler(getClass());
	}

	/**
	 * Returns handler that is responsible for annotations processing of this class and also for
	 * handling all annotations related operations for this class.
	 *
	 * @return Annotations handler specific for this class.
	 * @throws DatabaseException If annotations processing is not enabled for the Database library.
	 */
	@NonNull
	protected CursorAdapterAnnotationHandler getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return mAnnotationHandler;
	}

	/**
	 */
	@Override
	public void registerOnCursorChangeListener(@NonNull OnCursorChangeListener listener) {
		mDataSet.registerOnCursorChangeListener(listener);
	}

	/**
	 */
	@Override
	public void unregisterOnCursorChangeListener(@NonNull OnCursorChangeListener listener) {
		mDataSet.unregisterOnCursorChangeListener(listener);
	}

	/**
	 */
	@Override
	public void registerOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener) {
		mDataSet.registerOnCursorDataSetListener(listener);
	}

	/**
	 * Notifies all registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that the data
	 * set of this adapter has changed.
	 * <p>
	 * This method also propagates this call to {@link #notifyDataSetChanged()}.
	 */
	public void notifyCursorDataSetChanged() {
		notifyDataSetChanged();
		mDataSet.notifyCursorDataSetChanged();
	}

	/**
	 * Notifies all registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that the data
	 * set of this adapter has been just loaded.
	 * <p>
	 * Unlike {@link #notifyCursorDataSetChanged()} this method may be used to notify data set change
	 * to target directly data loading event not just inner data set change.
	 *
	 * @param loaderId Id of the loader that loaded the data set.
	 */
	public void notifyCursorDataSetLoaded(int loaderId) {
		mDataSet.notifyCursorDataSetLoaded(loaderId);
	}

	/**
	 * Notifies all registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that the data
	 * set of this adapter has been invalidated.
	 * <p>
	 * <b>Note that this method also invokes {@link #notifyDataSetChanged()} to ensure consistent
	 * knowledge about data state by the associated {@link RecyclerView} widget to which is this
	 * adapter attached.</b>
	 */
	public void notifyCursorDataSetInvalidated() {
		notifyDataSetChanged();
		mDataSet.notifyCursorDataSetInvalidated();
	}

	/**
	 */
	@Override
	public void unregisterOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener) {
		mDataSet.unregisterOnCursorDataSetListener(listener);
	}

	/**
	 */
	@Override
	public void registerOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener) {
		mDataSet.registerOnCursorDataSetActionListener(listener);
	}

	/**
	 * Notifies that the given <var>action</var> has been performed for the specified <var>position</var>.
	 * <p>
	 * If {@link #onCursorDataSetActionSelected(int, int, Object)} will not process this call, the
	 * registered {@link OnCursorDataSetActionListener OnCursorDataSetActionListeners} will be notified.
	 * <p>
	 * <b>Note, that invoking this method with 'invalid' position, out of bounds of the current
	 * data set, will be ignored.</b>
	 *
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled internally by this adapter or by one of
	 * the registers listeners, {@code false} otherwise.
	 */
	protected boolean notifyCursorDataSetActionSelected(int action, int position, @Nullable Object payload) {
		// Do not notify actions for invalid (out of bounds of the current data set) positions.
		return position >= 0 && position < getItemCount() && (
				onCursorDataSetActionSelected(action, position, payload) ||
						mDataSet.notifyCursorDataSetActionSelected(action, position, getItemId(position), payload)
		);
	}

	/**
	 * Invoked immediately after {@link #notifyCursorDataSetActionSelected(int, int, Object)} was called.
	 *
	 * @return {@code True} to indicate that this event was processed here, {@code false} to dispatch
	 * this event to the registered {@link OnCursorDataSetActionListener OnCursorDataSetActionListeners}.
	 */
	protected boolean onCursorDataSetActionSelected(int action, int position, @Nullable Object payload) {
		return false;
	}

	/**
	 */
	@Override
	public void unregisterOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener) {
		mDataSet.unregisterOnCursorDataSetActionListener(listener);
	}

	/**
	 */
	@Override
	public void changeCursor(@Nullable Cursor cursor) {
		final Cursor old = swapCursor(cursor);
		if (old != null) old.close();
	}

	/**
	 */
	@Nullable
	@Override
	@SuppressWarnings("unchecked")
	public C swapCursor(@Nullable Cursor cursor) {
		final C oldCursor = mDataSet.getCursor();
		if (cursor == mOriginalCursor || cursor == oldCursor) {
			return null;
		}
		if (cursor == null) {
			this.mDataSet.notifyCursorChange(null);
			this.mDataSet.attachCursor(null);
			this.mOriginalCursor = null;
			if (!onCursorChange(null, oldCursor)) {
				notifyCursorDataSetInvalidated();
			}
		} else {
			final C newCursor = wrapCursor(cursor);
			this.mDataSet.notifyCursorChange(newCursor);
			this.mDataSet.attachCursor(newCursor);
			this.mOriginalCursor = cursor;
			if (!onCursorChange(newCursor, oldCursor)) {
				notifyCursorDataSetChanged();
			}
		}
		this.mDataSet.notifyCursorChanged(mDataSet.getCursor());
		return oldCursor;
	}

	/**
	 * <b>Implementation of this method will be removed in the next release, so it will become abstract
	 * and thus its implementation will be required in the concrete inheritance hierarchies.</b>
	 */
	@NonNull
	@Override
	public C wrapCursor(@NonNull Cursor cursor) {
		throw new AndroidRuntimeException("Not implemented: " + getClass() + ".wrapCursor(Cursor)!");
	}

	/**
	 * Called from {@link #swapCursor(Cursor)} in order to handle change in cursor of this adapter.
	 * <p>
	 * <b>Note</b>, that during this call this adapter has already the new cursor attached.
	 * <p>
	 * This implementation does nothing.
	 *
	 * @param newCursor The new cursor data set for this adapter.
	 * @param oldCursor The old cursor data set of this adapter that has been replaced by the new one.
	 * @return {@code True} if change has been handled and appropriate callbacks has been fired to
	 * registered observers, {@code false} if default {@link #notifyDataSetChanged()} should be invoked.
	 */
	protected boolean onCursorChange(@Nullable C newCursor, @Nullable C oldCursor) {
		return false;
	}

	/**
	 */
	@Override
	public boolean isCursorAvailable() {
		return mDataSet.isCursorAvailable();
	}

	/**
	 */
	@Nullable
	@Override
	public C getCursorAt(int position) {
		return mDataSet.getCursorAt(position);
	}

	/**
	 */
	@Nullable
	@Override
	public C getCursor() {
		return mDataSet.getCursor();
	}

	/*
	 * @return {@code True} if {@link #getItemCount()} == 0, {@code false} otherwise.
	 */
	@Override
	public boolean isEmpty() {
		return getItemCount() == 0;
	}

	/**
	 */
	@Override
	public int getItemCount() {
		return mDataSet.getItemCount();
	}

	/**
	 * This implementation by default returns {@code true}.
	 */
	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	/**
	 */
	@Override
	public boolean hasItemAt(int position) {
		return mDataSet.hasItemAt(position);
	}

	/**
	 */
	@NonNull
	@Override
	public I getItem(int position) {
		return mDataSet.getItem(position);
	}

	/**
	 */
	@Override
	public long getItemId(int position) {
		return hasStableIds() ? mDataSet.getItemId(position) : NO_ID;
	}

	/**
	 */
	@Override
	@SuppressWarnings("unchecked")
	public VH onCreateViewHolder(ViewGroup parent, int viewType) {
		if (mItemViewRes == CursorAdapterAnnotationHandler.NO_VIEW_RES) {
			throw DatabaseException.missingClassAnnotation(
					CursorItemView.class,
					getClass(),
					"Cannot create view for the current position without item view layout resource."
			);
		}
		return (VH) new SimpleViewHolder(inflate(mItemViewRes, parent));
	}

	/**
	 * Inflates a view for item of this adapter via {@link #inflate(int, ViewGroup)} using layout
	 * resource specified via {@link CursorItemView @CursorItemView} annotation.
	 *
	 * @param parent A parent view, to resolve correct layout params for the newly creating view.
	 * @return The root view of the inflated item view hierarchy.
	 * @throws DatabaseException If there was no @CursorItemView annotation specified.
	 */
	@NonNull
	protected View inflateItemView(@NonNull ViewGroup parent) {
		if (mItemViewRes == CursorAdapterAnnotationHandler.NO_VIEW_RES) {
			throw DatabaseException.missingClassAnnotation(
					CursorItemView.class,
					getClass(),
					"Cannot inflate item view."
			);
		}
		return inflate(mItemViewRes, parent);
	}

	/**
	 * Inflates a new view hierarchy from the given xml resource.
	 *
	 * @param resource Resource id of a view to inflate.
	 * @param parent   A parent view, to resolve correct layout params for the newly creating view.
	 * @return The root view of the inflated view hierarchy.
	 * @see LayoutInflater#inflate(int, ViewGroup)
	 */
	@NonNull
	protected View inflate(@LayoutRes int resource, @NonNull ViewGroup parent) {
		return mLayoutInflater.inflate(resource, parent, false);
	}

	/**
	 */
	@Override
	public void onBindViewHolder(@NonNull VH viewHolder, int position) {
		final C cursor = mDataSet.getCursor();
		if (cursor == null || cursor.isClosed()) {
			throw new IllegalStateException("Cannot present data from the invalid cursor.");
		}
		final int correctedPosition = correctCursorPosition(position);
		if (cursor.moveToPosition(correctedPosition)) {
			onBindViewHolder(viewHolder, cursor, position);
		} else {
			throw new IllegalStateException(
					"Cannot move the attached cursor to the position(" + position + ") corrected as(" + correctedPosition + ")."
			);
		}
	}

	/**
	 * Corrects the given cursor <var>position</var> according to the current data set size to ensure
	 * that the attached cursor will be moved to the correct position.
	 * <p>
	 * This can be useful when this cursor adapter presents also data which are not available within
	 * the attached cursor, like array of alphabetic headers, when the cursor is sorted alphabetically
	 * and this adapter presents above each alphabetical section a header with a letter specific for
	 * such a section, so practically the current data set consists of two data sets, one is cursor
	 * and other one is array of headers.
	 * <p>
	 * For the current implementation of this adapter class, this method is called only for purpose
	 * of {@link #onBindViewHolder(RecyclerView.ViewHolder, int)} method, but may be called by
	 * implementations of this  adapter when its when its result is needed. Also the current
	 * implementation returns the same position as the given one.
	 *
	 * @param position Position which should be corrected.
	 * @return Corrected position for the cursor.
	 */
	protected int correctCursorPosition(int position) {
		return position;
	}

	/**
	 * Invoked to configure and bind an item view of the specified <var>viewHolder</var> with data of
	 * the current <var>cursor</var>. This is invoked whenever {@link #onBindViewHolder(RecyclerView.ViewHolder, int)}
	 * is called for the current iterated position.
	 *
	 * @param viewHolder The same holder as provided by {@link #onCreateViewHolder(ViewGroup, int)}
	 *                   for the specified position or converted view as holder as described above.
	 * @param cursor     Attached cursor already moved to the current iterated position.
	 * @param position   Position of the item from the current data set of which view to bind with data.
	 */
	protected abstract void onBindViewHolder(@NonNull VH viewHolder, @NonNull C cursor, int position);

	/**
	 * If you decide to override this method, do not forget to call {@code super.saveInstanceState()}
	 * and pass the obtained super state to the corresponding constructor of your saved state
	 * implementation to ensure the state of all classes along the chain is properly saved.
	 */
	@NonNull
	@Override
	@CallSuper
	public Parcelable saveInstanceState() {
		return AbsSavedState.EMPTY_STATE;
	}

	/**
	 * If you decide to override this method, do not forget to call {@code super.restoreInstanceState()}
	 * and pass here the parent state obtained from the your saved state implementation to ensure the
	 * state of all classes along the chain is properly restored.
	 */
	@Override
	@CallSuper
	public void restoreInstanceState(@NonNull Parcelable savedState) {
		// Inheritance hierarchies may restore theirs state here.
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Default {@link RecyclerView.ViewHolder} implementation used as default holder for purpose of
	 * {@link #onCreateViewHolder(ViewGroup, int)} method.
	 *
	 * @author Martin Albedinsky
	 */
	public static class SimpleViewHolder extends RecyclerView.ViewHolder {

		/**
		 * Creates a new instance of SimpleViewHolder for the specified <var>itemView</var>.
		 *
		 * @param itemView The view for which to create the new holder.
		 */
		public SimpleViewHolder(@NonNull View itemView) {
			super(itemView);
		}
	}
}
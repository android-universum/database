/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.test.model;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.Date;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.loremipsum.LoremIpsum;
import universum.studios.android.database.loremipsum.LoremIpsumGenerator;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.database.test.entity.TestModelEntity;
import universum.studios.android.database.util.TableColumn;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public class TestModel extends SimpleEntityModel<TestModel> {

	public interface TestValues {
		Long ID = 1200L;
		String TAG = "15619871654987";
		String AUTHOR = "author-1548";
		String TITLE = null;
		String DESCRIPTION = "Home to the Empire State Building.";
		Integer COUNT = null;
		Double LATITUDE = 40.7127D;
		Double LONGITUDE = 74.0059D;
		Long CATEGORY_ID = null;
		Boolean ENABLED = true;
		Boolean PUBLISHED = false;
		Integer FLAGS = 0;
		Float RATIO = 0.568F;
		Difficulty DIFFICULTY = Difficulty.HARD;
		Byte ORIENTATION = 1;
		Short CODE = 15200;
	}

	public interface JoinedTestValues {
		String CATEGORY_NAME = "stuffs";
	}

	private static final String TAG = "TestModel";

	public enum Difficulty {
		EASY, MEDIUM, HARD
	}

	public static final boolean DEFAULT_ENABLED = true;
	public static final boolean DEFAULT_PUBLISHED = false;
	public static final Difficulty DEFAULT_DIFFICULTY = Difficulty.EASY;

	public static final int COLUMNS_COUNT = 17;
	public static final int CONTENT_VALUES_COUNT = 16;
	public static final int INSERTABLE_CONTENT_VALUES_COUNT = 14;

	private static final int COLUMN_INDEX_ID = 8;

	public static final Object[] ALL_TEST_VALUES = {
			TestValues.AUTHOR,
			TestValues.CATEGORY_ID,
			TestValues.CODE,
			TestValues.COUNT,
			TestValues.DESCRIPTION,
			TestValues.DIFFICULTY.name(),
			(TestValues.ENABLED ? 1 : 0),
			TestValues.FLAGS,
			TestValues.ID,
			TestValues.LATITUDE,
			TestValues.LONGITUDE,
			TestValues.ORIENTATION,
			(TestValues.PUBLISHED ? 1 : 0),
			TestValues.RATIO,
			TestValues.TAG,
			TestValues.TITLE
	};

	public static final Parcelable.Creator<TestModel> CREATOR = new Parcelable.Creator<TestModel>() {

		@Override public TestModel createFromParcel(@NonNull final Parcel source) {
			return new TestModel(source);
		}

		@Override public TestModel[] newArray(final int size) {
			return new TestModel[size];
		}
	};

	public static final Factory<TestModel> FACTORY = new Factory<TestModel>() {

		@Override @NonNull public TestModel createModel() {
			return new TestModel();
		}
	};

	@Column.Primary(autoincrement = true)
	@Column(TestModelEntity.Columns.ID)
	public Long id;

	@Column.Unique
	@Column(value = TestModelEntity.Columns.TAG,
			constraints = TableColumn.CONSTRAINT_NOT_NULL)
	public String tag;

	@Column.Unique
	@Column(value = TestModelEntity.Columns.AUTHOR,
			constraints = TableColumn.CONSTRAINT_NOT_NULL)
	public String author;

	@Column(value = TestModelEntity.Columns.TITLE,
			defaultValue = "",
			typeConstraint = "56")
	public String title;

	@Column(value = TestModelEntity.Columns.DESCRIPTION,
			defaultValue = "")
	public String description;

	@Column(value = TestModelEntity.Columns.COUNT,
			nullable = true)
	public Integer count;

	@Column(TestModelEntity.Columns.LATITUDE)
	public Double latitude;

	@Column(TestModelEntity.Columns.LONGITUDE)
	public Double longitude;

	@Column.Foreign(TestModelEntity.References.CATEGORY_ID)
	@Column(TestModelEntity.Columns.CATEGORY_ID)
	public Long categoryId;

	@Column.Joined
	@Column(TestModelEntity.JoinedColumns.CATEGORY_NAME)
	public String categoryName;

	@Column(value = TestModelEntity.Columns.ENABLED,
			constraints = TableColumn.CONSTRAINT_NOT_NULL,
			defaultValue = "1")
	public Boolean enabled;

	@Column(value = TestModelEntity.Columns.PUBLISHED,
			constraints = TableColumn.CONSTRAINT_NOT_NULL,
			defaultValue = "0")
	public Boolean published;

	@Column(TestModelEntity.Columns.FLAGS)
	public Integer flags;

	@Column(TestModelEntity.Columns.RATIO)
	public Float ratio;

	@Column(value = TestModelEntity.Columns.DIFFICULTY,
			defaultValue = "EASY")
	public Difficulty difficulty;

	@Column(TestModelEntity.Columns.ORIENTATION)
	public Byte orientation;

	@Column(TestModelEntity.Columns.CODE)
	public Short code;

	public String address;

	public Date date;

	public TestModel() { super(); }
	public TestModel(@NonNull final Parcel source) { super(source); }

	public static TestModel withInitialContent() {
		final TestModel model = new TestModel();
		model.title = LoremIpsum.generateTitle();
		model.description = LoremIpsum.generateDescription();
		model.count = LoremIpsum.generateSize();
		model.enabled = LoremIpsumGenerator.RANDOM.nextBoolean();
		model.published = LoremIpsumGenerator.RANDOM.nextBoolean();
		model.flags = 0;
		model.ratio = LoremIpsumGenerator.RANDOM.nextFloat();
		model.difficulty = Difficulty.values()[LoremIpsumGenerator.RANDOM.nextInt(Difficulty.values().length)];
		model.orientation = (byte) (LoremIpsumGenerator.RANDOM.nextBoolean() ? 0 : 1);
		model.code = 0;
		return model;
	}

	public static TestModel withTestValues() {
		final TestModel model = new TestModel();
		model.id = TestValues.ID;
		model.tag = TestValues.TAG;
		model.author = TestValues.AUTHOR;
		model.title = TestValues.TITLE;
		model.description = TestValues.DESCRIPTION;
		model.count = TestValues.COUNT;
		model.latitude = TestValues.LATITUDE;
		model.longitude = TestValues.LONGITUDE;
		model.categoryId = TestValues.CATEGORY_ID;
		model.categoryName = JoinedTestValues.CATEGORY_NAME;
		model.enabled = TestValues.ENABLED;
		model.published = TestValues.PUBLISHED;
		model.flags = TestValues.FLAGS;
		model.ratio = TestValues.RATIO;
		model.difficulty = TestValues.DIFFICULTY;
		model.orientation = TestValues.ORIENTATION;
		model.code = TestValues.CODE;
		return model;
	}

	public static Cursor createCursorWithTestValues() {
		final MatrixCursor cursor = new MatrixCursor(TestModelEntity.ALL_ENTITY_COLUMNS, 1);
		cursor.addRow(TestModel.ALL_TEST_VALUES);
		cursor.moveToFirst();
		return cursor;
	}

	public static Cursor createCursorWithTestValues(int rowsCount) {
		final MatrixCursor cursor = new MatrixCursor(TestModelEntity.ALL_ENTITY_COLUMNS, rowsCount);
		for (int i = 0; i < rowsCount; i++) {
			final Object[] values = copyOf(TestModel.ALL_TEST_VALUES, TestModel.ALL_TEST_VALUES.length);
			values[COLUMN_INDEX_ID] = i + 1;
			cursor.addRow(values);
		}
		cursor.moveToFirst();
		return cursor;
	}

	private static <T> T[] copyOf(final T[] original, final int newLength) {
		if (original == null) {
			throw new NullPointerException("original == null");
		}
		if (newLength < 0) {
			throw new NegativeArraySizeException(Integer.toString(newLength));
		}
		return copyOfRange(original, 0, newLength);
	}

	@SuppressWarnings("unchecked")
	private static <T> T[] copyOfRange(final T[] original, final int start, final int end) {
		int originalLength = original.length; // For exception priority compatibility.
		if (start > end) {
			throw new IllegalArgumentException();
		}
		if (start < 0 || start > originalLength) {
			throw new ArrayIndexOutOfBoundsException();
		}
		int resultLength = end - start;
		int copyLength = Math.min(resultLength, originalLength - start);
		T[] result = (T[]) Array.newInstance(original.getClass().getComponentType(), resultLength);
		System.arraycopy(original, start, result, 0, copyLength);
		return result;
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.view.View;
import android.widget.FrameLayout;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class BaseAnnotationHandlerTest {

	@SuppressWarnings("unused")
	private static final String TAG = "BaseAnnotationHandlerTest";

	public void testInstantiation() {
		final TestHandler handler = new TestHandler(FrameLayout.class);
		assertThat(FrameLayout.class.equals(handler.getAnnotatedClass()), is(true));
	}

	@Test
	public void testInstantiationWithMaxSuperClass() {
		final TestHandler handler = new TestHandler(FrameLayout.class, View.class);
		assertThat(FrameLayout.class.equals(handler.getAnnotatedClass()), is(true));
		assertThat(View.class.equals(handler.mMaxSuperClass), is(true));
	}

	public void testFindAnnotation() {
		// Tested via DatabaseAnnotationsTest.
	}

	public void testFindAnnotationRecursive() {
		// Tested via DatabaseAnnotationsTest.
	}

	private static final class TestHandler extends BaseAnnotationHandler {

		TestHandler(Class<?> annotatedClass) {
			super(annotatedClass, null);
		}

		TestHandler(Class<?> annotatedClass, Class<?> maxSuperClass) {
			super(annotatedClass, maxSuperClass);
		}
	}
}
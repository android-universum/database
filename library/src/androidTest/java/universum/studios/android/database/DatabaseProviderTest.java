/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.annotation.Authority;
import universum.studios.android.database.annotation.PrimaryDatabase;
import universum.studios.android.database.inner.ContextBaseTest;
import universum.studios.android.database.inner.DatabaseTests;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class DatabaseProviderTest extends ContextBaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "DatabaseProviderTest";

	private static final String PROVIDER_AUTHORITY = "database.test.TestProvider";
	private final ProviderInfo PROVIDER_INFO = new ProviderInfo();
	{
		PROVIDER_INFO.exported = false;
	}

	private TestProvider mProvider;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		if (mProvider == null) {
			this.mProvider = new TestProvider(PROVIDER_AUTHORITY);
			this.mProvider.attachInfo(context, PROVIDER_INFO);
		}
		mProvider.clearDatabaseState();
	}

	@Test
	public void testInstantiationWithAnnotation() {
		final TestProvider provider = new TestProvider();
		assertThat(provider.getAuthority(), is("database.test.TestProviderWithAnnotation"));
	}

	@Test
	public void testInstantiationWithMissingAnnotation() {
		try {
			new TestProviderWithoutAnnotation();
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISSING_CLASS_ANNOTATION));
			assertThat(
					e.getMessage(),
					is("@Authority annotation for class(TestProviderWithoutAnnotation) is missing.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testInstantiationWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {
					@Override
					public void run() {
						new TestProvider();
					}
				},
				DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED
		);
	}

	@Test
	public void testGetAuthority() {
		assertThat(mProvider.getAuthority(), is(PROVIDER_AUTHORITY));
	}

	@Test
	public void testGetDatabase() {
		final Database database = mProvider.getDatabase();
		assertThat(database, is(not(nullValue())));
		assertThat(database instanceof TestSecondaryDatabase, is(true));
	}

	@Test
	public void testStoreDatabase() {
		final Database database = new TestTertiaryDatabase();
		DatabaseProvider.storeDatabase(database);
		assertThat(DatabaseProvider.provideDatabase(TestTertiaryDatabase.class), is(database));
	}

	// We do not test storing database as primary because when provider is created with database
	// that is primary we cannot store another one that is also primary and we already have one
	// TestProvider created for testing purpose with the primary database.

	@Test
	public void testStoreDatabaseAsPrimaryWithAlreadyStoredPrimaryDatabase() {
		try {
			DatabaseProvider.storeDatabase(new TestPrimaryDatabase());
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Only one database can be primary.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testObtainPrimaryDatabase() {
		assertThat(DatabaseProvider.providePrimaryDatabase(), is(not(nullValue())));
	}

	@Test
	public void testObtainDatabaseThatIsNotStored() {
		try {
			DatabaseProvider.provideDatabase(TestTertiaryDatabase.class);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_DEFAULT));
			assertThat(
					e.getMessage(),
					is("No such database(TestTertiaryDatabase) found.\n" +
							"Ensure that this database is attached to its corresponding provider and such " +
							"provider is specified within AndroidManifest.xml via <provider .../> tag.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testQuery() {
		final Uri uri = createUri("customers");
		mProvider.query(uri, null, null, null, null);
		mProvider.assertQueryDispatchedToDatabaseWithUri(uri);
	}

	@Test
	public void testInsert() {
		final Uri uri = createUri("invoices");
		mProvider.insert(uri, null);
		mProvider.assertInsertDispatchedToDatabaseWithUri(uri);
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testBulkInsert() {
		final Uri uri = createUri("invoices");
		mProvider.bulkInsert(uri, null);
		mProvider.assertBulkInsertDispatchedToDatabaseWithUri(uri);
	}

	@Test
	public void testUpdate() {
		final Uri uri = createUri("addresses");
		mProvider.update(uri, null, null, null);
		mProvider.assertUpdateDispatchedToDatabaseWithUri(uri);
	}

	@Test
	public void testDelete() {
		final Uri uri = createUri("customers");
		mProvider.delete(uri, null, null);
		mProvider.assertDeleteDispatchedToDatabaseWithUri(uri);
	}

	@Test
	public void testGetType() {
		final Uri uri = createUri("addresses");
		mProvider.getType(uri);
		mProvider.assertGetTypeDispatchedToDatabaseWithUri(uri);
	}

	private static Uri createUri(String path) {
		return Uri.parse("content://database.test/" + path);
	}

	@Authority("database.test.TestProviderWithAnnotation")
	private static final class TestProvider extends DatabaseProvider {

		private TestSecondaryDatabase database;

		public TestProvider() {
			super();
		}

		public TestProvider(@NonNull String authority) {
			super(authority);
		}

		@NonNull
		@Override
		protected Database onCreateDatabase(@NonNull Context context) {
			return database = new TestSecondaryDatabase();
		}

		void clearDatabaseState() {
			database.clearState();
		}

		void assertQueryDispatchedToDatabaseWithUri(Uri uri) {
			assertThat(database.queryDispatched, is(true));
			assertThat(database.dispatchedUri, is(uri));
		}

		void assertInsertDispatchedToDatabaseWithUri(Uri uri) {
			assertThat(database.insertDispatched, is(true));
			assertThat(database.dispatchedUri, is(uri));
		}

		void assertBulkInsertDispatchedToDatabaseWithUri(Uri uri) {
			assertThat(database.bulkInsertDispatched, is(true));
			assertThat(database.dispatchedUri, is(uri));
		}

		void assertUpdateDispatchedToDatabaseWithUri(Uri uri) {
			assertThat(database.updateDispatched, is(true));
			assertThat(database.dispatchedUri, is(uri));
		}

		void assertDeleteDispatchedToDatabaseWithUri(Uri uri) {
			assertThat(database.deleteDispatched, is(true));
			assertThat(database.dispatchedUri, is(uri));
		}

		void assertGetTypeDispatchedToDatabaseWithUri(Uri uri) {
			assertThat(database.getTypeDispatched, is(true));
			assertThat(database.dispatchedUri, is(uri));
		}
	}

	private static final class TestProviderWithoutAnnotation extends DatabaseProvider {

		@NonNull
		@Override
		@SuppressWarnings("ConstantConditions")
		protected Database onCreateDatabase(@NonNull Context context) {
			return null;
		}
	}

	@PrimaryDatabase
	private static final class TestPrimaryDatabase extends Database {

		TestPrimaryDatabase() {
			super(1, "test.db");
		}
	}

	private static final class TestSecondaryDatabase extends Database {

		boolean queryDispatched, insertDispatched, bulkInsertDispatched, updateDispatched, deleteDispatched, getTypeDispatched;
		Uri dispatchedUri;

		TestSecondaryDatabase() {
			/*We use the same name as for TestDatabase so we always have only one db file created.*/
			super(1, "test.db");
		}

		@Override
		protected Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
			this.queryDispatched = true;
			this.dispatchedUri = uri;
			return null;
		}

		@Override
		protected Uri insert(@NonNull Uri uri, ContentValues values) {
			this.insertDispatched = true;
			this.dispatchedUri = uri;
			return null;
		}

		@Override
		protected int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
			this.bulkInsertDispatched = true;
			this.dispatchedUri = uri;
			return 0;
		}

		@Override
		protected int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
			this.updateDispatched = true;
			this.dispatchedUri = uri;
			return 0;
		}

		@Override
		protected int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
			this.deleteDispatched = true;
			this.dispatchedUri = uri;
			return 0;
		}

		@Nullable
		@Override
		protected String getType(@NonNull Uri uri) {
			this.getTypeDispatched = true;
			this.dispatchedUri = uri;
			return null;
		}

		void clearState() {
			queryDispatched = insertDispatched = bulkInsertDispatched = updateDispatched = deleteDispatched = getTypeDispatched = false;
			dispatchedUri = null;
		}
	}

	private static final class TestTertiaryDatabase extends Database {

		TestTertiaryDatabase() {
			/*We use the same name as for TestDatabase so we always have only one db file created.*/
			super(1, "test.db");
		}
	}
}
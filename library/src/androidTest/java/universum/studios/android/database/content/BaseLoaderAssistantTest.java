/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;

import org.junit.Test;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.test.annotation.UiThreadTest;
import universum.studios.android.database.entity.TestModelEntity2;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class BaseLoaderAssistantTest extends LoaderAssistantBaseTest<Cursor, BaseLoaderAssistantTest.TestAssistant> {

	private static final int LOADER_ID = 0x01;
	private static final Bundle LOADER_PARAMS = new Bundle();
	private static final String LOADER_PARAM_KEY = "loader.PARAM.title";
	private static final String LOADER_PARAM_VALUE = "Rome";
	static {
		LOADER_PARAMS.putString(LOADER_PARAM_KEY, LOADER_PARAM_VALUE);
	}

	@Override
	protected TestAssistant onCreateAssistant(FragmentActivity activity) {
		return new TestAssistant(activity, activity.getSupportLoaderManager());
	}

	@Test
	@UiThreadTest
	public void testStartLoader() {
		assistant.startLoader(LOADER_ID, LOADER_PARAMS);
		this.waitForLoaderToBeAvailable();
		this.assertThatLoaderIsStarted(LOADER_ID);
		this.assertThatLoaderHasIdAndParams(LOADER_ID, LOADER_PARAMS);
		this.destroyLoader(LOADER_ID);
	}

	@Test
	@UiThreadTest
	public void testStartAlreadyStartedLoader() {
		assistant.startLoader(LOADER_ID, LOADER_PARAMS);
		this.waitForLoaderToBeAvailable();
		assistant.startLoader(LOADER_ID, LOADER_PARAMS);
		this.waitForLoaderToBeAvailable();
		this.assertThatLoaderIsStarted(LOADER_ID);
		this.assertThatLoaderHasIdAndParams(LOADER_ID, LOADER_PARAMS);
		this.destroyLoader(LOADER_ID);
	}

	@Test
	@UiThreadTest
	public void testInitLoader() {
		assistant.initLoader(LOADER_ID, LOADER_PARAMS);
		this.waitForLoaderToBeAvailable();
		this.assertThatLoaderIsStarted(LOADER_ID);
		this.assertThatLoaderHasIdAndParams(LOADER_ID, LOADER_PARAMS);
		this.destroyLoader(LOADER_ID);
	}

	@Test
	@UiThreadTest
	public void testRestartLoader() {
		assistant.restartLoader(LOADER_ID, LOADER_PARAMS);
		this.waitForLoaderToBeAvailable();
		this.assertThatLoaderIsStarted(LOADER_ID);
		this.assertThatLoaderHasIdAndParams(LOADER_ID, LOADER_PARAMS);
		this.destroyLoader(LOADER_ID);
	}

	@Test
	@UiThreadTest
	public void testGetLoader() {
		assistant.initLoader(LOADER_ID, LOADER_PARAMS);
		this.waitForLoaderToBeAvailable();
		assertThat(assistant.getLoader(LOADER_ID), is(not(nullValue())));
		this.destroyLoader(LOADER_ID);
	}

	@Test
	@UiThreadTest
	public void testDestroyLoader() {
		assistant.initLoader(LOADER_ID, LOADER_PARAMS);
		this.waitForLoaderToBeAvailable();
		assistant.destroyLoader(LOADER_ID);
		assertThat(assistant.getLoader(LOADER_ID), is(nullValue()));
	}

	@Test
	@UiThreadTest
	@SuppressWarnings("ConstantConditions")
	public void testOnLoadFinished() {
		assistant.startLoader(LOADER_ID, LOADER_PARAMS);
		final Loader<Cursor> loader = assistant.getLoader(LOADER_ID);
		assertThat(loader, is(not(nullValue())));
		assistant.onLoadFinished(loader, new MatrixCursor(new String[0]));
		assistant.onLoadFinished(loader, null);
		this.destroyLoader(LOADER_ID);
	}

	@Test
	public void testOnLoadFailed() {
		assistant.onLoadFailed(LOADER_ID);
	}

	@Test
	@UiThreadTest
	@SuppressWarnings("ConstantConditions")
	public void testOnLoaderReset() {
		assistant.startLoader(LOADER_ID, LOADER_PARAMS);
		final Loader<Cursor> loader = assistant.getLoader(LOADER_ID);
		assertThat(loader, is(not(nullValue())));
		assistant.onLoaderReset(loader);
		this.destroyLoader(LOADER_ID);
	}

	static final class TestAssistant extends BaseLoaderAssistant<Cursor> implements TestLoaderAssistant<Cursor> {

		static final int CALLBACK_ON_LOAD_FINISHED = 0x01;
		static final int CALLBACK_ON_LOAD_FAILED = 0x02;

		int loaderId;
		Bundle loaderParams;
		boolean callbackReceived;
		int callbackType;

		TestAssistant(@NonNull Context context, @NonNull LoaderManager loaderManager) {
			super(context, loaderManager);
		}

		@Override
		public Loader<Cursor> onCreateLoader(int loaderId, @Nullable Bundle arguments) {
			this.loaderId = loaderId;
			this.loaderParams = arguments;
			return new CursorLoader(mContext, TestModelEntity2.URI, null, null, null, null);
		}

		@Override
		protected void onLoadFinished(@IntRange(from = 0) int loaderId, @NonNull Cursor cursor) {
			this.callbackType = CALLBACK_ON_LOAD_FINISHED;
			this.callbackReceived = true;
		}

		@Override
		protected void onLoadFailed(@IntRange(from = 0) int loaderId) {
			super.onLoadFailed(loaderId);
			this.callbackType = CALLBACK_ON_LOAD_FAILED;
			this.callbackReceived = true;
		}

		@Override
		public int getLoaderId() {
			return loaderId;
		}

		@Nullable
		@Override
		public Bundle getLoaderParams() {
			return loaderParams;
		}
	}
}
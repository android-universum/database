/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.database.Cursor;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Model;

/**
 * @author Martin Albedinsky
 */
@Model(TestModel.class)
public class TestModelCursor extends EntityModelCursorWrapper<TestModel> {

	@SuppressWarnings("unused")
	private static final String TAG = "TestModelCursor";

	public TestModelCursor(@NonNull Cursor cursor) {
		super(cursor);
	}

	public TestModelCursor(@NonNull Cursor cursor, @NonNull TestModel model) {
		super(cursor, model);
	}

	@NonNull
	public static TestModelCursor wrap(@NonNull Cursor cursor) {
		return cursor instanceof TestModelCursor ? (TestModelCursor) cursor : new TestModelCursor(cursor);
	}
}

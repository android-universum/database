/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import universum.studios.android.database.util.TypeUtils;

/**
 * @author Martin Albedinsky
 */
class TestAnnotatedColumn implements AnnotatedColumn {

	@SuppressWarnings("unused")
	private static final String TAG = "TestAnnotatedColumn";

	private final String mName;
	private final int mType;
	private final Field mField;

	public TestAnnotatedColumn(Field field) {
		this(field.getName(), field);
	}

	public TestAnnotatedColumn(Field field, boolean accessible) {
		this(field.getName(), field, accessible);
	}

	public TestAnnotatedColumn(String name, Field field) {
		this(name, field, true);
	}

	public TestAnnotatedColumn(String name, Field field, boolean accessible) {
		this.mName = name;
		this.mType = TypeUtils.resolveType(field.getType());
		this.mField = field;
		if (accessible) field.setAccessible(true);
	}

	@NonNull
	@Override
	public String getName() {
		return mName;
	}

	@Override
	public int getType() {
		return mType;
	}

	@NonNull
	@Override
	public Field getField() {
		return mField;
	}

	@Override
	public boolean requiresUpgrade(int oldVersion, int newVersion) {
		return false;
	}

	@Override
	public boolean isNullable() {
		return false;
	}

	@Override
	public boolean isPrimary() {
		return false;
	}

	@Override
	public boolean isUnique() {
		return false;
	}

	@Override
	public boolean isForeign() {
		return false;
	}

	@Override
	public boolean isJoined() {
		return false;
	}

	@Override
	public boolean isDeprecated() {
		return false;
	}
}

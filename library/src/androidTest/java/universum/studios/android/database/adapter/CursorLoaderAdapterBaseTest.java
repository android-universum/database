/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import org.junit.Test;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.test.annotation.UiThreadTest;
import universum.studios.android.database.content.LoaderAssistantBaseTest;
import universum.studios.android.database.content.TestLoaderAssistant;
import universum.studios.android.database.entity.TestModelEntity2;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public abstract class CursorLoaderAdapterBaseTest extends LoaderAssistantBaseTest<Cursor, CursorLoaderAdapterBaseTest.TestAssistant> {

	private static final int LOADER_ID = 0x01;
	private static final Bundle LOADER_PARAMS = new Bundle();
	private static final String LOADER_PARAM_KEY = "loader.PARAM.favorite";
	private static final boolean LOADER_PARAM_VALUE = true;
	static {
		LOADER_PARAMS.putBoolean(LOADER_PARAM_KEY, LOADER_PARAM_VALUE);
	}

	private LoaderAdapter<Cursor> adapter;
	private LoaderAdapter<Cursor> adapterWithoutLoaderId;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.adapter = onCreateAdapter(activity);
		this.adapter.setLoaderAssistant(assistant);
		this.adapterWithoutLoaderId = onCreateAdapterWithoutLoaderId(activity);
	}
	@Override
	protected TestAssistant onCreateAssistant(FragmentActivity activity) {
		return new TestAssistant(activity, activity.getSupportLoaderManager());
	}

	abstract LoaderAdapter<Cursor> onCreateAdapter(FragmentActivity activity);

	abstract LoaderAdapter<Cursor> onCreateAdapterWithoutLoaderId(FragmentActivity activity);

	@Test
	public void testDefaultGetLoaderId() {
		assertThat(adapterWithoutLoaderId.getLoaderId(), is(LoadableAdapter.NO_LOADER_ID));
	}

	@Test
	public void testGetLoaderId() {
		adapter.setLoaderId(1000);
		assertThat(adapter.getLoaderId(), is(1000));
	}

	@Test
	@UiThreadTest
	public void testStartLoader() {
		adapter.startLoader();
		this.waitForLoaderToBeAvailable();
		this.assertThatLoaderIsStarted(adapter.getLoaderId());
		this.assertThatLoaderHasId(adapter.getLoaderId());
		this.destroyLoader(adapter.getLoaderId());
	}

	@Test
	public void testStartLoaderWithoutSpecifiedLoaderId() {
		adapterWithoutLoaderId.startLoader();
		assertThatNoLoaderIsRunning();
	}

	@Test
	@UiThreadTest
	public void testInitLoader() {
		adapter.initLoader();
		this.waitForLoaderToBeAvailable();
		this.assertThatLoaderIsStarted(adapter.getLoaderId());
		this.assertThatLoaderHasId(adapter.getLoaderId());
		this.destroyLoader(adapter.getLoaderId());
	}

	@Test
	public void testInitLoaderWithoutSpecifiedLoaderId() {
		adapterWithoutLoaderId.initLoader();
		assertThatNoLoaderIsRunning();
	}

	@Test
	@UiThreadTest
	public void testRestartLoader() {
		adapter.restartLoader();
		this.waitForLoaderToBeAvailable();
		this.assertThatLoaderIsStarted(adapter.getLoaderId());
		this.assertThatLoaderHasId(adapter.getLoaderId());
		this.destroyLoader(adapter.getLoaderId());
	}

	@Test
	public void testRestartLoaderWithoutSpecifiedLoaderId() {
		adapterWithoutLoaderId.restartLoader();
		assertThatNoLoaderIsRunning();
	}

	@Test
	@UiThreadTest
	public void testGetLoader() {
		adapter.initLoader();
		this.waitForLoaderToBeAvailable();
		assertThat(adapter.getLoader(), is(not(nullValue())));
		this.destroyLoader(adapter.getLoaderId());
	}

	@Test
	public void testGetLoaderWithoutSpecifiedLoaderId() {
		assertThat(adapterWithoutLoaderId.getLoader(), is(nullValue()));
	}

	@Test
	@UiThreadTest
	public void testDestroyLoader() {
		adapter.initLoader();
		this.waitForLoaderToBeAvailable();
		adapter.destroyLoader();
		assertThat(adapter.getLoader(), is(nullValue()));
	}

	static class TestAssistant extends DataLoaderAdapterAssistant<Cursor> implements TestLoaderAssistant<Cursor> {

		static final int CALLBACK_ON_LOAD_FINISHED = 0x01;
		static final int CALLBACK_ON_LOAD_FAILED = 0x02;

		boolean loaderCreated;
		int loaderId;
		Bundle loaderParams;
		boolean callbackReceived;
		int callbackType;

		private TestAssistant(@NonNull Context context, @NonNull LoaderManager loaderManager) {
			super(context, loaderManager);
		}

		@Override
		public Loader<Cursor> onCreateLoader(int loaderId, @Nullable Bundle params) {
			this.loaderId = loaderId;
			this.loaderParams = params;
			return new CursorLoader(mContext, TestModelEntity2.URI, null, null, null, null);
		}

		@Override
		protected void onLoadFinished(int loaderId, @NonNull Cursor cursor) {
			this.loaderCreated = true;
			this.callbackType = CALLBACK_ON_LOAD_FINISHED;
			this.callbackReceived = true;
		}

		@Override
		protected void onLoadFailed(int loaderId) {
			super.onLoadFailed(loaderId);
			this.loaderCreated = true;
			this.callbackType = CALLBACK_ON_LOAD_FAILED;
			this.callbackReceived = true;
		}

		@Override
		public int getLoaderId() {
			return loaderId;
		}

		@Nullable
		@Override
		public Bundle getLoaderParams() {
			return loaderParams;
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.database.Cursor;
import android.net.Uri;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.Database;
import universum.studios.android.database.TestPrimaryProvider;
import universum.studios.android.database.inner.BaseTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class JoinEntityTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "JoinEntityTest";
	private static final String JOIN_STATEMENT = ""
			+ TestEntity.NAME + " LEFT OUTER JOIN " + TestTypesEntity.NAME
			+ " ON (" + TestEntity.NAME + "." +  TestEntity.Columns.TYPE_ID + "=" + TestTypesEntity.NAME + "." + TestTypesEntity.Columns.ID + ")";
	private static final String INVALID_JOIN_STATEMENT = ""
			+ TestEntity.NAME + " OUTER LEFT JOIN " + TestTypesEntity.NAME
			+ " ON " + TestEntity.NAME + "." +  TestEntity.Columns.TYPE_ID + "=" + TestTypesEntity.NAME + "." + TestTypesEntity.Columns.ID + "";

	private TestJoinEntity mEntity;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		if (mEntity == null) this.mEntity = TestPrimaryProvider.accessDatabase().findEntityByClass(TestJoinEntity.class);
	}

	@Test
	public void testSetNotificationUriAfterAttachedToDatabase() {
		final Database database = TestPrimaryProvider.accessDatabase();
		final TestJoinEntity2 entity = new TestJoinEntity2();
		entity.attachToDatabase(database);
		assertThat(entity.getNotificationUri(), is(TestJoinEntity2.URI));
		entity.setNotificationEntity(TestEntity.class);
		assertThat(entity.getNotificationUri(), is(TestEntity.URI));
	}

	@Test
	public void testGetNotificationUri() {
		assertThat(mEntity.getNotificationUri(), is(TestEntity.URI));
	}

	@Test
	public void testCreatesInitialContent() {
		assertThat(mEntity.createsInitialContent(), is(false));
	}

	@Test
	public void testUpgradesContent() {
		assertThat(mEntity.upgradesContent(1, 2), is(false));
		assertThat(mEntity.upgradesContent(2, 3), is(false));
		assertThat(mEntity.upgradesContent(3, 4), is(false));
		// ...
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testQuery() {
		mEntity.setJoinStatement(JOIN_STATEMENT);
		final Cursor cursor = mEntity.query(null, null, null, null);
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.getColumnCount(), is(5));
		// TestEntity does not refer to any content.
		assertThat(cursor.getCount(), is(0));
	}

	@Test
	public void testQueryWithInvalidJoinStatement() {
		mEntity.setJoinStatement(INVALID_JOIN_STATEMENT);
		try {
			mEntity.query(null, null, null, null);
		} catch (IllegalArgumentException e) {
			assertThat(
					e.getMessage(),
					is("Join statement(" + INVALID_JOIN_STATEMENT + ") for the entity(TestJoinEntity) is not valid.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	private static final class TestJoinEntity2 extends JoinEntity {

		static final String NAME = "test_join2";
		static final Uri URI = Database.createContentUri(TestPrimaryProvider.AUTHORITY, NAME);

		public TestJoinEntity2() {
			super();
		}

		@NonNull
		@Override
		protected String onCreateJoinStatement() {
			// Ignored.
			return "";
		}
	}
}
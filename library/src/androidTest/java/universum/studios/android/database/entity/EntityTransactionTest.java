/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.util.AndroidRuntimeException;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.TestPrimaryDatabase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class EntityTransactionTest {

	@SuppressWarnings("unused")
	private static final String TAG = "DatabaseEntityTransactionTest";

	@Test
	public void testInstantiation() {
		new TestTransaction();
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testExecute() {
		assertThat(new TestTransaction(false).execute(), is(false));
		assertThat(new TestTransaction(true).execute(), is(true));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testExecuteWithException() {
		final TestTransaction transaction = new TestTransaction();
		assertThat(transaction.execute(), is(false));
		assertThat(transaction.onErrorReceived, is(true));
	}

	private static final class TestTransaction extends EntityTransaction<Boolean, TestEntity, TestPrimaryDatabase> {

		boolean onErrorReceived;
		private final Boolean result;

		public TestTransaction() {
			this(null);
		}

		public TestTransaction(Boolean result) {
			super(TestEntity.class, TestPrimaryDatabase.class);
			this.result = result;
		}

		@Nullable
		@Override
		protected Boolean onExecute(@NonNull TestPrimaryDatabase database, @NonNull TestEntity entity) {
			if (result == null) throw new AndroidRuntimeException("No test result specified.");
			return result;
		}

		@Nullable
		@Override
		protected Boolean onError(@NonNull TestPrimaryDatabase database, @NonNull TestEntity entity, @NonNull Throwable error) {
			this.onErrorReceived = true;
			return false;
		}
	}
}
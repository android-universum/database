/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.Cursor;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.EntityModelCursorWrapper;
import universum.studios.android.database.model.TestModel;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class EntityModelCursorAnnotationHandlerTest extends AnnotationHandlerBaseTest<EntityModelCursorAnnotationHandler> {

	@SuppressWarnings("unused")
	private static final String TAG = "EntityModelCursorWrapperAnnotationHandlerTest";

	public EntityModelCursorAnnotationHandlerTest() {
		super(EntityModelAnnotationHandlers.ModelCursorHandler.class);
	}

	@Override
	EntityModelCursorAnnotationHandler onObtainHandler() {
		return EntityModelAnnotationHandlers.obtainModelCursorHandler(TestWrapper.class);
	}

	@Override
	EntityModelCursorAnnotationHandler onObtainEmptyHandler() {
		return EntityModelAnnotationHandlers.obtainModelCursorHandler(TestWrapperWithoutAnnotation.class);
	}

	@Test
	public void testCreateModelForWrapperWithoutAnnotation() {
		try {
			mEmptyHandler.handleCreateModel();
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISSING_CLASS_ANNOTATION));
			assertThat(
					e.getMessage(),
					is("@Model annotation for class(TestWrapperWithoutAnnotation) is missing.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testCreateModel() {
		final EntityModel model = mHandler.handleCreateModel();
		assertThat(model, is(not(nullValue())));
		assertThat(model, is(instanceOf(TestModel.class)));
	}

	@Model(TestModel.class)
	private static final class TestWrapper extends EntityModelCursorWrapper<TestModel> {

		public TestWrapper(@NonNull Cursor cursor) {
			super(cursor);
		}
	}

	private static final class TestWrapperWithoutAnnotation extends EntityModelCursorWrapper {

		public TestWrapperWithoutAnnotation(@NonNull Cursor cursor) {
			super(cursor);
		}
	}
}
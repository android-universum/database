/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.content.Context;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Authority;

/**
 * @author Martin Albedinsky
 */
@Authority(TestPrimaryProvider.AUTHORITY)
public final class TestPrimaryProvider extends DatabaseProvider {

	@SuppressWarnings("unused")
	private static final String TAG = "TestPrimaryProvider";

	public static final String AUTHORITY = "universum.studios.android.database.test.TestProvider";

	@NonNull
	@Override
	protected Database onCreateDatabase(@NonNull Context context) {
		return new TestPrimaryDatabase();
	}

	@NonNull
	public static Database accessDatabase() {
		return providePrimaryDatabase();
	}
}

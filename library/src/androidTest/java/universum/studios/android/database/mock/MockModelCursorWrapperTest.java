/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.mock;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.inner.BaseTest;
import universum.studios.android.database.model.TestModel;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class MockModelCursorWrapperTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "MockModelCursorWrapperTest";

	@Test
	public void testInstantiation() {
		final TestMockWrapper wrapper = new TestMockWrapper(10);
		assertThat(wrapper.getCount(), is(10));
		assertThat(wrapper.moveToPosition(9), is(true));
		assertThat(wrapper.getModel(), is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithModel() {
		final TestMockWrapper wrapper = new TestMockWrapper(100, new TestModel());
		assertThat(wrapper.getCount(), is(100));
		assertThat(wrapper.moveToPosition(99), is(true));
		assertThat(wrapper.getModel(), is(not(nullValue())));
	}

	// No more extensive tests, as MockCursor along with MockModelCursorWrappers are classes that
	// heavily depends on theirs further implementation.

	@Model(TestModel.class)// Presented so we can use TestMockWrapper(int) constructor.
	@SuppressWarnings("ClassTooDeepInInheritanceTree")
	private static final class TestMockWrapper extends MockModelCursorWrapper<TestModel> {

		private TestMockWrapper(@Size(min = 0) int count) {
			super(count);
		}

		private TestMockWrapper(@Size(min = 0) int count, @NonNull TestModel model) {
			super(count, model);
		}
	}
}
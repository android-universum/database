/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.inner;

import org.junit.After;
import org.junit.Before;

/**
 * @author Martin Albedinsky
 */
public abstract class BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "BaseTest";

	public static final long WAIT_MAX_DURATION = 10000;

	@Before
	public void beforeTest() throws Exception {
	}

	@After
	public void afterTest() throws Exception {
	}
}

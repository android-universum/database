/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.database.Cursor;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.inner.BaseTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class EntityModelCursorWrapperTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "EntityModelCursorWrapperTest";

	private TestModelCursor mWrapper;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mWrapper = new TestModelCursor(TestModel.createCursorWithTestValues(), new TestModel());
	}

	@Test
	public void testInstantiation() {
		final EntityModelCursorWrapper wrapper = new TestModelCursor(TestModel.createCursorWithTestValues());
		assertThat(wrapper.accessModel(), is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithModel() {
		final TestModel model = new TestModel();
		final TestModelCursor wrapper = new TestModelCursor(TestModel.createCursorWithTestValues(), model);
		assertThat(wrapper.accessModel(), is(model));
	}

	@Test
	public void testInstantiationWithoutModelAnnotation() {
		try {
			new TestWrapperWithoutAnnotation(TestModel.createCursorWithTestValues());
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISSING_CLASS_ANNOTATION));
			assertThat(
					e.getMessage(),
					is("@Model annotation for class(TestWrapperWithoutAnnotation) is missing.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testInstantiationWithDisabledAnnotations() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		try {
			new TestModelCursor(TestModel.createCursorWithTestValues());
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Trying to access logic that requires annotations processing to be enabled, " +
							"but it seams that the annotations processing is disabled for the Database library.")
			);
			DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
			return;
		}
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mWrapper.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetModel() {
		// Wrapper will automatically move its cursor to the initial position when it was not moved yet.
		final TestModel model = mWrapper.getModel();
		assertThat(model, is(not(nullValue())));
		assertThat(mWrapper.getModel(), is(not(model)));
		assertThat(model.id, is(TestModel.TestValues.ID));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetModelAt() {
		final TestModel model = mWrapper.getModelAt(0);
		assertThat(model, is(not(nullValue())));
		assertThat(mWrapper.getModelAt(0), is(not(model)));
		assertThat(model.id, is(TestModel.TestValues.ID));
		assertThat(mWrapper.getModelAt(1), is(nullValue()));
	}

	@Test
	public void testAccessModel() {
		// Wrapper will automatically move its cursor to the initial position when it was not moved yet.
		final TestModel model = mWrapper.accessModel();
		assertThat(model, is(not(nullValue())));
		assertThat(mWrapper.accessModel(), is(model));
		assertThat(model.id, is(TestModel.TestValues.ID));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testAccessModelAt() {
		final TestModel model = mWrapper.accessModelAt(0);
		assertThat(model, is(not(nullValue())));
		assertThat(mWrapper.accessModelAt(0), is(model));
		assertThat(model.id, is(TestModel.TestValues.ID));
		assertThat(mWrapper.accessModelAt(1), is(nullValue()));
	}

	@Test
	public void testOnBindData() {
		final TestModel model = mWrapper.accessModel();
		assertThat(mWrapper.onBindData(), is(true));
		assertThat(model.id, is(TestModel.TestValues.ID));
		// We do not test here if the model has all data bound as we expect that SimpleEntityModel.fromCursor(Cursor)
		// method has been properly tested either via ModelCursorProcessorTest or via SimpleEntityModelTest.
	}

	private static final class TestWrapperWithoutAnnotation extends EntityModelCursorWrapper<TestModel> {

		private TestWrapperWithoutAnnotation(@NonNull Cursor cursor) {
			super(cursor);
		}
	}
}
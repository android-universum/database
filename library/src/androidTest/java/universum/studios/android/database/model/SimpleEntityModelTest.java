/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.content.ContentValues;
import android.os.Parcel;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.inner.BaseTest;
import universum.studios.android.database.inner.DatabaseTests;
import universum.studios.android.database.mock.MockCursor;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class SimpleEntityModelTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "SimpleEntityModelTest";

	private TestModel mModel;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mModel = TestModel.withTestValues();
	}

	@Test
	public void testInstantiation() {
		new TestModel();
	}

	@Test
	public void testInstantiationFromParcel() {
		final Parcel parcel = Parcel.obtain();
		new TestModel(parcel);
		parcel.recycle();
	}

	@Test
	public void testInstantiationFromParcelWithDisabledAnnotationsProcessing() {
		final Parcel parcel = Parcel.obtain();
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		new TestModel(parcel);
		// We do not perform any assertions here as we assume that the reading logic has been properly
		// tested in ModelParcelProcessorTest.
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		parcel.recycle();
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mModel.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandlerWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {
					@Override
					public void run() {
						mModel.getAnnotationHandler();
					}
				},
				DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED
		);
	}

	@Test
	public void testWriteToParcel() {
		final Parcel parcel = Parcel.obtain();
		mModel.writeToParcel(parcel, 0);
		// We do not perform any assertions here as we assume that the writing logic has been properly
		// tested in ModelParcelProcessorTest.
		parcel.recycle();
	}

	@Test
	public void testWriteToParcelWithDisabledAnnotationsProcessing() {
		final Parcel parcel = Parcel.obtain();
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		mModel.writeToParcel(parcel, 0);
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		parcel.recycle();
	}

	@Test
	public void testDescribeContents() {
		assertThat(mModel.describeContents(), is(0));
	}

	@Test
	public void testToContentValues() {
		final ContentValues values = mModel.toContentValues();
		assertThat(values, is(not(nullValue())));
		assertThat(values.size(), is(TestModel.INSERTABLE_CONTENT_VALUES_COUNT));
		// We do not perform any assertions here as we assume that the putting logic has been properly
		// tested in ModelContentValuesProcessorTest.
	}

	@Test
	public void testToContentValuesWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {
					@Override
					public void run() {
						mModel.toContentValues();
					}
				},
				"Failed to transform model(TestModel) into ContentValues. " +
						"Annotations processing is not enabled. Implementation of TestModel.toContentValues() method is required."
		);
	}

	@Test
	public void testFromCursor() {
		assertThat(mModel.fromCursor(new MockCursor(0)), is(true));
		// We do not perform any assertions here as we assume that the binding logic has been properly
		// tested in ModelCursorProcessorTest.
	}

	@Test
	public void testFromCursorWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {

					@Override
					@SuppressWarnings("ResourceType")
					public void run() {
						mModel.fromCursor(new MockCursor(0));
					}
				},
				"Failed to bind data of model(TestModel) from cursor. " +
						"Annotations processing is not enabled. Implementation of TestModel.fromCursor(Cursor) method is required."
		);
	}

	@Test
	public void testCreateSelection() {
		assertThat(mModel.createSelection(), is(not(nullValue())));
		// We do not perform any assertions here as we assume that the selection logic has been properly
		// tested in EntityModelAnnotationHandlerTest.
	}

	@Test
	public void testCreateSelectionWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {

					@Override
					public void run() {
						mModel.createSelection();
					}
				},
				"Failed to create selection for model(TestModel). " +
						"Annotations processing is not enabled. Implementation of TestModel.createSelection() method is required."
		);
	}

	@Test
	public void testClone() {
		final TestModel modelClone = mModel.clone();
		assertThat(modelClone, is(not(nullValue())));
		assertThat(mModel.id, is(modelClone.id));
		assertThat(mModel.tag, is(modelClone.tag));
		assertThat(mModel.author, is(modelClone.author));
		assertThat(mModel.title, is(modelClone.title));
		assertThat(mModel.description, is(modelClone.description));
		assertThat(mModel.count, is(modelClone.count));
		assertThat(mModel.latitude, is(modelClone.latitude));
		assertThat(mModel.longitude, is(modelClone.longitude));
		assertThat(mModel.categoryId, is(modelClone.categoryId));
		assertThat(mModel.categoryName, is(modelClone.categoryName));
		assertThat(mModel.enabled, is(modelClone.enabled));
		assertThat(mModel.published, is(modelClone.published));
		assertThat(mModel.flags, is(modelClone.flags));
		assertThat(mModel.difficulty, is(modelClone.difficulty));
		assertThat(mModel.orientation, is(modelClone.orientation));
		assertThat(mModel.count, is(modelClone.count));
		assertThat(mModel.address, is(modelClone.address));
		assertThat(mModel.date, is(modelClone.date));
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.database.Cursor;
import android.os.Bundle;

import org.junit.Test;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.content.Loader;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.annotation.LoaderId;
import universum.studios.android.database.inner.DatabaseTests;
import universum.studios.android.database.mock.MockCursor;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelCursor;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
public final class SpinnerCursorLoaderAdapterTest extends CursorLoaderAdapterBaseTest {

	private TestAdapter mAdapter;

	@Override
	LoaderAdapter<Cursor> onCreateAdapter(FragmentActivity activity) {
		return mAdapter = new TestAdapter(activity);
	}

	@Override
	LoaderAdapter<Cursor> onCreateAdapterWithoutLoaderId(FragmentActivity activity) {
		return new TestAdapterWithoutAnnotation(activity);
	}

	@Test
	public void testInstantiation() {
		final TestAdapter adapter = new TestAdapter(activity);
		assertAdapterProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithCursor() {
		final TestAdapter adapter = new TestAdapter(activity, new TestModelCursor(new MockCursor(0)));
		assertAdapterProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(not(nullValue())));
		assertThat(adapter.getCursor(), is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithDisabledAnnotationsProcessing() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		final TestAdapter adapter = new TestAdapter(activity);
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		assertAdapterProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(nullValue()));
	}

	private static void assertAdapterProperlyInstantiated(BaseCursorAdapter adapter) {
		assertThat(adapter, is(not(nullValue())));
		assertThat(adapter.mContext, is(not(nullValue())));
		assertThat(adapter.mResources, is(not(nullValue())));
		assertThat(adapter.mLayoutInflater, is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mAdapter.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandlerWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {
					@Override
					public void run() {
						mAdapter.getAnnotationHandler();
					}
				},
				DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED
		);
	}

	@Test
	@SuppressWarnings("ResourceType")
	public void testChangeCursor() {
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeLoaderData(mAdapter.getLoaderId(), new MockCursor(0));
		assertThat(mAdapter.getCursor(), is(not(nullValue())));
	}

	@Test
	public void testChangeCursorWithNotAdaptersLoaderId() {
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeLoaderData(0, new MockCursor(0));
	}

	@LoaderId(0x1002)
	@SuppressWarnings("ClassTooDeepInInheritanceTree")
	private static final class TestAdapter extends SpinnerCursorLoaderAdapter<TestModelCursor, TestModel, Void, Void> {

		private TestAdapter(@NonNull FragmentActivity context) {
			super(context);
		}

		private TestAdapter(@NonNull FragmentActivity context, @Nullable TestModelCursor cursor) {
			super(context, DataLoaderAdapterAssistants.<Cursor>createForActivity(context), cursor);
		}

		@Nullable
		@Override
		public Loader<Cursor> createLoader(int loaderId, @Nullable Bundle params) {
			return null;
		}

		@NonNull
		@Override
		public TestModelCursor wrapCursor(@NonNull Cursor cursor) {
			return TestModelCursor.wrap(cursor);
		}

		@Override
		protected void onUpdateViewHolder(@NonNull Void viewHolder, @NonNull TestModelCursor cursor, int position) {
		}
	}

	@SuppressWarnings("ClassTooDeepInInheritanceTree")
	private static final class TestAdapterWithoutAnnotation extends SpinnerCursorLoaderAdapter<TestModelCursor, TestModel, Void, Void> {

		private TestAdapterWithoutAnnotation(@NonNull FragmentActivity context) {
			super(context);
		}

		@Nullable
		@Override
		public Loader<Cursor> createLoader(int loaderId, @Nullable Bundle params) {
			return null;
		}

		@Override
		protected void onUpdateViewHolder(@NonNull Void viewHolder, @NonNull TestModelCursor cursor, int position) {
		}
	}
}
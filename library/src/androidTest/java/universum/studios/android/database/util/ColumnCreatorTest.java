/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.util;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.entity.TestModelEntity;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelWithUnsupportedColumns;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class ColumnCreatorTest {

	@SuppressWarnings("unused")
	private static final String TAG = "ColumnCreatorTest";

	private final TableColumn.ColumnCreator creator = new TableColumn.ColumnCreator();

	@Test
	public void testCanCreateFromField() throws Exception {
		assertThat(creator.canCreateFromField(TestModel.class.getField("author")), is(true));
	}

	@Test
	public void testCanCreateFromFieldWithoutAnnotation() throws Exception {
		assertThat(creator.canCreateFromField(TestModel.class.getField("address")), is(false));
	}

	@Test
	public void testCreateFromField() throws Exception {
		final Field field = TestModel.class.getField("enabled");
		final TableColumn column = creator.createFromField(field);
		assertThat(column, is(not(nullValue())));
		assertThat(column.name(), is(TestModelEntity.Columns.ENABLED));
		assertThat(column.type(), is(ColumnType.type(ColumnType.INTEGER)));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_NOT_NULL), is(true));
		assertThat((String) column.defaultValue(), is("1"));
	}

	@Test
	public void testCreateFromFieldWithTypeConstraint() throws Exception {
		final Field field = TestModel.class.getField("title");
		final TableColumn column = creator.createFromField(field);
		assertThat(column, is(not(nullValue())));
		assertThat(column.name(), is(TestModelEntity.Columns.TITLE));
		assertThat(column.type(), is(ColumnType.type(ColumnType.TEXT).constraint("56")));
	}

	@Test
	public void testCreateFromFieldWithPrimaryConstraint() throws Exception {
		final Field field = TestModel.class.getField("id");
		final TableColumn column = creator.createFromField(field);
		assertThat(column, is(not(nullValue())));
		assertThat(column.name(), is(TestModelEntity.Columns.ID));
		assertThat(column.type(), is(ColumnType.type(ColumnType.INTEGER)));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_PRIMARY_KEY), is(true));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_AUTOINCREMENT), is(true));
		assertThat(column.isPrimaryKey(), is(true));
	}

	@Test
	public void testCreateFromFieldWithPrimaryConstraintWithoutAutoIncrement() throws Exception {
		final Field field = TestColumns.class.getField("id");
		final TableColumn column = creator.createFromField(field);
		assertThat(column, is(not(nullValue())));
		assertThat(column.name(), is("id"));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_PRIMARY_KEY), is(true));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_AUTOINCREMENT), is(false));
		assertThat(column.isPrimaryKey(), is(true));
	}

	@Test
	public void testCreateFromFieldWithUniqueConstraint() throws Exception {
		final Field field = TestModel.class.getField("author");
		final TableColumn column = creator.createFromField(field);
		assertThat(column, is(not(nullValue())));
		assertThat(column.name(), is(TestModelEntity.Columns.AUTHOR));
		assertThat(column.type(), is(ColumnType.type(ColumnType.TEXT)));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_NOT_NULL), is(true));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_UNIQUE), is(true));
		assertThat(column.isUniqueKey(), is(true));
	}

	@Test
	public void testCreateFromFieldWithReference() throws Exception {
		final Field field = TestModel.class.getField("categoryId");
		final TableColumn column = creator.createFromField(field);
		assertThat(column, is(not(nullValue())));
		assertThat(column.name(), is(TestModelEntity.Columns.CATEGORY_ID));
		assertThat(column.type(), is(ColumnType.type(ColumnType.INTEGER)));
		assertThat(column.isForeignKey(), is(true));
		assertThat(column.reference(), is(TestModelEntity.References.CATEGORY_ID));
	}

	@Test
	public void testCreateFromFieldWithoutAnnotation() throws Exception {
		final Field field = TestModel.class.getField("date");
		assertCreateFromFieldThrowsException(field);
	}

	@Test
	public void testCreateFromFieldWithUnsupportedType() throws Exception {
		final Field field = TestModelWithUnsupportedColumns.class.getField("date");
		assertCreateFromFieldThrowsException(field);
	}

	private void assertCreateFromFieldThrowsException(Field field) {
		try {
			creator.createFromField(field);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Cannot create TableColumn from field(date) type of(Date). " +
							"Field is of unsupported type or does not present required @Column annotation.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testResolveColumnTypeForFieldWithRequestedType() throws Exception {
		final Field field = TestModel.class.getField("published");
		assertThat(creator.resolveColumnTypeForField(ColumnType.BOOLEAN, field), is(ColumnType.type(ColumnType.BOOLEAN)));
	}

	@Test
	public void testResolveColumnTypeForFieldWithIntegerAffinityAutomatically() throws Exception {
		final Field field = TestModel.class.getField("count");
		assertThat(creator.resolveColumnTypeForField(ColumnType.AUTO, field), is(ColumnType.type(ColumnType.INTEGER)));
	}

	@Test
	public void testResolveColumnTypeForFieldWithRealAffinityAutomatically() throws Exception {
		final Field field = TestModel.class.getField("ratio");
		assertThat(creator.resolveColumnTypeForField(ColumnType.AUTO, field), is(ColumnType.type(ColumnType.REAL)));
	}

	@Test
	public void testResolveColumnTypeForFieldWithTextAffinityAutomatically() throws Exception {
		final Field field = TestModel.class.getField("description");
		assertThat(creator.resolveColumnTypeForField(ColumnType.AUTO, field), is(ColumnType.type(ColumnType.TEXT)));
	}

	@Test
	public void testResolveColumnTypeForFieldWithUnknownAffinityAutomatically() throws Exception {
		final Field field = TestModel.class.getField("date");
		assertThat(creator.resolveColumnTypeForField(ColumnType.AUTO, field), is(nullValue()));
	}

	private static final class TestColumns {

		@Column
		@Column.Primary(autoincrement = false)
		public Long id;
	}
}
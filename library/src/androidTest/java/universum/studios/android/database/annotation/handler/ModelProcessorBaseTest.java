/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.inner.BaseTest;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelWithUnsupportedColumns;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
abstract class ModelProcessorBaseTest<P extends ModelProcessor> extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "ModelProcessorBaseTest";

	final P mProcessor;
	final AnnotatedColumn[] mColumns;
	final AnnotatedColumn[] mUnsupportedColumns;
	TestModel mModel;
	TestModelWithUnsupportedColumns mModelWithUnsupportedColumns;

	@SuppressWarnings("ConstantConditions")
	ModelProcessorBaseTest(P processor) {
		this.mProcessor = processor;
		this.mColumns = EntityModelAnnotationHandlers.obtainModelHandler(TestModel.class).getColumns();
		this.mUnsupportedColumns = EntityModelAnnotationHandlers.obtainModelHandler(TestModelWithUnsupportedColumns.class).getColumns();
	}

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mModel = new TestModel();
		this.mModelWithUnsupportedColumns = new TestModelWithUnsupportedColumns();
	}

	static void assertThatProcessingThrowsExceptionWithMessage(Runnable processingAction, String message) {
		try {
			processingAction.run();
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			final String eMessage = e.getMessage();
			if (!message.contentEquals(eMessage)) {
				throw new AssertionError(
						"Expected exception with message <" + message + "> but message was <" + eMessage + ">"
				);
			}
			return;
		}
		throw new AssertionError("No exception has been thrown when executing processing action.");
	}
}
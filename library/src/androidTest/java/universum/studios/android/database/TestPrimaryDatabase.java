/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import universum.studios.android.database.annotation.DatabaseEntities;
import universum.studios.android.database.annotation.PrimaryDatabase;
import universum.studios.android.database.entity.TestEntity;
import universum.studios.android.database.entity.TestJoinEntity;
import universum.studios.android.database.entity.TestModelEntity;
import universum.studios.android.database.entity.TestModelEntity2;
import universum.studios.android.database.entity.TestTypesEntity;
import universum.studios.android.database.loremipsum.LoremIpsumEntity;

/**
 * @author Martin Albedinsky
 */
@DatabaseEntities({
		TestEntity.class,
		TestTypesEntity.class,
		TestJoinEntity.class,
		TestModelEntity.class,
		TestModelEntity2.class,
		LoremIpsumEntity.class
})
@PrimaryDatabase
public class TestPrimaryDatabase extends Database {

	@SuppressWarnings("unused")
	private static final String TAG = "TestPrimaryDatabase";

	public TestPrimaryDatabase() {
		super(1, "test.db");
	}
}

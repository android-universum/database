/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.cursor;

import android.database.Cursor;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.entity.TestModelEntity;
import universum.studios.android.database.model.TestModel;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class CursorUtilsTest {

	@SuppressWarnings("unused")
	private static final String TAG = "CursorUtilsTest";

	private static final String NOT_PROJECTED_COLUMN = "no_column";

	private Cursor mCursor = TestModel.createCursorWithTestValues();

	@Test
	public void testObtainShort() {
		assertThat(CursorUtils.obtainShort(mCursor, TestModelEntity.Columns.CODE), is(TestModel.TestValues.CODE));
	}

	@Test
	public void testObtainShortForNotProjectedColumn() {
		assertThat(CursorUtils.obtainShort(mCursor, NOT_PROJECTED_COLUMN), is((short) 0));
	}

	@Test
	public void testObtainInt() {
		assertThat(CursorUtils.obtainInt(mCursor, TestModelEntity.Columns.FLAGS), is(TestModel.TestValues.FLAGS));
	}

	@Test
	public void testObtainIntForNotProjectedColumn() {
		assertThat(CursorUtils.obtainInt(mCursor, NOT_PROJECTED_COLUMN), is(0));
	}

	@Test
	public void testObtainLong() {
		assertThat(CursorUtils.obtainLong(mCursor, TestModelEntity.Columns.ID), is(TestModel.TestValues.ID));
	}

	@Test
	public void testObtainLongForNotProjectedColumn() {
		assertThat(CursorUtils.obtainLong(mCursor, NOT_PROJECTED_COLUMN), is(0L));
	}

	@Test
	public void testObtainFloat() {
		assertThat(CursorUtils.obtainFloat(mCursor, TestModelEntity.Columns.RATIO), is(TestModel.TestValues.RATIO));
	}

	@Test
	public void testObtainFloatForNotProjectedColumn() {
		assertThat(CursorUtils.obtainFloat(mCursor, NOT_PROJECTED_COLUMN), is(0f));
	}

	@Test
	public void testObtainDouble() {
		assertThat(CursorUtils.obtainDouble(mCursor, TestModelEntity.Columns.LATITUDE), is(TestModel.TestValues.LATITUDE));
	}

	@Test
	public void testObtainDoubleForNotProjectedColumn() {
		assertThat(CursorUtils.obtainDouble(mCursor, NOT_PROJECTED_COLUMN), is(0d));
	}

	@Test
	public void testObtainString() {
		assertThat(CursorUtils.obtainString(mCursor, TestModelEntity.Columns.AUTHOR), is(TestModel.TestValues.AUTHOR));
	}

	@Test
	public void testObtainStringForNotProjectedColumn() {
		assertThat(CursorUtils.obtainString(mCursor, NOT_PROJECTED_COLUMN), is(""));
	}

	@Test
	public void testObtainBoolean() {
		assertThat(CursorUtils.obtainBoolean(mCursor, TestModelEntity.Columns.PUBLISHED), is(TestModel.TestValues.PUBLISHED));
	}

	@Test
	public void testObtainBooleanForNotProjectedColumn() {
		assertThat(CursorUtils.obtainBoolean(mCursor, NOT_PROJECTED_COLUMN), is(false));
	}

	@Test
	public void testObtainIntBoolean() {
		assertThat(CursorUtils.obtainIntBoolean(mCursor, TestModelEntity.Columns.ENABLED), is(TestModel.TestValues.ENABLED));
	}

	@Test
	public void testObtainIntBooleanForNotProjectedColumn() {
		assertThat(CursorUtils.obtainIntBoolean(mCursor, NOT_PROJECTED_COLUMN), is(false));
	}
}

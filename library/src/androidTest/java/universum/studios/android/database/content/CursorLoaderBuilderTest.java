/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import androidx.test.annotation.UiThreadTest;
import androidx.test.rule.UiThreadTestRule;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.entity.TestModelEntity2;
import universum.studios.android.database.inner.ContextBaseTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class CursorLoaderBuilderTest extends ContextBaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "CursorLoaderBuilderTest";

	@Rule public final UiThreadTestRule UI_RULE = new UiThreadTestRule();

	private CursorLoaderBuilder mBuilder;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mBuilder = new CursorLoaderBuilder(TestModelEntity2.URI);
	}

	@Test
	public void testInstantiationWithUri() {
		final CursorLoaderBuilder builder = new CursorLoaderBuilder(TestModelEntity2.URI);
		assertThat(builder.getUri(), is(TestModelEntity2.URI));
	}

	@Test
	public void testDefaultProjection() {
		assertThat(mBuilder.projection(), is(nullValue()));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testProjection() {
		mBuilder.projection(new Projection().columns("column1", "column2"));
		final Projection projection = mBuilder.projection();
		assertThat(projection, is(not(nullValue())));
		assertThat(projection.columns().size(), is(2));
		// We do not perform any tests further as we assume that the Projection class
		// has been properly tested.
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testProjectionRaw() {
		mBuilder.projection(new String[]{"column1", "column2"});
		final Projection projection = mBuilder.projection();
		assertThat(projection, is(not(nullValue())));
		assertThat(projection.columns().size(), is(2));
		// We do not perform any tests further as we assume that the Projection class
		// has been properly tested.
	}

	@Test
	public void testDefaultSelection() {
		assertThat(mBuilder.selection(), is(nullValue()));
	}

	@Test
	public void testSelection() {
		mBuilder.selection(new Selection("count>0 AND enabled=1"));
		final Selection selection = mBuilder.selection();
		assertThat(selection, is(not(nullValue())));
		// We do not perform any tests further as we assume that the Selection class
		// has been properly tested.
	}

	@Test
	public void testSelectionRaw() {
		mBuilder.selection("published=1");
		final Selection selection = mBuilder.selection();
		assertThat(selection, is(not(nullValue())));
		// We do not perform any tests further as we assume that the Selection class
		// has been properly tested.
	}

	@Test
	public void testSelectionArgs() {
		mBuilder.selectionArgs(Selection.argsAsStringArray("Rome", 12, false));
		assertThat(mBuilder.selectionArgs(), is(new String[]{"Rome", "12", "false"}));
		// We do not perform any extensive testing here as we assume that the Selection class
		// has been properly tested.
	}

	@Test
	public void testDefaultSortOrder() {
		assertThat(mBuilder.sortOrder(), is(nullValue()));
	}

	@Test
	public void testSortOrder() {
		mBuilder.sortOrder(new SortOrder.DESC());
		assertThat(mBuilder.sortOrder(), is((SortOrder) new SortOrder.DESC()));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testSortOrderRaw() {
		mBuilder.sortOrder("ASC", new String[]{"title", "author"});
		final SortOrder sortOrder = mBuilder.sortOrder();
		assertThat(sortOrder, is(not(nullValue())));
		assertThat(sortOrder.keyWord(), is("ASC"));
		assertThat(Arrays.equals(new String[]{"title", "author"}, listToArray(sortOrder.columns())), is(true));
	}

	@Test
	@UiThreadTest
	public void testBuild() {
		assertThat(mBuilder.build(context), is(not(nullValue())));
		// We do not perform any extensive testing here as we assume that all loader argument builder
		// implementations has been properly tested.
	}

	private static String[] listToArray(List<String> list) {
		final String[] array = new String[list.size()];
		list.toArray(array);
		return array;
	}
}
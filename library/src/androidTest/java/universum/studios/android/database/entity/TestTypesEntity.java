/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.EntityName;

/**
 * @author Martin Albedinsky
 */
@EntityName(TestTypesEntity.NAME)
public final class TestTypesEntity extends Entity {

	@SuppressWarnings("unused")
	private static final String TAG = "TestTypesEntity";
	public static final String NAME = "test_types";

	public interface Columns {
		String ID = Column.Primary.COLUMN_NAME;
		String NAME = "title";
	}

	@Override
	protected void onCreate(@NonNull SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + NAME + " ("
				+ Columns.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ Columns.NAME + " TEXT NOT NULL"
				+ ")"
		);
	}
}

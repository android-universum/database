/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.inner;

import junit.framework.TestCase;

import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;

/**
 * @author Martin Albedinsky
 */
public final class DatabaseTests extends TestCase {

	@SuppressWarnings("unused")
	private static final String TAG = "DatabaseTests";

	public static final String EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED =
			"Trying to access logic that requires annotations processing to be enabled, " +
					"but it seams that the annotations processing is disabled for the Database library.";

	public static void assertThatActionWithDisabledAnnotationsProcessingThrowsException(Runnable action, String expectedMessage) {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		try {
			action.run();
		} catch (DatabaseException e) {
			assertEquals(DatabaseException.TYPE_MISCONFIGURATION, e.getType());
			assertEquals(expectedMessage, e.getMessage());
			DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
			return;
		}
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		throw new AssertionError("No exception thrown.");
	}
}

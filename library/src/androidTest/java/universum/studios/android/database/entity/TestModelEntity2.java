/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.database.Database;
import universum.studios.android.database.TestPrimaryProvider;
import universum.studios.android.database.annotation.EntityName;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.model.TestModel;

/**
 * @author Martin Albedinsky
 */
@Model(TestModel.class)
@EntityName(TestModelEntity2.NAME)
public final class TestModelEntity2 extends TestModelEntity {

	@SuppressWarnings("unused")
	private static final String TAG = "TestModelEntity2";

	public static final String NAME = "test_models_2";
	public static final Uri URI = Database.createContentUri(TestPrimaryProvider.AUTHORITY, NAME);
	public static final int INITIAL_CONTENT_SIZE = 100;

	@Override
	public boolean createsInitialContent() {
		return true;
	}

	@Override
	protected void onCreateInitialContent(@NonNull SQLiteDatabase db) {
		final List<TestModel> models = new ArrayList<>(INITIAL_CONTENT_SIZE);
		for (int i = 0; i < INITIAL_CONTENT_SIZE; i++) {
			final TestModel model = TestModel.withInitialContent();
			model.tag = "model.tag:" + Integer.toString(i);
			model.author = "model.author:" + Integer.toString(i + 1);
			models.add(model);
		}
		insertInitialModels(db, models);
	}
}

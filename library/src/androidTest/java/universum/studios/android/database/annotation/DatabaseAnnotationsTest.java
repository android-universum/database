/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class DatabaseAnnotationsTest {

	@SuppressWarnings("unused")
	private static final String TAG = "DatabaseAnnotationsTest";

	@Test
	public void testCheckIfEnabledOrThrow() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
	}

	@Test
	public void testCheckIfEnabledOrThrowForDisabledAnnotations() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		try {
			DatabaseAnnotations.checkIfEnabledOrThrow();
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Trying to access logic that requires annotations processing to be enabled, " +
							"but it seams that the annotations processing is disabled for the Database library.")
			);
		}
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
	}
}

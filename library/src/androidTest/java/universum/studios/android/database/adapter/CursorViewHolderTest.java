/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.view.View;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.inner.ContextBaseTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class CursorViewHolderTest extends ContextBaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "CursorViewHolderTest";

	private View mItemView;
	private CursorViewHolder mHolder;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mItemView = new View(context);
		this.mHolder = new CursorViewHolder(mItemView);
	}

	@Test
	public void testInstantiation() {
		final CursorViewHolder holder = new CursorViewHolder(mItemView);
		assertThat(holder.itemView, is(mItemView));
		assertThat(holder.getAdapterPosition(), is(CursorDataSetAdapter.NO_POSITION));
	}

	@Test
	public void testUpdateAdapterPosition() {
		mHolder.updateAdapterPosition(13);
		assertThat(mHolder.getAdapterPosition(), is(13));
	}
}
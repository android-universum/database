/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.util.AndroidRuntimeException;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class DatabaseTransactionTest {

	@SuppressWarnings("unused")
	private static final String TAG = "DatabaseTransactionTest";

	@Test
	public void testInstantiation() {
		new TestTransaction();
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testExecute() {
		assertThat(new TestTransaction(false).execute(), is(false));
		assertThat(new TestTransaction(true).execute(), is(true));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testExecuteWithException() {
		final TestTransaction transaction = new TestTransaction();
		assertThat(transaction.execute(), is(false));
		assertThat(transaction.onErrorReceived, is(true));
	}

	private static final class TestTransaction extends DatabaseTransaction<Boolean, TestPrimaryDatabase> {

		boolean onErrorReceived;
		private final Boolean result;

		private TestTransaction() {
			this(null);
		}

		private TestTransaction(Boolean result) {
			super(TestPrimaryDatabase.class);
			this.result = result;
		}

		@NonNull
		@Override
		protected Boolean onExecute(@NonNull TestPrimaryDatabase database) {
			if (result == null) throw new AndroidRuntimeException("No test result specified.");
			return result;
		}

		@Override
		protected Boolean onError(@NonNull TestPrimaryDatabase database, @NonNull Throwable error) {
			this.onErrorReceived = true;
			return false;
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import androidx.annotation.NonNull;

/**
 * @author Martin Albedinsky
 */
public final class TestJoinEntity extends JoinEntity {

	@SuppressWarnings("unused")
	private static final String TAG = "TestJoinEntity";

	private String mJoinStatement;

	public TestJoinEntity() {
		setNotificationEntity(TestEntity.class);
	}

	void setJoinStatement(String statement) {
		this.mJoinStatement = statement;
	}

	@NonNull
	@Override
	protected String onCreateJoinStatement() {
		return mJoinStatement;
	}
}

/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.inner;

import java.lang.reflect.Array;

/**
 * @author Martin Albedinsky
 */
public final class TestUtils {

	@SuppressWarnings("unused")
	private static final String TAG = "TestUtils";

	public static <T> T[] copyOf(T[] original, int newLength) {
		if (original == null) {
			throw new NullPointerException("original == null");
		}
		if (newLength < 0) {
			throw new NegativeArraySizeException(Integer.toString(newLength));
		}
		return copyOfRange(original, 0, newLength);
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] copyOfRange(T[] original, int start, int end) {
		int originalLength = original.length; // For exception priority compatibility.
		if (start > end) {
			throw new IllegalArgumentException();
		}
		if (start < 0 || start > originalLength) {
			throw new ArrayIndexOutOfBoundsException();
		}
		int resultLength = end - start;
		int copyLength = Math.min(resultLength, originalLength - start);
		T[] result = (T[]) Array.newInstance(original.getClass().getComponentType(), resultLength);
		System.arraycopy(original, start, result, 0, copyLength);
		return result;
	}
}

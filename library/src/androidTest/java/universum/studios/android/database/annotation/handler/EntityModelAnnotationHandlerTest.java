/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.TestModel;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class EntityModelAnnotationHandlerTest extends AnnotationHandlerBaseTest<EntityModelAnnotationHandler> {


	@SuppressWarnings("unused")
	private static final String TAG = "EntityModelAnnotationHandlerTest";

	public EntityModelAnnotationHandlerTest() {
		super(EntityModelAnnotationHandlers.ModelHandler.class);
	}

	@Override
	EntityModelAnnotationHandler onObtainHandler() {
		return EntityModelAnnotationHandlers.obtainModelHandler(TestModel.class);
	}

	@Test
	public void testCreateModel() {
		final EntityModel model = mHandler.createModel();
		assertThat(model, is(not(nullValue())));
		assertThat(model, is(instanceOf(TestModel.class)));
	}
}
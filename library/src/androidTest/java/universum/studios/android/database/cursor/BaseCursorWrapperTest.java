/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.cursor;

import android.database.Cursor;
import android.database.MatrixCursor;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.entity.TestModelEntity;
import universum.studios.android.database.inner.BaseTest;
import universum.studios.android.database.mock.MockCursor;
import universum.studios.android.database.model.TestModel;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class BaseCursorWrapperTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "BaseCursorWrapperTest";

	private static final int TEST_CURSOR_ROWS_COUNT = 10;

	private TestWrapper mWrapper;
	private TestWrapper mEmptyWrapper;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mWrapper = new TestWrapper(TestModel.createCursorWithTestValues(TEST_CURSOR_ROWS_COUNT));
		this.mEmptyWrapper = new TestWrapper(new MockCursor(0));
	}

	@Test
	public void testInstantiation() {
		final Cursor cursor = new MockCursor(0);
		final TestWrapper wrapper = new TestWrapper(cursor);
		assertThat(wrapper.getWrappedCursor(), is(cursor));
		assertThat(wrapper.getPosition(), is(-1));
	}

	@Test
	public void testInstantiationWithAlreadyMovedCursor() {
		final Cursor cursor = new MockCursor(5);
		cursor.moveToPosition(2);
		final TestWrapper wrapper = new TestWrapper(cursor);
		assertThat(wrapper.getWrappedCursor(), is(cursor));
		assertThat(wrapper.getPosition(), is(2));
	}

	@Test
	public void testMoveToFirst() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getId(), is(1L));
		assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));
	}

	@Test
	public void testMoveToFirstOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
		assertThat(mEmptyWrapper.author, is(nullValue()));
	}

	@Test
	public void testMoveToLast() {
		assertThat(mWrapper.moveToLast(), is(true));
		assertThat(mWrapper.getId(), is((long) TEST_CURSOR_ROWS_COUNT));
		assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));
	}

	@Test
	public void testMoveToLastOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToLast(), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
		assertThat(mEmptyWrapper.author, is(nullValue()));
	}

	@Test
	public void testMoveToNext() {
		mWrapper.moveToFirst();
		for (int i = 0; i < TEST_CURSOR_ROWS_COUNT; i++) {
			assertThat(mWrapper.getId(), is(i + 1L));
			assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));
			if (i < (TEST_CURSOR_ROWS_COUNT - 1)) assertThat(mWrapper.moveToNext(), is(true));
		}
	}

	@Test
	public void testMoveToNextAfterLastPosition() {
		mWrapper.moveToLast();
		assertThat(mWrapper.moveToNext(), is(false));
		assertThat(mWrapper.getId(), is(-1L));
		assertThat(mWrapper.author, is(nullValue()));
	}

	@Test
	public void testMoveToNextOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.moveToNext(), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
		assertThat(mEmptyWrapper.author, is(nullValue()));
	}

	@Test
	public void testMoveToPrevious() {
		assertThat(mWrapper.moveToLast(), is(true));
		for (int i = TEST_CURSOR_ROWS_COUNT - 1; i >= 0; i--) {
			assertThat(mWrapper.getId(), is(i + 1L));
			assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));
			if (i > 0) assertThat(mWrapper.moveToPrevious(), is(true));
		}
	}

	@Test
	public void testMoveToPreviousBeforeFirstPosition() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.moveToPrevious(), is(false));
		assertThat(mWrapper.getId(), is(-1L));
		assertThat(mWrapper.author, is(nullValue()));
	}

	@Test
	public void testMoveToPreviousOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToLast(), is(false));
		assertThat(mEmptyWrapper.moveToPrevious(), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
		assertThat(mEmptyWrapper.author, is(nullValue()));
	}

	@Test
	public void testMoveToPosition() {
		for (int i = 0; i < TEST_CURSOR_ROWS_COUNT; i++) {
			assertThat(mWrapper.moveToPosition(i), is(true));
			assertThat(mWrapper.getId(), is(i + 1L));
			assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));
		}
	}

	@Test
	public void testMoveToPositionOutOfRange() {
		assertThat(mWrapper.moveToPosition(-10), is(false));
	}

	@Test
	public void testMoveToPositionOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToPosition(0), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
		assertThat(mEmptyWrapper.author, is(nullValue()));
		assertThat(mEmptyWrapper.moveToPosition(5), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
		assertThat(mEmptyWrapper.author, is(nullValue()));
	}

	@Test
	public void testMove() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.move(3), is(true));
		assertThat(mWrapper.getId(), is(4L));
		assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));

		assertThat(mWrapper.move(5), is(true));
		assertThat(mWrapper.getId(), is(9L));
		assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));

		assertThat(mWrapper.move(-8), is(true));
		assertThat(mWrapper.getId(), is(1L));
		assertThat(mWrapper.author, is(TestModel.TestValues.AUTHOR));
	}

	@Test
	public void testMoveOutOfRange() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.move(-10), is(false));
		assertThat(mWrapper.getId(), is(-1L));
		assertThat(mWrapper.author, is(nullValue()));

		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.move(TEST_CURSOR_ROWS_COUNT + 10), is(false));
		assertThat(mWrapper.getId(), is(-1L));
		assertThat(mWrapper.author, is(nullValue()));
	}

	@Test
	public void testMoveOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.move(3), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
		assertThat(mEmptyWrapper.author, is(nullValue()));
	}

	@Test
	public void testDefaultGetId() {
		assertThat(mEmptyWrapper.getId(), is(-1L));
	}

	@Test
	public void testGetId() {
		assertThat(mWrapper.moveToPosition(2), is(true));
		assertThat(mWrapper.getId(), is(3L));
	}

	@Test
	public void testGetIdOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToPosition(2), is(false));
		assertThat(mEmptyWrapper.getId(), is(-1L));
	}

	@Test
	public void testGetShort() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getShort(TestModelEntity.Columns.CODE), is(TestModel.TestValues.CODE));
	}

	@Test
	public void testGetShortOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getShort(TestModelEntity.Columns.COUNT), is((short) 0));
	}

	@Test
	public void testGetInt() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getInt(TestModelEntity.Columns.FLAGS), is(TestModel.TestValues.FLAGS));
	}

	@Test
	public void testGetIntOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getInt(TestModelEntity.Columns.FLAGS), is(0));
	}

	@Test
	public void testGetLong() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getLong(TestModelEntity.Columns.ID), is(1L));
	}

	@Test
	public void testGetLongOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getLong(TestModelEntity.Columns.ID), is(0L));
	}

	@Test
	public void testGetFloat() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getFloat(TestModelEntity.Columns.RATIO), is(TestModel.TestValues.RATIO));
	}

	@Test
	public void testGetFloatOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getFloat(TestModelEntity.Columns.RATIO), is(0f));
	}

	@Test
	public void testGetDouble() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getDouble(TestModelEntity.Columns.LONGITUDE), is(TestModel.TestValues.LONGITUDE));
	}

	@Test
	public void testGetDoubleOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getDouble(TestModelEntity.Columns.LONGITUDE), is(0d));
	}

	@Test
	public void testGetBoolean() {
		final MatrixCursor cursor = new MatrixCursor(new String[]{TestModelEntity.Columns.ENABLED}, 1);
		cursor.addRow(new Object[]{TestModel.TestValues.ENABLED});
		final TestWrapper wrapper = new TestWrapper(cursor);
		assertThat(wrapper.moveToFirst(), is(true));
		assertThat(wrapper.getBoolean(TestModelEntity.Columns.ENABLED), is(TestModel.TestValues.ENABLED));
	}

	@Test
	public void testGetBooleanOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getBoolean(TestModelEntity.Columns.ENABLED), is(false));
	}

	@Test
	public void testGetIntBoolean() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getIntBoolean(TestModelEntity.Columns.PUBLISHED), is(TestModel.TestValues.PUBLISHED));
	}

	@Test
	public void testGetIntBooleanOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getIntBoolean(TestModelEntity.Columns.PUBLISHED), is(false));
	}

	@Test
	public void testGetString() {
		assertThat(mWrapper.moveToFirst(), is(true));
		assertThat(mWrapper.getString(TestModelEntity.Columns.AUTHOR), is(TestModel.TestValues.AUTHOR));
	}

	@Test
	public void testGetStringOnEmptyWrapper() {
		assertThat(mEmptyWrapper.moveToFirst(), is(false));
		assertThat(mEmptyWrapper.getString(TestModelEntity.Columns.AUTHOR), is(""));
	}

	private static class TestWrapper extends BaseCursorWrapper {

		String author;

		private TestWrapper(@NonNull Cursor cursor) {
			super(cursor);
		}

		@Override
		protected boolean onBindData() {
			this.author = getString(TestModelEntity.Columns.AUTHOR);
			return true;
		}

		@Override
		protected void onClearData() {
			this.author = null;
		}
	}
}
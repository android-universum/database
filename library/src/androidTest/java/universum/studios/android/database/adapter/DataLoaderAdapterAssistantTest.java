/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.test.annotation.UiThreadTest;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.content.LoaderAssistantBaseTest;
import universum.studios.android.database.content.TestLoaderAssistant;
import universum.studios.android.database.mock.MockCursor;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelCursor;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class DataLoaderAdapterAssistantTest extends LoaderAssistantBaseTest<Cursor, DataLoaderAdapterAssistantTest.TestAssistant> {

	@SuppressWarnings("unused")
	private static final String TAG = "DataLoaderAdapterAssistantTest";

	private static final int LOADER_ID = 0x1001;
	private static final int UNHANDLED_LOADER_ID = 0;

	private TestAdapter mAdapter;
	private Loader<Cursor> mLoader;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mAdapter = new TestAdapter(activity);
		try {
			ACTIVITY_RULE.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					mLoader = new CursorLoader(activity);
				}
			});
		} catch (Throwable throwable) {
			throwable.printStackTrace();
		}
	}

	@Override
	protected TestAssistant onCreateAssistant(FragmentActivity activity) {
		return new TestAssistant(activity, activity.getSupportLoaderManager());
	}

	@Test
	public void testDefaultGetAttachedAdapter() {
		assertThat(assistant.getAttachedAdapter(), is(nullValue()));
	}

	@Test
	public void testGetAttachedAdapter() {
		this.attachAdapterToAssistant();
		assertThat(assistant.getAttachedAdapter(), is((LoadableAdapter<Cursor>) mAdapter));
	}

	@Test
	@UiThreadTest
	public void testOnCreateLoader() {
		this.attachAdapterToAssistant();
		assertThat(assistant.onCreateLoader(LOADER_ID, null), is(not(nullValue())));
	}

	@Test
	@UiThreadTest
	public void testOnCreateLoaderWithUnhandledLoaderId() {
		this.attachAdapterToAssistant();
		assertThat(assistant.onCreateLoader(UNHANDLED_LOADER_ID, null), is(nullValue()));
	}

	@Test
	@UiThreadTest
	public void testOnCreateLoaderWithoutAttachedAdapter() {
		assertThat(assistant.onCreateLoader(LOADER_ID, null), is(nullValue()));
	}

	@Test
	public void testOnLoadFinished() {
		this.attachAdapterToAssistant();
		assertThat(mAdapter.getCursor(), is(nullValue()));
		assistant.onLoadFinished(LOADER_ID, new MockCursor(0));
		assertThat(mAdapter.getCursor(), is(not(nullValue())));
	}

	@Test
	public void testOnLoadFinishedWitUnhandledLoaderId() {
		this.attachAdapterToAssistant();
		assertThat(mAdapter.getCursor(), is(nullValue()));
		assistant.onLoadFinished(UNHANDLED_LOADER_ID, new MockCursor(0));
		assertThat(mAdapter.getCursor(), is(not(nullValue())));
	}

	@Test
	public void testOnLoadFinishedWithoutAttachedAdapter() {
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(0));
		assistant.onLoadFinished(LOADER_ID, new MockCursor(10));
		assertThat(mAdapter.getCount(), is(0));
	}

	@Test
	public void testOnLoadFailed() {
		this.attachAdapterToAssistant();
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(0));
		assistant.onLoadFailed(LOADER_ID);
		assertThat(mAdapter.getCursor(), is(nullValue()));
	}

	@Test
	public void testOnLoadFailedWithUnhandledLoaderId() {
		this.attachAdapterToAssistant();
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(10));
		assistant.onLoadFailed(UNHANDLED_LOADER_ID);
		assertThat(mAdapter.getCursor(), is(nullValue()));
	}

	@Test
	public void testOnLoadFailedWithoutAttachedAdapter() {
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(10));
		assistant.onLoadFailed(LOADER_ID);
		assertThat(mAdapter.getCursor(), is(not(nullValue())));
		assertThat(mAdapter.getCount(), is(10));
	}

	@Test
	public void testOnLoaderReset() {
		this.attachAdapterToAssistant();
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(0));
		assistant.onLoaderReset(mLoader);
		assertThat(mAdapter.getCursor(), is(nullValue()));
	}

	@Test
	public void testOnLoaderResetWithUnhandledLoaderId() {
		this.attachAdapterToAssistant();
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(10));
		assistant.onLoaderReset(mLoader);
		assertThat(mAdapter.getCursor(), is(nullValue()));
	}

	@Test
	public void testOnLoaderResetWithoutAttachedAdapter() {
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(10));
		assistant.onLoaderReset(mLoader);
		assertThat(mAdapter.getCursor(), is(not(nullValue())));
		assertThat(mAdapter.getCount(), is(10));
	}

	private void attachAdapterToAssistant() {
		assistant.attachAdapter(mAdapter);
	}

	static final class TestAssistant extends DataLoaderAdapterAssistant<Cursor> implements TestLoaderAssistant<Cursor> {

		private TestAssistant(@NonNull Context context, @NonNull LoaderManager loaderManager) {
			super(context, loaderManager);
		}

		@Override
		public int getLoaderId() {
			return 0;
		}

		@Nullable
		@Override
		public Bundle getLoaderParams() {
			return null;
		}
	}

	private static final class TestAdapter extends CursorLoaderAdapter<TestModelCursor, TestModel, Void> {

		public TestAdapter(@NonNull FragmentActivity context) {
			super(context);
			setLoaderId(LOADER_ID);
		}

		@Nullable
		@Override
		public Loader<Cursor> createLoader(@IntRange(from = 0) int loaderId, @Nullable Bundle params) {
			return loaderId == getLoaderId() ? new CursorLoader(mContext) : null;
		}

		@NonNull
		@Override
		public TestModelCursor wrapCursor(@NonNull Cursor cursor) {
			return TestModelCursor.wrap(cursor);
		}

		@Override
		protected void onBindViewHolder(@NonNull Void viewHolder, @NonNull TestModelCursor cursor, int position) {
		}
	}
}
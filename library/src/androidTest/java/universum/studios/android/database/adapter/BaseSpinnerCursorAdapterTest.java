/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.CursorDropDownView;
import universum.studios.android.database.annotation.CursorItemView;
import universum.studios.android.database.inner.DatabaseTests;
import universum.studios.android.database.mock.MockCursor;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelCursor;
import universum.studios.android.database.test.R;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class BaseSpinnerCursorAdapterTest extends AdapterBaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "BaseSpinnerCursorAdapterTest";

	private static final int TEST_CURSOR_ROWS_COUNT = 20;

	private ViewGroup mParentView;
	private View mItemView;
	private TestAdapter mAdapter, mEmptyAdapter;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mParentView = new ListView(context);
		this.mItemView = LayoutInflater.from(context).inflate(R.layout.test_item_list, mParentView, false);
		this.mAdapter = new TestAdapter(context);
		this.mAdapter.changeCursor(TestModel.createCursorWithTestValues(TEST_CURSOR_ROWS_COUNT));
		this.mEmptyAdapter = new TestAdapter(context);
	}

	@Test
	public void testInstantiation() {
		final TestAdapter adapter = new TestAdapter(context);
		assertAdapterProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithCursor() {
		final TestAdapter adapter = new TestAdapter(context, new TestModelCursor(new MockCursor(0)));
		assertAdapterProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(not(nullValue())));
		assertThat(adapter.getCursor(), is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithDisabledAnnotationsProcessing() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		final TestAdapter adapter = new TestAdapter(context);
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		assertAdapterProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(nullValue()));
	}

	private static void assertAdapterProperlyInstantiated(BaseSpinnerCursorAdapter adapter) {
		assertThat(adapter, is(not(nullValue())));
		assertThat(adapter.mContext, is(not(nullValue())));
		assertThat(adapter.mResources, is(not(nullValue())));
		assertThat(adapter.mLayoutInflater, is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mAdapter.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandlerWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(new Runnable() {
			@Override
			public void run() {
				mAdapter.getAnnotationHandler();
			}
		}, DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED);
	}

	@Test
	public void testOnCursorChange() {
		assertThat(mEmptyAdapter.getSelectedItemPosition(), is(TestAdapter.NO_POSITION));
		assertThat(mEmptyAdapter.getSelectedItemId(), is(TestAdapter.NO_ID));
		// We use Adapter.swapCursor(Cursor) so the index of id column is properly set up.
		mEmptyAdapter.swapCursor(new TestModelCursor(TestModel.createCursorWithTestValues(TEST_CURSOR_ROWS_COUNT)));
		assertThat(mEmptyAdapter.getSelectedItemPosition(), is(0));
		assertThat(mEmptyAdapter.getSelectedItemId(), is(1L));
	}

	@Test
	public void testOnCursorChangeWithNullCursor() {
		assertThat(mAdapter.getSelectedItemPosition(), is(0));
		assertThat(mAdapter.getSelectedItemId(), is(1L));
		// We use Adapter.swapCursor(Cursor) so the index of id column is properly set up.
		mAdapter.swapCursor(null);
		assertThat(mAdapter.getSelectedItemPosition(), is(TestAdapter.NO_POSITION));
		assertThat(mAdapter.getSelectedItemId(), is(TestAdapter.NO_ID));
	}

	@Test
	public void testOnCursorChangeWithEmptyCursor() {
		assertThat(mAdapter.getSelectedItemPosition(), is(0));
		assertThat(mAdapter.getSelectedItemId(), is(1L));
		// We use Adapter.swapCursor(Cursor) so the index of id column is properly set up.
		mAdapter.swapCursor(new TestModelCursor(new MockCursor(0)));
		assertThat(mAdapter.getSelectedItemPosition(), is(TestAdapter.NO_POSITION));
		assertThat(mAdapter.getSelectedItemId(), is(TestAdapter.NO_ID));
	}

	@Test
	public void testOnCursorChangeWithPreSelectedItemId() {
		assertThat(mEmptyAdapter.getSelectedItemPosition(), is(TestAdapter.NO_POSITION));
		assertThat(mEmptyAdapter.getSelectedItemId(), is(TestAdapter.NO_ID));
		mEmptyAdapter.setSelectedItemId(5);
		// We use Adapter.swapCursor(Cursor) so the index of id column is properly set up.
		mEmptyAdapter.swapCursor(new TestModelCursor(TestModel.createCursorWithTestValues(TEST_CURSOR_ROWS_COUNT)));
		assertThat(mEmptyAdapter.getSelectedItemPosition(), is(4));
		assertThat(mEmptyAdapter.getSelectedItemId(), is(5L));
	}

	@Test
	public void testOnCursorChangeWithAlreadySelectedItem() {
		assertThat(mAdapter.getSelectedItemPosition(), is(0));
		assertThat(mAdapter.getSelectedItemId(), is(1L));
		// We use Adapter.swapCursor(Cursor) so the index of id column is properly set up.
		mAdapter.swapCursor(new TestModelCursor(TestModel.createCursorWithTestValues(TEST_CURSOR_ROWS_COUNT)));
		assertThat(mAdapter.getSelectedItemPosition(), is(0));
		assertThat(mAdapter.getSelectedItemId(), is(1L));
	}

	@Test
	public void testOnCursorChangeWithAlreadySelectedItemNotInNewCursor() {
		assertThat(mAdapter.getSelectedItemPosition(), is(0));
		assertThat(mAdapter.getSelectedItemId(), is(1L));
		mAdapter.setSelectedItemId(19);
		assertThat(mAdapter.getSelectedItemPosition(), is(18));
		assertThat(mAdapter.getSelectedItemId(), is(19L));
		// We use Adapter.swapCursor(Cursor) so the index of id column is properly set up.
		mAdapter.swapCursor(new TestModelCursor(TestModel.createCursorWithTestValues(10)));
		assertThat(mAdapter.getSelectedItemPosition(), is(0));
		assertThat(mAdapter.getSelectedItemId(), is(1L));
	}

	@Test
	public void testGetSelectedItem() {
		assertThat(mAdapter.getSelectedItem(), is(not(nullValue())));
	}

	@Test
	public void testGetSelectedItemWithoutCursor() {
		try {
			mEmptyAdapter.getSelectedItem();
		} catch (IndexOutOfBoundsException e) {
			assertThat(
					e.getMessage(),
					is("Requested item at invalid position(-1). Data set has items in count of(0).")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testGetSelectedItemPosition() {
		mAdapter.getView(2, null, mParentView);
		assertThat(mAdapter.getSelectedItemPosition(), is(2));
	}

	@Test
	public void testGetSelectedPositionWithoutCursor() {
		assertThat(mEmptyAdapter.getSelectedItemPosition(), is(TestAdapter.NO_POSITION));
	}

	@Test
	public void testSetSelectedItemId() {
		mAdapter.setSelectedItemId(2);
		mAdapter.setSelectedItemId(2);
		assertThat(mAdapter.getSelectedItemId(), is(2L));
		assertThat(mAdapter.getSelectedItemPosition(), is(1));
	}

	@Test
	public void testSetSelectedItemIdWithoutCursor() {
		mEmptyAdapter.setSelectedItemId(2);
		assertThat(mEmptyAdapter.getSelectedItemId(), is(2L));
		assertThat(mEmptyAdapter.getSelectedItemPosition(), is(TestAdapter.NO_POSITION));
	}

	@Test
	public void testGetSelectedItemId() {
		mAdapter.getView(2, null, mParentView);
		assertThat(mAdapter.getSelectedItemId(), is(mAdapter.getItemId(2)));
	}

	@Test
	public void testGetSelectedItemIdWithoutCursor() {
		assertThat(mEmptyAdapter.getSelectedItemId(), is(TestAdapter.NO_ID));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetDropDownView() {
		final View view = mAdapter.getDropDownView(4, null, mParentView);
		assertThat(view instanceof TextView, is(true));
		final CursorViewHolder holder = (CursorViewHolder) view.getTag();
		assertThat(holder, is(not(nullValue())));
		assertThat(holder.getAdapterPosition(), is(4));
		assertThat(((TextView) view).getText().toString(), is("5"));
	}

	@Test
	public void testGetDropDownViewWithRecycledView() {
		mItemView.setTag(new CursorViewHolder(mItemView));
		final View view = mAdapter.getDropDownView(2, mItemView, mParentView);
		assertThat(mItemView, is(view));
		final CursorViewHolder holder = (CursorViewHolder) view.getTag();
		assertThat(holder, is(not(nullValue())));
		assertThat(holder.getAdapterPosition(), is(2));
		assertThat(((TextView) view).getText().toString(), is("3"));
	}

	@Test
	@SuppressWarnings("ClassTooDeepInInheritanceTree")
	public void testGetDropDownViewWithoutHolder() {
		final TestAdapterWithoutCursorViewHolder adapter = new TestAdapterWithoutCursorViewHolder(context, new MockCursor(1)) {
			@Override
			protected void onUpdateViewHolder(@NonNull View viewHolder, @NonNull Cursor cursor, int position) {
			}
		};
		final View view = adapter.getDropDownView(0, null, mParentView);
		assertThat(view instanceof TextView, is(true));
	}

	@Test
	@SuppressWarnings("ClassTooDeepInInheritanceTree")
	public void testGetDropDownViewWithoutHolderWithRecycledView() {
		final TestAdapterWithoutCursorViewHolder adapter = new TestAdapterWithoutCursorViewHolder(context, new MockCursor(1)) {
			@Override
			protected void onUpdateViewHolder(@NonNull View viewHolder, @NonNull Cursor cursor, int position) {
			}
		};
		final View view = adapter.getDropDownView(0, mItemView, mParentView);
		assertThat(mItemView, is(view));
	}

	@Test
	public void testOnCreateDropDownView() {
		final View view = mAdapter.onCreateDropDownView(mParentView, 0);
		assertThat(view instanceof TextView, is(true));
	}

	@Test
	public void testOnCreateDropDownViewWithoutDropDownViewAnnotation() {
		final TestAdapterWithoutDropDownViewAnnotation adapter = new TestAdapterWithoutDropDownViewAnnotation(context);
		// Adapter will use resource id specified via @CursorItemView annotation.
		final View view = adapter.onCreateDropDownView(mParentView, 0);
		assertThat(view instanceof TextView, is(true));
	}

	@Test
	public void testOnCreateDropDownViewWithoutAnnotation() {
		final TestAdapterWithoutAnnotation adapter = new TestAdapterWithoutAnnotation(context);
		try {
			adapter.onCreateView(mParentView, 0);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISSING_CLASS_ANNOTATION));
			assertThat(
					e.getMessage(),
					is("@CursorItemView annotation for class(TestAdapterWithoutAnnotation) is missing. " +
							"Cannot create view for the position(0) without item view layout resource.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testOnCreateDropDownViewHolder() {
		assertThat(mAdapter.onCreateDropDownViewHolder(mItemView, 0), is(not(nullValue())));
	}

	@Test
	public void testOnBindDropDownViewHolder() {
		final CursorViewHolder holder = new CursorViewHolder(mItemView);
		mAdapter.onBindDropDownViewHolder(holder, 2);
		assertThat(((TextView) holder.itemView).getText().toString(), is("3"));
	}

	@Test
	public void testOnBindDropDownViewHolderWithoutCursor() {
		mAdapter.swapCursor(null);
		try {
			mAdapter.onBindDropDownViewHolder(new CursorViewHolder(mItemView), 2);
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Cannot present data from the invalid cursor.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testOnBindDropDownViewHolderWithInvalidPosition() {
		try {
			mAdapter.onBindDropDownViewHolder(new CursorViewHolder(mItemView), TEST_CURSOR_ROWS_COUNT);
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Cannot move the attached cursor to the position(" + TEST_CURSOR_ROWS_COUNT + ") corrected as(" + TEST_CURSOR_ROWS_COUNT + ").")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testOnBindDropDownViewHolderCursor() {
		final CursorViewHolder holder = new CursorViewHolder(mItemView);
		mAdapter.onBindDropDownViewHolder(holder, mAdapter.getCursorAt(0), 0);
		assertThat(((TextView) holder.itemView).getText().toString(), is("1"));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testOnUpdateViewHolder() {
		final CursorViewHolder holder = new CursorViewHolder(mItemView);
		mAdapter.onUpdateViewHolder(holder, mAdapter.getCursorAt(4), 4);
		assertThat(((TextView) holder.itemView).getText().toString(), is("5"));
	}

	@CursorItemView(R.layout.test_item_list)
	@CursorDropDownView(R.layout.test_item_list)
	private static class TestAdapter extends BaseSpinnerCursorAdapter<TestModelCursor, TestModel, CursorViewHolder, CursorViewHolder> {

		public TestAdapter(@NonNull Context context) {
			super(context);
		}

		public TestAdapter(@NonNull Context context, @Nullable TestModelCursor cursor) {
			super(context, cursor);
		}

		@NonNull
		@Override
		public TestModelCursor wrapCursor(@NonNull Cursor cursor) {
			return TestModelCursor.wrap(cursor);
		}

		@Nullable
		@Override
		protected CursorViewHolder onCreateViewHolder(@NonNull View itemView, int position) {
			return new CursorViewHolder(itemView);
		}

		@Override
		@SuppressLint("SetTextI18n")
		protected void onUpdateViewHolder(@NonNull CursorViewHolder viewHolder, @NonNull TestModelCursor cursor, int position) {
			((TextView) viewHolder.itemView).setText(Long.toString(cursor.accessModel().id));
		}
	}

	@CursorItemView(R.layout.test_item_list)
	@CursorDropDownView(R.layout.test_item_list)
	private static class TestAdapterWithoutCursorViewHolder extends BaseSpinnerCursorAdapter<Cursor, String, View, View> {

		private TestAdapterWithoutCursorViewHolder(@NonNull Context context, @Nullable Cursor cursor) {
			super(context, cursor);
		}

		@Override
		protected void onUpdateViewHolder(@NonNull View viewHolder, @NonNull Cursor cursor, int position) {
		}
	}

	@CursorItemView(R.layout.test_item_list)
	private static final class TestAdapterWithoutDropDownViewAnnotation extends BaseSpinnerCursorAdapter<TestModelCursor, TestModel, Void, Void> {

		private TestAdapterWithoutDropDownViewAnnotation(@NonNull Context context) {
			super(context);
		}

		@Override
		protected void onUpdateViewHolder(@NonNull Void viewHolder, @NonNull TestModelCursor cursor, int position) {
		}
	}

	private static final class TestAdapterWithoutAnnotation extends BaseSpinnerCursorAdapter<TestModelCursor, TestModel, Void, Void> {

		public TestAdapterWithoutAnnotation(@NonNull Context context) {
			super(context);
		}

		@Override
		protected void onUpdateViewHolder(@NonNull Void viewHolder, @NonNull TestModelCursor cursor, int position) {
		}
	}
}
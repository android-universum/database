/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.entity.ModelEntity;
import universum.studios.android.database.entity.TestModelEntity;
import universum.studios.android.database.model.TestModel;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class ModelEntityAnnotationHandlerTest extends AnnotationHandlerBaseTest<ModelEntityAnnotationHandler> {

	@SuppressWarnings("unused")
	private static final String TAG = "ModelEntityAnnotationHandlerTest";

	public ModelEntityAnnotationHandlerTest() {
		super(ModelEntityAnnotationHandlers.ModelEntityHandler.class);
	}

	@Override
	ModelEntityAnnotationHandler onObtainHandler() {
		return ModelEntityAnnotationHandlers.obtainModelEntityHandler(TestModelEntity.class);
	}

	@Override
	ModelEntityAnnotationHandler onObtainEmptyHandler() {
		return ModelEntityAnnotationHandlers.obtainModelEntityHandler(TestEntityWithoutAnnotation.class);
	}

	@Test
	public void testDefaultGetModelClass() {
		assertThat(mEmptyHandler.getModelClass(), is(nullValue()));
	}

	@Test
	public void testGetModelClass() {
		assertThat(TestModel.class.equals(mHandler.getModelClass()), is(true));
	}

	@Test
	public void testHandleCreateModel() {
		assertThat(mHandler.handleCreateModel(), is(instanceOf(TestModel.class)));
	}

	@Test
	public void testHandleCreateModelForEntityWithoutAnnotation() {
		try {
			mEmptyHandler.handleCreateModel();
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISSING_CLASS_ANNOTATION));
			assertThat(
					e.getMessage(),
					is("@Model annotation for class(TestEntityWithoutAnnotation) is missing.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testHandleCreate() {
		// Tested via ModelEntityCreatorTest.
	}

	@Test
	public void testHandleUpgrade() {
		// Tested via ModelEntityUpgraderTest.
	}

	private static final class TestEntityWithoutAnnotation extends ModelEntity {
	}
}
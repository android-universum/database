/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.Cursor;
import android.database.MatrixCursor;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.entity.TestModelEntity;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelWithUnsupportedColumns;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
@SuppressWarnings("ResourceType")
public final class ModelCursorProcessorTest extends ModelProcessorBaseTest<ModelCursorProcessor> {

	@SuppressWarnings("unused")
	private static final String TAG = "ModelCursorProcessorTest";

	private static final String NOT_PROJECTED_COLUMN = "no_column";

	private Cursor mCursor;

	public ModelCursorProcessorTest() {
		super(new ModelCursorProcessor());
	}

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mCursor = TestModel.createCursorWithTestValues();
	}

	@Test
	public void testProcessModel() {
		// Note that when a cursor contains a NULL value for a specific column there is obtained
		// default value instead type of specific for that column (field) type.
		assertThat(mProcessor.processModel(mModel, mColumns, mCursor), is(true));
		assertThat(mModel.id, is(TestModel.TestValues.ID));
		assertThat(mModel.tag, is(TestModel.TestValues.TAG));
		assertThat(mModel.author, is(TestModel.TestValues.AUTHOR));
		assertThat(mModel.title, is(TestModel.TestValues.TITLE));
		assertThat(mModel.description, is(TestModel.TestValues.DESCRIPTION));
		assertThat(mModel.count, is(nullValue()));
		assertThat(mModel.latitude, is(TestModel.TestValues.LATITUDE));
		assertThat(mModel.longitude, is(TestModel.TestValues.LONGITUDE));
		assertThat(mModel.categoryId, is(nullValue()));
		assertThat(mModel.enabled, is(TestModel.TestValues.ENABLED));
		assertThat(mModel.published, is(TestModel.TestValues.PUBLISHED));
		assertThat(mModel.flags, is(TestModel.TestValues.FLAGS));
		assertThat(mModel.ratio, is(TestModel.TestValues.RATIO));
		assertThat(mModel.difficulty, is(TestModel.TestValues.DIFFICULTY));
		assertThat(mModel.orientation, is(TestModel.TestValues.ORIENTATION));
		assertThat(mModel.code, is(TestModel.TestValues.CODE));
	}

	@Test
	public void testProcessModelWithUnsupportedColumns() {
		final MatrixCursor testCursor = new MatrixCursor(new String[]{"date"}, 1);
		testCursor.addRow(new Object[]{0});
		testCursor.moveToFirst();
		assertThatProcessingThrowsExceptionWithMessage(
				new Runnable() {
					@Override
					public void run() {
						mProcessor.processModel(
								mModelWithUnsupportedColumns,
								mUnsupportedColumns,
								testCursor
						);
					}
				},
				"Failed to attach value from Cursor to column field. " +
						"Field(TestModelWithUnsupportedColumns.date) type of(Date) is not allowed to represent a column. " +
						"Only primitive type fields, Enums including, are allowed as columns."
		);
	}

	@Test
	public void testOnProcessColumnThatIsNotProjected() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("address"));
		final ModelCursorProcessor.CursorTask task = ModelCursorProcessor.CursorTask.obtainFor(mModel, mColumns, mCursor);
		assertThat(mProcessor.onProcessColumn(task, annotatedColumn), is(true));
		assertThat(mModel.address, is(nullValue()));
	}

	@Test
	public void testOnProcessColumnOfUnsupportedType() throws Exception {
		final MatrixCursor cursor = new MatrixCursor(new String[]{"date"}, 1);
		cursor.addRow(new Object[]{0});
		cursor.moveToFirst();
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getField("date"));
		final ModelCursorProcessor.CursorTask task = ModelCursorProcessor.CursorTask.obtainFor(mModelWithUnsupportedColumns, mUnsupportedColumns, cursor);
		try {
			mProcessor.onProcessColumn(task, annotatedColumn);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Failed to attach value from Cursor to column field. " +
							"Field(TestModelWithUnsupportedColumns.date) type of(Date) " +
							"is not allowed to represent a column. " +
							"Only primitive type fields, Enums including, are allowed as columns.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testAttachValueToColumnAsByte() throws Exception {
		this.testAttachValueToColumnInner("orientation", TestModelEntity.Columns.ORIENTATION, TestModel.TestValues.ORIENTATION);
	}

	@Test
	public void testAttachValueToColumnAsShort() throws Exception {
		this.testAttachValueToColumnInner("code", TestModelEntity.Columns.CODE, TestModel.TestValues.CODE);
	}

	@Test
	public void testAttachValueToColumnAsInteger() throws Exception {
		this.testAttachValueToColumnInner("flags", TestModelEntity.Columns.FLAGS, TestModel.TestValues.FLAGS);
	}

	@Test
	public void testAttachValueToColumnAsLong() throws Exception {
		this.testAttachValueToColumnInner("id", TestModelEntity.Columns.ID, TestModel.TestValues.ID);
	}

	@Test
	public void testAttachValueToColumnAsFloat() throws Exception {
		this.testAttachValueToColumnInner("ratio", TestModelEntity.Columns.RATIO, TestModel.TestValues.RATIO);
	}

	@Test
	public void testAttachValueToColumnAsDouble() throws Exception {
		this.testAttachValueToColumnInner("longitude", TestModelEntity.Columns.LONGITUDE, TestModel.TestValues.LONGITUDE);
	}

	@Test
	public void testAttachValueToColumnAsBoolean() throws Exception {
		this.testAttachValueToColumnInner("published", TestModelEntity.Columns.PUBLISHED, TestModel.TestValues.PUBLISHED);
	}

	@Test
	public void testAttachValueToColumnAsString() throws Exception {
		this.testAttachValueToColumnInner("author", TestModelEntity.Columns.AUTHOR, TestModel.TestValues.AUTHOR);
	}

	@Test
	public void testAttachValueToColumnAsEnum() throws Exception {
		this.testAttachValueToColumnInner("difficulty", TestModelEntity.Columns.DIFFICULTY, TestModel.TestValues.DIFFICULTY);
	}

	@Test
	public void testAttachValueToColumnAsEnumWithEmptyNameValue() throws Exception {
		final MatrixCursor cursor = new MatrixCursor(new String[]{TestModelEntity.Columns.DIFFICULTY}, 1);
		cursor.addRow(new Object[]{""});
		cursor.moveToFirst();
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("difficulty"));
		assertThat(mProcessor.attachValueToColumn(annotatedColumn, mModel, cursor, cursor.getColumnIndex(TestModelEntity.Columns.DIFFICULTY)), is(true));
		assertThat(mModel.difficulty, is(nullValue()));
	}

	@Test
	public void testAttachValueToColumnAsNull() throws Exception {
		this.testAttachValueToColumnInner("count", TestModelEntity.Columns.COUNT, null);
	}

	private void testAttachValueToColumnInner(String columnFieldName, String columnName, Object expected) throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField(columnFieldName));
		assertThat(mProcessor.attachValueToColumn(annotatedColumn, mModel, mCursor, mCursor.getColumnIndex(columnName)), is(true));
		assertThat(annotatedColumn.getField().get(mModel), is(expected));
	}

	@Test
	public void testAttachValueToColumnOfUnsupportedType() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getField("date"));
		assertThat(mProcessor.attachValueToColumn(annotatedColumn, mModelWithUnsupportedColumns, mCursor, 0), is(false));
		assertThat(mModelWithUnsupportedColumns.date, is(nullValue()));
	}

	@Test
	public void testAttachValueToColumnWithNotAccessibleField() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getDeclaredField("inaccessibleString"), false);
		try {
			mProcessor.attachValueToColumn(annotatedColumn, mModelWithUnsupportedColumns, mCursor, 0);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_DEFAULT));
			assertThat(e.getMessage(), is("Field(TestModelWithUnsupportedColumns.inaccessibleString) is not accessible."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testIndexOfColumn() {
		assertThat(ModelCursorProcessor.indexOfColumn(mCursor, TestModelEntity.Columns.CODE), is(2));
	}

	@Test
	public void testIndexOfColumnThatIsNotProjected() {
		assertThat(ModelCursorProcessor.indexOfColumn(mCursor, NOT_PROJECTED_COLUMN), is(-1));
	}

	@Test
	public void testValuesTaskObtain() {
		final ModelCursorProcessor.CursorTask task = ModelCursorProcessor.CursorTask.obtain();
		assertThat(task, is(not(nullValue())));
		assertThat(task.model, is(nullValue()));
		assertThat(task.columns, is(nullValue()));
		assertThat(task.cursor, is(nullValue()));
	}

	@Test
	public void testValuesTaskRecycle() {
		final int poolSize = ModelCursorProcessor.CursorTask.POOL_SIZE;
		final int testPoolRange = poolSize * 2;
		final ModelCursorProcessor.CursorTask[] tasks = new ModelCursorProcessor.CursorTask[testPoolRange];
		for (int i = 0; i < testPoolRange; i++) {
			tasks[i] = ModelCursorProcessor.CursorTask.obtain();
		}

		// Recycle all to populate the POOL.
		for (int i = 0; i < testPoolRange; i++) {
			tasks[i].recycle();
		}

		// When obtaining in size of the POOL all returned tasks are the same.
		for (int i = 0; i < poolSize; i++) {
			assertThat(ModelCursorProcessor.CursorTask.obtain(), is(tasks[i]));
		}

		for (int i = 0; i < testPoolRange; i++) {
			tasks[i].recycle();
		}
	}
}

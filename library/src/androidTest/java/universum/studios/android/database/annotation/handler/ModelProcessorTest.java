/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.os.Bundle;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelWithUnsupportedColumns;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class ModelProcessorTest {

	@SuppressWarnings("unused")
	private static final String TAG = "ModelProcessorTest";

	@Test
	@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
	public void testCreateFieldNotAllowedAsColumnException() throws Exception {
		final DatabaseException exception = ModelProcessor.createFieldNotAllowedAsColumnException(
				new TestModelWithUnsupportedColumns(),
				TestModelWithUnsupportedColumns.class.getField("date"),
				"Failed to process column field."
		);
		assertThat(exception.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
		assertThat(
				exception.getMessage(),
				is("Failed to process column field. " +
						"Field(TestModelWithUnsupportedColumns.date) type of(Date) " +
						"is not allowed to represent a column. " +
						"Only primitive type fields, Enums including, are allowed as columns.")
		);
	}

	@Test
	public void testProcessorTaskRecycle() {
		final TestTask task = new TestTask();
		task.model = new TestModel();
		task.bundle = new Bundle();
		task.recycle();
		assertThat(task.model, is(nullValue()));
		assertThat(task.bundle, is(nullValue()));
	}

	private static final class TestTask extends ModelProcessor.ProcessorTask {

		Bundle bundle;

		@Override
		void onRecycleInner() {
			this.bundle = null;
		}
	}
}

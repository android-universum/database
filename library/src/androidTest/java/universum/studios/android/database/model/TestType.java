/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.os.Parcel;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;

/**
 * @author Martin Albedinsky
 */
public final class TestType extends SimpleEntityModel<TestType> {

	public static final int TYPE_1 = 1;
	public static final int TYPE_2 = 2;
	public static final int TYPE_3 = 3;
	public static final int TYPE_4 = 4;
	public static final int TYPE_5 = 5;

	public static final Creator<TestType> CREATOR = new Creator<TestType>() {
		@Override
		public TestType createFromParcel(Parcel source) {
			return new TestType(source);
		}

		@Override
		public TestType[] newArray(int size) {
			return new TestType[size];
		}
	};

	@SuppressWarnings("unused")
	public static final Factory<TestType> FACTORY = new Factory<TestType>() {
		@NonNull
		@Override
		public TestType createModel() {
			return new TestType();
		}
	};

	@Column.Primary
	@Column(Column.Primary.COLUMN_NAME)
	public Long id;

	public TestType() {
		super();
	}

	protected TestType(@NonNull Parcel source) {
		super(source);
	}
}
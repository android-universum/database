/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.SparseArray;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Locale;
import java.util.concurrent.Executor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.annotation.DatabaseEntities;
import universum.studios.android.database.cursor.CursorUtils;
import universum.studios.android.database.cursor.EmptyCursor;
import universum.studios.android.database.entity.Entity;
import universum.studios.android.database.inner.ContextBaseTest;
import universum.studios.android.database.inner.DatabaseTests;
import universum.studios.android.database.loremipsum.LoremIpsumEntity;
import universum.studios.android.database.query.Query;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class DatabaseTest extends ContextBaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "DatabaseTest";

	private static final Executor CURRENT_THREAD_EXECUTOR = new Executor() {

		@Override
		public void execute(@NonNull Runnable runnable) {
			runnable.run();
		}
	};

	private final ProviderInfo PROVIDER_INFO = new ProviderInfo();

	{
		PROVIDER_INFO.exported = false;
	}

	private TestDatabase mDatabase;
	private TestSecondaryDatabase mSecondaryDatabase;
	private TestDatabase mEmptyDatabase;
	private BaseTestEntity mCustomersEntity;
	private BaseTestEntity mInvoicesEntity;
	private TestEntity mEntity;

	public static void dropTableIfExists(@NonNull String tableName) {
		TestPrimaryProvider.accessDatabase().asWritable().execSQL("DROP TABLE IF EXISTS " + tableName);
	}

	public static void assertTableExists(@NonNull String tableName) {
		final Cursor cursor = TestPrimaryProvider.accessDatabase().asReadable().rawQuery("SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name='" + tableName + "'", null);
		boolean exists = false;
		if (cursor != null) {
			exists = (cursor.getCount() > 0);
			cursor.close();
		}
		if (!exists) throw new AssertionError("Table with name(" + tableName + ") does not exist.");
	}

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mEmptyDatabase = new TestDatabase();
		this.ensureDatabaseCreated();
		this.ensureSecondaryDatabaseCreated();
		this.clearEntitiesState();
	}

	private void clearEntitiesState() {
		mCustomersEntity.clearState();
		mInvoicesEntity.clearState();
		mEntity.clearState();
	}

	private void ensureDatabaseCreated() {
		if (mDatabase == null) {
			final TestProvider provider = new TestProvider();
			provider.attachInfo(context, PROVIDER_INFO);
			this.mDatabase = (TestDatabase) provider.getDatabase();
			this.mCustomersEntity = mDatabase.findEntityByClass(TestCustomersEntity.class);
			this.mInvoicesEntity = mDatabase.findEntityByClass(TestInvoicesEntity.class);
		}
	}

	private void ensureSecondaryDatabaseCreated() {
		if (mSecondaryDatabase == null) {
			final TestSecondaryProvider provider = new TestSecondaryProvider();
			provider.attachInfo(context, PROVIDER_INFO);
			this.mSecondaryDatabase = (TestSecondaryDatabase) provider.getDatabase();
			this.mEntity = mSecondaryDatabase.findEntityByClass(TestEntity.class);
		}
	}

	@Test
	public void testInstantiation() {
		final Database database = new TestDatabase();
		assertThat(database.getVersion(), is(1));
		assertThat(database.getName(), is("test-database.db"));
		assertThat(database.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithDisabledAnnotationsProcessing() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		final Database database = new TestDatabase();
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		assertThat(database.getVersion(), is(1));
		assertThat(database.getName(), is("test-database.db"));
		assertThat(database.getAnnotationHandler(), is(nullValue()));
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mDatabase.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandlerWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {

					@Override
					public void run() {
						mDatabase.getAnnotationHandler();
					}
				},
				DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED
		);
	}

	@Test
	public void testCreateEntityUri() {
		assertThat(
				Database.createContentUri(TestProvider.AUTHORITY, "customers"),
				is(Uri.parse("content://" + TestProvider.AUTHORITY + "/customers"))
		);
	}

	@Test
	public void testCreateEntityMimeType() {
		assertThat(
				Database.createMimeType(ContentResolver.CURSOR_DIR_BASE_TYPE, TestProvider.AUTHORITY, "customers"),
				is(ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + TestProvider.AUTHORITY + ".customers")
		);
	}

	@Test
	public void testGetAuthority() {
		assertThat(mDatabase.getAuthority(), is(TestProvider.AUTHORITY));
	}

	@Test
	public void testGetAuthorityFromNotAttachedDatabase() {
		try {
			mEmptyDatabase.getAuthority();
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Database(TestDatabase) is not attached to its associated DatabaseProvider.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testGetVersion() {
		assertThat(mDatabase.getVersion(), is(TestDatabase.VERSION));
	}

	@Test
	public void testGetName() {
		assertThat(mDatabase.getName(), is(TestDatabase.NAME));
	}

	@Test
	public void testDefaultGetLocale() {
		assertThat(mDatabase.getLocale(), is(nullValue()));
	}

	@Test
	public void testSetLocale() {
		mDatabase.setLocale(Locale.FRENCH);
		assertThat(mDatabase.getLocale(), is(Locale.FRENCH));
	}

	@Test
	public void testDefaultCreateOnStartup() {
		assertThat(mDatabase.createsOnStartup(), is(true));
	}

	@Test
	public void testSetCreateOnStartup() {
		mDatabase.setCreateOnStartup(false);
		assertThat(mDatabase.createsOnStartup(), is(false));
	}

	@Test
	public void testCreateDB() {
		mSecondaryDatabase.createDB(mSecondaryDatabase.asWritable());
		assertThat(mEntity.onCreateInvoked, is(true));
		assertThat(mEntity.onCreateInitialContentInvoked, is(true));
	}

	@Test
	public void testUpgradeDB() {
		final SQLiteDatabase db = mSecondaryDatabase.asWritable();
		mSecondaryDatabase.upgradeDB(db, 1, 2);
		assertThat(mEntity.onUpgradeInvoked, is(true));
		assertThat(mEntity.invokedWithOldVersion, is(1));
		assertThat(mEntity.invokedWithNewVersion, is(2));
		assertThat(mEntity.onUpgradeContentInvoked, is(true));

		mEntity.clearState();
		mSecondaryDatabase.upgradeDB(db, 2, 3);
		assertThat(mEntity.onUpgradeInvoked, is(true));
		assertThat(mEntity.onUpgradeContentInvoked, is(false));
	}

	@Test
	public void testDowngradeDB() {
		mSecondaryDatabase.downgradeDB(mSecondaryDatabase.asWritable(), 2, 1);
		assertThat(mEntity.onDowngradeInvoked, is(true));
		assertThat(mEntity.invokedWithOldVersion, is(2));
		assertThat(mEntity.invokedWithNewVersion, is(1));
	}

	@Test
	public void testInsert() {
		assertThat(mDatabase.insert(TestCustomersEntity.URI, null), is(nullValue()));
		assertThat(mCustomersEntity.insertDispatched, is(true));
		assertThat(mCustomersEntity.dispatchedContentValues, is(nullValue()));

		final ContentValues values = new ContentValues();
		values.put("column", "value");
		assertThat(mDatabase.insert(TestInvoicesEntity.URI, values), is(nullValue()));
		assertThat(mInvoicesEntity.insertDispatched, is(true));
		assertThat(mInvoicesEntity.dispatchedContentValues, is(values));
	}

	@Test
	public void testInsertForNotAssignedEntity() {
		assertThat(mDatabase.insert(TestEntity.URI, null), is(nullValue()));
	}

	@Test
	public void testBulkInsert() {
		assertThat(mDatabase.bulkInsert(TestCustomersEntity.URI, new ContentValues[0]), is(0));
		assertThat(mCustomersEntity.bulkInsertDispatched, is(true));
		assertThat(mCustomersEntity.dispatchedContentValuesArray.length, is(0));
		final ContentValues values = new ContentValues();
		values.put("column", 15);
		assertThat(mDatabase.bulkInsert(TestInvoicesEntity.URI, new ContentValues[]{values}), is(0));
		assertThat(mInvoicesEntity.bulkInsertDispatched, is(true));
		assertThat(mInvoicesEntity.dispatchedContentValuesArray.length, is(1));
		assertThat(mInvoicesEntity.dispatchedContentValuesArray[0], is(values));
	}

	@Test
	public void testBulkInsertForNotAssignedEntity() {
		assertThat(mDatabase.bulkInsert(TestEntity.URI, new ContentValues[0]), is(-1));
	}

	@Test
	public void testQuery() {
		assertThat(mDatabase.query(TestCustomersEntity.URI, null, null, null, null), is(notNullValue()));
		assertThat(mCustomersEntity.queryDispatched, is(true));
		assertThat(mCustomersEntity.dispatchedProjection, is(nullValue()));
		assertThat(mCustomersEntity.dispatchedSelection, is(nullValue()));
		assertThat(mCustomersEntity.dispatchedSelectionArgs, is(nullValue()));
		assertThat(mCustomersEntity.dispatchedSortOrder, is(nullValue()));

		final String[] projection = new String[]{"column_1", "column_2"};
		final String selection = "column_1!=?";
		final String[] selectionArgs = new String[]{"0"};
		final String sortOrder = "column_2 ASC";
		assertThat(mDatabase.query(TestInvoicesEntity.URI, projection, selection, selectionArgs, sortOrder), is(notNullValue()));
		assertThat(mInvoicesEntity.queryDispatched, is(true));
		assertThat(mInvoicesEntity.dispatchedProjection, is(projection));
		assertThat(mInvoicesEntity.dispatchedSelection, is(selection));
		assertThat(mInvoicesEntity.dispatchedSelectionArgs, is(selectionArgs));
		assertThat(mInvoicesEntity.dispatchedSortOrder, is(sortOrder));
	}

	@Test
	public void testQueryForNotAssignedEntity() {
		assertThat(mDatabase.query(TestEntity.URI, null, null, null, null), is(nullValue()));
	}

	@Test
	public void testUpdate() {
		assertThat(mDatabase.update(TestCustomersEntity.URI, null, null, null), is(0));
		assertThat(mCustomersEntity.updateDispatched, is(true));
		assertThat(mCustomersEntity.dispatchedContentValues, is(nullValue()));
		assertThat(mCustomersEntity.dispatchedSelection, is(nullValue()));
		assertThat(mCustomersEntity.dispatchedSelectionArgs, is(nullValue()));
		final ContentValues values = new ContentValues();
		final String selection = "column_1=123";
		assertThat(mDatabase.update(TestInvoicesEntity.URI, values, selection, null), is(0));
		assertThat(mInvoicesEntity.updateDispatched, is(true));
		assertThat(mInvoicesEntity.dispatchedContentValues, is(values));
		assertThat(mInvoicesEntity.dispatchedSelection, is(selection));
		assertThat(mInvoicesEntity.dispatchedSelectionArgs, is(nullValue()));
	}

	@Test
	public void testUpdateForNotAssignedEntity() {
		assertThat(mDatabase.update(TestEntity.URI, null, null, null), is(-1));
	}

	@Test
	public void testDelete() {
		assertThat(mDatabase.delete(TestCustomersEntity.URI, null, null), is(0));
		assertThat(mCustomersEntity.deleteDispatched, is(true));
		assertThat(mCustomersEntity.dispatchedSelection, is(nullValue()));
		assertThat(mCustomersEntity.dispatchedSelectionArgs, is(nullValue()));
		final String selection = "column_2='?'";
		final String[] selectionArgs = new String[]{"Rome"};
		assertThat(mDatabase.delete(TestInvoicesEntity.URI, selection, selectionArgs), is(0));
		assertThat(mInvoicesEntity.deleteDispatched, is(true));
		assertThat(mInvoicesEntity.dispatchedSelection, is(selection));
		assertThat(mInvoicesEntity.dispatchedSelectionArgs, is(selectionArgs));
	}

	@Test
	public void testDeleteForNotAssignedEntity() {
		assertThat(mDatabase.delete(TestEntity.URI, null, null), is(-1));
	}

	@Test
	public void testGetType() {
		assertThat(mDatabase.getType(TestCustomersEntity.URI), is("vnd.android.cursor.dir/vnd." + TestProvider.AUTHORITY + ".customers"));
		assertThat(mDatabase.getType(TestInvoicesEntity.URI), is("vnd.android.cursor.dir/vnd." + TestProvider.AUTHORITY + ".invoices"));
	}

	@Test
	public void testGetTypeForNotAssignedEntity() {
		assertThat(mDatabase.getType(TestEntity.URI), is(nullValue()));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testExecuteQuery() {
		assertThat(mDatabase.executeQuery(new Query() {

			@Override
			public boolean isExecutable() {
				return true;
			}

			@NonNull
			@Override
			public String build() {
				return "INSERT INTO " + TestInvoicesEntity.NAME + " (" +
						TestInvoicesEntity.Columns.CUSTOMER_ID + ", " +
						TestInvoicesEntity.Columns.DATE + ", " +
						TestInvoicesEntity.Columns.SUM +
						") VALUES(" +
						Long.toString(1000) + ", " +
						"'2015-09-20 14:25'" + ", " +
						Float.toString(123.45f) +
						")";
			}
		}), is(true));
		final Cursor cursor = mDatabase.executeRawQuery(new Query() {
			@Override
			public boolean isExecutable() {
				return true;
			}

			@NonNull
			@Override
			public String build() {
				return "SELECT * FROM " + TestInvoicesEntity.NAME + " WHERE " + TestInvoicesEntity.Columns.CUSTOMER_ID + "=1000";
			}
		});
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.moveToFirst(), is(true));
		assertThat(cursor.getCount(), is(1));
		assertThat(CursorUtils.obtainLong(cursor, TestInvoicesEntity.Columns.CUSTOMER_ID), is(1000L));
		assertThat(CursorUtils.obtainString(cursor, TestInvoicesEntity.Columns.DATE), is("2015-09-20 14:25"));
		assertThat(CursorUtils.obtainFloat(cursor, TestInvoicesEntity.Columns.SUM), is(123.45f));
		// Remove inserted test row.
		mDatabase.executeQuery(new DeleteInvoiceQuery(TestInvoicesEntity.Columns.CUSTOMER_ID + "=1000"));
	}

	@Test
	public void testExecuteQueryThatIsNotExecutable() {
		assertThat(mDatabase.executeQuery(new Query() {

			@Override
			public boolean isExecutable() {
				return false;
			}

			@NonNull
			@Override
			public String build() {
				return "";
			}
		}), is(false));
	}

	@Test
	public void testExecuteQueryForNotCreatedDatabase() {
		assertActionThrowsIllegalStateException(new Runnable() {
			@Override
			public void run() {
				mEmptyDatabase.executeQuery(new Query() {
					@Override
					public boolean isExecutable() {
						return true;
					}

					@NonNull
					@Override
					public String build() {
						return "";
					}
				});
			}
		}, "Cannot perform database action(ACCESS WRITABLE DB). Database is not created yet.");
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testExecuteRawQuery() {
		final Cursor cursor = mDatabase.executeRawQuery(new Query() {

			@Override
			public boolean isExecutable() {
				return true;
			}

			@NonNull
			@Override
			public String build() {
				return "SELECT * FROM " + LoremIpsumEntity.NAME;
			}
		});
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.getColumnCount(), is(10));
	}

	@Test
	public void testExecuteRawQueryThatIsNotExecutable() {
		assertThat(mDatabase.executeRawQuery(new Query() {

			@Override
			public boolean isExecutable() {
				return false;
			}

			@NonNull
			@Override
			public String build() {
				return "";
			}
		}), is(nullValue()));
	}

	@Test
	public void testExecuteRawQueryForNotCreatedDatabase() {
		assertActionThrowsIllegalStateException(new Runnable() {

			@Override
			public void run() {
				mEmptyDatabase.executeRawQuery(new Query() {

					@Override
					public boolean isExecutable() {
						return true;
					}

					@NonNull
					@Override
					public String build() {
						return "";
					}
				});
			}
		}, "Cannot perform database action(ACCESS READABLE DB). Database is not created yet.");
	}

	@Test
	public void testAsReadable() {
		assertThat(mDatabase.asReadable(), is(not(nullValue())));
	}

	@Test
	public void testAsReadableForNotCreatedDatabase() {
		assertActionThrowsIllegalStateException(new Runnable() {

			@Override
			public void run() {
				mEmptyDatabase.asReadable();
			}
		}, "Cannot perform database action(ACCESS READABLE DB). Database is not created yet.");
	}

	@Test
	public void testAsWritable() {
		assertThat(mDatabase.asWritable(), is(not(nullValue())));
	}

	@Test
	public void testAsWritableForNotCreatedDatabase() {
		assertActionThrowsIllegalStateException(new Runnable() {

			@Override
			public void run() {
				mEmptyDatabase.asWritable();
			}
		}, "Cannot perform database action(ACCESS WRITABLE DB). Database is not created yet.");
	}

	@Test
	public void testAssignEntity() {
		mEmptyDatabase.assignEntity(TestCustomersEntity.class);
		mEmptyDatabase.assignEntity(TestInvoicesEntity.class);
		final SparseArray<Class<? extends DatabaseEntity>> entities = mEmptyDatabase.getAssignedEntities();
		assertThat(entities, is(not(nullValue())));
		assertThat(entities.size(), is(2));
		assertThat(entities.valueAt(0).equals(TestCustomersEntity.class), is(true));
		assertThat(entities.valueAt(1).equals(TestInvoicesEntity.class), is(true));
	}

	@Test
	public void testAssignAlreadyAssignedEntity() {
		mEmptyDatabase.assignEntity(TestCustomersEntity.class);
		mEmptyDatabase.assignEntity(TestCustomersEntity.class);
		final SparseArray<Class<? extends DatabaseEntity>> entities = mEmptyDatabase.getAssignedEntities();
		assertThat(entities, is(not(nullValue())));
		assertThat(entities.size(), is(1));
		assertThat(entities.valueAt(0).equals(TestCustomersEntity.class), is(true));
	}

	@Test
	public void testAssignEntityForAlreadyCreatedDatabase() {
		assertActionThrowsIllegalStateException(new Runnable() {
			@Override
			public void run() {
				mDatabase.assignEntity(TestCustomersEntity.class);
			}
		}, "Cannot assign new entity. Database has been already created.");
	}

	@Test
	public void testObtainEntityByUri() {
		assertThat(mDatabase.findEntityByUri(TestInvoicesEntity.URI), is(not(nullValue())));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtainEntityByUriForNotAssignedEntity() {
		mDatabase.findEntityByUri(TestEntity.URI);
	}

	@Test
	public void testObtainEntityByUriForNotCreatedDatabase() {
		try {
			mEmptyDatabase.findEntityByUri(TestEntity.URI);
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Cannot perform database action(FIND ENTITY BY URI). Database is not created yet.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testObtainEntityByClass() {
		assertThat(mDatabase.findEntityByClass(TestCustomersEntity.class), is(not(nullValue())));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testObtainEntityByClassForNotAssignedEntity() {
		mDatabase.findEntityByClass(TestEntity.class);
	}

	@Test
	public void testObtainEntityByClassForNotCreatedDatabase() {
		try {
			mEmptyDatabase.findEntityByClass(TestEntity.class);
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Cannot perform database action(FIND ENTITY BY CLASS). Database is not created yet.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	private static void assertActionThrowsIllegalStateException(Runnable action, String exceptionMessage) {
		try {
			action.run();
		} catch (IllegalStateException e) {
			assertThat(e.getMessage(), is(exceptionMessage));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	public static final class TestProvider extends DatabaseProvider {

		static final String AUTHORITY = "universum.studios.android.database.test.DatabaseTest.TestProvider";

		public TestProvider() {
			super(AUTHORITY);
		}

		@NonNull
		@Override
		protected Database onCreateDatabase(@NonNull Context context) {
			return new TestDatabase();
		}
	}

	public static final class TestSecondaryProvider extends DatabaseProvider {

		static final String AUTHORITY = "universum.studios.android.database.test.DatabaseTest.TestSecondaryProvider";

		public TestSecondaryProvider() {
			super(AUTHORITY);
		}

		@NonNull
		@Override
		protected Database onCreateDatabase(@NonNull Context context) {
			return new TestSecondaryDatabase();
		}
	}

	@DatabaseEntities({
			TestCustomersEntity.class,
			TestInvoicesEntity.class,
			LoremIpsumEntity.class
	})
	private static final class TestDatabase extends Database {

		static final int VERSION = 1;
		static final String NAME = "test-database.db";

		public TestDatabase() {
			super(VERSION, NAME);
			setExecutor(CURRENT_THREAD_EXECUTOR);
		}
	}

	@DatabaseEntities({TestEntity.class})
	private static final class TestSecondaryDatabase extends Database {

		static final int VERSION = 1;
		static final String NAME = "test-secondary-database.db";

		public TestSecondaryDatabase() {
			super(VERSION, NAME);
			setExecutor(CURRENT_THREAD_EXECUTOR);
		}
	}

	private static abstract class BaseTestEntity extends Entity {

		boolean insertDispatched, bulkInsertDispatched, queryDispatched, updateDispatched, deleteDispatched;
		ContentValues dispatchedContentValues;
		ContentValues[] dispatchedContentValuesArray;
		String[] dispatchedProjection;
		String dispatchedSelection;
		String[] dispatchedSelectionArgs;
		String dispatchedSortOrder;

		private BaseTestEntity(@NonNull String name) {
			super(name);
		}

		@Nullable
		@Override
		public Uri dispatchInsert(@NonNull SQLiteDatabase db, @Nullable ContentValues values) {
			this.insertDispatched = true;
			this.dispatchedContentValues = values;
			return null;
		}

		@Override
		public int dispatchBulkInsert(@NonNull SQLiteDatabase db, @NonNull ContentValues[] values) {
			this.bulkInsertDispatched = true;
			this.dispatchedContentValuesArray = values;
			return 0;
		}

		@NonNull
		@Override
		public Cursor dispatchQuery(@NonNull SQLiteDatabase db, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
			this.queryDispatched = true;
			this.dispatchedProjection = projection;
			this.dispatchedSelection = selection;
			this.dispatchedSelectionArgs = selectionArgs;
			this.dispatchedSortOrder = sortOrder;
			return EmptyCursor.create();
		}

		@Override
		public int dispatchUpdate(@NonNull SQLiteDatabase db, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
			this.updateDispatched = true;
			this.dispatchedContentValues = values;
			this.dispatchedSelection = selection;
			this.dispatchedSelectionArgs = selectionArgs;
			return 0;
		}

		@Override
		public int dispatchDelete(@NonNull SQLiteDatabase db, @Nullable String selection, @Nullable String[] selectionArgs) {
			this.deleteDispatched = true;
			this.dispatchedSelection = selection;
			this.dispatchedSelectionArgs = selectionArgs;
			return 0;
		}

		void clearState() {
			insertDispatched = bulkInsertDispatched = queryDispatched = updateDispatched = deleteDispatched = false;
			this.dispatchedContentValues = null;
			this.dispatchedContentValuesArray = null;
			this.dispatchedProjection = null;
			this.dispatchedSelection = null;
			this.dispatchedSelectionArgs = null;
			this.dispatchedSortOrder = null;
		}
	}

	static final class TestCustomersEntity extends BaseTestEntity {

		static final String NAME = "customers";
		static final Uri URI = Database.createContentUri(TestProvider.AUTHORITY, NAME);

		interface Columns {
			String ID = "_id";
			String FIRST_NAME = "first_name";
			String LAST_NAME = "last_name";
			String EMAIL_ADDRESS = "email_address";
		}

		public TestCustomersEntity() {
			super(NAME);
		}

		@Override
		protected void onCreate(@NonNull SQLiteDatabase db) {
			db.execSQL("CREATE TABLE IF NOT EXISTS " + NAME + " ("
					+ Columns.ID + " INTEGER PRIMARY KEY, "
					+ Columns.FIRST_NAME + " TEXT NOT NULL, "
					+ Columns.LAST_NAME + " TEXT NOT NULL, "
					+ Columns.EMAIL_ADDRESS + " TEXT NOT NULL"
					+ ")"
			);
		}
	}

	static final class TestInvoicesEntity extends BaseTestEntity {

		static final String NAME = "invoices";
		static final Uri URI = Database.createContentUri(TestProvider.AUTHORITY, NAME);

		interface Columns {
			String ID = "_id";
			String CUSTOMER_ID = "customer_id";
			String DATE = "date";
			String SUM = "sum";
		}

		public TestInvoicesEntity() {
			super(NAME);
		}

		@Override
		protected void onCreate(@NonNull SQLiteDatabase db) {
			db.execSQL("CREATE TABLE IF NOT EXISTS " + NAME + " ("
					+ Columns.ID + " INTEGER PRIMARY KEY, "
					+ Columns.CUSTOMER_ID + " INTEGER, "
					+ Columns.DATE + " TEXT DEFAULT '000-00-00 00:00', "
					+ Columns.SUM + " REAL DEFAULT 0"
					+ ")"
			);
		}
	}

	static final class TestEntity extends Entity {

		static final String NAME = "items";
		static final Uri URI = Database.createContentUri(TestProvider.AUTHORITY, "items");

		interface Columns {
			String ID = "_id";
		}

		private boolean onCreateInvoked, onCreateInitialContentInvoked, onUpgradeInvoked, onUpgradeContentInvoked, onDowngradeInvoked;
		private int invokedWithOldVersion, invokedWithNewVersion;

		public TestEntity() {
			super(NAME);
		}

		@Override
		protected void onCreate(@NonNull SQLiteDatabase db) {
			db.execSQL("CREATE TABLE IF NOT EXISTS " + NAME + " ("
					+ Columns.ID + " INTEGER PRIMARY KEY"
					+ ")"
			);
			this.onCreateInvoked = true;
		}

		@Override
		public boolean createsInitialContent() {
			return true;
		}

		@Override
		protected void onCreateInitialContent(@NonNull SQLiteDatabase db) {
			this.onCreateInitialContentInvoked = true;
		}

		@Override
		protected void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
			this.onUpgradeInvoked = true;
			this.invokedWithOldVersion = oldVersion;
			this.invokedWithNewVersion = newVersion;
		}

		@Override
		public boolean upgradesContent(int oldVersion, int newVersion) {
			return oldVersion == 1 && newVersion == 2;
		}

		@Override
		protected void onUpgradeContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
			this.onUpgradeContentInvoked = true;
			this.invokedWithOldVersion = oldVersion;
			this.invokedWithNewVersion = newVersion;
		}

		@Override
		protected void onDowngrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
			this.onDowngradeInvoked = true;
			this.invokedWithOldVersion = oldVersion;
			this.invokedWithNewVersion = newVersion;
		}

		void clearState() {
			onCreateInvoked = onCreateInitialContentInvoked = onUpgradeInvoked = onUpgradeContentInvoked = onDowngradeInvoked = false;
			invokedWithOldVersion = -1;
			invokedWithNewVersion = -1;
		}
	}

	private static final class DeleteInvoiceQuery implements Query {

		private final String where;

		DeleteInvoiceQuery(String where) {
			this.where = where;
		}

		@Override
		public boolean isExecutable() {
			return true;
		}

		@NonNull
		@Override
		public String build() {
			return "DELETE FROM " + TestInvoicesEntity.NAME + " WHERE " + where;
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.inner.BaseTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class LoaderArgumentBuilderTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "LoaderStatementBuilderTest";

	private TestBuilder mBuilder;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mBuilder = new TestBuilder();
	}

	@Test
	public void testDefaultColumns() {
		final List<String> columns = mBuilder.columns();
		assertThat(columns, is(not(nullValue())));
		assertThat(columns.isEmpty(), is(true));
	}

	@Test
	public void testColumn() {
		mBuilder.column("author");
		mBuilder.column("content");
		final List<String> columns = mBuilder.columns();
		assertThat(columns.size(), is(2));
		assertThat(columns.get(0), is("author"));
		assertThat(columns.get(1), is("content"));
	}

	@Test
	public void testColumns() {
		mBuilder.columns("title", "date_created", "published");
		final List<String> columns = mBuilder.columns();
		assertThat(columns.size(), is(3));
		assertThat(columns.get(0), is("title"));
		assertThat(columns.get(1), is("date_created"));
		assertThat(columns.get(2), is("published"));
	}

	@Test
	public void testClear() {
		mBuilder.columns("column_1", "column_2");
		mBuilder.clear();
		assertThat(mBuilder.columns().isEmpty(), is(true));
	}

	private static final class TestBuilder extends LoaderArgumentBuilder<String> {
	}
}
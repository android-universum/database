/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.os.Parcel;

import java.util.Date;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public class TestModelWithUnsupportedColumns extends SimpleEntityModel<TestModelWithUnsupportedColumns> {

	public interface TestValues {
		Date DATE = new Date(0);
	}

	@SuppressWarnings("unused")
	private static final String TAG = "TestModelWithUnsupportedColumns";

	public static final Creator<TestModelWithUnsupportedColumns> CREATOR = new Creator<TestModelWithUnsupportedColumns>() {

		@Override
		public TestModelWithUnsupportedColumns createFromParcel(Parcel source) {
			return new TestModelWithUnsupportedColumns(source);
		}

		@Override
		public TestModelWithUnsupportedColumns[] newArray(int size) {
			return new TestModelWithUnsupportedColumns[size];
		}
	};

	public static final Factory<TestModelWithUnsupportedColumns> FACTORY = new Factory<TestModelWithUnsupportedColumns>() {

		@NonNull
		@Override
		public TestModelWithUnsupportedColumns createModel() {
			return new TestModelWithUnsupportedColumns();
		}
	};

	@Column
	public Date date;

	Date inaccessibleDate;

	String inaccessibleString;

	public TestModelWithUnsupportedColumns() {
		super();
	}

	protected TestModelWithUnsupportedColumns(@NonNull Parcel source) {
		super(source);
	}

	public static TestModelWithUnsupportedColumns witTestValues() {
		final TestModelWithUnsupportedColumns model = new TestModelWithUnsupportedColumns();
		model.date = TestValues.DATE;
		return model;
	}
}

/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public abstract class AnnotationHandlerBaseTest<H> {

	@SuppressWarnings("unused")
	private static final String TAG = "AnnotationHandlerBaseTest";

	final Class<? extends H> mHandlerClass;
	final H mHandler;
	final H mEmptyHandler;

	AnnotationHandlerBaseTest(Class<? extends H> classOfHandler) {
		this.mHandlerClass = classOfHandler;
		this.mHandler = onObtainHandler();
		this.mEmptyHandler = onObtainEmptyHandler();
	}

	abstract H onObtainHandler();

	H onObtainEmptyHandler() {
		return null;
	}

	@Test
	public void testHandlerAccessibility() throws Exception {
		assertThat(mHandlerClass.getConstructor(Class.class), is(not(nullValue())));
	}

	@Test
	public void testHandlerObtained() {
		assertThat(mHandler, is(not(nullValue())));
	}

	@Test
	public void testHandlerCached() {
		assertThat(mHandler, is(onObtainHandler()));
	}
}
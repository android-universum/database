/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.net.Uri;

import androidx.annotation.NonNull;
import universum.studios.android.database.Database;
import universum.studios.android.database.TestPrimaryProvider;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.EntityName;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.model.TestModel;

/**
 * @author Martin Albedinsky
 */
@Model(TestModel.class)
@EntityName(TestModelEntity.NAME)
public class TestModelEntity extends ModelEntity<TestModel> {

	@SuppressWarnings("unused")
	private static final String TAG = "TestModelEntity";

	static final String NAME = "test_models";
	static final Uri URI = Database.createContentUri(TestPrimaryProvider.AUTHORITY, NAME);

	public interface Columns {
		String ID = Column.Primary.COLUMN_NAME;
		String TAG = "tag";
		String AUTHOR = "author";
		String TITLE = "title";
		String DESCRIPTION = "description";
		String COUNT = "count";
		String LATITUDE = "latitude";
		String LONGITUDE = "longitude";
		String CATEGORY_ID = "category_id";
		String ENABLED = "enabled";
		String PUBLISHED = "published";
		String FLAGS = "flags";
		String RATIO = "ratio";
		String DIFFICULTY = "difficulty";
		String CODE = "code";
		String ORIENTATION = "orientation";
	}

	public interface JoinedColumns {
		String CATEGORY_NAME = "category_name";
	}

	public interface References {
		String CATEGORY_ID = "categories(_id)";
	}

	public static final String[] ALL_ENTITY_COLUMNS = {
			Columns.AUTHOR,
			Columns.CATEGORY_ID,
			Columns.CODE,
			Columns.COUNT,
			Columns.DESCRIPTION,
			Columns.DIFFICULTY,
			Columns.ENABLED,
			Columns.FLAGS,
			Columns.ID,
			Columns.LATITUDE,
			Columns.LONGITUDE,
			Columns.ORIENTATION,
			Columns.PUBLISHED,
			Columns.RATIO,
			Columns.TAG,
			Columns.TITLE
	};

	public static final String[] ALL_COLUMNS = {
			Columns.AUTHOR,
			Columns.CATEGORY_ID,
			JoinedColumns.CATEGORY_NAME,
			Columns.CODE,
			Columns.COUNT,
			Columns.DESCRIPTION,
			Columns.DIFFICULTY,
			Columns.ENABLED,
			Columns.FLAGS,
			Columns.ID,
			Columns.LATITUDE,
			Columns.LONGITUDE,
			Columns.ORIENTATION,
			Columns.PUBLISHED,
			Columns.RATIO,
			Columns.TAG,
			Columns.TITLE
	};

	public TestModelEntity() {
		super();
	}

	TestModelEntity(@NonNull String name, @NonNull Class<TestModel> classOfModel) {
		super(name, classOfModel);
	}
}

/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.Database;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.TestPrimaryProvider;
import universum.studios.android.database.inner.BaseTest;
import universum.studios.android.database.inner.DatabaseTests;
import universum.studios.android.database.model.TestType;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class EntityTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "EntityTest";
	private static final boolean MARSHMALLOW = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;

	private TestEntity mEntity;
	private TestEntity2 mEntity2;
	private TestEntity mNotAttachedEntity;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		if (mEntity == null) this.mEntity = TestPrimaryProvider.accessDatabase().findEntityByClass(TestEntity.class);
		this.mEntity2 = new TestEntity2();
		if (mNotAttachedEntity == null) this.mNotAttachedEntity = new TestEntity();
		// Always have a clear content before each test.
		this.deleteEntityContent();
	}

	private void deleteEntityContent() {
		mEntity.dropContent();
	}

	@Test
	public void testInstantiation() {
		final TestEntity entity = new TestEntity();
		assertThat(entity.getName(), is(TestEntity.NAME));
	}

	@Test
	public void testInstantiationWithDisabledAnnotationsProcessing() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		final TestEntity entity = new TestEntity();
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		assertThat(entity.getName(), is("test"));
		assertThat(entity.mAnnotationHandler, is(nullValue()));
	}

	@Test
	public void testInstantiationWithName() {
		final TestEntity entity = new TestEntity("test_entity");
		assertThat(entity.getName(), is("test_entity"));
	}

	@Test
	public void testInstantiationWithNameAndPrimaryColumnName() {
		final TestEntity entity = new TestEntity("test_entity", "_id");
		assertThat(entity.getName(), is("test_entity"));
		assertThat(entity.getPrimaryColumnName(), is("_id"));
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mEntity.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandlerWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {
					@Override
					public void run() {
						mEntity.getAnnotationHandler();
					}
				},
				DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED
		);
	}

	@Test
	public void testGetContentUri() {
		assertThat(
				mEntity.getContentUri(),
				is(Database.createContentUri(TestPrimaryProvider.AUTHORITY, TestEntity.NAME))
		);
	}

	@Test
	public void testGetContentUriForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.getContentUri();
					}
				},
				"ACCESS CONTENT URI"
		);
	}

	@Test
	public void testGetNotificationUri() {
		assertThat(mEntity.getNotificationUri(), is(mEntity.getContentUri())); // Default.
		mEntity.setNotificationUri(TestModelEntity.URI);
		assertThat(mEntity.getNotificationUri(), is(TestModelEntity.URI));
		mEntity.setNotificationUri(null); // Resets to default one.
		assertThat(mEntity.getNotificationUri(), is(mEntity.getContentUri()));
	}

	@Test
	public void testGetNotificationUriForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.getNotificationUri();
					}
				},
				"ACCESS NOTIFICATION URI"
		);
	}

	@Test
	public void testGetMimeType() {
		final String defaultMimeType = Database.createMimeType(
				ContentResolver.CURSOR_DIR_BASE_TYPE,
				TestPrimaryProvider.AUTHORITY,
				TestEntity.NAME
		);
		assertThat(mEntity.getMimeType(), is(defaultMimeType));
		mEntity.setMimeType("vnd.android.cursor.dir/vnd.TestProvider.customers");
		assertThat(mEntity.getMimeType(), is("vnd.android.cursor.dir/vnd.TestProvider.customers"));
	}

	@Test
	public void testGetMimeTypeForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.getMimeType();
					}
				},
				"ACCESS MIME TYPE"
		);
	}

	@Test
	public void testIsContentChangeNotificationEnabled() {
		assertThat(mEntity.isContentChangeNotificationEnabled(), is(true)); // Default.
		mEntity.setContentChangeNotificationEnabled(false);
		assertThat(mEntity.isContentChangeNotificationEnabled(), is(false));
		mEntity.setContentChangeNotificationEnabled(true);
	}

	@Test
	public void testIsAutoInsertOnUpdateEnabled() {
		assertThat(mEntity.isAutoInsertOnUpdateEnabled(), is(false)); // Default.
		mEntity.setAutoInsertOnUpdateEnabled(true);
		assertThat(mEntity.isAutoInsertOnUpdateEnabled(), is(true));
		mEntity.setAutoInsertOnUpdateEnabled(false);
	}

	@Test
	public void testAttachToDatabase() {
		assertThat(mNotAttachedEntity.isAttachedToDatabase(), is(false));
		mNotAttachedEntity.attachToDatabase(TestPrimaryProvider.accessDatabase());
		assertThat(mNotAttachedEntity.isAttachedToDatabase(), is(true));
		assertThat(mNotAttachedEntity.getDatabase(), is(not(nullValue())));
	}

	@Test(expected = IllegalStateException.class)
	public void testAttachToDatabaseAlreadyAttachedEntity() {
		assertThat(mEntity.isAttachedToDatabase(), is(true));
		mEntity.attachToDatabase(TestPrimaryProvider.accessDatabase());
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testDispatchCreate() {
		mEntity2.dispatchCreate(null);
		assertThat(mEntity2.onCreateDispatched, is(true));
	}

	@Test
	public void testDefaultCreatesInitialContent() {
		assertThat(mEntity.createsInitialContent(), is(false));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testDispatchCreateInitialContent() {
		mEntity2.dispatchCreateInitialContent(null);
		assertThat(mEntity2.onCreateInitialContentDispatched, is(true));
	}

	@Test
	public void testInsertInitialContent() {
		final ContentValues[] valuesArray = initializeContentValuesArray(3);
		for (int i = 0; i < valuesArray.length; i++) {
			valuesArray[i] = createContentValues("Item#" + Integer.toString(i + 1), TestType.TYPE_1);
		}
		final Database database = TestPrimaryProvider.accessDatabase();
		mEntity.insertInitialContent(database.asWritable(), valuesArray);
		this.assertContentOfEntityInSize(3);
		this.deleteEntityContent();
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testDispatchUpgrade() {
		mEntity2.dispatchUpgrade(null, 1, 2);
		assertThat(mEntity2.onUpgradeDispatched, is(true));
	}

	@Test
	public void testDefaultUpgradesContent() {
		assertThat(mEntity.upgradesContent(1, 2), is(false));
		assertThat(mEntity.upgradesContent(2, 3), is(false));
		// ...
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testDispatchUpgradeContent() {
		mEntity2.dispatchUpgradeContent(null, 1, 2);
		assertThat(mEntity2.onUpgradeContentDispatched, is(true));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testDispatchDowngrade() {
		mEntity2.dispatchDowngrade(null, 2, 1);
		assertThat(mEntity2.onDowngradeDispatched, is(true));
	}

	@Test
	public void testCount() {
		final ContentValues[] valuesArray = initializeContentValuesArray(10);
		for (int i = 0; i < valuesArray.length; i++) {
			valuesArray[i] = createContentValues("Item#" + Integer.toString(i + 1), i + 1);
		}
		mEntity.bulkInsert(valuesArray);
		assertThat(mEntity.count(), is(10));
	}

	@Test
	public void testCountForEmptyContent() {
		assertThat(mEntity.count(), is(0));
	}

	@Test
	public void testIds() {
		final ContentValues[] valuesArray = initializeContentValuesArray(3);
		for (int i = 0; i < valuesArray.length; i++) {
			valuesArray[i] = createContentValues("Item#" + Integer.toString(i + 1), i + 1);
		}
		mEntity.bulkInsert(valuesArray);
		final long[] ids = mEntity.ids();
		assertThat(ids, is(not(nullValue())));
		assertThat(ids.length, is(3));
	}

	@Test
	public void testIdsForEmptyContent() {
		final long[] ids = mEntity.ids();
		assertThat(ids, is(not(nullValue())));
		assertThat(ids.length, is(0));
	}

	@SuppressLint("NewApi")
	@Test
	public void testCreateInsertOperations() {
		final ContentValues[] valuesArray = initializeContentValuesArray(2);
		final List<ContentProviderOperation> operations = mEntity.createInsertOperations(valuesArray);
		assertThat(operations.size(), is(valuesArray.length));
		assertContentProviderOperationIsValid(operations.get(0));
		assertContentProviderOperationIsValid(operations.get(1));
		assertThat(!MARSHMALLOW || operations.get(0).isInsert(), is(true));
		assertThat(!MARSHMALLOW || operations.get(1).isInsert(), is(true));
	}

	@SuppressLint("NewApi")
	@Test
	public void testCreateInsertOperation() {
		final ContentProviderOperation operation = mEntity.createInsertOperation(createNotEmptyContentValues());
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isInsert(), is(true));
	}

	@Test
	public void testPrepareInsertOperation() {
		final ContentProviderOperation.Builder operationBuilder = mEntity.prepareInsertOperation();
		assertThat(operationBuilder, is(not(nullValue())));
		operationBuilder.withValues(createNotEmptyContentValues());
		assertContentProviderOperationIsValid(operationBuilder.build());
	}

	@Test
	public void testPrepareInsertOperationForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.prepareInsertOperation();
					}
				},
				"PREPARE INSERT OPERATION"
		);
	}

	@SuppressLint("NewApi")
	@Test
	public void testCreateUpdateOperation() {
		final ContentProviderOperation operation = mEntity.createUpdateOperation(createNotEmptyContentValues(), null, null);
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isUpdate(), is(true));
	}

	@Test
	public void testPrepareUpdateOperation() {
		final ContentProviderOperation.Builder operationBuilder = mEntity.prepareUpdateOperation();
		assertThat(operationBuilder, is(not(nullValue())));
		operationBuilder.withValues(createNotEmptyContentValues());
		assertContentProviderOperationIsValid(operationBuilder.build());
	}

	@Test
	public void testPrepareUpdateOperationForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.prepareUpdateOperation();
					}
				},
				"PREPARE UPDATE OPERATION"
		);
	}

	@SuppressWarnings("NewApi")
	@Test
	public void testCreateDropContentOperation() {
		final ContentProviderOperation operation = mEntity.createDropContentOperation();
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isDelete(), is(true));
	}

	@SuppressWarnings("NewApi")
	@Test
	public void testCreateDeleteOperationForSingle() {
		final ContentProviderOperation operation = mEntity.createDeleteOperation(12345L);
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isDelete(), is(true));
	}

	@SuppressWarnings("NewApi")
	@Test
	public void testCreateDeleteOperationForMultipleIds() {
		final ContentProviderOperation operation = mEntity.createDeleteOperation(new long[]{12345L, 123455L, 1234567L});
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isDelete(), is(true));
	}

	@SuppressLint("NewApi")
	@Test
	public void testCreateDeleteOperation() {
		final ContentProviderOperation operation = mEntity.createDeleteOperation(null, null);
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isDelete(), is(true));
	}

	@Test
	public void testPrepareDeleteOperation() {
		final ContentProviderOperation.Builder operationBuilder = mEntity.prepareDeleteOperation();
		assertThat(operationBuilder, is(not(nullValue())));
		assertContentProviderOperationIsValid(operationBuilder.build());
	}

	@Test
	public void testPrepareDeleteOperationForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.prepareDeleteOperation();
					}
				},
				"PREPARE DELETE OPERATION"
		);
	}

	@SuppressLint("NewApi")
	@Test
	public void testCreateAssertOperation() {
		final ContentProviderOperation operation = mEntity.createAssertOperation(createNotEmptyContentValues(), null, null);
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isAssertQuery(), is(true));
	}

	@Test
	public void testPrepareAssertOperation() {
		final ContentProviderOperation.Builder operationBuilder = mEntity.prepareAssertOperation();
		assertThat(operationBuilder, is(not(nullValue())));
		operationBuilder.withValues(createNotEmptyContentValues());
		assertContentProviderOperationIsValid(operationBuilder.build());
	}

	@Test
	public void testPrepareAssertOperationForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.prepareAssertOperation();
					}
				},
				"PREPARE ASSERT OPERATION"
		);
	}

	private void assertContentProviderOperationIsValid(ContentProviderOperation operation) {
		assertThat(operation, is(not(nullValue())));
		assertThat(operation.getUri(), is(mEntity.getContentUri()));
	}

	@Test
	public void testApplyBatchOperations() {
		final ArrayList<ContentProviderOperation> operations = new ArrayList<>(1);
		operations.add(mEntity.createInsertOperation(createContentValues("Item#1", TestType.TYPE_1)));
		mEntity.applyBatchOperations(operations);
		assertContentOfEntityInSize(1);

		operations.clear();
		operations.add(mEntity.createDropContentOperation());
		mEntity.applyBatchOperations(operations);
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testApplyEmptyBatchOperations() {
		final ContentProviderResult[] results = mEntity.applyBatchOperations(new ArrayList<ContentProviderOperation>(0));
		assertThat(results, is(not(nullValue())));
		assertThat(results.length, is(0));
	}

	@Test
	public void testApplyBatchOperationsForNotAttachedEntity() {
		final ArrayList<ContentProviderOperation> operations = new ArrayList<>(1);
		operations.add(mEntity.createDropContentOperation());
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.applyBatchOperations(operations);
					}
				},
				"APPLY BATCH OPERATIONS"
		);
	}

	@Test
	public void testInsert() {
		final Uri rowUri = mEntity.insert(createContentValues("Item", TestType.TYPE_2));
		assertThat(rowUri, is(not(nullValue())));
		assertContentOfEntityInSize(1);
	}

	@Test
	public void testInsertForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.insert(createNotEmptyContentValues());
					}
				},
				"INSERT"
		);
	}

	@Test
	public void testBulkInsert() {
		final ContentValues[] valuesArray = initializeContentValuesArray(3);
		for (int i = 0; i < valuesArray.length; i++) {
			valuesArray[i] = createContentValues("Item#" + Integer.toString(i + 1), i + 1);
		}
		assertThat(mEntity.bulkInsert(valuesArray), is(3));
		assertContentOfEntityInSize(valuesArray.length);
		assertContentOfEntityInSize(1, TestEntity.Columns.TYPE_ID + "=?", TestType.TYPE_1);
		assertContentOfEntityInSize(1, TestEntity.Columns.TYPE_ID + "=?", TestType.TYPE_2);
		assertContentOfEntityInSize(1, TestEntity.Columns.TYPE_ID + "=?", TestType.TYPE_3);
	}

	@Test
	public void testBulkInsertForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.bulkInsert(initializeContentValuesArray(2));
					}
				},
				"BULK INSERT"
		);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void testQuery() {
		final ContentValues[] valuesArray = new ContentValues[5];
		valuesArray[0] = createContentValues("Item#1", TestType.TYPE_2);
		valuesArray[1] = createContentValues("Item#2", TestType.TYPE_2);
		valuesArray[2] = createContentValues("Item#3", TestType.TYPE_1);
		valuesArray[3] = createContentValues("Item#4", TestType.TYPE_5);
		valuesArray[4] = createContentValues("Item#5", TestType.TYPE_4);
		mEntity.bulkInsert(valuesArray);
		Cursor cursor;

		// 1)
		cursor = mEntity.query(null, null, null, null);
		assertCursorWithRowsInCount(cursor, 5);
		assertThat(cursor.getColumnCount(), is(3));

		// 2)
		cursor = mEntity.query(new String[]{TestEntity.Columns.ID, TestEntity.Columns.TITLE}, null, null, null);
		assertCursorWithRowsInCount(cursor, 5);
		assertThat(cursor.getColumnCount(), is(2));

		// 3)
		cursor = mEntity.query(null, TestEntity.Columns.TYPE_ID + "=?", new String[]{Integer.toString(TestType.TYPE_2)}, null);
		assertCursorWithRowsInCount(cursor, 2);

		// 4)
		cursor = mEntity.query(null, null, null, TestEntity.Columns.TYPE_ID + " DESC");
		assertCursorWithRowsInCount(cursor, 5);
		assertThat(cursor.moveToFirst(), is(true));
		assertThat(cursor.getString(cursor.getColumnIndex(TestEntity.Columns.TITLE)), is("Item#4"));
		assertThat(cursor.moveToLast(), is(true));
		assertThat(cursor.getString(cursor.getColumnIndex(TestEntity.Columns.TITLE)), is("Item#3"));
	}

	@Test
	public void testQueryAll() {
		final ContentValues[] valuesArray = initializeContentValuesArray(4);
		for (int i = 0; i < valuesArray.length; i++) {
			valuesArray[i] = createContentValues("Item#" + Integer.toString(i + 1), i + 1);
		}
		mEntity.bulkInsert(valuesArray);
		assertCursorWithRowsInCount(mEntity.queryAll(), 4);
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testQueryForEmptyContent() {
		final Cursor cursor = mEntity.query(null, null, null, null);
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.getCount(), is(0));
	}

	private void assertCursorWithRowsInCount(Cursor cursor, int count) {
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.getCount(), is(count));
	}

	@Test
	public void testQueryForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.query(null, null, null, null);
					}
				},
				"QUERY"
		);
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testUpdate() {
		assertThat(mEntity.insert(createContentValues("Item#1", TestType.TYPE_1)), is(not(nullValue())));
		assertThat(
				mEntity.update(
				createContentValues("Item#1:Update", TestType.TYPE_1),
				TestEntity.Columns.TYPE_ID + "=?",
				new String[]{Integer.toString(TestType.TYPE_1)}),
				is(1)
		);
		assertContentOfEntityInSize(1);
		final Cursor cursor = mEntity.query(
				null,
				TestEntity.Columns.TYPE_ID + "=?",
				new String[]{Integer.toString(TestType.TYPE_1)},
				null
		);
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.moveToFirst(), is(true));
		assertThat(cursor.getString(cursor.getColumnIndex(TestEntity.Columns.TITLE)), is("Item#1:Update"));
	}

	@Test
	public void testUpdateRowNotPresentedWithinDb() {
		assertThat(
				mEntity.update(
				createContentValues("Item", TestType.TYPE_1),
				TestEntity.Columns.TYPE_ID + "=?",
				new String[]{Integer.toString(TestType.TYPE_4)}),
				is(0)
		);
	}

	@Test
	public void testUpdateForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.update(createNotEmptyContentValues(), null, null);
					}
				},
				"UPDATE"
		);
	}

	@Test
	public void testDropContent() {
		mEntity.insert(createContentValues("Item#1", TestType.TYPE_1));
		assertContentOfEntityInSize(1);
		mEntity.dropContent();
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testDelete() {
		mEntity.insert(createContentValues("Item#1", TestType.TYPE_1));
		mEntity.insert(createContentValues("Item#2", TestType.TYPE_2));
		assertContentOfEntityInSize(2);
		assertThat(mEntity.delete(TestEntity.Columns.TYPE_ID + "=?", new String[]{Integer.toString(TestType.TYPE_1)}), is(1));
		assertContentOfEntityInSize(1);
		assertThat(mEntity.delete(TestEntity.Columns.TYPE_ID + "=?", new String[]{Integer.toString(TestType.TYPE_2)}), is(1));
	}

	@Test
	public void testDeleteById() {
		mEntity.insert(createContentValues(100, "Item#1", TestType.TYPE_2));
		mEntity.insert(createContentValues(105, "Item#2", TestType.TYPE_3));
		assertThat(mEntity.delete(105), is(1));
		assertThat(mEntity.count(), is(1));
	}

	@Test
	public void testDeleteByIds() {
		mEntity.insert(createContentValues(100, "Item#1", TestType.TYPE_2));
		mEntity.insert(createContentValues(105, "Item#2", TestType.TYPE_3));
		assertThat(mEntity.delete(new long[]{100L, 105L}), is(2));
		assertThat(mEntity.count(), is(0));
	}

	@Test
	public void testDeleteRowNotPresentedWithinDb() {
		assertThat(mEntity.delete(TestEntity.Columns.TYPE_ID + "=?", new String[]{Integer.toString(TestType.TYPE_2)}), is(0));
	}

	@Test
	public void testDeleteForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.delete(null, null);
					}
				},
				"DELETE"
		);
	}

	@Test
	public void testNotifyContentChanged() {
		mEntity.notifyContentChanged();
	}

	@Test
	public void testNotifyContentChangedWhenDisabledNotifications() {
		mEntity.setContentChangeNotificationEnabled(false);
		mEntity.notifyContentChanged();
		mEntity.setContentChangeNotificationEnabled(true);
	}

	@Test
	public void testNotifyContentChangedForNotAttachedEntity() {
		assertThatActionForNotAttachedEntityThrowsIllegalStateException(
				new Runnable() {
					@Override
					public void run() {
						mNotAttachedEntity.notifyContentChanged();
					}
				},
				"NOTIFY CONTENT CHANGE"
		);
	}

	private void assertContentOfEntityInSize(int size) {
		assertContentOfEntityInSize(size, null);
	}

	@SuppressWarnings("ConstantConditions")
	private void assertContentOfEntityInSize(int size, String selection, Object... selectionArgs) {
		String[] argsArray = null;
		if (selectionArgs != null && selectionArgs.length > 0) {
			argsArray = new String[selectionArgs.length];
			for (int i = 0; i < argsArray.length; i++) {
				argsArray[i] = String.valueOf(selectionArgs[i]);
			}
		}
		final Cursor cursor = mEntity.query(null, selection, argsArray, null);
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.getCount(), is(size));
	}

	private static ContentValues[] initializeContentValuesArray(int initialSize) {
		final ContentValues[] valuesArray = new ContentValues[initialSize];
		for (int i = 0; i < initialSize; i++) {
			valuesArray[i] = createNotEmptyContentValues();
		}
		return valuesArray;
	}

	private static ContentValues createNotEmptyContentValues() {
		final ContentValues values = new ContentValues();
		values.put(TestEntity.Columns.ID, 0L);
		return values;
	}

	private static ContentValues createContentValues(long id, String title, long typeId) {
		final ContentValues values = new ContentValues(3);
		values.put(TestEntity.Columns.ID, id);
		values.put(TestEntity.Columns.TITLE, title);
		values.put(TestEntity.Columns.TYPE_ID, typeId);
		return values;
	}

	private static ContentValues createContentValues(String title, long typeId) {
		final ContentValues values = new ContentValues(2);
		values.put(TestEntity.Columns.TITLE, title);
		values.put(TestEntity.Columns.TYPE_ID, typeId);
		return values;
	}

	private static void assertThatActionForNotAttachedEntityThrowsIllegalStateException(Runnable action, String actionName) {
		try {
			action.run();
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Cannot perform entity action(" + actionName + "). " +
							"Entity(TestEntity) is not attached to any database.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	private static final class TestEntity2 extends Entity {

		private boolean onCreateDispatched,
				onCreateInitialContentDispatched,
				onUpgradeDispatched,
				onUpgradeContentDispatched,
				onDowngradeDispatched;

		@Override
		protected void onCreate(@NonNull SQLiteDatabase db) {
			this.onCreateDispatched = true;
		}

		@Override
		protected void onCreateInitialContent(@NonNull SQLiteDatabase db) {
			super.onCreateInitialContent(db);
			this.onCreateInitialContentDispatched = true;
		}

		@Override
		protected void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
			super.onUpgrade(db, oldVersion, newVersion);
			this.onUpgradeDispatched = true;
		}

		@Override
		protected void onUpgradeContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
			super.onUpgradeContent(db, oldVersion, newVersion);
			this.onUpgradeContentDispatched = true;
		}

		@Override
		protected void onDowngrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
			super.onDowngrade(db, oldVersion, newVersion);
			this.onDowngradeDispatched = true;
		}
	}
}
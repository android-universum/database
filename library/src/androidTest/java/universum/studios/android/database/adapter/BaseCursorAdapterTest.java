/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.AbsSavedState;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.CursorItemView;
import universum.studios.android.database.cursor.BaseCursorWrapper;
import universum.studios.android.database.inner.DatabaseTests;
import universum.studios.android.database.mock.MockCursor;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelCursor;
import universum.studios.android.database.test.R;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class BaseCursorAdapterTest extends AdapterBaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "BaseCursorAdapterTest";

	private static final int TEST_CURSOR_ROWS_COUNT = 20;

	private ViewGroup mParentView;
	private View mItemView;
	private TestAdapter mAdapter, mEmptyAdapter;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mParentView = new ListView(context);
		this.mItemView = LayoutInflater.from(context).inflate(R.layout.test_item_list, mParentView, false);
		this.mAdapter = new TestAdapter(context);
		this.mAdapter.changeCursor(TestModel.createCursorWithTestValues(TEST_CURSOR_ROWS_COUNT));
		this.mEmptyAdapter = new TestAdapter(context);
	}

	@Test
	public void testInstantiation() {
		final TestAdapter adapter = new TestAdapter(context);
		assertThatAdapterIsProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithCursor() {
		final TestAdapter adapter = new TestAdapter(context, new TestModelCursor(new MockCursor(0)));
		assertThatAdapterIsProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(not(nullValue())));
		assertThat(adapter.getCursor(), is(not(nullValue())));
	}

	@Test
	public void testInstantiationWithDisabledAnnotationsProcessing() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		final TestAdapter adapter = new TestAdapter(context);
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		assertThatAdapterIsProperlyInstantiated(adapter);
		assertThat(adapter.mAnnotationHandler, is(nullValue()));
	}

	private static void assertThatAdapterIsProperlyInstantiated(BaseCursorAdapter adapter) {
		assertThat(adapter, is(not(nullValue())));
		assertThat(adapter.mContext, is(not(nullValue())));
		assertThat(adapter.mResources, is(not(nullValue())));
		assertThat(adapter.mLayoutInflater, is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mAdapter.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandlerWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {
					@Override
					public void run() {
						mAdapter.getAnnotationHandler();
					}
				},
				DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED
		);
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testSaveInstanceState() {
		mAdapter.selectedItemsCount = 11;
		final Parcelable state = mAdapter.saveInstanceState();
		assertThat(state, is(not(nullValue())));
		assertThat(state instanceof TestAdapter.AdapterState, is(true));
		assertThat(((TestAdapter.AdapterState) state).selectedItemsCount, is(11));
	}

	@Test
	public void testRestoreInstanceState() {
		mAdapter.selectedItemsCount = 11;
		final Parcelable state = mAdapter.saveInstanceState();

		final TestAdapter restoreAdapter = new TestAdapter(context);
		restoreAdapter.restoreInstanceState(state);
		assertThat(restoreAdapter.selectedItemsCount, is(11));
	}

	@Test
	public void testSetOnCursorDataSetListener() {
		mAdapter.registerOnCursorDataSetListener(new TestOnCursorDataSetListener());
	}

	@Test
	public void testOnCursorDataSetActionListener() {
		mAdapter.registerOnCursorDataSetActionListener(new TestOnCursorDataSetActionListener());
	}

	@Test
	public void testNotifyCursorDataSetLoaded() {
		final TestOnCursorDataSetListener listener = new TestOnCursorDataSetListener();
		mAdapter.registerOnCursorDataSetListener(listener);
		mAdapter.notifyCursorDataSetLoaded(0);
		assertThat(listener.loadedCallbackReceived, is(true));
		assertThat(listener.changedCallbackReceived, is(false));
		assertThat(listener.invalidatedCallbackReceived, is(false));
	}

	@Test
	public void testNotifyCursorDataSetLoadedWithoutListener() {
		mAdapter.notifyCursorDataSetLoaded(0);
	}

	@Test
	public void testNotifyCursorDataSetChanged() {
		final TestOnCursorDataSetListener listener = new TestOnCursorDataSetListener();
		mAdapter.registerOnCursorDataSetListener(listener);
		mAdapter.notifyCursorDataSetChanged();
		assertThat(listener.loadedCallbackReceived, is(false));
		assertThat(listener.changedCallbackReceived, is(true));
		assertThat(listener.invalidatedCallbackReceived, is(false));
	}

	@Test
	public void testNotifyCursorDataSetChangedWithoutListener() {
		mAdapter.notifyCursorDataSetChanged();
	}

	@Test
	public void testNotifyCursorDataSetInvalidated() {
		final TestOnCursorDataSetListener listener = new TestOnCursorDataSetListener();
		mAdapter.registerOnCursorDataSetListener(listener);
		mAdapter.notifyCursorDataSetInvalidated();
		assertThat(listener.loadedCallbackReceived, is(false));
		assertThat(listener.changedCallbackReceived, is(false));
		assertThat(listener.invalidatedCallbackReceived, is(true));
	}

	@Test
	public void testNotifyCursorDataSetInvalidatedWithoutListener() {
		mAdapter.notifyCursorDataSetInvalidated();
	}

	@Test
	public void testNotifyCursorDataSetActionSelected() {
		final TestOnCursorDataSetActionListener listener = new TestOnCursorDataSetActionListener();
		mAdapter.registerOnCursorDataSetActionListener(listener);
		this.testNotifyCursorDataSetActionSelectedInner(listener, 1, 0, null);
		this.testNotifyCursorDataSetActionSelectedInner(listener, 2, 15, new Date());
	}

	private void testNotifyCursorDataSetActionSelectedInner(TestOnCursorDataSetActionListener listener, int action, int position, Object data) {
		listener.reset();
		mAdapter.notifyCursorDataSetActionSelected(action, position, data);
		assertThat(listener.actionSelectedCallbackReceived, is(true));
		assertThat(listener.action, is(action));
		assertThat(listener.position, is(position));
		assertThat(listener.id, is(mAdapter.getItemId(position)));
		assertThat(listener.data, is(data));
	}

	@Test
	public void testNotifyCursorDataSetActionSelectedProcessedByAdapter() {
		final TestAdapter adapter = new TestAdapter(context) {

			@Override
			protected boolean onCursorDataSetActionSelected(int action, int position, @Nullable Object payload) {
				return action == 0;
			}
		};
		final TestOnCursorDataSetActionListener listener = new TestOnCursorDataSetActionListener();
		adapter.registerOnCursorDataSetActionListener(listener);
		adapter.notifyCursorDataSetActionSelected(0, 0, null);
		assertThat(listener.actionSelectedCallbackReceived, is(false));
	}

	@Test
	public void testNotifyCursorDataSetActionSelectedWithoutListener() {
		mAdapter.notifyCursorDataSetActionSelected(0, 0, null);
	}

	@Test
	public void testChangeCursor() {
		assertThat(mAdapter.getCursor(), is(not(nullValue())));
		mAdapter.changeCursor(null);
		assertThat(mAdapter.getCursor(), is(nullValue()));
		mAdapter.changeCursor(new MockCursor(0));
		assertThat(mAdapter.getCursor(), is(not(nullValue())));
	}

	@Test
	public void testSwapCursor() {
		final Cursor cursor = mAdapter.getCursor();
		assertThat(mAdapter.swapCursor(null), is(cursor));
		assertThat(mAdapter.getCursor(), is(nullValue()));
	}

	@Test
	public void testSwapCursorChangeProcessedByAdapter() {
		final TestAdapter adapter = new TestAdapter(context) {

			@Override
			protected boolean onCursorChange(@Nullable TestModelCursor newCursor, @Nullable TestModelCursor oldCursor) {
				notifyCursorDataSetChanged();
				return true;
			}
		};
		adapter.swapCursor(new MockCursor(0));
		adapter.swapCursor(null);
	}

	@Test
	public void testSwapSameCursor() {
		final Cursor cursor = TestModel.createCursorWithTestValues();
		assertThat(mAdapter.swapCursor(cursor), is(not(nullValue())));
		assertThat(mAdapter.swapCursor(mAdapter.getCursor()), is(nullValue()));
	}

	@Test
	public void testWrapCursor() {
		final Cursor cursor = TestModel.createCursorWithTestValues(100);
		final TestModelCursor testCursor = mAdapter.wrapCursor(cursor);
		assertThat(testCursor, is(not(nullValue())));
		assertThat(testCursor, is(not(cursor)));
		assertThat(testCursor.getCount(), is(cursor.getCount()));
	}

	@Test
	public void testWrapCursorOfSameType() {
		final TestModelCursor cursor = new TestModelCursor(TestModel.createCursorWithTestValues());
		assertThat(mAdapter.wrapCursor(cursor), is(cursor));
	}

	@Test
	public void testWrapCursorWithAdapterImplementation() {
		final TestAdapter adapter = new TestAdapter(context) {

			@NonNull
			@Override
			public TestModelCursor wrapCursor(@NonNull Cursor cursor) {
				return new TestModelCursor(cursor);
			}
		};
		assertThat(adapter.wrapCursor(new MockCursor(0)), is(not(nullValue())));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetCursorAt() {
		for (int i = 0; i < TEST_CURSOR_ROWS_COUNT; i++) {
			final TestModelCursor cursor = mAdapter.getCursorAt(i);
			assertThat(cursor, is(not(nullValue())));
			assertThat(cursor.accessModel().id, is(i + 1L));
		}
	}

	@Test
	public void testGetCursorAtInvalidPosition() {
		testActionThrowsIndexOutOfBoundsException(new Runnable() {
			@Override
			public void run() {
				mAdapter.getCursorAt(-1);
			}
		}, "Requested cursor at invalid position(-1). Cursor has rows in count of(" + TEST_CURSOR_ROWS_COUNT + ").");
		testActionThrowsIndexOutOfBoundsException(new Runnable() {
			@Override
			public void run() {
				mAdapter.getCursorAt(TEST_CURSOR_ROWS_COUNT);
			}
		}, "Requested cursor at invalid position(" + TEST_CURSOR_ROWS_COUNT + "). Cursor has rows in count of(" + TEST_CURSOR_ROWS_COUNT + ").");
	}

	@Test
	public void testGetCursorAtWithoutCursor() {
		final TestAdapter adapter = new TestAdapter(context);
		assertThat(adapter.getCursorAt(10), is(nullValue()));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetCursor() {
		final Cursor cursor = mAdapter.getCursor();
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.getCount(), is(TEST_CURSOR_ROWS_COUNT));
	}

	@Test
	public void testGetCount() {
		assertThat(mEmptyAdapter.getCount(), is(0));
		mEmptyAdapter.changeCursor(TestModel.createCursorWithTestValues(TEST_CURSOR_ROWS_COUNT));
		assertThat(mAdapter.getCount(), is(TEST_CURSOR_ROWS_COUNT));
	}

	@Test
	public void testHasItem() {
		for (int i = 0; i < TEST_CURSOR_ROWS_COUNT; i++) {
			assertThat(mAdapter.hasItemAt(i), is(true));
		}
	}

	@Test
	public void testHasItemWithInvalidPosition() {
		assertThat(mAdapter.hasItemAt(-5), is(false));
		assertThat(mAdapter.hasItemAt(TEST_CURSOR_ROWS_COUNT + 10), is(false));
	}

	@Test
	public void testGetItem() {
		for (int i = 0; i < TEST_CURSOR_ROWS_COUNT; i++) {
			final TestModel item = mAdapter.getItem(i);
			assertThat(item, is(not(nullValue())));
			assertThat(item.id, is(i + 1L));
		}
	}

	@Test
	public void testGetItemWithInvalidPosition() {
		testActionThrowsIndexOutOfBoundsException(new Runnable() {
			@Override
			public void run() {
				mAdapter.getItem(-2);
			}
		}, "Requested item at invalid position(-2). Data set has items in count of(" + TEST_CURSOR_ROWS_COUNT + ").");
		testActionThrowsIndexOutOfBoundsException(new Runnable() {
			@Override
			public void run() {
				mAdapter.getItem(TEST_CURSOR_ROWS_COUNT);
			}
		}, "Requested item at invalid position(" + TEST_CURSOR_ROWS_COUNT + "). Data set has items in count of(" + TEST_CURSOR_ROWS_COUNT + ").");
	}

	@Test
	public void testGetItemWithNotItemCursor() {
		final TestAdapterWithoutEntityModel adapter = new TestAdapterWithoutEntityModel(context);
		adapter.changeCursor(new MockCursor(10));
		try {
			adapter.getItem(5);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Adapter(TestAdapterWithoutEntityModel) does not have its attached cursor type of ItemCursor. " +
							"Implementation of TestAdapterWithoutEntityModel.getItem(int) is required.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testGetItemId() {
		for (int i = 0; i < TEST_CURSOR_ROWS_COUNT; i++) {
			assertThat(mAdapter.getItemId(i), is(i + 1L));
		}
	}

	@Test
	public void testGetItemIdWithNoStableIds() {
		final TestAdapter adapter = new TestAdapter(context) {
			@Override
			public boolean hasStableIds() {
				return false;
			}
		};
		assertThat(adapter.getItemId(0), is(TestAdapter.NO_ID));
	}

	@Test
	public void testGetItemIdWithoutCursor() {
		final TestAdapter adapter = new TestAdapter(context);
		assertThat(adapter.getItemId(0), is(TestAdapter.NO_ID));
	}

	@Test
	public void testHasStableIds() {
		assertThat(mAdapter.hasStableIds(), is(true));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetView() {
		final View view = mAdapter.getView(4, null, mParentView);
		assertThat(view instanceof TextView, is(true));
		assertThat(mAdapter.currentViewType(), is(0));

		final CursorViewHolder holder = (CursorViewHolder) view.getTag();
		assertThat(holder, is(not(nullValue())));
		assertThat(holder.getAdapterPosition(), is(4));
		assertThat(((TextView) view).getText().toString(), is("5"));
	}

	@Test
	public void testGetViewWithRecycledView() {
		mItemView.setTag(new CursorViewHolder(mItemView));
		final View view = mAdapter.getView(2, mItemView, mParentView);
		assertThat(mItemView, is(view));

		final CursorViewHolder holder = (CursorViewHolder) view.getTag();
		assertThat(holder, is(not(nullValue())));
		assertThat(holder.getAdapterPosition(), is(2));
		assertThat(((TextView) view).getText().toString(), is("3"));
	}

	@Test
	public void testGetViewWithoutHolder() {
		final TestAdapterWithoutCursorViewHolder adapter = new TestAdapterWithoutCursorViewHolder(context, new MockCursor(1)) {
			@Override
			protected void onBindViewHolder(@NonNull View holder, @NonNull Cursor cursor, int position) {
				// Ignored.
			}
		};
		final View view = adapter.getView(0, null, mParentView);
		assertThat(view instanceof TextView, is(true));
	}

	@Test
	public void testGetViewWithoutHolderWithRecycledView() {
		final TestAdapterWithoutCursorViewHolder adapter = new TestAdapterWithoutCursorViewHolder(context, new MockCursor(1)) {
			@Override
			protected void onBindViewHolder(@NonNull View holder, @NonNull Cursor cursor, int position) {
				// Ignored.
			}
		};
		final View view = adapter.getView(0, mItemView, mParentView);
		assertThat(mItemView, is(view));
	}

	@Test
	public void testOnCreateView() {
		final View view = mAdapter.onCreateView(mParentView, 0);
		assertThat(view instanceof TextView, is(true));
	}

	@Test
	public void testOnCreateViewWithoutDropDownViewAnnotation() {
		final TestAdapterWithoutAnnotation adapter = new TestAdapterWithoutAnnotation(context);
		try {
			adapter.onCreateView(mParentView, 0);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISSING_CLASS_ANNOTATION));
			assertThat(
					e.getMessage(),
					is("@CursorItemView annotation for class(TestAdapterWithoutAnnotation) is missing. " +
							"Cannot create view for the position(0) without item view layout resource.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testInflate() {
		final View view = mAdapter.inflate(R.layout.test_item_list, mParentView);
		assertThat(view instanceof TextView, is(true));
	}

	@Test
	public void testOnCreateViewHolder() {
		assertThat(mAdapter.onCreateViewHolder(mItemView, 0), is(not(nullValue())));
	}

	@Test
	public void testEnsureViewHolderPosition() {
		final CursorViewHolder holder = new CursorViewHolder(mItemView);
		assertThat(holder.getAdapterPosition(), is(CursorDataSetAdapter.NO_POSITION));
		mAdapter.ensureViewHolderPosition(holder, 12);
		assertThat(holder.getAdapterPosition(), is(12));

	}

	@Test
	public void testEnsureViewHolderPositionWithNotCursorViewHolder() {
		mAdapter.ensureViewHolderPosition(new Object(), 0);
	}

	@Test
	public void testOnBindViewHolder() {
		final CursorViewHolder holder = new CursorViewHolder(mItemView);
		mAdapter.onBindViewHolder(holder, 2);
		assertThat(((TextView) holder.itemView).getText().toString(), is("3"));
	}

	@Test
	public void testOnBindViewHolderWithoutCursor() {
		mAdapter.swapCursor(null);
		try {
			mAdapter.onBindViewHolder(new CursorViewHolder(mItemView), 2);
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Cannot present data from the invalid cursor.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testOnBindViewHolderWithInvalidPosition() {
		try {
			mAdapter.onBindViewHolder(new CursorViewHolder(mItemView), TEST_CURSOR_ROWS_COUNT);
		} catch (IllegalStateException e) {
			assertThat(
					e.getMessage(),
					is("Cannot move the attached cursor to the position(" + TEST_CURSOR_ROWS_COUNT + ") corrected as(" + TEST_CURSOR_ROWS_COUNT + ").")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testOnBindViewHolderCursor() {
		final CursorViewHolder holder = new CursorViewHolder(mItemView);
		mAdapter.onBindViewHolder(holder, mAdapter.getCursorAt(0), 0);
		assertThat(((TextView) holder.itemView).getText().toString(), is("1"));
	}

	@Test
	public void testCorrectCursorPosition() {
		assertThat(mAdapter.correctCursorPosition(10), is(10));
	}

	private static void testActionThrowsIndexOutOfBoundsException(Runnable action, String expectedMessage) {
		try {
			action.run();
		} catch (IndexOutOfBoundsException e) {
			assertThat(e.getMessage(), is(expectedMessage));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@CursorItemView(R.layout.test_item_list)
	private static class TestAdapter extends BaseCursorAdapter<TestModelCursor, TestModel, CursorViewHolder> {

		int selectedItemsCount;

		public TestAdapter(@NonNull Context context) {
			super(context);
		}

		public TestAdapter(@NonNull Context context, @Nullable TestModelCursor cursor) {
			super(context, cursor);
		}

		@NonNull
		@Override
		public TestModelCursor wrapCursor(@NonNull Cursor cursor) {
			return TestModelCursor.wrap(cursor);
		}

		@Nullable
		@Override
		protected CursorViewHolder onCreateViewHolder(@NonNull View itemView, int position) {
			return new CursorViewHolder(itemView);
		}

		@Override
		@SuppressLint("SetTextI18n")
		protected void onBindViewHolder(@NonNull CursorViewHolder viewHolder, @NonNull TestModelCursor cursor, int position) {
			((TextView) viewHolder.itemView).setText(Long.toString(cursor.accessModel().id));
		}

		@NonNull
		@Override
		public Parcelable saveInstanceState() {
			final AdapterState state = new AdapterState(super.saveInstanceState());
			state.selectedItemsCount = selectedItemsCount;
			return state;
		}

		@Override
		public void restoreInstanceState(@NonNull Parcelable savedState) {
			if (!(savedState instanceof AdapterState)) {
				super.restoreInstanceState(savedState);
				return;
			}

			final AdapterState state = (AdapterState) savedState;
			super.restoreInstanceState(state.getSuperState());
			this.selectedItemsCount = state.selectedItemsCount;
		}

		@SuppressWarnings("InnerClassTooDeeplyNested")
		static final class AdapterState extends AbsSavedState {

			public static final Creator<AdapterState> CREATOR = new Creator<AdapterState>() {

				@Override
				public AdapterState createFromParcel(Parcel source) {
					return new AdapterState(source);
				}

				@Override
				public AdapterState[] newArray(int size) {
					return new AdapterState[size];
				}
			};

			int selectedItemsCount;

			AdapterState(Parcel source) {
				super(source);
				this.selectedItemsCount = source.readInt();
			}

			AdapterState(Parcelable superState) {
				super(superState);
			}

			@Override
			public void writeToParcel(@NonNull Parcel dest, int flags) {
				super.writeToParcel(dest, flags);
				dest.writeInt(selectedItemsCount);
			}
		}
	}

	@CursorItemView(R.layout.test_item_list)
	private static final class TestAdapterWithoutEntityModel extends BaseCursorAdapter<NotItemCursor, String, Void> {

		private TestAdapterWithoutEntityModel(@NonNull Context context) {
			super(context);
		}

		@NonNull
		@Override
		public NotItemCursor wrapCursor(@NonNull Cursor cursor) {
			return new NotItemCursor(cursor);
		}

		@Override
		protected void onBindViewHolder(@NonNull Void viewHolder, @NonNull NotItemCursor cursor, int position) {
		}
	}

	@CursorItemView(R.layout.test_item_list)
	private static class TestAdapterWithoutCursorViewHolder extends BaseCursorAdapter<Cursor, String, View> {

		private TestAdapterWithoutCursorViewHolder(@NonNull Context context, @Nullable Cursor cursor) {
			super(context, cursor);
		}

		@Override
		protected void onBindViewHolder(@NonNull View viewHolder, @NonNull Cursor cursor, int position) {
		}
	}

	private static final class TestAdapterWithoutAnnotation extends BaseCursorAdapter<TestModelCursor, TestModel, Void> {

		public TestAdapterWithoutAnnotation(@NonNull Context context) {
			super(context);
		}

		@Override
		protected void onBindViewHolder(@NonNull Void viewHolder, @NonNull TestModelCursor cursor, int position) {
		}
	}

	private static final class TestOnCursorDataSetListener implements OnCursorDataSetListener<TestAdapter> {

		boolean loadedCallbackReceived, changedCallbackReceived, invalidatedCallbackReceived;

		@Override
		public void onCursorDataSetLoaded(@NonNull TestAdapter adapter, int loaderId) {
			this.loadedCallbackReceived = true;
		}

		@Override
		public void onCursorDataSetChanged(@NonNull TestAdapter adapter) {
			this.changedCallbackReceived = true;
		}

		@Override
		public void onCursorDataSetInvalidated(@NonNull TestAdapter adapter) {
			this.invalidatedCallbackReceived = true;
		}
	}

	private static final class TestOnCursorDataSetActionListener implements OnCursorDataSetActionListener<TestAdapter> {

		boolean actionSelectedCallbackReceived;
		int action;
		int position;
		long id;
		Object data;

		@Override
		public boolean onCursorDataSetActionSelected(@NonNull TestAdapter adapter, int action, int position, long id, @Nullable Object payload) {
			this.actionSelectedCallbackReceived = true;
			this.action = action;
			this.position = position;
			this.id = id;
			this.data = payload;
			return true;
		}

		void reset() {
			this.actionSelectedCallbackReceived = false;
			this.action = -1;
			this.position = -1;
			this.id = -1;
			this.data = null;
		}
	}

	private static final class NotItemCursor extends BaseCursorWrapper {

		public NotItemCursor(@NonNull Cursor cursor) {
			super(cursor);
		}

		@Override
		protected boolean onBindData() {
			return true;
		}

		@Override
		protected void onClearData() {
		}
	}
}
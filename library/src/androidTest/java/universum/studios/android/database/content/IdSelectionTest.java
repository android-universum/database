/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import androidx.test.runner.AndroidJUnit4;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class IdSelectionTest {

	@SuppressWarnings("unused")
	private static final String TAG = "IdSelectionTest";
	private static final String COLUMN_NAME = "_id";

	@Test
	public void testBuildWithSingleId() {
		final IdSelection idSelection = new IdSelection(COLUMN_NAME).withIds(new long[]{123456L});
		assertThat(idSelection.build(), is(COLUMN_NAME + "=?"));
		assertThat(idSelection.buildArgs(), is(new String[]{Long.toString(123456L)}));
	}

	@Test
	public void testBuildWithMultipleIds() {
		assertSelectionInLength(
				new IdSelection(COLUMN_NAME).withIds(
						new int[]{0, 15}
				),
				2
		);
		assertSelectionInLength(
				new IdSelection(COLUMN_NAME).withIds(
						new long[]{16546541L, 1654L, 164564L}
				),
				3
		);
		assertSelectionInLength(
				new IdSelection(COLUMN_NAME).withIds(
						new String[]{"165464"}
				),
				1
		);
	}

	private static void assertSelectionInLength(IdSelection idSelection, int length) {
		final String selection = idSelection.build();
		String expectedSelection = "";
		for (int i = 0; i < length; i++) {
			expectedSelection += COLUMN_NAME + "=?";
			if (i < (length - 1)) {
				expectedSelection += " OR ";
			}
		}
		assertThat(selection, is(expectedSelection));
		// We assume here that ArrayWrapper is properly tested so we do not need to test proper
		// conversion to String[] args array.
		final String[] args = idSelection.buildArgs();
		assertThat(args, is(not(nullValue())));
		assertThat(args.length, is(length));
	}

	@Test
	public void testBuildForMultipleIdsWithEmptyArray() {
		final IdSelection idSelection = new IdSelection(COLUMN_NAME).withIds(new long[0]);
		assertThat(idSelection.build(), is(""));
		assertThat(idSelection.buildArgs(), is(new String[0]));
	}

	@Test
	public void testArrayWrapperWrapArrayOfInts() {
		final IdSelection.ArrayWrapper arrayWrapper = IdSelection.ArrayWrapper.wrapArray(new int[]{123, 1234});
		assertThat(arrayWrapper instanceof IdSelection.ArrayWrapper.IntArrayWrapper, is(true));
		assertThat(arrayWrapper.length, is(2));
		assertThat(
				arrayWrapper.asStringArray(),
				is(new String[]{
						Integer.toString(123),
						Integer.toString(1234)
				})
		);
	}

	@Test
	public void testArrayWrapperWrapArrayOfLongs() {
		final IdSelection.ArrayWrapper arrayWrapper = IdSelection.ArrayWrapper.wrapArray(new long[]{12L, 46L, 99L});
		assertThat(arrayWrapper instanceof IdSelection.ArrayWrapper.LongArrayWrapper, is(true));
		assertThat(arrayWrapper.length, is(3));
		assertThat(
				arrayWrapper.asStringArray(),
				is(new String[]{
						Long.toString(12L),
						Long.toString(46L),
						Long.toString(99L)
				})
		);
	}

	@Test
	public void testArrayWrapperWrapEmptyArray() {
		final IdSelection.ArrayWrapper arrayWrapper = IdSelection.ArrayWrapper.wrapArray(new String[0]);
		assertThat(arrayWrapper instanceof IdSelection.ArrayWrapper.StringArrayWrapper, is(true));
		assertThat(arrayWrapper.length, is(0));
	}

	@Test
	public void testArrayWrapperWrapNullArray() {
		final IdSelection.ArrayWrapper wrapper = IdSelection.ArrayWrapper.wrapArray(null);
		assertThat(wrapper instanceof IdSelection.ArrayWrapper.EmptyArrayWrapper, is(true));
		assertThat(wrapper.length, is(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testArrayWrapperWrapUnsupportedArray() {
		IdSelection.ArrayWrapper.wrapArray(new Date[]{new Date(), new Date()});
	}
}
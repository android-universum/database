/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.Database;
import universum.studios.android.database.DatabaseEntity;
import universum.studios.android.database.annotation.DatabaseEntities;
import universum.studios.android.database.loremipsum.LoremIpsumEntity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class DatabaseAnnotationHandlerTest extends AnnotationHandlerBaseTest<DatabaseAnnotationHandler> {

	@SuppressWarnings("unused")
	private static final String TAG = "DatabaseAnnotationHandlerTest";

	public DatabaseAnnotationHandlerTest() {
		super(BaseAnnotationHandlers.DatabaseHandler.class);
	}

	@Override
	DatabaseAnnotationHandler onObtainHandler() {
		return BaseAnnotationHandlers.obtainDatabaseHandler(TestDatabase.class);
	}

	@Override
	DatabaseAnnotationHandler onObtainEmptyHandler() {
		return BaseAnnotationHandlers.obtainDatabaseHandler(TestDatabaseWithoutAnnotation.class);
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetEntities() {
		final Class<? extends DatabaseEntity>[] entities = mHandler.getEntities();
		assertThat(entities, is(not(nullValue())));
		assertThat(entities.length, is(1));
		assertThat(LoremIpsumEntity.class.equals(entities[0]), is(true));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetEntitiesForDatabaseWithEmptyEntities() {
		final Class<? extends DatabaseEntity>[] entities = BaseAnnotationHandlers.obtainDatabaseHandler(TestDatabaseWithEmptyEntities.class).getEntities();
		assertThat(entities, is(not(nullValue())));
		assertThat(entities.length, is(0));
	}

	@Test
	public void testGetEntitiesForDatabaseWithoutEntities() {
		assertThat(mEmptyHandler.getEntities(), is(nullValue()));
	}

	@DatabaseEntities({LoremIpsumEntity.class})
	private static final class TestDatabase extends Database {

		public TestDatabase() {
			super(1, "test.db");
		}
	}

	@DatabaseEntities({})
	private static final class TestDatabaseWithEmptyEntities extends Database {

		public TestDatabaseWithEmptyEntities() {
			super(1, "test.db");
		}
	}

	private static final class TestDatabaseWithoutAnnotation extends Database {

		public TestDatabaseWithoutAnnotation() {
			super(1, "test.db");
		}
	}
}
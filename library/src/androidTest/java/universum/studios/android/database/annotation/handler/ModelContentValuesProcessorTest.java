/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.content.ContentValues;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;
import java.util.Date;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.entity.TestModelEntity;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelWithUnsupportedColumns;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
@SuppressWarnings("ResourceType")
public final class ModelContentValuesProcessorTest extends ModelProcessorBaseTest<ModelContentValuesProcessor> {

	@SuppressWarnings("unused")
	private static final String TAG = "ModelContentValuesProcessorTest";

	private ContentValues mContentValues;

	public ModelContentValuesProcessorTest() {
		super(new ModelContentValuesProcessor());
	}

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mModel = TestModel.withTestValues();
		this.mModelWithUnsupportedColumns = TestModelWithUnsupportedColumns.witTestValues();
		this.mContentValues = new ContentValues();
	}

	@Test
	public void testProcessModel() {
		final ContentValues contentValues = mProcessor.processModel(mModel, mColumns);
		assertThat(contentValues, is(not(nullValue())));
		assertThat(contentValues.size(), is(TestModel.INSERTABLE_CONTENT_VALUES_COUNT));
		assertThat((Long) contentValues.get(TestModelEntity.Columns.ID), is(TestModel.TestValues.ID));
		assertThat((String) contentValues.get(TestModelEntity.Columns.TAG), is(TestModel.TestValues.TAG));
		assertThat((String) contentValues.get(TestModelEntity.Columns.AUTHOR), is(TestModel.TestValues.AUTHOR));
		// title is NULL but not nullable so it is not included in the ContentValues:
		// assertEquals(TestModel.TestValues.TITLE, contentValues.getAsString(TestModelEntity.Columns.TITLE));
		assertThat((String) contentValues.get(TestModelEntity.Columns.DESCRIPTION), is(TestModel.TestValues.DESCRIPTION));
		// count is NULL but nullable so it is included in the ContentValues:
		assertThat((Integer) contentValues.get(TestModelEntity.Columns.COUNT), is(TestModel.TestValues.COUNT));
		assertThat((Double) contentValues.get(TestModelEntity.Columns.LATITUDE), is(TestModel.TestValues.LATITUDE));
		assertThat((Double) contentValues.get(TestModelEntity.Columns.LONGITUDE), is(TestModel.TestValues.LONGITUDE));
		// categoryId is NULL but not nullable so it is not included in the ContentValues:
		// assertEquals(TestModel.TestValues.CATEGORY_ID, contentValues.get(TestModelEntity.Columns.CATEGORY_ID));
		// categoryName is @Joined column so it is not included in the ContentValues:
		// assertEquals(TestModel.TestValues.CATEGORY_NAME, contentValues.get(TestModelEntity.JoinedColumns.CATEGORY_NAME));
		assertThat(contentValues.getAsInteger(TestModelEntity.Columns.ENABLED) == 1, is(TestModel.TestValues.ENABLED));
		assertThat(contentValues.getAsInteger(TestModelEntity.Columns.PUBLISHED) == 1, is(TestModel.TestValues.PUBLISHED));
		assertThat((Integer) contentValues.get(TestModelEntity.Columns.FLAGS), is(TestModel.TestValues.FLAGS));
		assertThat((Float) contentValues.get(TestModelEntity.Columns.RATIO), is(TestModel.TestValues.RATIO));
		assertThat((String) contentValues.get(TestModelEntity.Columns.DIFFICULTY), is(TestModel.TestValues.DIFFICULTY.name()));
		assertThat((Byte) contentValues.get(TestModelEntity.Columns.ORIENTATION), is(TestModel.TestValues.ORIENTATION));
		assertThat((Short) contentValues.get(TestModelEntity.Columns.CODE), is(TestModel.TestValues.CODE));
	}

	@Test
	public void testProcessModelWithUnsupportedColumns() {
		assertThatProcessingThrowsExceptionWithMessage(
				new Runnable() {
					@Override
					public void run() {
						mProcessor.processModel(
								mModelWithUnsupportedColumns,
								mUnsupportedColumns
						);
					}
				},
				"Failed to insert value of column field into ContentValues. " +
						"Field(TestModelWithUnsupportedColumns.date) type of(Date) is not allowed to represent a column. " +
						"Only primitive type fields, Enums including, are allowed as columns."
		);
	}

	@Test
	public void testOnProcessColumnAsJoined() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("categoryId")) {

			@Override
			public boolean isJoined() {
				return true;
			}
		};
		final ModelContentValuesProcessor.ValuesTask task = ModelContentValuesProcessor.ValuesTask.obtainFor(mModel, mColumns);
		assertThat(mProcessor.onProcessColumn(task, annotatedColumn), is(true));
		assertThat(task.contentValues.containsKey(annotatedColumn.getName()), is(false));
		task.recycle();

	}

	@Test
	public void testOnProcessColumn() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("author"));
		final ModelContentValuesProcessor.ValuesTask task = ModelContentValuesProcessor.ValuesTask.obtainFor(mModel, mColumns);
		assertThat(mProcessor.onProcessColumn(task, annotatedColumn), is(true));
		assertThat(task.contentValues.getAsString(annotatedColumn.getName()), is(TestModel.TestValues.AUTHOR));
		task.recycle();
	}

	@Test
	public void testOnProcessColumnWithNullValue() throws Exception {
		mModel.author = null;
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("author"));
		final ModelContentValuesProcessor.ValuesTask task = ModelContentValuesProcessor.ValuesTask.obtainFor(mModel, mColumns);
		assertThat(mProcessor.onProcessColumn(task, annotatedColumn), is(true));
		assertThat(task.contentValues.containsKey(annotatedColumn.getName()), is(false));
		task.recycle();
	}

	@Test
	public void testOnProcessColumnAsNullable() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("count")) {

			@Override
			public boolean isNullable() {
				return true;
			}
		};
		final ModelContentValuesProcessor.ValuesTask task = ModelContentValuesProcessor.ValuesTask.obtainFor(mModel, mColumns);
		assertThat(mProcessor.onProcessColumn(task, annotatedColumn), is(true));
		assertThat(task.contentValues.getAsInteger(annotatedColumn.getName()), is(TestModel.TestValues.COUNT));
		task.recycle();
	}

	@Test
	public void testOnProcessColumnAsNullableWithNullValue() throws Exception {
		mModel.count = null;
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("count")) {

			@Override
			public boolean isNullable() {
				return true;
			}
		};
		final ModelContentValuesProcessor.ValuesTask task = ModelContentValuesProcessor.ValuesTask.obtainFor(mModel, mColumns);
		assertThat(mProcessor.onProcessColumn(task, annotatedColumn), is(true));
		assertThat(task.contentValues.containsKey(annotatedColumn.getName()), is(true));
		assertThat(task.contentValues.get(annotatedColumn.getName()), is(nullValue()));
		task.recycle();
	}

	@Test
	public void testOnProcessColumnOfUnsupportedType() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getField("date"));
		final ModelContentValuesProcessor.ValuesTask task = ModelContentValuesProcessor.ValuesTask.obtainFor(mModelWithUnsupportedColumns, mUnsupportedColumns);
		try {
			mProcessor.onProcessColumn(task, annotatedColumn);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Failed to insert value of column field into ContentValues. " +
							"Field(TestModelWithUnsupportedColumns.date) type of(Date) " +
							"is not allowed to represent a column. " +
							"Only primitive type fields, Enums including, are allowed as columns.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testPutValueOfColumnAsByte() throws Exception {
		this.testPutValueOfColumnInner("orientation", TestModel.TestValues.ORIENTATION);
	}

	@Test
	public void testPutValueOfColumnAsShort() throws Exception {
		this.testPutValueOfColumnInner("code", TestModel.TestValues.CODE);
	}

	@Test
	public void testPutValueOfColumnAsInteger() throws Exception {
		this.testPutValueOfColumnInner("flags", TestModel.TestValues.FLAGS);
	}

	@Test
	public void testPutValueOfColumnAsLong() throws Exception {
		this.testPutValueOfColumnInner("categoryId", TestModel.TestValues.CATEGORY_ID);
	}

	@Test
	public void testPutValueOfColumnAsFloat() throws Exception {
		this.testPutValueOfColumnInner("ratio", TestModel.TestValues.RATIO);
	}

	@Test
	public void testPutValueOfColumnAsDouble() throws Exception {
		this.testPutValueOfColumnInner("latitude", TestModel.TestValues.LATITUDE);
	}

	@Test
	public void testPutValueOfColumnAsBoolean() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("enabled"));
		assertThat(mProcessor.putValueOfColumn(annotatedColumn, mModel, mContentValues, annotatedColumn.getName()), is(true));
		assertThat(mContentValues.getAsInteger(annotatedColumn.getName()) == 1, is(TestModel.TestValues.ENABLED));
	}

	@Test
	public void testPutValueOfColumnAsString() throws Exception {
		this.testPutValueOfColumnInner("title", TestModel.TestValues.TITLE);
	}

	@Test
	public void testPutValueOfColumnAsEnum() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField("difficulty"));
		assertThat(mProcessor.putValueOfColumn(annotatedColumn, mModel, mContentValues, annotatedColumn.getName()), is(true));
		assertThat(mContentValues.getAsString(annotatedColumn.getName()), is(TestModel.TestValues.DIFFICULTY.name()));
	}

	@Test
	public void testPutValueOfColumnAsNull() throws Exception {
		this.testPutValueOfColumnInner("address", null);
	}

	private void testPutValueOfColumnInner(String columnFieldName, Object expected) throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField(columnFieldName));
		assertThat(mProcessor.putValueOfColumn(annotatedColumn, mModel, mContentValues, annotatedColumn.getName()), is(true));
		assertThat(mContentValues.get(annotatedColumn.getName()), is(expected));
	}

	@Test
	public void testPutValueOfColumnOfUnsupportedType() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getField("date"));
		assertThat(mProcessor.putValueOfColumn(annotatedColumn, mModelWithUnsupportedColumns, mContentValues, annotatedColumn.getName()), is(false));
		assertThat(mContentValues.containsKey(annotatedColumn.getName()), is(false));
	}

	@Test
	public void testPutValueOfColumnWithNotAccessibleField() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getDeclaredField("inaccessibleDate"), false);
		try {
			mProcessor.putValueOfColumn(annotatedColumn, mModelWithUnsupportedColumns, mContentValues, annotatedColumn.getName());
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_DEFAULT));
			assertThat(e.getMessage(), is("Field(TestModelWithUnsupportedColumns.inaccessibleDate) is not accessible."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testIsFieldValueNull() throws Exception {
		final Field dateField = TestModelWithUnsupportedColumns.class.getField("date");
		mModelWithUnsupportedColumns.date = null;
		assertThat(ModelContentValuesProcessor.isFieldValueNull(dateField, mModelWithUnsupportedColumns), is(true));
		mModelWithUnsupportedColumns.date = new Date();
		assertThat(ModelContentValuesProcessor.isFieldValueNull(dateField, mModelWithUnsupportedColumns), is(false));
	}

	@Test
	public void testIsFieldValueNullWithNotAccessibleField() throws Exception {
		final Field dateField = TestModelWithUnsupportedColumns.class.getDeclaredField("inaccessibleDate");
		try {
			ModelContentValuesProcessor.isFieldValueNull(dateField, mModelWithUnsupportedColumns);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_DEFAULT));
			assertThat(e.getMessage(), is("Field(TestModelWithUnsupportedColumns.inaccessibleDate) is not accessible."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testValuesTaskObtain() {
		final ModelContentValuesProcessor.ValuesTask task = ModelContentValuesProcessor.ValuesTask.obtain(0);
		assertThat(task, is(not(nullValue())));
		assertThat(task.model, is(nullValue()));
		assertThat(task.columns, is(nullValue()));
		assertThat(task.contentValues, is(not(nullValue())));
	}

	@Test
	public void testValuesTaskRecycle() {
		final int poolSize = ModelContentValuesProcessor.ValuesTask.POOL_SIZE;
		final int testPoolRange = poolSize * 2;
		final ModelContentValuesProcessor.ValuesTask[] tasks = new ModelContentValuesProcessor.ValuesTask[testPoolRange];
		for (int i = 0; i < testPoolRange; i++) {
			tasks[i] = ModelContentValuesProcessor.ValuesTask.obtain(0);
		}
		// Recycle all to populate the POOL.
		for (int i = 0; i < testPoolRange; i++) {
			tasks[i].recycle();
		}
		// When obtaining in size of the POOL all returned tasks are the same.
		for (int i = 0; i < poolSize; i++) {
			assertThat(ModelContentValuesProcessor.ValuesTask.obtain(0), is(tasks[i]));
		}
		for (int i = 0; i < testPoolRange; i++) {
			tasks[i].recycle();
		}
	}
}

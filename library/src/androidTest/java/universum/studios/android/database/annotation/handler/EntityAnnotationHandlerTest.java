/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.sqlite.SQLiteDatabase;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.annotation.EntityName;
import universum.studios.android.database.entity.Entity;
import universum.studios.android.database.entity.TestEntity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class EntityAnnotationHandlerTest extends AnnotationHandlerBaseTest<EntityAnnotationHandler> {

	@SuppressWarnings("unused")
	private static final String TAG = "EntityAnnotationHandlerTest";

	public EntityAnnotationHandlerTest() {
		super(EntityAnnotationHandlers.EntityHandler.class);
	}

	@Override
	EntityAnnotationHandler onObtainHandler() {
		return EntityAnnotationHandlers.obtainEntityHandler(TestEntity.class);
	}

	@Override
	EntityAnnotationHandler onObtainEmptyHandler() {
		return EntityAnnotationHandlers.obtainEntityHandler(NotAnnotatedEntity.class);
	}

	@Test
	public void testGetEntityName() {
		assertThat(mHandler.getEntityName(""), is(TestEntity.NAME));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetEntityNameWithEmptyValue() {
		assertThat(EntityAnnotationHandlers.obtainEntityHandler(TestEntityWithEmptyName.class).getEntityName("default"), is("default"));
	}

	@Test
	public void testGetEntityNameDefault() {
		assertThat(mEmptyHandler.getEntityName("default"), is("default"));
	}

	private static final class NotAnnotatedEntity extends Entity {

		@Override
		protected void onCreate(@NonNull SQLiteDatabase db) {
			// Ignored.
		}
	}

	@EntityName("")
	private static final class TestEntityWithEmptyName extends Entity {

		@Override
		protected void onCreate(@NonNull SQLiteDatabase db) {
			// Ignored.
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.entity;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.database.Cursor;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.TestPrimaryProvider;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.EntityName;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.inner.BaseTest;
import universum.studios.android.database.inner.DatabaseTests;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.database.model.TestModel;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static universum.studios.android.database.DatabaseTest.assertTableExists;
import static universum.studios.android.database.DatabaseTest.dropTableIfExists;
import static universum.studios.android.database.inner.DatabaseTests.assertThatActionWithDisabledAnnotationsProcessingThrowsException;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class ModelEntityTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "ModelEntityTest";
	private static final boolean MARSHMALLOW = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;

	private TestModelEntity mEntity;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		if (mEntity == null) this.mEntity = TestPrimaryProvider.accessDatabase().findEntityByClass(TestModelEntity.class);
		// Always have a clear content before each test.
		this.deleteEntityContent();
	}

	private void deleteEntityContent() {
		mEntity.dropContent();
	}

	@Test
	public void testInstantiation() {
		final TestModelEntity entity = new TestModelEntity();
		assertThat(entity.getName(), is(TestModelEntity.NAME));
		assertThat(entity.getModelClass(), Matchers.<Class<TestModel>>is(TestModel.class));
	}

	@Test
	public void testInstantiationWithoutModelAnnotation() {
		try {
			new TestModelEntityWithoutModelAnnotation();
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISSING_CLASS_ANNOTATION));
			assertThat(
					e.getMessage(),
					is("@Model annotation for class(TestModelEntityWithoutModelAnnotation) is missing.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testInstantiationWithDisabledAnnotationsProcessing() {
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = false;
		try {
			new TestModelEntity();
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Trying to access logic that requires annotations processing to be enabled, but " +
							"it seams that the annotations processing is disabled for the Database library.")
			);
			DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
			return;
		}
		DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED = true;
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testInstantiationWithNameAndClassOfModel() {
		final TestModelEntity entity = new TestModelEntity("test_model_entity", TestModel.class);
		assertThat(entity.getName(), is("test_model_entity"));
		assertThat(entity.getModelClass(), Matchers.<Class<TestModel>>is(TestModel.class));
	}

	@Test
	public void testGetAnnotationHandler() {
		assertThat(mEntity.getAnnotationHandler(), is(not(nullValue())));
	}

	@Test
	public void testGetAnnotationHandlerWithDisabledAnnotationsProcessing() {
		assertThatActionWithDisabledAnnotationsProcessingThrowsException(
				new Runnable() {
					@Override
					public void run() {
						mEntity.getAnnotationHandler();
					}
				},
				DatabaseTests.EXCEPTION_MESSAGE_ANNOTATIONS_PROCESSING_NOT_ENABLED
		);
	}

	@Test
	public void testOnCreate() {
		final TestModelEntity3 entity = new TestModelEntity3();
		entity.onCreate(TestPrimaryProvider.accessDatabase().asWritable());
		assertTableExists(entity.getName());
		dropTableIfExists(entity.getName());
		// Whether the created table has all columns specified via its Model is tested in ModelEntityCreatorTest.
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testOnCreateForModelWithoutColumns() {
		final ModelEntity entity = new ModelEntity("models", TestModelWithoutColumns.class) {
		};
		try {
			entity.onCreate(TestPrimaryProvider.accessDatabase().asWritable());
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Failed to create database table(models) for ModelEntity() " +
							"according to table structure defined by Model(TestModelWithoutColumns).")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testOnUpgrade() {
		// Ignored for now.
	}

	@Test
	public void testInsertInitialModels() {
		assertThat(mEntity.insertInitialModels(TestPrimaryProvider.accessDatabase().asWritable(), modelsWithValues(10)), is(10));
		assertContentOfEntityInSize(10);
	}

	@Test
	public void testInsertEmptyInitialModels() {
		assertThat(mEntity.insertInitialModels(TestPrimaryProvider.accessDatabase().asWritable(), new ArrayList<TestModel>(0)), is(0));
	}

	@SuppressLint("NewApi")
	@Test
	public void testCreateInsertModelsOperations() {
		final List<ContentProviderOperation> operations = mEntity.createInsertModelsOperations(modelsWithValues(5));
		assertThat(operations, is(not(nullValue())));
		assertThat(operations.size(), is(5));
		assertContentProviderOperationsAreValid(operations);
		assertThat(!MARSHMALLOW || operations.get(0).isInsert(), is(true));
	}

	@Test
	public void testCreateInsertModelsOperationsForEmptyList() {
		assertContentProviderOperationsAreEmpty(mEntity.createInsertModelsOperations(new ArrayList<TestModel>(0)));
	}

	@Test
	@SuppressLint("NewApi")
	public void testCreateInsertModelOperation() {
		final ContentProviderOperation operation = mEntity.createInsertModelOperation(modelWithValues());
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isInsert(), is(true));
	}

	@Test
	@SuppressLint("NewApi")
	public void testCreateUpdateModelsOperations() {
		final List<ContentProviderOperation> operations = mEntity.createUpdateModelsOperations(modelsWithValues(2));
		assertThat(operations, is(not(nullValue())));
		assertThat(operations.size(), is(2));
		assertContentProviderOperationsAreValid(operations);
		assertThat(!MARSHMALLOW || operations.get(0).isUpdate(), is(true));
	}

	@Test
	public void testCreateUpdateModelsOperationsForEmptyList() {
		assertContentProviderOperationsAreEmpty(mEntity.createUpdateModelsOperations(new ArrayList<TestModel>(0)));
	}

	@Test
	@SuppressLint("NewApi")
	public void testCreateUpdateModelOperation() {
		final ContentProviderOperation operation = mEntity.createUpdateModelOperation(modelWithValues());
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isUpdate(), is(true));
	}

	@Test
	@SuppressLint("NewApi")
	public void testCreateDeleteModelsOperations() {
		final List<ContentProviderOperation> operations = mEntity.createDeleteModelsOperation(modelsWithValues(3));
		assertThat(operations, is(not(nullValue())));
		assertThat(operations.size(), is(3));
		assertContentProviderOperationsAreValid(operations);
		assertThat(!MARSHMALLOW || operations.get(0).isDelete(), is(true));
	}

	@Test
	public void testCreateDeleteModelsOperationsForEmptyList() {
		assertContentProviderOperationsAreEmpty(mEntity.createDeleteModelsOperation(new ArrayList<TestModel>(0)));
	}

	@Test
	@SuppressLint("NewApi")
	public void testCreateDeleteModelOperation() {
		final ContentProviderOperation operation = mEntity.createDeleteModelOperation(modelWithValues());
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isDelete(), is(true));
	}

	@Test
	@SuppressLint("NewApi")
	public void testCreateAssertModelsOperations() {
		final List<ContentProviderOperation> operations = mEntity.createAssertModelsOperations(modelsWithValues(1));
		assertThat(operations, is(not(nullValue())));
		assertThat(operations.size(), is(1));
		assertContentProviderOperationsAreValid(operations);
		assertThat(!MARSHMALLOW || operations.get(0).isAssertQuery(), is(true));
	}

	@Test
	public void testCreateAssertModelsOperationsForEmptyList() {
		assertContentProviderOperationsAreEmpty(mEntity.createAssertModelsOperations(new ArrayList<TestModel>(0)));
	}

	@Test
	@SuppressLint("NewApi")
	public void testCreateAssertModelOperation() {
		final ContentProviderOperation operation = mEntity.createAssertModelOperation(modelWithValues());
		assertContentProviderOperationIsValid(operation);
		assertThat(!MARSHMALLOW || operation.isAssertQuery(), is(true));
	}

	private static void assertContentProviderOperationsAreEmpty(List<ContentProviderOperation> operations) {
		assertThat(operations, is(not(nullValue())));
		assertThat(operations.isEmpty(), is(true));
	}

	private void assertContentProviderOperationsAreValid(List<ContentProviderOperation> operations) {
		for (ContentProviderOperation operation : operations) {
			assertContentProviderOperationIsValid(operation);
		}
	}

	private void assertContentProviderOperationIsValid(ContentProviderOperation operation) {
		assertThat(operation, is(not(nullValue())));
		assertThat(operation.getUri(), is(mEntity.getContentUri()));
	}

	@Test
	public void testBulkInsertModels() {
		assertThat(mEntity.insertModels(modelsWithValues(6)), is(6));
		assertContentOfEntityInSize(6);
	}

	@Test
	public void testBulkInsertEmptyModels() {
		assertThat(mEntity.insertModels(new ArrayList<TestModel>(0)), is(0));
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testInsertModel() {
		assertThat(mEntity.insertModel(modelWithValues("#1")), is(not(nullValue())));
		assertContentOfEntityInSize(1);
		assertThat(mEntity.insertModel(modelWithValues("#2")), is(not(nullValue())));
		assertContentOfEntityInSize(2);
		assertThat(mEntity.insertModel(modelWithValues("#3")), is(not(nullValue())));
		assertThat(mEntity.insertModel(modelWithValues("#4")), is(not(nullValue())));
		assertContentOfEntityInSize(4);
	}

	@Test
	public void testQueryAllModels() {
		assertThat(mEntity.insertModels(modelsWithValues(10)), is(10));
		final List<TestModel> models = mEntity.queryAllModels();
		assertThat(models, is(not(nullValue())));
		assertThat(models.size(), is(10));
	}

	@Test
	public void testQueryAllModelsForEmptyContent() {
		final List<TestModel> models = mEntity.queryAllModels();
		assertThat(models, is(not(nullValue())));
		assertThat(models.size(), is(0));
	}

	@Test
	public void testQueryModelsByIds() {
		TestModel model = modelWithValues("#1");
		model.id = 100L;
		mEntity.insertModel(model);
		model = modelWithValues("#2");
		model.id = 105L;
		mEntity.insertModel(model);

		final List<TestModel> models = mEntity.queryModels(new long[]{100L, 105L, 106L/*no such row in DB table*/});
		assertThat(model, is(not(nullValue())));
		assertThat(models.size(), is(2));
	}

	@Test
	public void testQueryModelsByIdsForEmptyContent() {
		final List<TestModel> models = mEntity.queryModels(new long[]{105L});
		assertThat(models, is(not(nullValue())));
		assertThat(models.size(), is(0));
	}

	@Test
	public void testQueryModels() {
		TestModel model = modelWithValues("#1");
		model.categoryId = 2L;
		mEntity.insertModel(model);
		model = modelWithValues("#2");
		model.categoryId = 4L;
		mEntity.insertModel(model);

		final List<TestModel> models = mEntity.queryModels(
				null,
				TestModelEntity.Columns.CATEGORY_ID + "=?",
				new String[]{Long.toString(4L)},
				null
		);
		assertThat(model, is(not(nullValue())));
		assertThat(models.size(), is(1));
	}

	@Test
	public void testQueryModelsForEmptyContent() {
		final List<TestModel> models = mEntity.queryModels(null, null, null, null);
		assertThat(models, is(not(nullValue())));
		assertThat(models.size(), is(0));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testQueryModelById() {
		TestModel model = modelWithValues();
		model.id = 105L;
		mEntity.insertModel(model);

		model = mEntity.queryModel(105L);
		assertThat(model, is(not(nullValue())));
		assertThat(model.id, is(105L));
	}

	@Test
	public void testQueryModelByIdNotPresentedWithinDb() {
		assertThat(mEntity.queryModel(167L), is(nullValue()));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testQueryModel() {
		TestModel model = modelWithValues("#1");
		model.published = true;
		model.title = "Published";
		mEntity.insertModel(model);
		model = modelWithValues("#2");
		model.published = false;
		model.title = "Unpublished";
		mEntity.insertModel(model);

		model = mEntity.queryModel(
				null,
				TestModelEntity.Columns.PUBLISHED + "=?",
				new String[]{"1"},
				null
		);
		assertThat(model, is(not(nullValue())));
		assertThat(model.published, is(true));
		assertThat(model.title, is("Published"));
	}

	@Test
	public void testQueryModelNotPresentedWithinDb() {
		assertThat(
				mEntity.queryModel(
						null,
						TestModelEntity.Columns.CATEGORY_ID + "=?",
						new String[]{Long.toString(13L)},
						null),
				is(nullValue())
		);
	}

	@Test
	public void testUpdateModels() {
		List<TestModel> models = modelsWithValues(2);
		models.get(0).id = 100L;
		models.get(0).title = "Model#1";
		models.get(1).id = 105L;
		models.get(1).title = "Model#2";
		mEntity.insertModels(models);

		models.get(0).title = "Model#1.1";
		models.get(1).title = "Model#2.1";
		assertThat(mEntity.updateModels(models), is(2));
		assertContentOfEntityInSize(2);

		models = mEntity.queryAllModels();
		assertThat(models.get(0).title, is("Model#1.1"));
		assertThat(models.get(1).title, is("Model#2.1"));
	}

	@Test
	public void testUpdateModelsNotPresentedWithinDb() {
		final List<TestModel> models = modelsWithValues(1);
		models.get(0).id = 105L;
		assertThat(mEntity.updateModels(models), is(0));
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testUpdateEmptyModels() {
		assertThat(mEntity.updateModels(new ArrayList<TestModel>(0)), is(0));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testUpdateModel() {
		final List<TestModel> models = modelsWithValues(2);
		models.get(0).id = 100L;
		models.get(0).title = "Model#1";
		models.get(1).id = 105L;
		models.get(1).title = "Model#2";
		mEntity.insertModels(models);

		TestModel model = new TestModel();
		model.id = 105L;
		model.title = "Model#2.1";
		assertThat(mEntity.updateModel(model), is(1));
		assertContentOfEntityInSize(2);

		model = mEntity.queryModel(105L);
		assertThat(model.title, is("Model#2.1"));
	}

	@Test
	public void testUpdateModelNotPresentedWithinDb() {
		final TestModel model = modelWithValues();
		model.id = 1056L;
		assertThat(mEntity.updateModel(model), is(0));
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testDeleteModels() {
		List<TestModel> models = modelsWithValues(2);
		models.get(0).id = 100L;
		models.get(0).title = "Model#1";
		models.get(1).id = 105L;
		models.get(1).title = "Model#2";
		mEntity.insertModels(models);

		assertThat(mEntity.deleteModels(models), is(2));
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testDeleteModelsNotPresentedWithinDb() {
		final List<TestModel> models = modelsWithValues(3);
		assertThat(mEntity.deleteModels(models), is(0));
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testDeleteEmptyModels() {
		assertThat(mEntity.deleteModels(new ArrayList<TestModel>(0)), is(0));
	}

	@Test
	public void testDeleteModel() {
		final List<TestModel> models = modelsWithValues(2);
		models.get(0).id = 100L;
		models.get(1).id = 105L;
		mEntity.insertModels(models);

		assertThat(mEntity.deleteModel(models.get(0)), is(1));
		assertContentOfEntityInSize(1);
		assertThat(mEntity.deleteModel(models.get(1)), is(1));
		assertContentOfEntityInSize(0);
	}

	@Test
	public void testDeleteModelNotPresentedWithinDb() {
		assertThat(mEntity.deleteModel(modelWithValues()), is(0));
	}

	@Test
	public void testOnCreateModel() {
		assertThat(mEntity.onCreateModel(), is(not(nullValue())));
	}

	private void assertContentOfEntityInSize(int size) {
		assertContentOfEntityInSize(size, null);
	}

	@SuppressWarnings("ConstantConditions")
	private void assertContentOfEntityInSize(int size, String selection, Object... selectionArgs) {
		String[] argsArray = null;
		if (selectionArgs != null && selectionArgs.length > 0) {
			argsArray = new String[selectionArgs.length];
			for (int i = 0; i < argsArray.length; i++) {
				argsArray[i] = String.valueOf(selectionArgs[i]);
			}
		}
		final Cursor cursor = mEntity.query(null, selection, argsArray, null);
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor.getCount(), is(size));
	}

	private static List<TestModel> modelsWithValues(int count) {
		final List<TestModel> models = new ArrayList<>(count);
		for (int i = 0; i < count; i++) {
			models.add(modelWithValues(Integer.toString(i + 1)));
		}
		return models;
	}

	private static TestModel modelWithValues() {
		return modelWithValues("@");
	}

	private static TestModel modelWithValues(String uniqueIdentifier) {
		final TestModel model = TestModel.withTestValues();
		model.id = null;
		model.tag = "tag#" + uniqueIdentifier;
		model.author = "author:" + uniqueIdentifier;
		return model;
	}

	private static final class TestModelEntityWithoutModelAnnotation extends ModelEntity {
	}

	@Model(TestModel3.class)
	@EntityName(TestModelEntity3.NAME)
	@SuppressWarnings("InnerClassTooDeeplyNested")
	private static final class TestModelEntity3 extends ModelEntity<TestModel3> {

		static final String NAME = "test_models_3";

		interface Columns {
			String ID = Column.Primary.COLUMN_NAME;
		}
	}

	@SuppressWarnings("InnerClassTooDeeplyNested")
	private static final class TestModel3 extends SimpleEntityModel<TestModel3> {

		public static final Parcelable.Creator<TestModel3> CREATOR = new Parcelable.Creator<TestModel3>() {

			@Override
			public TestModel3 createFromParcel(Parcel source) {
				return new TestModel3(source);
			}

			@Override
			public TestModel3[] newArray(int size) {
				return new TestModel3[size];
			}
		};

		@SuppressWarnings("unused")
		public static final Factory<TestModel3> FACTORY = new Factory<TestModel3>() {

			@NonNull
			@Override
			public TestModel3 createModel() {
				return new TestModel3();
			}
		};

		@Column(TestModelEntity3.Columns.ID)
		public Long id;

		public TestModel3() {
			super();
		}

		TestModel3(@NonNull Parcel source) {
			super(source);
		}
	}

	@SuppressLint("ParcelCreator")
	private static final class TestModelWithoutColumns extends SimpleEntityModel {
	}
}
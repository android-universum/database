/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.TestPrimaryProvider;
import universum.studios.android.database.inner.BaseTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class LoremIpsumEntityTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "LoremIpsumEntityTest";

	private LoremIpsumEntity mEntity;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		if (mEntity == null) mEntity = TestPrimaryProvider.accessDatabase().findEntityByClass(LoremIpsumEntity.class);
	}

	@Test
	public void testInstantiation() {
		final LoremIpsumEntity entity = new LoremIpsumEntity(50);
		assertThat(entity.getInitialContentSize(), is(50));
		assertThat(entity.createsInitialContent(), is(true));
	}

	@Test
	public void testInstantiationWithZeroInitialContentSize() {
		final LoremIpsumEntity entity = new LoremIpsumEntity(0);
		assertThat(entity.getInitialContentSize(), is(0));
		assertThat(entity.createsInitialContent(), is(false));
	}

	@Test
	public void testDefaultGetInitialContent() {
		assertThat(mEntity.getInitialContentSize(), is(100));
	}

	@Test
	public void testCreatesInitialContent() {
		assertThat(mEntity.createsInitialContent(), is(true));
	}
}
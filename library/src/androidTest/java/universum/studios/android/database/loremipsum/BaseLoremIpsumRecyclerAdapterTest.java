/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import android.database.Cursor;

import org.junit.Rule;
import org.junit.Test;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.rule.ActivityTestRule;
import universum.studios.android.database.mock.MockCursor;
import universum.studios.android.test.instrumented.InstrumentedTestCase;
import universum.studios.android.test.instrumented.TestCompatActivity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class BaseLoremIpsumRecyclerAdapterTest extends InstrumentedTestCase {

	@Rule public final ActivityTestRule<TestCompatActivity> ACTIVITY_RULE = new ActivityTestRule<>(TestCompatActivity.class);

	private TestAdapter mAdapter;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mAdapter = new TestAdapter(ACTIVITY_RULE.getActivity());
	}

	@Test
	public void testCreateLoader() {
		ACTIVITY_RULE.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				assertThat(mAdapter.createLoader(0, null), is(not(nullValue())));
			}
		});
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	public void testWrapCursor() {
		final Cursor cursor = mAdapter.wrapCursor(new MockCursor(0));
		assertThat(cursor, is(not(nullValue())));
		assertThat(cursor instanceof LoremIpsumCursor, is(true));
	}

	private static final class TestAdapter extends BaseLoremIpsumRecyclerAdapter<RecyclerView.ViewHolder> {

		private TestAdapter(@NonNull FragmentActivity context) {
			super(context);
		}

		@Override
		protected void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @NonNull LoremIpsumCursor cursor, int position) {
		}
	}
}
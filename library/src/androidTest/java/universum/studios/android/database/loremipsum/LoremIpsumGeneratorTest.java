/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import junit.framework.TestCase;

import static universum.studios.android.database.loremipsum.LoremIpsumTest.assertTextEndsWithDot;
import static universum.studios.android.database.loremipsum.LoremIpsumTest.assertTextNotEmpty;
import static universum.studios.android.database.loremipsum.LoremIpsumTest.assertTextWithFirstLetterUpperCased;
import static universum.studios.android.database.loremipsum.LoremIpsumTest.assertTextWithWordsInCount;
import static universum.studios.android.database.loremipsum.LoremIpsumTest.assertWordWithLettersInCount;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("ResourceType")
public final class LoremIpsumGeneratorTest extends TestCase {

	@SuppressWarnings("unused")
	private static final String TAG = "LoremIpsumGeneratorTest";

	public void testRandomWordShort() {
		testRandomWord(LoremIpsumGenerator.SIZE_SHORT, 1, 3);
	}

	public void testRandomWordMedium() {
		testRandomWord(LoremIpsumGenerator.SIZE_MEDIUM, 4, 6);
	}

	public void testRandomWordLong() {
		testRandomWord(LoremIpsumGenerator.SIZE_LONG, 7, 9);
	}

	public void testRandomWordVeryLong() {
		testRandomWord(LoremIpsumGenerator.SIZE_VERY_LONG, 10, Integer.MAX_VALUE);
	}

	private static void testRandomWord(
			@LoremIpsumGenerator.Size int size,
			int minExpectedLetters,
			int maxExpectedLetters
	) {
		final String word = LoremIpsumGenerator.randomWord(size);
		assertValidWord(word);
		assertWordWithLettersInCount(word, minExpectedLetters, maxExpectedLetters);
	}

	public void testRandomWordOfUnsupportedSize() {
		assertEquals("", LoremIpsumGenerator.randomWord(10));
	}

	public void testRandomSentenceShort() {
		testRandomSentence(LoremIpsumGenerator.SIZE_SHORT, 3, 5);
	}

	public void testRandomSentenceMedium() {
		// For the medium sized sentence there can be presented also connector (a word or ", ").
		testRandomSentence(LoremIpsumGenerator.SIZE_MEDIUM, 6, 11);
	}

	public void testRandomSentenceLong() {
		// For the long sized sentence there can be presented also multiple connectors.
		testRandomSentence(LoremIpsumGenerator.SIZE_LONG, 12, 23);
	}

	public void testRandomSentenceVeryLong() {
		// For the very-long sized sentence there can be presented also multiple connectors.
		testRandomSentence(LoremIpsumGenerator.SIZE_VERY_LONG, 22, 47);
	}

	private static void testRandomSentence(
			@LoremIpsumGenerator.Size int size,
	        int minExpectedWords,
	        int maxExpectedWords
	) {
		final String sentence = LoremIpsumGenerator.randomSentence(size);
		assertValidSentence(sentence);
		assertTextWithWordsInCount(sentence, minExpectedWords, maxExpectedWords);
	}

	public void testRandomSentenceOfUnsupportedSize() {
		assertEquals("", LoremIpsumGenerator.randomSentence(10));
	}

	public void testFirstToUpperCase() {
		assertEquals("Rome is a town in Italy.", LoremIpsumGenerator.firstToUpperCase("rome is a town in Italy."));
	}

	public void testFirstToUpperCaseForEmptyText() {
		assertEquals("", LoremIpsumGenerator.firstToUpperCase(""));
	}

	public void testRandomFragment() {
		final String fragment = LoremIpsumGenerator.randomFragment();
		assertTextNotEmpty(fragment);
		assertTextWithWordsInCount(fragment, 3, 5);
	}

	private static void assertValidWord(String word) {
		assertTextNotEmpty(word);
	}

	private static void assertValidSentence(String sentence) {
		assertTextNotEmpty(sentence);
		assertTextWithFirstLetterUpperCased(sentence);
		assertTextEndsWithDot(sentence);
	}
}
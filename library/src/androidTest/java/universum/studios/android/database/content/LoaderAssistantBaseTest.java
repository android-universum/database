/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.os.Bundle;

import org.junit.Rule;

import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.test.rule.ActivityTestRule;
import universum.studios.android.test.instrumented.InstrumentedTestCase;
import universum.studios.android.test.instrumented.TestCompatActivity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public abstract class LoaderAssistantBaseTest<D, A extends TestLoaderAssistant<D>> extends InstrumentedTestCase {

	@Rule public final ActivityTestRule<TestCompatActivity> ACTIVITY_RULE = new ActivityTestRule<>(TestCompatActivity.class);

	protected A assistant;
	protected TestCompatActivity activity;
	private LoaderManager loaderManager;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.activity = ACTIVITY_RULE.getActivity();
		this.assistant = onCreateAssistant(activity);
		this.loaderManager = activity.getSupportLoaderManager();
	}

	protected abstract A onCreateAssistant(FragmentActivity activity);

	protected void waitForLoaderToBeAvailable() {
		while (true) {
			if (loaderManager.getLoader(assistant.getLoaderId()) != null) {
				break;
			}
		}
	}

	protected void assertThatLoaderHasId(int loaderId) {
		assertThat(assistant.getLoaderId(), is(loaderId));
	}

	protected void assertThatLoaderHasIdAndParams(int loaderId, Bundle loaderParams) {
		assertThat(assistant.getLoaderId(), is(loaderId));
		assertThat(assistant.getLoaderParams(), is(loaderParams));
	}

	@SuppressWarnings("ConstantConditions")
	protected void assertThatLoaderIsStarted(int loaderId) {
		final Loader<D> loader = assistant.getLoader(loaderId);
		assertThat(loader, is(not(nullValue())));
		// fixme: loaders in JUnit4Tests are never started, why ???
		// fixme: assertThat(loader.isStarted(), is(true));
	}

	@SuppressWarnings("unused")
	protected void assertThatLoaderDoesNotExist(int loaderId) {
		assertThat(assistant.getLoader(loaderId), is(nullValue()));
	}

	protected void assertSomeLoaderIsRunning() {
		// todo: assertTrue(mActivity.getLoaderManager().hasRunningLoaders());
	}

	protected void assertThatNoLoaderIsRunning() {
		// todo: assertFalse(mActivity.getLoaderManager().hasRunningLoaders());
	}

	protected void destroyLoader(int loaderId) {
		assistant.destroyLoader(loaderId);
	}
}
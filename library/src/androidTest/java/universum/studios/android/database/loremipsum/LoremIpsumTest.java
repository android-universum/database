/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.inner.BaseTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class LoremIpsumTest extends BaseTest {

	@SuppressWarnings("unused")
	private static final String TAG = "LoremIpsumTest";

	private LoremIpsum mEmptyLoremIpsum;
	private LoremIpsum mLoremIpsum;
	private long mCurrentTime;

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mEmptyLoremIpsum = new LoremIpsum();
		this.mLoremIpsum = LoremIpsum.generate();
		this.mCurrentTime = System.currentTimeMillis();
	}

	@Test
	public void testFactoryCreateModel() {
		assertThat(LoremIpsum.FACTORY.createModel(), is(not(nullValue())));
	}

	@Test
	public void testGenerate() {
		final LoremIpsum loremIpsum = LoremIpsum.generate();
		assertThat(loremIpsum, is(not(nullValue())));
		assertThat(loremIpsum.getId(), is(-1L));
	}

	@Test
	public void testGenerateWithId() {
		final LoremIpsum loremIpsum = LoremIpsum.generate(123456L);
		assertThat(loremIpsum, is(not(nullValue())));
		assertThat(loremIpsum.getId(), is(123456L));
	}

	@Test
	public void testDefaultGetId() {
		assertThat(mEmptyLoremIpsum.getId(), is(-1L));
	}

	@Test
	public void testGenerateTitle() {
		final String title = LoremIpsum.generateTitle();
		assertTextWithWordsInCount(title, 2, 5);
	}

	@Test
	public void testDefaultGetTitle() {
		assertThat(mEmptyLoremIpsum.getTitle(), is(""));
	}

	@Test
	public void testGetTitle() {
		assertTextWithWordsInCount(mLoremIpsum.getTitle(), 2, 5);
	}

	@Test
	public void testGenerateName() {
		final String name = LoremIpsum.generateName();
		assertTextWithWordsInCount(name, 1, 2);
	}

	@Test
	public void testDefaultGetName() {
		assertThat(mEmptyLoremIpsum.getName(), is(""));
	}

	@Test
	public void testGetName() {
		assertTextWithWordsInCount(mLoremIpsum.getName(), 1, 2);
	}

	@Test
	public void testGenerateNote() {
		final String note = LoremIpsum.generateNote();
		assertTextWithWordsInCount(note, 5, 15);
	}

	@Test
	public void testDefaultGetNote() {
		assertThat(mEmptyLoremIpsum.getNote(), is(""));
	}

	@Test
	public void testGetNote() {
		assertTextWithWordsInCount(mLoremIpsum.getNote(), 5, 15);
	}

	@Test
	public void testGenerateDescription() {
		final String description = LoremIpsum.generateDescription();
		assertTextWithSentencesInCount(description, 1, 2);
	}

	@Test
	public void testDefaultGetDescription() {
		assertThat(mEmptyLoremIpsum.getDescription(), is(""));
	}

	@Test
	public void testGetDescription() {
		assertTextWithSentencesInCount(mLoremIpsum.getDescription(), 1, 2);
	}

	@Test
	public void testGenerateContent() {
		final String content = LoremIpsum.generateContent();
		assertTextWithSentencesInCount(content, 5, 10);
	}

	@Test
	public void testDefaultGetContent() {
		assertThat(mEmptyLoremIpsum.getContent(), is(""));
	}

	@Test
	public void testGetContent() {
		assertTextWithSentencesInCount(mLoremIpsum.getContent(), 5, 10);
	}

	static void assertTextNotEmpty(String text) {
		assertThat(text, is(not(nullValue())));
		assertThat(text.length() > 0, is(true));
	}

	static void assertTextWithFirstLetterUpperCased(String text) {
		assertThat(Character.isUpperCase(text.charAt(0)), is(true));
	}

	static void assertTextEndsWithDot(String text) {
		assertThat(text.endsWith("."), is(true));
	}

	private static void assertTextWithSentencesInCount(String text, int minExpectedCount, int maxExpectedCount) {
		final String[] sentences = text.split("\n\n");
		if (minExpectedCount > 0) {
			assertThat(sentences, is(not(nullValue())));
			if (sentences.length < minExpectedCount || sentences.length > maxExpectedCount) {
				throw new AssertionError(
						"Expected sentences count from the range [" + minExpectedCount + ", " + maxExpectedCount + "]" +
								" but was(" + sentences.length + ") for the text(" + text + ")."
				);
			}
		}
	}

	static void assertTextWithWordsInCount(String text, int minExpectedCount, int maxExpectedCount) {
		final String[] words = text.split(" ");
		if (minExpectedCount > 0) {
			assertThat(words, is(not(nullValue())));
			if (words.length < minExpectedCount || words.length > maxExpectedCount) {
				throw new AssertionError(
						"Expected words count from the range [" + minExpectedCount + ", " + maxExpectedCount + "]" +
								" but was(" + words.length + ") for the text(" + text + ")."
				);
			}
		}
	}

	static void assertWordWithLettersInCount(String word, int minExpectedCount, int maxExpectedCount) {
		final int wordLength = word.length();
		if (wordLength < minExpectedCount || wordLength > maxExpectedCount) {
			throw new AssertionError(
					"Expected letters count from the range [" + minExpectedCount + ", " + maxExpectedCount + "]" +
							" but was(" + wordLength + ")."
			);
		}
	}

	@Test
	public void testGenerateSize() {
		final int size = LoremIpsum.generateSize();
		assertThat(size >= 0 && size < Integer.MAX_VALUE, is(true));
	}

	@Test
	public void testDefaultGetSize() {
		assertThat(mEmptyLoremIpsum.getSize(), is(0));
	}

	@Test
	public void testGetSize() {
		final int size = mLoremIpsum.getSize();
		assertThat(size >= 0 && size < Integer.MAX_VALUE, is(true));
	}

	@Test
	public void testGenerateDate() {
		final long currentTime = System.currentTimeMillis();
		final long begin = currentTime - 1000 * 10;
		final long end = currentTime;
		final long date = LoremIpsum.generateDate(begin, end);
		assertThat(date >= begin && date <= end, is(true));
	}

	@Test
	public void testDefaultGetDateCreated() {
		assertThat(mEmptyLoremIpsum.getDateCreated(), is(0L));
	}

	@Test
	public void testGedDateCreated() {
		final long dateCreated = mLoremIpsum.getDateCreated();
		assertThat(dateCreated >= 0 && dateCreated <= mCurrentTime, is(true));
	}

	@Test
	public void testDefaultGetDateModified() {
		assertThat(mEmptyLoremIpsum.getDateModified(), is(0L));
	}

	@Test
	public void testGetDateModified() {
		final long dateCreated = mLoremIpsum.getDateCreated();
		final long dateModified = mLoremIpsum.getDateModified();
		assertThat(dateModified >= dateCreated && dateModified <= mCurrentTime, is(true));
	}

	@Test
	public void testDefaultIsEmpty() {
		assertThat(mEmptyLoremIpsum.isEmpty(), is(false));
	}

	@Test
	public void testIsEmpty() {
		assertThat(mLoremIpsum.isEmpty(), is(not(nullValue())));
	}

	@Test
	public void testDefaultIsEnabled() {
		assertThat(mEmptyLoremIpsum.isEnabled(), is(false));
	}

	@Test
	public void testIsEnabled() {
		assertThat(mLoremIpsum.isEnabled(), is(not(nullValue())));
	}

	@Test
	public void testDefaultIsEditable() {
		assertThat(mEmptyLoremIpsum.isEditable(), is(false));
	}

	@Test
	public void testIsEditable() {
		assertThat(mLoremIpsum.isEditable(), is(not(nullValue())));
	}
}
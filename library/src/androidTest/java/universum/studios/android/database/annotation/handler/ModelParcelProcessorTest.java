/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.os.Parcel;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.model.TestModel;
import universum.studios.android.database.model.TestModelWithUnsupportedColumns;
import universum.studios.android.database.util.TypeUtils;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
@SuppressWarnings("ResourceType")
public final class ModelParcelProcessorTest extends ModelProcessorBaseTest<ModelParcelProcessor> {

	@SuppressWarnings("unused")
	private static final String TAG = "ModelParcelProcessorTest";

	private static final int UNSUPPORTED_TYPE_FLAG = TypeUtils.UNKNOWN;

	private Parcel mParcel;
	private TestModel mEmptyModel;
	private TestModelWithUnsupportedColumns mEmptyModelWithUnsupportedColumns;

	public ModelParcelProcessorTest() {
		super(new ModelParcelProcessor());
	}

	@Override
	public void beforeTest() throws Exception {
		super.beforeTest();
		this.mParcel = Parcel.obtain();
		this.mModel = TestModel.withTestValues();
		this.mEmptyModel = new TestModel();
		this.mModelWithUnsupportedColumns = TestModelWithUnsupportedColumns.witTestValues();
		this.mEmptyModelWithUnsupportedColumns = new TestModelWithUnsupportedColumns();
	}

	@Override
	public void afterTest() throws Exception {
		super.afterTest();
		mParcel.recycle();
	}

	@Test
	public void testProcessModelIntoParcel() {
		mProcessor.processModelIntoParcel(mModel, mColumns, mParcel, 0);
		mParcel.setDataPosition(0);
		/**
		 * Note, that each model processor processes the model's field in alphabetical order, so
		 * also values has been written into parcel in the alphabetical order.
		 */
		// TestModel.author
		assertThat(mParcel.readInt(), is(TypeUtils.STRING));
		assertThat(mParcel.readString(), is(TestModel.TestValues.AUTHOR));

		// TestModel.categoryId
		assertThat(mParcel.readInt(), is(TypeUtils.NULL));

		// TestModel.categoryName
		assertThat(mParcel.readInt(), is(TypeUtils.STRING));
		assertThat(mParcel.readString(), is(TestModel.JoinedTestValues.CATEGORY_NAME));

		// TestModel.code
		assertThat(mParcel.readInt(), is(TypeUtils.SHORT));
		assertThat(mParcel.readInt(), is(TestModel.TestValues.CODE.intValue()));

		// TestModel.count
		assertThat(mParcel.readInt(), is(TypeUtils.NULL));

		// TestModel.description
		assertThat(mParcel.readInt(), is(TypeUtils.STRING));
		assertThat(mParcel.readString(), is(TestModel.TestValues.DESCRIPTION));

		// TestModel.difficulty
		assertThat(mParcel.readInt(), is(TypeUtils.ENUM));
		assertThat(mParcel.readString(), is(TestModel.TestValues.DIFFICULTY.name()));

		// TestModel.enabled
		assertThat(mParcel.readInt(), is(TypeUtils.BOOLEAN));
		assertThat(mParcel.readInt() == 1, is(TestModel.TestValues.ENABLED));

		// TestModel.flags
		assertThat(mParcel.readInt(), is(TypeUtils.INTEGER));
		assertThat(mParcel.readInt(), is(TestModel.TestValues.FLAGS));

		// TestModel.id
		assertThat(mParcel.readInt(), is(TypeUtils.LONG));
		assertThat(mParcel.readLong(), is(TestModel.TestValues.ID));

		// TestModel.latitude
		assertThat(mParcel.readInt(), is(TypeUtils.DOUBLE));
		assertThat(mParcel.readDouble(), is(TestModel.TestValues.LATITUDE));

		// TestModel.longitude
		assertThat(mParcel.readInt(), is(TypeUtils.DOUBLE));
		assertThat(mParcel.readDouble(), is(TestModel.TestValues.LONGITUDE));

		// TestModel.orientation
		assertThat(mParcel.readInt(), is(TypeUtils.BYTE));
		assertThat(mParcel.readByte(), is(TestModel.TestValues.ORIENTATION));

		// TestModel.published
		assertThat(mParcel.readInt(), is(TypeUtils.BOOLEAN));
		assertThat(mParcel.readInt() == 0, is(TestModel.TestValues.ENABLED));

		// TestModel.ratio
		assertThat(mParcel.readInt(), is(TypeUtils.FLOAT));
		assertThat(mParcel.readFloat(), is(TestModel.TestValues.RATIO));

		// TestModel.tag
		assertThat(mParcel.readInt(), is(TypeUtils.STRING));
		assertThat(mParcel.readString(), is(TestModel.TestValues.TAG));

		// TestModel.title
		assertThat(mParcel.readInt(), is(TypeUtils.NULL));
	}

	@Test
	public void testProcessModelIntoParcelWithUnsupportedColumns() {
		final Parcel parcel = Parcel.obtain();
		assertThatProcessingThrowsExceptionWithMessage(
				new Runnable() {

					@Override
					public void run() {
						mProcessor.processModelIntoParcel(
								mModelWithUnsupportedColumns,
								mUnsupportedColumns,
								parcel, 0
						);
					}
				},
				"Failed to write value of column field into Parcel. " +
						"Field(TestModelWithUnsupportedColumns.date) type of(Date) is not allowed to represent a column. " +
						"Only primitive type fields, Enums including, are allowed as columns."
		);
		parcel.recycle();
	}

	@Test
	public void testProcessModelFromParcel() {
		// We first need to have some data written into parcel.
		mProcessor.processModelIntoParcel(mModel, mColumns, mParcel, 0);
		mParcel.setDataPosition(0);
		mProcessor.processModelFromParcel(mEmptyModel, mColumns, mParcel);

		/**
		 * Unlike in case of testing of processing into parcel, we do not need in case of this test
		 * to check values in alphabetical order.
		 */
		assertThat(mEmptyModel.id, is(TestModel.TestValues.ID));
		assertThat(mEmptyModel.tag, is(TestModel.TestValues.TAG));
		assertThat(mEmptyModel.author, is(TestModel.TestValues.AUTHOR));
		assertThat(mEmptyModel.title, is(TestModel.TestValues.TITLE));
		assertThat(mEmptyModel.description, is(TestModel.TestValues.DESCRIPTION));
		assertThat(mEmptyModel.count, is(TestModel.TestValues.COUNT));
		assertThat(mEmptyModel.latitude, is(TestModel.TestValues.LATITUDE));
		assertThat(mEmptyModel.longitude, is(TestModel.TestValues.LONGITUDE));
		assertThat(mEmptyModel.categoryId, is(TestModel.TestValues.CATEGORY_ID));
		assertThat(mEmptyModel.categoryName, is(TestModel.JoinedTestValues.CATEGORY_NAME));
		assertThat(mEmptyModel.enabled, is(TestModel.TestValues.ENABLED));
		assertThat(mEmptyModel.published, is(TestModel.TestValues.PUBLISHED));
		assertThat(mEmptyModel.flags, is(TestModel.TestValues.FLAGS));
		assertThat(mEmptyModel.ratio, is(TestModel.TestValues.RATIO));
		assertThat(mEmptyModel.difficulty, is(TestModel.TestValues.DIFFICULTY));
		assertThat(mEmptyModel.orientation, is(TestModel.TestValues.ORIENTATION));
		assertThat(mEmptyModel.code, is(TestModel.TestValues.CODE));
	}

	@Test
	public void testProcessModelFromParcelWithUnsupportedColumns() {
		final Parcel parcel = Parcel.obtain();
		parcel.writeInt(UNSUPPORTED_TYPE_FLAG);
		parcel.setDataPosition(0);
		assertThatProcessingThrowsExceptionWithMessage(
				new Runnable() {

					@Override
					public void run() {
						mProcessor.processModelFromParcel(
								mModelWithUnsupportedColumns,
								mUnsupportedColumns,
								parcel
						);
					}
				},
				"Failed to read value of column field from Parcel. " +
						"Field(TestModelWithUnsupportedColumns.date) type of(Date) is not allowed to represent a column. " +
						"Only primitive type fields, Enums including, are allowed as columns."
		);
		parcel.recycle();
	}

	@Test
	public void testOnProcessColumnIntoParcelOfUnsupportedType() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getField("date"));
		final ModelParcelProcessor.ParcelTask task = ModelParcelProcessor.ParcelTask.obtainFor(mModelWithUnsupportedColumns, mUnsupportedColumns, mParcel, 0);
		try {
			mProcessor.onProcessColumnIntoParcel(task, annotatedColumn);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Failed to write value of column field into Parcel. " +
							"Field(TestModelWithUnsupportedColumns.date) type of(Date) " +
							"is not allowed to represent a column. " +
							"Only primitive type fields, Enums including, are allowed as columns.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testWriteValueOfColumnAsByte() throws Exception {
		this.testWriteValueOfColumnInner("orientation", TypeUtils.BYTE);
		assertThat(mParcel.readByte(), is(TestModel.TestValues.ORIENTATION));
	}

	@Test
	public void testWriteValueOfColumnAsShort() throws Exception {
		this.testWriteValueOfColumnInner("code", TypeUtils.SHORT);
		assertThat((short) mParcel.readInt(), is(TestModel.TestValues.CODE));
	}

	@Test
	public void testWriteValueOfColumnAsInteger() throws Exception {
		this.testWriteValueOfColumnInner("flags", TypeUtils.INTEGER);
		assertThat(mParcel.readInt(), is(TestModel.TestValues.FLAGS));
	}

	@Test
	public void testWriteValueOfColumnAsLong() throws Exception {
		this.testWriteValueOfColumnInner("id", TypeUtils.LONG);
		assertThat(mParcel.readLong(), is(TestModel.TestValues.ID));
	}

	@Test
	public void testWriteValueOfColumnAsFloat() throws Exception {
		this.testWriteValueOfColumnInner("ratio", TypeUtils.FLOAT);
		assertThat(mParcel.readFloat(), is(TestModel.TestValues.RATIO));
	}

	@Test
	public void testWriteValueOfColumnAsDouble() throws Exception {
		this.testWriteValueOfColumnInner("latitude", TypeUtils.DOUBLE);
		assertThat(mParcel.readDouble(), is(TestModel.TestValues.LATITUDE));
	}

	@Test
	public void testWriteValueOfColumnAsString() throws Exception {
		this.testWriteValueOfColumnInner("author", TypeUtils.STRING);
		assertThat(mParcel.readString(), is(TestModel.TestValues.AUTHOR));
	}

	@Test
	public void testWriteValueOfColumnAsEnum() throws Exception {
		this.testWriteValueOfColumnInner("difficulty", TypeUtils.ENUM);
		assertThat(mParcel.readString(), is(TestModel.TestValues.DIFFICULTY.name()));
	}

	@Test
	public void testWriteValueOfColumnAsNull() throws Exception {
		this.testWriteValueOfColumnInner("categoryId", TypeUtils.NULL);
		// In case of NULL value there is written only type flag into parcel.
	}

	private void testWriteValueOfColumnInner(String columnFieldName, int expectedType) throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField(columnFieldName));
		assertThat(mProcessor.writeValueOfColumn(annotatedColumn, mModel, mParcel, 0), is(true));
		mParcel.setDataPosition(0);
		assertThat(mParcel.readInt(), is(expectedType));
	}

	@Test
	public void testWriteValueOfColumnWithNotAccessibleField() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getDeclaredField("inaccessibleString"), false);
		try {
			mProcessor.writeValueOfColumn(annotatedColumn, mModelWithUnsupportedColumns, mParcel, 0);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_DEFAULT));
			assertThat(e.getMessage(), is("Field(TestModelWithUnsupportedColumns.inaccessibleString) is not accessible."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testOnProcessColumnFromParcelOfUnsupportedType() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getField("date"));
		mParcel.writeInt(annotatedColumn.getType());
		mParcel.setDataPosition(0);
		final ModelParcelProcessor.ParcelTask task = ModelParcelProcessor.ParcelTask.obtainFor(mModelWithUnsupportedColumns, mUnsupportedColumns, mParcel, 0);
		try {
			mProcessor.onProcessColumnFromParcel(task, annotatedColumn);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_MISCONFIGURATION));
			assertThat(
					e.getMessage(),
					is("Failed to read value of column field from Parcel. " +
							"Field(TestModelWithUnsupportedColumns.date) type of(Date) " +
							"is not allowed to represent a column. " +
							"Only primitive type fields, Enums including, are allowed as columns.")
			);
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testReadValueIntoColumnAsByte() throws Exception {
		mParcel.writeInt(TypeUtils.BYTE);
		mParcel.writeByte(TestModel.TestValues.ORIENTATION);
		this.testReadValueIntoColumnInner("orientation", TestModel.TestValues.ORIENTATION);
	}

	@Test
	public void testReadValueIntoColumnAsShort() throws Exception {
		mParcel.writeInt(TypeUtils.SHORT);
		mParcel.writeInt(TestModel.TestValues.CODE);
		this.testReadValueIntoColumnInner("code", TestModel.TestValues.CODE);
	}

	@Test
	public void testReadValueIntoColumnAsInteger() throws Exception {
		mParcel.writeInt(TypeUtils.INTEGER);
		mParcel.writeInt(TestModel.TestValues.FLAGS);
		this.testReadValueIntoColumnInner("flags", TestModel.TestValues.FLAGS);
	}

	@Test
	public void testReadValueIntoColumnAsLong() throws Exception {
		mParcel.writeInt(TypeUtils.LONG);
		mParcel.writeLong(TestModel.TestValues.ID);
		this.testReadValueIntoColumnInner("id", TestModel.TestValues.ID);
	}

	@Test
	public void testReadValueIntoColumnAsFloat() throws Exception {
		mParcel.writeInt(TypeUtils.FLOAT);
		mParcel.writeFloat(TestModel.TestValues.RATIO);
		this.testReadValueIntoColumnInner("ratio", TestModel.TestValues.RATIO);
	}

	@Test
	public void testReadValueIntoColumnAsDouble() throws Exception {
		mParcel.writeInt(TypeUtils.DOUBLE);
		mParcel.writeDouble(TestModel.TestValues.LONGITUDE);
		this.testReadValueIntoColumnInner("longitude", TestModel.TestValues.LONGITUDE);
	}

	@Test
	public void testReadValueIntoColumnAsBoolean() throws Exception {
		mParcel.writeInt(TypeUtils.BOOLEAN);
		mParcel.writeInt(TestModel.TestValues.PUBLISHED ? 1 : 0);
		this.testReadValueIntoColumnInner("published", TestModel.TestValues.PUBLISHED);
	}

	@Test
	public void testReadValueIntoColumnAsString() throws Exception {
		mParcel.writeInt(TypeUtils.STRING);
		mParcel.writeString(TestModel.TestValues.AUTHOR);
		this.testReadValueIntoColumnInner("author", TestModel.TestValues.AUTHOR);
	}

	@Test
	public void testReadValueIntoColumnAsEnum() throws Exception {
		mParcel.writeInt(TypeUtils.ENUM);
		mParcel.writeString(TestModel.TestValues.DIFFICULTY.name());
		this.testReadValueIntoColumnInner("difficulty", TestModel.TestValues.DIFFICULTY);
	}

	@Test
	public void testReadValueIntoColumnAsEnumWithEmptyNameValue() throws Exception {
		mParcel.writeInt(TypeUtils.ENUM);
		mParcel.writeString("");
		this.testReadValueIntoColumnInner("difficulty", null);
	}

	@Test
	public void testReadValueIntoColumnAsNull() throws Exception {
		mParcel.writeInt(TypeUtils.NULL);
		this.testReadValueIntoColumnInner("description", null);
	}

	private void testReadValueIntoColumnInner(String columnFieldName, Object expected) throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModel.class.getField(columnFieldName));
		mParcel.setDataPosition(0);
		assertThat(mProcessor.readValueIntoColumn(annotatedColumn, mEmptyModel, mParcel), is(true));
		assertThat(annotatedColumn.getField().get(mEmptyModel), is(expected));
	}

	@Test
	public void testReadValueIntoColumnOfUnsupportedType() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getField("date"));
		mParcel.writeInt(annotatedColumn.getType());
		mParcel.setDataPosition(0);
		assertThat(mProcessor.readValueIntoColumn(annotatedColumn, mEmptyModelWithUnsupportedColumns, mParcel), is(false));
		assertThat(mEmptyModelWithUnsupportedColumns.date, is(nullValue()));
	}

	@Test
	public void testReadValueToColumnWithNotAccessibleField() throws Exception {
		final AnnotatedColumn annotatedColumn = new TestAnnotatedColumn(TestModelWithUnsupportedColumns.class.getDeclaredField("inaccessibleString"), false);
		try {
			mProcessor.readValueIntoColumn(annotatedColumn, mModelWithUnsupportedColumns, mParcel);
		} catch (DatabaseException e) {
			assertThat(e.getType(), is(DatabaseException.TYPE_DEFAULT));
			assertThat(e.getMessage(), is("Field(TestModelWithUnsupportedColumns.inaccessibleString) is not accessible."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test
	public void testValuesTaskObtain() {
		final ModelParcelProcessor.ParcelTask task = ModelParcelProcessor.ParcelTask.obtain();
		assertThat(task, is(not(nullValue())));
		assertThat(task.model, is(nullValue()));
		assertThat(task.columns, is(nullValue()));
		assertThat(task.parcel, is(nullValue()));
		assertThat(task.flags, is(0));
	}

	@Test
	public void testValuesTaskRecycle() {
		final int poolSize = ModelParcelProcessor.ParcelTask.POOL_SIZE;
		final int testPoolRange = poolSize * 2;
		final ModelParcelProcessor.ParcelTask[] tasks = new ModelParcelProcessor.ParcelTask[testPoolRange];
		for (int i = 0; i < testPoolRange; i++) {
			tasks[i] = ModelParcelProcessor.ParcelTask.obtain();
		}

		// Recycle all to populate the POOL.
		for (int i = 0; i < testPoolRange; i++) {
			tasks[i].recycle();
		}

		// When obtaining in size of the POOL all returned tasks are the same.
		for (int i = 0; i < poolSize; i++) {
			assertThat(ModelParcelProcessor.ParcelTask.obtain(), is(tasks[i]));
		}

		for (int i = 0; i < testPoolRange; i++) {
			tasks[i].recycle();
		}
	}
}
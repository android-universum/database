/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;
import universum.studios.android.database.annotation.LoaderId;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@RunWith(AndroidJUnit4.class)
public final class LoaderAdapterAnnotationHandlerTest extends AnnotationHandlerBaseTest<LoaderAdapterAnnotationHandler> {

	@SuppressWarnings("unused")
	private static final String TAG = "LoaderAdapterAnnotationHandlerTest";

	public LoaderAdapterAnnotationHandlerTest() {
		super(LoaderAdapterAnnotationHandlers.LoaderAdapterHandler.class);
	}

	@Override
	LoaderAdapterAnnotationHandler onObtainHandler() {
		return LoaderAdapterAnnotationHandlers.obtainLoaderAdapterHandler(TestAdapter.class);
	}

	@Override
	LoaderAdapterAnnotationHandler onObtainEmptyHandler() {
		return LoaderAdapterAnnotationHandlers.obtainLoaderAdapterHandler(TestAdapterWithoutAnnotation.class);
	}

	@Test
	public void testGetLoaderIdDefault() {
		assertThat(mEmptyHandler.getLoaderId(0), is(0));
	}

	@Test
	public void testGetLoaderId() {
		assertThat(mHandler.getLoaderId(0), is(TestAdapter.LOADER_ID));
	}

	@LoaderId(TestAdapter.LOADER_ID)
	private static final class TestAdapter {
		// Because adapter related annotation handlers do not require a specific base class of adapter,
		// we can create handlers for any type of adapter/class. This is intentional to allow reusing
		// of these handlers also for adapters that are not derived from Database library's base adapters.
		static final int LOADER_ID = 13;
	}

	private static final class TestAdapterWithoutAnnotation {
	}
}
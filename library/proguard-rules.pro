# Keep all database annotations.
-keep public @interface universum.studios.android.database.annotation.** { *; }
-keep @interface universum.studios.android.database.**$** { *; }
# Keep entities implementation details:
# - public empty constructor [always]
-keepclassmembers class * implements universum.studios.android.database.DatabaseEntity {
    public <init>();
}
# Keep entity models implementation details:
# - parcelable CREATOR [always],
# - model FACTORY [avoids instantiation via reflection],
# - public empty constructor [always],
# - columns [always],
-keepclassmembers class * implements universum.studios.android.database.model.EntityModel {
    public static final android.os.Parcelable$Creator CREATOR;
    public static final universum.studios.android.database.model.EntityModel$*Factory FACTORY;
    public <init>();
    @universum.studios.android.database.annotation.Column <fields>;
}
# Keep cursor wrappers implementation details:
# - public constructor taking Cursor parameter [if using wrappers within LoaderAdapter]
-keepclassmembers class * extends universum.studios.android.database.cursor.BaseCursorWrapper {
    public <init>(android.database.Cursor);
}
# Keep annotation handlers implementation details:
# - constructor taking Class parameter [always]
-keepclassmembers class * extends universum.studios.android.database.annotation.handler.BaseAnnotationHandler {
    public <init>(java.lang.Class);
}
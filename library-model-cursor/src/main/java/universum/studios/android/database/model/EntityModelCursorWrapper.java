/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.database.Cursor;

import androidx.annotation.CallSuper;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.annotation.handler.EntityModelAnnotationHandlers;
import universum.studios.android.database.annotation.handler.EntityModelCursorAnnotationHandler;
import universum.studios.android.database.cursor.BaseCursorWrapper;
import universum.studios.android.database.cursor.ItemCursor;

/**
 * A {@link BaseCursorWrapper} implementation that can be used in cooperation with {@link EntityModel}
 * instance that is used by this type of cursor wrapper to present its current data set via its specific
 * model instance.
 * <p>
 * This cursor wrapper basically creates a new instance of its model during its initialization so there
 * is only one model presented during a lifecycle of the wrapper and that is used to present the
 * cursor's data for its current moved position.
 * <p>
 * The cursor's model is populated by data for the current moved position whenever {@link #onBindData()}
 * is invoked so whenever the current cursor's position has been requested to be changed via one of
 * {@code move...(...)} methods.
 * <p>
 * Model may be accessed via {@link #accessModel()} or {@link #accessModelAt(int)} where each of these
 * methods returns a 'direct' reference to the cursor's model so its data are changed whenever the
 * cursor is moved to a new position. Accessing of the model via these methods may be useful primarily
 * when binding data within a {@link android.widget.CursorAdapter}. <b>You should never store this
 * way obtained model, because its data may change without your knowledge what may result in
 * unpredictable behaviour or inconsistency.</b>
 * <p>
 * If you need to access model for later usage use {@link #getModel()} or {@link #getModelAt(int)}
 * where each of these methods return a copy of the model created via {@link universum.studios.android.database.model.EntityModel#clone()}.
 * If the created copy will be shallow or deep really depends on the model's implementation.
 * {@link universum.studios.android.database.model.SimpleEntityModel SimpleEntityModel} for
 * example creates only shallow copy, if deep copy is needed, such copying logic need to be embedded
 * into the desired model.
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link universum.studios.android.database.annotation.Model @Model} <b>[class]</b>
 * <p>
 * If this annotation is presented, a class provided via this annotation will be used to instantiate
 * an instance of model specific for that type of ModelCursorWrapper that will be used to present
 * data of such ModelCursorWrapper for its current moved position.
 * </li>
 * </ul>
 *
 * @author Martin Albedinsky
 */
public class EntityModelCursorWrapper<M extends EntityModel> extends BaseCursorWrapper implements ItemCursor<M> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "EntityModelCursorWrapper";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Handler responsible for processing of all annotations of this class and also for handling all
	 * annotations related operations for this class.
	 */
	private final EntityModelCursorAnnotationHandler<M> mAnnotationHandler;

	/**
	 * Initial empty model that is used to create a new instance of model whenever this wrapper needs
	 * a new one, like when {@link #onClearData()} is invoked.
	 */
	private final M mModelTemplate;

	/**
	 * Model holding data bound from the wrapped cursor for the current position in {@link #onBindData()}.
	 */
	private M mModel;

	/**
	 * Boolean flag indicating whether the current model should be recycled when this cursor wrapper
	 * is being closed via {@link #close()}.
	 */
	private boolean mRecycleModelOnClose;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of EntityModelCursorWrapper.
	 * <p>
	 * <b>Note</b>, that this constructor is meant to be called only if required annotation
	 * {@link Model @Model} is presented above a subclass of this EntityModelCursorWrapper, otherwise
	 * exception will be thrown.
	 */
	@SuppressWarnings("CheckResult")
	public EntityModelCursorWrapper(@NonNull Cursor cursor) {
		super(cursor);
		if (!DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) {
			throw DatabaseException.annotationsNotEnabled();
		}
		this.mAnnotationHandler = onCreateAnnotationHandler();
		this.mModelTemplate = mAnnotationHandler.handleCreateModel();
		this.mModel = mAnnotationHandler.handleCreateModel();
		// If the wrapped cursor has been already moved to some position we need to bind data for
		// that position.
		final int position = mCursor.getPosition();
		if (position >= 0 && position < mCursor.getCount()) onBindData();
	}

	/**
	 * Creates a new instance of EntityModelCursorWrapper to wrap the given <var>cursor</var> of
	 * which data will be presented by the specified <var>model</var>.
	 *
	 * @param model The model used to present data of this cursor wrapper.
	 */
	@SuppressWarnings({"unchecked", "CheckResult"})
	public EntityModelCursorWrapper(@NonNull Cursor cursor, @NonNull M model) {
		super(cursor);
		this.mAnnotationHandler = onCreateAnnotationHandler();
		this.mModelTemplate = (M) model.clone();
		this.mModel = model;
		// If the wrapped cursor has been already moved to some position we need to bind data for
		// that position.
		final int position = mCursor.getPosition();
		if (position >= 0 && position < mCursor.getCount()) onBindData();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Invoked to create annotations handler for this instance.
	 *
	 * @return Annotations handler specific for this class.
	 */
	private EntityModelCursorAnnotationHandler<M> onCreateAnnotationHandler() {
		return EntityModelAnnotationHandlers.obtainModelCursorHandler(getClass());
	}

	/**
	 * Returns handler that is responsible for annotations processing of this class and also for
	 * handling all annotations related operations for this class.
	 *
	 * @return Annotations handler specific for this class.
	 * @throws DatabaseException If annotations processing is not enabled for the Database library.
	 */
	@NonNull
	protected EntityModelCursorAnnotationHandler<M> getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return mAnnotationHandler;
	}

	/**
	 * Sets a boolean flag indicating whether the current model of this cursor wrapper should be
	 * recycled via {@link #onRecycleModel()} when this cursor wrapper is being closed via {@link #close()}.
	 *
	 * @param recycleModel {@code True} to recycle model when this cursor wrapper is being closed,
	 *                     {@code false} otherwise.
	 */
	protected void setRecycleModelOnClose(boolean recycleModel) {
		this.mRecycleModelOnClose = recycleModel;
	}

	/**
	 * Ensures that the this cursor wrapper is initially moved to its first position if possible.
	 *
	 * @see #moveToFirst()
	 */
	private void ensureInitiallyMoved() {
		if (getPosition() == NO_POSITION) moveToFirst();
	}

	/**
	 * Same as {@link #getModel()}.
	 */
	@NonNull
	@Override
	public M getItem() {
		return getModel();
	}

	/**
	 * Returns copy of the associated model.
	 *
	 * @return Copy of the model with data for the current cursor position.
	 */
	@NonNull
	public M getModel() {
		this.assertNotClosedOrThrow();
		this.ensureInitiallyMoved();
		return onGetModel();
	}

	/**
	 * Returns copy of the associated model with data for the requested cursor <var>position</var>.
	 *
	 * @param position The position of this cursor for which to obtain and bind data to the requested
	 *                 model.
	 * @return Copy of the model with data for the requested cursor <var>position</var> or {@code null}
	 * if this cursor wrapper cannot be moved to the specified position.
	 * @see #getModel()
	 */
	@Nullable
	public M getModelAt(int position) {
		this.assertNotClosedOrThrow();
		return moveToPosition(position) ? onGetModel() : null;
	}

	/**
	 * Invoked whenever a new instance of model specific for this model cursor wrapper is requested.
	 * <p>
	 * This implementation assumes that the current model has been already bound with data via
	 * {@link #onBindModel(EntityModel)} and creates a shallow copy of the model via {@link EntityModel#clone()}.
	 *
	 * @return New instance of model with the data for the current position.
	 * @see #getModel()
	 * @see #getModelAt(int)
	 */
	@NonNull
	@SuppressWarnings("unchecked")
	protected M onGetModel() {
		return (M) mModel.clone();
	}

	/**
	 * Unlike {@link #getModel()}, this will allow only access to the current model with data for the
	 * current cursor position.
	 * <p>
	 * <b>Use this only to bind data within cursor adapter or just to access data form the model.
	 * If you intend to manipulate, in any way, with the requested model or you need to pass it
	 * further in your application's flow use {@link #getModel()} instead.</b>
	 *
	 * @return Model with data for the current cursor position.
	 * @see #accessModelAt(int)
	 */
	@NonNull
	public M accessModel() {
		this.assertNotClosedOrThrow();
		this.ensureInitiallyMoved();
		return mModel;
	}

	/**
	 * Unlike {@link #getModelAt(int)}, this will allow only access to the current model with data
	 * for the requested cursor <var>position</var>.
	 * <p>
	 * <b>Use this only to bind data within cursor adapter or just to access data form the model.
	 * If you intend to manipulate, in any way, with the requested model or you need to pass it
	 * further in your application's flow use {@link #getModelAt(int)} instead.</b>
	 *
	 * @param position The position of this cursor for which to obtain and bind data to the requested
	 *                 model.
	 * @return Model with data for the requested cursor <var>position</var> or {@code null} if this
	 * cursor wrapper cannot be moved to the specified position.
	 * @see #accessModel()
	 */
	@Nullable
	public M accessModelAt(int position) {
		this.assertNotClosedOrThrow();
		return moveToPosition(position) ? mModel : null;
	}

	/**
	 */
	@Override
	@CallSuper
	@CheckResult
	protected boolean onBindData() {
		if (mModel != null) {
			final M model = onBindModel(mModel);
			if (model != null) {
				this.mModel = model;
				return true;
			}
		}
		return false;
	}

	/**
	 * Invoked whenever {@link #onBindData()} is called to bind data for the current position to the
	 * given <var>model</var>.
	 * <p>
	 * This implementation binds the model via {@link EntityModel#fromCursor(Cursor)}.
	 *
	 * @param model The model to be bound with the current data.
	 * @return Model with data if bounding has been successful, {@code null} if data failed to be
	 * bound the the model.
	 */
	@Nullable
	protected M onBindModel(@NonNull M model) {
		return model.fromCursor(this) ? model : null;
	}

	/**
	 */
	@Override
	protected void onClearData() {
		if (mModel != null) this.mModel = onClearModel(mModel);
	}

	/**
	 * Invoked whenever {@link #onClearData()} is called to clear current data bound to the given
	 * <var>model</var>.
	 * <p>
	 * This implementation creates a new clone from the empty model instance specified for this
	 * cursor wrapper during initialization.
	 *
	 * @param model The model with data bound for the current cursor position.
	 * @return Model with all data cleared. Can be a new instance of the model.
	 */
	@SuppressWarnings("unchecked")
	protected M onClearModel(@NonNull M model) {
		return (M) mModelTemplate.clone();
	}

	/**
	 * Asserts that this cursor wrapper is not closed, if it is an {@link IllegalStateException} is
	 * thrown.
	 *
	 * @throws IllegalStateException If this cursor wrapper is already closed.
	 */
	private void assertNotClosedOrThrow() {
		if (isClosed()) throw new IllegalStateException(getClass().getSimpleName() + " is already closed!");
	}

	/**
	 */
	@Override
	public void close() {
		super.close();
		if (mRecycleModelOnClose) onRecycleModel();
	}

	/**
	 * Invoked to recycle the current model instance via {@link EntityModel#recycle()}.
	 * <p>
	 * If model has been already recycled, this method does nothing.
	 * <p>
	 * <b>Note, that after the model is recycled, this cursor wrapper should not be used further
	 * as any such attempt may result in {@link NullPointerException} to be thrown.</b>
	 */
	@CallSuper
	protected void onRecycleModel() {
		if (mModel != null) {
			this.mModel.recycle();
			this.mModel = null;
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
Database-Model-Cursor
===============

This module contains implementation of `CursorWrapper` that uses `EntityModel` in order to provide
its current data via **model** representation.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-model-cursor:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-model-core](https://bitbucket.org/android-universum/database/src/main/library-model-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [EntityModelCursorWrapper](https://bitbucket.org/android-universum/database/src/main/library-model-cursor/src/main/java/universum/studios/android/database/model/EntityModelCursorWrapper.java)
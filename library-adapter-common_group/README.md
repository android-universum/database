@Database-Adapter-Common
===============

This module groups the following modules into one **single group**:

- [Adapter-Core](https://bitbucket.org/android-universum/database/src/main/library-adapter-core)
- [Adapter-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-loader)
- [Adapter-Recycler-Base](https://bitbucket.org/android-universum/database/src/main/library-adapter-recycler-base)
- [Adapter-Recycler-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-recycler-loader)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-common:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-content-loader](https://bitbucket.org/android-universum/database/src/main/library-content-loader),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor)
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.CursorLoader;

/**
 * A {@link BaseCursorDataLoader} implementation that may be used to load a {@link Cursor} data that
 * need to be presented in the application's UI along with set of <b>headers</b>. A single header
 * could represent one category of data accessible by the loaded Cursor where all headers would
 * represent all categories of the Cursor's data. Type of headers depends on how the Cursor's data
 * are categorized/sorted, but such implementation detail is not important for a specific HeadersCursorLoader
 * implementation but rather for {@link DataHeaders} implementation. See section describing loader's
 * result below.
 * <p>
 * Result of HeadersCursorLoader is specified via {@link HeadersResult}. This result object will
 * contain the {@link Cursor} data loaded in the background along with <b>Headers</b> data created
 * from the loaded cursor. A new headers object is created whenever {@link #loadInBackground()} is
 * invoked for the loader implementation. Headers data are then created/populated via
 * {@link DataHeaders#fromData(Object)} method using the loaded cursor as source data.
 *
 * @param <H> Type of the headers to be created and delivered by this loader along with the
 *            related Cursor.
 * @author Martin Albedinsky
 */
public abstract class HeadersCursorLoader<H extends DataHeaders<Cursor>> extends BaseCursorDataLoader<HeadersResult<Cursor, H>> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "HeadersCursorLoader";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of HeadersCursorLoader with <b>query parameters</b> copied from the given
	 * <var>cursorLoader</var>.
	 *
	 * @param context      Context used to access {@link ContentResolver}.
	 * @param cursorLoader The loader used to configure the new HeadersCursorLoader using its parameters
	 *                     like <b>uri</b>, <b>selection</b>, <b>...</b>.
	 */
	public HeadersCursorLoader(@NonNull final Context context, @NonNull final CursorLoader cursorLoader) {
		super(context, cursorLoader);
	}

	/**
	 * Creates a new instance of HeadersCursorLoader with the specified parameters
	 *
	 * @param context       Context used to access {@link ContentResolver}.
	 * @param uri           The desired content uri for the query.
	 * @param projection    The desired projection for the query.
	 * @param selection     The desired selection for the query.
	 * @param selectionArgs The desired arguments for the <var>selection</var>.
	 * @param sortOrder     The desired sort order for the query.
	 */
	public HeadersCursorLoader(
			@NonNull final Context context,
			@NonNull final Uri uri,
			@Nullable final String[] projection,
			@Nullable final String selection,
			@Nullable final String[] selectionArgs,
			@Nullable final String sortOrder
	) {
		super(context, uri, projection, selection, selectionArgs, sortOrder);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Nullable
	@Override
	protected HeadersResult<Cursor, H> onLoadFinished(@NonNull final Cursor cursor) {
		final H headers = onCreateHeaders();
		return new Result<>(cursor, headers.fromData(cursor) ? headers : null);
	}

	/**
	 */
	@Nullable
	@Override
	protected HeadersResult<Cursor, H> onLoadFailed() {
		return new Result<>(null, null);
	}

	/**
	 * Invoked whenever this loader needs a new instance of Headers object. This is whenever
	 * {@link #loadInBackground()} is invoked to load new data.
	 *
	 * @return Always new instance of Headers specific for this type of loader.
	 */
	@NonNull
	protected abstract H onCreateHeaders();

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link HeadersResult} implementation used to deliver {@link Cursor} and {@link DataHeaders}
	 * data to {@link HeadersCursorLoader HeadersCursorLoader's} registered callbacks.
	 */
	private static final class Result<H extends DataHeaders<Cursor>> extends HeadersResult<Cursor, H> {

		/**
		 * Creates a new instance of Result with the given cursor <var>data</var> and <var>headers</var>.
		 *
		 * @param data    The cursor data for the new result.
		 * @param headers The headers for the new result
		 */
		Result(@Nullable final Cursor data, @Nullable final H headers) {
			super(data, headers);
		}

		/**
		 */
		@Override
		public void release() {
			if (data != null && !data.isClosed()) data.close();
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;

/**
 * An interface for headers that may be created for a specific data set.
 *
 * @param <D> Type of the data for which these headers implementation can be created.
 * @author Martin Albedinsky
 */
public interface DataHeaders<D> {

	/**
	 * Creates these headers for the specified <var>data</var>.
	 *
	 * @param data The data from which to create these headers.
	 * @return {@code True} if these headers were successfully created from the given data, {@code false}
	 * if some error has occurred and these headers are not valid for use.
	 */
	@CheckResult
	boolean fromData(@NonNull D data);
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.database.Cursor;

import androidx.annotation.Nullable;

/**
 * A result that may be loaded via {@link HeadersCursorLoader}. This result object contains a loaded
 * data and its associated headers object.
 *
 * @param <D> Type of the data to be carried by the headers result.
 * @param <H> Type of the headers associated with the result data.
 * @author Martin Albedinsky
 */
public abstract class HeadersResult<D, H extends DataHeaders<D>> implements DataResult {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "HeadersResult";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Data specified for this result. May be {@code null} if no data has been loaded.
	 */
	@Nullable
	public final D data;

	/**
	 * Headers specified for this result. May be {@code null} if no data has been loaded or filling
	 * of these headers from the loaded data has failed.
	 *
	 * @see DataHeaders#fromData(Object)
	 */
	@Nullable
	public final H headers;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of HeadersResult with the given <var>data</var> and <var>headers</var>.
	 *
	 * @param data    The data for the new result.
	 * @param headers The headers for the new result
	 */
	public HeadersResult(@Nullable final D data, @Nullable final H headers) {
		this.data = data;
		this.headers = headers;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * A subclass that holds a 'releasable' data, like {@link Cursor}, should release them here.
	 */
	@Override
	public abstract void release();

	/*
	 * Inner classes ===============================================================================
	 */
}
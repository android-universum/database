Database-Content-Header
===============

This module contains elements that may be used for implementation of loading of **headers** for
a `Cursor` data set.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-content-header:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-content-loader](https://bitbucket.org/android-universum/database/src/main/library-content-loader)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [HeadersCursorLoader](https://bitbucket.org/android-universum/database/src/main/library-content-header/src/main/java/universum/studios/android/database/content/HeadersCursorLoader.java)
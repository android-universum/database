/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import android.database.Cursor;
import android.os.Parcel;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.model.SimpleEntityModel;

/**
 * A {@link SimpleEntityModel} implementation which represents a model that holds <b>lorem ipsum</b>
 * data generated using {@link LoremIpsumGenerator}.
 *
 * <h3>Provided data:</h3>
 * <b>TEXTS:</b>
 * <ul>
 * <li>{@link #getTitle()}</li>
 * <li>{@link #getName()}</li>
 * <li>{@link #getNote()}</li>
 * <li>{@link #getDescription()}</li>
 * <li>{@link #getContent()}</li>
 * </ul>
 * <p>
 * <b>NUMBERS:</b>
 * <ul>
 * <li>{@link #getSize()}</li>
 * </ul>
 * <p>
 * <b>DATES:</b>
 * <ul>
 * <li>{@link #getDateCreated()}</li>
 * <li>{@link #getDateModified()}</li>
 * </ul>
 * <p>
 * <b>BOOLEAN FLAGS:</b>
 * <ul>
 * <li>{@link #isEnabled()}</li>
 * <li>{@link #isEmpty()}</li>
 * <li>{@link #isEditable()}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public class LoremIpsum extends SimpleEntityModel<LoremIpsum> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LoremIpsum";

	/**
	 * Flag indicating whether a specific lorem ipsum model is enabled or not.
	 */
	private static final int FLAG_ENABLED = 0x00000001;

	/**
	 * Flag indicating whether a specific lorem ipsum model is empty or not.
	 */
	private static final int FLAG_EMPTY = 0x00000001 << 1;

	/**
	 * Flag indicating whether a specific lorem ipsum model is editable or not.
	 */
	private static final int FLAG_EDITABLE = 0x00000001 << 2;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Creator used to create an instance or array of instances of LoremIpsum from {@link Parcel}.
	 */
	public static final Creator<LoremIpsum> CREATOR = new Creator<LoremIpsum>() {

		/**
		 */
		@Override
		public LoremIpsum createFromParcel(@NonNull Parcel source) {
			return new LoremIpsum(source);
		}

		/**
		 */
		@Override
		public LoremIpsum[] newArray(int size) {
			return new LoremIpsum[size];
		}
	};

	/**
	 * Factory that can be used to create empty instances of LoremIpsum model.
	 */
	public static final PoolFactory<LoremIpsum> FACTORY = new PoolFactory<LoremIpsum>(10) {

		/**
		 */
		@NonNull
		@Override
		public LoremIpsum createModel() {
			return new LoremIpsum();
		}
	};

	/*
	 * Members =====================================================================================
	 */

	/**
	 * See {@link LoremIpsumContract.Columns#ID}.
	 */
	@Column.Primary
	@Column(LoremIpsumContract.Columns.ID)
	Long id;

	// TEXTS ---------------------------------------------------------------------------------------

	/**
	 * See {@link LoremIpsumContract.Columns#TITLE}.
	 */
	@Column(LoremIpsumContract.Columns.TITLE)
	String title;

	/**
	 * See {@link LoremIpsumContract.Columns#NAME}.
	 */
	@Column(LoremIpsumContract.Columns.NAME)
	String name;

	/**
	 * See {@link LoremIpsumContract.Columns#NOTE}.
	 */
	@Column(LoremIpsumContract.Columns.NOTE)
	String note;

	/**
	 * See {@link LoremIpsumContract.Columns#CONTENT}.
	 */
	@Column(LoremIpsumContract.Columns.CONTENT)
	String content;

	/**
	 * See {@link LoremIpsumContract.Columns#DESCRIPTION}.
	 */
	@Column(LoremIpsumContract.Columns.DESCRIPTION)
	String description;

	// DATES ---------------------------------------------------------------------------------------

	/**
	 * See {@link LoremIpsumContract.Columns#DATE_CREATED}.
	 */
	@Column(LoremIpsumContract.Columns.DATE_CREATED)
	Long dateCreated;

	/**
	 * See {@link LoremIpsumContract.Columns#DATE_MODIFIED}.
	 */
	@Column(LoremIpsumContract.Columns.DATE_MODIFIED)
	Long dateModified;

	// NUMBERS -------------------------------------------------------------------------------------

	/**
	 * See {@link LoremIpsumContract.Columns#SIZE}.
	 */
	@Column(value = LoremIpsumContract.Columns.SIZE, defaultValue = "0")
	Integer size;

	// BOOLEAN FLAGS -------------------------------------------------------------------------------

	/**
	 * See {@link LoremIpsumContract.Columns#FLAGS}.
	 */
	@Column(value = LoremIpsumContract.Columns.FLAGS, defaultValue = "0")
	Integer flags;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of empty LoremIpsum.
	 * <p>
	 * <b>Note</b>, that this constructor is here only so a new instance of this model can be created
	 * via reflection. It should not be used directly by Android application unless it is instantiated
	 * for further data binding from {@link Cursor} via {@link #fromCursor(Cursor)}.
	 * <p>
	 * To create LoremIpsum with populated data use {@link #generate()} or {@link #generate(long)}
	 * instead.
	 */
	public LoremIpsum() {
		super();
	}

	/**
	 * Called form {@link #CREATOR} to create an instance of LoremIpsum form the given parcel
	 * <var>source</var>.
	 *
	 * @param source Parcel with data for the new instance.
	 */
	protected LoremIpsum(@NonNull Parcel source) {
		super(source);
	}

	/**
	 * Creates a new instance of LoremIpsum model with generated data.
	 *
	 * @param id Id for the new lorem ipsum model.
	 */
	LoremIpsum(Long id) {
		super();
		this.id = id;
		this.generateTexts();
		this.generateNumbers();
		this.generateDates();
		this.generateFlags();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called to generate texts data for this new LoremIpsum model.
	 */
	private void generateTexts() {
		this.title = generateTitle();
		this.name = generateName();
		this.note = generateNote();
		this.content = generateContent();
		this.description = generateDescription();
	}

	/**
	 * Called to generate numbers data for this new LoremIpsum model.
	 */
	private void generateNumbers() {
		this.size = generateSize();
	}

	/**
	 * Called to generate dates data for this new LoremIpsum model.
	 */
	private void generateDates() {
		final long currentTime = System.currentTimeMillis();
		this.dateCreated = generateDate(0, currentTime - 1);
		this.dateModified = generateDate(dateCreated, currentTime);
	}

	/**
	 * Called to generate flags for this new LoremIpsum model.
	 */
	private void generateFlags() {
		updatePrivateFlags(FLAG_ENABLED, generateEnabled());
		updatePrivateFlags(FLAG_EMPTY, generateEmpty());
		updatePrivateFlags(FLAG_EDITABLE, generateEditable());
	}

	/**
	 * Same as {@link #generate(long)} for lorem ipsum model without valid id.
	 */
	@NonNull
	public static LoremIpsum generate() {
		return new LoremIpsum((Long) null);
	}

	/**
	 * Generates a new instance of LoremIpsum model with generated random data.
	 *
	 * @param id Id for the new lorem ipsum model.
	 * @return New LoremIpsum of which data can be immediately accessed.
	 */
	@NonNull
	public static LoremIpsum generate(long id) {
		return new LoremIpsum(id);
	}

	/**
	 * Generates a text with <b>minimum 2</b> and <b>maximum 5</b> words.
	 *
	 * @return New lorem ipsum title text.
	 */
	@NonNull
	public static String generateTitle() {
		return LoremIpsumGenerator.firstToUpperCase(LoremIpsumGenerator.randomWords(randomCount(2, 5)));
	}

	/**
	 * Generates a text with <b>minimum 1</b> and <b>maximum 2</b> words.
	 *
	 * @return New lorem ipsum name text.
	 */
	@NonNull
	public static String generateName() {
		return LoremIpsumGenerator.firstToUpperCase(LoremIpsumGenerator.randomWords(randomCount(1, 2)));
	}

	/**
	 * Generates a text with <b>minimum 5</b> and <b>maximum 15</b> words.
	 *
	 * @return New lorem ipsum note text.
	 */
	@NonNull
	public static String generateNote() {
		return LoremIpsumGenerator.firstToUpperCase(LoremIpsumGenerator.randomWords(randomCount(5, 15))) + "";
	}

	/**
	 * Generates a text with words size of {@link LoremIpsumGenerator#SIZE_ANY} divided into
	 * <b>minimum 1</b> and <b>maximum 2</b> sentences.
	 *
	 * @return New lorem ipsum description text.
	 */
	@NonNull
	public static String generateDescription() {
		return LoremIpsumGenerator.randomSentences(randomCount(1, 2), LoremIpsumGenerator.SIZE_ANY);
	}

	/**
	 * Generates a text with words size of {@link LoremIpsumGenerator#SIZE_ANY} divided into
	 * <b>minimum 5</b> and <b>maximum 10</b> sentences.
	 *
	 * @return New lorem ipsum content text.
	 */
	@NonNull
	public static String generateContent() {
		return LoremIpsumGenerator.randomSentences(randomCount(5, 10), LoremIpsumGenerator.SIZE_ANY);
	}

	/**
	 * Generates a date from the specified range.
	 *
	 * @param begin The bottom boundary for the requested date in milliseconds.
	 * @param end   The top boundary for the requested date in milliseconds.
	 * @return Requested date from the specified range.
	 */
	public static long generateDate(long begin, long end) {
		return begin + ((end - begin) / (1 + LoremIpsumGenerator.RANDOM.nextInt(10)));
	}

	/**
	 * Generates an integer number from the range [0, {@link Integer#MAX_VALUE}).
	 *
	 * @return Positive integer number.
	 */
	public static int generateSize() {
		return Math.abs(LoremIpsumGenerator.RANDOM.nextInt());
	}

	/**
	 * Generates a boolean flag value of {@code true} with <b>minimum 40% probability</b>.
	 *
	 * @return {@code True} or {@code false}.
	 */
	public static boolean generateEnabled() {
		return generateBoolean(0.4f);
	}

	/**
	 * Generates a boolean flag value of {@code true} with <b>minimum 0% probability</b>.
	 *
	 * @return {@code True} or {@code false}.
	 */
	public static boolean generateEmpty() {
		return generateBoolean(0);
	}

	/**
	 * Generates a boolean flag value of {@code true} with <b>minimum 20% probability</b>.
	 *
	 * @return {@code True} or {@code false}.
	 */
	public static boolean generateEditable() {
		return generateBoolean(0.2f);
	}

	/**
	 * Generates a boolean flag with the specified minimum <var>probability</var>.
	 *
	 * @param probability The minimum probability that should be taken into count.
	 * @return {@code True} or {@code false} with the requested probability.
	 */
	private static boolean generateBoolean(float probability) {
		if (probability == 0) return LoremIpsumGenerator.RANDOM.nextBoolean();
		return LoremIpsumGenerator.RANDOM.nextFloat() + probability >= 0.5f;
	}

	/**
	 * Returns the id of this lorem ipsum model.
	 *
	 * @return This model's id.
	 */
	public long getId() {
		return id == null ? NO_ID : id;
	}

	/**
	 * Returns the title of this lorem ipsum model.
	 *
	 * @return The title text generated via {@link #generateTitle()}.
	 */
	@NonNull
	public String getTitle() {
		return title == null ? "" : title;
	}

	/**
	 * Returns the name of this lorem ipsum model.
	 *
	 * @return The name text generated via {@link #generateName()}.
	 */
	@NonNull
	public String getName() {
		return name == null ? "" : name;
	}

	/**
	 * Returns the note of this lorem ipsum model.
	 *
	 * @return The note text generated via {@link #generateNote()}.
	 */
	@NonNull
	public String getNote() {
		return note == null ? "" : note;
	}

	/**
	 * Returns the description of this lorem ipsum model.
	 *
	 * @return The description text generated via {@link #generateDescription()}.
	 */
	@NonNull
	public String getDescription() {
		return description == null ? "" : description;
	}

	/**
	 * Returns the content of this lorem ipsum model.
	 *
	 * @return The content text generated via {@link #generateContent()}.
	 */
	@NonNull
	public String getContent() {
		return content == null ? "" : content;
	}

	/**
	 * Returns the size of this lorem ipsum.
	 *
	 * @return Number from the range [0, {@link Integer#MAX_VALUE}) or {@code 0} if such value is
	 * not initialized.
	 */
	public int getSize() {
		return size == null ? 0 : size;
	}

	/**
	 * Returns the date when this lorem ipsum model has been created.
	 * <p>
	 * This date value has been generated as {@code LoremIpsum.generateDate(0, System.currentTimeMillis())}.
	 *
	 * @return Date of creation in milliseconds generated via {@link #generateDate(long, long)} or
	 * {@code 0} if such value is not initialized.
	 * @see #getDateModified()
	 */
	public long getDateCreated() {
		return dateCreated == null ? 0 : dateCreated;
	}

	/**
	 * Returns the date when this lorem ipsum model has been modified.
	 * <p>
	 * This date value has been generated as {@code LoremIpsum.generateDate(dateCreated, System.currentTimeMillis())}.
	 *
	 * @return Date of modification in milliseconds generated via {@link #generateDate(long, long)}
	 * or {@code 0} if such value is not initialized.
	 * @see #getDateCreated()
	 */
	public long getDateModified() {
		return dateModified == null ? 0 : dateModified;
	}

	/**
	 * Returns the boolean flag indicating whether this lorem ipsum is enabled or not.
	 * <p>
	 * Minimum probability: <b>40%</b>
	 *
	 * @return {@code True} if it is enabled, {@code false} otherwise.
	 */
	public boolean isEnabled() {
		return hasPrivateFlag(FLAG_ENABLED);
	}

	/**
	 * Returns the boolean flag indicating whether this lorem ipsum is empty or not.
	 * <p>
	 * Minimum probability: <b>0%</b>
	 *
	 * @return {@code True} if it is empty, {@code false} otherwise.
	 */
	public boolean isEmpty() {
		return hasPrivateFlag(FLAG_EMPTY);
	}

	/**
	 * Returns the boolean flag indicating whether this lorem ipsum is editable or not.
	 * <p>
	 * Minimum probability: <b>20%</b>
	 *
	 * @return {@code True} if it is editable, {@code false} otherwise.
	 */
	public boolean isEditable() {
		return hasPrivateFlag(FLAG_EDITABLE);
	}

	/**
	 * Generates a random count from the specified range.
	 *
	 * @param min The minimum number required (including).
	 * @param max The maximum number required (including).
	 * @return Count from the requested range.
	 */
	private static int randomCount(int min, int max) {
		return min + LoremIpsumGenerator.RANDOM.nextInt(max - min + 1);
	}

	/**
	 * Updates the current private flags.
	 *
	 * @param flag Value of the desired flag to add/remove to/from the current private flags.
	 * @param add  Boolean flag indicating whether to add or remove the specified <var>flag</var>.
	 */
	@SuppressWarnings("unused")
	private void updatePrivateFlags(int flag, boolean add) {
		if (flags == null) this.flags = 0;
		if (add) this.flags |= flag;
		else this.flags &= ~flag;
	}

	/**
	 * Returns the boolean flag indicating whether the specified <var>flag</var> is contained within
	 * the current private flags or not.
	 *
	 * @param flag Value of the flag to check.
	 * @return {@code True} if the requested flag is contained, {@code false} otherwise.
	 */
	@SuppressWarnings("unused")
	private boolean hasPrivateFlag(int flag) {
		return flags != null && (flags & flag) != 0;
	}

	/**
	 */
	@Override
	public void recycle() {
		super.recycle();
		FACTORY.recycleModel(this);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
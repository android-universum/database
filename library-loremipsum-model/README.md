Database-LoremIpsum-Model
===============

This module contains implementation of `EntityModel` that may represent **Lorem Ipsum** data generated
by `LoremIpsumGenerator`.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-loremipsum-model:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-model](https://bitbucket.org/android-universum/database/src/main/library-model),
[database-loremipsum-core](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [LoremIpsum](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-model/src/main/java/universum/studios/android/database/loremipsum/LoremIpsum.java)
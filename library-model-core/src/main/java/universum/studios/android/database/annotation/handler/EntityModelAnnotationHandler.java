/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.SimpleEntityModel;

/**
 * An {@link AnnotationHandler} extended interface for annotation handlers from the Database library
 * that are used to handle processing of annotations attached to classes derived from the {@link EntityModel}
 * interface provided by this library.
 *
 * @author Martin Albedinsky
 * @see SimpleEntityModel
 */
public interface EntityModelAnnotationHandler<M extends EntityModel> extends AnnotationHandler {

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Interface for entity model processor that is used to transform data of a specific EntityModel
	 * instance into {@link ContentValues} object.
	 *
	 * @author Martin Albedinsky
	 */
	interface ContentValuesProcessor {

		/**
		 * Puts current data of the specified <var>model</var> into ContentValues object.
		 * <p>
		 * <b>Note</b>, that if processing for some columns data fails other data will be still
		 * processed so the resulting content values object may contain less key-&gt;value pairs as
		 * there is columns.
		 * <p>
		 * Also note, that the resulting content values object will not contain data for columns
		 * marked by {@link Column.Joined @Column.Joined} annotation as such columns are used only
		 * for projection purpose.
		 *
		 * @param model   The model of which current data to put into ContentValues object.
		 * @param columns Array with column fields that are marked within the given model by
		 *                {@link Column @Column} annotation.
		 * @return ContentValues object with model's current data obtained from its fields mapped to
		 * column names obtained from {@link Column @Column} annotations as theirs keys. If the model
		 * cannot be processed, the content values object will be empty ({@link ContentValues#size()} == 0).
		 */
		@NonNull
		@CheckResult
		ContentValues processModel(@NonNull Object model, @NonNull AnnotatedColumn[] columns);
	}

	/**
	 * Interface for entity model processor that is used to bind data of a specific EntityModel
	 * instance from database {@link Cursor}.
	 *
	 * @author Martin Albedinsky
	 */
	interface CursorProcessor {

		/**
		 * Binds data of the specified <var>model</var> from the given <var>cursor</var>.
		 * <p>
		 * <b>Note</b>, that the given cursor need to be already moved to a position at which should
		 * be data for the model obtained.
		 *
		 * @param model   The model of which data to bind from the cursor.
		 * @param columns Array with column fields that are marked within the given model by
		 *                {@link Column @Column} annotation.
		 * @param cursor  The cursor from which to obtain data for the specified model.
		 * @return {@code True} if data for all annotated columns has been successfully bound,
		 * {@code false} if binding of at least one has failed.
		 */
		@CheckResult
		boolean processModel(@NonNull Object model, @NonNull AnnotatedColumn[] columns, @NonNull Cursor cursor);
	}

	/**
	 * Interface for entity model processor that is used to bind and write data of a specific EntityModel
	 * instance from/into {@link Parcel}.
	 *
	 * @author Martin Albedinsky
	 */
	interface ParcelProcessor {

		/**
		 * Writes current data of the specified <var>model</var> into the given <var>destination</var>
		 * parcel.
		 *
		 * @param model       The model of which current data to write into the parcel.
		 * @param columns     Array with column fields that are marked within the given model by
		 *                    {@link Column @Column} annotation.
		 * @param destination The parcel into which to write current values of all annotated columns
		 * @param flags       Contextual flags for parcelable column types.
		 * @return {@code True} if current values of all annotated columns has been successfully
		 * written into the parcel, {@code false} if writing of at least one has failed.
		 */
		@CheckResult
		boolean processModelIntoParcel(@NonNull Object model, @NonNull AnnotatedColumn[] columns, @NonNull Parcel destination, int flags);

		/**
		 * Binds data of the specified <var>model</var> from the given <var>source</var> parcel.
		 *
		 * @param model   The model of which data to bind from the parcel.
		 * @param columns Array with column fields that are marked within the given model by
		 *                {@link Column @Column} annotation.
		 * @param source  The parcel containing data for the given model.
		 * @return {@code True} if data for all annotated columns has been successfully bound,
		 * {@code false} if binding of at least one has failed.
		 */
		@CheckResult
		boolean processModelFromParcel(@NonNull Object model, @NonNull AnnotatedColumn[] columns, @NonNull Parcel source);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns all annotated columns for this handler related entity model class that are marked by
	 * {@link Column @Column} annotation.
	 *
	 * @return Array with columns that are marked by {@link Column @Column} annotation within the
	 * entity model class.
	 */
	@NonNull
	AnnotatedColumn[] getColumns();

	/**
	 * Handles instantiation of a new instance for this handler related entity model from the model
	 * class for which is this handler created.
	 *
	 * @return New instance of the requested model.
	 */
	@NonNull
	M createModel();

	/**
	 * Sets a processor that is used to transform current data of a specific entity model instance
	 * related to this handler into {@link ContentValues} object.
	 *
	 * @param processor The desired processor. Can be {@code null} to use the default one.
	 * @see #handleToContentValues(Object)
	 */
	void setContentValuesProcessor(@Nullable ContentValuesProcessor processor);

	/**
	 * Handles processing of current data of the specified <var>model</var> into {@link ContentValues}
	 * object.
	 *
	 * @param model The model of which data to put into content values.
	 * @return ContentValues with key-&gt;value pairs (current model's data mapped to theirs column names).
	 * @see #setContentValuesProcessor(ContentValuesProcessor)
	 * @see ContentValuesProcessor#processModel(Object, AnnotatedColumn[])
	 */
	@NonNull
	ContentValues handleToContentValues(@NonNull Object model);

	/**
	 * Sets a processor that is used to bind data of a specific entity model instance related to this
	 * handler from {@link Cursor}.
	 *
	 * @param processor The desired processor. Can be {@code null} to use the default one.
	 * @see #handleFromCursor(Object, Cursor)
	 */
	void setCursorProcessor(@Nullable CursorProcessor processor);

	/**
	 * Handles processing of data for the specified <var>model</var> from the given <var>cursor</var>.
	 * <p>
	 * <b>Note</b>, that the cursor need to be already moved to a position at which should be its
	 * data processed.
	 *
	 * @param model  The model of which data to bind from the cursor.
	 * @param cursor The cursor moved to the position containing data for the model.
	 * @return {@code True} if all data has been successfully bound, {@code false} if some error has
	 * occurred and some data failed to be bound.
	 * @see #setCursorProcessor(CursorProcessor)
	 * @see CursorProcessor#processModel(Object, AnnotatedColumn[], Cursor)
	 */
	boolean handleFromCursor(@NonNull Object model, @NonNull Cursor cursor);

	/**
	 * Sets a processor that is used to bind/write data of a specific entity model instance related
	 * to this handler from/into {@link Parcel}.
	 *
	 * @param processor The desired processor. Can be {@code null} to use the default one.
	 * @see #handleWriteToParcel(Object, Parcel, int)
	 * @see #handleFromParcel(Object, Parcel)
	 */
	void setParcelProcessor(@Nullable ParcelProcessor processor);

	/**
	 * Handles processing of current data of the specified <var>model</var> into the given <var>destination</var>
	 * parcel.
	 *
	 * @param model       The model of which data to write into the parcel.
	 * @param destination The parcel into which to write model's data.
	 * @param flags       Contextual flags for parcelable column types.
	 * @return {@code True} if all data has been written into the parcel, {@code false} if some
	 * error has occurred and some data failed to be written.
	 * @see #setParcelProcessor(ParcelProcessor)
	 * @see ParcelProcessor#processModelIntoParcel(Object, AnnotatedColumn[], Parcel, int)
	 */
	boolean handleWriteToParcel(@NonNull Object model, @NonNull Parcel destination, int flags);

	/**
	 * Handles processing of data for the specified <var>model</var> from the given <var>source</var>
	 * parcel.
	 *
	 * @param model  The model of which data to bind from the parcel.
	 * @param source The parcel containing data for the model.
	 * @return {@code True} if all data has been successfully bound, {@code false} if some error has
	 * occurred and some data failed to be bound.
	 * @see #setParcelProcessor(ParcelProcessor)
	 * @see ParcelProcessor#processModelFromParcel(Object, AnnotatedColumn[], Parcel)
	 */
	boolean handleFromParcel(@NonNull Object model, @NonNull Parcel source);

	/**
	 * Handles creation of a projection for this handler related entity model class.
	 *
	 * @return Projection containing all columns that are marked within the model class by
	 * {@link Column} annotation.
	 */
	@NonNull
	String[] handleCreateProjection();

	/**
	 * Handles creation of a selection targeting a single or multiple row/-s within a database table
	 * for the specified <var>model</var>.
	 *
	 * <h3>Example:</h3>
	 * <pre>
	 * public class Router extends SimpleEntityModel {
	 *
	 *     // ... CREATOR, FACTORY
	 *
	 *     &#64;Column.Primary
	 *     &#64;Column("_id")
	 *     public Long id; // With current value: 12345
	 *
	 *     &#64;Column.Unique
	 *     &#64;Column("mac_address")
	 *     public String macAddress; // With current value: '05:AD:34:19:BV:IE'
	 *
	 *     // ... other column fields marked by @Column annotation
	 * }
	 * </pre>
	 * For the sample model above the selection will be created with value and arguments like this:
	 * <p>
	 * <b>{@link EntityModel.Selection#getValue() Selection.getValue()}: _id=? AND mack_address=?</b>
	 * <p>
	 * <b>{@link EntityModel.Selection#getArguments() Selection.getArguments()}: ["12345", "05:AD:34:19:BV:IE"]</b>
	 *
	 * @param model The model for which to create selection.
	 * @return Selection with statement and arguments based on primary columns marked by
	 * {@link Column.Primary @Column.Primary} and unique columns marked by
	 * {@link Column.Unique @Column.Unique} annotations. May be {@link EntityModel#EMPTY_SELECTION}
	 * if the specified model does not have none of its column fields marked by one of the
	 * annotations mentioned above or the annotated fields are actually {@code null} at this time.
	 */
	@NonNull
	EntityModel.Selection handleCreateSelection(@NonNull Object model);

	/**
	 * Handles recycling of data of the given <var>model</var>.
	 *
	 * @param model The model of which current column data to recycle.
	 */
	void handleRecycle(@NonNull Object model);
}
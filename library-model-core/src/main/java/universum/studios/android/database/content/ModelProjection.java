/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import androidx.annotation.NonNull;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.handler.EntityModelAnnotationHandlers;
import universum.studios.android.database.model.EntityModel;

/**
 * Utility class that may be used to create a projection set for a desired {@link EntityModel} class.
 *
 * @author Martin Albedinsky
 */
public final class ModelProjection {

	/**
	 */
	private ModelProjection() {
		// Creation of instances of this class is not publicly allowed.
	}

	/**
	 * Creates a projection set of column names specified by the given <var>classOfModel</var>.
	 *
	 * @param classOfModel The entity model for which to create the desired projection set.
	 * @return Projection with the model's columns.
	 * @throws DatabaseException If annotations processing is disabled.
	 */
	@NonNull
	@SuppressWarnings("ConstantConditions")
	public static String[] create(@NonNull Class<? extends EntityModel> classOfModel) {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return EntityModelAnnotationHandlers.obtainModelHandler(classOfModel).handleCreateProjection();
	}
}
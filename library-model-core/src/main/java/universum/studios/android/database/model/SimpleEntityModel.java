/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.CallSuper;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.handler.EntityModelAnnotationHandler;
import universum.studios.android.database.annotation.handler.EntityModelAnnotationHandlers;

/**
 * An {@link EntityModel} implementation that supports both, {@link #fromCursor(Cursor)} and
 * {@link #toContentValues()} methods along with writing and reading into/from {@link Parcel} object.
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link universum.studios.android.database.annotation.Column @Column} <b>[field - recursive]</b>
 * <p>
 * All fields marked with this annotation will be used when transforming a model of subclass of
 * this SimpleEntityModel to {@link ContentValues} object via {@link #toContentValues()} or when
 * binding such model with data from {@link Cursor} via {@link #fromCursor(Cursor)}.
 * </li>
 * </ul>
 *
 * @param <M> A type of the sub-class of this SimpleEntityModel.
 * @author Martin Albedinsky
 */
public abstract class SimpleEntityModel<M extends SimpleEntityModel> implements EntityModel, Parcelable {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SimpleEntityModel";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Handler responsible for processing of all annotations of this class and also for handling all
	 * annotations related operations for this class.
	 */
	private final EntityModelAnnotationHandler<M> mAnnotationHandler;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SimpleEntityModel.
	 */
	@SuppressWarnings("unchecked")
	public SimpleEntityModel() {
		this.mAnnotationHandler = EntityModelAnnotationHandlers.obtainModelHandler((Class<M>) getClass());
	}

	/**
	 * Should be called form <b>{@code CREATOR}</b> to create an instance of SimpleEntityModel form
	 * the given parcel <var>source</var>.
	 *
	 * @param source Parcel with data for a new model instance.
	 */
	@SuppressWarnings("unchecked")
	protected SimpleEntityModel(@NonNull Parcel source) {
		this.mAnnotationHandler = EntityModelAnnotationHandlers.obtainModelHandler((Class<M>) getClass());
		if (mAnnotationHandler != null && !mAnnotationHandler.handleFromParcel(this, source)) {
			Log.e(getModelName(), "Failed to bind some some columns data from Parcel.");
		}
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns handler that is responsible for annotations processing of this class and also for
	 * handling all annotations related operations for this class.
	 *
	 * @return Annotations handler specific for this class.
	 * @throws DatabaseException If annotations processing is not enabled for the Database library.
	 */
	@NonNull
	protected EntityModelAnnotationHandler<M> getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return mAnnotationHandler;
	}

	/**
	 * Returns name of this simple entity model.
	 *
	 * @return Simple class name of this entity model.
	 */
	private String getModelName() {
		return ((Object) this).getClass().getSimpleName();
	}

	/**
	 * @throws DatabaseException If there is no ParcelableProcessor specified to support this logic.
	 */
	@Override
	@CallSuper
	public void writeToParcel(Parcel dest, int flags) {
		if (mAnnotationHandler != null && !mAnnotationHandler.handleWriteToParcel(this, dest, flags)) {
			Log.e(getModelName(), "Failed to write some columns data into Parcel.");
		}
	}

	/**
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * @throws DatabaseException If annotations processing is disabled for the Database library so
	 * this logic need to be implemented.
	 */
	@NonNull
	@Override
	public ContentValues toContentValues() {
		if (DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) {
			return mAnnotationHandler.handleToContentValues(this);
		}
		final String modelName = getModelName();
		throw DatabaseException.misconfiguration(
				"Failed to transform model(" + modelName + ") into ContentValues. " +
						"Annotations processing is not enabled. Implementation of " + modelName + ".toContentValues() method is required."
		);
	}

	/**
	 * @throws DatabaseException If annotations processing is disabled for the Database library so
	 * this logic need to be implemented.
	 */
	@Override
	@CheckResult
	public boolean fromCursor(@NonNull Cursor cursor) {
		if (DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) {
			return mAnnotationHandler.handleFromCursor(this, cursor);
		}
		final String modelName = getModelName();
		throw DatabaseException.misconfiguration(
				"Failed to bind data of model(" + modelName + ") from cursor. " +
						"Annotations processing is not enabled. Implementation of " + modelName + ".fromCursor(Cursor) method is required."
		);
	}

	/**
	 */
	@NonNull
	@Override
	public Selection createSelection() {
		if (DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) {
			return mAnnotationHandler.handleCreateSelection(this);
		}
		final String modelName = getModelName();
		throw DatabaseException.misconfiguration(
				"Failed to create selection for model(" + modelName + "). " +
						"Annotations processing is not enabled. Implementation of " + modelName + ".createSelection() method is required."
		);
	}

	/**
	 * Creates a <b>shallow!</b> copy of this model. If <b>deep</b> copy is needed, a subclass should
	 * override this method and call {@code super.clone()} to obtain shallow copy and then clone all
	 * nested objects.
	 */
	@NonNull
	@Override
	@SuppressWarnings("unchecked")
	public M clone() {
		try {
			return (M) super.clone();
		} catch (CloneNotSupportedException e) {
			Log.e(getModelName(), "Failed to clone.");
		}
		return (M) this;
	}

	/**
	 * This implementation sets values of all column fields of this model to {@code null}.
	 * <p>
	 * Subclasses may perform additional/custom data recycling logic here.
	 *
	 * @see PoolFactory#recycleModel(EntityModel)
	 */
	@Override
	public void recycle() {
		if (DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) mAnnotationHandler.handleRecycle(this);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
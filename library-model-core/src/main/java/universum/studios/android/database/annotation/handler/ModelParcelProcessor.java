/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.os.Parcel;
import android.text.TextUtils;

import java.lang.reflect.Field;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.database.util.TypeUtils;

/**
 * A {@link EntityModelAnnotationHandler.ParcelProcessor} default implementation that is used by
 * entity model annotation handler (within {@link SimpleEntityModel}) to support both
 * {@link SimpleEntityModel#SimpleEntityModel(Parcel)} unmarshaling constructor and
 * {@link SimpleEntityModel#writeToParcel(Parcel, int)} marshaling method.
 * <p>
 * This processor implementation basically iterates all {@link AnnotatedColumn AnnotatedColumns}
 * passed to he processing method ({@link #processModelIntoParcel(Object, AnnotatedColumn[], Parcel, int)})
 * and obtains current values from all fields hold be these columns and writes them into the specified
 * {@link Parcel} object or restores such model's state by unmarshaling its data from the Parcel object
 * whenever {@link #processModelFromParcel(Object, AnnotatedColumn[], Parcel)} is called for such model.
 *
 * <h3>Column types</h3>
 * Which type of column should be written into Parcel is determined by a type of field from which
 * should be such value obtained. Basically all <b>primitive types</b> are supported including
 * {@link Enum} type of which value is saved as <b>String</b> value that represents a name
 * of a specific instance of a particular Enum type. <b>Boolean</b> value is saved as <b>int</b>
 * (0 = false, 1 = true). Also <b>Parcelable</b> fields are supported.
 * <p>
 * <b>Note, that all column field values are written into the Parcel together with an integer flag
 * declaring what type of value has been stored. This flag is stored into the parcel before the
 * column value. In case of {@code null} values is stored only {@link TypeUtils#NULL} flag without
 * the null value itself.</b>
 *
 * @author Martin Albedinsky
 */
public class ModelParcelProcessor extends ModelProcessor implements EntityModelAnnotationHandler.ParcelProcessor {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ModelParcelProcessor";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	@CheckResult
	public boolean processModelIntoParcel(@NonNull Object model, @NonNull AnnotatedColumn[] columns, @NonNull Parcel destination, int flags) {
		final ParcelTask task = ParcelTask.obtainFor(model, columns, destination, flags);
		final boolean success = onProcessModelIntoParcel(task);
		task.recycle();
		return success;
	}

	/**
	 * Called from {@link #processModelIntoParcel(Object, AnnotatedColumn[], Parcel, int)} to start
	 * processing of the model contained within the given <var>task</var>.
	 *
	 * @param task The task holding data necessary to perform model's processing.
	 * @return {@code True} whenever the task has been executed and all values has been successfully
	 * written into Parcel, {@code false} if at least one column value failed to be written.
	 */
	@CheckResult
	protected boolean onProcessModelIntoParcel(@NonNull ParcelTask task) {
		boolean success = true;
		for (int i = 0; i < task.columns.length; i++) {
			if (!onProcessColumnIntoParcel(task, task.columns[i])) success = false;
		}
		return success;
	}

	/**
	 * Invoked to process the given <var>column</var> to obtain current value of its attached field
	 * so it can be written into the Parcel object contained within the given <var>task</var>.
	 *
	 * @param task   The task holding data necessary for processing of the given column.
	 * @param column Column of the model of which value to write into Parcel.
	 * @return {@code True} if value of the field has been successfully obtained and written into
	 * parcel object, {@code false} if some error has occurred.
	 * @throws DatabaseException If type of field hold by the the specified column is not allowed to
	 *                           represent a column due to its type or that such field is not accessible.
	 */
	@CheckResult
	protected boolean onProcessColumnIntoParcel(@NonNull ParcelTask task, @NonNull AnnotatedColumn column) {
		final Object model = task.model;
		if (!writeValueOfColumn(column, model, task.parcel, task.flags)) {
			throw createFieldNotAllowedAsColumnException(
					model, column.getField(),
					"Failed to write value of column field into Parcel."
			);
		}
		return true;
	}

	/**
	 * Obtains and writes current value of a field hold by the given <var>column</var> into the given
	 * <var>destination</var> parcel as type of the specified column.
	 * <p>
	 * <b>Note</b>, that this will also write one integer flag declaring what type of value has been
	 * stored.
	 *
	 * @param column      The column of which value to obtain and write into the parcel object.
	 * @param model       The model in which is the specified column declared.
	 * @param destination The parcel into which should be the column's current value written.
	 * @param flags       Contextual flags for parcelable column type.
	 * @return {@code True} if the column's value has been successfully obtained and written into
	 * the parcel, {@code false} if some error has occurred or the column's field is not allowed to
	 * represent a column due to its type.
	 * @throws DatabaseException If field of the specified column is not accessible.
	 */
	@CheckResult
	@SuppressWarnings("ConstantConditions")
	protected boolean writeValueOfColumn(@NonNull AnnotatedColumn column, @NonNull Object model, @NonNull Parcel destination, int flags) {
		final Field columnField = column.getField();
		try {
			final Object columnValue = columnField.get(model);
			if (columnValue == null) {
				destination.writeInt(TypeUtils.NULL);
				return true;
			}
			final int columnType = column.getType();
			switch (columnType) {
				case TypeUtils.BYTE:
					destination.writeInt(columnType);
					destination.writeByte((Byte) columnValue);
					return true;
				case TypeUtils.SHORT:
					destination.writeInt(columnType);
					destination.writeInt(((Short) columnValue).intValue());
					return true;
				case TypeUtils.INTEGER:
					destination.writeInt(columnType);
					destination.writeInt((Integer) columnValue);
					return true;
				case TypeUtils.FLOAT:
					destination.writeInt(columnType);
					destination.writeFloat((Float) columnValue);
					return true;
				case TypeUtils.DOUBLE:
					destination.writeInt(columnType);
					destination.writeDouble((Double) columnValue);
					return true;
				case TypeUtils.LONG:
					destination.writeInt(columnType);
					destination.writeLong((Long) columnValue);
					return true;
				case TypeUtils.BOOLEAN:
					destination.writeInt(columnType);
					// Save boolean value as INTEGER.
					destination.writeInt(((Boolean) columnValue) ? 1 : 0);
					return true;
				case TypeUtils.STRING:
					destination.writeInt(columnType);
					destination.writeString((String) columnValue);
					return true;
				case TypeUtils.ENUM:
					destination.writeInt(columnType);
					destination.writeString(((Enum) columnValue).name());
					return true;
				default:
					return false;
			}
		} catch (IllegalAccessException e) {
			final String fieldName = columnField.getName();
			final String modelName = model.getClass().getSimpleName();
			throw DatabaseException.exception("Field(" + modelName + "." + fieldName + ") is not accessible.");
		}
	}

	/**
	 */
	@Override
	@CheckResult
	public boolean processModelFromParcel(@NonNull Object model, @NonNull AnnotatedColumn[] columns, @NonNull Parcel source) {
		final ParcelTask task = ParcelTask.obtainFor(model, columns, source, 0);
		final boolean success = onProcessModelFromParcel(task);
		task.recycle();
		return success;
	}

	/**
	 * Called from {@link #processModelFromParcel(Object, AnnotatedColumn[], Parcel)} to start processing
	 * of the model contained within the given <var>task</var>.
	 *
	 * @param task The task holding data necessary to perform model's processing.
	 * @return {@code True} whenever the task has been executed and all values has been successfully
	 * read from Parcel, {@code false} if at least one column value failed to be read.
	 */
	@CheckResult
	protected boolean onProcessModelFromParcel(@NonNull ParcelTask task) {
		boolean success = true;
		for (int i = 0; i < task.columns.length; i++) {
			if (!onProcessColumnFromParcel(task, task.columns[i])) success = false;
		}
		return success;
	}

	/**
	 * Invoked to process Parcel contained within the given <var>task</var> to read one value from it
	 * and attach it to a corresponding field hold by the given <var>column</var>.
	 *
	 * @param task   The task holding data necessary for processing of the given column.
	 * @param column Column of the model of which value to read from the Parcel.
	 * @return {@code True} if value of the specified column has been successfully obtained and attached
	 * to its corresponding field, {@code false} if some error has occurred.
	 * @throws DatabaseException If type of field hold by the the specified column is not allowed to
	 *                           represent a column due to its type or that such field is not accessible.
	 */
	@CheckResult
	protected boolean onProcessColumnFromParcel(@NonNull ParcelTask task, @NonNull AnnotatedColumn column) {
		final Object model = task.model;
		if (!readValueIntoColumn(column, model, task.parcel)) {
			throw createFieldNotAllowedAsColumnException(
					model, column.getField(),
					"Failed to read value of column field from Parcel."
			);
		}
		return true;
	}

	/**
	 * Reads one value (with its corresponding type flag) from the given <var>source</var> parcel and
	 * attaches it to a field hold by the given <var>column</var>.
	 *
	 * @param column The column to which to set the obtained value.
	 * @param model  The model in which is the specified column declared.
	 * @param source The parcel from which should be the column's value read.
	 * @return {@code True} if obtained value has been successfully attached to the column, {@code false}
	 * if some error has occurred or the column's field is not allowed to represent a column due to
	 * its type.
	 * @throws DatabaseException If field of the specified column is not accessible.
	 */
	@CheckResult
	@SuppressWarnings("unchecked")
	protected boolean readValueIntoColumn(@NonNull AnnotatedColumn column, @NonNull Object model, @NonNull Parcel source) {
		final Field columnField = column.getField();
		try {
			final int columnType = source.readInt();
			if (columnType == TypeUtils.NULL) {
				columnField.set(model, null);
				return true;
			}
			switch (columnType) {
				case TypeUtils.BYTE:
					columnField.set(model, source.readByte());
					return true;
				case TypeUtils.SHORT:
					columnField.set(model, (short) source.readInt());
					return true;
				case TypeUtils.INTEGER:
					columnField.set(model, source.readInt());
					return true;
				case TypeUtils.FLOAT:
					columnField.set(model, source.readFloat());
					return true;
				case TypeUtils.DOUBLE:
					columnField.set(model, source.readDouble());
					return true;
				case TypeUtils.LONG:
					columnField.set(model, source.readLong());
					return true;
				case TypeUtils.BOOLEAN:
					// Read boolean value as INTEGER.
					columnField.set(model, source.readInt() == 1);
					return true;
				case TypeUtils.STRING:
					columnField.set(model, source.readString());
					return true;
				case TypeUtils.ENUM:
					final String enumName = source.readString();
					if (!TextUtils.isEmpty(enumName)) {
						columnField.set(model, Enum.valueOf((Class<? extends Enum>) columnField.getType(), enumName));
					}
					return true;
				default:
					return false;
			}
		} catch (IllegalAccessException e) {
			final String fieldName = columnField.getName();
			final String modelName = model.getClass().getSimpleName();
			throw DatabaseException.exception("Field(" + modelName + "." + fieldName + ") is not accessible.");
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link ProcessorTask} implementation used as task for {@link ModelParcelProcessor}.
	 *
	 * @author Martin Albedinsky
	 */
	protected static final class ParcelTask extends ProcessorTask {

		/**
		 * Size for the pool array containing tasks.
		 */
		static final int POOL_SIZE = 5;

		/**
		 * Array containing already recycled ({@link #recycle()}) tasks that can be obtained via
		 * {@link #obtain()} and thus reused.
		 */
		private static final ParcelTask[] POOL = new ParcelTask[POOL_SIZE];

		/**
		 * Parcel into/from which should be placed/retrieved data of/for the model.
		 */
		protected Parcel parcel;

		/**
		 * Contextual flags for parcelable column types.
		 */
		protected int flags;

		/**
		 * Same as {@link #obtain()} where the specified parameters will be attached to the obtained
		 * task.
		 */
		static ParcelTask obtainFor(Object model, AnnotatedColumn[] columns, Parcel parcel, int flags) {
			final ParcelTask task = ParcelTask.obtain();
			task.model = model;
			task.columns = columns;
			task.parcel = parcel;
			task.flags = flags;
			return task;
		}

		/**
		 * Obtains a new ParcelTask object (if available from the pool).
		 * <p>
		 * When you are done with the obtained task do not forget to recycle it via {@link #recycle()}.
		 *
		 * @return New or recycled ParcelTask.
		 */
		@NonNull
		@SuppressWarnings("AssignmentToNull")
		protected static ParcelTask obtain() {
			synchronized (POOL) {
				ParcelTask task;
				for (int i = 0; i < POOL_SIZE; i++) {
					task = POOL[i];
					if (task != null) {
						POOL[i] = null;
						return task;
					}
				}
			}
			return new ParcelTask();
		}

		/**
		 */
		@Override
		void onRecycleInner() {
			this.parcel = null;
			this.flags = 0;
			synchronized (POOL) {
				for (int i = 0; i < POOL_SIZE; i++) {
					if (POOL[i] == null) {
						POOL[i] = this;
						return;
					}
				}
			}
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import java.lang.reflect.Field;

import universum.studios.android.database.DatabaseException;

/**
 * A ModelProcessor represents a base class for all processors that are used to process an entity model
 * (its inner data structure) in a way specific for that particular type of processor.
 *
 * @author Martin Albedinsky
 */
public abstract class ModelProcessor {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ModelProcessor";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of DatabaseException type of {@link DatabaseException#TYPE_MISCONFIGURATION
	 * with the specified parameters to inform that the specified <var>field</var> is not allowed to
	 * be marked by {@link universum.studios.android.database.annotation.Column @Column} annotation.
	 *
	 * @param model Model of which field is not allowed as column.
	 * @param field The field that is not allowed as column.
	 * @param intro Intro text for the exception. This text will be placed at the beginning of the
	 *              exception's message.
	 * @return DatabaseException type of misconfiguration.
	 */
	static DatabaseException createFieldNotAllowedAsColumnException(Object model, Field field, String intro) {
		final String modelName = model.getClass().getSimpleName();
		final String fieldName = field.getName();
		final String fieldTypeName = field.getType().getSimpleName();
		return DatabaseException.misconfiguration(intro + " " +
						"Field(" + modelName + "." + fieldName + ") type of(" + fieldTypeName + ") " +
						"is not allowed to represent a column. " +
						"Only primitive type fields, Enums including, are allowed as columns."
		);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A ProcessorTask represents a base task for the implementations of {@link ModelProcessor ModelProcessor}.
	 *
	 * @author Martin Albedinsky
	 */
	protected static abstract class ProcessorTask {

		/**
		 * Instance of model that should be processed.
		 */
		protected Object model;

		/**
		 * Array with columns that are annotated within the specified model.
		 */
		protected AnnotatedColumn[] columns;

		/**
		 * Puts this task object back into the pool.
		 * <p>
		 * Do not touch this task after this call.
		 */
		protected final void recycle() {
			this.model = null;
			this.columns = null;
			onRecycleInner();
			onRecycle();
		}

		/**
		 * Invoked for internal purpose from {@link #recycle()} to clear all data assigned to this task.
		 */
		abstract void onRecycleInner();

		/**
		 * Invoked from {@link #recycle()} to clear all data assigned to this task.
		 */
		protected void onRecycle() {
			// Inheritance hierarchies may perform here theirs specific recycling logic.
		}
	}
}
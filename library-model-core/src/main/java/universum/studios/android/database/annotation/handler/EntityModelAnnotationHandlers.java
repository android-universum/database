/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.Model;
import universum.studios.android.database.annotation.Upgrade;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.database.model.SimpleEntityModelSelection;
import universum.studios.android.database.util.ColumnType;
import universum.studios.android.database.util.TableColumn;
import universum.studios.android.database.util.TypeUtils;

/**
 * An {@link AnnotationHandlers} implementation providing {@link AnnotationHandler} instances for
 * entity model related classes.
 *
 * @author Martin Albedinsky
 */
public final class EntityModelAnnotationHandlers extends AnnotationHandlers {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	EntityModelAnnotationHandlers() {
		super();
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains an {@link EntityModelAnnotationHandler} implementation for the given <var>classOfModel</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <M extends EntityModel> EntityModelAnnotationHandler<M> obtainModelHandler(@NonNull Class<M> classOfModel) {
		return obtainHandler(ModelHandler.class, classOfModel);
	}

	/**
	 * Obtains an {@link EntityModelCursorAnnotationHandler} implementation for the given <var>classOfCursor</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <M extends EntityModel> EntityModelCursorAnnotationHandler<M> obtainModelCursorHandler(@NonNull Class<? extends Cursor> classOfCursor) {
		return obtainHandler(ModelCursorHandler.class, classOfCursor);
	}

	/*
	 * Inner classes ===============================================================================
	 */
	/**
	 * An {@link EntityModelAnnotationHandler} implementation for {@link EntityModel} implementations.
	 */
	static final class ModelHandler<M extends EntityModel> extends BaseAnnotationHandler implements EntityModelAnnotationHandler<M> {

		/**
		 * Name of the static field within a specific entity model that can hold that model's factory
		 * instance.
		 */
		private static final String FACTORY_FIELD_NAME = "FACTORY";

		/**
		 * Array with all column fields that are marked by {@link Column @Column} annotation within the
		 * model class related to this handler.
		 */
		private final AnnotatedColumn[] columns;

		/**
		 * Array with all column fields that are marked by {@link Column.Primary @Column.Primary} or
		 * {@link Column.Unique @Column.Unique} annotations within the model class related to this handler.
		 */
		private final AnnotatedColumn[] selectionColumns;

		/**
		 * Factory obtained (if presented) from the model class related to this handler. If available, it
		 * is used to create new instances of the model.
		 *
		 * @see #createModel()
		 */
		private final EntityModel.Factory<M> factory;

		/**
		 * Processor that is used to transform a specific model instance into {@link ContentValues}
		 * object.
		 *
		 * @see #handleToContentValues(Object)
		 */
		private ContentValuesProcessor contentValuesProcessor = ModelProcessors.contentValuesProcessor();

		/**
		 * Processor that is used to bind data of a specific model instance from {@link Cursor}.
		 *
		 * @see #handleFromCursor(Object, Cursor)
		 */
		private CursorProcessor cursorProcessor = ModelProcessors.cursorProcessor();

		/**
		 * Processor that is used to bind/write data of a specific model instance from/into {@link Parcel}.
		 *
		 * @see #handleFromParcel(Object, Parcel)
		 * @see #handleWriteToParcel(Object, Parcel, int)
		 */
		private ParcelProcessor parcelProcessor = ModelProcessors.parcelProcessor();

		/**
		 * Creates a new instance of ModelHandler for the specified <var>annotatedClass</var>
		 * with {@link SimpleEntityModel} as <var>maxSuperClass</var>.
		 *
		 * @see BaseAnnotationHandler#BaseAnnotationHandler(Class, Class)
		 */
		public ModelHandler(@NonNull Class<?> annotatedClass) {
			super(annotatedClass, SimpleEntityModel.class);
			this.factory = findModelFactory(annotatedClass);
			final List<AnnotatedColumn> columns = obtainColumns(annotatedClass);
			this.columns = columnsListToArray(columns);
			this.selectionColumns = findSelectionColumns(columns);
		}

		/**
		 * Searches for the model factory within the specified model <var>annotatedClass</var>.
		 *
		 * @param annotatedClass The class of model that can contain the factory.
		 * @return Model factory or {@code null} if the specified model class does not have its factory
		 * specified.
		 */
		@SuppressWarnings("unchecked")
		private EntityModel.Factory<M> findModelFactory(Class<?> annotatedClass) {
			final String modelName = annotatedClass.getSimpleName();
			try {
				final Field factoryField = annotatedClass.getField(FACTORY_FIELD_NAME);
				if (factoryField == null) {
					throw DatabaseException.exception("Model factory protocol requires the FACTORY object to be static for entity model(" + modelName + ").");
				}
				return (EntityModel.Factory<M>) factoryField.get(null);
			} catch (NoSuchFieldException e) {
				// We did not found factory for this model class.
				return null;
			} catch (IllegalAccessException e) {
				throw DatabaseException.exception("IllegalAccessException when obtaining FACTORY for entity model(" + modelName + ").");
			} catch (ClassCastException e) {
				throw DatabaseException.exception("Model factory protocol supports only EntityModel.Factory type as FACTORY for entity model(" + modelName + ").");
			}
		}

		/**
		 * Iterates and obtains all column fields that are marked by {@link Column @Column} annotation
		 * within the specified <var>classOfModel</var>.
		 * <p>
		 * The fields will be iterated recursively along the super classes chain of the given model
		 * class max to {@link #mMaxSuperClass}
		 *
		 * @param classOfModel Class of the model of which column fields to iterate and obtain.
		 * @return List with all column fields wrapped into AnnotatedColumn interface.
		 */
		private List<AnnotatedColumn> obtainColumns(Class<?> classOfModel) {
			final List<AnnotatedColumn> columns = new ArrayList<>();
			DatabaseAnnotations.iterateFields(new DatabaseAnnotations.FieldProcessor() {

				/**
				 */
				@Override
				public void onProcessField(@NonNull Field field, @NonNull String name) {
					final Column column = field.getAnnotation(Column.class);
					if (column == null) {
						return;
					}
					field.setAccessible(true);
					final ModelColumnBuilder builder = new ModelColumnBuilder(field, column.value())
							.nullable(column.nullable())
							.primary(field.isAnnotationPresent(Column.Primary.class))
							.unique(field.isAnnotationPresent(Column.Unique.class))
							.foreign(field.isAnnotationPresent(Column.Foreign.class))
							.joined(field.isAnnotationPresent(Column.Joined.class))
							.deprecated(field.isAnnotationPresent(Deprecated.class));
					final Upgrade upgrade = field.getAnnotation(Upgrade.class);
					if (upgrade != null) {
						builder.upgradeVersion(upgrade.version());
					}
					columns.add(builder.build());
				}
			}, classOfModel, mMaxSuperClass);
			return columns;
		}

		/**
		 * Performs searching for columns that are marked by {@link Column.Primary @Column.Primary} or
		 * {@link Column.Unique @Column.Unique} annotation within the specified list of columns using
		 * {@link AnnotatedColumn#isPrimary()} and {@link AnnotatedColumn#isUnique()} flags.
		 *
		 * @param annotatedColumns The list of model's annotated columns where to perform search.
		 * @return Array with selection columns or empty array if there are no such columns presented.
		 */
		private AnnotatedColumn[] findSelectionColumns(List<AnnotatedColumn> annotatedColumns) {
			final List<AnnotatedColumn> selectionColumns = new ArrayList<>(1);
			for (final AnnotatedColumn column : annotatedColumns) {
				if (column.isPrimary() || column.isUnique()) selectionColumns.add(column);
			}
			return columnsListToArray(selectionColumns);
		}

		/**
		 * Transforms the specified <var>annotatedColumns</var> list into array of same length.
		 *
		 * @param annotatedColumns The list with columns to be transformed.
		 * @return Array with columns of same length as the given list.
		 */
		private AnnotatedColumn[] columnsListToArray(List<AnnotatedColumn> annotatedColumns) {
			final AnnotatedColumn[] columns = new AnnotatedColumn[annotatedColumns.size()];
			annotatedColumns.toArray(columns);
			return columns;
		}

		/**
		 */
		@NonNull
		@Override
		public AnnotatedColumn[] getColumns() {
			return columns;
		}

		/**
		 */
		@NonNull
		@Override
		public M createModel() {
			return factory == null ? createModelInner() : factory.createModel();
		}

		/**
		 * Instantiates entity model from the class obtained from @Model annotation via reflection.
		 *
		 * @return Instantiated model.
		 * @throws DatabaseException If instantiation of model failed.
		 */
		@SuppressWarnings("unchecked")
		private M createModelInner() {
			try {
				return (M) mAnnotatedClass.getConstructor().newInstance();
			} catch (Exception e) {
				throw DatabaseException.instantiation("ENTITY MODEL", mAnnotatedClass, e);
			}
		}

		/**
		 */
		@Override
		public void setContentValuesProcessor(@Nullable ContentValuesProcessor processor) {
			this.contentValuesProcessor = processor == null ? ModelProcessors.contentValuesProcessor() : processor;
		}

		/**
		 */
		@NonNull
		@Override
		public ContentValues handleToContentValues(@NonNull Object model) {
			this.ensureRelatedModelOrThrow(model);
			return columns.length == 0 ? new ContentValues() : contentValuesProcessor.processModel(model, columns);
		}

		/**
		 */
		@Override
		public void setCursorProcessor(@Nullable CursorProcessor processor) {
			this.cursorProcessor = processor == null ? ModelProcessors.cursorProcessor() : processor;
		}

		/**
		 */
		@Override
		public boolean handleFromCursor(@NonNull Object model, @NonNull Cursor cursor) {
			this.ensureRelatedModelOrThrow(model);
			return columns.length == 0 || cursorProcessor.processModel(model, columns, cursor);
		}

		/**
		 */
		@Override
		public void setParcelProcessor(@Nullable ParcelProcessor processor) {
			this.parcelProcessor = processor == null ? ModelProcessors.parcelProcessor() : processor;
		}

		/**
		 */
		@Override
		public boolean handleWriteToParcel(@NonNull Object model, @NonNull Parcel destination, int flags) {
			this.ensureRelatedModelOrThrow(model);
			return columns.length == 0 || parcelProcessor.processModelIntoParcel(model, columns, destination, flags);
		}

		/**
		 */
		@Override
		public boolean handleFromParcel(@NonNull Object model, @NonNull Parcel source) {
			this.ensureRelatedModelOrThrow(model);
			return columns.length == 0 || parcelProcessor.processModelFromParcel(model, columns, source);
		}

		/**
		 */
		@NonNull
		@Override
		public String[] handleCreateProjection() {
			if (columns.length == 0){
				return new String[0];
			}
			final String[] projection = new String[columns.length];
			for (int i = 0; i < columns.length; i++) {
				projection[i] = columns[i].getName();
			}
			return projection;
		}

		/**
		 */
		@NonNull
		@Override
		public EntityModel.Selection handleCreateSelection(@NonNull Object model) {
			this.ensureRelatedModelOrThrow(model);
			if (selectionColumns.length == 0) {
				return EntityModel.EMPTY_SELECTION;
			}
			// Optimize for models with only one selection column, i.e. '_id'.
			if (selectionColumns.length == 1) {
				final AnnotatedColumn column = selectionColumns[0];
				final Object fieldValue = obtainFieldValue(column.getField(), model);
				return fieldValue == null ?
						EntityModel.EMPTY_SELECTION :
						new SimpleEntityModelSelection(
								column.getName() + "=?",
								new String[]{String.valueOf(fieldValue)}
						);
			}
			// Iterate through selection columns and build up statement along with its corresponding
			// arguments for those of which value is not null.
			final StringBuilder statementBuilder = new StringBuilder(selectionColumns.length * 10);
			final List<String> argsList = new ArrayList<>(selectionColumns.length);
			for (final AnnotatedColumn column : selectionColumns) {
				final Object fieldValue = obtainFieldValue(column.getField(), model);
				if (fieldValue == null) {
					continue;
				}
				if (statementBuilder.length() > 0) {
					statementBuilder.append(" AND ");
				}
				statementBuilder.append(column.getName());
				statementBuilder.append("=?");
				argsList.add(String.valueOf(fieldValue));
			}
			if (argsList.isEmpty()) {
				return EntityModel.EMPTY_SELECTION;
			}
			final String[] args = new String[argsList.size()];
			argsList.toArray(args);
			return new SimpleEntityModelSelection(statementBuilder.toString(), args);
		}

		/**
		 * Obtains current value from the specified <var>field</var>.
		 *
		 * @param field The field of which current value to obtain.
		 * @param model Model where is the specified field declared and which holds the current field's
		 *              value.
		 * @return Obtained value or {@code null} if the value failed to be obtained.
		 */
		private static Object obtainFieldValue(Field field, Object model) {
			try {
				return field.get(model);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 */
		@Override
		public void handleRecycle(@NonNull Object model) {
			this.ensureRelatedModelOrThrow(model);
			if (columns.length > 0) {
				try {
					for (final AnnotatedColumn column : columns) {
						column.getField().set(model, null);
					}
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		/**
		 * Ensures that the specified model is class of the model for which is this handler created.
		 *
		 * @param model The model to be checked.
		 * @throws DatabaseException If the specified model is not of the same class as the attached one.
		 */
		private void ensureRelatedModelOrThrow(Object model) {
			if (mAnnotatedClass.equals(model.getClass())) return;
			final String relatedModelName = mAnnotatedClass.getSimpleName();
			final String unrelatedModelName = model.getClass().getSimpleName();
			throw DatabaseException.exception("Trying to use annotations handler of model(" + relatedModelName + ") for different model(" + unrelatedModelName + ")!");
		}
	}

	/**
	 * An {@link EntityModelCursorAnnotationHandler} implementation for model cursors.
	 */
	@SuppressWarnings("WeakerAccess") static final class ModelCursorHandler<M extends EntityModel> extends BaseAnnotationHandler implements EntityModelCursorAnnotationHandler<M> {

		/**
		 * Annotations handler of the related cursor's model.
		 */
		private final EntityModelAnnotationHandler<M> mModelHandler;

		/**
		 * Creates a new instance of ModelCursorHandler for the specified <var>annotatedClass</var>.
		 *
		 * @see BaseAnnotationHandler#BaseAnnotationHandler(Class, Class)
		 */
		@SuppressWarnings("unchecked")
		public ModelCursorHandler(Class<?> annotatedClass) {
			super(annotatedClass, null);
			final Model model = findAnnotation(Model.class);
			this.mModelHandler = model == null ? null : EntityModelAnnotationHandlers.obtainModelHandler((Class<M>) model.value());
		}

		/**
		 */
		@NonNull
		@Override
		public M handleCreateModel() {
			if (mModelHandler == null) throw DatabaseException.missingClassAnnotation(Model.class, mAnnotatedClass);
			return mModelHandler.createModel();
		}
	}

	/**
	 * An {@link AnnotatedColumn} implementation to wrap data of annotated column field.
	 */
	private static final class ModelColumn implements AnnotatedColumn {

		/**
		 * Constant used to determine that no upgrade version has been specified for a specific
		 * model column field.
		 */
		static final int NO_VERSION = -1;

		/**
		 * Flag indicating that a {@code NULL} value of a specific model column field can be inserted
		 * into database table.
		 */
		static final int PFLAG_NULLABLE = 0x00000001;

		/**
		 * Flag indicating that a specific model column field is marked by
		 * {@link Column.Primary @Column.Primary} annotation.
		 */
		static final int PFLAG_PRIMARY = 0x00000001 << 1;

		/**
		 * Flag indicating that a specific model column field is marked by
		 * {@link Column.Unique @Column.Unique} annotation.
		 */
		static final int PFLAG_UNIQUE = 0x00000001 << 2;

		/**
		 * Flag indicating that a specific model column field is marked by
		 * {@link Column.Foreign @Column.Foreign} annotation.
		 */
		static final int PFLAG_FOREIGN = 0x00000001 << 3;

		/**
		 * Flag indicating that a specific model column field is marked by
		 * {@link Column.Joined @Column.Joined} annotation.
		 */
		static final int PFLAG_JOINED = 0x00000001 << 4;

		/**
		 * Flag indicating that a specific model column field is marked by
		 * {@link Deprecated @Deprecated} annotation.
		 */
		static final int PFLAG_DEPRECATED = 0x00000001 << 5;

		/**
		 * Field that is marked by {@link Column @Column} annotation for which is this wrapper created.
		 */
		final Field field;

		/**
		 * Name of this column as it will be presented within database table.
		 */
		final String name;

		/**
		 * Flag determining type of this column.
		 *
		 * @see ColumnType
		 */
		final int type;

		/**
		 * Version at which should be this column upgraded.
		 */
		final int upgradeVersion;

		/**
		 * Set of private flags for this column.
		 */
		final int privateFlags;

		/**
		 * Creates a new instance of ModelColumn with data supplied by the specified <var>builder</var>.
		 *
		 * @param builder The builder with data for the new ModelColumn.
		 */
		ModelColumn(ModelColumnBuilder builder) {
			this.field = builder.field;
			this.type = builder.type;
			this.name = builder.name;
			this.upgradeVersion = builder.upgradeVersion;
			this.privateFlags = builder.privateFlags;
		}

		/**
		 */
		@NonNull
		@Override
		public String getName() {
			return name;
		}

		/**
		 */
		@Override
		@TypeUtils.Type
		public int getType() {
			return type;
		}

		/**
		 */
		@NonNull
		@Override
		public Field getField() {
			return field;
		}

		/**
		 */
		@Override
		public boolean requiresUpgrade(int oldVersion, int newVersion) {
			return oldVersion < upgradeVersion && upgradeVersion <= newVersion;
		}

		/**
		 */
		@Override
		public boolean isNullable() {
			return (privateFlags & PFLAG_NULLABLE) == PFLAG_NULLABLE;
		}

		/**
		 */
		@Override
		public boolean isPrimary() {
			return (privateFlags & PFLAG_PRIMARY) == PFLAG_PRIMARY;
		}

		/**
		 */
		@Override
		public boolean isUnique() {
			return (privateFlags & PFLAG_UNIQUE) == PFLAG_UNIQUE;
		}

		/**
		 */
		@Override
		public boolean isForeign() {
			return (privateFlags & PFLAG_FOREIGN) == PFLAG_FOREIGN;
		}

		/**
		 */
		@Override
		public boolean isJoined() {
			return (privateFlags & PFLAG_JOINED) == PFLAG_JOINED;
		}

		/**
		 */
		@Override
		public boolean isDeprecated() {
			return (privateFlags & PFLAG_DEPRECATED) == PFLAG_DEPRECATED;
		}
	}

	/**
	 * Builder that can be used to build a single {@link ModelColumn} based on the supplied column
	 * data.
	 */
	private static final class ModelColumnBuilder {

		/**
		 * See {@link ModelColumn#field}.
		 */
		final Field field;

		/**
		 * See {@link ModelColumn#type}.
		 */
		final int type;

		/**
		 * See {@link ModelColumn#name}.
		 */
		final String name;

		/**
		 * See {@link ModelColumn#upgradeVersion}.
		 */
		int upgradeVersion = ModelColumn.NO_VERSION;

		/**
		 * See {@link ModelColumn#privateFlags}.
		 */
		int privateFlags;

		/**
		 * Creates a new instance of ModelColumnBuilder.
		 *
		 * @param field         Field for which to build a new ModelColumn.
		 * @param annotatedName Column name that is presented by {@link Column#value()} attribute.
		 */
		ModelColumnBuilder(Field field, String annotatedName) {
			this.field = field;
			this.type = TypeUtils.resolveType(field.getType());
			this.name = TableColumn.resolveColumnNameForField(field.getName(), annotatedName);
		}

		/**
		 * Specifies an upgrade version for model column.
		 *
		 * @param version The column upgrade version used to determine whether such column need to
		 *                be upgraded according to the current version of database or not.
		 * @return This builder to allow methods chaining.
		 */
		ModelColumnBuilder upgradeVersion(int version) {
			this.upgradeVersion = version;
			return this;
		}

		/**
		 * Specifies a boolean flag indicating whether a {@code NULL} value of model column field
		 * can be inserted into database table or not.
		 *
		 * @param nullable {@code True} if {@code null} should be treated as valid value, {@code false}
		 *                 otherwise.
		 * @return This builder to allow methods chaining.
		 */
		ModelColumnBuilder nullable(boolean nullable) {
			updatePrivateFlags(ModelColumn.PFLAG_NULLABLE, nullable);
			return this;
		}

		/**
		 * Specifies a boolean flag indicating whether model column should represent a primary column
		 * within database table or not.
		 *
		 * @param primary {@code True} to treat the column as primary, {@code false} otherwise.
		 * @return This builder to allow methods chaining.
		 */
		ModelColumnBuilder primary(boolean primary) {
			updatePrivateFlags(ModelColumn.PFLAG_PRIMARY, primary);
			return this;
		}

		/**
		 * Specifies a boolean flag indicating whether model column should represent a unique column
		 * within database table or not.
		 *
		 * @param unique {@code True} to treat the column as unique, {@code false} otherwise.
		 * @return This builder to allow methods chaining.
		 */
		ModelColumnBuilder unique(boolean unique) {
			updatePrivateFlags(ModelColumn.PFLAG_UNIQUE, unique);
			return this;
		}

		/**
		 * Specifies a boolean flag indicating whether model column should represent a foreign column
		 * within database table or not.
		 *
		 * @param foreign {@code True} to treat the column as foreign, {@code false} otherwise.
		 * @return This builder to allow methods chaining.
		 */
		ModelColumnBuilder foreign(boolean foreign) {
			updatePrivateFlags(ModelColumn.PFLAG_FOREIGN, foreign);
			return this;
		}

		/**
		 * Specifies a boolean flag indicating whether model column should be treated as joined column.
		 * Joined columns are not included into database table structure, but are used only for projection
		 * purpose.
		 *
		 * @param joined {@code True} to treat the column as joined, {@code false} otherwise.
		 * @return This builder to allow methods chaining.
		 */
		ModelColumnBuilder joined(boolean joined) {
			updatePrivateFlags(ModelColumn.PFLAG_JOINED, joined);
			return this;
		}

		/**
		 * Specifies a boolean flag indicating whether model column should not be presented in database
		 * table's structure. Deprecated columns are simply not used when creating data structure for
		 * associated database table.
		 *
		 * @param deprecated {@code True} to treat the column as deprecated, {@code false} otherwise.
		 * @return This builder to allow methods chaining.
		 */
		ModelColumnBuilder deprecated(boolean deprecated) {
			updatePrivateFlags(ModelColumn.PFLAG_DEPRECATED, deprecated);
			return this;
		}

		/**
		 * Updates the current private flags.
		 *
		 * @param flag Value of the desired flag to add/remove to/from the current private flags.
		 * @param add  Boolean flag indicating whether to add or remove the specified <var>flag</var>.
		 */
		private void updatePrivateFlags(int flag, boolean add) {
			if (add) this.privateFlags |= flag;
			else this.privateFlags &= ~flag;
		}

		/**
		 * Builds a new AnnotatedColumn from the current data.
		 *
		 * @return New instance of AnnotatedColumn implementation.
		 */
		AnnotatedColumn build() {
			return new ModelColumn(this);
		}
	}
}
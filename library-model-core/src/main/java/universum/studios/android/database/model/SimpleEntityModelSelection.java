/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import androidx.annotation.Nullable;

/**
 * A {@link EntityModel.Selection} simple implementation used to capture selection state of a specific
 * {@link EntityModel} instance. Instances may be created via {@link #SimpleEntityModelSelection(String, String[])}
 * constructor.
 *
 * @author Martin Albedinsky
 */
public class SimpleEntityModelSelection implements EntityModel.Selection {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SimpleEntityModelSelection";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Statement value supplied for this selection.
	 */
	private final String mValue;

	/**
	 * Arguments supplied for statement of this selection.
	 */
	private final String[] mArguments;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SimpleEntityModelSelection with the specified <var>value</var> and
	 * <var>arguments</var>.
	 *
	 * @param value     Statement value of the new selection.
	 * @param arguments Array with arguments for statement of the new selection.
	 */
	public SimpleEntityModelSelection(@Nullable String value, @Nullable String[] arguments) {
		this.mValue = value;
		this.mArguments = arguments;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Nullable
	@Override
	public String getValue() {
		return mValue;
	}

	/**
	 */
	@Nullable
	@Override
	public String[] getArguments() {
		return mArguments;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
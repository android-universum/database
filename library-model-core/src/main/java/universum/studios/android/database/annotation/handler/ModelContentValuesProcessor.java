/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.util.Log;

import java.lang.reflect.Field;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.database.util.TypeUtils;

/**
 * A {@link EntityModelAnnotationHandler.ContentValuesProcessor} default implementation that is used
 * by entity model annotation handler (within {@link SimpleEntityModel}) to support
 * {@link EntityModel#toContentValues()} method.
 * <p>
 * This processor implementation basically iterates all {@link AnnotatedColumn AnnotatedColumns}
 * passed to the processing method ({@link #processModel(Object, AnnotatedColumn[])}) and obtains
 * current values from all fields hold be these columns and inserts them into {@link ContentValues}
 * object for the specified <b>model</b>.
 *
 * <h3>Column types</h3>
 * Which type of column should be inserted into ContentValues is determined by a type of field from
 * which should be such value obtained. Basically all <b>primitive types</b> are supported including
 * {@link Enum} type of which value is saved as <b>String</b> value that represents a name
 * of a specific instance of a particular Enum type. <b>Boolean</b> value is saved as <b>Integer</b>
 * (0 = false, 1 = true).
 *
 * <h3>Nullable columns</h3>
 * If a specific column is <b>not-nullable</b> ({@link Column#nullable()}) only its <b>not-null</b>
 * value will be put into ContentValues object. Null value of columns that explicitly requested
 * (via {@link Column#nullable()}) that such value can be inserted into database, will be put into
 * ContentValues object via {@link ContentValues#putNull(String)}.
 *
 * <h3>Joined columns</h3>
 * Columns that are marked by {@link Column.Joined @Column.Joined} annotation are not included into
 * processing as such columns are meant to be used only for projection purpose.
 *
 * @author Martin Albedinsky
 */
public class ModelContentValuesProcessor extends ModelProcessor implements EntityModelAnnotationHandler.ContentValuesProcessor {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "ModelContentValuesProcessor";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@NonNull
	@Override
	@CheckResult
	public ContentValues processModel(@NonNull Object model, @NonNull AnnotatedColumn[] columns) {
		final ValuesTask task = ValuesTask.obtainFor(model, columns);
		final ContentValues values = onProcessModel(task);
		task.recycle();
		return values;
	}

	/**
	 * Called from {@link #processModel(Object, AnnotatedColumn[])} to start processing of the model
	 * contained within the given <var>task</var>.
	 *
	 * @param task The task holding data necessary to perform model's processing.
	 * @return ContentValues object with current column values of the model mapped to their column
	 * names.
	 */
	@NonNull
	@SuppressLint("LongLogTag")
	protected ContentValues onProcessModel(@NonNull ValuesTask task) {
		final String modelName = task.model.getClass().getSimpleName();
		for (int i = 0; i < task.columns.length; i++) {
			if (!onProcessColumn(task, task.columns[i])) {
				final String fieldName = task.columns[i].getField().getName();
				Log.e(TAG, "Failed to insert value of column field(" + modelName + "." + fieldName + ") into ContentValues.");
			}
		}
		return task.contentValues;
	}

	/**
	 * Invoked to process the given <var>column</var> to obtain current value of its attached field
	 * so it can be inserted into ContentValues object contained within the given <var>task</var>.
	 *
	 * @param task   The task holding data necessary for processing of the given column.
	 * @param column Column of the model of which value to insert into ContentValues.
	 * @return {@code True} if value of the field has been successfully obtained and inserted into
	 * content values object, {@code false} if some error has occurred.
	 * @throws DatabaseException If type of field hold by the the specified column is not allowed to
	 *                           represent a column due to its type or that such field is not accessible.
	 */
	@CheckResult
	protected boolean onProcessColumn(@NonNull ValuesTask task, @NonNull AnnotatedColumn column) {
		// Do not process joined columns. Such columns are used only for projection purpose.
		if (column.isJoined()) {
			return true;
		}
		// Do not process deprecated column.
		if (column.isDeprecated()) {
			return true;
		}
		final Object model = task.model;
		final Field field = column.getField();
		// Check if column can be saved with null value.
		if ((column.isNullable() || !isFieldValueNull(field, model)) && !putValueOfColumn(column, model, task.contentValues, column.getName())) {
			throw createFieldNotAllowedAsColumnException(
					model, field,
					"Failed to insert value of column field into ContentValues."
			);
		}
		return true;
	}

	/**
	 * Obtains and inserts current value of a field hold by the given <var>column</var> into the given
	 * content <var>values</var> object under the specified <var>key</var>.
	 *
	 * @param column The column of which value to obtain and insert into the content values object.
	 * @param model  The model in which is the specified column declared.
	 * @param values The ContentValues object into which should be the column's current value inserted.
	 * @param key    The key under which should be inserted the obtained value within ContentValues.
	 * @return {@code True} if the column's value has been successfully obtained and inserted into
	 * the content values object, {@code false} if some error has occurred or the column's field is
	 * not allowed to represent a column due to its type.
	 * @throws DatabaseException If field of the specified column is not accessible.
	 */
	@CheckResult
	protected boolean putValueOfColumn(@NonNull AnnotatedColumn column, @NonNull Object model, @NonNull ContentValues values, @NonNull String key) {
		final Field columnField = column.getField();
		try {
			final Object columnValue = columnField.get(model);
			if (columnValue == null) {
				values.putNull(key);
				return true;
			}
			switch (column.getType()) {
				case TypeUtils.BYTE:
					values.put(key, (Byte) columnValue);
					return true;
				case TypeUtils.SHORT:
					values.put(key, (Short) columnValue);
					return true;
				case TypeUtils.INTEGER:
					values.put(key, (Integer) columnValue);
					return true;
				case TypeUtils.LONG:
					values.put(key, (Long) columnValue);
					return true;
				case TypeUtils.FLOAT:
					values.put(key, (Float) columnValue);
					return true;
				case TypeUtils.DOUBLE:
					values.put(key, (Double) columnValue);
					return true;
				case TypeUtils.BOOLEAN:
					// Save boolean value as INTEGER.
					values.put(key, ((Boolean) columnValue) ? 1 : 0);
					return true;
				case TypeUtils.STRING:
					values.put(key, (String) columnValue);
					return true;
				case TypeUtils.ENUM:
					values.put(key, ((Enum) columnValue).name());
					return true;
				default:
					return false;
			}
		} catch (IllegalAccessException e) {
			final String fieldName = columnField.getName();
			final String modelName = model.getClass().getSimpleName();
			throw DatabaseException.exception("Field(" + modelName + "." + fieldName + ") is not accessible.");
		}
	}

	/**
	 * Checks whether the given <var>field</var> holds some value or {@code null} instead.
	 *
	 * @param field The field to check for {@code null} value.
	 * @param model The model in which is the specified field declared.
	 * @return {@code True} if value stored within the given field is {@code null}, {@code false} otherwise.
	 * @throws DatabaseException If the given field is not accessible.
	 */
	@CheckResult
	protected static boolean isFieldValueNull(@NonNull Field field, @NonNull Object model) {
		try {
			return field.get(model) == null;
		} catch (IllegalAccessException e) {
			final String fieldName = field.getName();
			final String modelName = model.getClass().getSimpleName();
			throw DatabaseException.exception("Field(" + modelName + "." + fieldName + ") is not accessible.");
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link ProcessorTask} implementation used as task for {@link ModelContentValuesProcessor}.
	 *
	 * @author Martin Albedinsky
	 */
	protected static final class ValuesTask extends ProcessorTask {

		/**
		 * Size for the pool array containing tasks.
		 */
		static final int POOL_SIZE = 10;

		/**
		 * Array containing already recycled ({@link #recycle()}) tasks that can be obtained via
		 * {@link #obtain(int)} and thus reused.
		 */
		private static final ValuesTask[] POOL = new ValuesTask[POOL_SIZE];

		/**
		 * ContentValues object into which should be inserted values obtained form the annotated columns.
		 */
		protected ContentValues contentValues;

		/**
		 * Creates a new instance of ValuesTask with initialized ContentValues object.
		 */
		protected ValuesTask(int initialValuesSize) {
			super();
			this.contentValues = new ContentValues(initialValuesSize);
		}

		/**
		 * Same as {@link #obtain(int)} where the specified parameters will be attached to the obtained
		 * task.
		 */
		static ValuesTask obtainFor(Object model, AnnotatedColumn[] columns) {
			final ValuesTask task = ValuesTask.obtain(columns.length);
			task.model = model;
			task.columns = columns;
			return task;
		}

		/**
		 * Obtains a new ValuesTask object (if available from the pool).
		 * <p>
		 * When you are done with the obtained task do not forget to recycle it via {@link #recycle()}.
		 *
		 * @param initialValuesSize Initial size for the {@link ContentValues} object.
		 *
		 * @return New or recycled ValuesTask.
		 * @see ContentValues#ContentValues(int)
		 */
		@NonNull
		protected static ValuesTask obtain(int initialValuesSize) {
			synchronized (POOL) {
				ValuesTask task;
				for (int i = 0; i < POOL_SIZE; i++) {
					task = POOL[i];
					if (task != null) {
						POOL[i] = null;
						// experiment: obtain values from the pool
						task.contentValues = new ContentValues(initialValuesSize);
						return task;
					}
				}
			}
			return new ValuesTask(initialValuesSize);
		}

		/**
		 */
		@Override
		void onRecycleInner() {
			this.contentValues = null;
			synchronized (POOL) {
				for (int i = 0; i < POOL_SIZE; i++) {
					if (POOL[i] == null) {
						POOL[i] = this;
						return;
					}
				}
			}
		}
	}
}
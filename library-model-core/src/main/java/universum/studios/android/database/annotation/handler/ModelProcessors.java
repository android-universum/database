/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;

import androidx.annotation.NonNull;

/**
 * Factory providing default implementations of entity model processors.
 *
 * @author Martin Albedinsky
 */
public final class ModelProcessors {

	/**
	 * Default instance of entity model processor used to process model into {@link ContentValues} object.
	 */
	private static volatile EntityModelAnnotationHandler.ContentValuesProcessor sContentValuesProcessor;

	/**
	 * Default instance of entity model processor used to process model from {@link Cursor} object.
	 */
	private static volatile EntityModelAnnotationHandler.CursorProcessor sCursorProcessor;

	/**
	 * Default instance of entity model processor used to process model into/from {@link Parcel} object.
	 */
	private static volatile EntityModelAnnotationHandler.ParcelProcessor sParcelProcessor;

	/**
	 */
	private ModelProcessors() {
		// Creation of instances of this class is not publicly allowed.
	}

	/**
	 * Returns an instance of default ContentValuesProcessor implementation.
	 *
	 * @return New or cached instance of ContentValuesProcessor that can be used to process a particular
	 * entity model into {@link ContentValues} object.
	 */
	@NonNull
	public static EntityModelAnnotationHandler.ContentValuesProcessor contentValuesProcessor() {
		return sContentValuesProcessor == null ?
				(sContentValuesProcessor = new ModelContentValuesProcessor()) :
				sContentValuesProcessor;
	}

	/**
	 * Returns an instance of default CursorProcessor implementation.
	 *
	 * @return New or cached instance of CursorProcessor that can be used to process a {@link Cursor}
	 * to populate a particular entity model.
	 */
	@NonNull
	public static EntityModelAnnotationHandler.CursorProcessor cursorProcessor() {
		return sCursorProcessor == null ?
				(sCursorProcessor = new ModelCursorProcessor()) :
				sCursorProcessor;
	}

	/**
	 * Returns an instance of default ParcelProcessor implementation.
	 *
	 * @return New or cached instance of ParcelProcessor that can be used to process a particular
	 * entity model into/from {@link Parcel} object.
	 */
	@NonNull
	public static EntityModelAnnotationHandler.ParcelProcessor parcelProcessor() {
		return sParcelProcessor == null ?
				(sParcelProcessor = new ModelParcelProcessor()) :
				sParcelProcessor;
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.Cursor;

import androidx.annotation.NonNull;
import universum.studios.android.database.model.EntityModel;

/**
 * An {@link AnnotationHandler} extended interface for annotation handlers from the Database library
 * that are used to handle processing of annotations attached to classes derived from {@link Cursor}
 * class which uses entity model to provide theirs data structure.
 *
 * @param <M> Type of the model associated with the annotated model cursor.
 * @author Martin Albedinsky
 */
public interface EntityModelCursorAnnotationHandler<M extends EntityModel> extends AnnotationHandler {

	/**
	 * Handles creation of an entity model instance specific for the cursor for which is this handler
	 * created.
	 *
	 * @return New instance of the requested model.
	 * @see EntityModelAnnotationHandler#createModel()
	 */
	@NonNull
	M handleCreateModel();
}
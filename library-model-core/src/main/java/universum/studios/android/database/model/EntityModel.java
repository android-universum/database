/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.concurrent.atomic.AtomicInteger;

import androidx.annotation.CheckResult;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Interface that specifies API for objects that are used to represent models for a specific data
 * structures.
 * <p>
 * A particular implementation of EntityModel can be used either to store its current data within
 * a database through {@link ContentValues} that may be obtained via {@link #toContentValues()}
 * or to bind its data from a database through {@link Cursor} via {@link #fromCursor(Cursor)}.
 * <p>
 * A specific row where are stored data for a particular model instance may be targeted by that
 * model's current selection which may be obtained via {@link #createSelection()}. The model's
 * selection should be created in a way in which it properly reflects current state of the model's
 * <b>primary</b> and <b>unique</b> columns which should be combined in order to target the model's
 * database row precisely.
 * <p>
 * By default each EntityModel implementation should support at least shallow copying via {@link #clone()}.
 *
 * @author Martin Albedinsky
 */
public interface EntityModel extends Cloneable {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant that identifies invalid/unspecified id.
	 */
	long NO_ID = -1;

	/**
	 * Selection implementation that should be used in case when a specific {@link EntityModel} instance
	 * does not have any <b>primary</b> nor <b>unique</b> column fields as return value for
	 * {@link #createSelection()}. If current values of those fields at time of call to {@link #createSelection()}
	 * are actually {@code null} this selection instance should be used as  return value of that method.
	 * <p>
	 * Also this selection may be used to target all rows within a desired database table.
	 */
	Selection EMPTY_SELECTION = new Selection() {

		/**
		 */
		@Nullable
		@Override
		public String getValue() {
			return null;
		}

		/**
		 */
		@Nullable
		@Override
		public String[] getArguments() {
			return null;
		}
	};

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Interface for factories that may be used to create a specific instance of {@link EntityModel}.
	 * A new instance of the desired model may be created via {@link #createModel()}.
	 *
	 * @param <M> Type of the entity model that this factory can create instance of.
	 * @author Martin Albedinsky
	 */
	interface Factory<M extends EntityModel> {

		/**
		 * Creates a new instance of EntityModel type of specific for this factory.
		 *
		 * @return New entity model instance.
		 */
		@NonNull
		M createModel();
	}

	/**
	 * A {@link Factory} extension that may be used for factories that should provide creation of
	 * entity model with a data bound from the provided {@link Cursor}. A new instance with the data
	 * may be created via {@link #createModel(Cursor)}.
	 *
	 * @author Martin Albedinsky
	 */
	interface CursorFactory<M extends EntityModel> extends Factory<M> {

		/**
		 * Creates and binds a new model instance with data obtained from the given <var>cursor</var>
		 * at its current position.
		 *
		 * @param cursor The desired cursor from which to bind data to the new model.
		 * @return New entity model instance with data or {@code null} if data bounding has failed.
		 * @see EntityModel#fromCursor(Cursor)
		 */
		@Nullable
		M createModel(@NonNull Cursor cursor);
	}

	/**
	 * A {@link Factory} implementation that may be used to implement a model factory backed with
	 * <b>Pool</b> pattern that supports re-using of recycled models.
	 * <p>
	 * Each pool factory is created with a fixed capacity of pool via {@link #PoolFactory(int)}
	 * constructor. A particular instance of entity model may be put into the pool via
	 * {@link #recycleModel(EntityModel)}. Already recycled models may be obtained and re-used via
	 * {@link #obtainModel()}. The capacity and current size of the factory's pool may be obtained
	 * via {@link #getPoolCapacity()} and {@link #getPoolSize()} respectively.
	 * <p>
	 * <b>Note</b>, that the pool factory will provide recycled models only if there are some. If
	 * there are no models recycled yet, a new instance of model will be created via {@link #createModel()}.
	 *
	 * @author Martin Albedinsky
	 * @see #recycle()
	 */
	abstract class PoolFactory<M extends EntityModel> implements Factory<M> {

		/**
		 * Capacity of the pool for model instances.
		 */
		private final int poolCapacity;

		/**
		 * Pool for model instances.
		 */
		private final Object[] pool;

		/**
		 * Integer determining count of the model instances available in the pool.
		 */
		private final AtomicInteger poolSize = new AtomicInteger(0);

		/**
		 * Creates a new instance of PoolFactory with pool for model instances in the specified
		 * capacity.
		 *
		 * @param poolCapacity The desired capacity of the pool where the recycled model instances
		 *                     will be kept to be later re-used.
		 */
		public PoolFactory(@IntRange(from = 0) int poolCapacity) {
			this.pool = new Object[this.poolCapacity = poolCapacity];
		}

		/**
		 * Returns the capacity of this factory's model instances pool.
		 *
		 * @return Pool's capacity specified during initialization of this factory.
		 * @see #getPoolSize()
		 */
		@IntRange(from = 0)
		public final int getPoolCapacity() {
			return poolCapacity;
		}

		/**
		 * Returns the size of this factory's model instances pool.
		 *
		 * @return Pool's current size determining count of available model instances in the pool
		 * that may be obtained via {@link #obtainModel()}.
		 * @see #isPoolEmpty()
		 */
		@IntRange(from = 0)
		public final int getPoolSize() {
			return poolSize.get();
		}

		/**
		 * Checks whether this factory's model instances pool is empty or not.
		 * <p>
		 * If {@code true} call to {@link #obtainModel()} will return a new model instance, if
		 * {@code false} an instance from the pool will be returned.
		 *
		 * @return {@code True} if there are no instances in the pool at this time, {@code false} if
		 * there is at least one.
		 * @see #getPoolSize()
		 */
		public final boolean isPoolEmpty() {
			return poolSize.get() == 0;
		}

		/**
		 * Obtains an instance of model from the pool if the pool contains any recycled instances.
		 *
		 * @return Model instance from the pool or new model instance if the pool is empty.
		 * @see #createModel()
		 */
		@NonNull
		@SuppressWarnings("unchecked")
		public M obtainModel() {
			synchronized (pool) {
				Object model;
				for (int i = 0; i < poolCapacity; i++) {
					model = pool[i];
					if (model != null) {
						pool[i] = null;
						poolSize.decrementAndGet();
						return (M) model;
					}
				}
			}
			return createModel();
		}

		/**
		 * Recycles the given <var>model</var> by adding it to the instances pool if the pool is no
		 * already full.
		 * <p>
		 * To obtain already recycled model instance, call {@link #obtainModel()}.
		 *
		 * @param model The model to be added back into the pool.
		 */
		public void recycleModel(@NonNull M model) {
			synchronized (pool) {
				for (int i = 0; i < poolCapacity; i++) {
					if (pool[i] == null) {
						pool[i] = model;
						poolSize.incrementAndGet();
						return;
					}
				}
			}
		}
	}

	/**
	 * Simple interface that provides access to selection statement along with its corresponding
	 * arguments for a specific instance of {@link EntityModel}.
	 *
	 * @author Martin Albedinsky
	 * @see #createSelection()
	 * @see #EMPTY_SELECTION
	 */
	interface Selection {

		/**
		 * Returns value (SQLite selection statement) of this model selection.
		 *
		 * @return Value of this selection or {@code null} if this is an empty selection.
		 * @see #getArguments()
		 */
		@Nullable
		String getValue();

		/**
		 * Returns arguments for this selection.
		 *
		 * @return Array of arguments for this selection's statement value or {@code null} if this
		 * is an empty selection.
		 * @see #getValue()
		 */
		@Nullable
		String[] getArguments();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Transforms the current data of this model into ContentValues object that will hold
	 * {@code key(column_name)-&gt;value(field_value)} pairs.
	 *
	 * @return ContentValues with key-&gt;value pairs for the current data presented within this model.
	 */
	@NonNull
	ContentValues toContentValues();

	/**
	 * Populates data of this model with values from the specified <var>cursor</var>.
	 *
	 * @param cursor The cursor to be used to populate this model. Cursor must be already moved to
	 *               the position holding data for this model.
	 * @return {@code True} if all data has been successfully bound to this model, {@code false} if
	 * at least one value failed to be bound.
	 */
	@CheckResult
	boolean fromCursor(@NonNull Cursor cursor);

	/**
	 * Creates a selection that may be used to target a row in database table where are stored values
	 * for this model instance.
	 * <p>
	 * <b>Note</b>, that if this model does not have any {@link universum.studios.android.database.annotation.Column.Primary @Column.Primary}
	 * nor {@link universum.studios.android.database.annotation.Column.Unique @Column.Unique} column
	 * fields or values of those fields are actually {@code null}, the returned selection should be
	 * {@link #EMPTY_SELECTION}.
	 *
	 * @return Selection for this model according to the current values of its <b>primary</b>
	 * and <b>unique</b> column fields.
	 */
	@NonNull
	Selection createSelection();

	/**
	 * Creates a <b>shallow copy</b> of this model.
	 *
	 * @return New instance of this model with the same data.
	 * @see Object#clone()
	 */
	@NonNull
	@SuppressWarnings("CloneDoesntDeclareCloneNotSupportedException")
	EntityModel clone();

	/**
	 * Recycles this model instance and returns it back to Pool of available instances for further use.
	 * <p>
	 * <b>Note</b>, that Pool related logic depends on a specific EntityModel implementation. A common
	 * behaviour of the Pool pattern is that already recycled instances cannot be further used until
	 * they are again obtained from theirs Pool.
	 *
	 * @see PoolFactory
	 */
	void recycle();
}
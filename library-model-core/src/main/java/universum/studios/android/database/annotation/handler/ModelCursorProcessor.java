/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.database.Cursor;
import android.text.TextUtils;

import java.lang.reflect.Field;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.SimpleEntityModel;
import universum.studios.android.database.util.TypeUtils;

/**
 * A {@link EntityModelAnnotationHandler.CursorProcessor} default implementation that is used by
 * entity model annotation handler (within {@link SimpleEntityModel}) to support
 * {@link EntityModel#fromCursor(Cursor)} method.
 * <p>
 * This processor implementation basically iterates all {@link AnnotatedColumn AnnotatedColumns}
 * passed to the processing method ({@link #processModel(Object, AnnotatedColumn[], Cursor)})
 * and populates all fields hold be these columns with values obtained from the passed
 * {@link Cursor} for the specified <b>model</b>.
 *
 * <h3>Column types</h3>
 * Which type of column should be obtained from the cursor is determined by a type of field to which
 * should be such value attached. Basically all <b>primitive types</b> are supported including
 * {@link Enum} type of which value is obtained via <b>String</b> value that should represent
 * a name of a specific instance of a particular Enum type. <b>Boolean</b> value is obtained as
 * <b>Integer</b> (0 = false, 1 = true).
 *
 * @author Martin Albedinsky
 * @see ModelContentValuesProcessor
 */
public class ModelCursorProcessor extends ModelProcessor implements EntityModelAnnotationHandler.CursorProcessor {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ModelCursorProcessor";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	@CheckResult
	public boolean processModel(@NonNull Object model, @NonNull AnnotatedColumn[] columns, @NonNull Cursor cursor) {
		final CursorTask task = CursorTask.obtainFor(model, columns, cursor);
		final boolean success = onProcessModel(task);
		task.recycle();
		return success;
	}

	/**
	 * Called from {@link #processModel(Object, AnnotatedColumn[], Cursor)} to start processing of
	 * the model contained within the given <var>task</var>.
	 *
	 * @param task The task holding data necessary to perform model's processing.
	 * @return {@code True} whenever the task has been executed and all values has been successfully
	 * attached to the annotated columns, {@code false} it at least one column value failed to be attached.
	 */
	@CheckResult
	protected boolean onProcessModel(@NonNull CursorTask task) {
		boolean success = true;
		for (int i = 0; i < task.columns.length; i++) {
			if (!onProcessColumn(task, task.columns[i])) {
				success = false;
			}
		}
		return success;
	}

	/**
	 * Invoked to process Cursor contained within the given <var>task</var> to attach value from one
	 * of its columns to a corresponding field hold by the annotated <var>column</var>.
	 *
	 * @param task   The task holding data necessary for processing of the given column.
	 * @param column Column of the model for which to obtain its value from the Cursor.
	 * @return {@code True} if value of the specified column has been successfully obtained and attached
	 * to its corresponding field, {@code false} if some error occurs.
	 * @throws DatabaseException If type of field hold by the the specified column is not allowed to
	 *                           represent a column due to its type or that such field is not accessible.
	 */
	@CheckResult
	protected boolean onProcessColumn(@NonNull CursorTask task, @NonNull AnnotatedColumn column) {
		// Do not process deprecated column.
		if (column.isDeprecated()) {
			return true;
		}
		final Object model = task.model;
		final Cursor cursor = task.cursor;
		// Check if we the current cursor has value for this column.
		final int columnIndex = indexOfColumn(cursor, column.getName());
		if (columnIndex >= 0 && !attachValueToColumn(column, model, cursor, columnIndex)) {
			throw createFieldNotAllowedAsColumnException(
					model, column.getField(),
					"Failed to attach value from Cursor to column field."
			);
		}
		return true;
	}

	/**
	 * Attaches value obtained from the given <var>cursor</var> for the specified <var>index</var>
	 * to a field hold by the given <var>column</var>.
	 *
	 * @param column The column to which to set the obtained value.
	 * @param model  The model in which is the specified column declared.
	 * @param cursor The cursor from which should be the value obtained.
	 * @param index  The index under which is the value stored within the given cursor.
	 * @return {@code True} if obtained value has been successfully attached to the column, {@code false}
	 * if some error has occurred or the column's field is not allowed to represent a column due to
	 * its type.
	 * @throws DatabaseException If field of the specified column is not accessible.
	 */
	@CheckResult
	@SuppressWarnings("unchecked")
	protected boolean attachValueToColumn(@NonNull AnnotatedColumn column, @NonNull Object model, @NonNull Cursor cursor, int index) {
		final Field columnField = column.getField();
		try {
			if (cursor.getType(index) == Cursor.FIELD_TYPE_NULL) {
				columnField.set(model, null);
				return true;
			}
			switch (column.getType()) {
				case TypeUtils.BYTE:
					columnField.set(model, Integer.valueOf(cursor.getInt(index)).byteValue());
					return true;
				case TypeUtils.SHORT:
					columnField.set(model, cursor.getShort(index));
					return true;
				case TypeUtils.INTEGER:
					columnField.set(model, cursor.getInt(index));
					return true;
				case TypeUtils.LONG:
					columnField.set(model, cursor.getLong(index));
					return true;
				case TypeUtils.FLOAT:
					columnField.set(model, cursor.getFloat(index));
					return true;
				case TypeUtils.DOUBLE:
					columnField.set(model, cursor.getDouble(index));
					return true;
				case TypeUtils.BOOLEAN:
					// Obtain boolean value as INTEGER.
					columnField.set(model, cursor.getInt(index) == 1);
					return true;
				case TypeUtils.STRING:
					columnField.set(model, cursor.getString(index));
					return true;
				case TypeUtils.ENUM:
					final String enumName = cursor.getString(index);
					if (!TextUtils.isEmpty(enumName)) {
						columnField.set(model, Enum.valueOf((Class<? extends Enum>) columnField.getType(), enumName));
					}
					return true;
				default:
					return false;
			}
		} catch (IllegalAccessException e) {
			final String fieldName = columnField.getName();
			final String modelName = model.getClass().getSimpleName();
			throw DatabaseException.exception("Field(" + modelName + "." + fieldName + ") is not accessible.");
		}
	}

	/**
	 * Returns index of the specified <var>columnName</var> within the specified <var>cursor</var>.
	 *
	 * @param cursor     The cursor used to obtain the desired column index.
	 * @param columnName Name of the desired column of which index to obtain.
	 * @return Column index or {@code -1} if such a column is not presented within the specified cursor.
	 */
	@CheckResult
	protected static int indexOfColumn(@NonNull Cursor cursor, @NonNull String columnName) {
		return cursor.getColumnIndex(columnName);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link ProcessorTask} implementation used as task for {@link ModelCursorProcessor}.
	 *
	 * @author Martin Albedinsky
	 */
	protected static final class CursorTask extends ProcessorTask {

		/**
		 * Size for the pool array containing tasks.
		 */
		static final int POOL_SIZE = 10;

		/**
		 * Array containing already recycled ({@link #recycle()}) tasks that can be obtained via
		 * {@link #obtain()} and thus reused.
		 */
		private static final CursorTask[] POOL = new CursorTask[POOL_SIZE];

		/**
		 * Cursor from which to bind its data to the model.
		 */
		protected Cursor cursor;

		/**
		 * Same as {@link #obtain()} where the specified parameters will be attached to the obtained
		 * task.
		 */
		static CursorTask obtainFor(Object model, AnnotatedColumn[] columns, Cursor cursor) {
			final CursorTask task = CursorTask.obtain();
			task.model = model;
			task.columns = columns;
			task.cursor = cursor;
			return task;
		}

		/**
		 * Obtains a new CursorTask object (if available from the pool).
		 * <p>
		 * When you are done with the obtained task do not forget to recycle it via {@link #recycle()}.
		 *
		 * @return New or recycled CursorTask.
		 */
		@NonNull
		@SuppressWarnings("AssignmentToNull")
		protected static CursorTask obtain() {
			synchronized (POOL) {
				CursorTask task;
				for (int i = 0; i < POOL_SIZE; i++) {
					task = POOL[i];
					if (task != null) {
						POOL[i] = null;
						return task;
					}
				}
			}
			return new CursorTask();
		}

		/**
		 */
		@Override
		void onRecycleInner() {
			this.cursor = null;
			synchronized (POOL) {
				for (int i = 0; i < POOL_SIZE; i++) {
					if (POOL[i] == null) {
						POOL[i] = this;
						return;
					}
				}
			}
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.Upgrade;
import universum.studios.android.database.util.TypeUtils;

/**
 * An AnnotatedColumn represents a wrapper for table column field marked by {@link Column} annotation.
 *
 * @author Martin Albedinsky
 */
public interface AnnotatedColumn {

	/**
	 * Returns the name of this column that can be used to build a <b>create column statement</b>.
	 *
	 * @return Name of this table column.
	 * @see Column#value()
	 */
	@NonNull
	String getName();

	/**
	 * Returns the type of this column that can be used to build a <b>create column statement</b>.
	 *
	 * @return One of types specified by {@link TypeUtils} or {@link TypeUtils#UNKNOWN} if type of
	 * this column unknown/not supported.
	 * @see Column#type()
	 */
	@TypeUtils.Type
	int getType();

	/**
	 * Returns the field that is marked by {@link Column} annotation to represent a table column.
	 *
	 * @return Annotated column field.
	 */
	@NonNull
	Field getField();

	/**
	 * Returns a boolean flag indicating whether this column requires to be upgraded depends on the
	 * specified database versions.
	 * <p>
	 * <b>Note</b>, that upgrading can also mean to add this column as new along existing ones.
	 *
	 * @param oldVersion Old version of the database where is table with this column presented.
	 * @param newVersion New version of the database where is table with this column presented.
	 * @return {@code True} if this column requires to be upgraded, {@code false} otherwise.
	 * @see Upgrade
	 */
	boolean requiresUpgrade(int oldVersion, int newVersion);

	/**
	 * Returns a boolean flag indicating whether a <b>null</b> value can be inserted into table column
	 * represented by this column.
	 *
	 * @return {@code True} if also null value obtained from this column's field ({@link #getField()})
	 * can be inserted into database table, {@code false} otherwise.
	 * @see Column#nullable()
	 */
	boolean isNullable();

	/**
	 * Returns a boolean flag indicating whether this column represents a <b>primary key</b> or not.
	 *
	 * @return {@code True} if this column is also primary, {@code false} otherwise.
	 * @see #isUnique()
	 * @see #isForeign()
	 * @see Column.Primary
	 */
	boolean isPrimary();

	/**
	 * Returns a boolean flag indicating whether this column represents a <b>unique key</b> or not.
	 *
	 * @return {@code True} if this column is also unique,  {@code false} otherwise.
	 * @see #isPrimary()
	 * @see #isForeign()
	 * @see Column.Unique
	 */
	boolean isUnique();

	/**
	 * Returns a boolean flag indicating whether this column represents a <b>foreign key</b> or not.
	 *
	 * @return {@code True} if this column is also foreign, {@code false} otherwise.
	 * @see #isPrimary()
	 * @see #isUnique()
	 * @see Column.Foreign
	 */
	boolean isForeign();

	/**
	 * Returns a boolean flag indicating whether this column should be treated as joined.
	 *
	 * @return {@code True} if this column is joined and does not represents a column within database
	 * table but is used only to project value from multiple joined tables, {@code false} otherwise.
	 * @see Column.Joined
	 */
	boolean isJoined();

	/**
	 * Returns a boolean flag indicating whether this column is deprecated.
	 *
	 * @return {@code True} if this column is deprecated and thus should not be presented in database
	 * table's structure, {@code false} otherwise.
	 */
	boolean isDeprecated();
}
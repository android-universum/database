Database-Model-Core
===============

This module contains elements that are used in this library primarily in order to support **ORM** design.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-model-core:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [EntityModel](https://bitbucket.org/android-universum/database/src/main/library-model-core/src/main/java/universum/studios/android/database/model/EntityModel.java)
- [SimpleEntityModel](https://bitbucket.org/android-universum/database/src/main/library-model-core/src/main/java/universum/studios/android/database/model/SimpleEntityModel.java)
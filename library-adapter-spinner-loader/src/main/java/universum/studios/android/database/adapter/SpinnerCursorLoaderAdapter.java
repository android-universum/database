/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.content.Loader;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.LoaderId;
import universum.studios.android.database.annotation.handler.SpinnerLoaderAdapterAnnotationHandler;
import universum.studios.android.database.annotation.handler.SpinnerLoaderAdapterAnnotationHandlers;

/**
 * A {@link BaseSpinnerCursorAdapter} implementation that supports loading of its Cursor data set
 * using Loaders that are build in the Android framework.
 * <p>
 * This adapter implementation provides same functionality (related to management of data loading)
 * as {@link CursorLoaderAdapter} like {@link #initLoader()} or {@link #startLoader()}.
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link LoaderId @LoaderId} <b>[class - inherited]</b>
 * <p>
 * If this annotation is presented, an id provided via this annotation will be used to specify loader
 * id, same as via {@link #setLoaderId(int)}.
 * </li>
 * <li>{@link BaseSpinnerCursorAdapter + super annotations}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 */
public abstract class SpinnerCursorLoaderAdapter<C extends Cursor, M, VH, DVH> extends BaseSpinnerCursorAdapter<C, M, VH, DVH> implements LoaderAdapter<Cursor> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SpinnerCursorLoaderAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Assistant responsible for cursor Loader management.
	 */
	private LoaderAdapterAssistant<Cursor> mLoaderAssistant;

	/**
	 * Id of the loader that is used to load data set of this adapter.
	 */
	private int mLoaderId = NO_LOADER_ID;

	/**
	 * Arguments for this adapter's loader. See {@link #createLoader(int, Bundle)}.
	 */
	private Bundle mLoaderArguments = EMPTY_ARGUMENTS;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #SpinnerCursorLoaderAdapter(Context, LoaderAdapterAssistant, Cursor)} with
	 * {@code null} initial cursor.
	 */
	public SpinnerCursorLoaderAdapter(@NonNull FragmentActivity context) {
		this(context, DataLoaderAdapterAssistants.<Cursor>createForActivity(context), null);
	}

	/**
	 * Same as {@link #SpinnerCursorLoaderAdapter(Context, LoaderAdapterAssistant, Cursor)} with {@code null}
	 * initial cursor.
	 */
	public SpinnerCursorLoaderAdapter(@NonNull Context context, @NonNull LoaderAdapterAssistant<Cursor> loaderAssistant) {
		this(context, loaderAssistant, null);
	}

	/**
	 * Creates a new instance of SpinnerCursorLoaderAdapter with the given initial {@code cursor} data set.
	 * <p>
	 * If {@link LoaderId @LoaderId} annotation is presented above a subclass of this
	 * SpinnerCursorLoaderAdapter, it will be processed here.
	 *
	 * @param context         Context in which will be this adapter used.
	 * @param loaderAssistant Loader assistant to which the adapter will dispatch all loader related
	 *                        requests. See {@link LoaderAdapter} interface for more information.
	 * @param cursor          Initial cursor data set for this adapter. May be {@code null}.
	 */
	public SpinnerCursorLoaderAdapter(@NonNull Context context, @NonNull LoaderAdapterAssistant<Cursor> loaderAssistant, @Nullable C cursor) {
		super(context, cursor);
		(this.mLoaderAssistant = loaderAssistant).attachAdapter(this);
		if (mAnnotationHandler != null) {
			this.mLoaderId = ((SpinnerLoaderAdapterAnnotationHandler) mAnnotationHandler).getLoaderId(mLoaderId);
		}
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	SpinnerLoaderAdapterAnnotationHandler onCreateAnnotationHandler() {
		return SpinnerLoaderAdapterAnnotationHandlers.obtainSpinnerLoaderAdapterHandler(getClass());
	}

	/**
	 */
	@NonNull
	@Override
	protected SpinnerLoaderAdapterAnnotationHandler getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return (SpinnerLoaderAdapterAnnotationHandler) mAnnotationHandler;
	}

	/**
	 * @see #getLoaderAssistant()
	 */
	@Override
	public final void setLoaderAssistant(@NonNull LoaderAdapterAssistant<Cursor> assistant) {
		if (mLoaderAssistant != null) {
			mLoaderAssistant.attachAdapter(null);
		}
		this.mLoaderAssistant = assistant;
		this.mLoaderAssistant.attachAdapter(this);
	}

	/**
	 * Returns the loader assistant used by this adapter to load its data set.
	 *
	 * @return Loader assistant of this adapter.
	 * @see #setLoaderAssistant(LoaderAdapterAssistant)
	 */
	@NonNull
	protected final LoaderAdapterAssistant<Cursor> getLoaderAssistant() {
		return mLoaderAssistant;
	}

	/**
	 */
	@Override
	public void setLoaderId(@IntRange(from = NO_LOADER_ID) int loaderId) {
		this.mLoaderId = loaderId;
	}

	/**
	 */
	@Override
	@IntRange(from = NO_LOADER_ID)
	public int getLoaderId() {
		return mLoaderId;
	}

	/**
	 * Specifies a {@link Bundle} with arguments for this adapter's loader.
	 * <p>
	 * <b>Note that if the given arguments are {@code null}, this adapter will not delegate any
	 * of {@link #initLoader()}, {@link #restartLoader()} nor {@link #startLoader()} to its
	 * associated loader assistant.</b>
	 *
	 * @param arguments The desired arguments for the loader. May be {@link #EMPTY_ARGUMENTS} if
	 *                  this adapter's loader does not require any arguments.
	 * @see #getLoaderArguments()
	 * @see #createLoader(int, Bundle)
	 */
	public void setLoaderArguments(@Nullable Bundle arguments) {
		// todo: move signature of this method to the LoaderAdapter interface in version 3.0.0
		this.mLoaderArguments = arguments;
	}

	/**
	 * Returns the arguments for this adapter's loader.
	 *
	 * @return Loader arguments. May be {@code null}.
	 * @see #setLoaderArguments(Bundle)
	 */
	@Nullable
	public Bundle getLoaderArguments() {
		// todo: move signature of this method to the LoaderAdapter interface in version 3.0.0
		return mLoaderArguments;
	}

	/**
	 */
	@Override
	@SuppressWarnings("SimplifiableIfStatement")
	public boolean startLoader() {
		if (mLoaderId == NO_LOADER_ID || mLoaderArguments == null) {
			return false;
		}
		return mLoaderAssistant.startLoader(mLoaderId, mLoaderArguments) != null;
	}

	/**
	 */
	@Override
	@SuppressWarnings("SimplifiableIfStatement")
	public boolean initLoader() {
		if (mLoaderId == NO_LOADER_ID || mLoaderArguments == null) {
			return false;
		}
		return mLoaderAssistant.initLoader(mLoaderId, mLoaderArguments) != null;
	}

	/**
	 */
	@Override
	@SuppressWarnings("SimplifiableIfStatement")
	public boolean restartLoader() {
		if (mLoaderId == NO_LOADER_ID || mLoaderArguments == null) {
			return false;
		}
		return mLoaderAssistant.restartLoader(mLoaderId, mLoaderArguments) != null;
	}

	/**
	 */
	@Override
	public boolean destroyLoader() {
		return mLoaderId != NO_LOADER_ID && mLoaderAssistant.destroyLoader(mLoaderId);
	}

	/**
	 */
	@Nullable
	@Override
	public Loader<Cursor> getLoader() {
		return mLoaderId == NO_LOADER_ID ? null : mLoaderAssistant.getLoader(mLoaderId);
	}

	/**
	 * Performs cursor change via {@link #changeCursor(Cursor)}.
	 * <p>
	 * This method call also invokes {@link #notifyCursorDataSetLoaded(int)}.
	 *
	 * @param loaderId Id of the loader that loaded the specified <var>cursor</var>.
	 * @param data     Loaded cursor. Can be {@code null} to invalidate data set of this adapter.
	 */
	@Override
	public void changeLoaderData(@IntRange(from = 0) int loaderId, @Nullable Cursor data) {
		changeCursor(data);
		notifyCursorDataSetLoaded(loaderId);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
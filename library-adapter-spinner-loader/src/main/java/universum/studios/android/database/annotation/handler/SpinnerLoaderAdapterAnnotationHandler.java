/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

/**
 * Interface combining {@link SpinnerCursorAdapterAnnotationHandler} and {@link LoadableAnnotationHandler}
 * into one for annotation handlers from the Database library that are used to handle processing of
 * annotations attached to classes derived from <b>Loadable Spinner Adapter</b> classes provided by
 * this library.
 *
 * @author Martin Albedinsky
 */
public interface SpinnerLoaderAdapterAnnotationHandler extends SpinnerCursorAdapterAnnotationHandler, LoadableAnnotationHandler {
}
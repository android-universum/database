/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.adapter.SpinnerCursorLoaderAdapter;
import universum.studios.android.database.annotation.LoaderId;

/**
 * A {@link LoaderAdapterAnnotationHandlers} implementation providing {@link AnnotationHandler}
 * instances for <b>SpinnerLoaderAdapter</b> like classes.
 *
 * @author Martin Albedinsky
 */
public final class SpinnerLoaderAdapterAnnotationHandlers extends LoaderAdapterAnnotationHandlers {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private SpinnerLoaderAdapterAnnotationHandlers() {
		super();
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains a {@link SpinnerLoaderAdapterAnnotationHandler} implementation for the given <var>classOfAdapter</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	public static SpinnerLoaderAdapterAnnotationHandler obtainSpinnerLoaderAdapterHandler(@NonNull Class<?> classOfAdapter) {
		return obtainHandler(SpinnerLoaderAdapterHandler.class, classOfAdapter);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link SpinnerLoaderAdapterAnnotationHandler} implementation for {@link SpinnerCursorLoaderAdapter}
	 * like adapters.
	 */
	@SuppressWarnings("WeakerAccess") static final class SpinnerLoaderAdapterHandler
			extends SpinnerCursorAdapterAnnotationHandlers.SpinnerAdapterHandler
			implements SpinnerLoaderAdapterAnnotationHandler {

		/**
		 * Loader id for the related adapter obtained from the annotated class.
		 */
		private final int loaderId;

		/**
		 * Creates a new instance of LoaderSpinnerAdapterHandler for the specified <var>annotatedClass</var>
		 * with {@link SpinnerCursorLoaderAdapter} as <var>maxSuperClass</var>.
		 */
		public SpinnerLoaderAdapterHandler(@NonNull Class<?> annotatedClass) {
			super(annotatedClass, SpinnerCursorLoaderAdapter.class);
			final LoaderId loaderId = findAnnotationRecursive(LoaderId.class);
			this.loaderId = loaderId == null ? NO_LOADER_ID : loaderId.value();
		}

		/**
		 */
		@Override
		public int getLoaderId(int defaultId) {
			return loaderId == NO_LOADER_ID ? defaultId : loaderId;
		}
	}
}
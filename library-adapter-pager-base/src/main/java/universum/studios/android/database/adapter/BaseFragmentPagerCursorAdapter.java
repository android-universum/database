/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.database.Cursor;
import android.os.Parcelable;
import android.util.AndroidRuntimeException;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.handler.CursorAdapterAnnotationHandler;
import universum.studios.android.database.annotation.handler.CursorAdapterAnnotationHandlers;
import universum.studios.android.database.cursor.ItemCursor;
import universum.studios.android.pager.adapter.FragmentStatePagerAdapter;

/**
 * A {@link FragmentStatePagerAdapter} implementation that can present its data set from an attached
 * {@link Cursor}.
 * <p>
 * This adapter implementation provides same functionality (related to management of the Cursor data
 * set) as {@link BaseCursorAdapter} like {@link #changeCursor(Cursor)} or {@link #swapCursor(Cursor)},
 * and also expects that its cursor is automatically managed by an instance of {@link android.app.LoaderManager LoaderManager}
 * by which has been such cursor loaded.
 *
 * @param <C> Type of the cursor of which data will be presented by a subclass of this
 *            BaseFragmentPagerCursorAdapter.
 * @param <M> Type of the item model that can represent data for a single row of the specified
 *            cursor type.
 * @author Martin Albedinsky
 */
public abstract class BaseFragmentPagerCursorAdapter<C extends Cursor, M> extends FragmentStatePagerAdapter implements CursorDataSetAdapter<C, Fragment> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseFragmentPagerCursorAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Handler responsible for processing of all annotations of this class and also for handling all
	 * annotations related operations for this class.
	 */
	final CursorAdapterAnnotationHandler mAnnotationHandler;

	/**
	 * Data set handling cursor specified for this adapter.
	 */
	private final CursorAdapterDataSet<BaseFragmentPagerCursorAdapter<C, M>, C, Fragment> mDataSet;

	/**
	 * Cursor passed to {@link #swapCursor(Cursor)} of this adapter. This cursor is used to check
	 * whether a new cursor passed to {@link #swapCursor(Cursor)} is really a new instance or the
	 * same instance as previously specified.
	 */
	private Cursor mOriginalCursor;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #BaseFragmentPagerCursorAdapter(FragmentManager, Cursor)} with {@code null}
	 * initial cursor.
	 */
	public BaseFragmentPagerCursorAdapter(@NonNull FragmentManager fragmentManager) {
		this(fragmentManager, null);
	}

	/**
	 * Creates a new instance of BaseFragmentPagerCursorAdapter with the specified <var>fragmentManager</var>
	 * and the given initial <var>cursor</var> data set.
	 *
	 * @param fragmentManager The desired fragment manager that should be used by the new adapter to
	 *                        manage its fragments.
	 * @param cursor          Initial cursor data set for this adapter. May be {@code null}.
	 */
	public BaseFragmentPagerCursorAdapter(@NonNull FragmentManager fragmentManager, @Nullable C cursor) {
		super(fragmentManager);
		this.mAnnotationHandler = onCreateAnnotationHandler();
		this.mDataSet = new CursorAdapterDataSet<>(this);
		this.mDataSet.attachCursor(cursor);
		this.mOriginalCursor = cursor;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Invoked to create annotations handler for this instance.
	 *
	 * @return Annotations handler specific for this class.
	 */
	CursorAdapterAnnotationHandler onCreateAnnotationHandler() {
		return CursorAdapterAnnotationHandlers.obtainCursorAdapterHandler(getClass());
	}

	/**
	 * Returns handler that is responsible for annotations processing of this class and also for
	 * handling all annotations related operations for this class.
	 *
	 * @return Annotations handler specific for this class.
	 * @throws DatabaseException If annotations processing is not enabled for the Database library.
	 */
	@NonNull
	protected CursorAdapterAnnotationHandler getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return mAnnotationHandler;
	}

	/**
	 */
	@Override
	public void registerOnCursorChangeListener(@NonNull OnCursorChangeListener listener) {
		mDataSet.registerOnCursorChangeListener(listener);
	}

	/**
	 */
	@Override
	public void unregisterOnCursorChangeListener(@NonNull OnCursorChangeListener listener) {
		mDataSet.unregisterOnCursorChangeListener(listener);
	}

	/**
	 */
	@Override
	public void registerOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener) {
		mDataSet.registerOnCursorDataSetListener(listener);
	}

	/**
	 * Notifies all registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that the data
	 * set of this adapter has changed.
	 * <p>
	 * This method also propagates this call to {@link #notifyDataSetChanged()}.
	 */
	public void notifyCursorDataSetChanged() {
		notifyDataSetChanged();
		mDataSet.notifyCursorDataSetChanged();
	}

	/**
	 * Notifies all registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that the data
	 * set of this adapter has been just loaded.
	 * <p>
	 * Unlike {@link #notifyCursorDataSetChanged()} this method may be used to notify data set change
	 * to target directly data loading event not just inner data set change.
	 *
	 * @param loaderId Id of the loader that loaded the data set.
	 */
	public void notifyCursorDataSetLoaded(int loaderId) {
		mDataSet.notifyCursorDataSetLoaded(loaderId);
	}

	/**
	 * Notifies all registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that the data
	 * set of this adapter has been invalidated.
	 */
	public void notifyCursorDataSetInvalidated() {
		mDataSet.notifyCursorDataSetInvalidated();
	}

	/**
	 */
	@Override
	public void unregisterOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener) {
		mDataSet.unregisterOnCursorDataSetListener(listener);
	}

	/**
	 */
	@Override
	public void registerOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener) {
		mDataSet.registerOnCursorDataSetActionListener(listener);
	}

	/**
	 * Notifies that the given <var>action</var> has been performed for the specified <var>position</var>.
	 * <p>
	 * If {@link #onCursorDataSetActionSelected(int, int, Object)} will not process this call, the
	 * registered {@link OnCursorDataSetActionListener OnCursorDataSetActionListeners} will be notified.
	 * <p>
	 * <b>Note, that invoking this method with 'invalid' position, out of bounds of the current
	 * data set, will be ignored.</b>
	 *
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled internally by this adapter or by one of
	 * the registers listeners, {@code false} otherwise.
	 */
	protected boolean notifyCursorDataSetActionSelected(int action, int position, @Nullable Object payload) {
		// Do not notify actions for invalid (out of bounds of the current data set) positions.
		return position >= 0 && position < getItemCount() && (
				onCursorDataSetActionSelected(action, position, payload) ||
						mDataSet.notifyCursorDataSetActionSelected(action, position, getItemId(position), payload)
		);
	}

	/**
	 * Invoked immediately after {@link #notifyCursorDataSetActionSelected(int, int, Object)} was called.
	 *
	 * @return {@code True} to indicate that this event was processed here, {@code false} to dispatch
	 * this event to the registered {@link OnCursorDataSetActionListener OnCursorDataSetActionListeners}.
	 */
	protected boolean onCursorDataSetActionSelected(int action, int position, @Nullable Object payload) {
		return false;
	}

	/**
	 */
	@Override
	public void unregisterOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener) {
		mDataSet.unregisterOnCursorDataSetActionListener(listener);
	}

	/**
	 */
	@Override
	public void changeCursor(@Nullable Cursor cursor) {
		final Cursor old = swapCursor(cursor);
		if (old != null) old.close();
	}

	/**
	 */
	@Nullable
	@Override
	@SuppressWarnings("unchecked")
	public C swapCursor(@Nullable Cursor cursor) {
		final C oldCursor = mDataSet.getCursor();
		if (cursor == mOriginalCursor || cursor == oldCursor) {
			return null;
		}
		if (cursor == null) {
			this.mDataSet.notifyCursorChange(null);
			this.mDataSet.attachCursor(null);
			this.mOriginalCursor = null;
			if (!onCursorChange(null, oldCursor)) {
				notifyCursorDataSetInvalidated();
			}
		} else {
			final C newCursor = wrapCursor(cursor);
			this.mDataSet.notifyCursorChange(newCursor);
			this.mDataSet.attachCursor(newCursor);
			this.mOriginalCursor = cursor;
			if (!onCursorChange(newCursor, oldCursor)) {
				notifyCursorDataSetChanged();
			}
		}
		this.mDataSet.notifyCursorChanged(mDataSet.getCursor());
		return oldCursor;
	}

	/**
	 * <b>Implementation of this method will be removed in the next release, so it will become abstract
	 * and thus its implementation will be required in the concrete inheritance hierarchies.</b>
	 */
	@NonNull
	@Override
	public C wrapCursor(@NonNull Cursor cursor) {
		throw new AndroidRuntimeException("Not implemented: " + getClass() + ".wrapCursor(Cursor)!");
	}

	/**
	 * Called from {@link #swapCursor(Cursor)} in order to handle change in cursor of this adapter.
	 * <p>
	 * <b>Note</b>, that during this call this adapter has already the new cursor attached.
	 * <p>
	 * This implementation does nothing.
	 *
	 * @param newCursor The cursor that holds new data set for this adapter.
	 * @param oldCursor The current old cursor of this adapter that will be replaced by the new one.
	 * @return {@code True} if change has been notified to all registered observers, {@code false} if
	 * {@link #notifyDataSetChanged()} should be invoked.
	 */
	protected boolean onCursorChange(@Nullable C newCursor, @Nullable C oldCursor) {
		return false;
	}

	/**
	 */
	@Override
	public boolean isCursorAvailable() {
		return mDataSet.isCursorAvailable();
	}

	/**
	 */
	@Nullable
	@Override
	public C getCursorAt(int position) {
		return mDataSet.getCursorAt(position);
	}

	/**
	 */
	@Nullable
	@Override
	public C getCursor() {
		return mDataSet.getCursor();
	}

	/*
	 * @return {@code True} if {@link #getItemCount()} == 0, {@code false} otherwise.
	 */
	@Override
	public boolean isEmpty() {
		return getItemCount() == 0;
	}

	/**
	 * Same as {@link #getItemCount()}.
	 */
	@Override
	public final int getCount() {
		return getItemCount();
	}

	/**
	 */
	@Override
	public int getItemCount() {
		return mDataSet.getItemCount();
	}

	/**
	 * This implementation by default returns {@code true}.
	 */
	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	/**
	 */
	@Override
	public boolean hasItemAt(int position) {
		return mDataSet.hasItemAt(position);
	}

	/**
	 */
	@NonNull
	@Override
	public Fragment getItem(int position) {
		final C cursor = mDataSet.getCursor();
		if (cursor == null || cursor.isClosed()) {
			throw new IllegalStateException("Cannot present data from the invalid cursor.");
		}
		final int correctedPosition = correctCursorPosition(position);
		if (cursor.moveToPosition(correctedPosition)) {
			return onGetItem(cursor, position);
		}
		throw new IllegalStateException(
				"Cannot move the attached cursor to the position(" + position + ") corrected as(" + correctedPosition + ")."
		);
	}

	/**
	 * Corrects the given cursor <var>position</var> according to the current data set size to ensure
	 * that the attached cursor will be moved to the correct position.
	 * <p>
	 * This can be useful when this cursor adapter presents also data which are not available within
	 * the attached cursor, like some additional page a the start, so practically the current data
	 * set consists of two data sets, one is cursor and other one is a single page.
	 * <p>
	 * For the current implementation of this adapter class, this method is called only for purpose
	 * of {@link #getItem(int)} method, but may be called by implementations of this adapter when its
	 * result is needed. Also the current implementation returns the same position as the given one.
	 *
	 * @param position Position which should be corrected.
	 * @return Corrected position for the cursor.
	 */
	protected int correctCursorPosition(int position) {
		return position;
	}

	/**
	 * Invoked to create a new instance of {@link Fragment} for the specified <var>position</var> to
	 * present data from the given <var>cursor</var> for that position.
	 *
	 * @param cursor   Attached cursor already moved to the current iterated position.
	 * @param position Position of the item from the current data set for which to create new fragment.
	 * @return Fragment for the requested position.
	 */
	@NonNull
	protected abstract Fragment onGetItem(@NonNull C cursor, int position);

	/**
	 * Returns the item (model) containing data from the attached cursor data set for the specified
	 * <var>position</var>.
	 *
	 * @param position Position of the item for which to obtain the model with data.
	 * @return Item bound with data from the current attached cursor for the requested position.
	 * @throws IndexOutOfBoundsException If the specified position is out of bounds of the current
	 *                                   cursor attached to this adapter.
	 * @throws DatabaseException         Type of {@link DatabaseException#TYPE_MISCONFIGURATION} if
	 *                                   the current attached cursor is not type of {@link ItemCursor}
	 *                                   and so we have no information about how to obtain the item
	 *                                   model for the requested position.
	 * @see #hasItemAt(int)
	 * @see #getItemId(int)
	 */
	@NonNull
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	public M getItemModel(int position) {
		if (!hasItemAt(position)) {
			throw new IndexOutOfBoundsException(
					"Requested item at invalid position(" + position + "). " +
							"Data set has items in count of(" + getItemCount() + ")."
			);
		}
		final String adapterName = getClass().getSimpleName();
		final C cursor = getCursorAt(position);
		if (!(cursor instanceof ItemCursor)) {
			throw DatabaseException.misconfiguration(
					"Adapter(" + adapterName + ") does not have its attached cursor type of ItemCursor. " +
							"Implementation of " + adapterName + ".getItemModel(int) is required."
			);
		}
		final M item = ((ItemCursor<M>) cursor).getItem();
		if (item == null) {
			final String cursorName = cursor.getClass().getSimpleName();
			throw new IllegalArgumentException(
					"ItemCursor(" + cursorName + ") attached to adapter(" + adapterName + ")" +
							" returned null item for position(" + position + ")."
			);
		}
		return item;
	}

	/**
	 */
	@Override
	public long getItemId(int position) {
		return hasStableIds() ? mDataSet.getItemId(position) : CursorDataSet.NO_ID;
	}

	/**
	 */
	@Override
	public boolean hasStableIds() {
		return true;
	}

	/**
	 * If you decide to override this method, do not forget to call {@code super.saveInstanceState()}
	 * and pass the obtained super state to the corresponding constructor of your saved state
	 * implementation to ensure the state of all classes along the chain is properly saved.
	 * <p>
	 * This implementation delegates this call to {@link #saveState()}.
	 */
	@NonNull
	@Override
	@CallSuper
	public Parcelable saveInstanceState() {
		return saveState();
	}

	/**
	 * If you decide to override this method, do not forget to call {@code super.restoreInstanceState()}
	 * and pass here the parent state obtained from the your saved state implementation to ensure the
	 * state of all classes along the chain is properly restored.
	 * <p>
	 * This implementation delegates this call to {@link #restoreState(Parcelable, ClassLoader)} with
	 * {@link ClassLoader} of this adapter's class.
	 */
	@Override
	@CallSuper
	public void restoreInstanceState(@NonNull Parcelable savedState) {
		restoreState(savedState, getClass().getClassLoader());
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
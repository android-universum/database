Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/database/downloads "Downloads page") stable release.

- **[Core](https://github.com/universum-studios/android_database/tree/main/library-core)**
- **[Query](https://github.com/universum-studios/android_database/tree/main/library-query)**
- **[Base](https://github.com/universum-studios/android_database/tree/main/library-base)**
- **[Cursor](https://github.com/universum-studios/android_database/tree/main/library-base)**
- **[@Content](https://github.com/universum-studios/android_database/tree/main/library-content_group)**
- **[Content-Core](https://github.com/universum-studios/android_database/tree/main/library-content-core)**
- **[Content-Loader](https://github.com/universum-studios/android_database/tree/main/library-content-loader)**
- **[Content-Header](https://github.com/universum-studios/android_database/tree/main/library-content-header)**
- **[@Adapter](https://github.com/universum-studios/android_database/tree/main/library-adapter_group)**
- **[@Adapter-Common](https://github.com/universum-studios/android_database/tree/main/library-adapter-common_group)**
- **[Adapter-Core](https://github.com/universum-studios/android_database/tree/main/library-adapter-core)**
- **[Adapter-Header](https://github.com/universum-studios/android_database/tree/main/library-adapter-header)**
- **[Adapter-Loader](https://github.com/universum-studios/android_database/tree/main/library-adapter-loader)**
- **[@Adapter-List](https://github.com/universum-studios/android_database/tree/main/library-adapter-list_group)**
- **[Adapter-List-Base](https://github.com/universum-studios/android_database/tree/main/library-adapter-list-base)**
- **[Adapter-List-Loader](https://github.com/universum-studios/android_database/tree/main/library-adapter-list-loader)**
- **[@Adapter-Recycler](https://github.com/universum-studios/android_database/tree/main/library-adapter-recycler_group)**
- **[Adapter-Recycler-Base](https://github.com/universum-studios/android_database/tree/main/library-adapter-recycler-base)**
- **[Adapter-Recycler-Loader](https://github.com/universum-studios/android_database/tree/main/library-adapter-recycler-loader)**
- **[@Adapter-Spinner](https://github.com/universum-studios/android_database/tree/main/library-adapter-spinner_group)**
- **[Adapter-Spinner-Base](https://github.com/universum-studios/android_database/tree/main/library-adapter-spinner-base)**
- **[Adapter-Spinner-Loader](https://github.com/universum-studios/android_database/tree/main/library-adapter-spinner-loader)**
- **[@Adapter-Pager](https://github.com/universum-studios/android_database/tree/main/library-adapter-pager_group)**
- **[Adapter-Pager-Base](https://github.com/universum-studios/android_database/tree/main/library-adapter-pager-base)**
- **[Adapter-Pager-Loader](https://github.com/universum-studios/android_database/tree/main/library-adapter-pager-loader)**
- **[@Model](https://github.com/universum-studios/android_database/tree/main/library-model_group)**
- **[Model-Core](https://github.com/universum-studios/android_database/tree/main/library-model-core)**
- **[Model-Cursor](https://github.com/universum-studios/android_database/tree/main/library-model-cursor)**
- **[Model-Collection](https://github.com/universum-studios/android_database/tree/main/library-model-collection)**
- **[@Entity](https://github.com/universum-studios/android_database/tree/main/library-entity_group)**
- **[Entity-Core](https://github.com/universum-studios/android_database/tree/main/library-entity-core)**
- **[Entity-Model](https://github.com/universum-studios/android_database/tree/main/library-entity-model)**
- **[Mock](https://github.com/universum-studios/android_database/tree/main/library-mock)**
- **[@LoremIpsum](https://github.com/universum-studios/android_database/tree/main/library-loremipsum_group)**
- **[LoremIpsum-Core](https://github.com/universum-studios/android_database/tree/main/library-loremipsum-core)**
- **[LoremIpsum-Model](https://github.com/universum-studios/android_database/tree/main/library-loremipsum-model)**
- **[LoremIpsum-Entity](https://github.com/universum-studios/android_database/tree/main/library-loremipsum-entity)**
- **[LoremIpsum-Cursor](https://github.com/universum-studios/android_database/tree/main/library-loremipsum-cursor)**
- **[LoremIpsum-Adapter](https://github.com/universum-studios/android_database/tree/main/library-loremipsum-adapter)**

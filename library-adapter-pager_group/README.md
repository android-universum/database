@Database-Adapter-Pager
===============

This module groups the following modules into one **single group**:

- [Adapter-Pager-Base](https://bitbucket.org/android-universum/database/src/main/library-adapter-pager-base)
- [Adapter-Pager-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-pager-loader)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-pager:${DESIRED_VERSION}@aar"

_depends on:_
[pager-adapters-core](https://bitbucket.org/android-universum/pager-adapters/src/main/library-core),
[pager-adapters-stateful](https://bitbucket.org/android-universum/pager-adapters/src/main/library-stateful),
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-content-loader](https://bitbucket.org/android-universum/database/src/main/library-content-loader),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-adapter-core](https://bitbucket.org/android-universum/database/src/main/library-adapter-core),
[database-adapter-loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-loader)
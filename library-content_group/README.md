@Database-Content
===============

This module groups the following modules into one **single group**:

- [Content-Core](https://bitbucket.org/android-universum/database/src/main/library-content-core)
- [Content-Header](https://bitbucket.org/android-universum/database/src/main/library-content-header)
- [Content-Loader](https://bitbucket.org/android-universum/database/src/main/library-content-loader)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-content:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core)
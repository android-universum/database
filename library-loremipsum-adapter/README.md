Database-LoremIpsum-Adapter
===============

This module contains implementations of `CursorLoaderAdapter` and `RecyclerCursorLoaderAdapter`
for `LoremIpsum` content.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-loremipsum-adapter:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-base](https://bitbucket.org/android-universum/database/src/main/library-base),
[database-query](https://bitbucket.org/android-universum/database/src/main/library-query),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library/src/content/core),
[database-content-loader](https://bitbucket.org/android-universum/database/src/main/library/src/content/loader),
[database-adapter-common](https://bitbucket.org/android-universum/database/src/main/library-adapter-common_group),
[database-adapter-list](https://bitbucket.org/android-universum/database/src/main/library-adapter-list_group),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-entity](https://bitbucket.org/android-universum/database/src/main/library-entity_group),
[database-model](https://bitbucket.org/android-universum/database/src/main/library-model_group),
[database-loremipsum-core](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-core),
[database-loremipsum-cursor](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-cursor),
[database-loremipsum-model](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-model),
[database-loremipsum-entity](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-entity)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseLoremIpsumAdapter](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-adapter/src/main/java/universum/studios/android/database/loremipsum/BaseLoremIpsumAdapter.java)
- [BaseLoremIpsumRecyclerAdapter](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-adapter/src/main/java/universum/studios/android/database/loremipsum/BaseLoremIpsumRecyclerAdapter.java)
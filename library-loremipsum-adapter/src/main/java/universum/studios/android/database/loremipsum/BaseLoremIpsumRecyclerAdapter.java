/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.database.DatabaseProvider;
import universum.studios.android.database.adapter.DataLoaderAdapterAssistants;
import universum.studios.android.database.adapter.RecyclerCursorLoaderAdapter;
import universum.studios.android.database.content.CursorLoaderBuilder;

/**
 * Copied logic of {@link BaseLoremIpsumAdapter} into context of {@link RecyclerCursorLoaderAdapter}
 * implementation.
 *
 * @author Martin Albedinsky
 */
public abstract class BaseLoremIpsumRecyclerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerCursorLoaderAdapter<LoremIpsumCursor, LoremIpsum, VH> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseLoremIpsumRecyclerAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #BaseLoremIpsumRecyclerAdapter(FragmentActivity, LoremIpsumCursor)} with
	 * {@code null} initial cursor data set.
	 */
	public BaseLoremIpsumRecyclerAdapter(@NonNull FragmentActivity context) {
		this(context, null);
	}

	/**
	 * Creates a new instance of BaseLoremIpsumRecyclerAdapter with the specified initial <var>cursor</var>.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param cursor  Initial cursor data set for this adapter. Can be {@code null}.
	 */
	public BaseLoremIpsumRecyclerAdapter(@NonNull FragmentActivity context, @Nullable LoremIpsumCursor cursor) {
		super(context, DataLoaderAdapterAssistants.<Cursor>createForActivity(context), cursor);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Nullable
	@Override
	public Loader<Cursor> createLoader(@IntRange(from = 0L) int loaderId, @Nullable Bundle params) {
		final Uri entityUri = DatabaseProvider.providePrimaryDatabase().findEntityByClass(LoremIpsumEntity.class).getContentUri();
		return new CursorLoaderBuilder(entityUri).build(mContext);
	}

	/**
	 */
	@NonNull
	@Override
	public LoremIpsumCursor wrapCursor(@NonNull Cursor cursor) {
		return LoremIpsumCursor.wrap(cursor);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.content.Loader;
import universum.studios.android.database.DatabaseProvider;
import universum.studios.android.database.adapter.CursorLoaderAdapter;
import universum.studios.android.database.adapter.DataLoaderAdapterAssistants;
import universum.studios.android.database.content.CursorLoaderBuilder;

/**
 * A {@link CursorLoaderAdapter} implementation that loads and wraps cursor into {@link LoremIpsumCursor}
 * wrapper. A loader that loads such cursor is created within {@link #createLoader(int, Bundle)}
 * using {@link CursorLoaderBuilder} where {@link LoremIpsumEntity} class is passed as argument for
 * obtaining of a content Uri.
 *
 * <h3>Usage</h3>
 * <pre>
 * &#64;LoaderId(Loaders.CONTACTS_COLLECTION)
 * &#64;CursorItemView(R.layout.item_list_contact)
 * public final class ContactsAdapter extends BaseLoremIpsumAdapter&lt;ContactsAdapter.ItemHolder&gt; {
 *
 *      public ContactsAdapter(&#64;NonNull Activity context) {
 *          super(context);
 *      }
 *
 *      &#64;Nullable
 *      &#64;Override
 *      protected Holder onCreateViewHolder(&#64;NonNull View itemView, int position) {
 *          return new ItemHolder(itemView);
 *      }
 *
 *      &#64;Override
 *      protected void onBindView(&#64;NonNull ItemHolder viewHolder, &#64;NonNull LoremIpsumCursor cursor, int position) {
 *          final LoremIpsum loremIpsum = cursor.accessModel();
 *          // ... bind here your contact list item view with lorem ipsum data
 *      }
 *
 *      static final class ItemHolder extends CursorViewHolder {
 *
 *          // ... views of contact list item view
 *
 *          ItemHolder(&#64;NonNull View itemView) {
 *              super(itemView);
 *              // ... obtain here child views from the itemView
 *          }
 *      }
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 */
public abstract class BaseLoremIpsumAdapter<VH> extends CursorLoaderAdapter<LoremIpsumCursor, LoremIpsum, VH> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseLoremIpsumAdapter";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #BaseLoremIpsumAdapter(FragmentActivity, LoremIpsumCursor)} with {@code null} initial
	 * cursor data set.
	 */
	public BaseLoremIpsumAdapter(@NonNull FragmentActivity context) {
		this(context, null);
	}

	/**
	 * Creates a new instance of BaseLoremIpsumAdapter with the specified initial <var>cursor</var>.
	 *
	 * @param context Context in which will be this adapter used.
	 * @param cursor  Initial cursor data set for this adapter. Can be {@code null}.
	 */
	public BaseLoremIpsumAdapter(@NonNull FragmentActivity context, @Nullable LoremIpsumCursor cursor) {
		super(context, DataLoaderAdapterAssistants.<Cursor>createForActivity(context), cursor);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Nullable
	@Override
	public Loader<Cursor> createLoader(@IntRange(from = 0L) int loaderId, @Nullable Bundle params) {
		return new CursorLoaderBuilder(DatabaseProvider.providePrimaryDatabase().findEntityByClass(LoremIpsumEntity.class).getContentUri()).build(mContext);
	}

	/**
	 */
	@NonNull
	@Override
	public LoremIpsumCursor wrapCursor(@NonNull Cursor cursor) {
		return LoremIpsumCursor.wrap(cursor);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
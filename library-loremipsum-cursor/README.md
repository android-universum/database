Database-LoremIpsum-Cursor
===============

This module contains implementation of `EntityModelCursorWrapper` for `LoremIpsum` model.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-loremipsum-cursor:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-model](https://bitbucket.org/android-universum/database/src/main/library-model_group),
[database-loremipsum-core](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-core),
[database-loremipsum-model](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-model)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [LoremIpsumCursor](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-cursor/src/main/java/universum/studios/android/database/loremipsum/LoremIpsumCursor.java)
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import android.database.Cursor;

import androidx.annotation.NonNull;
import universum.studios.android.database.model.EntityModelCursorWrapper;

/**
 * An {@link EntityModelCursorWrapper} implementation that may be used to provide a cursor data set
 * of {@link LoremIpsum} models for a desired cursor adapter.
 *
 * @author Martin Albedinsky
 */
public class LoremIpsumCursor extends EntityModelCursorWrapper<LoremIpsum> {

	/**
	 * Creates a new instance of LoremIpsumCursor wrapper.
	 *
	 * @param cursor Cursor to wrap.
	 */
	protected LoremIpsumCursor(@NonNull Cursor cursor) {
		super(cursor, LoremIpsum.FACTORY.obtainModel());
		setRecycleModelOnClose(true);
	}

	/**
	 * Wraps the given <var>cursor</var> into instance of LoremIpsumCursor.
	 *
	 * @param cursor Cursor to wrap.
	 * @return Instance of LoremIpsumCursor with the given cursor.
	 */
	@NonNull
	public static LoremIpsumCursor wrap(@NonNull Cursor cursor) {
		return cursor instanceof LoremIpsumCursor ? (LoremIpsumCursor) cursor : new LoremIpsumCursor(cursor);
	}
}
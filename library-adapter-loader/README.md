Database-Adapter-Loader
===============

This module contains elements that are used to support loading of `Cursor` data set directly
within context of cursor adapters provided by this library.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-loader:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-content-loader](https://bitbucket.org/android-universum/database/src/main/library-content-loader),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [LoadableAdapter](https://bitbucket.org/android-universum/database/src/main/library-adapter-loader/src/main/java/universum/studios/android/database/adapter/LoadableAdapter.java)
- [LoaderAdapter](https://bitbucket.org/android-universum/database/src/main/library-adapter-loader/src/main/java/universum/studios/android/database/adapter/LoaderAdapter.java)
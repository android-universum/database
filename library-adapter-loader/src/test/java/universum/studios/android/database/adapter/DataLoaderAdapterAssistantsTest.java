/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import org.junit.Test;
import org.robolectric.Robolectric;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author Martin Albedinsky
 */
public final class DataLoaderAdapterAssistantsTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		DataLoaderAdapterAssistants.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<DataLoaderAdapterAssistants> constructor = DataLoaderAdapterAssistants.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testCreateForActivity() {
		// Arrange:
		final FragmentActivity activity = Robolectric.buildActivity(TestActivity.class).create().start().resume().get();
		// Act:
		final DataLoaderAdapterAssistant assistant = DataLoaderAdapterAssistants.createForActivity(activity);
		// Assert:
		assertThat(assistant, is(notNullValue()));
		assertThat(assistant.getAttachedAdapter(), is(nullValue()));
	}

	@Test public void testCreateForFragment() {
		// todo:
	}

	@Test public void testCreateForContext() {
		// Arrange;
		final LoaderManager mockManager = mock(LoaderManager.class);
		// Act:
		final DataLoaderAdapterAssistant assistant = DataLoaderAdapterAssistants.createForContext(context(), mockManager);
		// Assert:
		assertThat(assistant, is(notNullValue()));
		assertThat(assistant.getAttachedAdapter(), is(nullValue()));
		verifyZeroInteractions(mockManager);
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.Loader;

/**
 * Interface specifying API for loader based adapters of which data set can be loaded using standard
 * <b>loaders</b> that are build in the Android framework.
 * <p>
 * This interface is required to be implemented by such adapters that want to use {@link DataLoaderAdapterAssistant}
 * to manage such data set loading logic.
 *
 * @param <D> Type of the data to be loaded by the related loader.
 * @author Martin Albedinsky
 */
public interface LoaderAdapter<D> extends LoadableAdapter<D> {

	/**
	 * Sets an assistant that is responsible for loading of cursor data set of this adapter. The
	 * specified <var>assistant</var> will handle all requests made upon this adapter declared via
	 * {@link LoaderAdapter} interface.
	 *
	 * @param assistant The assistant that should handle loading of the cursor data set for this
	 *                  adapter.
	 */
	void setLoaderAssistant(@NonNull LoaderAdapterAssistant<D> assistant);

	/**
	 * Specifies a loader id for this adapter used whenever one of following methods is invoked
	 * upon an instance of this adapter:
	 * <ul>
	 * <li>{@link #startLoader()}</li>
	 * <li>{@link #initLoader()}</li>
	 * <li>{@link #restartLoader()}</li>
	 * <li>{@link #destroyLoader()}</li>
	 * </ul>
	 *
	 * @param loaderId Id of the loader that will be used to load data set of this adapter.
	 */
	void setLoaderId(@IntRange(from = NO_LOADER_ID) int loaderId);

	/**
	 * Returns the id of the loader that is used to load data set of this adapter.
	 *
	 * @return Current loader id or {@link universum.studios.android.database.content.LoaderAssistant#NO_LOADER_ID LoaderAssistant.NO_LOADER_ID}
	 * if no id has been specified
	 * yet.
	 */
	@IntRange(from = NO_LOADER_ID)
	int getLoaderId();

	/**
	 * Delegates to {@link DataLoaderAdapterAssistant#startLoader(int, Bundle)} with the current
	 * loader id that has been specified via {@link #setLoaderId(int)}.
	 *
	 * @return {@code True} if loader has been initialized/restarted, {@code false} otherwise.
	 */
	boolean startLoader();

	/**
	 * Delegates to {@link DataLoaderAdapterAssistant#initLoader(int, Bundle)} with the current loader
	 * id that has been specified via {@link #setLoaderId(int)}.
	 *
	 * @return {@code True} if loader has been initialized, {@code false} otherwise.
	 */
	boolean initLoader();

	/**
	 * Delegates to {@link DataLoaderAdapterAssistant#restartLoader(int, Bundle)} with the current
	 * loader id that has been specified via {@link #setLoaderId(int)}.
	 *
	 * @return {@code True} if loader has been restarted, {@code false} otherwise.
	 */
	boolean restartLoader();

	/**
	 * Delegates to {@link DataLoaderAdapterAssistant#destroyLoader(int)} with the current loader id
	 * that has been specified via {@link #setLoaderId(int)}.
	 */
	boolean destroyLoader();

	/**
	 * Delegates to {@link DataLoaderAdapterAssistant#getLoader(int)} with the current loader id that
	 * has been specified via {@link #setLoaderId(int)}.
	 */
	@Nullable
	Loader<D> getLoader();
}
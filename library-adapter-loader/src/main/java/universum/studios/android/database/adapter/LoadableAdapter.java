/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.Loader;

/**
 * An interface for adapter of which data set can be loaded via Android framework's {@link Loader Loaders}
 *
 * @param <D> Type of the data to be loaded as data set for this loadable adapter.
 * @author Martin Albedinsky
 */
public interface LoadableAdapter<D> {

	/**
	 * Constant used to identify unspecified loader id.
	 */
	int NO_LOADER_ID = -1;

	/**
	 * Empty arguments for loader.
	 */
	@NonNull
	Bundle EMPTY_ARGUMENTS = Bundle.EMPTY;

	/**
	 * Creates a loader for the requested <var>loaderId</var> responsible for data loading of this
	 * adapter.
	 *
	 * @param loaderId  Id of the desired loader to create.
	 * @param arguments Bundle with arguments for the loader. Should be {@link #EMPTY_ARGUMENTS} if
	 *                  loader does not require any arguments.
	 * @return Requested loader or {@code null} if this adapter does not create loader for the
	 * specified id.
	 * @throws IllegalArgumentException If some of arguments which the requested loader requires are
	 *                                  missing.
	 */
	@Nullable
	Loader<D> createLoader(@IntRange(from = 0) int loaderId, @NonNull Bundle arguments);

	/**
	 * Changes <var>data</var> of this adapter loaded by a loader with the specified <var>loaderId</var>.
	 *
	 * @param loaderId Id of the loader that has loaded the specified data.
	 * @param data     Loaded data to be changed for this adapter.
	 * @see #createLoader(int, Bundle)
	 */
	void changeLoaderData(@IntRange(from = 0) int loaderId, @Nullable D data);
}
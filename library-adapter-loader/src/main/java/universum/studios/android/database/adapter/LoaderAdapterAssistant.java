/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.database.Cursor;

import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import universum.studios.android.database.content.LoaderAssistant;

/**
 * Extends API of {@link LoaderAssistant} to allow usage of this loader assistant API in adapters
 * that load theirs data set via {@link LoaderManager} (from SQLite database as {@link Cursor} for example).
 *
 * @author Martin Albedinsky
 */
public interface LoaderAdapterAssistant<D> extends LoaderAssistant<D> {

	/**
	 * Attaches a loader adapter of which data set loading should be handled by this assistant.
	 *
	 * @param adapter The adapter of which data set loading will handle this assistant.
	 * @see #getAttachedAdapter()
	 */
	void attachAdapter(@Nullable LoadableAdapter<D> adapter);

	/**
	 * Returns the adapter that is currently attached to this assistant.
	 *
	 * @return The adapter that has been attached via {@link #attachAdapter(LoadableAdapter)} or
	 * {@code null} if no adapter is attached.
	 */
	@Nullable
	LoadableAdapter<D> getAttachedAdapter();
}
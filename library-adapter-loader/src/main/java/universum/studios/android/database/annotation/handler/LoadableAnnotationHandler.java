/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import androidx.loader.content.Loader;
import universum.studios.android.database.annotation.LoaderId;
import universum.studios.android.database.content.LoaderAssistant;

/**
 * An {@link AnnotationHandler} extended interface for annotation handlers from the Database library
 * that are used to handle processing of annotations attached to classes that use {@link Loader} to
 * load theirs content/data set.
 *
 * @author Martin Albedinsky
 */
public interface LoadableAnnotationHandler extends AnnotationHandler {

	/**
	 * Constant used to determine that no loader id has been specified via {@link LoaderId @LoaderId}
	 * annotation.
	 */
	int NO_LOADER_ID = LoaderAssistant.NO_LOADER_ID;

	/**
	 * Returns the loader id obtained from {@link LoaderId @LoaderId} annotation (if presented).
	 *
	 * @param defaultId Default loader id to be returned if there is no annotation presented.
	 * @return Via annotation specified id of loader or <var>defaultId</var>.
	 */
	int getLoaderId(int defaultId);
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import universum.studios.android.database.content.BaseLoaderAssistant;

/**
 * A {@link BaseLoaderAssistant} implementation that may be used to support loading of a desired data
 * set for instances of {@link LoaderAdapter LoaderAdapters}.
 *
 * <h3>Loading flow</h3>
 * <ul>
 * <li>
 * {@link #onCreateLoader(int, Bundle)}
 * <p>
 * calls {@link LoaderAdapter#createLoader(int, Bundle)} upon the attached adapter,
 * </li>
 * <li>
 * {@link #onLoadFinished(Loader, Object)}
 * <p>
 * calls {@link LoaderAdapter#changeLoaderData(int, Object)} upon the attached adapter,
 * </li>
 * <li>
 * {@link #onLoaderReset(Loader)}
 * <p>
 * calls {@link LoaderAdapter#changeLoaderData(int, Object) LoaderAdapter#changeLoaderData(int, null)}
 * upon the attached adapter to invalidate its data set.
 * </li>
 * </ul>
 *
 * @param <D> Type of the data that a loader will load for this assistant.
 * @author Martin Albedinsky
 */
public class DataLoaderAdapterAssistant<D> extends BaseLoaderAssistant<D> implements LoaderAdapterAssistant<D> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "DataLoaderAdapterAssistant";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Adapter of which data set loading will this assistant manage.
	 */
	private LoadableAdapter<D> mAdapter;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DataLoaderAdapterAssistant to be used for data loading for an instance
	 * of {@link LoadableAdapter}.
	 *
	 * @see BaseLoaderAssistant#BaseLoaderAssistant(Context, LoaderManager)
	 * @see #attachAdapter(LoadableAdapter)
	 */
	public DataLoaderAdapterAssistant(@NonNull final Context context, @NonNull final LoaderManager loaderManager) {
		super(context, loaderManager);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	public void attachAdapter(@Nullable final LoadableAdapter<D> adapter) {
		this.mAdapter = adapter;
	}

	/**
	 */
	@Nullable
	@Override
	public LoadableAdapter<D> getAttachedAdapter() {
		return mAdapter;
	}

	/**
	 * Delegates this callback to the attached adapter via {@link LoaderAdapter#createLoader(int, Bundle)}.
	 */
	@Override
	public Loader<D> onCreateLoader(@IntRange(from = 0) final int loaderId, @Nullable final Bundle arguments) {
		return mAdapter == null ? null : mAdapter.createLoader(loaderId, arguments == null ? LoadableAdapter.EMPTY_ARGUMENTS : arguments);
	}

	/**
	 * Changes the loaded <var>data</var> for the attached adapter via {@link LoaderAdapter#changeLoaderData(int, Object)}.
	 */
	@Override
	protected void onLoadFinished(@IntRange(from = 0) final int loaderId, @NonNull final D data) {
		if (mAdapter != null) mAdapter.changeLoaderData(loaderId, data);
	}

	/**
	 * Changes {@code null} data for the attached adapter via {@link LoaderAdapter#changeLoaderData(int, Object)}.
	 */
	@Override
	@SuppressLint("LongLogTag")
	protected void onLoadFailed(@IntRange(from = 0) final int loaderId) {
		if (mAdapter != null) {
			mAdapter.changeLoaderData(loaderId, null);
			final String adapterName = mAdapter.getClass().getSimpleName();
			Log.e(TAG, "Received invalid data for adapter(" + adapterName + ") from loader with id(" + loaderId + ").");
		}
	}

	/**
	 * Changes {@code null} data for the attached adapter via {@link LoaderAdapter#changeLoaderData(int, Object)}.
	 */
	@Override
	public void onLoaderReset(@NonNull final Loader<D> loader) {
		if (mAdapter != null) mAdapter.changeLoaderData(loader.getId(), null);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;

/**
 * Factory class which may be used to create {@link DataLoaderAdapterAssistant} instances for a
 * different contexts.
 *
 * @author Martin Albedinsky
 */
public final class DataLoaderAdapterAssistants {

	/**
	 */
	private DataLoaderAdapterAssistants() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/**
	 * Same as {@link #createForContext(Context, LoaderManager)} where the given <var>activity</var>
	 * is used as <var>context</var> parameter and {@link Activity#getLoaderManager()} as <var>loaderManager</var>
	 * parameter.
	 *
	 * @param activity The activity to be used to create the new loader assistant.
	 * @param <D>      Type of the data the loader assistant should load for desired adapter.
	 * @return Loader assistant ready to be used to load data for a desired adapter.
	 */
	@NonNull
	public static <D> DataLoaderAdapterAssistant<D> createForActivity(@NonNull final FragmentActivity activity) {
		return createForContext(activity, activity.getSupportLoaderManager());
	}

	/**
	 * Same as {@link #createForContext(Context, LoaderManager)} where parent {@link Activity} of
	 * the given <var>fragment</var> is used as <var>context</var> parameter and {@link Fragment#getLoaderManager()}
	 * as <var>loaderManager</var> parameter.
	 *
	 * @param fragment The fragment to be used to create the new loader assistant.
	 * @param <D>      Type of the data the loader assistant should load for desired adapter.
	 * @return Loader assistant ready to be used to load data for a desired adapter.
	 */
	@NonNull
	public static <D> DataLoaderAdapterAssistant<D> createForFragment(@NonNull final Fragment fragment) {
		return createForContext(fragment.getActivity(), fragment.getLoaderManager());
	}

	/**
	 * Creates a new loader adapter assistant which may be attached to a desired loader adapter which
	 * will use the assistant in order to load its data set.
	 *
	 * @param context       Context to be used by the assistant.
	 * @param loaderManager Loader manager to be used by the assistant.
	 * @param <D>           Type of the data the loader assistant should load for desired adapter.
	 * @return Loader assistant ready to be used to load data for a desired adapter.
	 */
	@NonNull
	public static <D> DataLoaderAdapterAssistant<D> createForContext(@NonNull final Context context, @NonNull final LoaderManager loaderManager) {
		return new DataLoaderAdapterAssistant<>(context, loaderManager);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
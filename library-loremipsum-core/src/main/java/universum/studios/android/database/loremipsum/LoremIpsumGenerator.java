/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import android.text.TextUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Random;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

/**
 * A LoremIpsumGenerator can be used to generate a <b>Lorem Ipsum</b> words or sentences. A desired
 * lorem ipsum word or sentence can be generated via methods listed below via passing one of size
 * flags: {@link #SIZE_SHORT}, {@link #SIZE_MEDIUM}, {@link #SIZE_LONG}, {@link #SIZE_VERY_LONG} or
 * {@link #SIZE_ANY}.
 *
 * <h3>Provided API</h3>
 * <ul>
 * <li>{@link #randomWord(int)}</li>
 * <li>{@link #randomWords(int)}</li>
 * <li>{@link #randomSentence(int)}</li>
 * <li>{@link #randomSentences(int, int)}</li>
 * <li>{@link #randomFragment()}</li>
 * </ul>
 *
 * <b>Note</b>, that implementation of this generator is based on LoremIpsum
 * <a href="https://github.com/lkcampbell/brackets-lorem-ipsum/blob/master/LoremIpsum.js">generator</a>
 * wrote in JavaScript.
 *
 * @author Martin Albedinsky
 */
public final class LoremIpsumGenerator {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LoremIpsumGenerator";

	/**
	 * Flag indicating that any size (one of {@link #SIZE_SHORT}, {@link #SIZE_MEDIUM}, {@link #SIZE_LONG},
	 * {@link #SIZE_VERY_LONG}) may be picked when is this flag used.
	 *
	 * @see #randomWord(int)
	 * @see #randomSentence(int)
	 */
	public static final int SIZE_ANY = 0x00;

	/**
	 * Flag indicating that a short word/sentence should be generated.
	 *
	 * @see #randomWord(int)
	 * @see #randomSentence(int)
	 */
	public static final int SIZE_SHORT = 0x01;

	/**
	 * Flag indicating that a medium word/sentence should be generated.
	 *
	 * @see #randomWord(int)
	 * @see #randomSentence(int)
	 */
	public static final int SIZE_MEDIUM = 0x02;

	/**
	 * Flag indicating that a long word/sentence should be generated.
	 *
	 * @see #randomWord(int)
	 * @see #randomSentence(int)
	 */
	public static final int SIZE_LONG = 0x03;

	/**
	 * Flag indicating that a very long word/sentence should be generated.
	 *
	 * @see #randomWord(int)
	 * @see #randomSentence(int)
	 */
	public static final int SIZE_VERY_LONG = 0x04;

	/**
	 * Defines an annotation for determining set of allowed sizes which may be requested either for
	 * lorem ipsum words or sentences.
	 */
	@Retention(RetentionPolicy.SOURCE)
	@IntDef({SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG, SIZE_VERY_LONG, SIZE_ANY})
	public @interface Size {
	}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Instance of random that can be used to generate a random values.
	 */
	public static final Random RANDOM = new Random();

	/**
	 * Array with all size flags.
	 */
	private static final Integer[] SIZES_ALL = {
			SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG, SIZE_VERY_LONG
	};

	/**
	 * Array containing words length of less than 4 letters.
	 */
	private static final String[] WORDS_SHORT = {
			"a", "ab", "ad", "an", "aut", "de", "do", "e", "ea", "est",
			"et", "eu", "ex", "hic", "id", "iis", "in", "ita", "nam", "ne",
			"non", "o", "qui", "quo", "se", "sed", "si", "te", "ubi", "ut"
	};

	/**
	 * Array containing words length of between 4 and 6 letters.
	 */
	private static final String[] WORDS_MEDIUM = {
			"amet", "aliqua", "anim", "aute", "cillum", "culpa", "dolor",
			"dolore", "duis", "elit", "enim", "eram", "esse", "fore",
			"fugiat", "illum", "ipsum", "irure", "labore", "legam", "lorem",
			"magna", "malis", "minim", "multos", "nisi", "noster", "nulla",
			"quae", "quem", "quid", "quis", "quorum", "sint", "summis",
			"sunt", "tamen", "tempor", "varias", "velit", "veniam"
	};

	/**
	 * Array containing words length of between 7 and 9 letters.
	 */
	private static final String[] WORDS_LONG = {
			"admodum", "aliquip", "appellat", "arbitror", "cernantur",
			"commodo", "consequat", "cupidatat", "deserunt", "doctrina",
			"eiusmod", "excepteur", "fabulas", "ingeniis", "iudicem",
			"laboris", "laborum", "litteris", "mentitum", "nescius",
			"nostrud", "occaecat", "officia", "offendit", "pariatur",
			"possumus", "probant", "proident", "quamquam", "quibusdam",
			"senserit", "singulis", "ullamco", "vidisse", "voluptate"
	};

	/**
	 * Array containing words length of more than 9 letters.
	 */
	private static final String[] WORDS_VERY_LONG = {
			"adipisicing", "arbitrantur", "cohaerescant", "comprehenderit",
			"concursionibus", "coniunctione", "consectetur", "despicationes",
			"distinguantur", "domesticarum", "efflorescere", "eruditionem",
			"exquisitaque", "exercitation", "expetendis", "familiaritatem",
			"fidelissimae", "firmissimum", "graviterque", "incididunt",
			"incurreret", "illustriora", "instituendarum", "imitarentur",
			"mandaremus", "philosophari", "praesentibus", "praetermissum",
			"relinqueret", "reprehenderit", "sempiternum", "tractavissent",
			"transferrem", "voluptatibus"
	};

	/**
	 * Array containing all lorem ipsum words.
	 */
	private static final String[] WORDS_ALL = joinArrays(
			WORDS_SHORT, WORDS_MEDIUM, WORDS_LONG, WORDS_VERY_LONG
	);

	/**
	 * Sentence fragment patterns, based on randomly selected latin phrases. Used to build all
	 * requested sentences and paragraphs.
	 */
	private static final int[][] FRAGMENT_PATTERNS = {
			// Three words patterns.
			{SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG},
			{SIZE_SHORT, SIZE_MEDIUM, SIZE_VERY_LONG},
			{SIZE_SHORT, SIZE_SHORT, SIZE_VERY_LONG},
			{SIZE_SHORT, SIZE_LONG, SIZE_VERY_LONG},
			{SIZE_MEDIUM, SIZE_LONG, SIZE_LONG},
			{SIZE_MEDIUM, SIZE_LONG, SIZE_VERY_LONG},
			{SIZE_MEDIUM, SIZE_SHORT, SIZE_LONG},
			{SIZE_LONG, SIZE_SHORT, SIZE_MEDIUM},
			{SIZE_LONG, SIZE_SHORT, SIZE_LONG},
			{SIZE_LONG, SIZE_MEDIUM, SIZE_LONG},
			// Four words patterns.
			{SIZE_SHORT, SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG},
			{SIZE_SHORT, SIZE_MEDIUM, SIZE_SHORT, SIZE_MEDIUM},
			{SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG, SIZE_LONG},
			{SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG, SIZE_VERY_LONG},
			{SIZE_SHORT, SIZE_LONG, SIZE_SHORT, SIZE_LONG},
			{SIZE_MEDIUM, SIZE_LONG, SIZE_SHORT, SIZE_LONG},
			{SIZE_MEDIUM, SIZE_LONG, SIZE_SHORT, SIZE_VERY_LONG},
			{SIZE_LONG, SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG},
			{SIZE_LONG, SIZE_MEDIUM, SIZE_LONG, SIZE_LONG},
			{SIZE_LONG, SIZE_VERY_LONG, SIZE_SHORT, SIZE_LONG},
			// Five words patterns.
			{SIZE_SHORT, SIZE_SHORT, SIZE_MEDIUM, SIZE_MEDIUM, SIZE_MEDIUM},
			{SIZE_SHORT, SIZE_MEDIUM, SIZE_MEDIUM, SIZE_SHORT, SIZE_LONG},
			{SIZE_SHORT, SIZE_MEDIUM, SIZE_MEDIUM, SIZE_MEDIUM, SIZE_LONG},
			{SIZE_MEDIUM, SIZE_SHORT, SIZE_SHORT, SIZE_MEDIUM, SIZE_LONG},
			{SIZE_MEDIUM, SIZE_SHORT, SIZE_LONG, SIZE_SHORT, SIZE_MEDIUM},
			{SIZE_MEDIUM, SIZE_LONG, SIZE_SHORT, SIZE_MEDIUM, SIZE_MEDIUM},
			{SIZE_MEDIUM, SIZE_VERY_LONG, SIZE_LONG, SIZE_MEDIUM, SIZE_LONG},
			{SIZE_LONG, SIZE_MEDIUM, SIZE_SHORT, SIZE_LONG, SIZE_VERY_LONG},
			{SIZE_LONG, SIZE_MEDIUM, SIZE_MEDIUM, SIZE_SHORT, SIZE_MEDIUM},
			{SIZE_LONG, SIZE_MEDIUM, SIZE_MEDIUM, SIZE_LONG, SIZE_MEDIUM}
	};

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private LoremIpsumGenerator() {
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Joins the specified set of String arrays into one.
	 *
	 * @param arrays The arrays to be joined.
	 * @return Joined array containing all items of the specified arrays.
	 */
	private static String[] joinArrays(String[]... arrays) {
		int totalLength = 0;
		for (String[] array : arrays) {
			totalLength += array.length;
		}
		final String[] joined = new String[totalLength];
		int index = 0;
		for (String[] array : arrays) {
			for (String text : array) {
				joined[index++] = text;
			}
		}
		return joined;
	}

	/**
	 * Generates the specified <var>count</var> of lorem ipsum words using {@link #randomWord(int)}.
	 *
	 * @param count The desired count of words to be generated.
	 * @return Lorem ipsum words size of {@link #SIZE_ANY} in the requested count.
	 */
	@NonNull
	public static String randomWords(int count) {
		final StringBuilder words = new StringBuilder(count * 8);
		for (int i = 0; i < count; i++) {
			words.append(randomWord(SIZE_ANY)).append(" ");
		}
		return words.toString().trim();
	}

	/**
	 * Generates a lorem ipsum words of the specified <var>size</var>.
	 * <p>
	 * <b>Note</b>, that the specified size is used to determine from which words category to pick
	 * the desired one.
	 *
	 * <h3>Words categories</h3>
	 * <ul>
	 * <li>{@link #SIZE_SHORT} - words with lest than 4 letters</li>
	 * <li>{@link #SIZE_MEDIUM} - words with at least 4 and with maximum 6 letters</li>
	 * <li>{@link #SIZE_LONG} - words with at least 7 and with maximum 9 letters</li>
	 * <li>{@link #SIZE_VERY_LONG} - words with more than 9 letters</li>
	 * </ul>
	 *
	 * @param size One of {@link #SIZE_SHORT}, {@link #SIZE_MEDIUM}, {@link #SIZE_LONG}, {@link #SIZE_VERY_LONG}
	 *             or {@link #SIZE_ANY}.
	 * @return Lorem ipsum word of the requested size or {@code empty text} if unsupported size has
	 * been requested.
	 */
	@NonNull
	public static String randomWord(@Size int size) {
		switch (size) {
			case SIZE_ANY:
				return randomElement(WORDS_ALL);
			case SIZE_SHORT:
				return randomElement(WORDS_SHORT);
			case SIZE_MEDIUM:
				return randomElement(WORDS_MEDIUM);
			case SIZE_LONG:
				return randomElement(WORDS_LONG);
			case SIZE_VERY_LONG:
				return randomElement(WORDS_VERY_LONG);
		}
		return "";
	}

	/**
	 * Picks a random element from the specified <var>array</var>.
	 *
	 * @param array The array of elements from which to pick one.
	 * @return Randomly picked element.
	 */
	private static <T> T randomElement(T[] array) {
		return array[Math.round(RANDOM.nextFloat() * (array.length - 1))];
	}

	/**
	 * Generates the specified <var>count</var> of lorem ipsum sentences of the specified <var>size</var>
	 * using {@link #randomSentence(int)}.
	 *
	 * @param count The desired count of sentences to be generated.
	 * @param size  One of {@link #SIZE_SHORT}, {@link #SIZE_MEDIUM}, {@link #SIZE_LONG}, {@link #SIZE_VERY_LONG}
	 *              or {@link #SIZE_ANY}.
	 * @return The requested count of sentences separated each by one empty line.
	 */
	@NonNull
	public static String randomSentences(int count, @Size int size) {
		final StringBuilder sentences = new StringBuilder(count * 24);
		for (int i = 0; i < count; i++) {
			sentences.append(randomSentence(size)).append("\n\n");
		}
		return sentences.toString().trim();
	}

	/**
	 * Generates a sentence of the specified <var>size</var> containing randomly picked lorem ipsum
	 * words. The generated sentence will start with upper-cased first letter and end with a dot.
	 * <p>
	 * <b>Note</b>, that the specified <var>size</var> is used to determine from how much fragments
	 * should be the requested sentence constructed, but does not specify exact count of words to be
	 * presented within it. Basically a {@link #SIZE_SHORT} sentence is constructed from one fragment
	 * obtained via {@link #randomFragment()}, sentence size of {@link #SIZE_MEDIUM} is constructed
	 * from two sentences size of {@link #SIZE_SHORT} separated by a connector (a word or ", "),
	 * sentence size of {@link #SIZE_LONG} is constructed from two sentences size of {@link #SIZE_MEDIUM}
	 * separated by the connector and so on.
	 *
	 * @param size One of {@link #SIZE_SHORT}, {@link #SIZE_MEDIUM}, {@link #SIZE_LONG}, {@link #SIZE_VERY_LONG}
	 *             or {@link #SIZE_ANY}.
	 * @return Sentence of lorem ipsum words with the requested size or {@code empty text} if unsupported
	 * size has been requested.
	 */
	@NonNull
	public static String randomSentence(@Size int size) {
		final String sentence = rawSentence(size);
		return !TextUtils.isEmpty(sentence) ? firstToUpperCase(sentence) + "." : "";
	}

	/**
	 * Generates a sentence of the specified <var>size</var> containing randomly picked lorem ipsum
	 * words.
	 * <p>
	 * <b>Note</b>, that the sentence is generated as raw which means that it does not starts with
	 * upper-cased first letter and also does not ends with a dot.
	 *
	 * @param size One of {@link #SIZE_SHORT}, {@link #SIZE_MEDIUM}, {@link #SIZE_LONG}, {@link #SIZE_VERY_LONG}
	 *             or {@link #SIZE_ANY}.
	 * @return Sentence of lorem ipsum words.
	 */
	private static String rawSentence(int size) {
		switch (size) {
			case SIZE_ANY:
				return rawSentence(randomElement(SIZES_ALL));
			case SIZE_SHORT:
				return randomFragment();
			case SIZE_MEDIUM:
				return rawSentence(SIZE_SHORT) + sentenceConnector() + rawSentence(SIZE_SHORT);
			case SIZE_LONG:
				return rawSentence(SIZE_MEDIUM) + sentenceConnector() + rawSentence(SIZE_MEDIUM);
			case SIZE_VERY_LONG:
				return rawSentence(SIZE_LONG) + sentenceConnector() + rawSentence(SIZE_LONG);
		}
		return "";
	}

	/**
	 * Transforms the specified <var>text</var> so it first word will be upper-cased.
	 *
	 * @param text The text to be transformed.
	 * @return Transformed text with first word upper-cased.
	 */
	@NonNull
	public static String firstToUpperCase(@NonNull String text) {
		if (!TextUtils.isEmpty(text)) {
			final String firstLetter = text.substring(0, 1);
			return text.replaceFirst(firstLetter, firstLetter.toUpperCase());
		}
		return text;
	}

	/**
	 * Generates a random fragment of lorem ipsum text.
	 * <p>
	 * <b>Note</b>, that the fragment will contain only lower-case words also without dot at the end.
	 *
	 * @return A text containing randomly picked lorem ipsum words from the range {@code [3, 5]}.
	 */
	@NonNull
	public static String randomFragment() {
		final int[] pattern = randomElement(FRAGMENT_PATTERNS);
		final StringBuilder fragment = new StringBuilder(pattern.length * 8);
		for (int size : pattern) {
			fragment.append(randomWord(size)).append(" ");
		}
		return fragment.toString().trim();
	}

	/**
	 * Generates a connector for a sentence.
	 *
	 * @return Word size of {@link #SIZE_SHORT} with 50 % probability or {@code ", "} otherwise.
	 */
	private static String sentenceConnector() {
		return RANDOM.nextFloat() < 0.5 ? ", " : " " + randomWord(SIZE_SHORT) + " ";
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
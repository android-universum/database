/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.loremipsum;

import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.util.ColumnType;

/**
 * The contract between LoremIpsum database entity and applications. Definitions for available columns
 * are contained within {@link Columns}.
 *
 * @author Martin Albedinsky
 */
public final class LoremIpsumContract {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LoremIpsumContract";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Defines all column names for Lorem Ipsum database table.
	 */
	public interface Columns {

		/**
		 * The id for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#INTEGER}</b>
		 */
		String ID = Column.Primary.COLUMN_NAME;

		/**
		 * The title for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#TEXT}</b>
		 */
		String TITLE = "title";

		/**
		 * The name for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#TEXT}</b>
		 */
		String NAME = "name";

		/**
		 * The note for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#TEXT}</b>
		 */
		String NOTE = "note";

		/**
		 * The content for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#TEXT}</b>
		 */
		String CONTENT = "content";

		/**
		 * The description for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#TEXT}</b>
		 */
		String DESCRIPTION = "description";

		/**
		 * The date of creation for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#INTEGER}</b>
		 */
		String DATE_CREATED = "date_created";

		/**
		 * The date of modification for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#INTEGER}</b>
		 */
		String DATE_MODIFIED = "date_modified";

		/**
		 * The size for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#INTEGER}</b>
		 */
		String SIZE = "size";

		/**
		 * The boolean flags for the LoremIpsum.
		 * <p>
		 * Type: <b>{@link ColumnType#INTEGER}</b>
		 */
		String FLAGS = "flags";
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private LoremIpsumContract() {
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/*
	 * Inner classes ===============================================================================
	 */
}
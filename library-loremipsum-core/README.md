Database-LoremIpsum-Core
===============

This module contains elements that may be used to generate **Lorem Ipsum** content like **simple words**
and/or **complete sentences**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-loremipsum-core:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [LoremIpsumGenerator](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-core/src/main/java/universum/studios/android/database/loremipsum/LoremIpsumGenerator.java)
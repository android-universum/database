/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

/**
 * A CursorLoaderBuilder may be used to build an instance of {@link CursorLoader}. One requires an
 * {@link Uri} that need to be supplied during its initialization via {@link #CursorLoaderBuilder(Uri)}.
 * <p>
 * All desired parameters that can be supplied to CursorLoader may be specified via following methods:
 * <ul>
 * <li>{@link #projection(Projection)}</li>
 * <li>{@link #projection(String[])}</li>
 * <li>{@link #selection(Selection)}</li>
 * <li>{@link #selection(String)}</li>
 * <li>{@link #selectionArgs(String[])}</li>
 * <li>{@link #sortOrder(SortOrder)}</li>
 * <li>{@link #sortOrder(String, String[])}</li>
 * </ul>
 * <p>
 * A new instance of CursorLoader for the supplied parameters may be then created via {@link #build(Context)}.
 *
 * @author Martin Albedinsky
 */
public class CursorLoaderBuilder {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CursorLoaderBuilder";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Content uri for cursor loader.
	 */
	private final Uri mUri;

	/**
	 * Projection statement builder for cursor loader.
	 */
	private Projection mProjection;

	/**
	 * Selection statement builder for cursor loader.
	 */
	private Selection mSelection;

	/**
	 * Sort order statement builder for cursor loader.
	 */
	private SortOrder mSortOrder;

	/**
	 * Selection arguments for cursor loader.
	 */
	private String[] mSelectionArgs;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of LoaderBuilder for the specified <var>uri</var>.
	 *
	 * @param uri Content Uri that is used by this builder to create the desired loader.
	 */
	public CursorLoaderBuilder(@NonNull final Uri uri) {
		this.mUri = uri;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the content Uri specified for this builder during its initialization.
	 *
	 * @return The uri for which this builder can create loader via {@link #build(Context)}.
	 */
	@NonNull
	public final Uri getUri() {
		return mUri;
	}

	/**
	 * Sets a raw projection for this builder.
	 *
	 * @param columns The desired columns to set as projection.
	 * @return This builder to allow methods chaining.
	 * @see #projection(Projection)
	 */
	public CursorLoaderBuilder projection(@NonNull final String[] columns) {
		return projection(new Projection().columns(columns));
	}

	/**
	 * Sets a projection for this builder. This will erase the current projection (if any)
	 * before specified for this builder.
	 *
	 * @param projection The desired projection. May be {@code null} to clear the current one.
	 * @return This builder to allow methods chaining.
	 * @see #projection()
	 * @see #projection(String...)
	 */
	public CursorLoaderBuilder projection(@Nullable final Projection projection) {
		this.mProjection = projection;
		return this;
	}

	/**
	 * Returns the current projection of this builder instance.
	 *
	 * @return Projection or {@code null} if this builder does not have any projection specified.
	 * @see #projection(Projection)
	 * @see #projection(String...)
	 */
	@Nullable
	public Projection projection() {
		return mProjection;
	}

	/**
	 * Sets a raw selection statement for this builder.
	 *
	 * @param selection The desired selection statement.
	 * @return This builder to allow methods chaining.
	 * @see #selection(Selection)
	 */
	public CursorLoaderBuilder selection(@NonNull final String selection) {
		return selection(new Selection(selection));
	}

	/**
	 * Sets a selection statement for this builder. This will erase the current selection (if any)
	 * before specified for this builder.
	 *
	 * @param selection The desired selection. May be {@code null} to clear the current one.
	 * @return This builder to allow methods chaining.
	 * @see #selection(String)
	 * @see #selection()
	 */
	public CursorLoaderBuilder selection(@Nullable final Selection selection) {
		this.mSelection = selection;
		return this;
	}

	/**
	 * Returns the current selection statement of this builder instance.
	 *
	 * @return Selection or {@code null} if this builder does not have any selection specified.
	 */
	@Nullable
	public Selection selection() {
		return mSelection;
	}

	/**
	 * Sets a selection arguments for this builder. This will erase the current selection arguments
	 * (if any) before specified for this builder.
	 *
	 * @param args The desired selection arguments. May be {@code null} to clear the current ones.
	 * @return This builder to allow methods chaining.
	 * @see #selectionArgs()
	 * @see #selection(Selection)
	 */
	public CursorLoaderBuilder selectionArgs(@Nullable final String[] args) {
		this.mSelectionArgs = args;
		return this;
	}

	/**
	 * Returns the current selection arguments of this builder instance.
	 *
	 * @return Selection arguments or {@code null} if this builder does not have any selection
	 * arguments specified.
	 * @see #selectionArgs(String[])
	 */
	@Nullable
	public String[] selectionArgs() {
		return mSelectionArgs;
	}

	/**
	 * Sets a raw sort order statement for this builder.
	 *
	 * @param keyWord Sort ordering key word.
	 * @param columns The desired keyword and columns for which to set the desired sort order.
	 * @return This builder to allow methods chaining.
	 * @see #sortOrder(SortOrder)
	 */
	public CursorLoaderBuilder sortOrder(@NonNull final String keyWord, @NonNull final String[] columns) {
		return sortOrder(new SortOrder(keyWord).columns(columns));
	}

	/**
	 * Sets a sort order for this builder. This will erase the current sort order (if any) before
	 * specified for this builder.
	 *
	 * @param sortOrder The desired sort order. May be {@code null} to clear the current one.
	 * @return This builder to allow methods chaining.
	 * @see #sortOrder(String, String[])
	 * @see #sortOrder()
	 */
	public CursorLoaderBuilder sortOrder(@Nullable final SortOrder sortOrder) {
		this.mSortOrder = sortOrder;
		return this;
	}

	/**
	 * Returns the current sort order of this builder instance.
	 *
	 * @return Sort order or {@code null} if this builder does not have any sort order specified.
	 * @see #sortOrder(SortOrder)
	 */
	@Nullable
	public SortOrder sortOrder() {
		return mSortOrder;
	}

	/**
	 * Creates a new instance of {@link CursorLoader} for the current parameters of this builder instance.
	 *
	 * @param context Context used to create loader.
	 * @return New instance of CursorLoader.
	 * @see #projection(Projection)
	 * @see #selection(Selection)
	 * @see #selectionArgs(String[])
	 * @see #sortOrder(SortOrder)
	 */
	@NonNull
	public Loader<Cursor> build(@NonNull final Context context) {
		return new CursorLoader(
				context,
				mUri,
				mProjection == null ? null : mProjection.build(),
				mSelection == null ? null : mSelection.build(),
				mSelectionArgs,
				mSortOrder == null ? null : mSortOrder.build()
		);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

/**
 * Interface providing API for loader assistants that simplify loading of content via {@link LoaderManager}.
 *
 * @param <D> Type of the data that a loader will load for this assistant.
 * @author Martin Albedinsky
 */
public interface LoaderAssistant<D> extends LoaderManager.LoaderCallbacks<D> {

	/**
	 * Constant used to identify unspecified loader id.
	 */
	int NO_LOADER_ID = -1;

	/**
	 * Empty arguments for loader.
	 */
	@NonNull
	Bundle EMPTY_ARGUMENTS = Bundle.EMPTY;

	/**
	 * Starts a loader with the specified <var>loaderId</var> either via the associated LoaderManager's
	 * {@link LoaderManager#initLoader(int, Bundle, LoaderManager.LoaderCallbacks) initLoader(int, Bundle, LoaderManager.LoaderCallbacks)}
	 * or {@link LoaderManager#restartLoader(int, Bundle, LoaderManager.LoaderCallbacks) restartLoader(int, Bundle, LoaderManager.LoaderCallbacks)}
	 * depending on whether the requested loader is already initialized or not.
	 * <p>
	 * This assistant will be used as {@link LoaderManager.LoaderCallbacks} callback.
	 *
	 * @param loaderId  Id of the desired loader to start.
	 * @param arguments The desired arguments for the loader. Should be {@link #EMPTY_ARGUMENTS} if
	 *                  loader does not require any arguments.
	 * @return Initialized or re-started loader instance or {@code null} if this assistant does not
	 * create loader for the specified <var>id</var>.
	 * @see #initLoader(int, Bundle)
	 * @see #restartLoader(int, Bundle)
	 * @see #destroyLoader(int)
	 */
	@Nullable
	Loader<D> startLoader(@IntRange(from = 0) int loaderId, @NonNull Bundle arguments);

	/**
	 * Initializes a loader with the specified <var>loaderId</var> via the associated LoaderManager's
	 * {@link LoaderManager#initLoader(int, Bundle, LoaderManager.LoaderCallbacks) initLoader(int, Bundle, LoaderManager.LoaderCallbacks)}.
	 * <p>
	 * This assistant will be used as {@link LoaderManager.LoaderCallbacks} callback.
	 *
	 * @param loaderId  Id of the desired loader to initialize.
	 * @param arguments The desired arguments for the loader. Should be {@link #EMPTY_ARGUMENTS} if
	 *                  loader does not require any arguments.
	 * @return Initialized loader instance or {@code null} if this assistant does not create loader
	 * for the specified <var>id</var>.
	 * @see #startLoader(int, Bundle)
	 * @see #restartLoader(int, Bundle)
	 * @see #destroyLoader(int)
	 */
	@Nullable
	Loader<D> initLoader(@IntRange(from = 0) int loaderId, @NonNull Bundle arguments);

	/**
	 * Re-starts a loader with the specified <var>loaderId</var> via the associated LoaderManager's
	 * {@link LoaderManager#restartLoader(int, Bundle, LoaderManager.LoaderCallbacks) restartLoader(int, Bundle, LoaderManager.LoaderCallbacks)}.
	 * <p>
	 * This assistant will be used as {@link LoaderManager.LoaderCallbacks} callback.
	 *
	 * @param loaderId  Id of the desired loader to re-start.
	 * @param arguments The desired arguments for the loader. Should be {@link #EMPTY_ARGUMENTS} if
	 *                  loader does not require any arguments.
	 * @return Re-started loader instance or {@code null} if this assistant does not create loader
	 * for the specified <var>id</var>.
	 * @see #startLoader(int, Bundle)
	 * @see #initLoader(int, Bundle)
	 * @see #destroyLoader(int)
	 */
	@Nullable
	Loader<D> restartLoader(@IntRange(from = 0) int loaderId, @NonNull Bundle arguments);

	/**
	 * Returns a loader with the specified <var>loaderId</var> via the associated LoaderManager's
	 * {@link LoaderManager#getLoader(int) getLoader(int)}.
	 *
	 * @param loaderId Id of the desired loader to obtain.
	 * @return Requested loader or {@code null} if there is no such loader with the requested id.
	 * @see #startLoader(int, Bundle)
	 * @see #initLoader(int, Bundle)
	 * @see #restartLoader(int, Bundle)
	 * @see #destroyLoader(int)
	 */
	@Nullable
	Loader<D> getLoader(@IntRange(from = 0) int loaderId);

	/**
	 * Destroys a loader with the specified <var>loaderId</var> via the associated LoaderManager's
	 * {@link LoaderManager#destroyLoader(int) destroyLoader(int)}.
	 *
	 * @param loaderId Id of the desired loader to destroy.
	 * @return {@code True} if loader has been destroyed, {@code false} otherwise.
	 * @see #startLoader(int, Bundle)
	 * @see #initLoader(int, Bundle)
	 * @see #restartLoader(int, Bundle)
	 * @see #getLoader(int)
	 */
	boolean destroyLoader(@IntRange(from = 0) int loaderId);
}
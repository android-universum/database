/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

/**
 * An interface that is required for all data results that may be loaded via Android framework's
 * loaders. Whenever a new data are loaded, the previous need to be released via {@link #release()}
 * so the system may free theirs resources.
 *
 * @author Martin Albedinsky
 */
public interface DataResult {

	/**
	 * Called to release resources hold by this data result.
	 * <p>
	 * After this call, resources of this result are considered to be invalid.
	 */
	void release();
}
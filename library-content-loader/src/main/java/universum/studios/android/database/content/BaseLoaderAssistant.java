/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

/**
 * Base implementation of {@link LoaderAssistant} that may be used to implement loader assistants for
 * a specific types of results.
 *
 * <h3>Simple usage</h3>
 * <pre>
 * public class DetailActivity extends FragmentActivity {
 *
 *      // ... activity members
 *
 *      private static final int LOADER_ID = 0x01;
 *      private LoaderAssistant mLoaderAssistant;
 *
 *      &#64;Override
 *      protected void onCreateDB(Bundle savedInstanceState) {
 *          super.onCreate(savedInstanceState);
 *          mLoaderAssistant = new DetailLoaderAssistant(this, getLoaderManager());
 *          mLoaderAssistant.initLoader(LOADER_ID);
 *      }
 *
 *      // Re-starts the cursor loader to load new data from database.
 *      private void reloadUi() {
 *          mLoaderAssistant.startLoader(LOADER_ID);
 *      }
 *
 *      // Updates UI of this activity from the specified cursor.
 *      final void updateUi(&#64;NonNull Cursor cursor) {
 *          // ... update here UI of DetailActivity
 *      }
 *
 *      final class DetailLoaderAssistant extends BaseLoaderAssistant&lt;Cursor&gt; {
 *
 *          DetailLoaderAssistant(&#64;NonNull Context context, &#64;NonNull LoaderManager loaderManager) {
 *              super(loaderManager);
 *          }
 *
 *          &#64;Override
 *          public Loader&lt;Cursor&gt; onCreateLoader(&#64;IntRange(from = 0) int loaderId, &#64;Nullable Bundle params) {
 *              return new LoaderBuilder(LoremIpsumEntity.class)
 *                      .projection(LoremIpsum.class)
 *                      .build(mContext);
 *          }
 *
 *          &#64;Override
 *          protected void onLoadFinished(&#64;IntRange(from = 0) int loaderId, &#64;NonNull Cursor cursor) {
 *              updateUi(cursor);
 *          }
 *
 *          &#64;Override
 *          protected void onLoadFailed(&#64;IntRange(from = 0) int loaderId) {
 *              // Optional.
 *              // Invoked whenever the loaded cursor within onLoadFinished(Loader, Cursor) is null.
 *          }
 *
 *          &#64;Override
 *          protected void onLoaderReset(&#64;NonNull Loader&lt;Cursor&gt; loader) {
 *              // Optional.
 *          }
 *      }
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 */
public abstract class BaseLoaderAssistant<D> implements LoaderAssistant<D> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseLoaderAssistant";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Context that may be used to create loaders for this assistant.
	 */
	@NonNull
	protected final Context mContext;

	/**
	 * Manager used to perform loading operations.
	 */
	@NonNull
	protected final LoaderManager mLoaderManager;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseLoaderAssistant for the specified <var>loaderManager</var>.
	 *
	 * @param context       Context that may be used to create loaders for this assistant.
	 * @param loaderManager The manager that is used to perform content loading operations.
	 */
	public BaseLoaderAssistant(@NonNull final Context context, @NonNull final LoaderManager loaderManager) {
		this.mContext = context;
		this.mLoaderManager = loaderManager;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Nullable
	@Override
	public Loader<D> startLoader(@IntRange(from = 0) final int loaderId, @NonNull final Bundle arguments) {
		return mLoaderManager.getLoader(loaderId) == null ? initLoader(loaderId, arguments) : restartLoader(loaderId, arguments);
	}

	/**
	 */
	@Nullable
	@Override
	public Loader<D> initLoader(@IntRange(from = 0) final int loaderId, @NonNull final Bundle arguments) {
		return mLoaderManager.initLoader(loaderId, arguments, this);
	}

	/**
	 */
	@Nullable
	@Override
	public Loader<D> restartLoader(@IntRange(from = 0) final int loaderId, @NonNull final Bundle arguments) {
		return mLoaderManager.restartLoader(loaderId, arguments, this);
	}

	/**
	 */
	@Override
	@Nullable
	public Loader<D> getLoader(@IntRange(from = 0) final int loaderId) {
		return mLoaderManager.getLoader(loaderId);
	}

	/**
	 */
	@Override
	public boolean destroyLoader(@IntRange(from = 0) final int loaderId) {
		mLoaderManager.destroyLoader(loaderId);
		return true;
	}

	/**
	 */
	@Override
	public abstract Loader<D> onCreateLoader(@IntRange(from = 0) int loaderId, @Nullable Bundle arguments);

	/**
	 */
	@Override
	public void onLoadFinished(@NonNull final Loader<D> loader, @Nullable final D result) {
		final int loaderId = loader.getId();
		if (result == null) onLoadFailed(loaderId);
		else onLoadFinished(loaderId, result);
	}

	/**
	 * Invoked whenever {@link #onLoadFinished(Loader, Object)} is fired and the received <var>result</var>
	 * is valid (not {@code null}).
	 *
	 * @param loaderId Id of the loader for which has been loading finished.
	 * @param result   The result received in {@link #onLoadFinished(Loader, Object)}.
	 */
	protected abstract void onLoadFinished(@IntRange(from = 0) int loaderId, @NonNull D result);

	/**
	 * Invoked whenever {@link #onLoadFinished(Loader, Object)} is fired and the received results is
	 * {@code null}.
	 *
	 * @param loaderId Id of the loader for which has loading failed.
	 */
	protected void onLoadFailed(@IntRange(from = 0) int loaderId) {
		// Inheritance hierarchies may perform here logic related to failed loading of data.
	}

	/**
	 */
	@Override
	public void onLoaderReset(@NonNull Loader<D> loader) {
		// Inheritance hierarchies may perform here logic related to reset of data loading.
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
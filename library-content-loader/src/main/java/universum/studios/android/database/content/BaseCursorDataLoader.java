/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.CursorLoader;

/**
 * An {@link AsyncTaskLoader} implementation that may be used to load a specific data structure that
 * is desired to be created from a database {@link Cursor}.
 * <p>
 * A new instance of BaseCursorDataLoader can be created like {@link CursorLoader} via
 * {@link #BaseCursorDataLoader(Context, Uri, String[], String, String[], String)} constructor or via
 * {@link #BaseCursorDataLoader(Context, CursorLoader)} using instance of CursorLoader of which query
 * parameters will be copied for the new BaseCursorDataLoader instance.
 *
 * @param <D> Type of the data to be delivered by this loader to its registered callbacks.
 * @author Martin Albedinsky
 */
public abstract class BaseCursorDataLoader<D extends DataResult> extends AsyncTaskLoader<D> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseCursorDataLoader";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Content observer used to listen for changes in the content of the associated loaded Cursor.
	 */
	private final ForceLoadContentObserver mObserver;

	/**
	 * Content uri used to query desired data from.
	 */
	private final Uri mUri;

	/**
	 * Projection statement for the query.
	 */
	private String[] mProjection;

	/**
	 * Selection statement for the query.
	 */
	private String mSelection;

	/**
	 * Arguments for {@link #mSelection}.
	 */
	private String[] mSelectionArgs;

	/**
	 * Sort order statement for the query.
	 */
	private String mSortOrder;

	/**
	 * Signal used to cancel loading in the background for this loader.
	 */
	@Nullable
	private CancellationSignal mCancellationSignal;

	/**
	 * Current result data loaded and processed by this loader (if any).
	 */
	private D mData;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseCursorLoader with <b>query parameters</b> copied from the given
	 * <var>cursorLoader</var>.
	 *
	 * @param context      Context used to access {@link ContentResolver}.
	 * @param cursorLoader The loader used to configure the new loader instance using its parameters
	 *                     like <b>uri</b>, <b>selection</b>, <b>...</b>.
	 * @see ContentResolver#query(Uri, String[], String, String[], String)
	 */
	public BaseCursorDataLoader(@NonNull Context context, @NonNull CursorLoader cursorLoader) {
		this(
				context,
				cursorLoader.getUri(),
				cursorLoader.getProjection(),
				cursorLoader.getSelection(),
				cursorLoader.getSelectionArgs(),
				cursorLoader.getSortOrder()
		);
	}

	/**
	 * Creates a new instance of BaseCursorLoader with the specified parameters
	 *
	 * @param context       Context used to access {@link ContentResolver}.
	 * @param uri           The desired content uri for the query.
	 * @param projection    The desired projection for the query.
	 * @param selection     The desired selection for the query.
	 * @param selectionArgs The desired arguments for the <var>selection</var>.
	 * @param sortOrder     The desired sort order for the query.
	 * @see ContentResolver#query(Uri, String[], String, String[], String)
	 * @see #getUri()
	 * @see #getProjection()
	 * @see #getSelection()
	 * @see #getSelectionArguments()
	 * @see #getSortOrder()
	 */
	public BaseCursorDataLoader(
			@NonNull Context context,
			@NonNull Uri uri,
			@Nullable String[] projection,
			@Nullable String selection,
			@Nullable String[] selectionArgs,
			@Nullable String sortOrder
	) {
		super(context);
		this.mObserver = new ForceLoadContentObserver();
		this.mUri = uri;
		this.mProjection = projection;
		this.mSelection = selection;
		this.mSelectionArgs = selectionArgs;
		this.mSortOrder = sortOrder;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the content Uri specified for this loader.
	 *
	 * @return Uri used for the query operation.
	 * @see #BaseCursorDataLoader(Context, Uri, String[], String, String[], String)
	 * @see #query()
	 */
	@NonNull
	public final Uri getUri() {
		return mUri;
	}

	/**
	 * Specifies a projection with column names to be used by this loader for query operation.
	 *
	 * @param projection The desired array with column names. May be {@code null} to clear the
	 *                   previous one.
	 * @see #getProjection()
	 */
	public final void setProjection(@Nullable final String[] projection) {
		this.mProjection = projection;
	}

	/**
	 * Returns the array with column names to be projected by query result of this loader.
	 *
	 * @return Columns projection for the query operation or {@code null} if no projection has been
	 * specified.
	 * @see #setProjection(String[])
	 * @see #BaseCursorDataLoader(Context, Uri, String[], String, String[], String)
	 * @see #query()
	 */
	@Nullable
	public final String[] getProjection() {
		return mProjection;
	}

	/**
	 * Specifies a selection statement to be used by this loader for query operation.
	 *
	 * @param selection The desired selection statement. May be {@code null} to clear the previous
	 *                  one.
	 * @see #getSelection()
	 * @see #setSelectionArguments(String[])
	 */
	public final void setSelection(@Nullable final String selection) {
		this.mSelection = selection;
	}

	/**
	 * Returns the selection statement specified for this loader.
	 *
	 * @return Selection statement for the query operation or {@code null} if no selection has been
	 * specified.
	 * @see #setSelection(String)
	 * @see #BaseCursorDataLoader(Context, Uri, String[], String, String[], String)
	 * @see #query()
	 */
	@Nullable
	public final String getSelection() {
		return mSelection;
	}

	/**
	 * Specifies arguments for selection statement to be used by this loader for query operation.
	 *
	 * @param arguments The desired array of string arguments for the selection. May be {@code null}
	 *                  to clear the previous ones.
	 * @see #getSelectionArguments()
	 * @see #setSelection(String)
	 */
	public final void setSelectionArguments(@Nullable final String[] arguments) {
		this.mSelectionArgs = arguments;
	}

	/**
	 * Returns the array with arguments for selection statement specified for this loader.
	 *
	 * @return Selection arguments for the query operation or {@code null} if no arguments have been
	 * specified.
	 * @see #setSelectionArguments(String[])
	 * @see #BaseCursorDataLoader(Context, Uri, String[], String, String[], String)
	 * @see #query()
	 */
	@Nullable
	public final String[] getSelectionArguments() {
		return mSelectionArgs;
	}

	/**
	 * Specifies a sort order statement to be used by this loader for query operation.
	 *
	 * @param sortOrder The desired sort order statement. May be {@code null} to clear the previous
	 *                  one.
	 * @see #getSortOrder()
	 */
	public final void setSortOrder(@Nullable final String sortOrder) {
		this.mSortOrder = sortOrder;
	}

	/**
	 * Returns the sort order statement specified for this loader.
	 *
	 * @return Sort order statement for the query operation or {@code null} if no sort order has
	 * been specified.
	 * @see #setSortOrder(String)
	 * @see #BaseCursorDataLoader(Context, Uri, String[], String, String[], String)
	 * @see #query()
	 */
	@Nullable
	public final String getSortOrder() {
		return mSortOrder;
	}

	/**
	 */
	@Override
	public D loadInBackground() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			synchronized (this) {
				if (isLoadInBackgroundCanceled()) {
					throw new OperationCanceledException();
				}
				this.mCancellationSignal = new CancellationSignal();
			}
		}
		try {
			final Cursor cursor = query();
			if (cursor != null) {
				try {
					// Ensure that the cursor window is filled.
					cursor.getCount();
					cursor.registerContentObserver(mObserver);
				} catch (RuntimeException e) {
					cursor.close();
					throw e;
				}
			}
			return cursor == null ? onLoadFailed() : onLoadFinished(cursor);
		} finally {
			synchronized (this) {
				this.mCancellationSignal = null;
			}
		}
	}

	/**
	 * Invoked from within {@link #loadInBackground()} whenever {@link #query()} method has returned
	 * the valid <var>cursor</var>.
	 * <p>
	 * Subclasses of this loader should process here the given cursor and prepare the result data
	 * to be delivered to this loader's registered callbacks.
	 *
	 * @param cursor The cursor queried via {@link #query()} method.
	 * @return Data to be delivered as result to registered callbacks. Should not be {@code null}.
	 */
	@Nullable
	protected abstract D onLoadFinished(@NonNull Cursor cursor);

	/**
	 * Invoked from within {@link #loadInBackground()} if {@link #query()} method has returned invalid
	 * ({@code null}) cursor.
	 *
	 * @return Data to be delivered as result to registered callbacks. May be {@code nul}.
	 */
	@Nullable
	protected D onLoadFailed() {
		return null;
	}

	/**
	 * Performs query operation via {@link ContentResolver} for the query parameters specified for
	 * this loader.
	 *
	 * @return Cursor with data queried for this loader's parameters. May be {@code null} if query
	 * fails.
	 * @see #query(Uri, String[], String, String[], String)
	 * @see #getUri()
	 * @see #getProjection()
	 * @see #getSelection()
	 * @see #getSelectionArguments()
	 * @see #getSortOrder()
	 */
	@Nullable
	protected final Cursor query() {
		return query(mUri, mProjection, mSelection, mSelectionArgs, mSortOrder);
	}

	/**
	 * Performs query operation via {@link ContentResolver} for the given query parameters.
	 * <p>
	 * If this method is called during execution of {@link #loadInBackground()} it will use the current
	 * {@link CancellationSignal} created to allow cancellation of the query operation in case when
	 * this loader receives {@link #cancelLoadInBackground()} call.
	 *
	 * @param uri           Uri of the content for which to perform query.
	 * @param projection    The desired projection for the query.
	 * @param selection     The desired selection for the query.
	 * @param selectionArgs The desired arguments for the <var>selection</var>.
	 * @param sortOrder     The desired sort order for the query.
	 * @return Cursor with data queried for the specified parameters. May be {@code null} if query
	 * fails.
	 */
	@Nullable
	@SuppressWarnings("NewApi")
	protected final Cursor query(
			@NonNull Uri uri,
			@Nullable String[] projection,
			@Nullable String selection,
			@Nullable String[] selectionArgs,
			@Nullable String sortOrder
	) {
		final ContentResolver contentResolver = getContext().getContentResolver();
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ?
				contentResolver.query(
						uri,
						projection,
						selection,
						selectionArgs,
						sortOrder,
						mCancellationSignal
				) :
				contentResolver.query(
						uri,
						projection,
						selection,
						selectionArgs,
						sortOrder
				);
	}

	/**
	 */
	@Override
	public void deliverResult(@Nullable final D data) {
		if (isReset()) {
			this.releaseResult(data);
			return;
		}
		final D oldResult = this.mData;
		this.mData = data;
		if (isStarted()) {
			super.deliverResult(data);
		}
		if (oldResult != null && !oldResult.equals(data)) {
			this.releaseResult(oldResult);
		}
	}

	/**
	 */
	@Override
	protected void onStartLoading() {
		if (mData != null) {
			deliverResult(mData);
		}
		if (takeContentChanged() || mData == null) {
			forceLoad();
		}
	}

	/**
	 */
	@Override
	protected void onStopLoading() {
		cancelLoad();
	}

	/**
	 */
	@Override
	protected void onReset() {
		onStopLoading();
		this.releaseResult(mData);
		this.mData = null;
	}

	/**
	 */
	@Override
	public void onCanceled(@Nullable final D data) {
		this.releaseResult(data);
	}

	/**
	 * Releases resources of the specified <var>data</var> object.
	 *
	 * @param data The result data of which resources to release.
	 * @see DataResult#release()
	 */
	private void releaseResult(final D data) {
		if (data != null) data.release();
	}

	/**
	 */
	@Override
	public void cancelLoadInBackground() {
		super.cancelLoadInBackground();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			synchronized (this) {
				if (mCancellationSignal != null) {
					mCancellationSignal.cancel();
				}
			}
		}
	}

	/**
	 */
	@Override
	public void dump(String prefix, FileDescriptor fd, PrintWriter writer, String[] args) {
		super.dump(prefix, fd, writer, args);
		writer.print(prefix);
		writer.print("mUri=");
		writer.println(mUri);
		writer.print(prefix);
		writer.print("mProjection=");
		writer.println(Arrays.toString(mProjection));
		writer.print(prefix);
		writer.print("mSelection=");
		writer.println(mSelection);
		writer.print(prefix);
		writer.print("mSelectionArgs=");
		writer.println(Arrays.toString(mSelectionArgs));
		writer.print(prefix);
		writer.print("mSortOrder=");
		writer.println(mSortOrder);
		writer.print(prefix);
		writer.print("mData=");
		writer.println(mData);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
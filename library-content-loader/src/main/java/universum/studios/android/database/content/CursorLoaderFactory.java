/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

/**
 * A CursorLoaderFactory is an utility class that may be used directly to create instances of
 * {@link CursorLoader CursorLoaders} or as base for factory that creates these loaders for an
 * Android application.
 *
 * @author Martin Albedinsky
 * @see CursorLoaderBuilder
 */
public abstract class CursorLoaderFactory {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CursorLoaderFactory";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new empty instance of CursorLoaderFactory. Inheritance hierarchies should declare
	 * theirs constructors private in order to became a standard utility classes.
	 */
	protected CursorLoaderFactory() {
		// We allow to override this class only so its inheritance hierarchies may declare other
		// factory methods for loader creation.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of {@link CursorLoader} for the specified parameters.
	 *
	 * @param context Context to create loader.
	 * @param uri     The desired content Uri for which should be loader created.
	 * @return New instance of cursor loader with the given uri and all parameters specified as {@code null}.
	 */
	@NonNull
	public static Loader<Cursor> createLoader(@NonNull final Context context, @NonNull final Uri uri) {
		return createLoader(context, uri, null, null, null, null);
	}

	/**
	 * Creates a new instance of {@link CursorLoader} for the specified parameters.
	 *
	 * @param context       Context to create loader.
	 * @param uri           The desired content Uri for which should be loader created.
	 * @param projection    Projection for queried cursor.
	 * @param selection     Selection statement for queried cursor.
	 * @param selectionArgs Arguments for the selection statement.
	 * @param sortOrder     Sort order statement for queried cursor.
	 * @return New instance of cursor loader with the given parameters.
	 */
	@NonNull
	public static Loader<Cursor> createLoader(
			@NonNull final Context context,
			@NonNull final Uri uri,
			@Nullable final String[] projection,
			@Nullable final String selection,
			@Nullable final String[] selectionArgs,
			@Nullable String sortOrder
	) {
		return new CursorLoader(context, uri, projection, selection, selectionArgs, sortOrder);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
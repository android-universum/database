Database-Content-Loader
===============

This module contains elements that may be used to simplify usage of `Loaders` in an **Android** application.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-content-loader:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseCursorDataLoader](https://bitbucket.org/android-universum/database/src/main/library-content-loader/src/main/java/universum/studios/android/database/content/BaseCursorDataLoader.java)
- [BaseLoaderAssistant](https://bitbucket.org/android-universum/database/src/main/library-content-loader/src/main/java/universum/studios/android/database/content/BaseLoaderAssistant.java)
- [CursorLoaderBuilder](https://bitbucket.org/android-universum/database/src/main/library-content-loader/src/main/java/universum/studios/android/database/content/CursorLoaderBuilder.java)
Database-Base
===============

This module contains base elements that provide simple entry point and usage of `SQLite` database
in an **Android** application.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-base:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-query](https://bitbucket.org/android-universum/database/src/main/library-query)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [DatabaseProvider](https://bitbucket.org/android-universum/database/src/main/library-base/src/main/java/universum/studios/android/database/DatabaseProvider.java)
- [Database](https://bitbucket.org/android-universum/database/src/main/library-base/src/main/java/universum/studios/android/database/Database.java)
- [DatabaseEntity](https://bitbucket.org/android-universum/database/src/main/library-base/src/main/java/universum/studios/android/database/DatabaseEntity.java)
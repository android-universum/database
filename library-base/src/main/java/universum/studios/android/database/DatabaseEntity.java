/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * DatabaseEntity specifies a base API layer that is required to be provided by each entity instance
 * that can be assigned to a particular {@link Database} via {@link Database#assignEntity(Class)} or
 * via {@link universum.studios.android.database.annotation.DatabaseEntities @DatabaseEntities} annotation.
 *
 * @author Martin Albedinsky
 */
public interface DatabaseEntity {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant used to identify that no rows has been affected by the executed content operation.
	 */
	int NO_ROWS = Database.NO_ROWS;

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Interface for dispatcher that may be used by implementations of {@link DatabaseEntity} to handle
	 * dispatching of database content related operations like {@code CREATE, UPDATE, QUERY, ...}.
	 *
	 * @author Martin Albedinsky
	 */
	interface ContentOperationDispatcher {

		/**
		 * Called to perform bulk insert of the given content <var>values</var> set into the table
		 * represented by this database entity.
		 *
		 * @param entity The database entity for which to perform the desired content operation.
		 * @param values The desired set of values to insert into database.
		 * @return Count of rows successfully inserted into database.
		 */
		int dispatchBulkInsert(@NonNull DatabaseEntity entity, @NonNull ContentValues[] values);

		/**
		 * Called to insert the given content <var>values</var> into the table represented by this
		 * database entity.
		 *
		 * @param entity The database entity for which to perform the desired content operation.
		 * @param values The desired values to insert into database.
		 * @return Uri of the newly inserted row or {@code null} if database transaction failed.
		 */
		Uri dispatchInsert(@NonNull DatabaseEntity entity, @Nullable ContentValues values);

		/**
		 * Called to query a content of the table represented by this database entity.
		 * <p>
		 * This is called whenever {@link Database#query(Uri, String[], String, String[], String)} is
		 * called with uri of this database entity for the database to which is this entity attached.
		 *
		 * @param entity        The database entity for which to perform the desired query operation.
		 * @param projection    The list of columns to put into the cursor. If {@code null} all columns
		 *                      are included.
		 * @param selection     A selection criteria to apply when filtering rows. If {@code null} then
		 *                      all rows are included.
		 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
		 *                      values from <var>selectionArgs</var>, in order that they appear in the
		 *                      selection. The values will be bound as Strings.
		 * @param sortOrder     How the rows in the cursor should be sorted. If {@code null} then the
		 *                      entity is free to define the sort order.
		 * @return Queried Cursor for the specified parameters or {@code null} if query operation failed.
		 */
		@NonNull
		Cursor dispatchQuery(@NonNull DatabaseEntity entity, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder);

		/**
		 * Called to update all rows targeted by the given <var>selection</var> + <var>selectionArgs</var>
		 * within the table represented by this database entity.
		 * <p>
		 * This is called whenever {@link Database#update(Uri, ContentValues, String, String[])} is called
		 * with uri of this database entity for the database to which is this entity attached.
		 *
		 * @param entity        The database entity for which to perform the desired content operation.
		 * @param values        Content values to update the desired rows with.
		 * @param selection     A selection criteria to apply when filtering rows to update.
		 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
		 *                      values from <var>selectionArgs</var>, in order that they appear in the
		 *                      selection. The values will be bound as Strings.
		 * @return Count of rows affected by the operation or {@link #NO_ROWS} if database transaction failed.
		 */
		int dispatchUpdate(@NonNull DatabaseEntity entity, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs);

		/**
		 * Called to delete all rows targeted by the given <var>selection</var> + <var>selectionArgs</var>
		 * from the table represented by this database entity.
		 *
		 * @param entity        The database entity for which to perform the desired content operation.
		 * @param selection     A selection criteria to apply when filtering rows to delete.
		 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
		 *                      values from <var>selectionArgs</var>, in order that they appear in the
		 *                      selection. The values will be bound as Strings.
		 * @return Count of rows affected by the operation or {@link #NO_ROWS} if database transaction failed.
		 */
		int dispatchDelete(@NonNull DatabaseEntity entity, @Nullable String selection, @Nullable String[] selectionArgs);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the name of this database entity.
	 *
	 * @return Entity name.
	 */
	@NonNull
	String getName();

	/**
	 * Returns the uri of the content of this database entity.
	 *
	 * @return Content uri.
	 */
	@NonNull
	Uri getContentUri();

	/**
	 * Returns the Mime type of the content of this database entity.
	 *
	 * @return Content Mime type.
	 */
	@NonNull
	String getMimeType();

	/**
	 * Specifies a dispatcher that should be used by this database entity to handle dispatching of
	 * operations related to this database entity's content.
	 *
	 * @param dispatcher The desired dispatcher. May be {@code null} to use a default one.
	 */
	void setContentOperationDispatcher(@Nullable ContentOperationDispatcher dispatcher);

	/**
	 * Attaches this database entity to the given <var>database</var> instance.
	 * <p>
	 * <b>Note, that each entity instance may be attached to its associated database only once and
	 * any subsequent calls to this method should throw an exception.</b>
	 *
	 * @param database The database to which should be this entity attached.
	 * @throws IllegalStateException If this database entity is already attached to some database.
	 * @see #isAttachedToDatabase()
	 */
	void attachToDatabase(@NonNull Database database);

	/**
	 * Checks whether this database entity is attached to database or not.
	 *
	 * @return {@code True} if this entity instance is attached to database, {@code false} otherwise.
	 * @see #attachToDatabase(Database)
	 * @see #getDatabase()
	 */
	boolean isAttachedToDatabase();

	/**
	 * Returns the database to which is this database entity attached.
	 *
	 * @return Attached database instance.
	 * @throws IllegalStateException If this database entity is not attached to any database.
	 * @see #isAttachedToDatabase()
	 */
	@NonNull
	Database getDatabase();

	/**
	 * Called to create the table and its structure represented by this database entity.
	 *
	 * @param db SQLite database object managed by the parent database.
	 */
	void dispatchCreate(@NonNull SQLiteDatabase db);

	/**
	 * Returns flag indicating whether this database entity requires insertion of initial content
	 * when its table is being for the first time created.
	 * <p>
	 * If required, {@link #dispatchCreateInitialContent(SQLiteDatabase)} will be called upon this
	 * database entity.
	 *
	 * @return {@code True} if initial content need to be created, {@code false} otherwise.
	 * @see #createsInitialContent(int, int)
	 */
	boolean createsInitialContent();

	/**
	 * Called to insert an initial content into the table represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#onCreateInitialContent(SQLiteDatabase)} is called for
	 * the database to which is this entity attached.
	 *
	 * @param db SQLite database object managed by the parent database.
	 * @see #createsInitialContent()
	 */
	void dispatchCreateInitialContent(@NonNull SQLiteDatabase db);

	/**
	 * Returns flag indicating whether this database entity requires insertion of initial content
	 * when its table is being for the first time created on database upgrade.
	 * <p>
	 * If required, {@link #dispatchCreateInitialContent(SQLiteDatabase, int, int)} will be called
	 * upon this database entity.
	 *
	 * @return {@code True} if initial content according to the specified database version need to
	 * be created, {@code false} otherwise.
	 * @see #createsInitialContent()
	 */
	boolean createsInitialContent(int oldVersion, int newVersion);

	/**
	 * Called to insert an initial content into the table represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#onUpgradeContent(SQLiteDatabase, int, int)} is called
	 * for the database to which is this entity attached and this entity is being for the first time
	 * created.
	 *
	 * @param db         SQLite database object managed by the parent database.
	 * @param oldVersion An old version of the current database.
	 * @param newVersion A new version of the updating database.
	 * @see #createsInitialContent(int, int)
	 */
	void dispatchCreateInitialContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion);

	/**
	 * Called to upgrade a table structure represented by this database entity.
	 * <p>
	 * This is called from the Database to which is this entity assigned, from within
	 * {@link Database#onUpgradeDB(SQLiteDatabase, int, int)}.
	 *
	 * @param db         SQLite database object managed by the parent database.
	 * @param oldVersion An old version of the current database.
	 * @param newVersion A new version of the updating database.
	 */
	void dispatchUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion);

	/**
	 * Returns a flag indicating whether this database entity requires upgrade of its content
	 * whenever a database is being upgraded.
	 * <p>
	 * If required, {@link #dispatchUpgradeContent(SQLiteDatabase, int, int)} will be called upon
	 * this database entity.
	 *
	 * @return {@code True} if content need to be upgraded, {@code false} otherwise.
	 */
	boolean upgradesContent(int oldVersion, int newVersion);

	/**
	 * Called to upgrade a content of the table represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#onUpgradeContent(SQLiteDatabase, int, int)} is called
	 * for the database to which is this entity attached.
	 *
	 * @param db         SQLite database object managed by the parent database.
	 * @param oldVersion An old version of the database.
	 * @param newVersion A new version to which has been database upgraded.
	 * @see #upgradesContent(int, int)
	 */
	void dispatchUpgradeContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion);

	/**
	 * Called to downgrade the table structure represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#onDowngradeDB(SQLiteDatabase, int, int)} is called for
	 * the database to which is this entity attached.
	 *
	 * @param db         SQLite database object managed by the parent database.
	 * @param oldVersion An old version of the current database.
	 * @param newVersion A new version of the downgrading database.
	 */
	void dispatchDowngrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion);

	/**
	 * Called to perform bulk insert of the given content <var>values</var> set into the table
	 * represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#bulkInsert(Uri, ContentValues[])} is called with uri
	 * of this database entity from the database to which is this entity attached.
	 *
	 * @param db     SQLite database object managed by the parent database.
	 * @param values The desired set of values to insert into database.
	 * @return Count of rows successfully inserted into database.
	 */
	int dispatchBulkInsert(@NonNull SQLiteDatabase db, @NonNull ContentValues[] values);

	/**
	 * Called to insert the given content <var>values</var> into the table represented by this
	 * database entity.
	 * <p>
	 * This is called whenever {@link Database#insert(Uri, ContentValues)} is called with uri of this
	 * database entity for the database to which is this entity attached.
	 *
	 * @param db     SQLite database object managed by the parent database.
	 * @param values The desired values to insert into database.
	 * @return Uri of the newly inserted row or {@code null} if database transaction failed.
	 */
	Uri dispatchInsert(@NonNull SQLiteDatabase db, @Nullable ContentValues values);

	/**
	 * Called to query a content of the table represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#query(Uri, String[], String, String[], String)} is
	 * called with uri of this database entity for the database to which is this entity attached.
	 *
	 * @param db            SQLite database object managed by the parent database.
	 * @param projection    The list of columns to put into the cursor. If {@code null} all columns
	 *                      are included.
	 * @param selection     A selection criteria to apply when filtering rows. If {@code null} then
	 *                      all rows are included.
	 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
	 *                      values from <var>selectionArgs</var>, in order that they appear in the
	 *                      selection. The values will be bound as Strings.
	 * @param sortOrder     How the rows in the cursor should be sorted. If {@code null} then the
	 *                      entity is free to define the sort order.
	 * @return Queried Cursor for the specified parameters or {@code null} if query operation failed.
	 */
	@NonNull
	Cursor dispatchQuery(@NonNull SQLiteDatabase db, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder);

	/**
	 * Called to update all rows targeted by the given <var>selection</var> + <var>selectionArgs</var>
	 * within the table represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#update(Uri, ContentValues, String, String[])} is called
	 * with uri of this database entity for the database to which is this entity attached.
	 *
	 * @param db            SQLite database object managed by the parent database.
	 * @param values        Content values to update the desired rows with.
	 * @param selection     A selection criteria to apply when filtering rows to update.
	 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
	 *                      values from <var>selectionArgs</var>, in order that they appear in the
	 *                      selection. The values will be bound as Strings.
	 * @return Count of rows affected by the operation or {@link #NO_ROWS} if database transaction failed.
	 */
	int dispatchUpdate(@NonNull SQLiteDatabase db, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs);

	/**
	 * Called to delete all rows targeted by the given <var>selection</var> + <var>selectionArgs</var>
	 * from the table represented by this database entity.
	 * <p>
	 * This is called whenever {@link Database#delete(Uri, String, String[])} is called with uri of
	 * this database entity for the database to which is this entity attached.
	 *
	 * @param db            SQLite database object managed by the parent database.
	 * @param selection     A selection criteria to apply when filtering rows to delete.
	 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
	 *                      values from <var>selectionArgs</var>, in order that they appear in the
	 *                      selection. The values will be bound as Strings.
	 * @return Count of rows affected by the operation or {@link #NO_ROWS} if database transaction failed.
	 */
	int dispatchDelete(@NonNull SQLiteDatabase db, @Nullable String selection, @Nullable String[] selectionArgs);

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.Database;
import universum.studios.android.database.DatabaseEntity;
import universum.studios.android.database.annotation.DatabaseEntities;

/**
 * An {@link AnnotationHandlers} implementation providing {@link AnnotationHandler} instances for
 * classes from the database base package.
 *
 * @author Martin Albedinsky
 */
public final class BaseAnnotationHandlers extends AnnotationHandlers {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "AnnotationHandlers";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private BaseAnnotationHandlers() {
		super();
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains a {@link DatabaseAnnotationHandler} implementation for the given <var>classOfDatabase</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	public static DatabaseAnnotationHandler obtainDatabaseHandler(@NonNull Class<? extends Database> classOfDatabase) {
		return obtainHandler(DatabaseHandler.class, classOfDatabase);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link DatabaseAnnotationHandler} implementation for {@link Database} class.
	 */
	@SuppressWarnings("WeakerAccess") static final class DatabaseHandler extends BaseAnnotationHandler implements DatabaseAnnotationHandler {

		/**
		 * Same as {@link BaseAnnotationHandler#BaseAnnotationHandler(Class, Class)} with
		 * {@link Database} as <var>maxSuperClass</var>.
		 */
		public DatabaseHandler(@NonNull Class<?> annotatedClass) {
			super(annotatedClass, Database.class);
		}

		/**
		 */
		@Nullable
		@Override
		public Class<? extends DatabaseEntity>[] getEntities() {
			final DatabaseEntities databaseEntities = findAnnotation(DatabaseEntities.class);
			return databaseEntities == null ? null : databaseEntities.value();
		}
	}
}
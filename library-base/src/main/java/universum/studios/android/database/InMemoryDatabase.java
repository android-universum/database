/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

/**
 * A {@link Database} implementation that can be used to store data <b>in memory</b>. In memory
 * database is created on Android application's start-up (or when it for the first time accessed,
 * depends on setting) and then destroyed when application is also destroyed. This type of database
 * may be used to store data only <b>temporarily</b> due to its lifecycle described above.
 *
 * @author Martin Albedinsky
 */
public abstract class InMemoryDatabase extends Database {

	/**
	 * Creates a new InMemoryDatabase that can be used to store data <b>temporarily</b> in memory.
	 * <p>
	 * <b>Note, that In Memory database does not have name.</b>
	 */
	public InMemoryDatabase() {
		super(1, "");
	}
}
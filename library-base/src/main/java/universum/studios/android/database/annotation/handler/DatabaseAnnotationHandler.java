/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import androidx.annotation.Nullable;
import universum.studios.android.database.Database;
import universum.studios.android.database.DatabaseEntity;
import universum.studios.android.database.annotation.DatabaseEntities;

/**
 * An {@link AnnotationHandler} extended interface for annotation handlers from the Database library
 * that are used to handle processing of annotations attached to classes derived from {@link Database}
 * class provided by this library.
 *
 * @author Martin Albedinsky
 */
public interface DatabaseAnnotationHandler extends AnnotationHandler {

	/**
	 * Returns the array with database entity classes obtained from {@link DatabaseEntities @DatabaseEntities}
	 * annotation (if presented).
	 *
	 * @return Array with attached database entity classes or {@code null} it there is no annotation
	 * presented.
	 */
	@Nullable
	Class<? extends DatabaseEntity>[] getEntities();
}
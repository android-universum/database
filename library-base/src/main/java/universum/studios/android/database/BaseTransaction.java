/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link Transaction} base implementation which supports setting of mode via {@link #setMode(int)}
 * and retrieving it via {@link #getMode()}. This base class also adds interface for asynchronous
 * execution via {@link #executeAsync()}. A desired executor that should be used for asynchronous
 * execution may be specified via {@link #setExecutor(Executor)}.
 *
 * @author Martin Albedinsky
 */
public abstract class BaseTransaction<R> implements Transaction<R> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseTransaction";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Mode specified for this transaction.
	 */
	private int mMode = EXCLUSIVE;

	/**
	 * Executor to be used for asynchronous execution of this transaction.
	 */
	private Executor mExecutor = EXECUTOR;

	/**
	 * Boolean indicating whether this transaction is being executed or not.
	 */
	private final AtomicBoolean mBeingExecuted = new AtomicBoolean(false);

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * @see #getMode()
	 */
	@Override
	public final void setMode(@Mode int mode) {
		if (mBeingExecuted.get()) {
			throw new IllegalStateException("Cannot change mode of transaction that is being executed.");
		}
		this.mMode = mode;
	}

	/**
	 * Returns the mode specified for this transaction.
	 *
	 * @return This transaction's mode. One of modes defined by {@link Mode @Mode} annotation.
	 * @see #setMode(int)
	 */
	@Mode
	public final int getMode() {
		return mMode;
	}

	/**
	 * Sets an executor to be used when executing this transaction asynchronously via {@link #executeAsync()}.
	 *
	 * @param executor The desired executor to be used. May be {@code null} to use a default one.
	 * @see #EXECUTOR
	 * @see #getExecutor()
	 */
	public final void setExecutor(@Nullable Executor executor) {
		this.mExecutor = executor == null ? EXECUTOR : executor;
	}

	/**
	 * Returns the executor specified for this transaction to be used for asynchronous execution.
	 *
	 * @return This transaction's executor.
	 * @see #setExecutor(Executor)
	 */
	@NonNull
	public final Executor getExecutor() {
		return mExecutor;
	}

	/**
	 * Like {@link #execute()} but this method may be used to execute this transaction asynchronously.
	 *
	 * @see #setExecutor(Executor)
	 */
	public abstract void executeAsync();

	/**
	 */
	@Nullable
	@Override
	public final R execute() {
		mBeingExecuted.set(true);
		final R result = onExecute();
		mBeingExecuted.set(false);
		return result;
	}

	/**
	 * Invoked whenever {@link #execute()} is called for this transaction.
	 *
	 * @return Result of this transaction.
	 */
	@Nullable
	protected abstract R onExecute();

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link BaseTransaction} implementation that may be used to centralize a specific database
 * transaction logic (select, insert, update, delete, or theirs combination) into one place to allow
 * re-use of such logic and also to lighten execution of such queries from within Android application.
 * <p>
 * A specific database transaction can be executed via {@link #execute()}. Implementations of this
 * transaction class need to implement {@link #onExecute(Database)} where will be passed Database
 * instance for the class specified during initialization  via {@link #DatabaseTransaction(Class)}.
 * If some exception occurs during execution of database transaction, then {@link #onError(Database, Throwable)}
 * will be invoked with that exception.
 * <p>
 * Database transaction can be also executed asynchronously via {@link #executeAsync()} in such case
 * {@link #onExecuteAsync(Database)} will be invoked. By default, DatabaseTransaction uses {@link AsyncTask}
 * for asynchronous execution and default {@link #EXECUTOR} as executor. The executor may be changed
 * via {@link #setExecutor(java.util.concurrent.Executor)}.
 *
 * @param <D> Type of the database upon which will be this transaction executing operations.
 * @author Martin Albedinsky
 */
public abstract class DatabaseTransaction<R, D extends Database> extends BaseTransaction<R> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DatabaseTransaction";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Class of the database upon which will be this transaction executing its specific queries.
	 */
	private final Class<D> mDatabaseClass;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DatabaseTransaction for database with the specified <var>classOfDatabase</var>
	 *
	 * @param classOfDatabase Class of the database upon which will be the new transaction executing
	 *                        operations.
	 */
	public DatabaseTransaction(@NonNull Class<D> classOfDatabase) {
		super();
		this.mDatabaseClass = classOfDatabase;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	public void executeAsync() {
		onExecuteAsync(DatabaseProvider.provideDatabase(mDatabaseClass));
	}

	/**
	 * Like {@link #onExecute(Database)} but this is invoked due to call to {@link #executeAsync()}.
	 * <p>
	 * By default this will execute asynchronous task to perform execution of {@link #onExecute(Database)}
	 * asynchronously. Inheritance hierarchies may override this method to perform custom asynchronous
	 * execution.
	 * <p>
	 * If {@link #onExecute(Database)} finishes successfully {@link #onExecutedAsync(Database, Object)}
	 * is invoked, otherwise {@link #onError(Database, Throwable)}.
	 */
	protected void onExecuteAsync(@NonNull D database) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			new AsyncTransactionTask<>(this, database).executeOnExecutor(getExecutor());
		} else {
			new AsyncTransactionTask<>(this, database).execute();
		}
	}

	/**
	 * Invoked after {@link #onExecute(Database)} has finished asynchronously after call to
	 * {@link #onExecuteAsync(Database)}.
	 * <p>
	 * <b>Note</b>, that this is always invoked on the UI thread.
	 *
	 * @param database The database instance specific for this transaction.
	 * @param result   Result returned via {@link #onExecute(Database)} or via {@link #onError(Database, Throwable)}.
	 */
	protected void onExecutedAsync(@NonNull D database, @Nullable R result) {
		// Inheritance hierarchies may perform here logic related to finished asynchronous execution.
	}

	/**
	 * @see #onSuccess(Database, Object)
	 * @see #onError(Database, Throwable)
	 */
	@Nullable
	@Override
	protected R onExecute() {
		final D database = DatabaseProvider.provideDatabase(mDatabaseClass);
		final int mode = getMode();
		if (mode != NONE) {
			database.beginTransaction(mode);
		}
		R result;
		boolean success = true;
		try {
			result = onExecute(database);
			if (mode != NONE) {
				database.setTransactionSuccessful();
			}
		} catch (Exception error) {
			success = false;
			result = onError(database, error);
		} finally {
			if (mode != NONE) {
				database.endTransaction();
			}
		}
		if (success) {
			onSuccess(database, result);
		}
		return result;
	}

	/**
	 * Invoked from {@link #onExecute()} to perform execution of database operations specific for this
	 * transaction.
	 * <p>
	 * <b>Note</b>, that this will be invoked on the same thread as {@link #execute()} or on a background
	 * thread if {@link #executeAsync()} has been invoked.
	 *
	 * @param database The database instance upon which to execute operations.
	 * @return Result specific for this transaction when execution is successful. May be {@code null}.
	 * @see #onError(Database, Throwable)
	 */
	@Nullable
	protected abstract R onExecute(@NonNull D database);

	/**
	 * Invoked if {@link #onExecute()} has finished successfully.
	 *
	 * @param database The database instance specific for this transaction.
	 * @param result   Result returned by {@link #onExecute(Database)}.
	 * @see #onError(Database, Throwable)
	 */
	protected void onSuccess(@NonNull D database, @Nullable R result) {
		// Inheritance hierarchies may perform here logic related to successful execution.
	}

	/**
	 * Invoked if {@link #onExecute(Database)} throws some exception during execution.
	 * <p>
	 * <b>Note</b>, that in case of asynchronous execution via {@link #onExecuteAsync(Database)} is
	 * this method always invoked on the UI thread and the result is passed to {@link #onExecutedAsync(Database, Object)}.
	 *
	 * @param database The database instance specific for this transaction.
	 * @param error    The error thrown when performing execution of this transaction.
	 * @return Result specific for this transaction when some error occurs. May be {@code null}.
	 * @see #onSuccess(Database, Object)
	 */
	@Nullable
	protected R onError(@NonNull D database, @NonNull Throwable error) {
		Log.e(database.getClass().getSimpleName() + "#" + getClass().getSimpleName(), error.getMessage(), error);
		return null;
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * An {@link AsyncTask} implementation used to asynchronously execute DatabaseTransaction.
	 *
	 * @see #onExecuteAsync(Database)
	 */
	private static final class AsyncTransactionTask<R, D extends Database> extends AsyncTask<Void, Void, R> {

		/**
		 * Transaction to be executed.
		 */
		private final DatabaseTransaction<R, D> transaction;

		/**
		 * Database instance to be used for desired operations execution.
		 */
		private final D database;

		/**
		 * Creates a new instance of AsyncTransactionTask for the specified <var>database</var> and
		 * <var>transaction</var>.
		 *
		 * @param transaction Transaction to be executed.
		 * @param database    The database to be used for queries execution.
		 */
		AsyncTransactionTask(DatabaseTransaction<R, D> transaction, D database) {
			super();
			this.database = database;
			this.transaction = transaction;
		}

		/**
		 */
		@Override
		protected R doInBackground(Void... params) {
			return transaction.execute();
		}

		/**
		 */
		@Override
		protected void onPostExecute(@Nullable R result) {
			transaction.onExecutedAsync(database, result);
		}
	}
}
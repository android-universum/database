/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.DatabaseEntities;
import universum.studios.android.database.annotation.handler.BaseAnnotationHandlers;
import universum.studios.android.database.annotation.handler.DatabaseAnnotationHandler;
import universum.studios.android.database.query.Query;

/**
 * Database class represents an abstraction/wrapper for SQLite database. A single Database object can
 * be attached to a specific {@link DatabaseProvider} during its initialization. DatabaseProvider will
 * use the attached database to delegate all content related (CREATE, INSERT, UPDATE, QUERY, ...)
 * methods to it so such database can those calls delegate to its assigned entities.
 * <p>
 * Each Database may have assigned multiple {@link DatabaseEntity DatabaseEntities}. A desired database
 * entity can be assigned to database via {@link #assignEntity(Class)} by its class. This can be done
 * in {@link #onAssignEntities()} or by {@link DatabaseEntities @DatabaseEntities} annotation as described
 * in section dedicated to annotations below. Assigned entities (theirs classes) are then used by a
 * particular Database instance to create instances of those entities to which are then dispatched
 * provider calls by a specific entity's content uri. An instance of the assigned entity may be obtained
 * via {@link #findEntityByClass(Class)} or {@link #findEntityByUri(Uri)}.
 *
 * <h3>Life cycle</h3>
 * <b>1) Attaching and Creating</b>
 * <p>
 * When an instance of Database object is attached to its associated DatabaseProvider (during its
 * initialization) {@link #onAttachedToProvider(DatabaseProvider)} is invoked for that database object.
 * and the provider will request from its attached database to create its SQLite database structure
 * via {@link #create(Context)} internal method which will result in call to {@link #onCreateDB(SQLiteDatabase)}.
 * <p>
 * When a database structure is being for the first time created all assigned entities will be instantiated
 * and requested to create database tables that they represent via {@link DatabaseEntity#dispatchCreate(SQLiteDatabase)}.
 * <p>
 * <b>2) Querying, Inserting, Updating, Deleting</b>
 * <p>
 * Below are listed database content related calls that are dispatched further to the attached
 * entities by theirs corresponding {@link Uri}:
 * <ul>
 * <li>
 * {@link #query(Uri, String[], String, String[], String)}
 * <p>
 * delegates to {@link DatabaseEntity#dispatchQuery(SQLiteDatabase, String[], String, String[], String)}
 * </li>
 * <li>
 * {@link #insert(Uri, ContentValues)}
 * <p>
 * delegates to {@link DatabaseEntity#dispatchInsert(SQLiteDatabase, ContentValues)}
 * </li>
 * <li>
 * {@link #bulkInsert(Uri, ContentValues[])}
 * <p>
 * delegates to {@link DatabaseEntity#dispatchBulkInsert(SQLiteDatabase, ContentValues[])}
 * </li>
 * <li>
 * {@link #update(Uri, ContentValues, String, String[])}
 * <p>
 * delegates to {@link DatabaseEntity#dispatchUpdate(SQLiteDatabase, ContentValues, String, String[])}
 * </li>
 * <li>
 * {@link #delete(Uri, String, String[])}
 * <p>
 * delegates to {@link DatabaseEntity#dispatchDelete(SQLiteDatabase, String, String[])}
 * </li>
 * </ul>
 * <p>
 * <b>3) Upgrading</b>
 * <p>
 * A previous version of the database may be upgraded by supplying a new version number (grater than
 * the current one) during initialization of the Database object. If done so, {@link #onUpgradeDB(SQLiteDatabase, int, int)}
 * will be invoked to upgrade the current database structure. Assigned entities will be also upgraded
 * via {@link DatabaseEntity#dispatchUpgrade(SQLiteDatabase, int, int)}.
 * <p>
 *
 * <h3>Configuration</h3>
 * Below are listed methods that can be used to configure a specific database instance:
 * <ul>
 * <li>{@link #setCreateOnStartup(boolean)}</li>
 * <li>{@link #setLocale(Locale)}</li>
 * </ul>
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link DatabaseEntities @DatabaseEntities} <b>[class]</b>
 * <p>
 * If this annotation is presented, all classes provided via this annotation will be assigned as entity
 * classes via {@link #assignEntity(Class)} to an instance of subclass of this Database.
 * </li>
 * </ul>
 *
 * @author Martin Albedinsky
 */
public abstract class Database {

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "Database";

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant used to identify that no rows has been affected by the executed content operation.
	 */
	public static final int NO_ROWS = -1;

	/**
	 * Format used to create content Uri for a database table/entity.
	 * <p>
	 * This format takes two arguments as shown below.
	 *
	 * <h3>Arguments:</h3>
	 * <ul>
	 * <li>1) <b>database authority</b> (provider name)</li>
	 * <li>2) <b>entity/table name</b></li>
	 * </ul>
	 * Example Uri output: <b>content://com.android.contacts/Contacts</b>
	 */
	public static final String CONTENT_URI_FORMAT = "content://%s/%s";

	/**
	 * Format used to create content Mime type for a database table/entity.
	 * <p>
	 * This format takes two arguments as shown below.
	 *
	 * <h3>Arguments:</h3>
	 * <ul>
	 * <li>1) <b>base type</b> (see {@link ContentResolver#CURSOR_DIR_BASE_TYPE ContentResolver.CURSOR_DIR_BASE_TYPE})</li>
	 * <li>2) <b>database authority</b> (provider name)</li>
	 * <li>3) <b>entity/table name</b></li>
	 * </ul>
	 */
	public static final String MIME_TYPE_FORMAT = "%s/vnd.%s.%s";

	/**
	 * Format for query that is used to check if a specific table exists within a master database.
	 * <p>
	 * This format takes single argument as shown below.
	 *
	 * <h3>Arguments:</h3>
	 * <ul>
	 * <li>1) <b>table name</b></li>
	 * </ul>
	 */
	private static final String CHECK_TABLE_EXISTENCE_QUERY_FORMAT = "SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name='%s'";

	/**
	 * Format for SQL <b>DROP TABLE IF EXISTS</b> statement.
	 */
	private static final String DROP_TABLE_IF_EXISTS_FORMAT = "DROP TABLE IF EXISTS %s";

	/**
	 * Flag indicating that the database file should be created on application's startup.
	 */
	private static final int PFLAG_CREATE_ON_STARTUP = 0x00000001;

	/**
	 * Flag indicating that an instance of database is attached to its associated database provider.
	 */
	private static final int PFLAG_ATTACHED_TO_PROVIDER = 0x00000001 << 2;

	/**
	 * Flag indicating that an instance of database is created.
	 */
	private static final int PFLAG_CREATED = 0x00000001 << 3;

	/**
	 * Flag indicating that SQLite database is created.
	 */
	private static final int PFLAG_DB_CREATED = 0x00000001 << 4;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Array containing classes of entities that are assigned to this database mapped to theirs unique
	 * id managed by {@link #mUriMatcher}.
	 */
	private final SparseArray<Class<? extends DatabaseEntity>> mAssignedEntities = new SparseArray<>(5);

	/**
	 * Array containing Uris of assigned entities mapped to theirs key by which are mapped in
	 * {@link #mAssignedEntities} array.
	 */
	private final SparseArray<Uri> mEntityUris = new SparseArray<>();

	/**
	 * Array containing attached entities. Entities are mapped by the same key as theirs classes in
	 * {@link #mAssignedEntities}.
	 */
	private final SparseArray<DatabaseEntity> mEntities = new SparseArray<>();

	/**
	 * Uri matcher holding Uris of all assigned entities. This matcher is used to obtain unique id
	 * for a specific entity when it is needed.
	 */
	private final UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	/**
	 * Handler responsible for processing of all annotations of this class and also for handling all
	 * annotations related operations for this class.
	 */
	private final DatabaseAnnotationHandler mAnnotationHandler;

	/**
	 * Current version of database. This version may be changed in different versions/builds of
	 * application, if so, {@link #onUpgradeDB(SQLiteDatabase, int, int)} will be called to upgrade
	 * database to a new version.
	 */
	private final int mVersion;

	/**
	 * Name of database file that should be created and managed by this database instance.
	 */
	private final String mName;

	/**
	 * Locale for database content.
	 */
	private Locale mLocale;

	/**
	 * Name of authority as specified by the DatabaseProvider to which is this database instance attached.
	 */
	private String mAuthority;

	/**
	 * Application context passed to this database instance during call to {@link #create(Context)}.
	 */
	Context mApplicationContext;

	/**
	 * Helper used to create and then manage database file.
	 */
	private DatabaseHelper mDatabaseHelper;

	/**
	 * Stores all private flags of this database instance.
	 */
	private int mPrivateFlags = PFLAG_CREATE_ON_STARTUP;

	/**
	 * Temporary instance of writable database. This instance is valid for example during creation
	 * or upgrade of database.
	 */
	private SQLiteDatabase mTempWritableDB;

	/**
	 * Temporary list of database entities that requested creation of initial content. Such creation
	 * will be performed within {@link #onCreateInitialContent(SQLiteDatabase)}.
	 */
	List<DatabaseEntity> mEntitiesWithInitialContent;

	/**
	 * Temporary list of database entities that requested upgrade of theirs existing content. Such
	 * upgrade will be performed within {@link #onUpgradeContent(SQLiteDatabase, int, int)}.
	 */
	List<DatabaseEntity> mEntitiesWithContentToUpgrade;

	/**
	 * Temporary list of database entities that are marked by {@link Deprecated @Deprecated} annotation
	 * to indicate that theirs tables should be deleted. Such deletion will be performed within
	 * {@link #upgradeDB(SQLiteDatabase, int, int)}.
	 */
	List<DatabaseEntity> mDeprecatedEntities;

	/**
	 * Executor used to execute long running operations on a background thread.
	 */
	private Executor mExecutor = DatabaseExecutors.background();

	/**
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new Database with the specified <var>version</var> and <var>name</var>.
	 *
	 * @param version The version of the database. If grater than the previous version, database
	 *                can be upgraded to the new version in {@link #onUpgradeDB(SQLiteDatabase, int, int)}.
	 * @param name    The name for the database file that is used for storing of database content.
	 */
	public Database(int version, @NonNull String name) {
		this.mAnnotationHandler = onCreateAnnotationHandler();
		this.mVersion = version;
		this.mName = name;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates an Uri for the requested parameters using {@link #CONTENT_URI_FORMAT}.
	 *
	 * @param authority  Authority of the content/database provider that will manage database content
	 *                   of the entity with the specified name.
	 * @param entityName Name of the desired entity for which should be the requested Uri created.
	 * @return Valid database content Uri.
	 * @see #createMimeType(String, String, String)
	 */
	@NonNull
	public static Uri createContentUri(@NonNull String authority, @NonNull String entityName) {
		return Uri.parse(String.format(CONTENT_URI_FORMAT, authority, entityName));
	}

	/**
	 * Creates a MIME type for the requested parameters using {@link #MIME_TYPE_FORMAT}.
	 *
	 * @param baseType   See {@link ContentResolver#CURSOR_DIR_BASE_TYPE ContentResolver.CURSOR_DIR_BASE_TYPE}
	 *                   for info.
	 * @param authority  Authority of the content/database provider that will manage database content
	 *                   of the entity with the specified name.
	 * @param entityName Name of the desired entity for which should be the requested MIME type created.
	 * @return Valid database mime type.
	 * @see #createContentUri(String, String)
	 */
	@NonNull
	public static String createMimeType(@NonNull String baseType, @NonNull String authority, @NonNull String entityName) {
		return String.format(MIME_TYPE_FORMAT, baseType, authority, entityName);
	}

	/**
	 * Instantiates an instance of the specified <var>classOfEntity</var>.
	 *
	 * @param classOfEntity Class of the desired entity to instantiate.
	 * @return Instance of the requested DatabaseEntity.
	 * @throws DatabaseException If entity cannot be instantiated.
	 */
	private static DatabaseEntity instantiateEntity(Class<? extends DatabaseEntity> classOfEntity) {
		try {
			return classOfEntity.getConstructor().newInstance();
		} catch (InvocationTargetException e) {
			final Throwable cause = e.getCause();
			if (cause instanceof DatabaseException) {
				throw (DatabaseException) cause;
			}
			throw DatabaseException.instantiation("ENTITY", classOfEntity, e);
		} catch (Exception e) {
			throw DatabaseException.instantiation("ENTITY", classOfEntity, e);
		}
	}

	/**
	 * Invoked to create annotations handler for this instance.
	 *
	 * @return Annotations handler specific for this class.
	 */
	private DatabaseAnnotationHandler onCreateAnnotationHandler() {
		return BaseAnnotationHandlers.obtainDatabaseHandler(getClass());
	}

	/**
	 * Returns handler that is responsible for annotations processing of this class and also for
	 * handling all annotations related operations for this class.
	 *
	 * @return Annotations handler specific for this class.
	 * @throws DatabaseException If annotations processing is not enabled for the Database library.
	 */
	@NonNull
	protected DatabaseAnnotationHandler getAnnotationHandler() {
		DatabaseAnnotations.checkIfEnabledOrThrow();
		return mAnnotationHandler;
	}

	/**
	 * Returns the current version of this database.
	 *
	 * @return Database version.
	 * @see #getName()
	 * @see #Database(int, String)
	 */
	public final int getVersion() {
		return mVersion;
	}

	/**
	 * Returns the name of this database.
	 *
	 * @return Database name used for SQLite database file.
	 * @see #getVersion()
	 * @see #Database(int, String)
	 */
	@NonNull
	public final String getName() {
		return mName;
	}

	/**
	 * Specifies a locale that should be used for content of SQLite database created and managed by
	 * this database instance.
	 *
	 * @param locale The desired locale for this database. May be {@code null}.
	 * @see #getLocale()
	 */
	public final void setLocale(@NonNull Locale locale) {
		this.mLocale = locale;
		if (isDBCreated()) writableDB().setLocale(locale);
	}

	/**
	 * Returns the locale of this database.
	 *
	 * @return Database locale or {@code null} if no locale has been specified.
	 * @see #setLocale(Locale)
	 */
	@Nullable
	public final Locale getLocale() {
		return mLocale;
	}

	/**
	 * Sets an executor which should be used by this database for performing of long running operations.
	 * <p>
	 * Default value: <b>{@link DatabaseExecutors#background()}</b>
	 *
	 * @param executor The desired executor. May be {@code null} to indicate use of a default one.
	 */
	@VisibleForTesting
	final void setExecutor(@Nullable Executor executor) {
		this.mExecutor = executor == null ? DatabaseExecutors.background() : executor;
	}

	/**
	 * Specifies a boolean flag indicating whether an SQLite database file for this database should
	 * be created on application's start up or when a database content is being for the first time
	 * accessed.
	 * <p>
	 * Default value: <b>true</b>
	 *
	 * @param createOnStartup {@code True} to create database on application's startup, {@code false}
	 *                        otherwise.
	 * @see #createsOnStartup()
	 */
	protected final void setCreateOnStartup(boolean createOnStartup) {
		if (createOnStartup) this.mPrivateFlags |= PFLAG_CREATE_ON_STARTUP;
		else this.mPrivateFlags &= ~PFLAG_CREATE_ON_STARTUP;
	}

	/**
	 * Returns boolean flag indicating whether an SQLite database for this database will be created
	 * on application's start up.
	 *
	 * @return {@code True} to create on application's startup, {@code false} otherwise.
	 * @see #setCreateOnStartup(boolean)
	 */
	protected final boolean createsOnStartup() {
		return (mPrivateFlags & PFLAG_CREATE_ON_STARTUP) != 0;
	}

	/**
	 * Called to attach this database instance to the specified <var>provider</var>. Authority of
	 * the specified provider will be used as authority of this database.
	 *
	 * @param provider The provider to which to attach this database.
	 */
	final void attachToProvider(DatabaseProvider provider) {
		this.mPrivateFlags |= PFLAG_ATTACHED_TO_PROVIDER;
		this.mAuthority = provider.getAuthority();
		onAttachedToProvider(provider);
	}

	/**
	 * Invoked after this database instance has been attached to its associated provider.
	 * <p>
	 * From this point on, this database is considered to be attached to the provider and {@link #getAuthority()}
	 * will return authority of that provider.
	 *
	 * @param provider The provider to which is this database attached.
	 */
	protected void onAttachedToProvider(@NonNull DatabaseProvider provider) {
		// Inheritance hierarchies may perform here logic related to event when they are attached
		// to theirs corresponding provider.
	}

	/**
	 * Asserts that this database instance is attached to its associated database provider. If not,
	 * an exception is thrown.
	 *
	 * @throws IllegalStateException If this database instance is not attached to any database provider.
	 */
	private void assertAttachedToProviderOrThrow() {
		if (!isAttachedToProvider()) {
			throw new IllegalStateException(
					"Database(" + getClass().getSimpleName() + ") is not attached to its associated DatabaseProvider."
			);
		}
	}

	/**
	 * Checks whether this database is attached to database provider or not.
	 *
	 * @return {@code True} if this database instance is attached to its associated database provider,
	 * {@code false} otherwise.
	 * @see #onAttachedToProvider(DatabaseProvider)
	 */
	public final boolean isAttachedToProvider() {
		return (mPrivateFlags & PFLAG_ATTACHED_TO_PROVIDER) != 0;
	}

	/**
	 * Returns the authority of database provider to which is this database attached.
	 *
	 * @return Authority of the associated provider.
	 * @throws IllegalStateException If this database is not attached to its provider yet.
	 * @see #onAttachedToProvider(DatabaseProvider)
	 */
	public final String getAuthority() {
		this.assertAttachedToProviderOrThrow();
		return mAuthority;
	}

	/**
	 * Performs creation of this database instance, its database tables according to the assigned
	 * entities. If some of the assigned entities also requests to create an initial content it will
	 * be also created during this call. If this database has been already created, this method does
	 * nothing.
	 * <p>
	 * <b>Note</b>, that the database file with tables and initial content can be created also later
	 * when such content is first time accessed if this database should not be created on application's
	 * startup as request via {@link #setCreateOnStartup(boolean)}.
	 *
	 * @param context Context used to create SQLite database.
	 * @see #isCreated()
	 * @see #getContext()
	 */
	protected final void create(@NonNull Context context) {
		this.mApplicationContext = context.getApplicationContext();
		if (!isCreated()) {
			// Assign entities to this database.
			if (mAnnotationHandler != null) {
				final Class<? extends DatabaseEntity>[] entityClasses = mAnnotationHandler.getEntities();
				if (entityClasses != null) {
					for (final Class<? extends DatabaseEntity> classOfEntity : entityClasses) {
						assignEntity(classOfEntity);
					}
				}
			}
			// Dispatch to assign entities manually.
			onAssignEntities();
			this.mPrivateFlags |= PFLAG_CREATED;
			// Now when the entities are assigned, instantiate and attach them to this database.
			final List<String> entityNames = new ArrayList<>(mAssignedEntities.size());
			for (int i = 0; i < mAssignedEntities.size(); i++) {
				final int entityKey = mAssignedEntities.keyAt(i);
				final DatabaseEntity entity = instantiateEntity(mAssignedEntities.get(entityKey));
				final String entityName = entity.getName();
				if (entityNames.contains(entityName)) {
					throw DatabaseException.misconfiguration(
							"Each database entity assigned to same database must have a unique name." +
									"Found multiple entities with name(" + entityName + ")."
					);

				} else {
					entityNames.add(entityName);
				}
				entity.attachToDatabase(this);
				mEntities.put(entityKey, entity);
				mUriMatcher.addURI(mAuthority, entity.getName(), entityKey);
				mEntityUris.append(entityKey, entity.getContentUri());
			}
			// The database is configured and ready, lets create helper which will create SQLite database.
			this.mDatabaseHelper = new DatabaseHelper(
					mApplicationContext,
					TextUtils.isEmpty(mName) ? null : mName,
					mVersion,
					(mPrivateFlags & PFLAG_CREATE_ON_STARTUP) != 0
			);
		}
	}

	/**
	 * Invoked to assign all desired entities to this database. All classes of database entities
	 * provided via {@link DatabaseEntities @DatabaseEntities}
	 * annotation has been already assigned.
	 * <p>
	 * This method is invoked during creation of this database instance.
	 */
	protected void onAssignEntities() {
		// Inheritance hierarchies may perform here "manual" assigning of theirs entities.
	}

	/**
	 * Assigns the given <var>classOfEntity</var> to this database.
	 * <p>
	 * <b>Note</b>, that the desired entity must have public empty constructor without parameters,
	 * otherwise such entity cannot be instantiated, thus, cannot be properly assigned.
	 *
	 * @param classOfEntity Class of database entity to be assigned to this database.
	 * @throws IllegalStateException If database has been already created.
	 * @see #isCreated()
	 */
	protected final void assignEntity(@NonNull Class<? extends DatabaseEntity> classOfEntity) {
		if (isCreated()) {
			throw new IllegalStateException("Cannot assign new entity. Database has been already created.");
		}
		// Check for already assigned entity.
		if (mAssignedEntities.indexOfValue(classOfEntity) >= 0) {
			if (DatabaseConfig.LOG_ENABLED) {
				Log.v(getClass().getSimpleName(), "Database entity(" + classOfEntity.getSimpleName() + ") is already assigned.");
			}
		} else {
			// Map all entities by theirs index.
			final int entityKey = mAssignedEntities.size();
			mAssignedEntities.append(entityKey, classOfEntity);
		}
	}

	/**
	 * Returns the array containing all entity classes assigned to this database instance.
	 *
	 * @return Array of assigned entity classes.
	 * @see #onAssignEntities()
	 * @see #assignEntity(Class)
	 */
	@NonNull
	protected final SparseArray<Class<? extends DatabaseEntity>> getAssignedEntities() {
		return mAssignedEntities;
	}

	/**
	 * Checks whether this database instance has a database entity with the specified <var>entityUri</var>
	 * assigned or not.
	 *
	 * @param entityUri Content uri of the desired entity to check if it is assigned.
	 * @return {@code True} if entity with the specified uri is assigned, {@code false} otherwise.
	 * @see #findEntityByClass(Class)
	 * @see #findEntityByUri(Uri)
	 */
	public final boolean isEntityAssigned(@NonNull Uri entityUri) {
		return mUriMatcher.match(entityUri) != -1;
	}

	/**
	 * Searches for and returns an instance of DatabaseEntity assigned and attached to this database
	 * by the specified <var>entityUri</var>.
	 *
	 * @param entityUri Uri of the desired entity to find and return.
	 * @param <T>       Type of the requested entity.
	 * @return Requested database entity.
	 * @throws IllegalStateException    If database is not created yet.
	 * @throws IllegalArgumentException If database does not have the requested entity assigned.
	 * @see #isCreated()
	 * @see #isEntityAssigned(Uri)
	 * @see #findEntityByClass(Class)
	 */
	@NonNull
	@SuppressWarnings("unchecked")
	public final <T extends DatabaseEntity> T findEntityByUri(@NonNull Uri entityUri) {
		assertCreatedOrThrow("FIND ENTITY BY URI");
		final int entityKey = mUriMatcher.match(entityUri);
		if (entityKey == -1) {
			throw new IllegalArgumentException("There is no database entity assigned with uri(" + entityUri + ").");
		}
		return (T) mEntities.get(entityKey);
	}


	/**
	 * Checks whether this database instance has a database entity with the specified <var>classOfEntity</var>
	 * assigned or not.
	 *
	 * @param classOfEntity Class of the desired entity to check if it is assigned.
	 * @return {@code True} if entity with the specified class is assigned, {@code false} otherwise.
	 * @see #findEntityByClass(Class)
	 * @see #findEntityByUri(Uri)
	 */
	public final boolean isEntityAssigned(@NonNull Class<? extends DatabaseEntity> classOfEntity) {
		return mAssignedEntities.indexOfValue(classOfEntity) >= 0;
	}

	/**
	 * Searches for and returns an instance of DatabaseEntity assigned and attached to this database
	 * by the specified <var>classOfEntity</var>.
	 *
	 * @param classOfEntity Class of the desired entity to find and return.
	 * @param <T>           Type of the requested entity.
	 * @return Requested database entity.
	 * @throws IllegalStateException    If database is not created yet.
	 * @throws IllegalArgumentException If database does not have the requested entity assigned.
	 * @see #isCreated()
	 * @see #isEntityAssigned(Class)
	 * @see #findEntityByUri(Uri)
	 */
	@NonNull
	@SuppressWarnings("unchecked")
	public final <T extends DatabaseEntity> T findEntityByClass(@NonNull Class<T> classOfEntity) {
		assertCreatedOrThrow("FIND ENTITY BY CLASS");
		final int index = mAssignedEntities.indexOfValue(classOfEntity);
		if (index < 0) {
			throw new IllegalArgumentException("There is no database entity assigned with class(" + classOfEntity.getSimpleName() + ").");
		}
		return (T) mEntities.get(mAssignedEntities.keyAt(index));
	}

	/**
	 * Asserts that this database instance is created. If not, an exception is thrown.
	 *
	 * @param forAction Name of action for which is required that database is already created.
	 * @throws IllegalStateException If this database instance is not created yet.
	 */
	private void assertCreatedOrThrow(String forAction) {
		if (!isCreated()) {
			throw new IllegalStateException(
					"Cannot perform database action(" + forAction + "). Database is not created yet."
			);
		}
	}

	/**
	 * Returns a boolean flag indicating whether this database instance has been already created or not.
	 * <p>
	 * <b>Note</b>, that database can be created only when it is properly attached to its DatabaseProvider.
	 *
	 * @return {@code True} if this database has been already created so all database content related
	 * calls like {@link #insert(Uri, ContentValues)}, {@link #delete(Uri, String, String[])}, ...,
	 * can be invoked, {@code false} otherwise.
	 * @see #isDBCreated()
	 */
	public final boolean isCreated() {
		return (mPrivateFlags & PFLAG_CREATED) != 0;
	}

	/**
	 * Returns the application context attached to this database during its creation.
	 *
	 * @return Application context.
	 * @throws IllegalStateException If this database is not created yet.
	 */
	@NonNull
	public final Context getContext() {
		assertCreatedOrThrow("ACCESS CONTEXT");
		return mApplicationContext;
	}

	/**
	 * Returns the content resolver accessible from the application context.
	 *
	 * @return Content resolver for the application context.
	 * @throws IllegalStateException If this database is not created yet.
	 * @see #getContext()
	 */
	@NonNull
	public final ContentResolver getContentResolver() {
		assertCreatedOrThrow("ACCESS CONTENT RESOLVER");
		return mApplicationContext.getContentResolver();
	}

	/**
	 * Called to configure the specified SQLite database.
	 *
	 * @param db The SQLite database to configure if necessary.
	 * @see DatabaseHelper#onConfigure(SQLiteDatabase)
	 */
	final void configureDB(SQLiteDatabase db) {
		onConfigureDB(db);
	}

	/**
	 * Invoked to configure the specified SQLite database.
	 * <p>
	 * <b>Note</b>, that this will be called before {@link #onCreateDB(SQLiteDatabase)}, {@link #onUpgradeDB(SQLiteDatabase, int, int)},
	 * {@link #onDowngradeDB(SQLiteDatabase, int, int)} or {@link #onDBOpened(SQLiteDatabase)} each time
	 * one of these methods is invoked.
	 * <p>
	 * See {@link SQLiteOpenHelper#onConfigure(SQLiteDatabase)} for more info.
	 *
	 * @param db The SQLite database to configure if necessary.
	 */
	protected void onConfigureDB(@NonNull SQLiteDatabase db) {
		// Inheritance hierarchies may perform here logic related to configuration of the DB.
	}

	/**
	 * Called to dispatch that the specified SQLite database has been opened.
	 *
	 * @param db The currently opened SQLite database.
	 */
	final void dbOpened(SQLiteDatabase db) {
		onDBOpened(db);
	}

	/**
	 * Invoked whenever the specified SQLite database has been opened.
	 * <p>
	 * See {@link SQLiteOpenHelper#onOpen(SQLiteDatabase)} for more info.
	 *
	 * @param db The currently opened SQLite database.
	 */
	protected void onDBOpened(@NonNull SQLiteDatabase db) {
		// Inheritance hierarchies may perform here logic related to opening of the DB.
	}

	/**
	 * Called to create a new database structure within the specified SQLite database.
	 *
	 * @param db The SQLite database where to create tables specific for this database instance.
	 */
	final void createDB(final SQLiteDatabase db) {
		if (DatabaseConfig.LOG_ENABLED) {
			Log.v(getClass().getSimpleName(), "Creating in version(" + mVersion + ").");
		}
		if (mLocale != null) {
			db.setLocale(mLocale);
		}
		this.mTempWritableDB = db;
		onCreateDB(db);
		this.mTempWritableDB = null;
		this.mPrivateFlags |= PFLAG_DB_CREATED;
		onDBCreated();
		// Database is fully created, lets create initial content if it is needed.
		mExecutor.execute(new Runnable() {

			/**
			 */
			@Override
			public void run() {
				onCreateInitialContent(db);
			}
		});
	}

	/**
	 * Invoked to create a new database structure within the specified SQLite database.
	 * <p>
	 * Implementation of this method creates tables for all assigned entities via
	 * {@link DatabaseEntity#dispatchCreate(SQLiteDatabase)}.
	 * <p>
	 * When done, {@link #onDBCreated()} will be invoked immediately after this followed by
	 * {@link #onCreateInitialContent(SQLiteDatabase)}.
	 * <p>
	 * <b>Note</b>, that this method is invoked on the <b>UI thread</b> so no long running operations
	 * should be performed here.
	 * <p>
	 * See {@link SQLiteOpenHelper#onCreate(SQLiteDatabase)} for more info.
	 *
	 * @param db The SQLite database where to create the database structure.
	 */
	protected void onCreateDB(@NonNull SQLiteDatabase db) {
		for (int i = 0; i < mEntities.size(); i++) {
			final DatabaseEntity entity = mEntities.get(mEntities.keyAt(i));
			final Class<?> classOfEntity = entity.getClass();
			if (classOfEntity.isAnnotationPresent(Deprecated.class)) {
				if (DatabaseConfig.LOG_ENABLED) {
					Log.v(getClass().getSimpleName(), "Skipping creation of entity(" + classOfEntity.getSimpleName() + ") due to its deprecation.");
				}
				continue;
			}
			entity.dispatchCreate(db);
			if (entity.createsInitialContent()) {
				if (mEntitiesWithInitialContent == null) {
					this.mEntitiesWithInitialContent = new ArrayList<>(5);
				}
				mEntitiesWithInitialContent.add(entity);
			}
		}
	}

	/**
	 * Invoked after the SQLite database of this database instance has been successfully created.
	 * <p>
	 * <b>Note</b>, that this is invoked on the <b>UI thread</b> so no long running operations should
	 * be performed here.
	 *
	 * @see #onCreateDB(SQLiteDatabase)
	 */
	protected void onDBCreated() {
		// Inheritance hierarchies may perform here logic related to finished creation of the DB.
	}

	/**
	 * Unlike {@link #isCreated()} this method returns boolean flag indicating whether an SQLite
	 * database file for this database instance is already created or not.
	 *
	 * @return {@code True} if SQLite database is created, {@code false} otherwise.
	 */
	public final boolean isDBCreated() {
		return (mPrivateFlags & PFLAG_DB_CREATED) != 0;
	}

	/**
	 * Invoked after {@link #onCreateDB(SQLiteDatabase)} and {@link #onDBCreated()} has been performed.
	 * If this database inserts some initial content into its tables this is a good place to perform
	 * such insertion.
	 * <p>
	 * Implementation of this method performs creation of initial content for all assigned entities
	 * that requested such creation ({@link DatabaseEntity#createsInitialContent()}) via
	 * {@link DatabaseEntity#dispatchCreateInitialContent(SQLiteDatabase)}.
	 * <p>
	 * <b>Note</b>, that this method is invoked on the <b>background thread</b> so insertion of the
	 * initial content (if any) will not cause this running application to not respond.
	 *
	 * @param db The SQLite database where to create initial content.
	 */
	protected void onCreateInitialContent(@NonNull SQLiteDatabase db) {
		if (mEntitiesWithInitialContent != null && !mEntitiesWithInitialContent.isEmpty()) {
			for (final DatabaseEntity entity : mEntitiesWithInitialContent) {
				entity.dispatchCreateInitialContent(db);
			}
			mEntitiesWithInitialContent.clear();
		}
		this.mEntitiesWithInitialContent = null;
	}

	/**
	 * Called to upgrade a database structure within the specified SQLite database to a new version.
	 *
	 * @param db         The SQLite database where to upgrade tables specific for this database instance.
	 * @param oldVersion The old version of the database.
	 * @param newVersion The new version of the database.
	 */
	final void upgradeDB(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
		if (DatabaseConfig.LOG_ENABLED) {
			Log.v(getClass().getSimpleName(), "Upgrading from version(" + oldVersion + ") to new version(" + newVersion + ").");
		}
		this.mTempWritableDB = db;
		onUpgradeDB(db, oldVersion, newVersion);
		this.mTempWritableDB = null;
		onDBUpgraded(oldVersion, newVersion);
		// Database is fully upgraded, lets upgrade also initial content if such update is needed.
		mExecutor.execute(new Runnable() {

			/**
			 */
			@Override
			public void run() {
				onUpgradeContent(db, oldVersion, newVersion);
				// Delete tables for all deprecated entities.
				if (mDeprecatedEntities != null && !mDeprecatedEntities.isEmpty()) {
					final String databaseName = getClass().getSimpleName();
					for (final DatabaseEntity entity : mDeprecatedEntities) {
						final String entityTableName = entity.getName();
						final String entityName = entity.getClass().getSimpleName();
						if (tableExists(db, entityTableName) && dropTable(db, entityTableName) && DatabaseConfig.LOG_ENABLED) {
							Log.v(databaseName, "Table(" + entityTableName + ") for deprecated entity(" + entityName + ") has been deleted from database.");
						}
					}
					mDeprecatedEntities.clear();
				}
				mDeprecatedEntities = null;
			}
		});
	}

	/**
	 * Checks whether a table with the specified <var>tableName</var> exists within the given <var>db</var>.
	 *
	 * @param db        SQLite database where to check for the requested table existence.
	 * @param tableName Name of the desired table to check.
	 * @return {@code True} if the requested table exists, {@code false} otherwise.
	 */
	final boolean tableExists(SQLiteDatabase db, String tableName) {
		final Cursor cursor = db.rawQuery(String.format(CHECK_TABLE_EXISTENCE_QUERY_FORMAT, tableName), null);
		if (cursor == null) {
			return false;
		}
		final boolean exists = cursor.getCount() > 0;
		cursor.close();
		return exists;
	}

	/**
	 * Deletes a table with the specified <var>tableName</var> within the given <var>db</var>.
	 *
	 * @param db        SQLite database where to delete the table.
	 * @param tableName Name of the desired table to delete.
	 * @return {@code True} if table with the requested name has been successfully deleted, {@code false}
	 * if some error has occurred.
	 */
	final boolean dropTable(SQLiteDatabase db, String tableName) {
		db.execSQL(String.format(DROP_TABLE_IF_EXISTS_FORMAT, tableName));
		return true;
	}

	/**
	 * Invoked to upgrade an existing database structure within the specified SQLite database from
	 * an old version to a new one.
	 * <p>
	 * Implementation of this method upgrades tables for all assigned entities via
	 * {@link DatabaseEntity#dispatchUpgrade(SQLiteDatabase, int, int)}.
	 * <p>
	 * When done, {@link #onDBUpgraded(int, int)} will be invoked immediately after this followed by
	 * {@link #onUpgradeContent(SQLiteDatabase, int, int)}.
	 * <p>
	 * <b>Note</b>, that this is invoked on the <b>UI thread</b> so no long running operations should
	 * be performed here.
	 * <p>
	 * See {@link SQLiteOpenHelper#onUpgrade(SQLiteDatabase, int, int)} for more info.
	 *
	 * @param db         The SQLite database where to upgrade the database structure.
	 * @param oldVersion The old database version.
	 * @param newVersion The new database version to which will be the database upgraded.
	 */
	protected void onUpgradeDB(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		// All assigned entities has been already instantiated in create() method.
		for (int i = 0; i < mEntities.size(); i++) {
			final DatabaseEntity entity = mEntities.get(mEntities.keyAt(i));
			final Class<?> classOfEntity = entity.getClass();
			if (classOfEntity.isAnnotationPresent(Deprecated.class)) {
				if (mDeprecatedEntities == null) {
					this.mDeprecatedEntities = new ArrayList<>(2);
				}
				mDeprecatedEntities.add(entity);
				if (DatabaseConfig.LOG_ENABLED) {
					Log.v(getClass().getSimpleName(), "Entity(" + classOfEntity.getSimpleName() + ") prepared for deletion due to its deprecation.");
				}
				continue;
			}
			this.upgradeDatabaseEntity(entity, db, oldVersion, newVersion);
		}
	}

	/**
	 * Called to upgrade the given <var>entity</var> (its database table) within the specified SQLite
	 * database.
	 * <p>
	 * This method will invoke {@link DatabaseEntity#dispatchUpgrade(SQLiteDatabase, int, int)} upon
	 * the given entity if its database table already exists otherwise that entity will be created
	 * via {@link DatabaseEntity#dispatchCreate(SQLiteDatabase)}.
	 *
	 * @param entity     The entity to be upgraded.
	 * @param db         The SQLite database where should be table of the specified entity created.
	 * @param oldVersion The old database version.
	 * @param newVersion The new database version to which will be the database upgraded.
	 */
	private void upgradeDatabaseEntity(DatabaseEntity entity, SQLiteDatabase db, int oldVersion, int newVersion) {
		final String entityTableName = entity.getName();
		if (tableExists(db, entityTableName)) {
			entity.dispatchUpgrade(db, oldVersion, newVersion);
			if (entity.upgradesContent(oldVersion, newVersion)) {
				if (mEntitiesWithContentToUpgrade == null) {
					this.mEntitiesWithContentToUpgrade = new ArrayList<>(3);
				}
				mEntitiesWithContentToUpgrade.add(entity);
			}
		} else {
			entity.dispatchCreate(db);
			if (entity.createsInitialContent(oldVersion, newVersion)) {
				if (mEntitiesWithInitialContent == null) {
					this.mEntitiesWithInitialContent = new ArrayList<>(3);
				}
				mEntitiesWithInitialContent.add(entity);
			}
		}
	}

	/**
	 * Invoked after {@link #onUpgradeDB(SQLiteDatabase, int, int)} and {@link #onDBUpgraded(int, int)}
	 * has been performed. If this database upgrades content of its tables this is a good place to
	 * perform such upgrade.
	 * <p>
	 * Implementation of this method requests upgrade of content for all assigned entities that
	 * requested such upgrade ({@link DatabaseEntity#upgradesContent(int, int)}) via
	 * {@link DatabaseEntity#dispatchUpgradeContent(SQLiteDatabase, int, int)}.
	 * <p>
	 * <b>Note</b>, that this is invoked on a <b>background thread</b> as this operation can take
	 * some time.
	 *
	 * @param db         The SQLite database where to upgrade the current content.
	 * @param oldVersion The old database version.
	 * @param newVersion The new database version to which has been the database upgraded.
	 */
	protected void onUpgradeContent(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		// Upgrade content of existing entities (if any).
		if (mEntitiesWithContentToUpgrade != null) {
			for (final DatabaseEntity entity : mEntitiesWithContentToUpgrade) {
				entity.dispatchUpgradeContent(db, oldVersion, newVersion);
			}
			mEntitiesWithContentToUpgrade.clear();
			this.mEntitiesWithContentToUpgrade = null;
		}
		// Create initial content for all new entities (if any).
		if (mEntitiesWithInitialContent != null && !mEntitiesWithInitialContent.isEmpty()) {
			for (final DatabaseEntity entity : mEntitiesWithInitialContent) {
				entity.dispatchCreateInitialContent(db, oldVersion, newVersion);
			}
			mEntitiesWithInitialContent.clear();
		}
		this.mEntitiesWithInitialContent = null;
	}

	/**
	 * Invoked after the SQLite database of this database instance has been successfully upgraded to
	 * a new version.
	 * <p>
	 * <b>Note</b>, that this is invoked on the <b>UI thread</b> so no long running operations should
	 * be performed here.
	 *
	 * @param oldVersion The old database version.
	 * @param newVersion The new database version to which has been the database upgraded.
	 * @see #onUpgradeDB(SQLiteDatabase, int, int)
	 */
	protected void onDBUpgraded(int oldVersion, int newVersion) {
		// Inheritance hierarchies may perform here logic related to finished upgrade of the DB.
	}

	/**
	 * Called to downgrade a database structure within the specified SQLite database to a new version.
	 *
	 * @param db         The SQLite database where to downgrade tables specific for this database instance.
	 * @param oldVersion The old version of the database.
	 * @param newVersion The new version of the database.
	 */
	final void downgradeDB(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (DatabaseConfig.DEBUG_LOG_ENABLED) {
			Log.d(getClass().getSimpleName(), "Downgrading from version(" + oldVersion + ") to new version(" + newVersion + ").");
		}
		this.mTempWritableDB = db;
		onDowngradeDB(db, oldVersion, newVersion);
		this.mTempWritableDB = null;
	}

	/**
	 * Invoked to downgrade an existing database structure within the specified SQLite database from
	 * an old version to a new one.
	 * <p>
	 * Implementation of this method downgrades tables for all assigned entities via
	 * {@link DatabaseEntity#dispatchDowngrade(SQLiteDatabase, int, int)}.
	 * <p>
	 * <b>Note</b>, that this is invoked on the <b>UI thread</b> so no long running operations should
	 * be performed here.
	 * <p>
	 * See {@link SQLiteOpenHelper#onDowngrade(SQLiteDatabase, int, int)} for more info.
	 *
	 * @param db         The SQLite database where to downgrade the database structure.
	 * @param oldVersion The old database version.
	 * @param newVersion The new database version to which will be the database downgraded.
	 */
	protected void onDowngradeDB(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		for (int i = 0; i < mEntities.size(); i++) {
			final DatabaseEntity entity = mEntities.get(mEntities.keyAt(i));
			if (tableExists(db, entity.getName())) {
				entity.dispatchDowngrade(db, oldVersion, newVersion);
			}
		}
	}

	/**
	 * Beings a new transaction for the {@link SQLiteDatabase} managed by this database instance
	 * in the specified <var>mode</var>.
	 * <p>
	 * <b>Note, that there is no synchronized mechanism built in when managing transactions via
	 * this method nor via {@link #endTransaction()}. Any subsequent calls to this method before
	 * {@link #endTransaction()} is called will only create a 'deeper' (nested) transactions hierarchy
	 * and any changes for the successfully ended transactions marked via {@link #setTransactionSuccessful()}
	 * will be reflected in the DB after the first (top) began transaction is successfully ended.
	 * See {@link SQLiteDatabase#beginTransaction()} for more information.</b>
	 *
	 * @param mode The desired mode in which to begin the new transaction. One of modes defined by
	 *             {@link Transaction.Mode @Transaction.Mode} annotation.
	 * @see #setTransactionSuccessful()
	 * @see #endTransaction()
	 * @see SQLiteDatabase#beginTransaction()
	 */
	public void beginTransaction(@Transaction.Mode int mode) {
		switch (mode) {
			case Transaction.EXCLUSIVE:
				asWritable().beginTransaction();
				break;
			case Transaction.IMMEDIATE:
				asWritable().beginTransactionNonExclusive();
				break;
			case Transaction.NONE:
			default:
				break;
		}
	}

	/**
	 * Marks the transaction started via {@link #beginTransaction(int)} as successful.
	 *
	 * @see #endTransaction()
	 * @see SQLiteDatabase#setTransactionSuccessful()
	 */
	public void setTransactionSuccessful() {
		asWritable().setTransactionSuccessful();
	}

	/**
	 * Ends the transaction started via {@link #beginTransaction(int)}.
	 *
	 * @see #setTransactionSuccessful()
	 * @see SQLiteDatabase#endTransaction()
	 */
	public void endTransaction() {
		asWritable().endTransaction();
	}

	/**
	 * Executes the specified <var>query</var> upon this database via {@link SQLiteDatabase#execSQL(String)}.
	 *
	 * @param query The desired query to be executed.
	 * @return {@code True} if the specified query has been successfully executed, {@code false} if
	 * some error occurs or the query is not executable ({@link Query#isExecutable()}).
	 * @see #asWritable()
	 * @see #executeRawQuery(Query)
	 */
	public boolean executeQuery(@NonNull Query query) {
		if (query.isExecutable()) {
			asWritable().execSQL(query.build());
			return true;
		}
		return false;
	}

	/**
	 * Executes the specified <var>query</var> upon this database via {@link SQLiteDatabase#rawQuery(String, String[])}.
	 *
	 * @param query The desired query to be executed.
	 * @return Cursor as result of the executed query or {@code null} if some error occurs or the
	 * query is not executable ({@link Query#isExecutable()}).
	 * @see #asReadable()
	 * @see #executeQuery(Query)
	 */
	@Nullable
	public Cursor executeRawQuery(@NonNull Query query) {
		return query.isExecutable() ? asReadable().rawQuery(query.build(), null) : null;
	}

	/**
	 * Returns the SQLiteDatabase object managed by this database instance with <b>read</b> permissions.
	 *
	 * @return Read-able only database.
	 * @see SQLiteOpenHelper#getReadableDatabase()
	 * @see #asWritable()
	 */
	@NonNull
	public final SQLiteDatabase asReadable() {
		this.assertCreatedOrThrow("ACCESS READABLE DB");
		return mDatabaseHelper.getReadableDatabase();
	}

	/**
	 * Returns an SQLiteDatabase object managed by this database instance with <b>read</b> and
	 * <b>write</b> permissions.
	 *
	 * @return Read/write-able database.
	 * @see SQLiteOpenHelper#getWritableDatabase()
	 * @see #asReadable()
	 */
	@NonNull
	public final SQLiteDatabase asWritable() {
		this.assertCreatedOrThrow("ACCESS WRITABLE DB");
		return mDatabaseHelper.getWritableDatabase();
	}

	/**
	 * Returns the Mime type of a content stored for the specified content <var>uri</var>.
	 *
	 * @param uri The uri of which mime type to check.
	 * @return Mime type of the specified content uri or {@code null} if content with the specified
	 * uri does not exist.
	 * @throws IllegalStateException If this database is not created yet.
	 */
	@Nullable
	protected String getType(@NonNull Uri uri) {
		assertCreatedOrThrow("GET TYPE");
		return isEntityAssigned(uri) ? findEntityByUri(uri).getMimeType() : null;
	}

	/**
	 * Inserts a new content for the specified content <var>uri</var> as bulk operation.
	 *
	 * @param uri    The content uri where to perform the requested content operation.
	 * @param values The desired set of values to insert into database.
	 * @return Count of rows successfully inserted into database or content with the specified uri
	 * does not exist or database transaction failed.
	 * @throws IllegalStateException If this database is not created yet.
	 */
	protected int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
		assertCreatedOrThrow("BULK INSERT");
		return isEntityAssigned(uri) ? findEntityByUri(uri).dispatchBulkInsert(writableDB(), values) : NO_ROWS;
	}

	/**
	 * Inserts a new content for the specified content <var>uri</var>.
	 *
	 * @param uri    The content uri where to perform the requested content operation.
	 * @param values The desired values to insert into database.
	 * @return Uri of the newly inserted row or {@code null} if content with the specified uri does
	 * not exist or database transaction failed.
	 * @throws IllegalStateException If this database is not created yet.
	 */
	@Nullable
	protected Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
		assertCreatedOrThrow("INSERT");
		return isEntityAssigned(uri) ? findEntityByUri(uri).dispatchInsert(writableDB(), values) : null;
	}

	/**
	 * Queries an existing content for the specified content <var>uri</var>.
	 *
	 * @param uri           The content uri from where to perform the requested query operation.
	 * @param projection    The list of columns to put into the cursor. If {@code null} all columns
	 *                      are included.
	 * @param selection     A selection criteria to apply when filtering rows. If {@code null} then
	 *                      all rows are included.
	 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
	 *                      values from <var>selectionArgs</var>, in order that they appear in the
	 *                      selection. The values will be bound as Strings.
	 * @param sortOrder     How the rows in the cursor should be sorted. If {@code null} then the
	 *                      entity is free to define the sort order.
	 * @return Queried Cursor for the specified parameters or {@code null} if content with the specified
	 * uri does not exist or query operation failed.
	 */
	@Nullable
	protected Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		assertCreatedOrThrow("QUERY");
		return isEntityAssigned(uri) ? findEntityByUri(uri).dispatchQuery(readableDB(), projection, selection, selectionArgs, sortOrder) : null;
	}

	/**
	 * Updates an existing content for the specified content <var>uri</var>.
	 *
	 * @param uri           The content uri where to perform the requested content operation.
	 * @param values        Content values to update the desired rows with.
	 * @param selection     A selection criteria to apply when filtering rows to update.
	 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
	 *                      values from <var>selectionArgs</var>, in order that they appear in the
	 *                      selection. The values will be bound as Strings.
	 * @return Count of rows affected by the operation or {@link #NO_ROWS} if content with the specified
	 * uri does not exist or database transaction failed.
	 * @throws IllegalStateException If this database is not created yet.
	 */
	protected int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
		assertCreatedOrThrow("UPDATE");
		return isEntityAssigned(uri) ? findEntityByUri(uri).dispatchUpdate(writableDB(), values, selection, selectionArgs) : NO_ROWS;
	}

	/**
	 * Deletes an existing content for the specified content <var>uri</var>.
	 *
	 * @param uri           The content uri where to perform the requested content operation.
	 * @param selection     A selection criteria to apply when filtering rows to delete.
	 * @param selectionArgs You may include ?s in <var>selection</var>, which will be replaced by the
	 *                      values from <var>selectionArgs</var>, in order that they appear in the
	 *                      selection. The values will be bound as Strings.
	 * @return Count of rows affected by the operation or {@link #NO_ROWS} if content with the specified
	 * uri does not exist or database transaction failed.
	 * @throws IllegalStateException If this database is not created yet.
	 */
	protected int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
		assertCreatedOrThrow("DELETE");
		return isEntityAssigned(uri) ? findEntityByUri(uri).dispatchDelete(writableDB(), selection, selectionArgs) : NO_ROWS;
	}

	/**
	 * Accesses SQLite database for writing purpose.
	 *
	 * @return SQLite database with only <b>write</b> permissions.
	 */
	private SQLiteDatabase writableDB() {
		if (mTempWritableDB != null && !mTempWritableDB.isOpen()) {
			this.mTempWritableDB = null;
		}
		return mTempWritableDB == null ? asWritable() : mTempWritableDB;
	}

	/**
	 * Accesses SQLite database for reading purpose.
	 *
	 * @return SQLite database with only <b>read</b> permissions.
	 */
	private SQLiteDatabase readableDB() {
		return mDatabaseHelper.getReadableDatabase();
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Database helper implementation for internal purpose only.
	 */
	private final class DatabaseHelper extends SQLiteOpenHelper {

		/**
		 * Creates a new instance of DatabaseHelper that is used to create SQLite database with the
		 * specified <var>name</var> in the specified <var>version</var>.
		 *
		 * @param context           Context used to create the desired database.
		 * @param name              Name for the database.
		 * @param version           Current version of database.
		 * @param createImmediately Flag indicating whether to create the requested database
		 *                          immediately. If {@code false} database will be created when it
		 *                          is for the first time accessed.
		 */
		DatabaseHelper(Context context, String name, int version, boolean createImmediately) {
			super(context, name, null, version);
			if (createImmediately) ensureDBCreated();
		}

		/**
		 * Ensures that the SQLite database is created.
		 */
		void ensureDBCreated() {
			getReadableDatabase();
		}

		/**
		 */
		@Override
		public void onConfigure(SQLiteDatabase db) {
			configureDB(db);
		}

		/**
		 */
		@Override
		public void onOpen(SQLiteDatabase db) {
			dbOpened(db);
		}

		/**
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			createDB(db);
		}

		/**
		 */
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			upgradeDB(db, oldVersion, newVersion);
		}

		/**
		 */
		@Override
		public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			downgradeDB(db, oldVersion, newVersion);
		}
	}
}
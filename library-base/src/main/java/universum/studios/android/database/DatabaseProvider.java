/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.annotation.SuppressLint;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import universum.studios.android.database.annotation.Authority;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.annotation.DatabaseAnnotations;
import universum.studios.android.database.annotation.PrimaryDatabase;

/**
 * A {@link ContentProvider} implementation that is required to be used in association with {@link Database}.
 * Database object attached to DatabaseProvider via {@link #onCreateDatabase(Context)} is used to
 * dispatch all callbacks related to <b>inserting, updating, deleting, ...</b> of database content
 * managed by a specific instance of DatabaseProvider to {@link DatabaseEntity DatabaseEntities} that
 * are assigned to that Database object.
 * <p>
 * Below are listed all methods that are dispatched to Database by DatabaseProvider:
 * <ul>
 * <li>
 * {@link #query(Uri, String[], String, String[], String)}
 * <p>
 * delegates to {@link Database#query(Uri, String[], String, String[], String) Database.query(...)}
 * </li>
 * <li>
 * {@link #insert(Uri, ContentValues)}
 * <p>
 * delegates to {@link Database#insert(Uri, ContentValues) Database.insert(...)}
 * </li>
 * <li>
 * {@link #bulkInsert(Uri, ContentValues[])}
 * <p>
 * delegates to {@link Database#bulkInsert(Uri, ContentValues[]) Database.bulkInsert(...)}
 * </li>
 * <li>
 * {@link #update(Uri, ContentValues, String, String[])}
 * <p>
 * delegates to {@link Database#update(Uri, ContentValues, String, String[]) Database.update(...)}
 * </li>
 * <li>
 * {@link #delete(Uri, String, String[])}
 * <p>
 * delegates to {@link Database#delete(Uri, String, String[]) Database.delete(...)}
 * </li>
 * <li>
 * {@link #getType(Uri)}
 * <p>
 * delegates to {@link Database#getType(Uri) Database.getType(...)}
 * </li>
 * </ul>
 *
 * <h3>Accepted annotations</h3>
 * <ul>
 * <li>
 * {@link Authority @Authority} <b>[class]</b>
 * <p>
 * If this annotation is presented, a text provided via this annotation will be used as authority name
 * for an annotated instance of subclass of this DatabaseProvider.
 * </li>
 * </ul>
 *
 * @author Martin Albedinsky
 */
public abstract class DatabaseProvider extends ContentProvider {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "DatabaseProvider";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Map containing all created database instances mapped to theirs class.
	 */
	private static final Map<Class<? extends Database>, Database> sDatabases = new HashMap<>(1);

	/**
	 * Primary database instance. This is the database that has been marked by
	 * {@link PrimaryDatabase @PrimaryDatabase} annotation.
	 */
	@SuppressLint("StaticFieldLeak")
	private static volatile Database sPrimaryDatabase;

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Authority of this content provider implementation.
	 */
	private final String mAuthority;

	/**
	 * Database object that has assigned {@link DatabaseEntity DatabaseEntities}. All calls related
	 * to <b>inserting, updating, deleting, ...</b> invoked upon this object will be delegated to
	 * those entities based on theirs {@link Uri}.
	 */
	private Database mDatabase;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DatabaseProvider.
	 * <p>
	 * <b>Note</b>, that this constructor is meant to be called only if required annotation
	 * {@link Authority @Authority} is presented above a subclass of this DatabaseProvider, otherwise
	 * an exception will be thrown.
	 */
	public DatabaseProvider() {
		super();
		DatabaseAnnotations.checkIfEnabledOrThrow();
		final Authority authority = DatabaseAnnotations.obtainAnnotationFrom(Authority.class, getClass(), null);
		if (authority == null) {
			throw DatabaseException.missingClassAnnotation(Authority.class, getClass());
		}
		this.mAuthority = authority.value();
	}

	/**
	 * Creates a new instance of DatabaseProvider with the specified <var>authority</var>.
	 *
	 * @param authority The authority of this DatabaseProvider. This need to be the same authority
	 *                  as presented within <b>AndroidManifest.xml</b>.
	 */
	public DatabaseProvider(@NonNull String authority) {
		super();
		this.mAuthority = authority;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Ensures that database for {@link DatabaseProvider} with the specified <var>authority</var> is
	 * created.
	 *
	 * @param context    Context used to access {@link ContentResolver}.
	 * @param authority  The authority of DatabaseProvider of which database to ensure that is created.
	 * @param entityName Name of {@link DatabaseEntity} that is assigned to the database. May be name
	 *                   of any entity that is assigned to that database. See {@link DatabaseEntity#getName()}.
	 *                   <b>Note, that the table of the specified entity need to have {@link Column.Primary#COLUMN_NAME _id}
	 *                   column presented.</b>
	 */
	public static void ensureDatabaseCreated(@NonNull Context context, @NonNull String authority, @NonNull String entityName) {
		final Cursor cursor = context.getContentResolver().query(
				Database.createContentUri(authority, entityName),
				new String[]{Column.Primary.COLUMN_NAME},
				null,
				null,
				Column.Primary.COLUMN_NAME + " LIMIT 1"
		);
		if (cursor != null) {
			cursor.close();
		}
	}

	/**
	 * Returns instance of the primary Database stored by one of DatabaseProvider instances.
	 *
	 * @return Database instance that is marked by {@link PrimaryDatabase @PrimaryDatabase} annotation
	 * and has been properly created within its associated DatabaseProvider instance.
	 * @throws DatabaseException If primary database does not exist.
	 */
	@NonNull
	public static Database providePrimaryDatabase() {
		synchronized (sDatabases) {
			if (sPrimaryDatabase == null) {
				throw DatabaseException.exception("Primary database not found. " +
						"One of databases must be marked by @PrimaryDatabase annotation.\n" +
						"Ensure that all databases are attached to theirs corresponding provider and " +
						"that all those providers are specified within AndroidManifest.xml via <provider .../> tag."
				);
			}
		}
		return sPrimaryDatabase;
	}

	/**
	 * Returns instance of the Database requested by the specified <var>classOfDatabase</var>.
	 *
	 * @param classOfDatabase Class of the database to return.
	 * @param <T>             Type of the requested database.
	 * @return Requested Database instance.
	 * @throws DatabaseException If database with the requested class does not exist.
	 */
	@NonNull
	@SuppressWarnings("unchecked")
	public static <T extends Database> T provideDatabase(@NonNull Class<T> classOfDatabase) {
		synchronized (sDatabases) {
			if (!sDatabases.containsKey(classOfDatabase)) {
				final String databaseName = classOfDatabase.getSimpleName();
				throw DatabaseException.exception("No such database(" + databaseName + ") found.\n" +
						"Ensure that this database is attached to its corresponding provider and such " +
						"provider is specified within AndroidManifest.xml via <provider .../> tag."
				);
			}
		}
		return (T) sDatabases.get(classOfDatabase);
	}

	/**
	 * Stores the specified <var>database</var> instance so it can be later accessed.
	 *
	 * @param database Database instance to be stored.
	 */
	@VisibleForTesting
	static void storeDatabase(Database database) {
		synchronized (sDatabases) {
			final Class<? extends Database> databaseClass = database.getClass();
			sDatabases.put(databaseClass, database);
			// Store as primary database if it is marked by @PrimaryDatabase annotation.
			if (databaseClass.isAnnotationPresent(PrimaryDatabase.class)) {
				if (sPrimaryDatabase == null) {
					sPrimaryDatabase = database;
				} else {
					throw DatabaseException.misconfiguration("Only one database can be primary.");
				}
			}
		}
	}

	/**
	 * Returns authority of this database provider.
	 *
	 * @return Provider's authority supplied during its initialization or via {@link Authority @Authority}
	 * annotation.
	 */
	@NonNull
	public final String getAuthority() {
		return mAuthority;
	}

	/**
	 */
	@Override
	@SuppressWarnings("ConstantConditions")
	public boolean onCreate() {
		final Context context = getContext();
		this.mDatabase = onCreateDatabase(context);
		this.mDatabase.attachToProvider(this);
		storeDatabase(mDatabase);
		if (onImportInitialDatabase(context, mDatabase) && DatabaseConfig.LOG_ENABLED) {
			Log.v(TAG, "Successfully imported initial database with name(" + mDatabase.getName() + ").");
		}
		mDatabase.create(context);
		return true;
	}

	/**
	 * Invoked from {@link #onCreate()} to create and attach Database object to this provider.
	 *
	 * @param context Context that may be used to properly set up the requested database.
	 * @return Database that will be managed by this provider.
	 */
	@NonNull
	protected abstract Database onCreateDatabase(@NonNull Context context);

	/**
	 * Called when this provider is being created to import initial database structure also with
	 * initial content if need.
	 * <p>
	 * This implementation by default searches for database file at {@code assets/database/DATABASE_NAME}
	 * path. If such file exists, its content is than copied into file at
	 * {@link Context#getDatabasePath(String) Context.getDatabasePath(DATABASE_NAME)} path.
	 *
	 * @param context  Application context that may be used to access application data.
	 * @param database The database instance created via {@link #onCreateDatabase(Context)}.
	 * @return {@code True} if database has been successfully imported, {@code false} otherwise.
	 */
	protected boolean onImportInitialDatabase(@NonNull Context context, @NonNull Database database) {
		final String databaseName = database.getName();
		if (TextUtils.isEmpty(databaseName)) {
			return false;
		}
		// First check if there is database file presented within assets at path assets/database/.
		InputStream dbAssetStream;
		try {
			dbAssetStream = context.getAssets().open("database/" + databaseName);
		} catch (IOException ignored) {
			// Initial database file not found.
			return false;
		}
		try {
			// We have initial database file within assets, copy it into internal android directory
			// dedicated for storing application's database data.
			boolean imported = false;
			final File dbFile = context.getDatabasePath(databaseName);
			if (!dbFile.exists()) {
				final File dbDir = dbFile.getParentFile();
				if (!dbDir.exists() && !dbDir.mkdirs()) {
					Log.e(TAG, "Failed to create parent directory for database(" + dbFile.getPath() + ") file.");
					return false;
				}
				final FileOutputStream dbFileOutputStream = new FileOutputStream(dbFile);
				final byte[] buffer = new byte[1024];
				int bytes;
				while ((bytes = dbAssetStream.read(buffer, 0, buffer.length)) > 0) {
					dbFileOutputStream.write(buffer, 0, bytes);
				}
				dbFileOutputStream.flush();
				dbFileOutputStream.close();
				imported = true;
			}
			dbAssetStream.close();
			return imported;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Returns the database of this provider attached during its creation.
	 *
	 * @return Attached database that may be used to access the data within it or its assigned entities.
	 */
	@NonNull
	protected final Database getDatabase() {
		return mDatabase;
	}

	/**
	 * Delegates to {@link Database#getType(Uri)}.
	 */
	@Override
	public String getType(@NonNull Uri uri) {
		return mDatabase.getType(uri);
	}

	/**
	 * Delegates to {@link Database#insert(Uri, ContentValues)}.
	 */
	@Override
	public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
		return mDatabase.insert(uri, values);
	}

	/**
	 * Delegates to {@link Database#bulkInsert(Uri, ContentValues[])}.
	 */
	@Override
	public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
		return mDatabase.bulkInsert(uri, values);
	}

	/**
	 * Delegates to {@link Database#query(Uri, String[], String, String[], String)}.
	 */
	@Override
	public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
		return mDatabase.query(uri, projection, selection, selectionArgs, sortOrder);
	}

	/**
	 * Delegates to {@link Database#update(Uri, ContentValues, String, String[])}.
	 */
	@Override
	public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
		return mDatabase.update(uri, values, selection, selectionArgs);
	}

	/**
	 * Delegates to {@link Database#delete(Uri, String, String[])}.
	 */
	@Override
	public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
		return mDatabase.delete(uri, selection, selectionArgs);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
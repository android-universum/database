Database-Model-Collection
===============

This module contains collections that may be used to store set of `EntityModel` instances.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-model-collection:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-model-core](https://bitbucket.org/android-universum/database/src/main/library-model-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [EntityModelList](https://bitbucket.org/android-universum/database/src/main/library-model-collection/src/main/java/universum/studios/android/database/model/EntityModelList.java)
- [EntityModelArrayList](https://bitbucket.org/android-universum/database/src/main/library-model-collection/src/main/java/universum/studios/android/database/model/EntityModelArrayList.java)
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import java.util.List;

/**
 * An extended {@link List} interface that may be used for list implementations containing re-usable
 * entity models. Contents of a specific list implementation may be recycled via {@link #recycle()}
 * method.
 *
 * @author Martin Albedinsky
 */
public interface EntityModelList<M extends EntityModel> extends List<M> {

	/**
	 * Recycles all entity models contained within this list so they may be later re-used.
	 * <p>
	 * Implementations should call {@link EntityModel#recycle()} for each of theirs {@code none null}
	 * entries.
	 */
	void recycle();
}
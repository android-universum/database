/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import java.util.ArrayList;
import java.util.Collection;

import androidx.annotation.NonNull;

/**
 * A {@link ArrayList} and {@link EntityModelList} implementation that may be used for re-usable
 * entity models that support recycling feature via {@link EntityModel#recycle()}. When an instance
 * of this array list implementation is not further needed, all entity models currently contained
 * within it may be recycled via {@link #recycle()} method so they may be later re-used.
 *
 * @param <M>
 * @author Martin Albedinsky
 */
public class EntityModelArrayList<M extends EntityModel> extends ArrayList<M> implements EntityModelList<M> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "EntityModelArrayList";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of EntityModelArrayList with no data.
	 *
	 * @see ArrayList#ArrayList()
	 */
	public EntityModelArrayList() {
		super();
	}

	/**
	 * Creates a new instance of EntityModelArrayList with the initial data capacity.
	 *
	 * @param initialCapacity The desired initial capacity.
	 * @see ArrayList#ArrayList(int)
	 */
	public EntityModelArrayList(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * Creates a new instance fo EntityModelArrayList with the contents of the given <var>collection</var>.
	 *
	 * @param collection The collection with the data for the new array list.
	 * @see ArrayList#ArrayList(Collection)
	 */
	public EntityModelArrayList(@NonNull Collection<? extends M> collection) {
		super(collection);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override
	public void recycle() {
		if (!isEmpty()) {
			for (final M model : this) {
				if (model != null) model.recycle();
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.cursor.CursorUtils;
import universum.studios.android.database.cursor.ItemCursor;

/**
 * Utility class that may be used to create collections of {@link EntityModel EntityModels}.
 *
 * @author Martin Albedinsky
 */
public final class EntityModelCollections {

	/**
	 */
	private EntityModelCollections() {
		// Creation of instances of this class is not publicly allowed.
	}

	/**
	 * Returns data of the given cursor as {@link EntityModelList} of item models.
	 *
	 * @return List of models with theirs corresponding data or empty list if the provided cursor
	 * does not have any data available or it is {@code null}.
	 */
	@NonNull
	public static <M extends EntityModel> EntityModelList<M> listFromCursor(@Nullable ItemCursor<M> cursor) {
		if (cursor == null || !cursor.moveToFirst()) {
			return new EntityModelArrayList<>(0);
		}
		final EntityModelList<M> collection = new EntityModelArrayList<>(cursor.getCount());
		do {
			collection.add(cursor.getItem());
		} while (cursor.moveToNext());
		return collection;
	}

	/**
	 * Returns data of the given cursor as {@link LongSparseArray} of item models mapped to theirs id.
	 *
	 * @return Sparse array of models with theirs corresponding data or empty array if the provided
	 * cursor does not have any data available or it is {@code null}.
	 */
	@NonNull
	public static <M extends EntityModel> LongSparseArray<M> sparseArrayFromCursor(@Nullable ItemCursor<M> cursor) {
		if (cursor == null || !cursor.moveToFirst()) {
			return new LongSparseArray<>(0);
		}
		final LongSparseArray<M> collection = new LongSparseArray<>(cursor.getCount());
		do {
			collection.append(CursorUtils.obtainLong(cursor, Column.Primary.COLUMN_NAME), cursor.getItem());
		} while (cursor.moveToNext());
		return collection;
	}
}
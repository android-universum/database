/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.annotation.TargetApi;
import android.os.Build;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * An exception that is used across the Database library to inform about any misconfiguration or about
 * missing annotations that are required for proper working of parts of an Android application that
 * depend on the Database library.
 *
 * @author Martin Albedinsky
 */
public final class DatabaseException extends AndroidRuntimeException {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DatabaseException";

	/**
	 * Default type of database exception.
	 */
	public static final int TYPE_DEFAULT = 0x00;

	/**
	 * Type of database exception determining that such exception has been thrown only due to wrong
	 * configuration.
	 */
	public static final int TYPE_MISCONFIGURATION = 0x01;

	/**
	 * Type of database exception determining that such exception has been thrown due to instantiation
	 * failure of a specific <b>class</b> of which instance is required.
	 */
	public static final int TYPE_INSTANTIATION = 0x02;

	/**
	 * Type of database exception determining that such exception has been thrown due to missing
	 * required <b>class</b> annotation.
	 */
	public static final int TYPE_MISSING_CLASS_ANNOTATION = 0x03;

	/**
	 * Type of database exception determining that such exception has been thrown due to missing
	 * required <b>field</b> annotation.
	 */
	public static final int TYPE_MISSING_FIELD_ANNOTATION = 0x04;

	/**
	 * Message of database exception that can be created via {@link #annotationsNotEnabled()}.
	 */
	private static final String ANNOTATIONS_NOT_ENABLED_MESSAGE =
			"Trying to access logic that requires annotations processing to be enabled, " +
					"but it seams that the annotations processing is disabled for the Database library.";

	/**
	 * Format for the message of database exception that can be created via {@link #instantiation(String, Class, Throwable, String)}.
	 */
	private static final String INSTANTIATION_FORMAT = "Failed to instantiate instance of %s class of(%s).";

	/**
	 * Default note for database exception that can be created via {@link #instantiation(String, Class, Throwable)}
	 */
	private static final String INSTANTIATION_NOTE = "Make sure that such class has public access and empty (without arguments) public constructor.";

	/**
	 * Format for the message of database exception that can be created via {@link #missingClassAnnotation(Class, Class, String)}.
	 */
	private static final String MISSING_CLASS_ANNOTATION_FORMAT = "@%s annotation for class(%s) is missing.";

	/**
	 * Format for the message of database exception that can be created via {@link #missingFieldAnnotation(Class, Field, Class, String)}.
	 */
	private static final String MISSING_FIELD_ANNOTATION_FORMAT = "@%s annotation for field(%s) of class(%s) is missing.";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Type of this database exception. Can be one of types defined within this class.
	 */
	private final int mType;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of DatabaseException of the specified <var>type</var>.
	 *
	 * @param type    Type of the new exception.
	 * @param message Message for the new exception.
	 */
	private DatabaseException(int type, String message) {
		super(message);
		this.mType = type;
	}

	/**
	 * Creates a new instance of DatabaseException of the specified <var>type</var>.
	 *
	 * @param type    Type of the new exception.
	 * @param message Message for the new exception.
	 * @param cause   Cause of the new exception.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private DatabaseException(int type, String message, Throwable cause) {
		super(message, cause);
		this.mType = type;
	}

	/**
	 * Creates a new instance of DatabaseException of the specified <var>type</var>.
	 *
	 * @param type  Type of the new exception.
	 * @param cause Cause of the new exception.
	 */
	private DatabaseException(int type, Exception cause) {
		super(cause);
		this.mType = type;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_DEFAULT}.
	 *
	 * @param message A message for the new exception.
	 */
	@NonNull
	public static DatabaseException exception(String message) {
		return new DatabaseException(TYPE_DEFAULT, message);
	}

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_DEFAULT}.
	 *
	 * @param message A message for the new exception.
	 * @param cause   A cause for the new exception.
	 */
	@NonNull
	public static DatabaseException exception(String message, Throwable cause) {
		return new DatabaseException(TYPE_DEFAULT, message, cause);
	}

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_DEFAULT}.
	 *
	 * @param cause A cause for the new exception.
	 */
	@NonNull
	public static DatabaseException exception(Exception cause) {
		return new DatabaseException(TYPE_DEFAULT, cause);
	}

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_MISCONFIGURATION} with message
	 * saying that annotations are not enabled but functionality that requires annotations to be enabled
	 * has been requested.
	 */
	@NonNull
	public static DatabaseException annotationsNotEnabled() {
		return misconfiguration(ANNOTATIONS_NOT_ENABLED_MESSAGE);
	}

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_MISCONFIGURATION} with the
	 * specified <var>message</var>.
	 *
	 * @param message The message for the new misconfiguration exception.
	 */
	@NonNull
	public static DatabaseException misconfiguration(@NonNull String message) {
		return new DatabaseException(TYPE_MISCONFIGURATION, message);
	}

	/**
	 * Same as {@link #instantiation(String, Class, Throwable, String)} with default additional note.
	 */
	@NonNull
	public static DatabaseException instantiation(@NonNull String objectType, @NonNull Class<?> classOf, @Nullable Throwable cause) {
		return instantiation(objectType, classOf, cause, INSTANTIATION_NOTE);
	}

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_INSTANTIATION} for the specified
	 * <var>objectType</var> and <var>classOf</var>.
	 *
	 * @param objectType Type (name) of the object of which instantiation has failed.
	 * @param classOf    Class of the object of which instantiation has failed.
	 * @param cause      todo:
	 * @param note       Additional note for the new instantiation exception that will be added at
	 *                   the end of the exception's message text.
	 */
	@NonNull
	public static DatabaseException instantiation(@NonNull String objectType, @NonNull Class<?> classOf, @Nullable Throwable cause, @NonNull String note) {
		return new DatabaseException(
				TYPE_INSTANTIATION,
				String.format(INSTANTIATION_FORMAT,
						objectType,
						classOf.getSimpleName()
				) + (TextUtils.isEmpty(note) ? "" : " " + note),
				cause
		);
	}

	/**
	 * Same as {@link #missingClassAnnotation(Class, Class, String)} without additional note.
	 */
	@NonNull
	public static DatabaseException missingClassAnnotation(@NonNull Class<? extends Annotation> annotation, @NonNull Class<?> forClass) {
		return missingClassAnnotation(annotation, forClass, "");
	}

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_MISSING_CLASS_ANNOTATION} for
	 * the specified <var>annotation</var> and <var>forClass</var>.
	 *
	 * @param annotation Type of the missing annotation that is required for proper working of the
	 *                   Database library.
	 * @param forClass   The class that is missing the required annotation.
	 * @param note       Additional note for the new missing annotation exception that will be added
	 *                   at the end of the exception's message text.
	 */
	@NonNull
	public static DatabaseException missingClassAnnotation(@NonNull Class<? extends Annotation> annotation, @NonNull Class<?> forClass, @NonNull String note) {
		return new DatabaseException(
				TYPE_MISSING_CLASS_ANNOTATION,
				String.format(MISSING_CLASS_ANNOTATION_FORMAT,
						annotation.getSimpleName(),
						forClass.getSimpleName()
				) + (TextUtils.isEmpty(note) ? "" : " " + note)
		);
	}

	/**
	 * Same as {@link #missingFieldAnnotation(Class, Field, Class, String)} without additional note.
	 */
	@NonNull
	public static DatabaseException missingFieldAnnotation(@NonNull Class<? extends Annotation> annotation, @NonNull Field field, @NonNull Class<?> ofClass) {
		return missingFieldAnnotation(annotation, field, ofClass, "");
	}

	/**
	 * Creates a new instance of DatabaseException type of {@link #TYPE_MISSING_FIELD_ANNOTATION} for
	 * the specified <var>annotation</var> and <var>field</var>.
	 *
	 * @param annotation Type of the missing annotation that is required for proper working of the
	 *                   Database library.
	 * @param field      The field that is missing the required annotation.
	 * @param ofClass    A class that is holder of the specified field.
	 * @param note       Additional note for the new missing annotation exception that will be added
	 *                   at the end of the exception's message text.
	 */
	@NonNull
	public static DatabaseException missingFieldAnnotation(@NonNull Class<? extends Annotation> annotation, @NonNull Field field, @NonNull Class<?> ofClass, @NonNull String note) {
		return new DatabaseException(
				TYPE_MISSING_FIELD_ANNOTATION,
				String.format(MISSING_FIELD_ANNOTATION_FORMAT,
						annotation.getSimpleName(),
						field.getName(),
						ofClass.getSimpleName()
				) + (TextUtils.isEmpty(note) ? "" : " " + note)
		);
	}

	/**
	 * Returns a type of this exception.
	 *
	 * @return One of {@link #TYPE_MISCONFIGURATION}, {@link #TYPE_MISSING_CLASS_ANNOTATION},
	 * {@link #TYPE_MISSING_FIELD_ANNOTATION}, {@link #TYPE_INSTANTIATION}.
	 */
	public int getType() {
		return mType;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
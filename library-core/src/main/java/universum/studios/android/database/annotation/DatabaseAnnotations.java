/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseConfig;
import universum.studios.android.database.DatabaseException;

/**
 * Annotation utils for the Database library.
 *
 * @author Martin Albedinsky
 */
public final class DatabaseAnnotations {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "DatabaseAnnotations";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Simple callback which allows processing of all declared fields of a desired class via
	 * {@link #iterateFields(FieldProcessor, Class, Class)}.
	 *
	 * @author Martin Albedinsky
	 */
	public interface FieldProcessor {

		/**
		 * Invoked for each of iterated fields.
		 *
		 * @param field The currently iterated field.
		 * @param name  Name of the currently iterated field.
		 */
		void onProcessField(@NonNull Field field, @NonNull String name);
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private DatabaseAnnotations() {
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Performs check for enabled state of the annotations processing for the Database library.
	 * <p>
	 * This check is requested mostly from parts of the Database library that require to be annotations
	 * processing enabled.
	 *
	 * @throws DatabaseException If annotations processing is disabled.
	 */
	public static void checkIfEnabledOrThrow() {
		if (!DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) {
			throw DatabaseException.annotationsNotEnabled();
		}
	}

	/**
	 * Performs check for enabled state of the annotations processing for the Database library.
	 *
	 * @param exceptionMessage Message for the exception to be thrown when annotations processing is
	 *                         not enabled.
	 * @throws DatabaseException If annotations processing is disabled.
	 */
	public static void checkIfEnabledOrThrow(@NonNull String exceptionMessage) {
		if (!DatabaseConfig.ANNOTATIONS_PROCESSING_ENABLED) {
			throw DatabaseException.exception(exceptionMessage);
		}
	}

	/**
	 * Obtains the requested type of annotation from the given <var>fromClass</var> if it is presented.
	 *
	 * @param classOfAnnotation Class of the requested annotation.
	 * @param fromClass         Class from which should be the requested annotation obtained.
	 * @param maxSuperClass     If {@code not null}, this method will be called (recursively) for
	 *                          all super classes of the given annotated class (max to the specified
	 *                          <var>maxSuperClass</var> excluding) until the requested annotation
	 *                          is presented and obtained, otherwise annotation will be obtained only
	 *                          from the given annotated class.
	 * @param <A>               Type of the requested annotation.
	 * @return Obtained annotation or {@code null} if the requested annotation is not presented
	 * for the given class or its supers if requested.
	 */
	@Nullable
	public static <A extends Annotation> A obtainAnnotationFrom(@NonNull Class<A> classOfAnnotation, @NonNull Class<?> fromClass, @Nullable Class<?> maxSuperClass) {
		final A annotation = fromClass.getAnnotation(classOfAnnotation);
		if (annotation != null) {
			return annotation;
		} else if (maxSuperClass != null) {
			final Class<?> parent = fromClass.getSuperclass();
			if (parent != null && !parent.equals(maxSuperClass)) {
				return obtainAnnotationFrom(classOfAnnotation, parent, maxSuperClass);
			}
		}
		return null;
	}

	/**
	 * Iterates all declared fields of the given <var>ofClass</var>.
	 *
	 * @param processor     Field processor callback to be invoked for each of iterated fields.
	 * @param ofClass       Class of which fields to iterate.
	 * @param maxSuperClass If {@code not null}, this method will be called (recursively) for all
	 *                      super classes of the given class (max to the specified <var>maxSuperClass</var>
	 *                      excluding), otherwise only fields of the given class will be iterated.
	 */
	public static void iterateFields(@NonNull FieldProcessor processor, @NonNull Class<?> ofClass, @Nullable Class<?> maxSuperClass) {
		final Field[] fields = ofClass.getDeclaredFields();
		if (fields.length > 0) {
			for (final Field field : fields) {
				processor.onProcessField(field, field.getName());
			}
		}
		if (maxSuperClass != null) {
			final Class<?> parent = ofClass.getSuperclass();
			if (parent != null && !parent.equals(maxSuperClass)) {
				iterateFields(processor, parent, maxSuperClass);
			}
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
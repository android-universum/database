/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;

/**
 * Utility class that is used across Database library to resolve type of a specific class whether it
 * is a primitive or boxed type or a type of which data can be in proper representation stored within
 * a database table column.
 *
 * <h3>Supported types</h3>
 * <ul>
 * <li>{@link Byte} +</li>
 * <li>{@link Character} +</li>
 * <li>{@link Short} +</li>
 * <li>{@link Boolean} +</li>
 * <li>{@link Integer} +</li>
 * <li>{@link Long} +</li>
 * <li>{@link Float} +</li>
 * <li>{@link Double} +</li>
 * <li>{@link String} +</li>
 * <li>{@link Enum} (stored by its name as string)</li>
 * </ul>
 *
 * <b>Legend</b>
 * <p>
 * + stored as raw value
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class TypeUtils {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>unknown</b> data type.
	 */
	public static final int UNKNOWN = -0x1000;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>null</b> data type.
	 */
	public static final int NULL = 0x00;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>byte</b> data type.
	 */
	public static final int BYTE = 0x01;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>byte array</b> data type.
	 */
	public static final int BYTE_ARRAY = -BYTE;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>char</b> data type.
	 */
	public static final int CHAR = 0x02;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>char array</b> data type.
	 */
	public static final int CHAR_ARRAY = -CHAR;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>short</b> data type.
	 */
	public static final int SHORT = 0x03;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>short array</b> data type.
	 */
	public static final int SHORT_ARRAY = -SHORT;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>boolean</b> data type.
	 */
	public static final int BOOLEAN = 0x04;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>boolean array</b> data type.
	 */
	public static final int BOOLEAN_ARRAY = -BOOLEAN;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>integer</b> data type.
	 */
	public static final int INTEGER = 0x05;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>integer array</b> data type.
	 */
	public static final int INTEGER_ARRAY = -INTEGER;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>long</b> data type.
	 */
	public static final int LONG = 0x06;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>long array</b> data type.
	 */
	public static final int LONG_ARRAY = -LONG;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>float</b> data type.
	 */
	public static final int FLOAT = 0x07;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>float array</b> data type.
	 */
	public static final int FLOAT_ARRAY = -FLOAT;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>double</b> data type.
	 */
	public static final int DOUBLE = 0x08;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>double array</b> data type.
	 */
	public static final int DOUBLE_ARRAY = -DOUBLE;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>string</b> data type.
	 */
	public static final int STRING = 0x09;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>string array</b> data type.
	 */
	public static final int STRING_ARRAY = -STRING;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>enum</b> data type.
	 */
	public static final int ENUM = 0x0a;

	/**
	 * Constant determining that type or class of type passed to one of resolve methods of this utility
	 * class represents <b>enum array</b> data type.
	 */
	public static final int ENUM_ARRAY = -ENUM;

	/**
	 * Defines an annotation for determining set of defined types in {@link TypeUtils} class.
	 */
	@Retention(RetentionPolicy.SOURCE)
	@IntDef({
			UNKNOWN, NULL,
			BYTE, BYTE_ARRAY, CHAR, CHAR_ARRAY, SHORT, SHORT_ARRAY,
			BOOLEAN, BOOLEAN_ARRAY,
			INTEGER, INTEGER_ARRAY, LONG, LONG_ARRAY, FLOAT, FLOAT_ARRAY, DOUBLE, DOUBLE_ARRAY,
			STRING, STRING_ARRAY,
			ENUM, ENUM_ARRAY
	})
	public @interface Type {}

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private TypeUtils() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Resolves a data type of the given class.
	 * <p>
	 * <b>Note</b> that this implementation will check as classes of primitive types so classes of
	 * boxed types.
	 *
	 * @param classOfValue The class of which data type to resolve.
	 * @return One of data types defined by this class or {@link #UNKNOWN} if the given class is of
	 * unknown type (not primitive or boxed type) or the class is {@code null}.
	 */
	@Type public static int resolveType(@Nullable final Class<?> classOfValue) {
		if (classOfValue != null) {
			if (byte.class.equals(classOfValue) || Byte.class.equals(classOfValue)) {
				return BYTE;
			} else if (byte[].class.equals(classOfValue) || Byte[].class.equals(classOfValue)) {
				return BYTE_ARRAY;
			} else if (char[].class.equals(classOfValue) || Character[].class.equals(classOfValue)) {
				return CHAR_ARRAY;
			} else if (char.class.equals(classOfValue) || Character.class.equals(classOfValue)) {
				return CHAR;
			} else if (short.class.equals(classOfValue) || Short.class.equals(classOfValue)) {
				return SHORT;
			} else if (short[].class.equals(classOfValue) || Short[].class.equals(classOfValue)) {
				return SHORT_ARRAY;
			} else if (boolean.class.equals(classOfValue) || Boolean.class.equals(classOfValue)) {
				return BOOLEAN;
			} else if (boolean[].class.equals(classOfValue) || Boolean[].class.equals(classOfValue)) {
				return BOOLEAN_ARRAY;
			} else if (int.class.equals(classOfValue) || Integer.class.equals(classOfValue)) {
				return INTEGER;
			} else if (int[].class.equals(classOfValue) || Integer[].class.equals(classOfValue)) {
				return INTEGER_ARRAY;
			} else if (long.class.equals(classOfValue) || Long.class.equals(classOfValue)) {
				return LONG;
			} else if (long[].class.equals(classOfValue) || Long[].class.equals(classOfValue)) {
				return LONG_ARRAY;
			} else if (float.class.equals(classOfValue) || Float.class.equals(classOfValue)) {
				return FLOAT;
			} else if (float[].class.equals(classOfValue) || Float[].class.equals(classOfValue)) {
				return FLOAT_ARRAY;
			} else if (double.class.equals(classOfValue) || Double.class.equals(classOfValue)) {
				return DOUBLE;
			} else if (double[].class.equals(classOfValue) || Double[].class.equals(classOfValue)) {
				return DOUBLE_ARRAY;
			} else if (String.class.equals(classOfValue)) {
				return STRING;
			} else if (String[].class.equals(classOfValue)) {
				return STRING_ARRAY;
			} else if (Enum.class.isAssignableFrom(classOfValue)) {
				return ENUM;
			} else if (Enum[].class.isAssignableFrom(classOfValue)) {
				return ENUM_ARRAY;
			}
		}
		return UNKNOWN;
	}

	/**
	 * Unlike {@link #resolveType(Class)}, this implementation will check only classes of primitive
	 * types.
	 *
	 * @param classOfValue The class of which primitive data type to resolve.
	 */
	@Type public static int resolvePrimitiveType(@Nullable final Class<?> classOfValue) {
		if (classOfValue != null) {
			if (byte.class.equals(classOfValue)) {
				return BYTE;
			} else if (byte[].class.equals(classOfValue)) {
				return BYTE_ARRAY;
			} else if (char.class.equals(classOfValue)) {
				return CHAR;
			} else if (char[].class.equals(classOfValue)) {
				return CHAR_ARRAY;
			} else if (short.class.equals(classOfValue)) {
				return SHORT;
			} else if (short[].class.equals(classOfValue)) {
				return SHORT_ARRAY;
			} else if (boolean.class.equals(classOfValue)) {
				return BOOLEAN;
			} else if (boolean[].class.equals(classOfValue)) {
				return BOOLEAN_ARRAY;
			} else if (int.class.equals(classOfValue)) {
				return INTEGER;
			} else if (int[].class.equals(classOfValue)) {
				return INTEGER_ARRAY;
			} else if (long.class.equals(classOfValue)) {
				return LONG;
			} else if (long[].class.equals(classOfValue)) {
				return LONG_ARRAY;
			} else if (float.class.equals(classOfValue)) {
				return FLOAT;
			} else if (float[].class.equals(classOfValue)) {
				return FLOAT_ARRAY;
			} else if (double.class.equals(classOfValue)) {
				return DOUBLE;
			} else if (double[].class.equals(classOfValue)) {
				return DOUBLE_ARRAY;
			} else if (String.class.equals(classOfValue)) {
				return STRING;
			} else if (String[].class.equals(classOfValue)) {
				return STRING_ARRAY;
			}
		}
		return UNKNOWN;
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.annotation.Column;

/**
 * This class specifies all <b>data types</b> that are supported by SQLite database. These types
 * can be used to request a field that will represent a database table column to be a desired type
 * of when marked by {@link Column @Column} annotation.
 *
 * <h3>SQLite 3 data types:</h3>
 * See SQLite Version 3 <a href="http://www.sqlite.org/datatype3.html">documentation</a> for info.
 * <p>
 * <b>Note</b>, that by SQLite data types documentation mentioned above, numeric arguments in parentheses
 * that follows the type name (ex: "VARCHAR(255)") are ignored by SQLite - <b>SQLite does not impose
 * any length restrictions (other than the large global SQLITE_MAX_LENGTH limit) on the length of
 * strings, BLOBs or numeric values.</b>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class ColumnType {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "ColumnType";

	/**
	 * Column type determining that a type of column for a specific column field should be resolved
	 * from that field's Java type.
	 *
	 * @see TypeUtils#resolveType(Class)
	 */
	public static final int AUTO = -0x0001;

	// NONE Affinity types -------------------------------------------------------------------------

	/**
	 * <b>Blob</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>NONE</b>
	 */
	public static final int BLOB = 0x0000;

	// INTEGER Affinity types ----------------------------------------------------------------------

	/**
	 * <b>Int</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int INT = 0x0001;

	/**
	 * <b>Integer</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int INTEGER = 0x0002;

	/**
	 * <b>Tinyint</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int TINYINT = 0x0003;

	/**
	 * <b>Smallint</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int SMALLINT = 0x0004;

	/**
	 * <b>Mediumint</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int MEDIUMINT = 0x0005;

	/**
	 * <b>Bigint</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int BIGINT = 0x0006;

	/**
	 * <b>Unsigned big int</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int UNSIGNED_BIG_INT = 0x0007;

	/**
	 * <b>Int2</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int INT2 = 0x0008;

	/**
	 * <b>Int8</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>INTEGER</b>
	 */
	public static final int INT8 = 0x0009;

	// TEXT Affinity types -------------------------------------------------------------------------

	/**
	 * <b>Character</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int CHARACTER = 0x1001;

	/**
	 * <b>Varchar</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int VARCHAR = 0x1002;

	/**
	 * <b>Varying character</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int VARYING_CHARACTER = 0x1003;

	/**
	 * <b>Nchar</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int NCHAR = 0x1004;

	/**
	 * <b>Native character</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int NATIVE_CHARACTER = 0x1005;

	/**
	 * <b>Nvarchar</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int NVARCHAR = 0x1006;

	/**
	 * <b>Text</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int TEXT = 0x1007;

	/**
	 * <b>Clob</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>TEXT</b>
	 */
	public static final int CLOB = 0x1008;

	// REAL Affinity types -------------------------------------------------------------------------

	/**
	 * <b>Real</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>REAL</b>
	 */
	public static final int REAL = 0x2001;

	/**
	 * <b>Double</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>REAL</b>
	 */
	public static final int DOUBLE = 0x2002;

	/**
	 * <b>Double precision</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>REAL</b>
	 */
	public static final int DOUBLE_PRECISION = 0x2003;

	/**
	 * <b>Float</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>REAL</b>
	 */
	public static final int FLOAT = 0x2004;

	// NUMERIC Affinity types ----------------------------------------------------------------------

	/**
	 * <b>Numeric</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>NUMERIC</b>
	 */
	public static final int NUMERIC = 0x3001;

	/**
	 * <b>Decimal</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>NUMERIC</b>
	 */
	public static final int DECIMAL = 0x3002;

	/**
	 * <b>Boolean</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>NUMERIC</b>
	 */
	public static final int BOOLEAN = 0x3003;

	/**
	 * <b>Date</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>NUMERIC</b>
	 */
	public static final int DATE = 0x3004;

	/**
	 * <b>Datetime</b> SQL value type.
	 * <p>
	 * Resulting Affinity: <b>NUMERIC</b>
	 */
	public static final int DATETIME = 0x3005;

	/**
	 * Defines an annotation for determining set of allowed types which can be created via {@link ColumnType}
	 * factory class.
	 */
	@Retention(RetentionPolicy.SOURCE)
	@IntDef({
			AUTO,
			BLOB,
			INT, INTEGER, TINYINT, SMALLINT, MEDIUMINT, BIGINT, UNSIGNED_BIG_INT, INT2, INT8,
			CHARACTER, VARCHAR, VARYING_CHARACTER, NCHAR, NATIVE_CHARACTER, NVARCHAR, TEXT, CLOB,
			REAL, DOUBLE, DOUBLE_PRECISION, FLOAT,
			NUMERIC, DECIMAL, BOOLEAN, DATE, DATETIME
	})
	public @interface Type {}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Name of this type.
	 */
	private final String name;

	/**
	 * Constraint for this type instance.
	 */
	private Object constraint;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #ColumnType(String, Object)} with {@code null} constraint value.
	 */
	ColumnType(@NonNull final String name) {
		this(name, null);
	}

	/**
	 * Creates a new instance of ColumnType.
	 *
	 * @param name       The desired name for the new type.
	 * @param constraint The desired constraint for the new type. Its string representation will be
	 *                   used when building the new type via {@link #build()}.
	 */
	ColumnType(@NonNull final String name, @Nullable final Object constraint) {
		this.name = name;
		this.constraint = constraint;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns a new instance of ColumnType.
	 *
	 * @param type Flag of the desired type of which instance to return. Use one of type flags specified
	 *             within this class.
	 * @return ColumnType instance for the specified type.
	 * @throws IllegalArgumentException If the requested type is not supported by this class.
	 */
	@NonNull public static ColumnType type(@Type final int type) {
		switch (type) {
			case AUTO:
				throw new IllegalArgumentException("Cannot create ColumnType for AUTO type.");
			case BLOB:
				// INTEGER based types.
			case INT:
			case INTEGER:
			case TINYINT:
			case SMALLINT:
			case MEDIUMINT:
			case BIGINT:
			case UNSIGNED_BIG_INT:
			case INT2:
			case INT8:
				// TEXT based types.
			case CHARACTER:
			case VARCHAR:
			case VARYING_CHARACTER:
			case NCHAR:
			case NATIVE_CHARACTER:
			case NVARCHAR:
			case TEXT:
			case CLOB:
				// REAL based types.
			case REAL:
			case DOUBLE:
			case DOUBLE_PRECISION:
			case FLOAT:
				// NUMERIC based types.
			case NUMERIC:
			case DECIMAL:
			case BOOLEAN:
			case DATE:
			case DATETIME:
				return new ColumnType(nameOfType(type));
			default:
				throw new IllegalArgumentException("Requested unsupported column type(" + type + ").");
		}
	}

	/**
	 * Returns a name of the requested <var>type</var> specified within this class as it should be
	 * used when creating a database table column of such type.
	 *
	 * @param type Flag of the desired type of which name to return. Use one of type flags specified
	 *             within this class.
	 * @return Type name or empty string if the requested type is not specified within this class.
	 */
	@NonNull public static String nameOfType(@Type final int type) {
		switch (type) {
			case BLOB:
				return "BLOB";
			case INT:
				return "INT";
			case INTEGER:
				return "INTEGER";
			case TINYINT:
				return "TINYINT";
			case SMALLINT:
				return "SMALLINT";
			case MEDIUMINT:
				return "MEDIUMINT";
			case BIGINT:
				return "BIGINT";
			case UNSIGNED_BIG_INT:
				return "UNSIGNED_BIG_INT";
			case INT2:
				return "INT2";
			case INT8:
				return "INT8";
			case CHARACTER:
				return "CHARACTER";
			case VARCHAR:
				return "VARCHAR";
			case VARYING_CHARACTER:
				return "VARYING_CHARACTER";
			case NCHAR:
				return "NCHAR";
			case NATIVE_CHARACTER:
				return "NATIVE_CHARACTER";
			case NVARCHAR:
				return "NVARCHAR";
			case TEXT:
				return "TEXT";
			case CLOB:
				return "CLOB";
			case REAL:
				return "REAL";
			case DOUBLE:
				return "DOUBLE";
			case DOUBLE_PRECISION:
				return "DOUBLE_PRECISION";
			case FLOAT:
				return "FLOAT";
			case NUMERIC:
				return "NUMERIC";
			case DECIMAL:
				return "DECIMAL";
			case BOOLEAN:
				return "BOOLEAN";
			case DATE:
				return "DATE";
			case DATETIME:
				return "DATETIME";
			default:
				return "";
		}
	}

	/**
	 * Clears this column type. This actually clears only constraint value of this type.
	 *
	 * @return This column type to allow methods chaining.
	 */
	public ColumnType clear() {
		this.constraint = null;
		return this;
	}

	/**
	 * Sets a value of the constraint for this column type.
	 *
	 * @param value The desired constraint value.
	 * @return This column type to allow methods chaining.
	 *
	 * @see #build()
	 */
	public ColumnType constraint(@Nullable final Object value) {
		this.constraint = value;
		return this;
	}

	/**
	 * Returns the current constraint value of this column type.
	 *
	 * @return Constraint value or {@code null} by default.
	 */
	@Nullable public Object constraint() {
		return constraint;
	}

	/**
	 * Returns the name of this column type.
	 *
	 * @return Type name.
	 */
	@NonNull public final String name() {
		return name;
	}

	/**
	 * Builds a string representation of this column type which can be used when creating database
	 * table columns.
	 *
	 * @return This type as string. If this type instance has specified its constraint, a string
	 * representation of this constraint will be also contained within the resulting string value
	 * enclosed by parenthesis like: <b>VARCHAR(255)</b>.
	 */
	@NonNull public String build() {
		final StringBuilder builder = new StringBuilder(name);
		final String constraint = this.constraint == null ? null : String.valueOf(this.constraint);
		if (constraint != null && constraint.length() > 0) {
			builder.append("(");
			builder.append(constraint);
			builder.append(")");
		}
		return builder.toString();
	}

	/**
	 */
	@Override public int hashCode() {
		int hash = name.hashCode();
		hash = 31 * hash + (constraint == null ? 0 : constraint.hashCode());
		return hash;
	}

	/**
	 */
	@SuppressWarnings("SimplifiableIfStatement")
	@Override public boolean equals(@Nullable final Object other) {
		if (other == this) return true;
		if (!(other instanceof ColumnType)) return false;
		final ColumnType type = (ColumnType) other;
		if (!name.equals(type.name)) {
			return false;
		}
		if (constraint == type.constraint) {
			return true;
		}
		return constraint != null && type.constraint != null && constraint.equals(type.constraint);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
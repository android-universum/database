/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.util.Log;

/**
 * Configuration options for the Database library.
 *
 * @author Martin Albedinsky
 */
public final class DatabaseConfig {

	/**
	 * Flag indicating whether the <b>verbose</b> output for the Database library trough log-cat
	 * is enabled or not.
	 *
	 * @see Log#v(String, String)
	 */
	public static boolean LOG_ENABLED = true;

	/**
	 * Flag indicating whether the <b>debug</b> output for the Database library trough log-cat is
	 * enabled or not.
	 *
	 * @see Log#d(String, String)
	 */
	public static boolean DEBUG_LOG_ENABLED = false;

	/**
	 * Flag indicating whether the processing of annotations for the Database library is enabled
	 * or not.
	 * <p>
	 * If annotations processing is enabled, it may decrease performance for the parts of an Android
	 * application depending on the classes from the Database library that uses annotations.
	 */
	public static boolean ANNOTATIONS_PROCESSING_ENABLED = true;

	/**
	 */
	private DatabaseConfig() {
		// Creation of instances of this class is not publicly allowed.
	}
}
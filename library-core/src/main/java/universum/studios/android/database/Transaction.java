/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import android.database.sqlite.SQLiteDatabase;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;

/**
 * Interface for executable database transaction.
 *
 * @param <R> Type of the result returned by this transaction.
 * @author Martin Albedinsky
 */
public interface Transaction<R> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Defines an annotation for determining allowed transaction modes.
	 *
	 * <h3>Available modes:</h3>
	 * <ul>
	 * <li>{@link #EXCLUSIVE}</li>
	 * <li>{@link #IMMEDIATE}</li>
	 * </ul>
	 */
	@IntDef({NONE, EXCLUSIVE, IMMEDIATE})
	@Retention(RetentionPolicy.SOURCE)
	public @interface Mode {
	}

	/**
	 * Constant that may be used to not execute transaction in any mode.
	 */
	int NONE = 0x00;

	/**
	 * Mode that may be used to begin transaction in <b>exclusive</b> mode.
	 *
	 * @see SQLiteDatabase#beginTransaction()
	 */
	int EXCLUSIVE = 0x01;

	/**
	 * Mode that may be used to begin transaction in <b>immediate</b> mode.
	 * <p>
	 * <b>Note</b>, that this mode is supported from the {@link android.os.Build.VERSION_CODES#HONEYCOMB HONEYCOMB}
	 * Android version.
	 *
	 * @see SQLiteDatabase#beginTransactionNonExclusive()
	 */
	int IMMEDIATE = 0x02;

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Executor that may be used to execute database transactions on a background one at a time.
	 * <p>
	 * This is the same executor as provided via {@link DatabaseExecutors#background()}.
	 */
	Executor EXECUTOR = DatabaseExecutors.background();

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets a mode for this transaction determining how the transaction should be executed.
	 *
	 * @param mode The desired mode. One of modes defined by {@link Mode @Mode} annotation.
	 */
	void setMode(@Mode int mode);

	/**
	 * Executes this transaction.
	 *
	 * @return Result of this transaction. This may be either a success or an error result.
	 */
	@Nullable
	R execute();
}
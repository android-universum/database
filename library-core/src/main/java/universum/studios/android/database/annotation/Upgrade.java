/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import androidx.annotation.IntRange;

/**
 * Defines an annotation for marking a field representing a single column that should be upgraded
 * within a database table.
 * <p>
 * <b>For now is supported only adding of a new columns marked via this annotation into corresponding
 * database table via 'ALTER TABLE table_name ADD COLUMN column_definition_statement' SQL statement</b>.
 *
 * @author Martin Albedinsky
 */
@Documented
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Upgrade {

	/**
	 * Database version on which to perform upgrade.
	 * <p>
	 * Minimum version: {@code 2}
	 */
	@IntRange(from = 2)
	int version();
}
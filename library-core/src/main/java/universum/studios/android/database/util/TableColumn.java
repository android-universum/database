/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;
import androidx.annotation.StringDef;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.Column;

/**
 * TableColumn represents a single column of a specific database entity that may be used to build a
 * column create statement while creating database table.
 * <p>
 * Each column instance need to be created with its name and type that can be specified using
 * {@link ColumnType} class that lists all possible types supported by SQL database.
 * <p>
 * To build a column into representation that can be used within SQL 'CREATE' query, call {@link #build()}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class TableColumn {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "TableColumn";

	/**
	 * <b>Primary key</b> constraint key word.
	 */
	public static final String CONSTRAINT_PRIMARY_KEY = "PRIMARY KEY";

	/**
	 * <b>Autoincrement</b> constraint key word.
	 */
	public static final String CONSTRAINT_AUTOINCREMENT = "AUTOINCREMENT";

	/**
	 * <b>Not null</b> constraint key word.
	 */
	public static final String CONSTRAINT_NOT_NULL = "NOT NULL";

	/**
	 * <b>Unique</b> constraint key word.
	 */
	public static final String CONSTRAINT_UNIQUE = "UNIQUE";

	/**
	 * Defines an annotation for determining set of allowed constraints for {@link #constraint(String)}
	 * method.
	 */
	@Retention(RetentionPolicy.SOURCE)
	@StringDef({
			CONSTRAINT_NOT_NULL,
			CONSTRAINT_PRIMARY_KEY,
			CONSTRAINT_AUTOINCREMENT,
			CONSTRAINT_UNIQUE
	})
	public @interface Constraint {}

	/**
	 * <b>Check</b> constraint key word format.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) check (string) column</li>
	 * </ul>
	 */
	public static final String CONSTRAINT_CHECK = "CHECK (%s)";

	/**
	 * <b>Default</b> constraint key word string based format.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) default (string) value</li>
	 * </ul>
	 */
	public static final String CONSTRAINT_DEFAULT = "DEFAULT '%s'";

	/**
	 * <b>Default</b> constraint key word integer based format.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) default (integer) value</li>
	 * </ul>
	 */
	public static final String CONSTRAINT_DEFAULT_INT = "DEFAULT %d";

	/**
	 * <b>Collate</b> constraint key word format.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) collate (string) value</li>
	 * </ul>
	 */
	public static final String CONSTRAINT_COLLATE = "COLLATE %s";

	/**
	 * <b>Foreign key</b> statement format.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) foreign key name</li>
	 * <li>2) set of references</li>
	 * </ul>
	 */
	public static final String FOREIGN_KEY_FORMAT = "FOREIGN KEY(%s) REFERENCES %s";

	/**
	 * <b>Reference</b> statement format.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) reference</li>
	 * </ul>
	 */
	public static final String REFERENCE_FORMAT = "REFERENCES %s";

	/**
	 * Defines an annotation for determining set of allowed constraints for {@link #constraint(String, Object)}
	 * method.
	 */
	@Retention(RetentionPolicy.SOURCE)
	@StringDef({
			CONSTRAINT_CHECK,
			CONSTRAINT_DEFAULT,
			CONSTRAINT_DEFAULT_INT,
			CONSTRAINT_COLLATE
	})
	public @interface FormattedConstraint {}

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Name of this column
	 */
	private final String name;

	/**
	 * Type of this column.
	 */
	private final ColumnType type;

	/**
	 * Default value of this column.
	 */
	private Object defaultValue;

	/**
	 * Reference value for this column as foreign key.
	 */
	private String reference;

	/**
	 * List of constraints fro this column.
	 */
	private List<String> constraints;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #TableColumn(String, ColumnType)} where the desired type will be obtained
	 * from the default column types by the specified <var>type</var> flag.
	 */
	public TableColumn(@NonNull final String name, @ColumnType.Type final int type) {
		this(name, ColumnType.type(type));
	}

	/**
	 * Creates a new instance of TableColumn with the given <var>name</var> and <var>type</var>.
	 *
	 * @param name The desired name for the new column.
	 * @param type The desired type for the new column.
	 */
	public TableColumn(@NonNull final String name, @NonNull final ColumnType type) {
		this.name = name;
		this.type = type;
	}

	/**
	 * Creates a new instance of Entity column from the specified <var>builder</var>.
	 *
	 * @param builder The builder with data for the new column.
	 */
	TableColumn(final Builder builder) {
		this.name = builder.name;
		this.type = builder.type;
		this.defaultValue = builder.defaultValue;
		this.reference = builder.reference;
		this.constraints = builder.constraints;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Resolves a name for the entity column based on the specified parameters.
	 *
	 * @param fieldName     Name of field that should be used as column name if the <var>annotatedName</var>
	 *                      is empty.
	 * @param annotatedName Column name from the annotation, if empty the field's name will be returned.
	 * @return Column name, either annotated or field's name.
	 */
	@NonNull public static String resolveColumnNameForField(@NonNull final String fieldName, @Nullable final String annotatedName) {
		return TextUtils.isEmpty(annotatedName) ? fieldName : annotatedName;
	}

	/**
	 * Helper method for {@link #reference(String)}. Creates a formatted string in format:
	 * <b>tableName(columnName)</b>.
	 *
	 * @param tableName  Name of entity for which should be reference format created.
	 * @param columnName Name of column which is referenced.
	 * @return Formatted string as described above.
	 */
	@NonNull public static String createReference(@NonNull final String tableName, @NonNull final String columnName) {
		return tableName + "(" + columnName + ")";
	}

	/**
	 * Returns the name of this column.
	 *
	 * @return Column name supplied to constructor.
	 */
	@NonNull public final String name() {
		return name;
	}

	/**
	 * Returns the type of this column.
	 *
	 * @return Type supplied to constructor.
	 */
	@NonNull public final ColumnType type() {
		return type;
	}

	/**
	 * Sets a default value for this column.
	 *
	 * @param value The desired default value. Can be {@code null}.
	 * @return This column to allow methods chaining.
	 *
	 * @see #defaultValue()
	 */
	public TableColumn defaultValue(@Nullable final Object value) {
		this.defaultValue = value;
		return this;
	}

	/**
	 * Returns the default value of this column.
	 *
	 * @return Default value or {@code null} if this column does not have default value specified.
	 *
	 * @see #defaultValue(Object)
	 */
	@Nullable public Object defaultValue() {
		return defaultValue;
	}

	/**
	 * Return a boolean flag indicating whether this column represents a primary key or not.
	 *
	 * @return {@code True} if this column represents primary key so {@link #CONSTRAINT_PRIMARY_KEY}
	 * is presented, {@code false} otherwise.
	 *
	 * @see #constraint(String)
	 * @see #hasConstraint(String)
	 */
	public boolean isPrimaryKey() {
		return hasConstraint(CONSTRAINT_PRIMARY_KEY);
	}

	/**
	 * Return a boolean flag indicating whether this column represents a unique key or not.
	 *
	 * @return {@code True} if this column represents unique key so {@link #CONSTRAINT_UNIQUE} is
	 * presented, {@code false} otherwise.
	 *
	 * @see #constraint(String)
	 * @see #hasConstraint(String)
	 */
	public boolean isUniqueKey() {
		return hasConstraint(CONSTRAINT_UNIQUE);
	}

	/**
	 * Specifies a reference value for this column if this column should represent a <b>foreign key</b>.
	 *
	 * @param reference Column reference statement containing referenced column name and table name
	 *                  where is such column presented like: 'categories(_id)'.
	 * @return This column to allow methods chaining.
	 *
	 * @see #reference()
	 * @see #isForeignKey()
	 */
	public TableColumn reference(@NonNull final String reference) {
		if (!TextUtils.isEmpty(reference)) this.reference = reference;
		return this;
	}

	/**
	 * Returns the current reference for this column.
	 *
	 * @return Reference supplied to {@link #reference(String)} or empty string by default.
	 *
	 * @see #reference(String)
	 * @see #isForeignKey()
	 */
	@NonNull public String reference() {
		return reference == null ? "" : reference;
	}

	/**
	 * Returns a boolean flag indicating whether this column represents a foreign key or not.
	 *
	 * @return {@code True} if this column represents foreign key for which reference will be added
	 * into column definition statement build via {@link #build()}, {@code false} otherwise.
	 *
	 * @see #reference(String)
	 * @see #reference()
	 */
	public boolean isForeignKey() {
		return !TextUtils.isEmpty(reference);
	}

	/**
	 * Same as {@link #constraint(String)}, but this method is meant to be called with some of constraints
	 * specified within this class like {@link #CONSTRAINT_CHECK} which takes some arguments.
	 * <p>
	 * For {@link #CONSTRAINT_DEFAULT}, {@link #CONSTRAINT_DEFAULT_INT} use {@link #defaultValue(Object)}
	 * instead.
	 *
	 * @param constraint The desired constraint format.
	 * @param value      Value to be used to format the specified constraint.
	 * @return This column to allow methods chaining.
	 *
	 * @see #constraints()
	 * @see #hasConstraint(String)
	 */
	public TableColumn constraint(@FormattedConstraint @NonNull final String constraint, @NonNull final Object value) {
		this.ensureConstraints();
		this.constraints.add(String.format(constraint, value));
		return this;
	}

	/**
	 * Appends the specified <var>constraint</var> into the current constraints of this column.
	 *
	 * @param constraint The desired constraint to add. See constraints specified within this class.
	 * @return This column to allow methods chaining.
	 *
	 * @see #constraint(String, Object)
	 * @see #constraints()
	 * @see #hasConstraint(String)
	 */
	public TableColumn constraint(@Constraint @NonNull final String constraint) {
		this.ensureConstraints();
		this.constraints.add(constraint);
		return this;
	}

	/**
	 * Appends the specified <var>constraints</var> into the current constraints of this column.
	 *
	 * @param constraints The desired constraints to add. See constraints specified within this class.
	 * @return This column to allow methods chaining.
	 *
	 * @see #constraint(String)
	 * @see #constraint(String, Object)
	 * @see #constraints()
	 * @see #hasConstraint(String)
	 */
	public TableColumn constraints(@NonNull @Size(min = 1) final String... constraints) {
		this.ensureConstraints();
		if (constraints.length > 0) {
			this.constraints.addAll(Arrays.asList(constraints));
		}
		return this;
	}

	/**
	 * Ensures that the current list of constraints is initialized.
	 */
	private void ensureConstraints() {
		if (constraints == null) constraints = new ArrayList<>();
	}

	/**
	 * Checks whether the specified <var>constraint</var> has been specified for this column or not.
	 * <p>
	 * <b>Note</b>, that if you need to check for constraint specified via {@link #constraint(String, Object)},
	 * this should be already constraint with the formatted value applied.
	 *
	 * @param constraint The desired constraint to check.
	 * @return {@code True} if constraint is presented, {@code false} otherwise.
	 *
	 * @see #constraint(String)
	 * @see #constraints()
	 */
	public boolean hasConstraint(@NonNull final String constraint) {
		return constraints != null && constraints.contains(constraint);
	}

	/**
	 * Removes the specified <var>constraint</var> from the current constraints of this column.
	 * <p>
	 * <b>Note</b>, that if you need to remove constraint specified via {@link #constraint(String, Object)},
	 * this should be already constraint with the formatted value applied.
	 *
	 * @param constraint The desired constraint to remove. See constraints specified within this class.
	 * @return This column to allow methods chaining.
	 *
	 * @see #constraint(String)
	 * @see #hasConstraint(String)
	 * @see #constraints()
	 */
	public TableColumn removeConstraint(@NonNull final String constraint) {
		if (constraints != null) {
			this.constraints.remove(constraint);
			if (constraints.isEmpty()) {
				this.constraints = null;
			}
		}
		return this;
	}

	/**
	 * Clears all constraint specified for this column.
	 *
	 * @return This column to allow methods chaining.
	 *
	 * @see #constraints()
	 */
	public TableColumn clearConstraints() {
		if (constraints != null) {
			this.constraints.clear();
			this.constraints = null;
		}
		return this;
	}

	/**
	 * Returns the current list of constraints of this column.
	 *
	 * @return This column constraints or {@link Collections#EMPTY_LIST} if no constraints were
	 * specified for this column.
	 * @see #constraint(String)
	 * @see #constraint(String, Object)
	 * @see #constraints(String[])
	 * @see #clearConstraints()
	 * @see #hasConstraint(String)
	 */
	@SuppressWarnings("unchecked")
	@NonNull public List<String> constraints() {
		return constraints == null ? Collections.EMPTY_LIST : new ArrayList<>(constraints);
	}

	/**
	 * Builds this column into formatted valid SQLite column definition statement.
	 * <p>
	 * For example, if this column was created as column for date, this method can return statement
	 * like:
	 * <p>
	 * <b>inception_date TEXT DEFAULT '0000-00-00 00:00' NOT NULL</b>.
	 * <p>
	 * <b>Note</b> that if there is also specified reference for this column via {@link #reference(String)}
	 * the created statement will also contain ' REFERENCES SPECIFIED_REFERENCE' part like:
	 * <p>
	 * <b>category_id INTEGER NOT NULL REFERENCES categories(_id)</b>
	 *
	 * @return This column's definition statement.
	 */
	@SuppressLint("DefaultLocale")
	@NonNull public String build() {
		final StringBuilder builder = new StringBuilder(name.length());
		builder.append(name);
		builder.append(" ");
		builder.append(type.build());
		// Add constraints if specified.
		if (constraints != null) {
			for (final String constraint : constraints) {
				builder.append(" ");
				builder.append(constraint);
			}
		}
		// Add default value if specified.
		final String defaultValue = this.defaultValue == null ? "" : this.defaultValue.toString();
		if (!TextUtils.isEmpty(defaultValue)) {
			builder.append(" ");
			if (TextUtils.isDigitsOnly(defaultValue)) {
				builder.append(String.format(CONSTRAINT_DEFAULT_INT, Integer.parseInt(defaultValue)));
			} else {
				builder.append(String.format(CONSTRAINT_DEFAULT, defaultValue));
			}
		}
		// Add reference if specified.
		if (!TextUtils.isEmpty(reference)) {
			builder.append(" ");
			builder.append(String.format(REFERENCE_FORMAT, reference));
		}
		return builder.toString();
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Creator class that can be used to create an instance of {@link TableColumn} from an annotated
	 * column field.
	 * <p>
	 * Whether this creator can create a valid instance of TableColumn from the desired field can
	 * be determined via {@link #canCreateFromField(Field)}. If this
	 * method returns {@code true} the desired column can be then created via
	 * {@link #createFromField(Field)}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class ColumnCreator {

		/**
		 * Checks whether an instance of {@link TableColumn} can be created from the given
		 * <var>field</var> or not.
		 *
		 * @param field Field to be checked.
		 * @return {@code True} if this creator can create TableColumn from the specified field via
		 * {@link #createFromField(Field)}, {@code false} otherwise.
		 */
		public boolean canCreateFromField(@NonNull final Field field) {
			return field.isAnnotationPresent(Column.class);
		}

		/**
		 * Creates a new instance of TableColumn from the given <var>columnField</var> with configuration
		 * processed from column relevant annotations attached to that field.
		 *
		 * @param columnField The desired field from which should be column created.
		 * @return Always valid instance of TableColumn or exception is thrown.
		 * @throws DatabaseException If the specified field cannot be used to create TableColumn due
		 *                           to its type or that it does not present {@link Column @Column} annotation.
		 *
		 * @see #canCreateFromField(Field)
		 */
		@NonNull public TableColumn createFromField(@NonNull Field columnField) {
			final Column column = columnField.getAnnotation(Column.class);
			final ColumnType type = column == null ? null : resolveColumnTypeForField(column.type(), columnField);
			if (column == null || type == null) {
				final String fieldName = columnField.getName();
				final String fieldType = columnField.getType().getSimpleName();
				throw DatabaseException.misconfiguration(
						"Cannot create TableColumn from field(" + fieldName + ") type of(" + fieldType + "). " +
								"Field is of unsupported type or does not present required @Column annotation.");
			}
			final Builder builder = new Builder(resolveColumnNameForField(columnField.getName(), column.value()), type);
			builder.typeConstraint(TextUtils.isEmpty(column.typeConstraint()) ? null : column.typeConstraint());
			builder.defaultValue(TextUtils.isEmpty(column.defaultValue()) ? null : column.defaultValue());
			builder.constraints(column.constraints());
			final Column.Primary primaryColumn = columnField.getAnnotation(Column.Primary.class);
			if (primaryColumn != null) {
				builder.constraint(CONSTRAINT_PRIMARY_KEY);
				if (primaryColumn.autoincrement()) {
					builder.constraint(CONSTRAINT_AUTOINCREMENT);
				}
			}
			final Column.Unique uniqueColumn = columnField.getAnnotation(Column.Unique.class);
			if (uniqueColumn != null) {
				builder.constraint(CONSTRAINT_UNIQUE);
			}
			final Column.Foreign foreignColumn = columnField.getAnnotation(Column.Foreign.class);
			if (foreignColumn != null) {
				builder.reference(foreignColumn.value());
			}
			return builder.build();
		}

		/**
		 * Resolves column type represented by the given <var>type</var> flag.
		 * <p>
		 * If there are used custom column types within your application, this method should be
		 * overridden to provide the desired custom types to ensure proper column creation.
		 *
		 * @param type  Type flag obtained from {@link Column#type()}.
		 * @param field Annotated column field that can be used to resolve column type automatically
		 *              in case when the specified <var>type</var> is {@link ColumnType#AUTO}.
		 * @return ColumnType instance from the default types or {@code null} if the given
		 * type flag is not supported.
		 */
		@SuppressWarnings("ResourceType")
		@Nullable protected ColumnType resolveColumnTypeForField(final int type, @NonNull final Field field) {
			int columnType = type;
			if (columnType == ColumnType.AUTO) {
				switch (TypeUtils.resolveType(field.getType())) {
					case TypeUtils.BYTE:
					case TypeUtils.CHAR:
					case TypeUtils.SHORT:
					case TypeUtils.INTEGER:
					case TypeUtils.LONG:
					case TypeUtils.BOOLEAN:
						columnType = ColumnType.INTEGER;
						break;
					case TypeUtils.FLOAT:
					case TypeUtils.DOUBLE:
						columnType = ColumnType.REAL;
						break;
					case TypeUtils.STRING:
					case TypeUtils.ENUM:
						columnType = ColumnType.TEXT;
						break;
					default:
						return null;
				}
			}
			return ColumnType.type(columnType);
		}
	}

	/**
	 * A Builder that is used by {@link ColumnCreator} to build an instance of {@link TableColumn}
	 * from an annotated field.
	 */
	static final class Builder {

		/**
		 * See {@link TableColumn#name}.
		 */
		final String name;

		/**
		 * See {@link TableColumn#type}.
		 */
		final ColumnType type;

		/**
		 * See {@link TableColumn#defaultValue}.
		 */
		Object defaultValue;

		/**
		 * See {@link #TableColumn#reference}.
		 */
		String reference;

		/**
		 * See {@link TableColumn#constraints}.
		 */
		List<String> constraints;

		/**
		 * Creates a new instance of Builder that can be used to build an instance of {@link TableColumn}.
		 *
		 * @param name Name for the new TableColumn to build.
		 * @param type Type of the new TableColumn to build.
		 */
		Builder(final String name, final ColumnType type) {
			this.name = name;
			this.type = type;
		}

		/**
		 * Specifies a constraint for the type of the new TableColumn.
		 *
		 * @param typeConstraint The desired constraint for the column type.
		 * @return This builder to allow methods chaining.
		 */
		Builder typeConstraint(final Object typeConstraint) {
			this.type.constraint(typeConstraint);
			return this;
		}

		/**
		 * Specifies a default value for the new TableColumn.
		 *
		 * @param defaultValue The desired default value.
		 * @return This builder to allow methods chaining.
		 */
		Builder defaultValue(final Object defaultValue) {
			this.defaultValue = defaultValue;
			return this;
		}

		/**
		 * Specifies a reference value for the new TableColumn if that column should represent
		 * a foreign key.
		 *
		 * @param reference The desired reference.
		 * @return This builder to allow methods chaining.
		 */
		Builder reference(final String reference) {
			this.reference = reference;
			return this;
		}

		/**
		 * Specifies a constraint for the new TableColumn.
		 *
		 * @param constraint The desired constraint to append to the current ones.
		 * @return This builder to allow methods chaining.
		 */
		Builder constraint(final String constraint) {
			ensureConstraints(1);
			constraints.add(constraint);
			return this;
		}

		/**
		 * Specifies a set of constraints for the new TableColumn.
		 *
		 * @param constraints The desired set of constraints to append to the current ones.
		 * @return This builder to allow methods chaining.
		 */
		Builder constraints(final String[] constraints) {
			if (constraints.length > 0) {
				ensureConstraints(constraints.length);
				this.constraints.addAll(Arrays.asList(constraints));
			}
			return this;
		}

		/**
		 * Ensures that the constraints array is initialized.
		 *
		 * @param initialSize Initial size for the constraints array.
		 */
		private void ensureConstraints(final int initialSize) {
			if (constraints == null) constraints = new ArrayList<>(initialSize);
		}

		/**
		 * Builds a new instance of TableColumn.
		 *
		 * @return TableColumn instance with the current data of this builder.
		 */
		TableColumn build() {
			return new TableColumn(this);
		}
	}
}
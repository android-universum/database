/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import universum.studios.android.database.util.ColumnType;
import universum.studios.android.database.util.TableColumn;

/**
 * Defines an annotation for marking a field that should represent a column within a database table.
 *
 * @author Martin Albedinsky
 * @see Primary
 * @see Unique
 * @see Foreign
 * @see Joined
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

	/**
	 * Name that will be used to name a table column and also as key for value inserted into ContentValues
	 * or as column name to obtain its value from a Cursor.
	 * <p>
	 * If empty, name of this field will be used.
	 * <p>
	 * Default value: <b>""</b>
	 */
	String value() default "";

	/**
	 * Type determining what data can be stored within a database table under this column.
	 * <p>
	 * Default value: <b>{@link ColumnType#AUTO AUTO}</b>
	 */
	@ColumnType.Type int type() default ColumnType.AUTO;

	/**
	 * Constrain for the type of this column.
	 */
	String typeConstraint() default "";

	/**
	 * Default value for this column.
	 */
	String defaultValue() default "";

	/**
	 * Set of constraints for this column.
	 */
	String[] constraints() default {};

	/**
	 * Flag indicating whether a {@code null} value of this column can be inserted into ContentValues
	 * object and thus also into database or not.
	 * <p>
	 * Default value: <b>false</b>
	 */
	boolean nullable() default false;

	/**
	 * Defines an annotation for marking a field that should represent a <b>primary column</b> within
	 * a database table. This annotation requests auto addition of {@link TableColumn#CONSTRAINT_PRIMARY_KEY},
	 * with also {@link TableColumn#CONSTRAINT_AUTOINCREMENT} if {@link Primary#autoincrement()}
	 * is {@code true}, into this column's constraints when processed, so this column's SQLite definition
	 * statement will look for example like:
	 * <p>
	 * <i>mac_address TEXT NOT NULL PRIMARY KEY AUTOINCREMENT</i>
	 * <p>
	 * <b>Note, that if there are marked multiple columns with this annotation the PRIMARY KEY with
	 * AUTOINCREMENT constraint will be removed from such columns and names of these columns will
	 * be added as group at the end of CREATE TABLE statement into PRIMARY KEY(COLUMN, COLUMN, ...)
	 * group.</b>
	 *
	 * @author Martin Albedinsky
	 * @see Column#constraints()
	 * @see Unique
	 * @see Foreign
	 * @see Joined
	 */
	@Target({ElementType.FIELD, ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@interface Primary {

		/**
		 * Default name for the primary column holding an id within database table.
		 * <p>
		 * Constant value: <b>_id</b>
		 */
		String COLUMN_NAME = "_id";

		/**
		 * Flag indicating whether this primary column should be auto-incremented or not.
		 * <p>
		 * <b>Note, that AUTOINCREMENT keyword imposes extra CPU, memory, disk space, and disk I/O
		 * overhead. For more info see <a href="http://www.sqlite.org/autoinc.html">http://www.sqlite.org/autoinc.html</a></b>
		 * <p>
		 * Default value: <b>false</b>
		 */
		boolean autoincrement() default false;
	}

	/**
	 * Defines an annotation for marking a field that should represent a <b>unique column</b> within
	 * a database table. This annotation requests auto addition of {@link TableColumn#CONSTRAINT_UNIQUE}
	 * into this column's constraints when processed, so this column's SQLite definition statement
	 * will look for example like:
	 * <p>
	 * <i>author TEXT NOT NULL UNIQUE</i>
	 * <p>
	 * <b>Note, that if there are marked multiple columns with this annotation the UNIQUE constraint
	 * will be removed from such columns and names of these columns will be added as group at the
	 * end of CREATE TABLE statement into UNIQUE(COLUMN, COLUMN, ...) group followed by ON CONFLICT REPLACE.</b>
	 *
	 * @author Martin Albedinsky
	 * @see Column#constraints()
	 * @see Primary
	 * @see Foreign
	 * @see Joined
	 */
	@Target({ElementType.FIELD, ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@interface Unique {
	}

	/**
	 * Defines an annotation for marking a field that should represent a <b>foreign column</b> within
	 * a database table. This annotation requests building of this column's definition statement with
	 * additional REFERENCES followed by reference value specified via {@link Foreign#value()}, so
	 * this column's SQLite definition statement will look for example like:
	 * <p>
	 * <i>category_id INTEGER REFERENCES categories(_id)</i>
	 *
	 * @author Martin Albedinsky
	 * @see Primary
	 * @see Unique
	 * @see Joined
	 */
	@Target({ElementType.FIELD, ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@interface Foreign {

		/**
		 * Value of reference for this foreign column. Like: <b>categories(_id)</b>.
		 */
		String value();
	}

	/**
	 * Defines an annotation for marking a column field that should be used to bind data from a cursor
	 * that has been obtained from multiple joined tables.
	 * <p>
	 * Values of fields marked via this annotation are used only for projection purpose and thus are
	 * not stored within database.
	 *
	 * @author Martin Albedinsky
	 */
	@Target({ElementType.FIELD, ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@interface Joined {
	}
}
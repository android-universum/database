/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.annotation.NonNull;

/**
 * Simple utility class which provides executors that are used by the Database library to execute
 * database content related operations on a background thread.
 *
 * @author Martin Albedinsky
 */
public final class DatabaseExecutors {

	/**
	 * Factory for threads used to execute database operations asynchronously.
	 */
	private static final ThreadFactory THREAD_FACTORY = new ThreadFactory() {

		/**
		 * Determines current count of threads created by this factory.
		 */
		private final AtomicInteger count = new AtomicInteger(1);

		/**
		 */
		@Override
		public Thread newThread(@NonNull Runnable runnable) {
			return new Thread(runnable, "DatabaseThread.BACKGROUND#" + count.getAndIncrement());
		}
	};

	/**
	 * Executor used for execution of database operations on a single background thread.
	 */
	private static final Executor BACKGROUND = Executors.newSingleThreadExecutor(THREAD_FACTORY);

	/**
	 */
	private DatabaseExecutors() {
		// Creation of instances of this class is not publicly allowed.
	}

	/**
	 * Returns the executor that should be used for execution of database content related operations
	 * on a background thread.
	 * <p>
	 * The returned executor is a <b>single-threaded</b> executor with queue for database tasks with
	 * capacity of {@link Integer#MAX_VALUE}.
	 *
	 * @return The database executor for background operations.
	 */
	@NonNull
	public static Executor background() {
		return BACKGROUND;
	}
}
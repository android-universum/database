/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.util;

import org.hamcrest.MatcherAssert;
import org.junit.Test;

import java.util.Date;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class TypeUtilsTest extends TestCase {

	@Test public void testResolveType() {
		// Act + Assert:
		// - unknown:
		MatcherAssert.assertThat(TypeUtils.resolveType(null), is(TypeUtils.UNKNOWN));
		assertThat(TypeUtils.resolveType(Date.class), is(TypeUtils.UNKNOWN));
		// - byte:
		assertThat(TypeUtils.resolveType(byte.class), is(TypeUtils.BYTE));
		assertThat(TypeUtils.resolveType(Byte.class), is(TypeUtils.BYTE));
		assertThat(TypeUtils.resolveType(byte[].class), is(TypeUtils.BYTE_ARRAY));
		assertThat(TypeUtils.resolveType(Byte[].class), is(TypeUtils.BYTE_ARRAY));
		// - char:
		assertThat(TypeUtils.resolveType(char.class), is(TypeUtils.CHAR));
		assertThat(TypeUtils.resolveType(Character.class), is(TypeUtils.CHAR));
		assertThat(TypeUtils.resolveType(char[].class), is(TypeUtils.CHAR_ARRAY));
		assertThat(TypeUtils.resolveType(Character[].class), is(TypeUtils.CHAR_ARRAY));
		// - short:
		assertThat(TypeUtils.resolveType(short.class), is(TypeUtils.SHORT));
		assertThat(TypeUtils.resolveType(Short.class), is(TypeUtils.SHORT));
		assertThat(TypeUtils.resolveType(short[].class), is(TypeUtils.SHORT_ARRAY));
		assertThat(TypeUtils.resolveType(Short[].class), is(TypeUtils.SHORT_ARRAY));
		// - boolean:
		assertThat(TypeUtils.resolveType(boolean.class), is(TypeUtils.BOOLEAN));
		assertThat(TypeUtils.resolveType(Boolean.class), is(TypeUtils.BOOLEAN));
		assertThat(TypeUtils.resolveType(boolean[].class), is(TypeUtils.BOOLEAN_ARRAY));
		assertThat(TypeUtils.resolveType(Boolean[].class), is(TypeUtils.BOOLEAN_ARRAY));
		// - int:
		assertThat(TypeUtils.resolveType(int.class), is(TypeUtils.INTEGER));
		assertThat(TypeUtils.resolveType(Integer.class), is(TypeUtils.INTEGER));
		assertThat(TypeUtils.resolveType(int[].class), is(TypeUtils.INTEGER_ARRAY));
		assertThat(TypeUtils.resolveType(Integer[].class), is(TypeUtils.INTEGER_ARRAY));
		// - long:
		assertThat(TypeUtils.resolveType(long.class), is(TypeUtils.LONG));
		assertThat(TypeUtils.resolveType(Long.class), is(TypeUtils.LONG));
		assertThat(TypeUtils.resolveType(long[].class), is(TypeUtils.LONG_ARRAY));
		assertThat(TypeUtils.resolveType(Long[].class), is(TypeUtils.LONG_ARRAY));
		// - float:
		assertThat(TypeUtils.resolveType(float.class), is(TypeUtils.FLOAT));
		assertThat(TypeUtils.resolveType(Float.class), is(TypeUtils.FLOAT));
		assertThat(TypeUtils.resolveType(float[].class), is(TypeUtils.FLOAT_ARRAY));
		assertThat(TypeUtils.resolveType(Float[].class), is(TypeUtils.FLOAT_ARRAY));
		// - double:
		assertThat(TypeUtils.resolveType(double.class), is(TypeUtils.DOUBLE));
		assertThat(TypeUtils.resolveType(Double.class), is(TypeUtils.DOUBLE));
		assertThat(TypeUtils.resolveType(double[].class), is(TypeUtils.DOUBLE_ARRAY));
		assertThat(TypeUtils.resolveType(Double[].class), is(TypeUtils.DOUBLE_ARRAY));
		// - string:
		assertThat(TypeUtils.resolveType(String.class), is(TypeUtils.STRING));
		assertThat(TypeUtils.resolveType(String[].class), is(TypeUtils.STRING_ARRAY));
		// - enum:
		assertThat(TypeUtils.resolveType(Enum.class), is(TypeUtils.ENUM));
		assertThat(TypeUtils.resolveType(Enum[].class), is(TypeUtils.ENUM_ARRAY));
	}

	@Test public void testResolvePrimitiveType() {
		// - unknown:
		assertThat(TypeUtils.resolvePrimitiveType(null), is(TypeUtils.UNKNOWN));
		assertThat(TypeUtils.resolvePrimitiveType(Date[].class), is(TypeUtils.UNKNOWN));
		// - byte:
		assertThat(TypeUtils.resolvePrimitiveType(byte.class), is(TypeUtils.BYTE));
		assertThat(TypeUtils.resolvePrimitiveType(byte[].class), is(TypeUtils.BYTE_ARRAY));
		// - char:
		assertThat(TypeUtils.resolvePrimitiveType(char.class), is(TypeUtils.CHAR));
		assertThat(TypeUtils.resolvePrimitiveType(char[].class), is(TypeUtils.CHAR_ARRAY));
		// - short:
		assertThat(TypeUtils.resolvePrimitiveType(short.class), is(TypeUtils.SHORT));
		assertThat(TypeUtils.resolvePrimitiveType(short[].class), is(TypeUtils.SHORT_ARRAY));
		// - boolean:
		assertThat(TypeUtils.resolvePrimitiveType(boolean.class), is(TypeUtils.BOOLEAN));
		assertThat(TypeUtils.resolvePrimitiveType(boolean[].class), is(TypeUtils.BOOLEAN_ARRAY));
		// - int:
		assertThat(TypeUtils.resolvePrimitiveType(int.class), is(TypeUtils.INTEGER));
		assertThat(TypeUtils.resolvePrimitiveType(int[].class), is(TypeUtils.INTEGER_ARRAY));
		// - long:
		assertThat(TypeUtils.resolvePrimitiveType(long.class), is(TypeUtils.LONG));
		assertThat(TypeUtils.resolvePrimitiveType(long[].class), is(TypeUtils.LONG_ARRAY));
		// - float:
		assertThat(TypeUtils.resolvePrimitiveType(float.class), is(TypeUtils.FLOAT));
		assertThat(TypeUtils.resolvePrimitiveType(float[].class), is(TypeUtils.FLOAT_ARRAY));
		// - double:
		assertThat(TypeUtils.resolvePrimitiveType(double.class), is(TypeUtils.DOUBLE));
		assertThat(TypeUtils.resolvePrimitiveType(double[].class), is(TypeUtils.DOUBLE_ARRAY));
		// - string:
		assertThat(TypeUtils.resolvePrimitiveType(String.class), is(TypeUtils.STRING));
		assertThat(TypeUtils.resolvePrimitiveType(String[].class), is(TypeUtils.STRING_ARRAY));
	}
}
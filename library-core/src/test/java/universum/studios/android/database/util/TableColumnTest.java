/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.util;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("ResourceType")
public final class TableColumnTest extends AndroidTestCase {

	@Test public void testInstantiation() {
		// Act:
		final TableColumn column = new TableColumn("date", ColumnType.DATE);
		// Assert:
		assertThat(column.name(), is("date"));
		assertThat(column.type(), is(ColumnType.type(ColumnType.DATE)));
		assertThat(column.reference(), is(""));
	}

	@Test public void testInstantiationWithUnsupportedType() {
		try {
			// Act:
			new TableColumn("date", 1237);
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(e.getMessage(), is("Requested unsupported column type(1237)."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test public void testInstantiationWithColumnType() {
		// Act:
		final TableColumn column = new TableColumn("date", new ColumnType("DATE"));
		// Assert:
		assertThat(column.name(), is("date"));
		assertThat(column.type(), is(new ColumnType("DATE")));
	}

	@Test public void testInstantiationViaBuilder() {
		// Arrange:
		final TableColumn.Builder builder = new TableColumn.Builder("mac_address", new ColumnType("TEXT"));
		builder.defaultValue("00:00:00:00:00:00");
		builder.typeConstraint(256);
		builder.constraint("NOT NULL");
		builder.constraints(new String[]{
				TableColumn.CONSTRAINT_PRIMARY_KEY,
				TableColumn.CONSTRAINT_AUTOINCREMENT
		});
		builder.reference("mac_addresses(value)");
		// Act:
		final TableColumn column = builder.build();
		// Assert:
		assertThat(column, is(not(nullValue())));
		assertThat(column.name(), is("mac_address"));
		assertThat(column.type(), is(new ColumnType("TEXT", 256)));
		assertThat(column.defaultValue(), is("00:00:00:00:00:00"));
		assertThat(column.constraints().size(), is(3));
		assertThat(column.hasConstraint("NOT NULL"), is(true));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_PRIMARY_KEY), is(true));
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_AUTOINCREMENT), is(true));
		assertThat(column.isPrimaryKey(), is(true));
		assertThat(column.isUniqueKey(), is(false));
		assertThat(column.isForeignKey(), is(true));
		assertThat(column.reference(), is("mac_addresses(value)"));
	}

	@Test public void testResolveColumnNameForField() {
		// Act + Assert:
		assertThat(TableColumn.resolveColumnNameForField("date", ""), is("date"));
		assertThat(TableColumn.resolveColumnNameForField("titleField", "title"), is("title"));
	}

	@Test public void testCreateReference() {
		// Act + Assert:
		assertThat(TableColumn.createReference("categories", "_id"), is("categories(_id)"));
	}

	@Test public void testDefaultValue() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.defaultValue("none");
		assertThat(column.defaultValue(), is("none"));
	}

	@Test public void testIsPrimaryKey() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		assertThat(column.isPrimaryKey(), is(false));
		column.constraint(TableColumn.CONSTRAINT_PRIMARY_KEY);
		assertThat(column.isPrimaryKey(), is(true));
	}

	@Test public void testIsUniqueKey() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		assertThat(column.isUniqueKey(), is(false));
		column.constraint(TableColumn.CONSTRAINT_UNIQUE);
		assertThat(column.isUniqueKey(), is(true));
	}

	@Test public void testReference() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		assertThat(column.isForeignKey(), is(false));
		column.reference("categories(_id)");
		assertThat(column.isForeignKey(), is(true));
		assertThat(column.reference(), is("categories(_id)"));
	}

	@SuppressWarnings("ConstantConditions")
	@Test public void testReferenceWithInvalidValue() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.reference(null);
		assertThat(column.reference(), is(""));
	}

	@Test public void testConstraint() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		assertThat(column.constraints(), is(Collections.EMPTY_LIST));
		column.constraint(TableColumn.CONSTRAINT_NOT_NULL);
		column.constraint(TableColumn.CONSTRAINT_UNIQUE);
	}

	@Test public void testConstraintValue() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraint(TableColumn.CONSTRAINT_DEFAULT, "none");
		final List<String> constraints = column.constraints();
		assertThat(constraints.size() == 1, is(true));
		assertThat(constraints.get(0), is("DEFAULT 'none'"));
	}

	@Test public void testConstraints() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraints(
				TableColumn.CONSTRAINT_PRIMARY_KEY,
				TableColumn.CONSTRAINT_AUTOINCREMENT,
				TableColumn.CONSTRAINT_NOT_NULL
		);
		final List<String> constraints = column.constraints();
		assertThat(constraints.size() == 3, is(true));
		assertThat(constraints.get(0), is(TableColumn.CONSTRAINT_PRIMARY_KEY));
		assertThat(constraints.get(1), is(TableColumn.CONSTRAINT_AUTOINCREMENT));
		assertThat(constraints.get(2), is(TableColumn.CONSTRAINT_NOT_NULL));
	}

	@Test public void testConstraintsWithEmptyArray() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraints(new String[0]);
		assertThat(column.constraints(), is(Collections.EMPTY_LIST));
	}

	@Test public void testHasConstraint() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraint(TableColumn.CONSTRAINT_PRIMARY_KEY);
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_PRIMARY_KEY), is(true));
	}

	@Test public void testHasFormattedConstraint() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraint(TableColumn.CONSTRAINT_DEFAULT_INT, 10);
		assertThat(column.hasConstraint("DEFAULT 10"), is(true));
	}

	@Test public void testRemoveConstraint() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraint(TableColumn.CONSTRAINT_UNIQUE);
		column.constraint(TableColumn.CONSTRAINT_NOT_NULL);
		column.removeConstraint(TableColumn.CONSTRAINT_UNIQUE);
		assertThat(column.hasConstraint(TableColumn.CONSTRAINT_UNIQUE), is(false));
	}

	@Test public void testRemoveConstraintWithoutAnyConstraintsSpecified() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.removeConstraint(TableColumn.CONSTRAINT_NOT_NULL);
	}

	@Test public void testRemoveConstraintWithLastConstraintToBeRemoved() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraint(TableColumn.CONSTRAINT_NOT_NULL);
		column.removeConstraint(TableColumn.CONSTRAINT_NOT_NULL);
	}

	@Test public void testClearConstraints() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.constraint(TableColumn.CONSTRAINT_UNIQUE);
		column.clearConstraints();
		assertThat(column.constraints(), is(Collections.EMPTY_LIST));
	}

	@Test public void testClearConstraintsWithoutAnyConstraintsSpecified() {
		// Arrange:
		final TableColumn column = new TableColumn("description", new ColumnType("TEXT"));
		// Act + Assert:
		column.clearConstraints();
	}

	@Test public void testBuildWithoutConstraints() {
		final TableColumn column = new TableColumn("author", ColumnType.type(ColumnType.TEXT));
		assertThat(column.build(), is("author TEXT"));
	}

	@Test public void testBuildWithSingleConstraint() {
		final TableColumn column = new TableColumn("author", ColumnType.type(ColumnType.TEXT));
		column.constraint(TableColumn.CONSTRAINT_NOT_NULL);
		assertThat(column.build(), is("author TEXT NOT NULL"));
	}

	@Test public void testBuildWithMultipleConstraints() {
		final TableColumn column = new TableColumn("author", ColumnType.type(ColumnType.TEXT));
		column.constraint(TableColumn.CONSTRAINT_NOT_NULL);
		column.defaultValue("none");
		assertThat(column.build(), is("author TEXT NOT NULL DEFAULT 'none'"));
	}

	@Test public void testBuildWithReference() {
		final TableColumn column = new TableColumn("category_id", ColumnType.type(ColumnType.INTEGER));
		column.reference("categories(_id)");
		assertThat(column.build(), is("category_id INTEGER REFERENCES categories(_id)"));
	}

	@Test public void testBuildWithMultipleConstraintsAndReference() {
		final TableColumn column = new TableColumn("category_id", ColumnType.type(ColumnType.INTEGER));
		column.constraints(TableColumn.CONSTRAINT_NOT_NULL, TableColumn.CONSTRAINT_UNIQUE);
		column.reference("categories(_id)");
		assertThat(column.build(), is("category_id INTEGER NOT NULL UNIQUE REFERENCES categories(_id)"));
	}

	@Test public void testBuildWithDefaultValueAsDigit() {
		final TableColumn column = new TableColumn("enabled", ColumnType.type(ColumnType.INTEGER));
		column.defaultValue("1");
		assertThat(column.build(), is("enabled INTEGER DEFAULT 1"));
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.util;

import org.junit.Ignore;
import org.junit.Test;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("ResourceType")
public final class ColumnTypeTest extends TestCase {

	@Test public void testInstantiation() {
		// Act:
		final ColumnType type = new ColumnType("INTEGER");
		// Assert:
		assertThat(type.name(), is("INTEGER"));
	}

	@Test public void testInstantiationWithConstraint() {
		// Act:
		final ColumnType columnType = new ColumnType("VARCHAR", 256);
		// Assert:
		assertThat(columnType.name(), is("VARCHAR"));
		assertThat(columnType.constraint(), is(256));
	}

	@Test public void testType() {
		// Assert:
		assertTypeWithName(ColumnType.BLOB, "BLOB");
		// - integer:
		assertTypeWithName(ColumnType.INT, "INT");
		assertTypeWithName(ColumnType.INTEGER, "INTEGER");
		assertTypeWithName(ColumnType.TINYINT, "TINYINT");
		assertTypeWithName(ColumnType.SMALLINT, "SMALLINT");
		assertTypeWithName(ColumnType.MEDIUMINT, "MEDIUMINT");
		assertTypeWithName(ColumnType.BIGINT, "BIGINT");
		assertTypeWithName(ColumnType.UNSIGNED_BIG_INT, "UNSIGNED_BIG_INT");
		assertTypeWithName(ColumnType.INT2, "INT2");
		assertTypeWithName(ColumnType.INT8, "INT8");
		// - text:
		assertTypeWithName(ColumnType.CHARACTER, "CHARACTER");
		assertTypeWithName(ColumnType.VARCHAR, "VARCHAR");
		assertTypeWithName(ColumnType.VARYING_CHARACTER, "VARYING_CHARACTER");
		assertTypeWithName(ColumnType.NCHAR, "NCHAR");
		assertTypeWithName(ColumnType.NATIVE_CHARACTER, "NATIVE_CHARACTER");
		assertTypeWithName(ColumnType.NVARCHAR, "NVARCHAR");
		assertTypeWithName(ColumnType.TEXT, "TEXT");
		assertTypeWithName(ColumnType.CLOB, "CLOB");
		// - real:
		assertTypeWithName(ColumnType.REAL, "REAL");
		assertTypeWithName(ColumnType.DOUBLE, "DOUBLE");
		assertTypeWithName(ColumnType.DOUBLE_PRECISION, "DOUBLE_PRECISION");
		assertTypeWithName(ColumnType.FLOAT, "FLOAT");
		// - numeric:
		assertTypeWithName(ColumnType.NUMERIC, "NUMERIC");
		assertTypeWithName(ColumnType.DECIMAL, "DECIMAL");
		assertTypeWithName(ColumnType.BOOLEAN, "BOOLEAN");
		assertTypeWithName(ColumnType.DATE, "DATE");
		assertTypeWithName(ColumnType.DATETIME, "DATETIME");
	}

	@Test public void testNameOfType() {
		// Assert:
		assertThat(ColumnType.nameOfType(-5), is(""));
		assertThat(ColumnType.nameOfType(ColumnType.AUTO), is(""));
		assertThat(ColumnType.nameOfType(ColumnType.BLOB), is("BLOB"));
		// - integer:
		assertThat(ColumnType.nameOfType(ColumnType.INT), is("INT"));
		assertThat(ColumnType.nameOfType(ColumnType.INTEGER), is("INTEGER"));
		assertThat(ColumnType.nameOfType(ColumnType.TINYINT), is("TINYINT"));
		assertThat(ColumnType.nameOfType(ColumnType.SMALLINT), is("SMALLINT"));
		assertThat(ColumnType.nameOfType(ColumnType.MEDIUMINT), is("MEDIUMINT"));
		assertThat(ColumnType.nameOfType(ColumnType.BIGINT), is("BIGINT"));
		assertThat(ColumnType.nameOfType(ColumnType.UNSIGNED_BIG_INT), is("UNSIGNED_BIG_INT"));
		assertThat(ColumnType.nameOfType(ColumnType.INT2), is("INT2"));
		assertThat(ColumnType.nameOfType(ColumnType.INT8), is("INT8"));
		// - text:
		assertThat(ColumnType.nameOfType(ColumnType.CHARACTER), is("CHARACTER"));
		assertThat(ColumnType.nameOfType(ColumnType.VARCHAR), is("VARCHAR"));
		assertThat(ColumnType.nameOfType(ColumnType.VARYING_CHARACTER), is("VARYING_CHARACTER"));
		assertThat(ColumnType.nameOfType(ColumnType.NCHAR), is("NCHAR"));
		assertThat(ColumnType.nameOfType(ColumnType.NATIVE_CHARACTER), is("NATIVE_CHARACTER"));
		assertThat(ColumnType.nameOfType(ColumnType.NVARCHAR), is("NVARCHAR"));
		assertThat(ColumnType.nameOfType(ColumnType.TEXT), is("TEXT"));
		assertThat(ColumnType.nameOfType(ColumnType.CLOB), is("CLOB"));
		// - real:
		assertThat(ColumnType.nameOfType(ColumnType.REAL), is("REAL"));
		assertThat(ColumnType.nameOfType(ColumnType.DOUBLE), is("DOUBLE"));
		assertThat(ColumnType.nameOfType(ColumnType.DOUBLE_PRECISION), is("DOUBLE_PRECISION"));
		assertThat(ColumnType.nameOfType(ColumnType.FLOAT), is("FLOAT"));
		// - numeric:
		assertThat(ColumnType.nameOfType(ColumnType.NUMERIC), is("NUMERIC"));
		assertThat(ColumnType.nameOfType(ColumnType.DECIMAL), is("DECIMAL"));
		assertThat(ColumnType.nameOfType(ColumnType.BOOLEAN), is("BOOLEAN"));
		assertThat(ColumnType.nameOfType(ColumnType.DATE), is("DATE"));
		assertThat(ColumnType.nameOfType(ColumnType.DATETIME), is("DATETIME"));
	}

	private static void assertTypeWithName(int type, String name) {
		final ColumnType columnType = ColumnType.type(type);
		assertThat(columnType.name(), is(name));
	}

	@Test public void testTypeAUTO() {
		try {
			// Act:
			ColumnType.type(ColumnType.AUTO);
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(e.getMessage(), is("Cannot create ColumnType for AUTO type."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test public void testUnsupportedType() {
		try {
			// Act:
			ColumnType.type(-3);
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(e.getMessage(), is("Requested unsupported column type(-3)."));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}

	@Test public void testConstraint() {
		// Arrange:
		final ColumnType columnType = new ColumnType("VARCHAR");
		// Act + Assert:
		assertThat(columnType.constraint(), is(nullValue()));
		columnType.constraint(128);
		assertThat(columnType.constraint(), is(128));
	}

	@Test public void testClear() {
		// Arrange:
		final ColumnType columnType = new ColumnType("VARCHAR", 256).clear();
		// Act + Assert:
		assertThat(columnType.constraint(), is(nullValue()));
	}

	@Ignore("Fails on CI server with java.lang.RuntimeException!")
	@Test public void testBuild() {
		// We will not test building of all types here because regardless of the type's name each type
		// builds the same way.
		// Arrange:
		final ColumnType columnType = new ColumnType("TEXT");
		// Act + Assert:
		assertThat(columnType.build(), is("TEXT"));
	}

	@Ignore("Fails on CI server with java.lang.RuntimeException!")
	@Test public void testBuildWithConstraint() {
		// As for testBuild() its enough to test building of a single type not all of them.
		// Arrange:
		final ColumnType columnType = new ColumnType("TEXT", 256);
		// Act + Assert:
		assertThat(columnType.build(), is("TEXT(256)"));
	}

	@Test public void testHashCode() {
		// Arrange:
		final ColumnType type = new ColumnType("title");
		// Act + Assert:
		assertThat(type.hashCode(), is(31 * "title".hashCode()));
	}

	@Test public void testHashCodeWithConstraint() {
		// Arrange:
		final ColumnType type = new ColumnType("title", "128");
		// Act + Assert:
		assertThat(type.hashCode(), is(31 * "title".hashCode() + "128".hashCode()));
	}

	@Test public void testEqualsWithEqualNames() {
		// Arrange:
		final ColumnType first = new ColumnType("title");
		final ColumnType second = new ColumnType("title");
		// Act + Assert:
		assertThat(first.equals(second), is(true));
	}

	@Test public void testEqualsWithEqualNamesAndConstraints() {
		// Arrange:
		final ColumnType first = new ColumnType("title", "56");
		final ColumnType second = new ColumnType("title", "56");
		// Act + Assert:
		assertThat(first.equals(second), is(true));
	}

	@Test public void testEqualsWithDifferentNames() {
		// Arrange:
		final ColumnType first = new ColumnType("title1");
		final ColumnType second = new ColumnType("title2");
		// Act + Assert:
		assertThat(first.equals(second), is(false));
	}

	@Test public void testEqualsWithDifferentConstraints() {
		// Arrange + Act + Assert:
		assertThat(new ColumnType("title").equals(new ColumnType("title", "56")), is(false));
		assertThat(new ColumnType("title", "56").equals(new ColumnType("title")), is(false));
	}

	@Test public void testEqualsWithDifferentNamesAndConstraints() {
		// Arrange:
		final ColumnType first = new ColumnType("title1", "56");
		final ColumnType second = new ColumnType("title2", "128");
		// Act + Assert:
		assertThat(first.equals(second), is(false));
	}

	@SuppressWarnings("EqualsWithItself")
	@Test public void testEqualsWithSameInstances() {
		// Arrange:
		final ColumnType type = new ColumnType("title");
		// Act + Assert:
		assertThat(type.equals(type), is(true));
	}

	@SuppressWarnings("EqualsBetweenInconvertibleTypes")
	@Test public void testEqualsWithDifferentTypes() {
		// Arrange:
		final ColumnType type = new ColumnType("title");
		// Act + Assert:
		assertThat(type.equals("title"), is(false));
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database;

import org.junit.Test;

import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.test.model.TestModel;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class DatabaseExceptionTest extends AndroidTestCase {

	@Test public void testExceptionWithMessage() {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.exception("Unknown database failure."),
				DatabaseException.TYPE_DEFAULT,
				"Unknown database failure."
		);
	}

	@Test public void testExceptionWithMessageAndCause() {
		final Throwable cause = new NullPointerException("");
		assertThatExceptionHasTypeAndMessageAndCause(
				DatabaseException.exception("NullPointer encountered.", cause),
				DatabaseException.TYPE_DEFAULT,
				"NullPointer encountered.",
				cause
		);
	}

	@Test public void testExceptionWithCause() {
		final Exception cause = new NullPointerException("NullPointer encountered.");
		assertThatExceptionHasTypeAndMessageAndCause(
				DatabaseException.exception(cause),
				DatabaseException.TYPE_DEFAULT,
				"java.lang.NullPointerException: NullPointer encountered.",
				cause
		);
	}

	@Test public void testAnnotationsNotEnabled() {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.annotationsNotEnabled(),
				DatabaseException.TYPE_MISCONFIGURATION,
				"Trying to access logic that requires annotations processing to be enabled, " +
						"but it seams that the annotations processing is disabled for the Database library."
		);
	}

	@Test public void testMisconfiguration() {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.misconfiguration("Misconfiguration EntityModel."),
				DatabaseException.TYPE_MISCONFIGURATION,
				"Misconfiguration EntityModel."
		);
	}

	@Test public void testInstantiation() {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.instantiation("MODEL", TestModel.class, null),
				DatabaseException.TYPE_INSTANTIATION,
				"Failed to instantiate instance of MODEL class of(TestModel). " +
						"Make sure that such class has public access and empty (without arguments) public constructor."
		);
	}

	@Test public void testInstantiationWithNote() {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.instantiation("MODEL", TestModel.class, null, ""),
				DatabaseException.TYPE_INSTANTIATION,
				"Failed to instantiate instance of MODEL class of(TestModel)."
		);
	}

	// todo:
	/*@Test public void testMissingClassAnnotation() {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.missingClassAnnotation(PrimaryDatabase.class, TestPrimaryDatabase.class),
				DatabaseException.TYPE_MISSING_CLASS_ANNOTATION,
				"@PrimaryDatabase annotation for class(TestPrimaryDatabase) is missing."
		);
	}*/

	// todo:
	/*@Test public void testMissingClassAnnotationWithNote() {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.missingClassAnnotation(PrimaryDatabase.class, TestPrimaryDatabase.class, "Make sure that this annotation is properly attached."),
				DatabaseException.TYPE_MISSING_CLASS_ANNOTATION,
				"@PrimaryDatabase annotation for class(TestPrimaryDatabase) is missing. " +
						"Make sure that this annotation is properly attached."
		);
	}*/

	@Test public void testMissingFieldAnnotation() throws Exception {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.missingFieldAnnotation(Column.class, TestModel.class.getField("author"), TestModel.class),
				DatabaseException.TYPE_MISSING_FIELD_ANNOTATION,
				"@Column annotation for field(author) of class(TestModel) is missing."
		);
	}

	@Test public void testMissingFieldAnnotationWithNote() throws Exception {
		assertThatExceptionHasTypeAndMessage(
				DatabaseException.missingFieldAnnotation(Column.class, TestModel.class.getField("author"), TestModel.class, "Make sure that this annotation is properly attached."),
				DatabaseException.TYPE_MISSING_FIELD_ANNOTATION,
				"@Column annotation for field(author) of class(TestModel) is missing. " +
						"Make sure that this annotation is properly attached."
		);
	}

	private static void assertThatExceptionHasTypeAndMessageAndCause(final DatabaseException exception, final int type, final String message, final Throwable cause) {
		assertThatExceptionHasTypeAndMessage(exception, type, message);
		assertThat(exception.getCause(), is(cause));
	}

	private static void assertThatExceptionHasTypeAndMessage(final DatabaseException exception, final int type, final String message) {
		assertThat(exception, is(not(nullValue())));
		assertThat(exception.getType(), is(type));
		assertThat(exception.getMessage(), is(message));
	}
}
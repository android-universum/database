Database-Core
===============

This module contains core elements for this library.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [DatabaseExecutors](https://bitbucket.org/android-universum/database/src/main/library-core/src/main/java/universum/studios/android/database/DatabaseExecutors.java)
- [Transaction](https://bitbucket.org/android-universum/database/src/main/library-core/src/main/java/universum/studios/android/database/Transaction.java)
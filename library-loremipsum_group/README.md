@Database-LoremIpsum
===============

This module groups the following modules into one **single group**:

- [LoremIpsum-Core](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-core)
- [LoremIpsum-Model](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-model)
- [LoremIpsum-Entity](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-entity)
- [LoremIpsum-Cursor](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-cursor)
- [LoremIpsum-Adapter](https://bitbucket.org/android-universum/database/src/main/library-loremipsum-adapter)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-loremipsum:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-base](https://bitbucket.org/android-universum/database/src/main/library-base),
[database-query](https://bitbucket.org/android-universum/database/src/main/library-query),
[database-content-core](https://bitbucket.org/android-universum/database/src/main/library-content-core),
[database-content-loader](https://bitbucket.org/android-universum/database/src/main/library-content-loader),
[database-adapter-common](https://bitbucket.org/android-universum/database/src/main/library-adapter-common_group),
[database-adapter-list](https://bitbucket.org/android-universum/database/src/main/library-adapter-list_group),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-entity](https://bitbucket.org/android-universum/database/src/main/library-entity_group),
[database-model](https://bitbucket.org/android-universum/database/src/main/library-model_group)
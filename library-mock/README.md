Database-Mock
===============

This module contains mock implementations of some elements provided by this library.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-mock:${DESIRED_VERSION}@aar"

_depends on:_
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor),
[database-model-core](https://bitbucket.org/android-universum/database/src/main/library-model-core),
[database-model-cursor](https://bitbucket.org/android-universum/database/src/main/library-model-cursor)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [MockCursor](https://bitbucket.org/android-universum/database/src/main/library-mock/src/main/java/universum/studios/android/database/mock/MockCursor.java)
- [MockModelCursorWrapper](https://bitbucket.org/android-universum/database/src/main/library-mock/src/main/java/universum/studios/android/database/mock/MockModelCursorWrapper.java)
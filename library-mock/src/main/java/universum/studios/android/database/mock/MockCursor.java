/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.mock;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

import java.util.concurrent.atomic.AtomicBoolean;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;

/**
 * A {@link Cursor} implementation that may be used for testing or to provide mock data set during
 * the development phase of an Android application.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class MockCursor implements Cursor {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "MockCursor";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Mock count of this cursor data set.
	 */
	private final int count;

	/**
	 * Current iterated position of this cursor's data set.
	 */
	private int position = -1;

	/**
	 * Mock notification uri of this cursor.
	 */
	private Uri notificationUri;

	/**
	 * Mock extras of this cursor.
	 */
	private Bundle extras;

	/**
	 * Flag indicating whether this mock cursor is closed or not.
	 */
	private final AtomicBoolean closed = new AtomicBoolean(false);

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of MockCursor in the specified data set <var>count</var>.
	 *
	 * @param count The desired size of the new mock cursor.
	 */
	public MockCursor(@Size(min = 0) final int count) {
		this.count = count;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the count passed to constructor of this MockCursor.
	 */
	@Override public int getCount() {
		return count;
	}

	/**
	 * Always returns the given <var>extras</var>.
	 */
	@Override @NonNull public Bundle respond(@NonNull final Bundle extras) {
		return extras;
	}

	/**
	 */
	@Override public int getPosition() {
		return position;
	}

	/**
	 */
	@Override public boolean moveToFirst() {
		return moveToPosition(0);
	}

	/**
	 */
	@Override public boolean moveToLast() {
		return moveToPosition(count - 1);
	}

	/**
	 */
	@Override public boolean moveToNext() {
		return moveToPosition(position + 1);
	}

	/**
	 */
	@Override public boolean moveToPrevious() {
		return moveToPosition(position - 1);
	}

	/**
	 */
	@Override public boolean move(int offset) {
		return moveToPosition(position + offset);
	}

	/**
	 */
	@Override public boolean moveToPosition(final int position) {
		if (count == 0) {
			this.position = -1;
			return false;
		}
		if (position >= count) {
			this.position = count;
			return false;
		}
		if (position < 0) {
			this.position = -1;
			return false;
		}
		this.position = position;
		return true;
	}

	/**
	 */
	@Override public boolean isFirst() {
		return count > 0 && position == 0;
	}

	/**
	 */
	@Override public boolean isLast() {
		return count > 0 && position == count - 1;
	}

	/**
	 */
	@Override public boolean isBeforeFirst() {
		return position < 0;
	}

	/**
	 */
	@Override public boolean isAfterLast() {
		return position >= count;
	}

	/**
	 * Always returns {@code -1}.
	 */
	@Override public int getColumnIndex(@NonNull final String columnName) {
		return -1;
	}

	/**
	 * Always returns {@code -1}.
	 */
	@Override public int getColumnIndexOrThrow(@NonNull final String columnName) throws IllegalArgumentException {
		return -1;
	}

	/**
	 * Always returns empty name.
	 */
	@Override public String getColumnName(final int columnIndex) {
		return "";
	}

	/**
	 * Always returns empty String array.
	 */
	@Override @NonNull public String[] getColumnNames() {
		return new String[0];
	}

	/**
	 * Always returns {@code 0}.
	 */
	@Override public int getColumnCount() {
		return 0;
	}

	/**
	 * Always returns empty byte array.
	 */
	@Override @NonNull public byte[] getBlob(final int columnIndex) {
		return new byte[0];
	}

	/**
	 * Always returns empty string.
	 */
	@Override @NonNull public String getString(final int columnIndex) {
		return "";
	}

	/**
	 * Always returns {@code 0}.
	 */
	@Override public short getShort(final int columnIndex) {
		return 0;
	}

	/**
	 * Always returns {@code 0}.
	 */
	@Override public int getInt(final int columnIndex) {
		return 0;
	}

	/**
	 * Always returns {@code 0}.
	 */
	@Override public long getLong(final int columnIndex) {
		return 0;
	}

	/**
	 * Always returns {@code 0}.
	 */
	@Override public float getFloat(final int columnIndex) {
		return 0;
	}

	/**
	 * Always returns {@code 0}.
	 */
	@Override public double getDouble(final int columnIndex) {
		return 0;
	}

	/**
	 * Always returns {@code true}.
	 */
	@Override public boolean isNull(final int columnIndex) {
		return getType(columnIndex) == FIELD_TYPE_NULL;
	}

	/**
	 * Always returns {@link #FIELD_TYPE_NULL}.
	 */
	@Override public int getType(final int columnIndex) {
		return FIELD_TYPE_NULL;
	}

	/**
	 * Does nothing.
	 */
	@Override public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
		// Mock implementation does nothing.
	}

	/**
	 * Does nothing.
	 */
	@Override public void deactivate() {
		// Mock implementation does nothing.
	}

	/**
	 * Does nothing and always returns {@code false}.
	 */
	@Override public boolean requery() {
		return false;
	}

	/**
	 * Does nothing.
	 */
	@Override public void registerContentObserver(ContentObserver observer) {
		// Mock implementation does nothing.
	}

	/**
	 * Does nothing.
	 */
	@Override public void unregisterContentObserver(ContentObserver observer) {
		// Mock implementation does nothing.
	}

	/**
	 * Does nothing.
	 */
	@Override public void registerDataSetObserver(DataSetObserver observer) {
		// Mock implementation does nothing.
	}

	/**
	 * Does nothing.
	 */
	@Override public void unregisterDataSetObserver(DataSetObserver observer) {
		// Mock implementation does nothing.
	}

	/**
	 * Sets the mock notification uri for this MockCursor instance.
	 */
	@Override public void setNotificationUri(@Nullable final ContentResolver resolver, @Nullable final Uri uri) {
		this.notificationUri = uri;
	}

	/**
	 * Returns the mock notification uri passed to {@link #setNotificationUri(ContentResolver, Uri)}.
	 */
	@Override @Nullable public Uri getNotificationUri() {
		return notificationUri;
	}

	/**
	 * Sets the mock extras for this MockCursor instance.
	 */
	@Override public void setExtras(@Nullable final Bundle extras) {
		this.extras = extras;
	}

	/**
	 * Returns the mock extras passed to {@link #setExtras(Bundle)}.
	 */
	@Override @Nullable public Bundle getExtras() {
		return extras;
	}

	/**
	 * Always returns {@code false}.
	 */
	@Override public boolean getWantsAllOnMoveCalls() {
		return false;
	}

	/**
	 * Sets the closed flag to {@code true}.
	 */
	@Override public void close() {
		this.closed.set(true);
	}

	/**
	 * Returns {@code true} if {@link #close()} was already called, {@code false} otherwise.
	 */
	@Override public boolean isClosed() {
		return closed.get();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
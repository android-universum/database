/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.mock;

import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.annotation.Size;
import universum.studios.android.database.model.EntityModel;
import universum.studios.android.database.model.EntityModelCursorWrapper;

/**
 * An {@link EntityModelCursorWrapper} that may be used for testing or to provide mock data set for
 * a desired model. This wrapper uses {@link universum.studios.android.database.mock.MockCursor MockCursor}
 * to 'fake' its data set.
 *
 * @param <M> A type of the model used to bind and present data of this class of MockModelCursorWrapper.
 * @author Martin Albedinsky
 */
public class MockModelCursorWrapper<M extends EntityModel> extends EntityModelCursorWrapper<M> {

	/**
	 * Creates a new instance of MockModelCursorWrapper with the specified <var>count</var> for
	 * {@link universum.studios.android.database.mock.MockCursor} instance.
	 *
	 * @param count The desired size of this cursor wrapper's data set.
	 * @see EntityModelCursorWrapper#EntityModelCursorWrapper(Cursor)
	 */
	public MockModelCursorWrapper(@Size(min = 0) int count) {
		super(new MockCursor(count));
	}

	/**
	 * Creates a new instance of MockModelCursorWrapper with the specified <var>count</var> for
	 * {@link universum.studios.android.database.mock.MockCursor} instance.
	 *
	 * @param count The desired size of this cursor wrapper's data set.
	 * @see EntityModelCursorWrapper#EntityModelCursorWrapper(Cursor, EntityModel)
	 */
	public MockModelCursorWrapper(@Size(min = 0) int count, @NonNull M model) {
		super(new MockCursor(count), model);
	}
}
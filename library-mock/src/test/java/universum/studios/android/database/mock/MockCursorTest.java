/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.mock;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class MockCursorTest extends AndroidTestCase {

	private static final int CURSOR_SIZE = 100;

	@Test public void testInstantiation() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.isClosed(), is(false));
		assertThat(cursor.getCount(), is(CURSOR_SIZE));
		assertThat(cursor.getColumnCount(), is(0));
		assertThat(cursor.getPosition(), is(-1));
		assertThat(cursor.isBeforeFirst(), is(true));
		assertThat(cursor.isFirst(), is(false));
		assertThat(cursor.isLast(), is(false));
		assertThat(cursor.isAfterLast(), is(false));
		assertThat(cursor.getColumnIndex("column"), is(-1));
		assertThat(cursor.getColumnIndexOrThrow("column"), is(-1));
		assertThat(cursor.getType(0), is(0));
		assertThat(cursor.isNull(0), is(true));
		assertThat(cursor.getString(0), is(""));
		assertThat(cursor.getShort(0), is((short) 0));
		assertThat(cursor.getInt(0), is(0));
		assertThat(cursor.getLong(0), is(0L));
		assertThat(cursor.getFloat(0), is(0.0f));
		assertThat(cursor.getDouble(0), is(0.0d));
		final byte[] blob = cursor.getBlob(0);
		assertThat(blob, is(notNullValue()));
		assertThat(blob.length, is(0));
		assertThat(cursor.getWantsAllOnMoveCalls(), is(false));
	}

	@Test public void testRespond() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		final Bundle extras = new Bundle();
		// Act + Assert:
		assertThat(cursor.respond(extras), is(extras));
	}

	@Test public void testGetPositionOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		// Not yet moved cursor is by default at -1 position.
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveToFirst() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.moveToFirst(), is(true));
		assertThat(cursor.getPosition(), is(0));
	}

	@Test public void testMoveToFirstOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		assertThat(cursor.moveToFirst(), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveToLast() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.moveToLast(), is(true));
		assertThat(cursor.getPosition(), is(CURSOR_SIZE - 1));
	}

	@Test public void testMoveToLastOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		assertThat(cursor.moveToLast(), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveToNext() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.moveToNext(), is(true));
		assertThat(cursor.getPosition(), is(0));
		assertThat(cursor.moveToNext(), is(true));
		assertThat(cursor.getPosition(), is(1));
		cursor.moveToLast();
		assertThat(cursor.moveToNext(), is(false));
		assertThat(cursor.getPosition(), is(CURSOR_SIZE));
	}

	@Test public void testMoveToNextOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		assertThat(cursor.moveToNext(), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveToPrevious() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.moveToPrevious(), is(false));
		cursor.moveToLast();
		assertThat(cursor.moveToPrevious(), is(true));
		assertThat(cursor.getPosition(), is(CURSOR_SIZE - 2));
		cursor.moveToFirst();
		assertThat(cursor.moveToPrevious(), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveToPreviousOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		cursor.moveToLast();
		assertThat(cursor.moveToPrevious(), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveByPositiveOffset() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		// Not yet moved cursor is by default at -1 position.
		assertThat(cursor.move(10), is(true));
		assertThat(cursor.getPosition(), is(9));
		cursor.moveToLast();
		assertThat(cursor.move(1), is(false));
		assertThat(cursor.getPosition(), is(CURSOR_SIZE));
	}

	@Test public void testMoveByPositiveOffsetOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		assertThat(cursor.move(1), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveByNegativeOffset() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		cursor.moveToLast();
		assertThat(cursor.move(-10), is(true));
		assertThat(cursor.getPosition(), is(89));
		cursor.moveToFirst();
		assertThat(cursor.move(-1), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveByNegativeOffsetOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		cursor.moveToLast();
		assertThat(cursor.move(-1), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testMoveToPosition() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.moveToPosition(0), is(true));
		assertThat(cursor.getPosition(), is(0));
		assertThat(cursor.moveToPosition(CURSOR_SIZE - 1), is(true));
		assertThat(cursor.getPosition(), is(CURSOR_SIZE - 1));
		assertThat(cursor.moveToPosition(-1), is(false));
		assertThat(cursor.getPosition(), is(-1));
		assertThat(cursor.moveToPosition(CURSOR_SIZE), is(false));
		assertThat(cursor.getPosition(), is(CURSOR_SIZE));
	}

	@Test public void testMoveToPositionOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		assertThat(cursor.moveToPosition(0), is(false));
		assertThat(cursor.getPosition(), is(-1));
		assertThat(cursor.moveToPosition(-1), is(false));
		assertThat(cursor.getPosition(), is(-1));
	}

	@Test public void testIsFirst() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		cursor.moveToFirst();
		assertThat(cursor.isFirst(), is(true));
		cursor.moveToPosition(0);
		assertThat(cursor.isFirst(), is(true));
		cursor.moveToPosition(1);
		assertThat(cursor.isFirst(), is(false));
	}

	@Test public void testIsFirstOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		cursor.moveToFirst();
		// Act + Assert:
		assertThat(cursor.isFirst(), is(false));
	}

	@Test public void testIsLast() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		cursor.moveToLast();
		assertThat(cursor.isLast(), is(true));
		cursor.moveToPosition(CURSOR_SIZE - 1);
		assertThat(cursor.isLast(), is(true));
		cursor.moveToPosition(90);
		assertThat(cursor.isLast(), is(false));
	}

	@Test public void testIsLastOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		cursor.moveToLast();
		// Act + Assert:
		assertThat(cursor.isLast(), is(false));
	}

	@Test public void testIsBeforeFirst() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		cursor.moveToPosition(10);
		assertThat(cursor.isBeforeFirst(), is(false));
		cursor.moveToPosition(-10);
		assertThat(cursor.isBeforeFirst(), is(true));
		cursor.moveToPosition(0);
		cursor.moveToPrevious();
		assertThat(cursor.isBeforeFirst(), is(true));
	}

	@Test public void testIsBeforeFirstOnEmptyCursor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		cursor.moveToPosition(0);
		assertThat(cursor.isBeforeFirst(), is(true));
	}

	@Test public void testIsAfterLast() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		cursor.moveToPosition(CURSOR_SIZE);
		assertThat(cursor.isAfterLast(), is(true));
		cursor.moveToPosition(CURSOR_SIZE - 1);
		cursor.moveToNext();
		assertThat(cursor.isAfterLast(), is(true));
	}

	@Test public void testIsAfterLastOnEmptyConstructor() {
		// Arrange:
		final Cursor cursor = new MockCursor(0);
		// Act + Assert:
		cursor.moveToPosition(CURSOR_SIZE);
		assertThat(cursor.isAfterLast(), is(false));
	}

	@Test public void testGetColumnNames() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		final String[] columnNames = cursor.getColumnNames();
		assertThat(columnNames, is(notNullValue()));
		assertThat(columnNames.length, is(0));
	}

	@Test public void testGetColumnName() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.getColumnName(0), is(""));
		assertThat(cursor.getColumnName(1), is(""));
		// ... for what ever index this method returns "" string
	}

	@Test public void testSetExtras() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.getExtras(), is(nullValue()));
		final Bundle extras = new Bundle();
		cursor.setExtras(extras);
		assertThat(cursor.getExtras(), is(extras));
	}

	@Test public void testCopyStringToBuffer() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act:
		cursor.copyStringToBuffer(0, null);
	}

	@Test public void testSetNotificationUri() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		cursor.setNotificationUri(null, Uri.parse("content://database.test"));
		assertThat(cursor.getNotificationUri(), is(Uri.parse("content://database.test")));
	}

	@Test public void testRegisterContentObserver() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act:
		cursor.registerContentObserver(null);
	}

	@Test public void testUnregisterContentObserver() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act:
		cursor.unregisterContentObserver(null);
	}

	@Test public void testRegisterDataSetObserver() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act:
		cursor.registerDataSetObserver(null);
	}

	@Test public void testUnregisterDataSetObserver() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act:
		cursor.unregisterDataSetObserver(null);
	}

	@Test public void testRequery() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act + Assert:
		assertThat(cursor.requery(), is(false));
	}

	@Test public void testDeactivate() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act:
		cursor.deactivate();
	}

	@Test public void testClose() {
		// Arrange:
		final Cursor cursor = new MockCursor(CURSOR_SIZE);
		// Act:
		cursor.close();
		// Assert:
		assertThat(cursor.isClosed(), is(true));
	}
}
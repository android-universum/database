@Database-Adapter
===============

This module groups the following modules into one **single group**:

- [Adapter-Core](https://bitbucket.org/android-universum/database/src/main/library-adapter-core)
- [Adapter-Header](https://bitbucket.org/android-universum/database/src/main/library-adapter-header)
- [Adapter-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-loader)
- [Adapter-List-Base](https://bitbucket.org/android-universum/database/src/main/library-adapter-list-base)
- [Adapter-List-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-list-loader)
- [Adapter-Pager-Base](https://bitbucket.org/android-universum/database/src/main/library-adapter-pager-base)
- [Adapter-Pager-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-pager-loader)
- [Adapter-Recycler-Base](https://bitbucket.org/android-universum/database/src/main/library-adapter-recycler-base)
- [Adapter-Recycler-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-recycler-loader)
- [Adapter-Spinner-Base](https://bitbucket.org/android-universum/database/src/main/library-adapter-spinner-base)
- [Adapter-Spinner-Loader](https://bitbucket.org/android-universum/database/src/main/library-adapter-spinner-loader)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter:${DESIRED_VERSION}@aar"

_depends on:_
[pager-adapters-core](https://github.com/universum-studios/android_pager_adapters/tree/main/library-core),
[pager-adapters-stateful](https://github.com/universum-studios/android_pager_adapters/tree/main/library-stateful),
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content](https://bitbucket.org/android-universum/database/src/main/library-content_group),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor)
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class SortOrderTest extends TestCase {

	@Test public void testContract() {
		// Assert:
		assertThat(SortOrder.ASC, is("ASC"));
		assertThat(SortOrder.DESC, is("DESC"));
	}

	@Test public void testInstantiation() {
		// Act:
		final SortOrder order = new SortOrder("ASC");
		// Assert:
		assertThat(order.keyWord(), is("ASC"));
	}

	@Test public void testAsc() {
		// Act:
		final SortOrder order = SortOrder.asc().column("title");
		// Assert:
		assertThat(order.build(), is("title ASC"));
	}

	@Test public void testDesc() {
		// Act:
		final SortOrder order = SortOrder.desc().column("title");
		// Assert:
		assertThat(order.build(), is("title DESC"));
	}

	@Test public void testBuild() {
		// Arrange:
		final SortOrder order = new SortOrder("ASC").columns("title", "category_id");
		// Act + Assert:
		assertThat(order.build(), is("title, category_id ASC"));
	}

	@Test public void testBuildWithoutColumns() {
		// Arrange:
		final SortOrder order = new SortOrder("ASC");
		// Act + Assert:
		assertThat(order.build(), is("ASC"));
	}

	@Test public void testHashCode() {
		// Arrange:
		final SortOrder sortOrder = new SortOrder("ASC");
		// Act + Assert:
		assertThat(sortOrder.hashCode(), is(31 * "ASC".hashCode() + 1));
	}

	@Test public void testHashCodeWithColumns() {
		// Arrange:
		final List<String> columns = new ArrayList<>(2);
		columns.add("column1");
		columns.add("column2");
		final SortOrder sortOrder = new SortOrder("ASC").columns("column1", "column2");
		// Act + Assert:
		assertThat(sortOrder.hashCode(), is(31 * "ASC".hashCode() + columns.hashCode()));
	}

	@Test public void testEqualsWithEqualKeyWords() {
		// Arrange:
		final SortOrder first = new SortOrder("ASC");
		final SortOrder second = new SortOrder("ASC");
		// Act + Assert:
		assertThat(first.equals(second), is(true));
	}

	@Test public void testEqualsWithEqualKeyWordsAndColumns() {
		// Arrange:
		final SortOrder first = new SortOrder("ASC").columns("title", "author");
		final SortOrder second = new SortOrder("ASC").columns("title", "author");
		// Act + Assert:
		assertThat(first.equals(second), is(true));
	}

	@Test public void testEqualsWithDifferentKeyWords() {
		// Arrange:
		final SortOrder first = new SortOrder("ASC");
		final SortOrder second = new SortOrder("DESC");
		// Act:
		// Assert:
		assertThat(first.equals(second), is(false));
	}

	@Test public void testEqualsWithDifferentColumns() {
		// Arrange:
		final SortOrder first = new SortOrder("ASC").columns("title");
		final SortOrder second = new SortOrder("ASC").columns("title", "author");
		// Act + Assert:
		assertThat(first.equals(second), is(false));
	}

	@Test public void testEqualsWithColumnsInDifferentOrder() {
		// Arrange:
		final SortOrder first = new SortOrder("ASC").columns("author", "title");
		final SortOrder second = new SortOrder("ASC").columns("title", "author");
		// Act + Assert:
		assertThat(first.equals(second), is(false));
	}

	@Test public void testEqualsWithDifferentKeyWordsAndColumn() {
		// Arrange:
		final SortOrder first = new SortOrder("ASC").columns("title");
		final SortOrder second = new SortOrder("DESC").columns("title", "author");
		// Act:
		// Assert:
		assertThat(first.equals(second), is(false));
	}

	@SuppressWarnings("EqualsWithItself")
	@Test public void testEqualsWithSameInstances() {
		// Arrange:
		final SortOrder order = new SortOrder("ASC").columns("title", "author");
		// Act + Assert:
		assertThat(order.equals(order), is(true));
	}

	@SuppressWarnings("EqualsBetweenInconvertibleTypes")
	@Test public void testEqualsWithDifferentTypes() {
		// Arrange:
		final SortOrder order = new SortOrder("ASC");
		// Act + Assert:
		assertThat(order.equals(1), is(false));
	}
}
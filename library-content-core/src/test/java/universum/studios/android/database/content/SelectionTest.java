/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class SelectionTest extends AndroidTestCase {

	@Test public void testInFormat() {
		// Act + Assert:
		assertThat(
				Selection.inFormat("state", 4),
				is("state IN(?,?,?,?)")
		);
	}

	@Test public void testInIntValuesVarious() {
		// Act + Assert:
		assertThat(
				Selection.in("state", 2, 3, -1),
				is("state IN(2, 3, -1)")
		);
	}

	@Test public void testInIntValuesArray() {
		// Arrange:
		final int[] values = new int[]{2, 3, -1};
		// Act + Assert:
		assertThat(
				Selection.in("state", values),
				is("state IN(2, 3, -1)")
		);
	}

	@Test public void testInLongValuesVarious() {
		// Act + Assert:
		assertThat(
				Selection.in("_id", 100L, 1000L, 10000L),
				is("_id IN(100, 1000, 10000)")
		);
	}

	@Test public void testInLongValuesArray() {
		// Act + Assert:
		final long[] values = new long[]{100L, 1000L, 10000L};
		// Act + Assert:
		assertThat(
				Selection.in("_id", values),
				is("_id IN(100, 1000, 10000)")
		);
	}

	@Test public void testInValuesVarious() {
		// Act + Assert:
		assertThat(
				Selection.in("state", "DONE", "PENDING", 11),
				is("state IN(DONE, PENDING, 11)")
		);
	}

	@Test public void testInValuesArray() {
		// Arrange:
		final Object[] values = new Object[]{"DONE", "PENDING", 11};
		// Act + Assert:
		assertThat(
				Selection.in("state", values),
				is("state IN(DONE, PENDING, 11)")
		);
	}

	@Test public void testNotInFormat() {
		// Act + Assert:
		assertThat(
				Selection.notInFormat("state", 2),
				is("state NOT IN(?,?)")
		);
	}

	@Test public void testNotIntValuesVarious() {
		// Act + Assert:
		assertThat(
				Selection.notIn("state", 2, 3, -1),
				is("state NOT IN(2, 3, -1)")
		);
	}

	@Test public void testNotIntValuesArray() {
		// Arrange:
		final int[] values = new int[]{2, 3, -1};
		// Act + Assert:
		assertThat(
				Selection.notIn("state", values),
				is("state NOT IN(2, 3, -1)")
		);
	}

	@Test public void testNotLongValuesVarious() {
		// Act + Assert:
		assertThat(
				Selection.notIn("_id", 100L, 1000L, 10000L),
				is("_id NOT IN(100, 1000, 10000)")
		);
	}

	@Test public void testNotLongValuesArray() {
		// Arrange:
		final long[] values = new long[]{100L, 1000L, 10000L};
		// Act + Assert:
		assertThat(
				Selection.notIn("_id", values),
				is("_id NOT IN(100, 1000, 10000)")
		);
	}

	@Test public void testNotInValuesVarious() {
		// Act + Assert:
		assertThat(
				Selection.notIn("state", 4, 5),
				is("state NOT IN(4, 5)")
		);
	}

	@Test public void setNotInValuesArray() {
		// Arrange:
		final Object[] args = new Object[]{4, 5};
		// Act + Assert:
		assertThat(
				Selection.notIn("state", args),
				is("state NOT IN(4, 5)")
		);
	}

	@Test public void testIntArgsAsStringArray() {
		// Arrange + Act:
		final String[] args = Selection.argsAsStringArray(new int[]{1, 2, 3});
		// Assert:
		assertThat(args, is(not(nullValue())));
		assertThat(args.length, is(3));
		assertThat(args[0], is("1"));
		assertThat(args[1], is("2"));
		assertThat(args[2], is("3"));
	}

	@Test public void testLongArgsAsStringArray() {
		// Arrange + Act:
		final String[] args = Selection.argsAsStringArray(new long[]{1001L, 1002L, 1003L});
		// Assert:
		assertThat(args, is(not(nullValue())));
		assertThat(args.length, is(3));
		assertThat(args[0], is("1001"));
		assertThat(args[1], is("1002"));
		assertThat(args[2], is("1003"));
	}

	@Test public void testObjectArgsAsStringArray() {
		// Arrange + Act:
		final String[] args = Selection.argsAsStringArray("Rome", 1564L, true);
		// Assert:
		assertThat(args, is(not(nullValue())));
		assertThat(args.length, is(3));
		assertThat(args[0], is("Rome"));
		assertThat(args[1], is("1564"));
		assertThat(args[2], is("true"));
	}

	@Test public void testEmptyArgsAsStringArray() {
		// Arrange + Act:
		final String[] args = Selection.argsAsStringArray();
		// Assert:
		assertThat(args, is(not(nullValue())));
		assertThat(args.length, is(0));
	}

	@Test public void testTextArgsAsStringArray() {
		// Arrange + Act:
		final String[] args = Selection.textArgsAsStringArray("Rome", 1564L, true, "New York");
		// Assert:
		assertThat(args, is(not(nullValue())));
		assertThat(args.length, is(4));
		assertThat(args[0], is("'Rome'"));
		assertThat(args[1], is("1564"));
		assertThat(args[2], is("true"));
		assertThat(args[3], is("'New York'"));
	}

	@Test public void testEmptyTextArgsAsStringArray() {
		// Arrange + Act:
		final String[] args = Selection.textArgsAsStringArray();
		// Assert:
		assertThat(args, is(not(nullValue())));
		assertThat(args.length, is(0));
	}

	@Test
	public void testInstantiationWithoutInitialValue() {
		final Selection selection = new Selection();
		assertThat(selection.isEmpty(), is(true));
		assertThat(selection.hasArguments(), is(false));
		assertThat(selection.build(), is(""));
	}

	@Test
	public void testInstantiationWithInitialValue() {
		final Selection selection = new Selection("_id=1234 AND count>0");
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.hasArguments(), is(false));
		assertThat(selection.build(), is("_id=1234 AND count>0"));
	}

	@Test
	public void testAndSelection() {
		final Selection selection = new Selection("_id!=1234");
		assertThat(selection.and(new Selection("enabled=1")), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id!=1234 AND enabled=1"));
	}

	@Test
	public void testAnd() {
		final Selection selection = new Selection();
		assertThat(selection.and("_id=1234"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id=1234"));
	}

	@Test
	public void testMultipleAnd() {
		final Selection selection = new Selection();
		assertThat(selection.and("_id!=1234").and("count>0"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id!=1234 AND count>0"));
	}

	@Test
	public void testGroupAndSelection() {
		final Selection selection = new Selection("_id!=1234");
		assertThat(selection.groupAnd(new Selection("count>0 OR enabled=1")), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id!=1234 AND (count>0 OR enabled=1)"));
	}

	@Test
	public void testGroupAnd() {
		final Selection selection = new Selection("_id!=1234");
		assertThat(selection.groupAnd("count>0 OR enabled=1"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id!=1234 AND (count>0 OR enabled=1)"));
	}

	@Test
	public void testOrSelection() {
		final Selection selection = new Selection("_id=1234");
		assertThat(selection.or(new Selection("published=1")), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id=1234 OR published=1"));
	}

	@Test
	public void testOr() {
		final Selection selection = new Selection();
		assertThat(selection.or("_id=1234"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id=1234"));
	}

	@Test
	public void testMultipleOr() {
		final Selection selection = new Selection();
		assertThat(selection.or("_id=1234").or("_id=1235"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id=1234 OR _id=1235"));
	}

	@Test
	public void testGroupOrSelection() {
		final Selection selection = new Selection("_id=1234");
		assertThat(selection.groupOr(new Selection("enabled=1 AND published=1")), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id=1234 OR (enabled=1 AND published=1)"));
	}

	@Test
	public void testGroupOr() {
		final Selection selection = new Selection("_id=1234");
		assertThat(selection.groupOr("enabled=1 AND published=1"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id=1234 OR (enabled=1 AND published=1)"));
	}

	@Test
	public void testSelectionString() {
		final Selection selection = new Selection();
		assertThat(selection.selection("_id!=1234 OR _id!=1235"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id!=1234 OR _id!=1235"));
	}

	@Test
	public void testSelection() {
		final Selection selection = new Selection();
		assertThat(selection.selection(new Selection("_id=1234")), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("_id=1234"));
	}

	@Test
	public void testGroupSelectionString() {
		final Selection selection = new Selection();
		assertThat(selection.groupSelection("enabled=1 AND published=0"), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("(enabled=1 AND published=0)"));
	}

	@Test
	public void testGroupSelection() {
		final Selection selection = new Selection();
		assertThat(selection.groupSelection(new Selection("published=1 AND reviewed=1")), is(selection));
		assertThat(selection.isEmpty(), is(false));
		assertThat(selection.build(), is("(published=1 AND reviewed=1)"));
	}

	@Test
	public void testDefaultArguments() {
		assertThat(new Selection().arguments(), is(notNullValue()));
	}

	@Test
	public void testDefaultHasArguments() {
		assertThat(new Selection().hasArguments(), is(false));
	}

	@Test
	public void testObjectArgument() {
		final Selection selection = new Selection();
		assertThat(selection.argument(45), is(selection));
		assertThat(selection.hasArguments(), is(true));
		assertThat(selection.isEmpty(), is(true));
		final String[] args = selection.arguments();
		assertThat(args, is(notNullValue()));
		assertThat(args.length, is(1));
		assertThat(args[0], is("45"));
	}

	@Test
	public void testStringArgumentMultiple() {
		final Selection selection = new Selection();
		assertThat(selection.argument("Arg.1").argument("Arg.2").argument("Arg.3"), is(selection));
		assertThat(selection.hasArguments(), is(true));
		assertThat(selection.isEmpty(), is(true));
		final String[] args = selection.arguments();
		assertThat(args, is(notNullValue()));
		assertThat(args.length, is(3));
		assertThat(args[0], is("Arg.1"));
		assertThat(args[1], is("Arg.2"));
		assertThat(args[2], is("Arg.3"));
	}

	@Test
	public void testObjectArguments() {
		final Selection selection = new Selection();
		assertThat(selection.arguments(2, 1000L, "Arg.1", false), is(selection));
		assertThat(selection.hasArguments(), is(true));
		assertThat(selection.isEmpty(), is(true));
		final String[] args = selection.arguments();
		assertThat(args, is(notNullValue()));
		assertThat(args.length, is(4));
		assertThat(args[0], is("2"));
		assertThat(args[1], is("1000"));
		assertThat(args[2], is("Arg.1"));
		assertThat(args[3], is("false"));
	}

	@Test
	public void testStringArgumentsSingle() {
		final Selection selection = new Selection();
		assertThat(selection.arguments("2", "1000", "Arg.1", "false"), is(selection));
		assertThat(selection.hasArguments(), is(true));
		assertThat(selection.isEmpty(), is(true));
		final String[] args = selection.arguments();
		assertThat(args, is(notNullValue()));
		assertThat(args.length, is(4));
		assertThat(args[0], is("2"));
		assertThat(args[1], is("1000"));
		assertThat(args[2], is("Arg.1"));
		assertThat(args[3], is("false"));
	}

	@Test
	public void testStringArgumentsMultiple() {
		final Selection selection = new Selection();
		assertThat(selection.arguments("2", "1000", "Arg.1", "false").arguments("-2", "-1000", "Arg.2", "true"), is(selection));
		assertThat(selection.hasArguments(), is(true));
		assertThat(selection.isEmpty(), is(true));
		final String[] args = selection.arguments();
		assertThat(args, is(notNullValue()));
		assertThat(args.length, is(8));
		assertThat(args[0], is("2"));
		assertThat(args[1], is("1000"));
		assertThat(args[2], is("Arg.1"));
		assertThat(args[3], is("false"));
		assertThat(args[4], is("-2"));
		assertThat(args[5], is("-1000"));
		assertThat(args[6], is("Arg.2"));
		assertThat(args[7], is("true"));
	}

	@Test
	public void testClear() {
		final Selection selection = new Selection("_id=?").argument(101L);
		assertThat(selection.clear(), is(selection));
		assertThat(selection.isEmpty(), is(true));
		assertThat(selection.hasArguments(), is(false));
		selection.selection("_id=? AND state=?").arguments(101L, 2);
		assertThat(selection.clear(), is(selection));
		assertThat(selection.isEmpty(), is(true));
		assertThat(selection.hasArguments(), is(false));
	}
}
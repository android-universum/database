/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import org.junit.Test;

import java.util.List;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class ProjectionTest extends TestCase {

	@Test public void testColumnProjection() {
		// Arrange:
		final Projection projection = new Projection();
		// Act:
		projection.column(new Projection.Column("title"));
		// Assert:
		final List<String> columns = projection.columns();
		assertThat(columns.size(), is(1));
		assertThat(columns.get(0), is("title"));
	}

	@Test public void testBuild() {
		// Arrange:
		final Projection projection = new Projection().columns("title", "author");
		// Act + Assert:
		assertThat(projection.build(), is(new String[]{"title", "author"}));
	}

	@Test public void testColumnBuild() {
		// Arrange:
		final Projection.Column column = new Projection.Column("name");
		// Act + Assert:
		assertThat(column.build(), is("name"));
	}

	@Test public void testColumnBuildWithAsParameter() {
		// Arrange:
		final Projection.Column column = new Projection.Column("name").as("customer_name");
		// Act + Assert:
		assertThat(column.build(), is("name AS customer_name"));
	}

	@Test public void testColumnBuildWithFromAndAsParameter() {
		// Arrange:
		final Projection.Column column = new Projection.Column("name").from("customers").as("customer_name");
		// Act:
		// Assert:
		assertThat(column.build(), is("customers.name AS customer_name"));
	}

	@Test public void testColumnBuildWithInvalidName() {
		// Arrange:
		final Projection.Column column = new Projection.Column("");
		try {
			// Act:
			column.build();
		} catch (IllegalArgumentException e) {
			// Assert:
			assertThat(e.getMessage(), is("No column name specified!"));
			return;
		}
		throw new AssertionError("No exception thrown.");
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import android.database.DatabaseUtils;

import java.util.Arrays;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Size;

/**
 * Selection builder may be used to build a <b>selection</b> argument for a desired {@link android.content.CursorLoader CursorLoader}.
 * <p>
 * Selection may be used to supply only simple selection specified via {@link #Selection(String)}
 * constructor and when building via {@link #build()} that supplied selection string will be returned.
 * Or it may be used to build a little more complicated selection statement via following methods:
 * <ul>
 * <li>{@link #selection(Selection)}</li>
 * <li>{@link #selection(String)}</li>
 * <li>{@link #groupSelection(Selection)}</li>
 * <li>{@link #groupSelection(String)}</li>
 * <li>{@link #and(String)}</li>
 * <li>{@link #and(Selection)}</li>
 * <li>{@link #groupAnd(Selection)}</li>
 * <li>{@link #groupAnd(String)}</li>
 * <li>{@link #or(String)}</li>
 * <li>{@link #or(Selection)}</li>
 * <li>{@link #groupOr(Selection)}</li>
 * <li>{@link #groupOr(String)}</li>
 * </ul>
 *
 * <h3>Usage</h3>
 * Given the following sample inputs, Selection builder produces selection statements like follows:
 * <pre>
 * 1) ==============================================================================================
 *
 * final Selection selection = new Selection();
 * selection.selection("type=2");
 * selection.groupSelection("title='SOME TITLE' OR title='SOME ANOTHER TITLE'");
 * selection.build(); =&gt; type=2 AND (title='SOME TITLE' OR title='SOME ANOTHER TITLE')
 *
 * 2) ==============================================================================================
 *
 * final Selection selection = new Selection();
 * selection.groupSelection(new Selection("category=1").or("expired=1"));
 * selection.and("count&gt;=100");
 * selection.build(); =&gt; (category=1 OR category=1) AND count&gt;=100
 * </pre>
 *
 * @author Martin Albedinsky
 * @see Projection
 * @see SortOrder
 * @see IdSelection
 */
public class Selection {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "Selection";

	/**
	 * Maximum size for the array of arguments.
	 */
	private static final int ARGUMENTS_MAX_SIZE = Integer.MAX_VALUE - 8;

	/**
	 * Shared empty array of arguments used as default arguments data.
	 */
	private static final String[] EMPTY_ARGUMENTS = {};

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Builder used to build selection statement.
	 */
	private final StringBuilder mBuilder;

	/**
	 * Array of arguments specified for this selection.
	 */
	@NonNull
	private String[] mArguments = EMPTY_ARGUMENTS;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of empty Selection.
	 */
	public Selection() {
		this("");
	}

	/**
	 * Same as {@link #Selection(String)} with initial selection.
	 *
	 * @see Selection#build()
	 */
	public Selection(@NonNull final Selection selection) {
		this(selection.build());
	}

	/**
	 * Creates a new instance of Selection with the specified initial <var>selection</var>.
	 *
	 * @param selection The desired initial selection value.
	 */
	public Selection(@NonNull final String selection) {
		this.mBuilder = new StringBuilder(selection);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a format with question marks in array sequence format: {@code ?,?,?,...,n} where {@code n=count}.
	 *
	 * @param count The desired count of question marks in the format.
	 * @return Format with desired count of question marks separated by {@code ','} separator.
	 */
	private static String questionMarksArrayFormat(final int count) {
		final StringBuilder builder = new StringBuilder(count * 2);
		for (int i = 0; i < count; i++) {
			builder.append("?");
			if (i < count - 1) {
				builder.append(",");
			}
		}
		return builder.toString();
	}

	/**
	 * Creates a format for {@code column_name IN(...)} statement
	 *
	 * @param columnName  Name of the column for which to create IN statement format.
	 * @param valuesCount Number of values that the statement will be formatted by. The created format
	 *                    will contain exactly the same count of {@code '?'} marks inside {@code (...)}
	 *                    brackets part separated by {@code ','} separator.
	 * @return IN format ready to be formatted and used in conjunction with WHERE clause.
	 */
	@NonNull
	public static String inFormat(@NonNull final String columnName, @IntRange(from = 1) final int valuesCount) {
		return columnName + " IN(" + questionMarksArrayFormat(valuesCount) + ")";
	}

	/**
	 * Version of {@link #in(String, Object...)} specific for array of int values.
	 *
	 * @param columnName Name of the column for which to create {@code IN(...)} statement.
	 * @param values     Array of int values of which string representations will be placed inside
	 *                   {@code (...)} brackets part of the created IN statement.
	 * @return IN statement for the specified int values that may be used in conjunction with WHERE clause.
	 */
	@NonNull
	public static String in(@NonNull final String columnName, @NonNull @Size(min = 1) final int[] values) {
		return columnName + " IN(" + notEnclosedArrayAsString(values) + ")";
	}

	/**
	 * Version of {@link #in(String, Object...)} specific for array of long values.
	 *
	 * @param columnName Name of the column for which to create IN statement.
	 * @param values     Array of long values of which string representations will be placed inside
	 *                   {@code (...)} brackets part of the created IN statement.
	 * @return IN statement for the specified long values that may be used in conjunction with WHERE clause.
	 */
	@NonNull
	public static String in(@NonNull final String columnName, @NonNull @Size(min = 1) final long[] values) {
		return columnName + " IN(" + notEnclosedArrayAsString(values) + ")";
	}

	/**
	 * Creates a {@code column_name IN(value, value, ...)} statement for the specified <var>columnName</var>
	 * and given <var>values</var>.
	 *
	 * @param columnName Name of the column for which to create IN statement.
	 * @param values     Array of values of which string representations will be placed inside
	 *                   {@code (...)} brackets part of the created IN statement.
	 * @return IN statement for the specified values that may be used in conjunction with WHERE clause.
	 * @see #notIn(String, Object...)
	 * @see #argsAsStringArray(Object...)
	 */
	@NonNull
	public static String in(@NonNull final String columnName, @NonNull @Size(min = 1) final Object... values) {
		return columnName + " IN(" + notEnclosedArrayAsString(values) + ")";
	}

	/**
	 * Creates a format for {@code column_name NOT IN(...)} statement
	 *
	 * @param columnName  Name of the column for which to create NOT IN statement format.
	 * @param valuesCount Number of values that the statement will be formatted by. The created format
	 *                    will contain exactly the same count of {@code '?'} marks inside {@code (...)}
	 *                    brackets part separated by {@code ','} separator.
	 * @return IN format ready to be formatted and used in conjunction with WHERE clause.
	 */
	@NonNull
	public static String notInFormat(@NonNull final String columnName, @IntRange(from = 1) final int valuesCount) {
		return columnName + " NOT IN(" + questionMarksArrayFormat(valuesCount) + ")";
	}

	/**
	 * Version of {@link #notIn(String, Object...)} specific for array of int values.
	 *
	 * @param columnName Name of the column for which to create NOT IN statement.
	 * @param values     Array of int values of which string representations will be placed inside
	 *                   {@code (...)} brackets part of the created NOT IN statement.
	 * @return NOT IN statement for the specified int values that may be used in conjunction with WHERE clause.
	 */
	@NonNull
	public static String notIn(@NonNull final String columnName, @NonNull @Size(min = 1) final long[] values) {
		return columnName + " NOT IN(" + notEnclosedArrayAsString(values) + ")";
	}

	/**
	 * Version of {@link #notIn(String, Object...)} specific for array of long values.
	 *
	 * @param columnName Name of the column for which to create NOT IN statement.
	 * @param values     Array of long values of which string representations will be placed inside
	 *                   {@code (...)} brackets part of the created NOT IN statement.
	 * @return NOT IN statement for the specified long values that may be used in conjunction with WHERE clause.
	 */
	@NonNull
	public static String notIn(@NonNull final String columnName, @NonNull @Size(min = 1) final int[] values) {
		return columnName + " NOT IN(" + notEnclosedArrayAsString(values) + ")";
	}

	/**
	 * Creates a {@code column_name NOT IN(value, value, ...)} statement for the specified <var>columnName</var>
	 * and given <var>values</var>.
	 *
	 * @param columnName Name of the column for which to create NOT IN statement.
	 * @param values     Array of value of which string representations will be placed inside
	 *                   {@code (...)}  brackets part of the created NOT IN statement.
	 * @return NOT IN statement for the specified value that may be used in conjunction with WHERE clause.
	 * @see #in(String, Object...)
	 * @see #argsAsStringArray(Object...)
	 */
	@NonNull
	public static String notIn(@NonNull final String columnName, @NonNull @Size(min = 1) final Object... values) {
		return columnName + " NOT IN(" + notEnclosedArrayAsString(values) + ")";
	}

	/**
	 * Like {@link #notEnclosedArrayAsString(Object...)} but specific for array of int values.
	 *
	 * @param values The int values to transform to not enclosed string array representation.
	 * @return String representing the given array of int values that is not enclosed by brackets.
	 */
	private static String notEnclosedArrayAsString(final int[] values) {
		final String arrayAsString = Arrays.toString(values);
		return arrayAsString.substring(1, arrayAsString.length() - 1);
	}

	/**
	 * Like {@link #notEnclosedArrayAsString(Object...)} but specific for array of long values.
	 *
	 * @param values The long values to transform to not enclosed string array representation.
	 * @return String representing the given array of long values that is not enclosed by brackets.
	 */
	private static String notEnclosedArrayAsString(final long[] values) {
		final String arrayAsString = Arrays.toString(values);
		return arrayAsString.substring(1, arrayAsString.length() - 1);
	}

	/**
	 * Transforms the given array of <var>values</var> into string representation of that array where
	 * array elements are separated by coma separator and are not enclosed by {@code []} brackets.
	 *
	 * @param values The values to transform to not enclosed string array representation.
	 * @return String representing the given array of values that is not enclosed by brackets.
	 * @see #argsAsStringArray(Object...)
	 */
	private static String notEnclosedArrayAsString(final Object... values) {
		final String arrayAsString = Arrays.toString(Selection.argsAsStringArray(values));
		return arrayAsString.substring(1, arrayAsString.length() - 1);
	}

	/**
	 * Like {@link #argsAsStringArray(Object...)} but specific for array of int values.
	 *
	 * @param args The desired int arguments to convert into string array.
	 * @return Array of same length as the specified array with string representation of each of
	 * elements contained within the given array.
	 * @see #argsAsStringArray(long...)
	 */
	@NonNull
	public static String[] argsAsStringArray(@NonNull @Size(min = 1) final int[] args) {
		final String[] stringArray = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			stringArray[i] = Integer.toString(args[i]);
		}
		return stringArray;
	}

	/**
	 * Like {@link #argsAsStringArray(Object...)} but specific for variable array of long values.
	 *
	 * @param args The desired long arguments to convert into string array.
	 * @return Array of same length as the specified array with string representation of each of
	 * elements contained within the given array.
	 * @see #argsAsStringArray(int...)
	 */
	@NonNull
	public static String[] argsAsStringArray(@NonNull @Size(min = 1) final long[] args) {
		final String[] stringArray = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			stringArray[i] = Long.toString(args[i]);
		}
		return stringArray;
	}

	/**
	 * Converts the specified array of Object <var>args</var> into array of theirs String
	 * representations using {@link String#valueOf(Object)}.
	 *
	 * @param args The desired arguments to convert into string array.
	 * @return Array of same length as the specified array with string representation of each of
	 * elements contained within the given array.
	 */
	@NonNull
	public static String[] argsAsStringArray(@NonNull @Size(min = 1) final Object... args) {
		final String[] stringArray = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			stringArray[i] = String.valueOf(args[i]);
		}
		return stringArray;
	}

	/**
	 * Like {@link #argsAsStringArray(Object...)} but this implementation will enclose elements of
	 * the given <var>args</var> array that are type of {@link String} in {@code ''} apostrophes,
	 * like {@code 'New York'}.
	 *
	 * @param args The desired arguments to convert into string array. Text arguments will be enclosed
	 *             into {@code ''}.
	 * @return Array of same length as the specified array with string representation of each of
	 * elements contained within the given array.
	 */
	@NonNull
	public static String[] textArgsAsStringArray(@NonNull @Size(min = 1) final Object... args) {
		final String[] stringArray = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			final Object arg = args[i];
			stringArray[i] = arg instanceof String ?
					DatabaseUtils.sqlEscapeString((String) arg) :
					String.valueOf(arg);
		}
		return stringArray;
	}

	/**
	 * Same as {@link #groupSelection(String)} where the raw selection will be build via {@link Selection#build()}
	 * from the given <var>selection</var>.
	 *
	 * @param selection The desired selection to append.
	 * @return This selection to allow methods chaining.
	 */
	public Selection groupSelection(@NonNull final Selection selection) {
		return groupSelection(selection.build());
	}

	/**
	 * Same as {@link #selection(String)} where the given <var>selection</var> will be appended as
	 * one group enclosed with <b>(...)</b> brackets.
	 *
	 * @param selection The desired selection to append.
	 * @return This selection to allow methods chaining.
	 */
	public Selection groupSelection(@NonNull final String selection) {
		return selection("(" + selection + ")");
	}

	/**
	 * Same as {@link #selection(String)} where the raw selection will be build via {@link Selection#build()}
	 * from the given <var>selection</var>.
	 *
	 * @param selection The desired selection to append.
	 * @return This selection to allow methods chaining.
	 */
	public Selection selection(@NonNull final Selection selection) {
		return selection(selection.build());
	}

	/**
	 * Appends a raw <var>selection</var> into the current selection data.
	 *
	 * @param selection The desired selection to append.
	 * @return This selection to allow methods chaining.
	 */
	public Selection selection(@NonNull final String selection) {
		mBuilder.append(selection);
		return this;
	}

	/**
	 * Same as {@link #groupAnd(String)} where the raw selection will be build via {@link Selection#build()}
	 * from the given <var>selection</var>.
	 *
	 * @param selection The desired selection to append as grouped AND condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection groupAnd(@NonNull final Selection selection) {
		return groupAnd(selection.build());
	}

	/**
	 * Same as {@link #and(String)} where the given <var>selection</var> will be appended as one group
	 * enclosed with <b>(...)</b> brackets.
	 *
	 * @param selection The desired selection to append as grouped AND condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection groupAnd(@NonNull final String selection) {
		return and("(" + selection + ")");
	}

	/**
	 * Same as {@link #and(String)} where the raw selection will be build via {@link Selection#build()}
	 * from the given <var>selection</var>.
	 *
	 * @param selection The desired selection to append as AND condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection and(@NonNull final Selection selection) {
		return and(selection.build());
	}

	/**
	 * Appends a raw <var>selection</var> into the current selection data as <b>AND</b> SQL condition.
	 *
	 * @param selection The desired selection to append as AND condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection and(@NonNull final String selection) {
		if (mBuilder.length() > 0) {
			mBuilder.append(" AND ");
		}
		mBuilder.append(selection);
		return this;
	}

	/**
	 * Same as {@link #groupOr(String)} where the raw selection will be build via {@link Selection#build()}
	 * from the given <var>selection</var>.
	 *
	 * @param selection The desired selection to append as grouped OR condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection groupOr(@NonNull final Selection selection) {
		return groupOr(selection.build());
	}

	/**
	 * Same as {@link #or(String)} where the given <var>selection</var> will be appended as one group
	 * enclosed with <b>(...)</b> brackets.
	 *
	 * @param selection The desired selection to append as grouped OR condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection groupOr(@NonNull final String selection) {
		return or("(" + selection + ")");
	}

	/**
	 * Same as {@link #or(String)} where the raw selection will be build via {@link Selection#build()}
	 * from the given <var>selection</var>.
	 *
	 * @param selection The desired selection to append as OR condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection or(@NonNull final Selection selection) {
		return or(selection.build());
	}

	/**
	 * Appends a raw <var>selection</var> into the current selection data as <b>OR</b> SQL condition.
	 *
	 * @param selection The desired selection to append as OR condition.
	 * @return This selection to allow methods chaining.
	 */
	public Selection or(@NonNull final String selection) {
		if (mBuilder.length() > 0) {
			mBuilder.append(" OR ");
		}
		mBuilder.append(selection);
		return this;
	}

	/**
	 * Checks whether this builder contains some selection data or not.
	 *
	 * @return {@code True} if some selection data are presented within this builder, {@code false}
	 * otherwise.
	 * @see #hasArguments()
	 */
	public boolean isEmpty() {
		return mBuilder.length() == 0;
	}

	/**
	 * Same as {@link #argument(String)} for generic Object argument.
	 *
	 * @param arg The desired argument of which string representation will be appended to this
	 *            selection's arguments.
	 * @return This selection to allow methods chaining.
	 * @see #arguments(Object...)
	 */
	public Selection argument(@NonNull final Object arg) {
		return argument(arg.toString());
	}

	/**
	 * Appends the given <var>arg</var> value into arguments of this selection.
	 *
	 * @param arg The desired argument to be appended.
	 * @return This selection to allow methods chaining.
	 * @see #arguments(String...)
	 */
	public Selection argument(@NonNull final String arg) {
		final int argumentsSize = mArguments.length;
		this.ensureArgumentsCapacity(argumentsSize + 1);
		this.mArguments[argumentsSize] = arg;
		return this;
	}

	/**
	 * Same as {@link #arguments(String...)} for generic array of Object arguments.
	 *
	 * @param args The desired array of arguments of which string representations will be appended
	 *             to this selection's arguments.
	 * @return This selection to allow methods chaining.
	 * @see #argument(Object)
	 */
	public Selection arguments(@NonNull @Size(min = 1) final Object... args) {
		return arguments(argsAsStringArray(args));
	}

	/**
	 * Appends all the given <var>args</var> values into arguments of this selection.
	 *
	 * @param args The desired arguments to be appended.
	 * @return This selection to allow methods chaining.
	 * @see #argument(String)
	 * @see #arguments()
	 */
	public Selection arguments(@NonNull final String... args) {
		final int argumentsSize = mArguments.length;
		this.ensureArgumentsCapacity(argumentsSize + args.length);
		System.arraycopy(args, 0, mArguments, argumentsSize, args.length);
		return this;
	}

	/**
	 * Ensures that the array of arguments has at least the <var>minCapacity</var>.
	 * <p>
	 * If the arguments array already has the requested capacity, this method does nothing.
	 *
	 * @param minCapacity The desired minimum capacity the arguments array should have.
	 */
	private void ensureArgumentsCapacity(int minCapacity) {
		if (minCapacity - mArguments.length > 0) expandArguments(minCapacity);
	}

	/**
	 * Expands the array of arguments so it has at least the <var>minCapacity</var>.
	 *
	 * @param minCapacity The desired minimum capacity the arguments array should have.
	 */
	private void expandArguments(int minCapacity) {
		final int oldCapacity = mArguments.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity < 0) {
			newCapacity = minCapacity;
		}
		if (newCapacity - ARGUMENTS_MAX_SIZE > 0) {
			newCapacity = minCapacity > ARGUMENTS_MAX_SIZE ? Integer.MAX_VALUE : ARGUMENTS_MAX_SIZE;
		}
		this.mArguments = Arrays.copyOf(mArguments, newCapacity);
	}

	/**
	 * Checks whether this selection has some arguments specified or not.
	 *
	 * @return {@code True} if there are some arguments added into this selection, {@code false} otherwise.
	 * @see #argument(String)
	 * @see #arguments(String...)
	 * @see #arguments()
	 */
	public boolean hasArguments() {
		return mArguments.length > 0;
	}

	/**
	 * Returns the array containing all arguments added into this selection.
	 *
	 * @return This selection arguments. If no arguments were added, this method returns an empty array.
	 * @see #argument(String)
	 * @see #arguments(String...)
	 */
	@NonNull
	public String[] arguments() {
		return mArguments;
	}

	/**
	 * Builds a selection statement composed from selection data specified for this builder instance.
	 *
	 * @return The desired selection statement.
	 * @see #selection(Selection)
	 * @see #and(Selection)
	 * @see #or(Selection)
	 * @see #groupSelection(Selection)
	 * @see #groupAnd(Selection)
	 * @see #groupOr(Selection)
	 */
	@NonNull
	public String build() {
		return mBuilder.toString();
	}

	/**
	 * Clears all selection and arguments data of this selection.
	 *
	 * @return This selection to allow methods chaining.
	 */
	public Selection clear() {
		this.mBuilder.setLength(0);
		this.mArguments = EMPTY_ARGUMENTS;
		return this;
	}
}
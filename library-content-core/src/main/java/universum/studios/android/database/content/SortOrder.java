/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A {@link LoaderArgumentBuilder} implementation that may be used to build a <b>sort order</b>
 * argument for a desired {@link android.content.CursorLoader CursorLoader}.
 * <p>
 * SortOrder builder builds a sort order statement from all column names supplied to it via either
 * {@link #column(String)} or {@link #columns(String[])}.
 *
 * <h3>Usage</h3>
 * Given the following sample inputs, SortOrder builder produces sort order statements like follows:
 * <pre>
 * 1) ==============================================================================================
 *
 * final SortOrder sortOrder = new SortOrder.ASC();
 * sortOrder.columns("title", "date");
 * sortOrder.build(); =&gt; title, date ASC
 *
 * 2) ==============================================================================================
 *
 * final SortOrder sortOrder = new SortOrder.DSC();
 * sortOrder.column("name");
 * sortOrder.build(); =&gt; name DSC
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Projection
 * @see Selection
 * @see IdSelection
 */
public class SortOrder extends LoaderArgumentBuilder<String> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SortOrder";

	/**
	 * Keyword for <b>ASCENDING</b> sort order type.
	 */
	public static final String ASC = "ASC";

	/**
	 * Keyword for <b>DESCENDING</b> sort order type.
	 */
	public static final String DESC = "DESC";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * This sort order key word.
	 */
	private final String keyWord;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of SortOrder.
	 *
	 * @param keyWord Key word for this sort order instance.
	 */
	public SortOrder(@NonNull final String keyWord) {
		super();
		this.keyWord = keyWord;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Creates a new SortOrder with {@link #ASC} keyword.
	 *
	 * @return New sort order instance which may be used to sort results in ascending order.
	 */
	@NonNull public static SortOrder asc() {
		return new SortOrder(ASC);
	}

	/**
	 * Creates a new SortOrder with {@link #DESC} keyword.
	 *
	 * @return New sort order instance which may be used to sort results in descending order.
	 */
	@NonNull public static SortOrder desc() {
		return new SortOrder(DESC);
	}

	/**
	 * Returns the key word of this sort order instance.
	 *
	 * @return Sort order key word.
	 */
	@NonNull public final String keyWord() {
		return keyWord;
	}

	/**
	 * @return This sort order to allow methods chaining.
	 */
	@Override public SortOrder column(@NonNull final String column) {
		return (SortOrder) super.column(column);
	}

	/**
	 * @return This sort order to allow methods chaining.
	 */
	@Override public SortOrder columns(@NonNull final String... columns) {
		return (SortOrder) super.columns(columns);
	}

	/**
	 * Builds a sort order statement composed from the all columns specified for this sort order
	 * instance.
	 *
	 * @return Sort order statement appended by the keyword of this sort order instance or just the
	 * keyword if there were no columns specified.
	 */
	@NonNull public String build() {
		if (columns.isEmpty()) {
			return keyWord;
		}
		final int n = columns.size();
		final StringBuilder builder = new StringBuilder(keyWord.length() + n * 10);
		for (int i = 0; i < n; i++) {
			builder.append(columns.get(i));
			if (i < (n - 1)) {
				builder.append(", ");
			}
		}
		// Append the sort/order keyword and build.
		return builder.append(" ").append(keyWord).toString();
	}

	/**
	 */
	@Override public int hashCode() {
		int hash = keyWord.hashCode();
		hash = 31 * hash + columns.hashCode();
		return hash;
	}

	/**
	 */
	@SuppressWarnings("SimplifiableIfStatement")
	@Override public boolean equals(@Nullable final Object other) {
		if (other == this) return true;
		if (!(other instanceof SortOrder)) return false;
		final SortOrder sortOrder = (SortOrder) other;
		if (!keyWord.equals(sortOrder.keyWord)) {
			return false;
		}
		return columns.equals(sortOrder.columns);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * <b>This class has been deprecated and will be removed in the next release.</b>
	 * <p>
	 * Ascending {@link SortOrder} implementation.
	 *
	 * @author Martin Albedinsky
	 *
	 * @deprecated Use {@link #asc()} instead.
	 */
	@Deprecated
	public static class ASC extends SortOrder {

		/**
		 * Key word for <b>ASCENDING</b> sort order type.
		 */
		public static final String KEY_WORD = "ASC";

		/**
		 * Creates a new instance of ASC sort order.
		 */
		public ASC() {
			super(KEY_WORD);
		}
	}

	/**
	 * <b>This class has been deprecated and will be removed in the next release.</b>
	 * <p>
	 * Descending {@link SortOrder} implementation.
	 *
	 * @author Martin Albedinsky
	 * @deprecated Use {@link #desc()} instead.
	 */
	@Deprecated
	public static class DESC extends SortOrder {

		/**
		 * Key word for <b>DESCENDING</b> sort order type.
		 */
		public static final String KEY_WORD = "DESC";

		/**
		 * Creates a new instance of DESC sort order.
		 */
		public DESC() {
			super(KEY_WORD);
		}
	}
}
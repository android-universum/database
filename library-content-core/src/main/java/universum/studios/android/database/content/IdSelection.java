/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.util.TypeUtils;

/**
 * Like {@link Selection} but this builder is meant to be used for building of selection statement
 * for a desired set of primary <b>ids</b>.
 * <p>
 * An instance of IdSelection builder needs to be created with name primary column that is then used
 * to build a selection string that can be supplied as <var>selection</var> parameter for
 * {@link android.content.CursorLoader CursorLoader}.
 *
 * <h3>Supported types of ids:</h3>
 * <ul>
 * <li>{@link #withIds(int[])}</li>
 * <li>{@link #withIds(long[])}</li>
 * <li>{@link #withIds(String[])}</li>
 * </ul>
 *
 * <h3>Usage</h3>
 * Given the following sample input, IdSelection builder produces selection + selection arguments
 * statements like follows:
 * <pre>
 * final IdSelection idSelection = new IdSelection("_id").withIds(new long[]{148L, 1426L, 2368L, 4568L});
 * idSelection.build();      =&gt; _id=? OR _id=? OR _id=? OR _id=?
 * idSelection.buildArgs();  =&gt; ["148", "1426", "2368", "4568"]
 *
 * where Android's SQLite framework will transform this into query like:
 * 'SELECT/UPDATE/DELETE ... WHERE _id=148 OR _id=1426 OR _id=2368 OR _id=4568 ...'
 * </pre>
 *
 * @author Martin Albedinsky
 * @see Projection
 * @see Selection
 * @see SortOrder
 */
public class IdSelection {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "IdSelection";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Name of the column holding id.
	 */
	private final String mColumnName;

	/**
	 * Wrapped array of supplied ids.
	 */
	private ArrayWrapper mIdsArray;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of IdSelection to build selection for the specified <var>column</var>.
	 *
	 * @param column Name of the column that will be used to build the desired ids selection.
	 */
	public IdSelection(@NonNull String column) {
		this.mColumnName = column;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Specifies a set of <b>int</b> ids for this id selection builder.
	 * <p>
	 * Any ids supplied to this builder before will be overwritten.
	 *
	 * @param ids The desired array with ids used to build the selection along with its associated
	 *            arguments.
	 * @return This builder to allow methods chaining.
	 */
	public IdSelection withIds(@NonNull int[] ids) {
		this.mIdsArray = ArrayWrapper.wrapArray(ids);
		return this;
	}

	/**
	 * Specifies a set of <b>long</b> ids for this id selection builder.
	 * <p>
	 * Any ids supplied to this builder before will be overwritten.
	 *
	 * @param ids The desired array with ids used to build the selection along with its associated
	 *            arguments.
	 * @return This builder to allow methods chaining.
	 */
	public IdSelection withIds(@NonNull long[] ids) {
		this.mIdsArray = ArrayWrapper.wrapArray(ids);
		return this;
	}

	/**
	 * Specifies a set of <b>String</b> ids for this id selection builder.
	 * <p>
	 * Any ids supplied to this builder before will be overwritten.
	 *
	 * @param ids The desired array with ids used to build the selection along with its associated
	 *            arguments.
	 * @return This builder to allow methods chaining.
	 */
	public IdSelection withIds(@NonNull String[] ids) {
		this.mIdsArray = ArrayWrapper.wrapArray(ids);
		return this;
	}

	/**
	 * Builds a selection statement composed from the primary column name and id/-s specified for
	 * this builder.
	 * <p>
	 * Sample output: {@code _id=? OR _id=? OR _id=? ...}
	 *
	 * @return Selection statement.
	 * @see #buildArgs()
	 */
	@NonNull
	public String build() {
		if (mIdsArray == null || mIdsArray.length == 0) {
			return "";
		}
		final int n = mIdsArray.length;
		final StringBuilder builder = new StringBuilder(((mColumnName.length() + 2) * n) + (4 * (n - 1)));
		for (int i = 0; i < n; i++) {
			builder.append(mColumnName);
			builder.append("=?");
			if (i < (n - 1)) {
				builder.append(" OR ");
			}
		}
		return builder.toString();
	}

	/**
	 * Builds an arguments array for the selection statement created via {@link #build()}.
	 *
	 * @return Arguments for selection statement.
	 */
	@NonNull
	public String[] buildArgs() {
		return mIdsArray == null ? new String[0] : mIdsArray.asStringArray();
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Wrapper for arrays holding primitive and boxed primitive values. This wrapper may be used to simplify
	 * conversion of a specific array into array of {@link String} representations of each item contained
	 * within such array.
	 *
	 * <h3>Supported array types:</h3>
	 * <ul>
	 * <li><b>int[]</b></li>
	 * <li><b>long[]</b></li>
	 * <li><b>String[]</b></li>
	 * </ul>
	 */
	static abstract class ArrayWrapper {

		/**
		 * Length of the wrapped array.
		 */
		final int length;

		/**
		 * Wrapped array.
		 */
		final Object array;

		/**
		 * Creates a new instance of ArrayWrapper for the specified <var>array</var> with the given
		 * <var>length</var>.
		 *
		 * @param array  The array to wrap.
		 * @param length Length of the given array.
		 */
		ArrayWrapper(Object array, int length) {
			this.array = array;
			this.length = length;
		}

		/**
		 * Wraps the specified <var>array</var> into instance of ArrayWrapper specific for type of the
		 * given array.
		 * <p>
		 * See {@link ArrayWrapper description} for supported array types.
		 *
		 * @param array The array to wrap. May be {@code null}.
		 * @return Instance of determined array wrapper.
		 * @throws IllegalArgumentException If the specified value is not an array or it is not supported
		 *                                  type of array.
		 */
		@NonNull
		static ArrayWrapper wrapArray(@Nullable Object array) {
			if (array == null) return new EmptyArrayWrapper();
			switch (TypeUtils.resolveType(array.getClass())) {
				case TypeUtils.INTEGER_ARRAY:
					return new IntArrayWrapper((int[]) array);
				case TypeUtils.LONG_ARRAY:
					return new LongArrayWrapper((long[]) array);
				case TypeUtils.STRING_ARRAY:
					return new StringArrayWrapper((String[]) array);
				default:
					throw new IllegalArgumentException("Value(" + array + ") is not an array or it is not supported type of array to wrap.");
			}
		}

		/**
		 * Converts the wrapped array to array of string representations of each item contained within
		 * the wrapped array.
		 *
		 * @return Converted string array.
		 */
		@NonNull
		public abstract String[] asStringArray();

		/**
		 * A {@link ArrayWrapper} implementation used as wrapper for {@code null} array.
		 */
		static final class EmptyArrayWrapper extends ArrayWrapper {

			/**
			 * Creates a new instance of EmptyArrayWrapper with {@code null} array and in length of {@code 0}.
			 */
			EmptyArrayWrapper() {
				super(null, 0);
			}

			/**
			 */
			@NonNull
			@Override
			public String[] asStringArray() {
				return new String[0];
			}
		}

		/**
		 * A {@link ArrayWrapper} implementation to wrap array of <b>int</b> type values.
		 */
		static final class IntArrayWrapper extends ArrayWrapper {

			/**
			 * Creates a new instance of IntArrayWrapper to wrap the given <var>array</var> of <b>ints</b>.
			 *
			 * @param array The array to wrap.
			 */
			IntArrayWrapper(int[] array) {
				super(array, array.length);
			}

			/**
			 */
			@NonNull
			@Override
			public String[] asStringArray() {
				final int[] wrappedArray = (int[]) array;
				final String[] stringArray = new String[length];
				for (int i = 0; i < length; i++) {
					stringArray[i] = Integer.toString(wrappedArray[i]);
				}
				return stringArray;
			}
		}

		/**
		 * A {@link ArrayWrapper} implementation to wrap array of <b>long</b> type values.
		 */
		static final class LongArrayWrapper extends ArrayWrapper {

			/**
			 * Creates a new instance of LongArrayWrapper to wrap the given <var>array</var> of <b>longs</b>.
			 *
			 * @param array The array to wrap.
			 */
			LongArrayWrapper(long[] array) {
				super(array, array.length);
			}

			/**
			 */
			@NonNull
			@Override
			public String[] asStringArray() {
				final long[] wrappedArray = (long[]) array;
				final String[] stringArray = new String[length];
				for (int i = 0; i < length; i++) {
					stringArray[i] = Long.toString(wrappedArray[i]);
				}
				return stringArray;
			}
		}

		/**
		 * A {@link ArrayWrapper} implementation to wrap array of <b>String</b> type values.
		 */
		static final class StringArrayWrapper extends ArrayWrapper {

			/**
			 * Creates a new instance of StringArrayWrapper to wrap the given <var>array</var> of <b>Strings</b>.
			 *
			 * @param array The array to wrap.
			 */
			StringArrayWrapper(String[] array) {
				super(array, array.length);
			}

			/**
			 */
			@NonNull
			@Override
			@SuppressWarnings("RedundantCast")
			public String[] asStringArray() {
				final String[] stringArray = new String[length];
				System.arraycopy((String[]) array, 0, stringArray, 0, length);
				return stringArray;
			}
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;

/**
 * A {@link LoaderArgumentBuilder} implementation that may be used to build a <b>projection</b>
 * argument for a desired {@link android.content.CursorLoader CursorLoader}.
 * <p>
 * Projection builder is a very simple builder that builds a projection set from all column names
 * supplied to it via either {@link #column(String)} or {@link #columns(String[])}.
 *
 * <h3>Usage</h3>
 * Given the following sample inputs, Projection builder produces projection set like follows:
 * <pre>
 * final Projection projection = new Projection();
 * projection.column("title").column("date").column("is_favorite");
 * projection.build(); =&gt; ["title", "date", "is_favorite"]
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see SortOrder
 * @see Selection
 * @see IdSelection
 */
public class Projection extends LoaderArgumentBuilder<String> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "Projection";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Same as {@link #column(String)} where the specified <var>column</var> will be built via
	 * {@link Column#build()}.
	 *
	 * @param column The desired column.
	 * @return This builder to allow methods chaining.
	 */
	public Projection column(@NonNull final Column column) {
		return column(column.build());
	}

	/**
	 */
	@Override public Projection column(@NonNull final String column) {
		return (Projection) super.column(column);
	}

	/**
	 */
	@Override public Projection columns(@NonNull @Size(min = 1) final String... columns) {
		return (Projection) super.columns(columns);
	}

	/**
	 * Builds a projection set composed from all columns specified for this builder.
	 *
	 * @return Projection set containing column names.
	 *
	 * @see #column(Column)
	 * @see #column(String)
	 * @see #columns(String...)
	 */
	@NonNull public String[] build() {
		return columns.toArray(new String[0]);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A Column represents a simple builder that may be used to build a single column for {@link Projection}.
	 *
	 * @author Martin Albedinsky
	 * @since 1.0
	 */
	public static class Column {

		/**
		 * Name of this column as it is presented within database table.
		 */
		public String name;

		/**
		 * Table name where is this column presented.
		 */
		public String from;

		/**
		 * Name of this column as it should be presented within a queried Cursor.
		 */
		public String as;

		/**
		 * Creates a new instance of Column with the specified <var>name</var>.
		 *
		 * @param name The desired column name.
		 *
		 * @see #build()
		 */
		public Column(@NonNull final String name) {
			this.name = name;
		}

		/**
		 * Specifies a table name where is this column presented.
		 *
		 * @param from The table name. If {@code null} or empty only name of this column specified
		 *             during initialization will be used when building this column.
		 * @return This column to allow methods chaining.
		 *
		 * @see #build()
		 * @see #as(String)
		 */
		public Column from(@Nullable final String from) {
			this.from = from;
			return this;
		}

		/**
		 * Specifies an alias for this column.
		 *
		 * @param as The desired alias to be used in projection statement in association with
		 *           <b>AS</b> keyword. Can be {@code null} or empty to not use alias for this column.
		 * @return This column to allow methods chaining.
		 *
		 * @see #build()
		 * @see #as(String)
		 */
		public Column as(@Nullable final String as) {
			this.as = as;
			return this;
		}

		/**
		 * Builds this column into string statement that can be used within a desired projection.
		 * <p>
		 * Sample projection column statements:
		 * <ul>
		 * <li>name</li>
		 * <li>customers.name</li>
		 * <li>customers.name <b>AS</b> customer_name</li>
		 * </ul>
		 *
		 * @return Projection statement of this column composed of its name prefixed with table name
		 * (if specified) and appended with AS keyword (also if specified).
		 * @throws IllegalArgumentException If this column does not have its name specified.
		 *
		 * @see #from(String)
		 * @see #as(String)
		 */
		@NonNull public String build() {
			if (name == null || name.length() == 0) {
				throw new IllegalArgumentException("No column name specified!");
			}
			final StringBuilder builder = new StringBuilder(64);
			if (from != null && from.length() > 0) {
				builder.append(from);
				builder.append(".");
			}
			builder.append(name);
			if (as != null && as.length() > 0) {
				builder.append(" AS ");
				builder.append(as);
			}
			return builder.toString();
		}
	}
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.content;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * LoaderArgumentBuilder represents a base class for all builders that accept set of a specific
 * types of columns, {@link String} in most cases, that are then used to build a statement
 * (projection, sort, ...) from those columns to be used when creating an instance of desired
 * {@link android.content.CursorLoader CursorLoader}.
 *
 * @param <T> Type of the column that is to be supplied to the builder.
 * @author Martin Albedinsky
 */
abstract class LoaderArgumentBuilder<T> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "LoaderArgumentBuilder";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * List of columns specified for this builder.
	 */
	final List<T> columns = new ArrayList<>(5);

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Appends the specified <var>column</var> into the current columns.
	 *
	 * @param column The desired column to append.
	 * @return This builder to allow methods chaining.
	 */
	public LoaderArgumentBuilder column(@NonNull T column) {
		columns.add(column);
		return this;
	}

	/**
	 * Appends the specified <var>columns</var> into the current columns.
	 *
	 * @param columns The desired columns to append.
	 * @return This builder to allow methods chaining.
	 */
	@SuppressWarnings("unchecked")
	public LoaderArgumentBuilder columns(@NonNull T... columns) {
		this.columns.addAll(Arrays.asList(columns));
		return this;
	}

	/**
	 * Returns the current columns of this builder.
	 *
	 * @return List of columns that has been supplied to this builder.
	 * @see #column(Object)
	 * @see #columns(Object[])
	 * @see #isEmpty()
	 */
	@NonNull
	public final List<T> columns() {
		return columns;
	}

	/**
	 * Checks whether this builder contains some columns or not.
	 *
	 * @return {@code True} if some columns are presented within this builder, {@code false} otherwise.
	 * @see #columns()
	 */
	public final boolean isEmpty() {
		return columns.isEmpty();
	}

	/**
	 * Clears all columns supplied to this builder.
	 *
	 * @return This builder to allow methods chaining.
	 */
	public LoaderArgumentBuilder clear() {
		columns.clear();
		return this;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
Database-Content-Core
===============

This module contains core elements that may be used when working with or targeting content stored
in `SQLite` database.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-content-core:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Projection](https://bitbucket.org/android-universum/database/src/main/library-content-core/src/main/java/universum/studios/android/database/content/Projection.java)
- [Selection](https://bitbucket.org/android-universum/database/src/main/library-content-core/src/main/java/universum/studios/android/database/content/Selection.java)
- [SortOrder](https://bitbucket.org/android-universum/database/src/main/library-content-core/src/main/java/universum/studios/android/database/content/SortOrder.java)
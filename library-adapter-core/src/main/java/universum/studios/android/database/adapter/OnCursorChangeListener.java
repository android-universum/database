/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Listener which receives callbacks that may be used to listen for events before and after cursor
 * change for a specific adapter.
 * <p>
 * This listener may be useful for example when there need to be saved and restored current scroll
 * position of a collection view for which a specific adapter provides its data set. Then in such
 * scenario we can save the current scroll position in {@link #onCursorChange(Object, Cursor)} and
 * restore it in {@link #onCursorChanged(Object, Cursor)}.
 * <p>
 * Both these callbacks should be guarantied to be called by the associated adapter if this listener
 * is attached to it.
 *
 * @param <A> Type of the adapter to which will be this data change listener attached.
 * @author Martin Albedinsky
 */
public interface OnCursorChangeListener<A> {

	/**
	 * Invoked whenever the specified <var>cursor</var> is about to be changed in the given <var>adapter</var>.
	 * <p>
	 * During duration of this call, the adapter still references the previous cursor.
	 *
	 * @param adapter The adapter of which cursor is about to be changed.
	 * @param cursor  The cursor to be changed. May be {@code null}.
	 */
	void onCursorChange(@NonNull A adapter, @Nullable Cursor cursor);

	/**
	 * Invoked after {@link #onCursorChange(Object, Cursor)} callback has been fired and cursor change
	 * has been finished.
	 * <p>
	 * When this callback is fired, the adapter already references the changed cursor.
	 *
	 * @param adapter The adapter where the cursor has been changed.
	 * @param cursor  The changed cursor. May be {@code null}.
	 */
	void onCursorChanged(@NonNull A adapter, @Nullable Cursor cursor);
}
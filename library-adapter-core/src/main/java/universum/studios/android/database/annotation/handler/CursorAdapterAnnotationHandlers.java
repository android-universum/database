/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import android.widget.BaseAdapter;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.annotation.CursorItemView;

/**
 * An {@link AnnotationHandlers} implementation providing {@link AnnotationHandler} instances for
 * <b>CursorAdapter</b> like classes.
 *
 * @author Martin Albedinsky
 */
@SuppressWarnings("unused")
public class CursorAdapterAnnotationHandlers extends AnnotationHandlers {

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	CursorAdapterAnnotationHandlers() {
		super();
		// Creation of instances of this class is not publicly allowed.
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Obtains a {@link CursorAdapterAnnotationHandler} implementation for the given <var>classOfAdapter</var>.
	 *
	 * @see AnnotationHandlers#obtainHandler(Class, Class)
	 */
	@Nullable
	public static CursorAdapterAnnotationHandler obtainCursorAdapterHandler(@NonNull Class<?> classOfAdapter) {
		return obtainHandler(AdapterHandler.class, classOfAdapter);
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * A {@link CursorAdapterAnnotationHandler} implementation for {@code BaseCursorAdapter} like
	 * adapters.
	 */
	static class AdapterHandler extends BaseAnnotationHandler implements CursorAdapterAnnotationHandler {

		/**
		 * Layout resource of the related adapter's item view obtained from the annotated class.
		 */
		private final int itemViewRes;

		/**
		 * Same as {@link #AdapterHandler(Class, Class)} with {@link BaseAdapter} as <var>maxSuperClass</var>.
		 */
		public AdapterHandler(@NonNull Class<?> annotatedClass) {
			this(annotatedClass, BaseAdapter.class);
		}

		/**
		 * Creates a new instance of AdapterHandler for the specified <var>annotatedClass</var>.
		 *
		 * @see BaseAnnotationHandler#BaseAnnotationHandler(Class, Class)
		 */
		AdapterHandler(Class<?> annotatedClass, Class<?> maxSuperClass) {
			super(annotatedClass, maxSuperClass);
			final CursorItemView itemView = findAnnotationRecursive(CursorItemView.class);
			this.itemViewRes = itemView == null ? NO_VIEW_RES : itemView.value();
		}

		/**
		 */
		@Override
		@LayoutRes
		public int getItemViewRes(@LayoutRes int defaultViewRes) {
			return itemViewRes == NO_VIEW_RES ? defaultViewRes : itemViewRes;
		}
	}
}
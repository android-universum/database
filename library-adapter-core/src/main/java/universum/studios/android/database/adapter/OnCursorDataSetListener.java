/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.loader.content.Loader;

/**
 * Listener which receives callbacks about loaded, changed and invalidated adapter's data set.
 *
 * @param <A> Type of the Adapter to which will be this data set listener attached.
 * @author Martin Albedinsky
 */
public interface OnCursorDataSetListener<A> {

	/**
	 * Invoked whenever the current cursor data set within the passed <var>adapter</var> was loaded
	 * via {@link Loader} with the specified <var>loaderId</var>.
	 *
	 * @param adapter  The adapter of which current data set was just loaded.
	 * @param loaderId Id of the loader that loaded new data set for the adapter.
	 */
	void onCursorDataSetLoaded(@NonNull A adapter, @IntRange(from = 0) int loaderId);

	/**
	 * Invoked whenever the current cursor data set within the passed <var>adapter</var> was changed.
	 *
	 * @param adapter The adapter of which current data set was just changed.
	 */
	void onCursorDataSetChanged(@NonNull A adapter);

	/**
	 * Invoked whenever the current cursor data set within the passed <var>adapter</var> was invalidated.
	 *
	 * @param adapter The adapter of which current data set was just invalidated.
	 */
	void onCursorDataSetInvalidated(@NonNull A adapter);
}
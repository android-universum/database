/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Listener which receives callback about selected action within adapter's data set for a specific
 * position.
 *
 * @param <A> Type of the Adapter to which will be this data set action listener attached.
 * @author Martin Albedinsky
 */
public interface OnCursorDataSetActionListener<A> {

	/**
	 * Invoked whenever the specified <var>action</var> was selected for the specified <var>position</var>
	 * within the passed <var>adapter</var> in which is this callback registered.
	 *
	 * @param adapter  The adapter in which was the specified action selected.
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param id       An id of the item at the specified position within the current data set of the
	 *                 passed adapter.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled, {@code false} otherwise.
	 */
	boolean onCursorDataSetActionSelected(@NonNull A adapter, int action, int position, long id, @Nullable Object payload);
}
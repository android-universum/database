/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.database.Cursor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.annotation.Column;
import universum.studios.android.database.cursor.ItemCursor;

/**
 * A {@link CursorDataSet} implementation that may be used to wrap {@link Cursor} and use it as data
 * set in adapters.
 *
 * @param <A> Type of the adapter where this data set will be used.
 * @param <C> Type of the cursor that provides data of this data set.
 * @param <I> Item model that represents data structure of the data provided by the cursor.
 * @author Martin Albedinsky
 */
final class CursorAdapterDataSet<A extends CursorDataSetAdapter<C, I>, C extends Cursor, I> implements CursorDataSet<C, I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CursorAdapterDataSet";

	/**
	 * Constant that identifies invalid/unspecified index.
	 */
	static final int NO_INDEX = -1;

	/**
	 * Flag used to identify {@link OnCursorChangeListener}.
	 */
	static final int LISTENER_CHANGE = 0x00000001;

	/**
	 * Flag used to identify {@link OnCursorDataSetListener}.
	 */
	static final int LISTENER_DATA_SET = 0x00000001 << 1;

	/**
	 * Flag used to identify {@link OnCursorDataSetActionListener}.
	 */
	static final int LISTENER_DATA_SET_ACTION = 0x00000001 << 2;

	/**
	 * Defines an annotation for determining set of flags that may be used for listeners related
	 * flagging.
	 */
	@IntDef(flag = true, value = {
			LISTENER_CHANGE,
			LISTENER_DATA_SET,
			LISTENER_DATA_SET_ACTION
	})
	@Retention(RetentionPolicy.SOURCE)
	@interface Listener {
	}

	/**
	 * Flag grouping all listener flags defined by {@link Listener @Listener} annotation.
	 */
	private static final int LISTENER_ALL = LISTENER_CHANGE | LISTENER_DATA_SET | LISTENER_DATA_SET_ACTION;

	/**
	 * Flag used to identify {@link OnCursorChangeListener#onCursorChange(Object, Cursor)} callback.
	 */
	static final int CALLBACK_CURSOR_CHANGE = 0x00000001;

	/**
	 * Flag used to identify {@link OnCursorChangeListener#onCursorChanged(Object, Cursor)} callback.
	 */
	static final int CALLBACK_CURSOR_CHANGED = 0x00000001 << 1;

	/**
	 * Flag used to identify {@link OnCursorDataSetListener#onCursorDataSetLoaded(Object, int)} callback.
	 */
	static final int CALLBACK_CURSOR_DATA_SET_LOADED = 0x00000001 << 2;

	/**
	 * Flag used to identify {@link OnCursorDataSetListener#onCursorDataSetChanged(Object)} callback.
	 */
	static final int CALLBACK_CURSOR_DATA_SET_CHANGED = 0x00000001 << 3;

	/**
	 * Flag used to identify {@link OnCursorDataSetListener#onCursorDataSetInvalidated(Object)} callback.
	 */
	static final int CALLBACK_CURSOR_DATA_SET_INVALIDATED = 0x00000001 << 4;

	/**
	 * Flag used to identify {@link OnCursorDataSetActionListener#onCursorDataSetActionSelected(Object, int, int, long, Object)}
	 * callback.
	 */
	static final int CALLBACK_CURSOR_DATA_SET_ACTION_SELECTED = 0x00000001 << 5;

	/**
	 * Defines an annotation for determining set of flags that may be used for listener callbacks
	 * related flagging.
	 */
	@IntDef(flag = true, value = {
			CALLBACK_CURSOR_CHANGE,
			CALLBACK_CURSOR_CHANGED,
			CALLBACK_CURSOR_DATA_SET_LOADED,
			CALLBACK_CURSOR_DATA_SET_CHANGED,
			CALLBACK_CURSOR_DATA_SET_INVALIDATED,
			CALLBACK_CURSOR_DATA_SET_ACTION_SELECTED
	})
	@Retention(RetentionPolicy.SOURCE)
	@interface ListenerCallback {
	}

	/**
	 * Flag grouping all listener callback flags defined by {@link ListenerCallback @ListenerCallback}
	 * annotation.
	 */
	private static final int CALLBACK_ALL = CALLBACK_CURSOR_CHANGE | CALLBACK_CURSOR_CHANGED |
			CALLBACK_CURSOR_DATA_SET_LOADED | CALLBACK_CURSOR_DATA_SET_CHANGED | CALLBACK_CURSOR_DATA_SET_INVALIDATED |
			CALLBACK_CURSOR_DATA_SET_ACTION_SELECTED;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Adapter to which is this data set attached.
	 */
	private final A mAdapter;

	/**
	 * List with registered cursor change listeners.
	 */
	private List<OnCursorChangeListener> mCursorChangeListeners;

	/**
	 * List with registered cursor data set listeners.
	 */
	private List<OnCursorDataSetListener> mCursorDataSetListeners;

	/**
	 * List with registered cursor data set action listeners.
	 */
	private List<OnCursorDataSetActionListener> mCursorDataSetActionListeners;

	/**
	 * Cursor attached to this data set.
	 */
	private C mCursor;

	/**
	 * Index of column that holds primary key within the current cursor (if any). This index is used
	 * to obtain an id for a specific position from the cursor whenever {@link #getItemId(int)} is
	 * called.
	 */
	private int mIdColumnIndex = NO_INDEX;

	/**
	 * Listener flags determining which listeners are enabled to be notified.
	 */
	private int mEnabledListeners = LISTENER_ALL;

	/**
	 * Listener callback flags determining which listener callbacks are enabled to be notified.
	 */
	private int mEnabledListenerCallbacks = CALLBACK_ALL;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of CursorAdapterDataSet for the given <var>adapter</var>.
	 *
	 * @param adapter The adapter where the new data set will be used.
	 */
	CursorAdapterDataSet(A adapter) {
		this.mAdapter = adapter;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets whether callbacks for listeners with the specified <var>listeners</var> flags registered
	 * upon this data set should be notified or not.
	 *
	 * @param listeners The desired listener flags. One of flags defined by {@link Listener @Listener}
	 *                  annotation or theirs combination.
	 * @param enabled   {@code True} to enable listener notifications, {@code false} to disable them.
	 */
	void setListenersEnabled(@Listener int listeners, boolean enabled) {
		if (enabled) this.mEnabledListeners |= listeners;
		else this.mEnabledListeners &= ~listeners;
	}

	/**
	 * Sets whether a particular callback with the specified <var>listenerCallback</var> flags for
	 * listeners registered upon this data set should be notified or not.
	 *
	 * @param listenerCallback The desired listener callback flags. One of flags defined by
	 *                         {@link ListenerCallback @ListenerCallback} annotation or theirs combination.
	 * @param enabled          {@code True} to enable listener callback notification, {@code false}
	 *                         to disable it.
	 */
	void setListenerCallbacksEnabled(@ListenerCallback int listenerCallback, boolean enabled) {
		if (enabled) this.mEnabledListenerCallbacks |= listenerCallback;
		else this.mEnabledListenerCallbacks &= ~listenerCallback;
	}

	/**
	 */
	@Override
	public void registerOnCursorChangeListener(@NonNull OnCursorChangeListener listener) {
		if (mCursorChangeListeners == null) this.mCursorChangeListeners = new ArrayList<>(1);
		if (!mCursorChangeListeners.contains(listener)) this.mCursorChangeListeners.add(listener);
	}

	/**
	 * Notifies registered {@link OnCursorChangeListener OnCursorChangeListeners} that the given
	 * <var>cursor</var> is about to be changed for this data set.
	 *
	 * @param cursor The cursor that is about to be changed for this data set.
	 * @see #notifyCursorChanged(Cursor)
	 */
	@SuppressWarnings("unchecked")
	void notifyCursorChange(Cursor cursor) {
		if ((mEnabledListeners & LISTENER_CHANGE) == 0 || (mEnabledListenerCallbacks & CALLBACK_CURSOR_CHANGE) == 0) {
			return;
		}
		if (mCursorChangeListeners != null && !mCursorChangeListeners.isEmpty()) {
			for (final OnCursorChangeListener listener : mCursorChangeListeners) {
				listener.onCursorChange(mAdapter, cursor);
			}
		}
	}

	/**
	 * Notifies registered {@link OnCursorChangeListener OnCursorChangeListeners} that the given
	 * <var>cursor</var> has been changed for this data set.
	 *
	 * @param cursor The cursor that has been changed for this data set.
	 * @see #notifyCursorChange(Cursor)
	 */
	@SuppressWarnings("unchecked")
	void notifyCursorChanged(Cursor cursor) {
		if ((mEnabledListeners & LISTENER_CHANGE) == 0 || (mEnabledListenerCallbacks & CALLBACK_CURSOR_CHANGED) == 0) {
			return;
		}
		if (mCursorChangeListeners != null && !mCursorChangeListeners.isEmpty()) {
			for (final OnCursorChangeListener listener : mCursorChangeListeners) {
				listener.onCursorChanged(mAdapter, cursor);
			}
		}
	}

	/**
	 */
	@Override
	public void unregisterOnCursorChangeListener(@NonNull OnCursorChangeListener listener) {
		if (mCursorChangeListeners != null) this.mCursorChangeListeners.remove(listener);
	}

	/**
	 */
	@Override
	public void registerOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener) {
		if (mCursorDataSetListeners == null) this.mCursorDataSetListeners = new ArrayList<>(1);
		if (!mCursorDataSetListeners.contains(listener)) this.mCursorDataSetListeners.add(listener);
	}

	/**
	 * Notifies registered {@link OnCursorDataSetListener OnCursorDataSetListeners} the data for this
	 * data set has been loaded.
	 *
	 * loaderId Id of the loader that loaded the data set.
	 *
	 * @see #notifyCursorDataSetChanged()
	 * @see #notifyCursorDataSetInvalidated()
	 */
	@SuppressWarnings("unchecked")
	void notifyCursorDataSetLoaded(int loaderId) {
		if ((mEnabledListeners & LISTENER_DATA_SET) == 0 || (mEnabledListenerCallbacks & CALLBACK_CURSOR_DATA_SET_LOADED) == 0) {
			return;
		}
		if (mCursorDataSetListeners != null && !mCursorDataSetListeners.isEmpty()) {
			for (final OnCursorDataSetListener listener : mCursorDataSetListeners) {
				listener.onCursorDataSetLoaded(mAdapter, loaderId);
			}
		}
	}

	/**
	 * Notifies registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that data of this
	 * data set has been changed.
	 *
	 * @see #notifyCursorDataSetLoaded(int)
	 * @see #notifyCursorDataSetInvalidated()
	 */
	@SuppressWarnings("unchecked")
	void notifyCursorDataSetChanged() {
		if ((mEnabledListeners & LISTENER_DATA_SET) == 0 || (mEnabledListenerCallbacks & CALLBACK_CURSOR_DATA_SET_CHANGED) == 0) {
			return;
		}
		if (mCursorDataSetListeners != null && !mCursorDataSetListeners.isEmpty()) {
			for (final OnCursorDataSetListener listener : mCursorDataSetListeners) {
				listener.onCursorDataSetChanged(mAdapter);
			}
		}
	}

	/**
	 * Notifies registered {@link OnCursorDataSetListener OnCursorDataSetListeners} that data of this
	 * data set has been invalidated.
	 *
	 * @see #notifyCursorDataSetLoaded(int)
	 * @see #notifyCursorDataSetChanged()
	 */
	@SuppressWarnings("unchecked")
	void notifyCursorDataSetInvalidated() {
		if ((mEnabledListeners & LISTENER_DATA_SET) == 0 || (mEnabledListenerCallbacks & CALLBACK_CURSOR_DATA_SET_INVALIDATED) == 0) {
			return;
		}
		if (mCursorDataSetListeners != null && !mCursorDataSetListeners.isEmpty()) {
			for (final OnCursorDataSetListener listener : mCursorDataSetListeners) {
				listener.onCursorDataSetInvalidated(mAdapter);
			}
		}
	}

	/**
	 */
	@Override
	public void unregisterOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener) {
		if (mCursorDataSetListeners != null) this.mCursorDataSetListeners.remove(listener);
	}

	/**
	 */
	@Override
	public void registerOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener) {
		if (mCursorDataSetActionListeners == null) this.mCursorDataSetActionListeners = new ArrayList<>(1);
		if (!mCursorDataSetActionListeners.contains(listener)) this.mCursorDataSetActionListeners.add(listener);
	}

	/**
	 * Notifies registered {@link OnCursorDataSetActionListener OnCursorDataSetActionListeners} that
	 * the specified <var>action</var> has been selected in this data set.
	 *
	 * @param action   The action that was selected.
	 * @param position The position for which was the specified action selected.
	 * @param id       Id of the item for which has been the action selected.
	 * @param payload  Additional payload data for the selected action. May be {@code null} if no
	 *                 payload has been specified.
	 * @return {@code True} if the action has been handled by one of the registered listeners,
	 * {@code false} otherwise.
	 */
	@SuppressWarnings("unchecked")
	boolean notifyCursorDataSetActionSelected(int action, int position, long id, Object payload) {
		if ((mEnabledListeners & LISTENER_DATA_SET_ACTION) == 0 || (mEnabledListenerCallbacks & CALLBACK_CURSOR_DATA_SET_ACTION_SELECTED) == 0) {
			return false;
		}
		if (mCursorDataSetActionListeners != null && !mCursorDataSetActionListeners.isEmpty()) {
			for (final OnCursorDataSetActionListener listener : mCursorDataSetActionListeners) {
				if (listener.onCursorDataSetActionSelected(mAdapter, action, position, id, payload)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 */
	@Override
	public void unregisterOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener) {
		if (mCursorDataSetActionListeners != null) this.mCursorDataSetActionListeners.remove(listener);
	}

	/**
	 * Attaches the given <var>cursor</var> to this data set and returns the old cursor that has been
	 * attached.
	 *
	 * @param cursor The desired cursor to attach. May be {@code null}.
	 * @return Old cursor that has been attached to this data set before or {@code null} if there
	 * was no cursor attached.
	 * @see #isCursorAvailable()
	 * @see #getCursor()
	 */
	C attachCursor(C cursor) {
		final C oldCursor = mCursor;
		this.mCursor = cursor;
		if (mCursor == null || mCursor.isClosed()) {
			this.mIdColumnIndex = NO_INDEX;
		} else {
			this.mIdColumnIndex = mCursor.getColumnIndexOrThrow(Column.Primary.COLUMN_NAME);
		}
		return oldCursor;
	}

	/**
	 * Returns the index of the id column of the cursor attached to this data set.
	 *
	 * @return Column index or {@link #NO_INDEX} if there is no cursor attached.
	 * @see #isCursorAvailable()
	 */
	int getIdColumnIndex() {
		return mIdColumnIndex;
	}

	/**
	 * @see #attachCursor(Cursor)
	 */
	@Override
	public boolean isCursorAvailable() {
		return mCursor != null && !mCursor.isClosed();
	}

	/**
	 */
	@Nullable
	@Override
	public C getCursorAt(int position) {
		if (isCursorAvailable()) {
			final int rowsCount = mCursor.getCount();
			if (position < 0 || position >= rowsCount) {
				throw new IndexOutOfBoundsException(
						"Requested cursor at invalid position(" + position + "). " +
								"Cursor has rows in count of(" + rowsCount + ")."
				);
			}
			return mCursor.moveToPosition(position) ? mCursor : null;
		}
		return null;
	}

	/**
	 */
	@Nullable
	@Override
	public C getCursor() {
		return mCursor;
	}

	/**
	 */
	@Override
	public boolean isEmpty() {
		return getItemCount() == 0;
	}

	/**
	 */
	@Override
	public int getItemCount() {
		return isCursorAvailable() ? mCursor.getCount() : 0;
	}

	/**
	 */
	@Override
	public boolean hasItemAt(int position) {
		return position >= 0 && position < getItemCount();
	}

	/**
	 */
	@NonNull
	@Override
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	public I getItem(int position) {
		if (!hasItemAt(position)) {
			throw new IndexOutOfBoundsException(
					"Requested item at invalid position(" + position + "). " +
							"Data set has items in count of(" + getItemCount() + ")."
			);
		}
		final String adapterName = mAdapter.getClass().getSimpleName();
		final C cursor = getCursorAt(position);
		if (!(cursor instanceof ItemCursor)) {
			throw DatabaseException.misconfiguration(
					"Adapter(" + adapterName + ") does not have its attached cursor type of ItemCursor. " +
							"Implementation of " + adapterName + ".getItem(int) is required."
			);
		}
		final I item = ((ItemCursor<I>) cursor).getItem();
		if (item == null) {
			final String cursorName = cursor.getClass().getSimpleName();
			throw new IllegalArgumentException(
					"ItemCursor(" + cursorName + ") attached to adapter(" + adapterName + ")" +
							" returned null item for position(" + position + ")."
			);
		}
		return item;
	}

	/**
	 */
	@Override
	public long getItemId(int position) {
		return hasItemAt(position) && mCursor.moveToPosition(position) ? mCursor.getLong(mIdColumnIndex) : NO_ID;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseException;
import universum.studios.android.database.cursor.ItemCursor;

/**
 * Interface specifying API for data sets that provide theirs data from an attached {@link Cursor}.
 *
 * @param <C> Type of the cursor that provides data of this data set.
 * @param <I> Item model that represents data structure of the data provided by the cursor.
 * @author Martin Albedinsky
 */
public interface CursorDataSet<C extends Cursor, I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Constant that identifies invalid/unspecified position.
	 */
	int NO_POSITION = -1;

	/**
	 * Constant that identifies invalid/unspecified id.
	 */
	long NO_ID = -1;

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Registers a callback to be invoked when a cursor change occurs in this data set.
	 *
	 * @param listener The desired listener callback to register.
	 * @see #unregisterOnCursorChangeListener(OnCursorChangeListener)
	 */
	void registerOnCursorChangeListener(@NonNull OnCursorChangeListener listener);

	/**
	 * Unregisters the given callback from the cursor change listeners, so it will not receive any
	 * callbacks further.
	 *
	 * @param listener The desired listener callback to unregister.
	 * @see #registerOnCursorChangeListener(OnCursorChangeListener)
	 */
	void unregisterOnCursorChangeListener(@NonNull OnCursorChangeListener listener);

	/**
	 * Registers a callback to be invoked when a cursor data set event occurs. The occurred event
	 * may be a load of the data set, a local change in the data set, or an invalidation of the data
	 * set.
	 *
	 * @param listener The desired listener callback to register.
	 * @see #unregisterOnCursorDataSetListener(OnCursorDataSetListener)
	 */
	void registerOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener);

	/**
	 * Unregisters the given callback from the data set listeners, so it will not receive any
	 * callbacks further.
	 *
	 * @param listener The desired listener callback to unregister.
	 * @see #registerOnCursorDataSetListener(OnCursorDataSetListener)
	 */
	void unregisterOnCursorDataSetListener(@NonNull OnCursorDataSetListener listener);

	/**
	 * Registers a callback to be invoked when a specific cursor data set action is selected within
	 * this data set.
	 *
	 * @param listener The desired listener callback to register.
	 * @see #unregisterOnCursorDataSetActionListener(OnCursorDataSetActionListener)
	 */
	void registerOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener);

	/**
	 * Unregisters the given callback from the data set action listeners, so it will not receive any
	 * callbacks further.
	 *
	 * @param listener The desired listener callback to unregister.
	 * @see #registerOnCursorDataSetActionListener(OnCursorDataSetActionListener)
	 */
	void unregisterOnCursorDataSetActionListener(@NonNull OnCursorDataSetActionListener listener);

	/**
	 * Checks whether the cursor attached to this data set is available or not.
	 *
	 * @return {@code True} if cursor is available and may be used, {@code false} if there is no
	 * cursor attached or it is already closed.
	 * @see Cursor#isClosed()
	 */
	boolean isCursorAvailable();

	/**
	 * Like {@link #getCursor()} but this implementation will also move the current cursor to the
	 * requested position (if possible).
	 *
	 * @param position The position to which to move the cursor.
	 * @return Current cursor moved to the specified position or {@code null} if there is no cursor
	 * specified or the current one cannot be moved to the requested position.
	 * @throws IndexOutOfBoundsException If the specified position is out of bounds of the current
	 *                                   cursor attached to this data set.
	 * @see #getCursor()
	 */
	@Nullable
	C getCursorAt(int position);

	/**
	 * Returns the current cursor attached to this data set.
	 *
	 * @return Current cursor or {@code null} if there was no cursor attached yet.
	 * @see #getCursorAt(int)
	 */
	@Nullable
	C getCursor();

	/**
	 * Returns a boolean flag indicating whether this data set is empty or not.
	 *
	 * @return {@code True} if cursor attached to this data set has no rows or it is {@code null},
	 * {@code false} otherwise.
	 * @see #getItemCount()
	 */
	boolean isEmpty();

	/**
	 * Returns the count of items available within this data set.
	 *
	 * @return Count of rows of the attached cursor or {@code 0} if no cursor is attached or it has
	 * no rows.
	 * @see #isEmpty()
	 * @see #hasItemAt(int)
	 * @see #getItem(int)
	 */
	int getItemCount();

	/**
	 * Returns a boolean flag indicating whether this data set has item that can provide data for the
	 * specified <var>position</var> or not.
	 *
	 * @param position The position of item to check.
	 * @return {@code True} if {@link #getItem(int)} can be called 'safely', {@code false} otherwise.
	 */
	boolean hasItemAt(int position);

	/**
	 * Returns the item (model) containing data from this data set for the specified <var>position</var>.
	 *
	 * @param position Position of the item to obtain.
	 * @return Item bound with data from the current attached cursor for the requested position.
	 * @throws IndexOutOfBoundsException If the specified position is out of bounds of the current
	 *                                   cursor attached to this data set.
	 * @throws DatabaseException         Type of {@link DatabaseException#TYPE_MISCONFIGURATION} if
	 *                                   the current attached cursor is not type of {@link ItemCursor}
	 *                                   and so we have no information about how to obtain the item
	 *                                   for the requested position.
	 * @see #hasItemAt(int)
	 * @see #getItemId(int)
	 */
	@NonNull
	I getItem(int position);

	/**
	 * Returns the stable ID of the item at the specified <var>position</var>.
	 *
	 * @param position The position of item of which id to obtain.
	 * @return Item's id or {@link #NO_ID} if there is no item at the specified position.
	 * @see #hasItemAt(int)
	 * @see #getItem(int)
	 */
	long getItemId(int position);
}
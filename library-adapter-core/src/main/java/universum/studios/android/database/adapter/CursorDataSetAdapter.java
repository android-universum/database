/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.database.Cursor;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.database.DatabaseException;

/**
 * A convenience interface specifying common API for adapters that hold a {@link Cursor} as theirs
 * data set.
 *
 * @param <C> Type of the cursor data set that can be attached to the adapter.
 * @param <I> Type of item model that represents data structure of the adapter's data set.
 * @author Martin Albedinsky
 */
public interface CursorDataSetAdapter<C extends Cursor, I> extends CursorDataSet<C, I> {

	/**
	 * Like {@link #swapCursor(Cursor)} but this implementation will close the old cursor (if any).
	 *
	 * @param cursor The cursor to provide a new data set for this adapter.
	 */
	void changeCursor(@Nullable Cursor cursor);

	/**
	 * Swaps the current cursor of this adapter for the specified <var>newCursor</var>.
	 * <p>
	 * <b>Note</b>, that you should close the returned cursor (if any) when it is no longer needed.
	 *
	 * @param newCursor The cursor to provide a new data set for this adapter.
	 * @return Old cursor or {@code null} if the current one is the same as the specified one.
	 * @see #changeCursor(Cursor)
	 * @see #getCursor()
	 * @see #getCursorAt(int)
	 */
	C swapCursor(@Nullable Cursor newCursor);

	/**
	 * Wraps the specified <var>cursor</var> into wrapper specific for this type of adapter.
	 *
	 * @param cursor Cursor to wrap.
	 * @return An instance of cursor wrapper holding the given cursor or cursor itself if this
	 * adapter does not require cursor wrapping or the specified cursor is already wrapped into
	 * required wrapper.
	 * @throws DatabaseException Type of {@link DatabaseException#TYPE_MISCONFIGURATION} if the cursor
	 *                           wrapper class cannot be instantiated to wrap the specified cursor.
	 * @see #swapCursor(Cursor)
	 * @see #changeCursor(Cursor)
	 */
	@NonNull
	C wrapCursor(@NonNull Cursor cursor);

	/**
	 * Returns the current cursor of this adapter specified via {@link #swapCursor(Cursor)} or
	 * {@link #changeCursor(Cursor)} or during initialization.
	 * <p>
	 * <b>Note</b>, that if this adapter wraps its original cursor into cursor wrapper, the returned
	 * cursor will be the same cursor wrapper as may be created via {@link #wrapCursor(Cursor)}.
	 *
	 * @return Current cursor that provides data set of this adapter or {@code null} if there was
	 * no cursor specified yet.
	 * @see #getCursorAt(int)
	 */
	@Nullable
	C getCursor();

	/**
	 * Returns a boolean flag indicating whether an item at the specified <var>position</var> is
	 * enabled or not.
	 *
	 * @param position The position of item to check.
	 * @return {@code True} if the item is enabled, {@code false} otherwise.
	 */
	boolean isEnabled(int position);

	/**
	 * Returns a boolean flag indicating whether the data set of this adapter has stable ids or not.
	 *
	 * @return {@code True} if the data set has stable ids so each position has its unique id,
	 * {@code false} otherwise.
	 * @see #getItemId(int)
	 */
	boolean hasStableIds();

	/**
	 * Saves the current state of this adapter.
	 *
	 * @return Saved state of this adapter or an <b>empty</b> state if this adapter does not need to
	 * save its state.
	 */
	@NonNull
	Parcelable saveInstanceState();

	/**
	 * Restores the previous state, saved via {@link #saveInstanceState()}, of this adapter.
	 *
	 * @param savedState Should be the same state as obtained via {@link #saveInstanceState()} before.
	 */
	void restoreInstanceState(@NonNull Parcelable savedState);
}
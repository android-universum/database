/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.annotation.handler;

import androidx.annotation.LayoutRes;
import universum.studios.android.database.annotation.CursorItemView;

/**
 * An {@link AnnotationHandler} extended interface for annotation handlers from the Database library
 * that are used to handle processing of annotations attached to classes like {@code BaseCursorAdapter}
 * provided by this library.
 *
 * @author Martin Albedinsky
 */
public interface CursorAdapterAnnotationHandler extends AnnotationHandler {

	/**
	 * Constant used to determine that no item view resource has been specified via {@link CursorItemView @CursorItemView}
	 * annotation.
	 */
	int NO_VIEW_RES = -1;

	/**
	 * Returns the item view layout resource obtained from {@link CursorItemView @CursorItemView}
	 * annotation (if presented).
	 *
	 * @param defaultViewRes Default layout resource to be returned if there is no annotation presented.
	 * @return Via annotation specified layout resource id or <var>defaultViewRes</var>.
	 */
	@LayoutRes
	int getItemViewRes(@LayoutRes int defaultViewRes);
}
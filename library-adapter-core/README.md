Database-Adapter-Core
===============

This module contains core elements and interfaces that are used by cursor adapters provided by this library.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-core:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [CursorDataSet](https://bitbucket.org/android-universum/database/src/main/library-adapter-core/src/main/java/universum/studios/android/database/adapter/CursorDataSet.java)
- [CursorDataSetAdapter](https://bitbucket.org/android-universum/database/src/main/library-adapter-core/src/main/java/universum/studios/android/database/adapter/CursorDataSetAdapter.java)
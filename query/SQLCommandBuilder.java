/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package com.albedinsky.android.database.inner.database.query;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * todo: description
 *
 * @author Martin Albedinsky
 */
@Deprecated
abstract class SQLCommandBuilder {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "SQLCommandBuilder";

	/**
	 * Flag indicating whether the output trough log-cat is enabled or not.
	 */
	// private final boolean LOG_ENABLED = true;

	/**
	 * Flag indicating whether the debug output trough log-cat is enabled or not.
	 */
	// private final boolean DEBUG_ENABLED = true;

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * String builder for commands creation.
	 */
	final StringBuilder STATEMENT_BUILDER = new StringBuilder("");

	/**
	 * Entity query to which is this builder attached.
	 */
	SimpleQuery mQuery;

	/*
	 * Constructors ================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Public --------------------------------------------------------------------------------------
	 */

	/**
	 * Getters + Setters ---------------------------------------------------------------------------
	 */

	/**
	 * Protected -----------------------------------------------------------------------------------
	 */

	/**
	 * Appends the given <var>column</var> into the given <var>builder</var>, also with delimiter
	 * if requested.
	 *
	 * @param builder   Builder to which should be the given value appended.
	 * @param column    Column to append.
	 * @param delimiter {@code True} when to append also delimiter (,).
	 */
	static void appendValue(StringBuilder builder, String column, boolean delimiter) {
		if (!TextUtils.isEmpty(column)) {
			builder.append(column);
			if (delimiter) {
				builder.append(", ");
			}
		}
	}

	/**
	 * Returns flag indicating whether this builder can build its SQL command or not.
	 *
	 * @param throwWarning {@code True} to throw an exception when can not to build command,
	 *                     {@code false} otherwise.
	 * @return {@code True} if {@link #buildQuery()} will return executable SQL query, {@code false}
	 * otherwise.
	 * @throws IllegalStateException If this builder can not build command and <var>throwWarning</var>
	 *                               flag is set to {@code true}.
	 */
	boolean canBuildQuery(boolean throwWarning) {
		final boolean can = !TextUtils.isEmpty(mQuery.mEntityName);
		if (!can && throwWarning) {
			throw new IllegalStateException("Missing table name for SQL QUERY.");
		}
		return can;
	}

	/**
	 * Builds the SQL query command from the current state of this builder.
	 *
	 * @return Executable SQL query or empty string if this builder can not to build its command
	 * due to incomplete state.
	 * @see #canBuildQuery(boolean)
	 */
	String buildQuery() {
		if (canBuildQuery(true)) {
			clearBuilder();
			return onBuildQuery();
		}
		return "";
	}

	/**
	 * Invoked whenever {@link #buildQuery()} is called and {@link #canBuildQuery(boolean)} returns
	 * {@code true}.
	 */
	abstract String onBuildQuery();

	/**
	 * Clears the current StringBuilder.
	 */
	void clearBuilder() {
		STATEMENT_BUILDER.setLength(0);
	}

	/**
	 * Converts the given object <var>value</var> into its string representation quoting instance
	 * of {@link String} like 'value'.
	 *
	 * @param value The desired value to convert into string.
	 * @return String representation of the given object value.
	 */
	String valueToString(Object value) {
		if (value instanceof String) {
			return "'" + value.toString() + "'";
		}
		return value.toString();
	}

	/**
	 * Private -------------------------------------------------------------------------------------
	 */

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * SQLite query builder for CREATE command.
	 */
	static class Create extends SQLCommandBuilder {

		/**
		 */
		@Override
		boolean canBuildQuery(boolean throwWarning) {
			boolean can = super.canBuildQuery(throwWarning);
			if (can && !(can = !(mQuery.mColumns == null || mQuery.mColumns.isEmpty())) && throwWarning) {
				throw new IllegalStateException("Missing 'columns' for SQL CREATE command.");
			}
			return can;
		}

		/**
		 */
		@Override
		String onBuildQuery() {
			// Add columns to create.
			final int nColumns = mQuery.mColumns.size();
			for (int i = 0; i < nColumns; i++) {
				appendValue(STATEMENT_BUILDER, mQuery.mColumns.get(i), (i != (nColumns - 1)));
			}
			// Append foreign keys to create if there are some.
			if (mQuery.mForeignKeys != null && !mQuery.mForeignKeys.isEmpty()) {
				final int nKeys = mQuery.mForeignKeys.size();
				STATEMENT_BUILDER.append(", ");
				for (int i = 0; i < nKeys; i++) {
					appendValue(STATEMENT_BUILDER, mQuery.mForeignKeys.get(i), (i != (nKeys - 1)));
				}
			}
			return String.format(mQuery.mCreateTableStatement, mQuery.mEntityName, STATEMENT_BUILDER.toString());
		}
	}

	/**
	 * SQLite query builder for INSERT command.
	 */
	static class Insert extends SQLCommandBuilder {

		/**
		 */
		@Override
		boolean canBuildQuery(boolean throwWarning) {
			boolean can = super.canBuildQuery(throwWarning);
			if (can) {
				final int nColumns = (mQuery.mColumns != null) ? mQuery.mColumns.size() : 0;
				final int nValues = (mQuery.mValues != null) ? mQuery.mValues.size() : 0;
				boolean hasValues = false;

				// First check single values.
				if (nValues > 0) {
					hasValues = true;
					can = checkGroup(mQuery.mValues, nColumns, throwWarning);
				}
				// Now check value groups.
				if (mQuery.mValueGroups != null && !mQuery.mValueGroups.isEmpty()) {
					for (List<Object> group : mQuery.mValueGroups) {
						can = checkGroup(group, nColumns, throwWarning);
						if (!group.isEmpty()) {
							hasValues = true;
						}
					}
				}

				if (!hasValues) {
					can = false;
					if (throwWarning) {
						throw new IllegalStateException("Missing 'values' for SQL INSERT command.");
					}
				}
			}
			return can;
		}

		/**
		 * Checks the given <var>group</var> if its size is consistent with the specified <var>columnsSize</var>.
		 *
		 * @param group        The group to check.
		 * @param columnsSize  The size against which will be the size of the given group checked.
		 * @param throwWarning {@code True} to throw an exception when the sizes are inconsistent,
		 *                     {@code false} otherwise.
		 * @return {@code True} if the group passes this check, {@code false} otherwise.
		 */
		boolean checkGroup(List<Object> group, int columnsSize, boolean throwWarning) {
			final int nValues = group.size();
			if (columnsSize > 0 && nValues != columnsSize) {
				if (throwWarning) {
					throw new IllegalStateException(
							"Inconsistent size of group of values(" + nValues + ") vs. columns(" + columnsSize + ") for SQL INSERT command."
					);
				}
				return false;
			}
			return true;
		}

		/**
		 */
		@Override
		String onBuildQuery() {
			STATEMENT_BUILDER.append(mQuery.mEntityName);
			// Check if we should use columns within command.
			if (mQuery.mColumns != null && !mQuery.mColumns.isEmpty()) {
				// Build the bulkUpdateModels columns part.
				STATEMENT_BUILDER.append(" (");
				final int nColumns = mQuery.mColumns.size();
				for (int i = 0; i < nColumns; i++) {
					appendValue(STATEMENT_BUILDER, mQuery.mColumns.get(i), (i != (nColumns - 1)));
				}
				STATEMENT_BUILDER.append(")");
			}
			final String into = STATEMENT_BUILDER.toString();

			// Append single values as one group.
			final List<List<Object>> groups = new ArrayList<>();
			if (mQuery.mValueGroups != null && !mQuery.mValueGroups.isEmpty()) {
				groups.addAll(mQuery.mValueGroups);
			}
			if (mQuery.mValues != null && !mQuery.mValues.isEmpty()) {
				groups.add(mQuery.mValues);
			}
			clearBuilder();

			// Now build the values part.
			final int nGroups = groups.size();
			for (int i = 0; i < nGroups; i++) {
				final List<Object> group = groups.get(i);
				final int nValues = group.size();
				STATEMENT_BUILDER.append("(");
				for (int j = 0; j < nValues; j++) {
					appendValue(
							STATEMENT_BUILDER,
							valueToString(group.get(j)),
							j != (nValues - 1)
					);
				}
				appendValue(STATEMENT_BUILDER, ")", i != (nGroups - 1));
			}
			return String.format(SimpleQuery.STATEMENT_INSERT_FORMAT, into, STATEMENT_BUILDER.toString());
		}
	}

	/**
	 * SQLite query builder for UPDATE command.
	 */
	static class Update extends SQLCommandBuilder {

		/**
		 */
		@Override
		boolean canBuildQuery(boolean throwWarning) {
			boolean can = super.canBuildQuery(throwWarning);
			if (can) {
				final int nColumns = (mQuery.mColumns != null) ? mQuery.mColumns.size() : 0;
				final int nValues = (mQuery.mValues != null) ? mQuery.mValues.size() : 0;
				// Check for columns vs. values consistency.
				if (nValues != nColumns || nValues == 0) {
					can = false;
					if (throwWarning) {
						throw new IllegalStateException(
								"Inconsistent count of values(" + nValues + ") vs. columns(" + nColumns + ") or they are missing for SQL UPDATE command."
						);
					}
				}
				// Check for empty where statement.
				else if (!(can = mQuery.hasConditions()) && throwWarning) {
					throw new IllegalStateException("Missing 'where' for SQL UPDATE command.");
				}
			}
			return can;
		}

		/**
		 */
		@Override
		String onBuildQuery() {
			// Build the set part.
			final int nValues = mQuery.mValues.size();
			for (int i = 0; i < nValues; i++) {
				appendValue(
						STATEMENT_BUILDER,
						mQuery.mColumns.get(i) + "=" + valueToString(mQuery.mValues.get(i)),
						i != (nValues - 1)
				);
			}
			return String.format(SimpleQuery.STATEMENT_UPDATE_FORMAT, mQuery.mEntityName, STATEMENT_BUILDER.toString(), mQuery.mConditions);
		}
	}

	/**
	 * SQLite query builder for DELETE command.
	 */
	static class Delete extends SQLCommandBuilder {

		/**
		 */
		@Override
		boolean canBuildQuery(boolean throwWarning) {
			boolean can = super.canBuildQuery(throwWarning);
			if (can) {
				if (!(can = mQuery.hasConditions()) && throwWarning) {
					throw new IllegalStateException("Missing 'where' for SQL DELETE command.");
				}
			}
			return can;
		}

		/**
		 */
		@Override
		String onBuildQuery() {
			return String.format(SimpleQuery.STATEMENT_DELETE_FORMAT, mQuery.mEntityName, mQuery.mConditions.toString());
		}
	}

	/**
	 * SQLite query builder for SELECT command.
	 */
	static class Select extends SQLCommandBuilder {

		/**
		 */
		@Override
		boolean canBuildQuery(boolean throwWarning) {
			boolean can = super.canBuildQuery(throwWarning);
			if (can) {
				can = (mQuery.mColumns != null && mQuery.mColumns.size() > 0);
				if (!can && throwWarning) {
					throw new IllegalStateException("Missing 'columns' for SQL SELECT command.");
				}
			}
			return can;
		}

		/**
		 */
		@Override
		String onBuildQuery() {
			// Build the columns part.
			final int nColumns = mQuery.mColumns.size();
			for (int i = 0; i < nColumns; i++) {
				appendValue(STATEMENT_BUILDER, mQuery.mColumns.get(i), (i != (nColumns - 1)));
			}
			final String columns = STATEMENT_BUILDER.toString();

			// Now build the join part if it should be presented.
			clearBuilder();
			STATEMENT_BUILDER.append(mQuery.mEntityName);
			if (mQuery.hasJoins()) {
				STATEMENT_BUILDER.append(" ");
				STATEMENT_BUILDER.append(mQuery.mJoins.toString());
			}

			// Now build the conditions part if it should be presented.
			if (mQuery.hasConditions()) {
				STATEMENT_BUILDER.append(" ");
				STATEMENT_BUILDER.append(mQuery.mConditions.toString());
			}
			return String.format(SimpleQuery.STATEMENT_SELECT_FORMAT, columns, STATEMENT_BUILDER.toString());
		}
	}
}

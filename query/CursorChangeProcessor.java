/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package com.albedinsky.android.database.adapter;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.CursorJoiner;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import com.albedinsky.android.support.database.DatabaseConfig;
import com.albedinsky.android.support.database.annotation.Column;

import java.util.ArrayList;
import java.util.List;

/**
 * A CursorChangeProcessor can be used to handle change in data set of cursors of
 * {@link RecyclerView.Adapter RecyclerView.Adapter} by comparison of
 * current data of these cursors. This processor compares current value of <b>_id</b> column for
 * each row of the specified cursors passed to {@link #processCursors(Cursor, Cursor)} and according
 * to whether such value is found in booth or just in one of the specified cursors will determine
 * whether a new row or the old one has been inserted, removed or unchanged. Such change is then
 * notified to all registered observers of the attached adapter via one of {@code notifyItem...(...)}
 * calls.
 *
 * @author Martin Albedinsky
 */
@SuppressLint("LongLogTag")
public class CursorChangeProcessor {

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * todo:
	 *
	 * @author Martin Albedinsky
	 */
	public interface Adapter {

		/**
		 * todo:
		 *
		 * @param position
		 * @param payload
		 */
		void notifyItemChanged(int position, @Nullable Object payload);

		/**
		 * todo:
		 *
		 * @param startPosition
		 * @param count
		 * @param payload
		 */
		void notifyItemRangeChanged(int startPosition, int count, @Nullable Object payload);

		/**
		 * todo:
		 *
		 * @param position
		 */
		void notifyItemInserted(int position);

		/**
		 * todo:
		 *
		 * @param startPosition
		 * @param count
		 */
		void notifyItemRangeInserted(int startPosition, int count);

		/**
		 * todo:
		 *
		 * @param position
		 */
		void notifyItemRemoved(int position);

		/**
		 * todo:
		 *
		 * @param startPosition
		 * @param count
		 */
		void notifyItemRangeRemoved(int startPosition, int count);

		/**
		 * todo:
		 *
		 * @param fromPosition
		 * @param toPosition
		 */
		void notifyItemMoved(int fromPosition, int toPosition);
	}

	/**
	 * todo:
	 *
	 * @author Martin Albedinsky
	 */
	public interface ItemChangeHandler {

		/**
		 * todo:
		 *
		 * @param newCursor
		 * @return
		 */
		boolean hasChanged(@NonNull Cursor newCursor);
	}

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "CursorChangeProcessor";

	/**
	 * todo
	 */
	public static final int NO_POSITION = -1;

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Set of column names that is used for comparison process.
	 */
	private static final String[] COMPARABLE_COLUMN_NAMES = new String[]{Column.Primary.COLUMN_NAME};

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Handler for changes in inserted items.
	 */
	private final ChangeHandler mInsertHandler;

	/**
	 * Handler for changes in removed items.
	 */
	private final ChangeHandler mRemoveHandler;

	/**
	 * todo:
	 */
	private ItemChangeHandler mItemChangeHandler;

	/**
	 * todo
	 */
	private int mFirstVisibleItemPosition = NO_POSITION;

	/**
	 * todo
	 */
	private int mLastVisibleItemPosition = NO_POSITION;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of CursorChangeProcessor that can be used to process changes in
	 * <b>two cursors</b>.
	 */
	public CursorChangeProcessor() {
		this.mInsertHandler = new InsertHandler();
		this.mRemoveHandler = new RemoveHandler();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * todo:
	 *
	 * @param changeHandler
	 */
	public void setItemChangeHandler(@Nullable ItemChangeHandler changeHandler) {
		this.mItemChangeHandler = changeHandler;
	}

	/**
	 * todo:
	 *
	 * @param firstVisiblePosition
	 * @param lastVisiblePosition
	 */
	public void setVisibleItemPositions(@IntRange(from = 0) int firstVisiblePosition, @IntRange(from = 0) int lastVisiblePosition) {
		if (lastVisiblePosition < firstVisiblePosition) {
			throw new IndexOutOfBoundsException(
					"Specified last visible item position(" + lastVisiblePosition + ") is not equal or " +
							"grater than first visible item position(" + firstVisiblePosition + ")."
			);
		}
		this.mFirstVisibleItemPosition = firstVisiblePosition;
		this.mLastVisibleItemPosition = lastVisiblePosition;
	}

	/**
	 * todo:
	 */
	public void clearVisibleItemPositions() {
		this.mFirstVisibleItemPosition = NO_POSITION;
		this.mLastVisibleItemPosition = NO_POSITION;
	}

	/**
	 * todo:
	 *
	 * @param newCursor
	 * @param oldCursor
	 * @return
	 */
	@NonNull
	@SuppressWarnings("ConstantConditions")
	public Result processCursors(@Nullable Cursor newCursor, @Nullable Cursor oldCursor) {
		if (newCursor == null && oldCursor == null) return new Result();
		Result result;
		if ((result = processCursorsTotalChange(oldCursor, newCursor)) != null) {
			return result;
		}

		final CursorJoiner joiner = new CursorJoiner(
				oldCursor, COMPARABLE_COLUMN_NAMES,
				newCursor, COMPARABLE_COLUMN_NAMES
		);

		mInsertHandler.clear();
		mRemoveHandler.clear();
		result = new Result();

		DataSetChange[] dataSetChanges;
		for (CursorJoiner.Result joinerResult : joiner) {
			switch (joinerResult) {
				// Row has been removed.
				case LEFT:
					this.resolvePendingChange(mInsertHandler, result);

					dataSetChanges = mRemoveHandler.handleChange(oldCursor.getPosition());
					if (dataSetChanges != null) {
						if (dataSetChanges.length == 1) {
							result.addDataSetChange(dataSetChanges[0]);
						} else {
							result.addDataSetChange(dataSetChanges[1]);
						}
					}
					break;
				// Row has been inserted.
				case RIGHT:
					this.resolvePendingChange(mRemoveHandler, result);

					dataSetChanges = mInsertHandler.handleChange(newCursor.getPosition());
					if (dataSetChanges != null) {
						if (dataSetChanges.length == 1) {
							result.addDataSetChange(dataSetChanges[0]);
						} else {
							result.addDataSetChange(dataSetChanges[1]);
						}
					}
					break;
				// Row has been possibly changed.
				case BOTH:
					if (mItemChangeHandler != null && mFirstVisibleItemPosition != NO_POSITION && mLastVisibleItemPosition != NO_POSITION) {
						final int cursorPosition = newCursor.getPosition();
						if (cursorPosition >= mFirstVisibleItemPosition && cursorPosition <= mLastVisibleItemPosition &&
								mItemChangeHandler.hasChanged(newCursor)) {
							this.resolvePendingChange(mInsertHandler, result);
							this.resolvePendingChange(mRemoveHandler, result);

							// Item at the current processing position is visible and its data has
							// changed so its UI should be re-bound with the new data.
							result.addDataSetChange(new ItemChanged(cursorPosition));
							// todo: handle item range changes
						}
					}
					break;
			}
		}
		return result;
	}

	/**
	 * Called to process the specified cursors for "total" change in theirs data sets. By <b>total</b>
	 * here is meant a change where old cursor does not contain any items and new one contains some
	 * or reversed.
	 *
	 * @return Result with total data set changes or {@code null} if the change in cursors is more
	 * complex and need to be resolved more closely.
	 */
	private Result processCursorsTotalChange(Cursor oldCursor, Cursor newCursor) {
		final int oldDataSetSize = oldCursor != null ? oldCursor.getCount() : 0;
		final int newDataSetSize = newCursor != null ? newCursor.getCount() : 0;

		// Check for all items removed.
		if (newCursor == null || newDataSetSize == 0) {
			return new Result().addDataSetChange(new ItemRangeRemoved(0, oldDataSetSize));
		}

		// Check for all items inserted.
		if (oldCursor == null || oldDataSetSize == 0) {
			return new Result().addDataSetChange(new ItemRangeInserted(0, newDataSetSize));
		}
		return null;
	}

	/**
	 * todo:
	 *
	 * @param handler
	 * @param result
	 */
	private void resolvePendingChange(ChangeHandler handler, Result result) {
		if (handler.hasPendingChange()) result.addDataSetChange(handler.popPendingChange());
	}

	/*
	 * Inner classes ===============================================================================
	 */

	/**
	 * Helper used when processing changes in cursor's data set.
	 */
	private static abstract class ChangeHandler {

		/**
		 * Last handled position for the change in items range.
		 */
		int rangeLastHandledPosition = -1;

		/**
		 * Origin (start position) of the range of changed items.
		 */
		int rangeOrigin = -1;

		/**
		 * Size of the range of change items.
		 */
		int rangeSize = -1;

		/**
		 * Returns a boolean flag indicating whether this handler still need to notify the attached
		 * adapter about changed item or changed range of items.
		 * <p>
		 * This handler can be in such state when {@link #handleChange(int)} has been called with
		 * position that was determined to belong into range of previous handled positions but
		 * {@code handleChange(int)} is not to be called further due to fully processed cursors so
		 * the saved range of changed items (positions) need to be still notified to the adapter
		 * (its observers).
		 *
		 * @return {@code True} if calling {@link #popPendingChange()} will return todo.
		 */
		boolean hasPendingChange() {
			return rangeOrigin != -1 && rangeSize > 0;
		}

		/**
		 * todo
		 */
		DataSetChange popPendingChange() {
			DataSetChange dataSetChange = null;
			if (hasPendingChange()) {
				dataSetChange = onItemRangeChanged(rangeOrigin, rangeSize);
				clear();
			}
			return dataSetChange;
		}

		/**
		 * Handles change in the specified <var>position</var> in such a way that this handler will
		 * determine whether the provided position can be included into <b>successive</b> range of
		 * positions that has been provided to this handler via previous calls of this method or if
		 * such range has been interrupted so change for the provided position should be notified as
		 * "single" change.
		 * <p>
		 * This handler will notify the occurred change accordingly to its implementation to the
		 * attached adapter via one of {@code RecyclerView.Adapter.notifyItem...(...)} calls.
		 *
		 * @param position The position for which to handle the change.
		 * @return Updated offset by count of positions that has been notified to the adapter's observers.
		 */
		DataSetChange[] handleChange(int position) {
			DataSetChange[] dataSetChanges = null;
			if ((rangeLastHandledPosition + 1) == position || rangeLastHandledPosition == -1) {
				if (rangeOrigin == -1) {
					rangeOrigin = position;
					rangeSize = 0;
				}
				rangeLastHandledPosition = position;
				rangeSize++;
			} else if (rangeSize > 0) {
				dataSetChanges = new DataSetChange[]{
						onItemRangeChanged(rangeOrigin, rangeSize),
						onItemChanged(position)
				};
				clear();
			} else {
				dataSetChanges = new DataSetChange[]{onItemChanged(position)};
			}
			return dataSetChanges;
		}

		/**
		 * Invoked from {@link #handleChange(int)} if the specified position cannot be included
		 * into <b>successive</b> range of previously provided positions.
		 *
		 * @param position The position for which should be the change notified to all registered
		 *                 observers of the attached adapter.
		 * @return Updated offset by single position that has been notified if it is appropriate for
		 * this type of handler.
		 */
		abstract DataSetChange onItemChanged(int position);

		/**
		 * Invoked from {@link #handleChange(int)} if the current <b>successive</b> range of
		 * positions has been interrupted and thus change in such range should be notified to all
		 * registered observers of the attached adapter.
		 *
		 * @param rangeOrigin Origin (start position) of the range.
		 * @param rangeSize   Size of the range to be notified.
		 * @return Updated offset by the range size that has been notified if it is appropriate for
		 * this type of handler.
		 */
		abstract DataSetChange onItemRangeChanged(int rangeOrigin, int rangeSize);

		/**
		 * Clears the current values of this handler.
		 */
		void clear() {
			rangeLastHandledPosition = rangeOrigin = rangeSize = -1;
		}
	}

	/**
	 * A {@link ChangeHandler} implementation used to handle changes in <b>inserted</b> items.
	 */
	private static final class InsertHandler extends ChangeHandler {

		/**
		 */
		@Override
		DataSetChange onItemChanged(int position) {
			return new ItemInserted(position);
		}

		/**
		 */
		@Override
		DataSetChange onItemRangeChanged(int rangeOrigin, int rangeSize) {
			return new ItemRangeInserted(rangeOrigin, rangeOrigin);
		}
	}

	/**
	 * A {@link ChangeHandler} implementation used to handle changes in <b>removed</b> items.
	 */
	private static final class RemoveHandler extends ChangeHandler {

		/**
		 */
		@Override
		DataSetChange onItemChanged(int position) {
			return new ItemRemoved(position);
		}

		/**
		 */
		@Override
		DataSetChange onItemRangeChanged(int rangeOrigin, int rangeSize) {
			return new ItemRangeRemoved(rangeOrigin, rangeSize);
		}
	}

	/**
	 * todo:
	 */
	public static class Result {

		/**
		 * todo:
		 */
		@SuppressWarnings("unused")
		static final String TAG = "CursorChangeProcessor.Result";

		/**
		 * todo:
		 */
		private List<DataSetChange> dataSetChanges;

		/**
		 * todo:
		 */
		private Result() {
		}

		/**
		 * todo:
		 *
		 * @param change
		 * @return
		 */
		Result addDataSetChange(@NonNull DataSetChange change) {
			if (dataSetChanges == null) dataSetChanges = new ArrayList<>(1);
			dataSetChanges.add(change);
			return this;
		}

		/**
		 * todo:
		 *
		 * @return
		 */
		public boolean hasChanges() {
			return dataSetChanges != null;
		}

		/**
		 * todo:
		 *
		 * @param adapter
		 * @return
		 */
		public boolean notifyAdapter(@NonNull Adapter adapter) {
			if (dataSetChanges != null) {
				// Value used to offset the changed positions properly while the adapter's observers
				// are notified which results in data set is being dynamically changed.
				int offset = 0;
				for (DataSetChange change : dataSetChanges) {
					offset = change.notifyAdapter(adapter, offset);
				}
				dataSetChanges.clear();
				return true;
			}
			return false;
		}
	}

	/**
	 * todo:
	 *
	 * @author Martin Albedinsky
	 */
	private static abstract class DataSetChange {

		/**
		 * todo:
		 *
		 * @param offset
		 * @param adapter
		 * @return
		 */
		abstract int notifyAdapter(Adapter adapter, int offset);
	}

	/**
	 * todo:
	 *
	 * @author Martin Albedinsky
	 */
	private static final class ItemChanged extends DataSetChange {

		/**
		 * todo
		 */
		private final int position;

		/**
		 * todo
		 */
		private final Object payload;

		/**
		 * todo:
		 *
		 * @param position
		 */
		ItemChanged(int position) {
			this(position, null);
		}

		/**
		 * todo:
		 *
		 * @param position
		 * @param payload
		 */
		ItemChanged(int position, @Nullable Object payload) {
			this.position = position;
			this.payload = payload;
		}

		/**
		 */
		@Override
		int notifyAdapter(Adapter adapter, int offset) {
			if (DatabaseConfig.DEBUG_LOG_ENABLED) {
				Log.d(Result.TAG, "Notifying adapter about changed item at(" + position + ").");
			}
			adapter.notifyItemChanged(position, payload);
			return offset;
		}
	}

	/**
	 * todo:
	 */
	private static final class ItemRangeChanged extends DataSetChange {

		/**
		 * todo
		 */
		private final int startPosition;

		/**
		 * todo
		 */
		private final int count;

		/**
		 * todo
		 */
		private final Object payload;

		/**
		 * todo:
		 *
		 * @param startPosition
		 * @param count
		 */
		ItemRangeChanged(int startPosition, int count) {
			this(startPosition, count, null);
		}

		/**
		 * todo:
		 *
		 * @param startPosition
		 * @param count
		 * @param payload
		 */
		ItemRangeChanged(int startPosition, int count, @Nullable Object payload) {
			this.startPosition = startPosition;
			this.count = count;
			this.payload = payload;
		}

		/**
		 */
		@Override
		int notifyAdapter(Adapter adapter, int offset) {
			if (DatabaseConfig.DEBUG_LOG_ENABLED) {
				Log.d(Result.TAG, "Notifying adapter about changed items in range(start: " + startPosition + ", count: " + count + ").");
			}
			adapter.notifyItemRangeChanged(startPosition, count, payload);
			return offset;
		}
	}

	/**
	 * todo:
	 */
	private static final class ItemInserted extends DataSetChange {

		/**
		 * todo:
		 */
		private final int position;

		/**
		 * todo:
		 *
		 * @param position
		 */
		ItemInserted(int position) {
			this.position = position;
		}

		/**
		 */
		@Override
		int notifyAdapter(Adapter adapter, int offset) {
			if (DatabaseConfig.DEBUG_LOG_ENABLED) {
				Log.d(Result.TAG, "Notifying adapter about inserted item at(" + position + ").");
			}
			adapter.notifyItemInserted(position + offset);
			return offset;
		}
	}

	/**
	 * todo:
	 */
	private static final class ItemRangeInserted extends DataSetChange {

		/**
		 * todo
		 */
		@SuppressWarnings("unused")
		private static final String TAG = "ItemRangeInserted";

		/**
		 * todo:
		 */
		private final int startPosition;

		/**
		 * todo:
		 */
		private final int count;

		/**
		 * todo:
		 *
		 * @param startPosition
		 * @param count
		 */
		ItemRangeInserted(int startPosition, int count) {
			this.startPosition = startPosition;
			this.count = count;
		}

		/**
		 */
		@Override
		int notifyAdapter(Adapter adapter, int offset) {
			if (DatabaseConfig.DEBUG_LOG_ENABLED) {
				Log.d(Result.TAG, "Notifying adapter about inserted items in range(start: " + startPosition + ", count: " + count + ").");
			}
			adapter.notifyItemRangeInserted(startPosition + offset, count);
			return offset;
		}
	}

	/**
	 * todo:
	 */
	private static final class ItemRemoved extends DataSetChange {

		/**
		 * todo:
		 */
		private final int position;

		/**
		 * todo:
		 *
		 * @param position
		 */
		ItemRemoved(int position) {
			this.position = position;
		}

		/**
		 */
		@Override
		int notifyAdapter(Adapter adapter, int offset) {
			if (DatabaseConfig.DEBUG_LOG_ENABLED) {
				Log.d(Result.TAG, "Notifying adapter about removed item at(" + position + ").");
			}
			adapter.notifyItemRemoved(position + offset);
			return --offset;
		}
	}

	/**
	 * todo:
	 */
	private static final class ItemRangeRemoved extends DataSetChange {

		/**
		 * todo:
		 */
		private final int startPosition;

		/**
		 * todo:
		 */
		private final int count;

		/**
		 * todo:
		 *
		 * @param startPosition
		 * @param count
		 */
		ItemRangeRemoved(int startPosition, int count) {
			this.startPosition = startPosition;
			this.count = count;
		}

		/**
		 */
		@Override
		int notifyAdapter(Adapter adapter, int offset) {
			if (DatabaseConfig.DEBUG_LOG_ENABLED) {
				Log.d(Result.TAG, "Notifying adapter about removed items in range(start: " + startPosition + ", count: " + count + ").");
			}
			adapter.notifyItemRangeRemoved(startPosition + offset, count);
			return offset - count;
		}
	}
}

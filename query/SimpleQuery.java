/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package com.albedinsky.android.database.inner.database.query;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.albedinsky.android.database.inner.database.DatabaseConfig;
import com.albedinsky.android.database.inner.database.util.TableColumn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * todo: description
 *
 * @author Martin Albedinsky
 */
@Deprecated
final class SimpleQuery {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "Query";

	/**
	 * Flag indicating whether the debug output trough log-cat is enabled or not.
	 */
	private final boolean DEBUG_ENABLED = DatabaseConfig.DEBUG_LOG_ENABLED;

	/**
	 * Flag indicating whether the output trough log-cat is enabled or not.
	 */
	// private final boolean LOG_ENABLED = true;

	/**
	 * Format for <b>CREATE TABLE ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>table name</b></li>
	 * <li>2) <b>columns</b></li>
	 * </ul>
	 */
	public static final String STATEMENT_CREATE_TABLE_FORMAT = "CREATE TABLE %s (%s)";

	/**
	 * Format for <b>CREATE TABLE IF NOT EXISTS ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>table name</b></li>
	 * <li>2) <b>columns to create</b></li>
	 * </ul>
	 */
	public static final String STATEMENT_CREATE_TABLE_IF_NOT_EXISTS_FORMAT = "CREATE TABLE IF NOT EXISTS %s (%s)";

	/**
	 * Format for <b>INSERT INTO ... VALUES ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>table name [+ columns to bulkUpdateModels]</b></li>
	 * <li>2) <b>values</b></li>
	 * </ul>
	 */
	public static final String STATEMENT_INSERT_FORMAT = "INSERT INTO %s VALUES %s";

	/**
	 * Format for <b>UPDATE ... SET ... WHERE</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>table name</b></li>
	 * <li>2) <b>column new values</b></li>
	 * <li>3) <b>conditions</b></li>
	 * </ul>
	 */
	public static final String STATEMENT_UPDATE_FORMAT = "UPDATE %s SET %s WHERE %s";

	/**
	 * Format for <b>DELETE FROM ... WHERE ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>table name</b></li>
	 * <li>2) <b>conditions</b></li>
	 * </ul>
	 */
	public static final String STATEMENT_DELETE_FORMAT = "DELETE FROM %s WHERE %s";

	/**
	 * Format for <b>SELECT ... FROM ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>columns</b></li>
	 * <li>2) <b>table name [+ conditions]</b></li>
	 * </ul>
	 */
	public static final String STATEMENT_SELECT_FORMAT = "SELECT %s FROM %s";

	/**
	 * Format for <b>SELECT TOP ... FROM ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>number/percent</b></li>
	 * <li>2) <b>columns</b></li>
	 * <li>3) <b>table name [+ conditions]</b></li>
	 * </ul>
	 */
	private static final String STATEMENT_SELECT_TOP_FORMAT = "SELECT TOP %s %s FROM %s";

	/**
	 * Format for <b>WHERE ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>columns</b></li>
	 * </ul>
	 */
	public static final String CONDITION_WHERE_FORMAT = "WHERE %s";

	/**
	 * Format for <b>ORDER BY ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>columns</b></li>
	 * <li>2) <b>key word</b></li>
	 * </ul>
	 */
	public static final String CONDITION_ORDER_BY_FORMAT = "ORDER BY %s %s";

	/**
	 * Format for <b>GROUP BY ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>columns</b></li>
	 * </ul>
	 */
	public static final String CONDITION_GROUP_BY_FORMAT = "GROUP BY %s";

	/**
	 * Format for <b>LIMIT ...</b> SQL statement.
	 * <h3>Parameters:</h3>
	 * <ul>
	 * <li>1) <b>number</b></li>
	 * </ul>
	 */
	public static final String CONDITION_LIMIT_FORMAT = "LIMIT %d";

	/**
	 * Key word for {@link #orderBy(String, String)} to order <b>ascending</b>.
	 */
	public static final String ASC = "ASC";

	/**
	 * Key word for {@link #orderBy(String, String)} to order <b>descending</b>.
	 */
	public static final String DESC = "DESC";

	/**
	 * Type flag for {@link #ensure(int)} method.
	 */
	private static final int COLLECTION_COLUMNS = 0x01;

	/**
	 * Type flag for {@link #ensure(int)} method.
	 */
	private static final int COLLECTION_FOREIGN_KEYS = 0x02;

	/**
	 * Type flag for {@link #ensure(int)} method.
	 */
	private static final int COLLECTION_VALUES = 0x03;

	/**
	 * Type flag for {@link #ensure(int)} method.
	 */
	private static final int COLLECTION_VALUE_GROUPS = 0x04;

	/**
	 * Private flag indicating whether the log is enabled or not.
	 */
	private static final int PFLAG_LOG_ENABLED = 0x01;

	/**
	 * Private flag indicating whether the current data of this query object has changed or not.
	 */
	private static final int PFLAG_DATA_CHANGED = 0x02;

	/**
	 * Private flag indicating whether there was request for join operation or not.
	 */
	private static final int PFLAG_JOIN_REQUESTED = 0x04;

	/*
	 * Enums =======================================================================================
	 */

	/**
	 * <h3>Enum Overview</h3>
	 * Specifies type of Join for {@link #join(SimpleQuery.Join, String)}
	 * method.
	 *
	 * @author Martin Albedinsky
	 */
	public enum Join {
		/**
		 * <b>Cross</b> join type.
		 */
		CROSS("CROSS"),
		/**
		 * <b>Inner</b> join type.
		 */
		INNER("INNER"),
		/**
		 * <b>Left-Outer</b> join type.
		 */
		LEFT_OUTER("LEFT OUTER");

		/**
		 * Format of this join statement.
		 */
		final String statementFormat;

		/**
		 * Creates a new instance of Join.
		 *
		 * @param name Name of the join command.
		 */
		private Join(String name) {
			this.statementFormat = name + " JOIN %s ON";
		}

		/**
		 * Creates a statement for this join instance.
		 *
		 * @param entityName Name of entity for which to created join statement.
		 * @return Join statement.
		 */
		String createStatement(String entityName) {
			return String.format(statementFormat, entityName);
		}
	}

	/**
	 * Enum used to define type of SQL command to mark the current query state.
	 */
	private enum SQLCommand {
		/**
		 * Defines none SQL command selected.
		 */
		NONE(null),
		/**
		 * Defines SQL command for CREATE query.
		 */
		CREATE(SQLCommandBuilder.Create.class),
		/**
		 * Defines SQL command for INSERT query.
		 */
		INSERT(SQLCommandBuilder.Insert.class),
		/**
		 * Defines SQL command for UPDATE query.
		 */
		UPDATE(SQLCommandBuilder.Update.class),
		/**
		 * Defines SQL command for DELETE query.
		 */
		DELETE(SQLCommandBuilder.Delete.class),
		/**
		 * Defines SQL command for SELECT query.
		 */
		SELECT(SQLCommandBuilder.Select.class),
		/**
		 * Defines SQL command for EXPLICIT (raw) query.
		 */
		EXPLICIT(null);

		/**
		 * Class of builder specific for this command.
		 */
		final Class<? extends SQLCommandBuilder> classOfBuilder;

		/**
		 * Creates a new instance of SQLCommand.
		 *
		 * @param classOfBuilder A class of builder which can build this type of command.
		 */
		SQLCommand(Class<? extends SQLCommandBuilder> classOfBuilder) {
			this.classOfBuilder = classOfBuilder;
		}

		/**
		 * Check whether this command is a valid one.
		 *
		 * @return {@code True} if this is not {@link #NONE} command instance.
		 */
		boolean isValid() {
			return !this.equals(NONE);
		}

		/**
		 * Creates a new instance of builder specific for this command.
		 *
		 * @param query Instance of query to which to attach created builder.
		 * @return Command builder.
		 */
		SQLCommandBuilder create(SimpleQuery query) {
			if (!this.equals(NONE)) {
				try {
					final SQLCommandBuilder builder = classOfBuilder.newInstance();
					builder.mQuery = query;
					return builder;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Entity name for which should be this query executed.
	 */
	String mEntityName;

	/**
	 * Format for CREATE TABLE statement.
	 */
	String mCreateTableStatement = STATEMENT_CREATE_TABLE_IF_NOT_EXISTS_FORMAT;

	/**
	 * Joins builder.
	 */
	StringBuilder mJoins;

	/**
	 * Conditions builder.
	 */
	StringBuilder mConditions;

	/**
	 * Command by which is this query currently marked.
	 */
	private SQLCommand mCommand = SQLCommand.NONE;

	/**
	 * SQL command builder.
	 */
	private SQLCommandBuilder mBuilder;

	/**
	 * SQLite database upon which should be this query executed. Can be null.
	 */
	private SQLiteDatabase mDb;

	/**
	 * Currently cached query string if data since last change wasn't changed yet.
	 */
	private String mCachedQuery;

	/**
	 * Stores all private flags for this object.
	 */
	private int mPrivateFlags;

	/**
	 * Set of columns for this query. This is used for CREATE, UPDATE query.
	 */
	List<String> mColumns;

	/**
	 * Set of foreign keys for this query. This is used for CREATE query.
	 */
	List<String> mForeignKeys;

	/**
	 * Set of values for this query. This is used for CREATE, UPDATE queries.
	 */
	List<Object> mValues;

	/**
	 * List of set of values for this query. This is used for INSERT query.
	 */
	List<List<Object>> mValueGroups;

	/**
	 * Set of selection arguments for {@link android.database.sqlite.SQLiteDatabase#rawQuery(String, String[])}
	 * method.
	 */
	String[] mSelectionArgs;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of Query with empty initial state.
	 */
	public SimpleQuery() {
	}

	/**
	 * Same as {@link #SimpleQuery(String, android.database.sqlite.SQLiteDatabase)} with {@code null}
	 * SQLite database.
	 * <p>
	 * Use this constructor when you want to execute just the specified <var>rawQuery</var> by
	 * {@link #executeRaw(android.database.sqlite.SQLiteDatabase)} or {@link #executeSQL(android.database.sqlite.SQLiteDatabase)}.
	 */
	public SimpleQuery(@NonNull String rawQuery) {
		if (!TextUtils.isEmpty(rawQuery)) {
			this.mCachedQuery = rawQuery;
			this.mCommand = SQLCommand.EXPLICIT;
		}
		this.mDb = null;
	}

	/**
	 * Same as {@link #SimpleQuery(String, android.database.sqlite.SQLiteDatabase)} with {@code null}
	 * raw query.
	 * <p>
	 * Use this constructor when you want to create an SQL query using build-in methods
	 * of this query object.
	 */
	public SimpleQuery(@NonNull SQLiteDatabase db) {
		this("", db);
	}

	/**
	 * Creates a new instance of Query.
	 *
	 * @param rawQuery A raw query to be executed when {@link #executeRaw()} or {@link #executeRaw(android.database.sqlite.SQLiteDatabase)}
	 *                 is called.
	 * @param db       An instance of database upon which should be this query executed.
	 */
	public SimpleQuery(@NonNull String rawQuery, @NonNull SQLiteDatabase db) {
		if (!TextUtils.isEmpty(rawQuery)) {
			this.mCachedQuery = rawQuery;
			this.mCommand = SQLCommand.EXPLICIT;
		}
		this.mDb = db;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Public --------------------------------------------------------------------------------------
	 */

	/**
	 * Sets the name of entity for this query.
	 *
	 * @param entityName Name of the desired entity for which will be this query executed.
	 * @return This query to allow methods chaining.
	 */
	public SimpleQuery entity(@NonNull String entityName) {
		this.saveDataChanged();
		this.mEntityName = entityName;
		return this;
	}

	/**
	 * Same as {@link #create(String, String)} with {@link #STATEMENT_CREATE_TABLE_IF_NOT_EXISTS_FORMAT}
	 * statement.
	 */
	public SimpleQuery create(@NonNull String entityName) {
		return create(entityName, STATEMENT_CREATE_TABLE_IF_NOT_EXISTS_FORMAT);
	}

	/**
	 * Marks this query as <b>CREATE</b> SQL command.
	 *
	 * @param entityName Name of the desired entity to be created by this query.
	 * @param statement  Format for create statement. Use {@link #STATEMENT_CREATE_TABLE_FORMAT} or
	 *                   {@link #STATEMENT_CREATE_TABLE_IF_NOT_EXISTS_FORMAT}.
	 * @return  This query to allow methods chaining.
	 * @see #create(String)
	 * @see #columns(String[])
	 */
	public SimpleQuery create(@NonNull String entityName, String statement) {
		this.updateCommand(SQLCommand.CREATE);
		this.mCreateTableStatement = statement;
		return entity(entityName);
	}

	/**
	 * Appends the current columns for this query with the given one.
	 *
	 * @param column The desired column to append.
	 * @return This query to allow methods chaining.
	 */
	public SimpleQuery column(@NonNull String column) {
		this.saveDataChanged();
		this.ensure(COLLECTION_COLUMNS);
		mColumns.add(column);
		return this;
	}

	/**
	 * Appends the current columns for this query with the given one. If the column represents also
	 * foreign key ({@link TableColumn#isForeignKey()}) it will be added also into the current
	 * foreign keys.
	 *
	 * @param column The desired column to append.
	 * @return This query to allow methods chaining.
	 */
	public SimpleQuery column(@NonNull TableColumn column) {
		this.saveDataChanged();
		this.ensure(COLLECTION_COLUMNS);
		mColumns.add(column.build());
		// Check for foreign key.
		if (column.isForeignKey()) {
			this.ensure(COLLECTION_FOREIGN_KEYS);
			// fixme: mForeignKeys.add(column.buildReference());
		}
		return this;
	}

	/**
	 * Appends the current columns for this query with the given ones.
	 *
	 * @param columns The desired columns to append.
	 * @return This query to allow methods chaining.
	 * @see #values(Object...)
	 * @see #column(String)
	 * @see #columns(TableColumn[])
	 */
	public SimpleQuery columns(@NonNull String[] columns) {
		this.saveDataChanged();
		this.ensure(COLLECTION_COLUMNS);
		mColumns.addAll(Arrays.asList(columns));
		return this;
	}

	/**
	 * Like {@link #column(TableColumn)} where all the given
	 * <var>columns</var> will be iterated.
	 *
	 * @param columns Columns for this query.
	 * @return This query to allow methods chaining.
	 * @see #column(TableColumn)
	 * @see #columns(String[])
	 */
	public SimpleQuery columns(@NonNull TableColumn[] columns) {
		if (columns.length > 0) {
			this.saveDataChanged();
			for (TableColumn column : columns) {
				column(column);
			}
		}
		return this;
	}

	/**
	 * Marks this query as <b>INSERT</b> SQL command.
	 *
	 * @return This query to allow methods chaining.
	 * @see #into(String)
	 * @see #columns(String[])
	 * @see #values(Object[])
	 */
	public SimpleQuery insert() {
		this.updateCommand(SQLCommand.INSERT);
		return this;
	}

	/**
	 * Same as {@link #entity(String)}.
	 */
	public SimpleQuery into(@NonNull String entityName) {
		return entity(entityName);
	}

	/**
	 * Appends the current values for this query with the given one. Use this for <b>CREATE + UPDATE</b>
	 * SQL commands only.
	 *
	 * @param value The desired value to append.
	 * @return This query to allow methods chaining.
	 * @see #values(Object[])
	 */
	public SimpleQuery value(Object value) {
		this.saveDataChanged();
		this.ensure(COLLECTION_VALUES);
		mValues.add(value);
		return this;
	}

	/**
	 * Appends the current values for this query with the given ones. Use this for <b>CREATE + UPDATE</b>
	 * SQL commands only.
	 *
	 * @param values The desired values to append.
	 * @return This query to allow methods chaining.
	 * @see #value(Object)
	 */
	public SimpleQuery values(@NonNull Object[] values) {
		this.saveDataChanged();
		this.ensure(COLLECTION_VALUES);
		mValues.addAll(Arrays.asList(values));
		return this;
	}

	/**
	 * Appends the current group of values for this query with the given ones. Use this for <b>INSERT</b>
	 * SQL command only.
	 *
	 * @param values The desired set of values.
	 * @return This query to allow methods chaining.
	 * @see #columns(String[])
	 */
	public SimpleQuery bulkValues(@NonNull Object[] values) {
		this.saveDataChanged();
		this.ensure(COLLECTION_VALUE_GROUPS);
		mValueGroups.add(Arrays.asList(values));
		return this;
	}

	/**
	 * Marks this query as <b>UPDATE</b> SQL command.
	 *
	 * @return This query.
	 * @see #entity(String)
	 * @see #columns(String[])
	 * @see #values(Object[])
	 * @see #where(String)
	 */
	public SimpleQuery update() {
		this.updateCommand(SQLCommand.UPDATE);
		return this;
	}

	/**
	 * Marks this query as <b>SELECT</b> SQL command.
	 *
	 * @return This query to allow methods chaining.
	 * @see #from(String)
	 * @see #columns(String[])
	 * @see #where(String)
	 */
	public SimpleQuery select() {
		this.updateCommand(SQLCommand.SELECT);
		return this;
	}

	/**
	 * Same as {@link #entity(String)}.
	 */
	public SimpleQuery from(@NonNull String entityName) {
		return entity(entityName);
	}

	/**
	 * Sets the where condition for this query.
	 *
	 * @param condition The desired where condition.
	 * @return This query to allow methods chaining.
	 * @see #orderBy(String, String)
	 * @see #groupBy(String)
	 * @see #limit(int)
	 * @see #condition(String)
	 */
	public SimpleQuery where(@NonNull String condition) {
		return condition(condition);
	}

	/**
	 * Sets the selection arguments for this query. This arguments are used when executing this query
	 * as raw query for {@link android.database.sqlite.SQLiteDatabase#rawQuery(String, String[])}.
	 *
	 * @param args The desired selection arguments.
	 * @return This query to allow methods chaining.
	 */
	public SimpleQuery selectionArgs(@NonNull String[] args) {
		this.saveDataChanged();
		this.mSelectionArgs = args;
		return this;
	}

	/**
	 * Sets the join type for this query.
	 *
	 * @param joinType   The desired type of join.
	 * @param entityName A name of entity for which is the specified join requested.
	 * @return This query to allow methods chaining.
	 */
	public SimpleQuery join(@NonNull Join joinType, @NonNull String entityName) {
		this.saveDataChanged();
		this.appendJoins(joinType.createStatement(entityName));
		this.updatePrivateFlags(PFLAG_JOIN_REQUESTED, true);
		return this;
	}

	/**
	 * Sets the join condition for join statement for type specified by {@link #join(SimpleQuery.Join, String)}.
	 * <p>
	 * <b>Note</b>, that this must be called after {@link #join(SimpleQuery.Join, String)}
	 * was requested otherwise exception will be thrown.
	 *
	 * @param condition The desired join on condition.
	 * @return This query to allow methods chaining.
	 * @throws java.lang.IllegalStateException If join(Join, String) was not called before this call.
	 */
	public SimpleQuery on(@NonNull String condition) {
		if (!hasPrivateFlag(PFLAG_JOIN_REQUESTED)) {
			throw new IllegalStateException("Join not requested. Call join(Join, String) before this call.");
		}
		this.saveDataChanged();
		this.appendJoins(condition);
		this.updatePrivateFlags(PFLAG_JOIN_REQUESTED, false);
		return this;
	}

	/**
	 * Sets the order by condition for this query.
	 * <p>
	 * <b>Note</b>, that this should be called only once for the current state of this query, otherwise
	 * it will leads no invalid SQL query.
	 *
	 * @param condition The desired order by condition. This is basically set of columns by which to
	 *                  order by.
	 * @param keyWord   The desired order by key word. Can be one of {@link #ASC} or {@link #DESC}.
	 * @return This query to allow methods chaining.
	 * @see #groupBy(String)
	 * @see #limit(int)
	 * @see #condition(String)
	 */
	public SimpleQuery orderBy(@NonNull String condition, @NonNull String keyWord) {
		return condition(String.format(CONDITION_ORDER_BY_FORMAT, condition, keyWord));
	}

	/**
	 * Sets the group by condition for this query.
	 * <p>
	 * <b>Note</b>, that this should be called only once for the current state of this query, otherwise
	 * it will leads no invalid SQL query.
	 *
	 * @param condition The desired group by condition. This is basically set of columns by which to
	 *                  group by.
	 * @return This query to allow methods chaining.
	 * @see #orderBy(String, String)
	 * @see #limit(int)
	 * @see #condition(String)
	 */
	public SimpleQuery groupBy(@NonNull String condition) {
		return condition(String.format(CONDITION_GROUP_BY_FORMAT, condition));
	}

	/**
	 * Sets the limit for this query.
	 *
	 * @param limit The desired rows limit.
	 * @return This query to allow methods chaining.
	 * @see #orderBy(String, String)
	 * @see #groupBy(String)
	 * @see #condition(String)
	 */
	public SimpleQuery limit(int limit) {
		return condition(String.format(CONDITION_LIMIT_FORMAT, limit));
	}

	/**
	 * Marks this query as <b>DELETE</b> SQL command.
	 *
	 * @return This query to allow methods chaining.
	 * @see #from(String)
	 * @see #where(String)
	 */
	public SimpleQuery delete() {
		this.updateCommand(SQLCommand.DELETE);
		return this;
	}

	/**
	 * Appends the current conditions used for <b>UPDATE, SELECT, DELETE</b> statements.
	 *
	 * @param condition The desired condition to append to the current ones.
	 * @return This query to allow methods chaining.
	 * @see #orderBy(String, String)
	 * @see #groupBy(String)
	 * @see #limit(int)
	 */
	public SimpleQuery condition(@NonNull String condition) {
		this.saveDataChanged();
		appendConditions(condition);
		return this;
	}

	/**
	 * Builds the raw query for SQLite database from the current data depends on the current command
	 * for this query.
	 *
	 * @return Raw query for SQLite database.
	 * @throws java.lang.IllegalStateException If there is no query command resolved or data for the
	 *                                         current command are not complete or valid.
	 */
	@NonNull
	public String buildQuery() {
		// Check for cached query.
		if (!hasPrivateFlag(PFLAG_DATA_CHANGED) && !TextUtils.isEmpty(mCachedQuery)) {
			return mCachedQuery;
		}
		if (!mCommand.isValid()) {
			throw new IllegalStateException("Failed to build query. No SQL statement specified, like(INSERT -> insertModel(), SELECT -> select(), ...).");
		}
		this.updatePrivateFlags(PFLAG_DATA_CHANGED, false);
		return mCachedQuery = mBuilder.buildQuery();
	}

	/**
	 * Returns flag indicating whether this query is executable or not.
	 * <p>
	 * Query is always executable if was marked as a specific SQL command by {@link #create(String)},
	 * {@link #create(String, String)}, {@link #insert()}, {@link #update()}, {@link #select()} or
	 * {@link #delete()} and all necessary parameters were specified for the marked command.
	 *
	 * @return {@code True} if this query can be executed by one of execution related methods
	 * of this query, {@code false} otherwise.
	 */
	public boolean isExecutable() {
		switch (mCommand) {
			case NONE:
				return false;
			default:
				return mCommand.create(this).canBuildQuery(false);
		}
	}

	/**
	 * Same as {@link #executeSQL(android.database.sqlite.SQLiteDatabase)} with the current database
	 * provided during initialization of this query.
	 * <p>
	 * <b>Note</b>, that if this query is successfully executed its current SQLite database will be
	 * closed and reference to it released and this query becomes invalid and next call of this method
	 * will throw IllegalStateException.
	 *
	 * @throws IllegalStateException If there was no SQLite database provided for this query during
	 *                               initialization.
	 */
	public boolean executeSQL() {
		if (checkSQLiteDatabase()) {
			if (executeSQL(mDb)) {
				mDb.close();
				mDb = null;
				return true;
			}
			return false;
		}
		return false;
	}

	/**
	 * Executes this query upon the given SQLite database.
	 * <p>
	 * <b>Note</b>, that only <b>CREATE, INSERT, DELETE, UPDATE</b> SQL commands can be executed by
	 * this method. To execute <b>SELECT</b> SQL command use {@link #executeRaw(android.database.sqlite.SQLiteDatabase)}
	 * instead.
	 *
	 * @param db The SQLite database upon which to execute this query.
	 * @return {@code True} if this query was successfully executed, {@code false} if it is
	 * not executable or marked as a wrong SQL command.
	 * @see #isExecutable()
	 */
	public boolean executeSQL(@NonNull SQLiteDatabase db) {
		switch (mCommand) {
			case CREATE:
			case INSERT:
			case DELETE:
			case UPDATE:
			case EXPLICIT:
				final String rawQuery = buildQuery();
				if (DEBUG_ENABLED || hasPrivateFlag(PFLAG_LOG_ENABLED)) {
					Log.d(TAG, "Executing query('" + rawQuery + "').");
				}
				// Execute raw query without result.
				db.execSQL(rawQuery);
				return true;
			default:
				Log.e(TAG, "Only query with command("
						+ SQLCommand.CREATE.name() + " | "
						+ SQLCommand.INSERT.name() + " | "
						+ SQLCommand.DELETE.name() + " | "
						+ SQLCommand.UPDATE.name() + " | "
						+ SQLCommand.EXPLICIT.name()
						+ ") can be executed as SQL query.");
		}
		return false;
	}

	/**
	 * Same as {@link #executeRaw(android.database.sqlite.SQLiteDatabase)} with the current database
	 * provided during initialization of this query.
	 * <p>
	 * <b>Note</b>, that if this query is successfully executed its current SQLite database will be
	 * closed and reference to it released and this query becomes invalid and next call of this method
	 * will throw IllegalStateException.
	 *
	 * @throws IllegalStateException If there was no SQLite database provided for this query during
	 *                               initialization.
	 * @see #executeSQL()
	 */
	@Nullable
	public Cursor executeRaw() {
		if (checkSQLiteDatabase()) {
			final Cursor cursor = executeRaw(mDb);
			mDb.close();
			mDb = null;
			return cursor;
		}
		return null;
	}

	/**
	 * Executes this query upon the given SQLite database.
	 * <p>
	 * <b>Note</b>, that only <b>SELECT</b> SQL command can be executed by this method. To execute
	 * <b>CREATE, INSERT, DELETE, UPDATE</b> SQL commands use {@link #executeSQL(android.database.sqlite.SQLiteDatabase)}
	 * instead.
	 *
	 * @param db The SQLite database upon which to execute this query.
	 * @return Queried cursor or {@code null} if it is not executable or marked as a wrong SQL
	 * command.
	 * @see #isExecutable()
	 * @see #executeSQL(android.database.sqlite.SQLiteDatabase)
	 */
	@Nullable
	public Cursor executeRaw(@NonNull SQLiteDatabase db) {
		if (!isExecutable()) {
			return null;
		}

		switch (mCommand) {
			case SELECT:
			case EXPLICIT:
				final String rawQuery = buildQuery();
				if (DEBUG_ENABLED || hasPrivateFlag(PFLAG_LOG_ENABLED)) {
					Log.d(TAG, "Executing query('" + rawQuery + "').");
				}
				// Execute raw query and obtain cursor with data.
				return db.rawQuery(rawQuery, mSelectionArgs);
			default:
				Log.e(TAG, "Only query with command(" + SQLCommand.SELECT.name() + ") can be executed as raw query.");
		}
		return null;
	}

	/**
	 * Clears all values provided for this query except its reference to SQLite database if provided
	 * during initialization of this query.
	 *
	 * @return This query with initial state.
	 */
	public SimpleQuery clear() {
		this.mCommand = SQLCommand.NONE;
		this.mBuilder = null;
		this.mCreateTableStatement = "";
		this.mPrivateFlags = 0;
		this.mCachedQuery = null;
		this.mConditions = null;
		if (mColumns != null) {
			mColumns.clear();
			mColumns = null;
		}
		if (mForeignKeys != null) {
			mForeignKeys.clear();
			this.mForeignKeys = null;
		}
		if (mValues != null) {
			mValues.clear();
			this.mValues = null;
		}
		return this;
	}

	/**
	 * Getters + Setters ---------------------------------------------------------------------------
	 */

	/**
	 * Enables/disables the log output for this query regardless the {@link DatabaseConfig#DEBUG_LOG_ENABLED}
	 * flag is set to.
	 *
	 * @param enable {@code True} to enable, {@code false} to disable logging.
	 * @return This query.
	 */
	public SimpleQuery enableLogging(boolean enable) {
		this.updatePrivateFlags(PFLAG_LOG_ENABLED, enable);
		return this;
	}

	/**
	 * Protected -----------------------------------------------------------------------------------
	 */

	/**
	 * Returns a flag indicating whether this query has some conditions or not.
	 *
	 * @return {@code True} if some conditions are specified, {@code false} otherwise.
	 */
	boolean hasConditions() {
		return mConditions != null && mConditions.length() > 0;
	}

	/**
	 * Returns a flag indicating whether this query has some join statement or not.
	 *
	 * @return {@code True} if join statement is specified, {@code false} otherwise.
	 */
	boolean hasJoins() {
		return mJoins != null && mJoins.length() > 0;
	}

	/**
	 * Private -------------------------------------------------------------------------------------
	 */

	/**
	 * Updates the current type of SQL command.
	 *
	 * @param command The desired command to which set this query.
	 */
	private void updateCommand(SQLCommand command) {
		if (!mCommand.equals(command)) {
			this.saveDataChanged();
			this.mCommand = command;
			this.mBuilder = command.create(this);
		}
	}

	/**
	 * Sets the {@link #PFLAG_DATA_CHANGED} flag to true.
	 */
	private void saveDataChanged() {
		if (mCommand != SQLCommand.EXPLICIT) {
			this.updatePrivateFlags(PFLAG_DATA_CHANGED, true);
		}
	}

	/**
	 * Checks whether the current SQLite database is available.
	 *
	 * @return {@code True} if the current SQLite database is available for execution of this
	 * query, {@code false} otherwise.
	 * @throws IllegalStateException If the the current SQLite database is {@code null} or is
	 *                               already closed.
	 */
	private boolean checkSQLiteDatabase() {
		if (mDb == null || !mDb.isOpen()) {
			throw new IllegalStateException("Cannot execute query. No SQLite database specified or query was already executed.");
		}
		return true;
	}

	/**
	 * Appends the current conditions buuilder.
	 *
	 * @param condition The desired condition to append.
	 */
	private void appendConditions(String condition) {
		if (mConditions == null) {
			mConditions = new StringBuilder("");
		}
		this.appendBuilder(mConditions, condition);
	}

	/**
	 * Appends the current join statement builder.
	 *
	 * @param value The desired value to append.
	 */
	private void appendJoins(String value) {
		if (mJoins == null) {
			mJoins = new StringBuilder("");
		}
		this.appendBuilder(mJoins, value);
	}

	/**
	 * Appends the given <var>builder</var> with the given <var>value</var>.
	 *
	 * @param builder Builder to which the given value append.
	 * @param value   Value to append.
	 */
	private void appendBuilder(StringBuilder builder, String value) {
		if (TextUtils.isEmpty(value)) {
			return;
		}
		if (builder.length() > 0) {
			builder.append(" ");
		}
		builder.append(value);
	}

	/**
	 * Ensures that a member represented by the given <var>type</var> flag is initialized.
	 *
	 * @param type Flag of the member to ensure initialization of.
	 */
	private void ensure(int type) {
		switch (type) {
			case COLLECTION_COLUMNS:
				if (mColumns == null) {
					this.mColumns = new ArrayList<>();
				}
				break;
			case COLLECTION_VALUES:
				if (mValues == null) {
					this.mValues = new ArrayList<>();
				}
				break;
			case COLLECTION_VALUE_GROUPS:
				if (mValueGroups == null) {
					this.mValueGroups = new ArrayList<>();
				}
				break;
			case COLLECTION_FOREIGN_KEYS:
				if (mForeignKeys == null) {
					this.mForeignKeys = new ArrayList<>();
				}
				break;
		}
	}

	/**
	 * Updates the current private flags.
	 *
	 * @param flag Value of the desired flag to add/remove to/from the current private flags.
	 * @param add  Boolean flag indicating whether to add or remove the specified <var>flag</var>.
	 */
	private void updatePrivateFlags(int flag, boolean add) {
		if (add) {
			this.mPrivateFlags |= flag;
		} else {
			this.mPrivateFlags &= ~flag;
		}
	}

	/**
	 * Returns a boolean flag indicating whether the specified <var>flag</var> is contained within
	 * the current private flags or not.
	 *
	 * @param flag Value of the flag to check.
	 * @return {@code True} if the requested flag is contained, {@code false} otherwise.
	 */
	private boolean hasPrivateFlag(int flag) {
		return (mPrivateFlags & flag) != 0;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}

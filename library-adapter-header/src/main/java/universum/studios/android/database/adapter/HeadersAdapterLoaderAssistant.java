/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import universum.studios.android.database.content.BaseLoaderAssistant;
import universum.studios.android.database.content.DataHeaders;
import universum.studios.android.database.content.HeadersResult;

/**
 * A {@link BaseLoaderAssistant} implementation that may be used to support loading of {@link Cursor Cursor}
 * data set with associated <b>Headers</b> for instances of {@link LoadableHeadersAdapter LoadableHeadersAdapters}.
 *
 * <h3>Loading flow</h3>
 * <ul>
 * <li>
 * {@link #onCreateLoader(int, Bundle)}
 * <p>
 * calls {@link LoadableHeadersAdapter#createHeadersLoader(int, Bundle)} upon the attached adapter,
 * </li>
 * <li>
 * {@link #onLoadFinished(int, HeadersResult)}
 * <p>
 * calls {@link LoadableHeadersAdapter#changeLoaderDataWithHeaders(int, Object, DataHeaders)} upon the
 * attached adapter,
 * </li>
 * <li>
 * {@link #onLoaderReset(Loader)}
 * <p>
 * calls {@link LoadableHeadersAdapter#changeLoaderDataWithHeaders(int, Object, DataHeaders)} LoadableHeadersAdapter#changeLoaderDataWithHeaders(int, null)}
 * upon the attached adapter to invalidate its data set.
 * </li>
 * </ul>
 *
 * @param <D> Type of the data that a loader will load for this assistant.
 * @param <H> Type of the headers associated with the loadable data.
 * @author Martin Albedinsky
 */
@SuppressLint("LongLogTag")
public class HeadersAdapterLoaderAssistant<D, H extends DataHeaders<D>> extends BaseLoaderAssistant<HeadersResult<D, H>> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "HeadersAdapterLoaderAssistant";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Adapter of which cursor + headers data set loading will this assistant manage.
	 */
	private LoadableHeadersAdapter<D, H> mAdapter;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of HeadersAdapterLoaderAssistant to be used for {@link Cursor} +
	 * <b>Headers</b> data loading for an instance of {@link LoadableHeadersAdapter}.
	 *
	 * @see BaseLoaderAssistant#BaseLoaderAssistant(Context, LoaderManager)
	 * @see #attachAdapter(LoadableHeadersAdapter)
	 */
	public HeadersAdapterLoaderAssistant(@NonNull Context context, @NonNull LoaderManager loaderManager) {
		super(context, loaderManager);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Attaches a loader adapter of which data set with headers loading should be handled by this
	 * assistant.
	 *
	 * @param adapter The adapter of which data set loading including headers will handle this assistant.
	 * @see #getAttachedAdapter()
	 */
	public void attachAdapter(@Nullable LoadableHeadersAdapter<D, H> adapter) {
		this.mAdapter = adapter;
	}

	/**
	 * Returns the adapter that is currently attached to this assistant.
	 *
	 * @return The adapter that has been attached via {@link #attachAdapter(LoadableHeadersAdapter)}
	 * or {@code null} if no adapter is attached.
	 */
	@Nullable
	public LoadableHeadersAdapter<D, H> getAttachedAdapter() {
		return mAdapter;
	}

	/**
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Loader<HeadersResult<D, H>> onCreateLoader(@IntRange(from = 0) int loaderId, @Nullable Bundle arguments) {
		return mAdapter == null ? null : mAdapter.createHeadersLoader(loaderId, arguments == null ? LoadableHeadersAdapter.EMPTY_ARGUMENTS : arguments);
	}

	/**
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void onLoadFinished(@IntRange(from = 0) int loaderId, @NonNull HeadersResult<D, H> result) {
		if (mAdapter != null) mAdapter.changeLoaderDataWithHeaders(loaderId, result.data, result.headers);
	}

	/**
	 */
	@Override
	protected void onLoadFailed(@IntRange(from = 0) int loaderId) {
		if (mAdapter != null) {
			mAdapter.changeLoaderDataWithHeaders(loaderId, null, null);
			final String adapterName = mAdapter.getClass().getSimpleName();
			Log.e(TAG, "Received invalid headers cursor for adapter(" + adapterName + ") from loader with id(" + loaderId + ").");
		}
	}

	/**
	 */
	@Override
	public void onLoaderReset(@NonNull Loader<HeadersResult<D, H>> loader) {
		if (mAdapter != null) mAdapter.changeLoaderDataWithHeaders(loader.getId(), null, null);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}
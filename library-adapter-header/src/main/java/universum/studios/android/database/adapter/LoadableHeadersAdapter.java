/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.database.adapter;

import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.Loader;
import universum.studios.android.database.content.DataHeaders;
import universum.studios.android.database.content.HeadersResult;

/**
 * An interface for adapters of which data set along with the associated headers can be loaded via
 * Android framework's {@link Loader Loaders}
 *
 * @param <D> Type of the data to be loaded as data set for this loadable adapter.
 * @param <H> Type of the headers associated with the loadable data.
 * @author Martin Albedinsky
 */
public interface LoadableHeadersAdapter<D, H extends DataHeaders<D>> {

	/**
	 * Empty arguments for loader.
	 */
	@NonNull
	Bundle EMPTY_ARGUMENTS = Bundle.EMPTY;

	/**
	 * Creates the loader for the requested <var>loaderId</var> that should load the desired data
	 * with headers.
	 *
	 * @param loaderId  Id of the desired loader to create.
	 * @param arguments Bundle with arguments for the loader. Should be {@link #EMPTY_ARGUMENTS} if
	 *                  loader does not require any arguments.
	 * @return Requested headers loader or {@code null} if this adapter does not create loader for
	 * the specified id.
	 */
	@Nullable
	Loader<HeadersResult<D, H>> createHeadersLoader(@IntRange(from = 0) int loaderId, @NonNull Bundle arguments);

	/**
	 * Changes the given <var>data</var> along with <var>headers</var> of this adapter loaded by the
	 * loader with the specified <var>loaderId</var>.
	 *
	 * @param loaderId Id of the loader that has loaded the given data.
	 * @param data     Loaded data to be changed for this adapter.
	 * @param headers  Headers created from the loaded data to be changed for this adapter.
	 * @see #createHeadersLoader(int, Bundle)
	 */
	void changeLoaderDataWithHeaders(@IntRange(from = 0) int loaderId, @Nullable D data, @Nullable H headers);
}
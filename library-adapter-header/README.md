Database-Adapter-Header
===============

This module contains elements that are used by cursor adapters to support creation of **headers**
for `Cursor` data set.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adatabase/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adatabase/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:database-adapter-header:${DESIRED_VERSION}@aar"

_depends on:_
[database-core](https://bitbucket.org/android-universum/database/src/main/library-core),
[database-content](https://bitbucket.org/android-universum/database/src/main/library-content_group),
[database-cursor](https://bitbucket.org/android-universum/database/src/main/library-cursor)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [LoadableHeadersAdapter](https://bitbucket.org/android-universum/database/src/main/library-adapter-header/src/main/java/universum/studios/android/database/adapter/LoadableHeadersAdapter.java)
- [HeadersAdapterLoaderAssistant](https://bitbucket.org/android-universum/database/src/main/library-adapter-header/src/main/java/universum/studios/android/database/adapter/HeadersAdapterLoaderAssistant.java)